import { PactUtil } from './util';

// Give pact some time to spin everything up.
jest.setTimeout(30000);

const provider = PactUtil.getInstance();

beforeAll(async () => await provider.setup());
afterEach(async () => await provider.verify());
afterAll(async () => await provider.finalize());
