import { PactUtil } from './util';

PactUtil.initialize({
  consumer: ' -api',
  pactfileWriteMode: 'overwrite',
  provider: process.env.PACT_PROVIDER,
  port: parseInt(process.env.PACT_PORT, 10),
});
