import { term } from '@pact-foundation/pact/dsl/matchers';

enum ContentType {
  JSON,
  FORM_URLENCODED,
}

function mapContentTypeToMatcher(contentType: ContentType): { generate: string; matcher: string } {
  switch (contentType) {
    case ContentType.JSON:
      return {
        generate: 'application/json; charset=utf-8',
        matcher: '^\\s*application/json(?:;\\s*charset=[Uu][Tt][Ff]-\\d)?\\s*$',
      };
    case ContentType.FORM_URLENCODED:
      return {
        generate: 'application/x-www-form-urlencoded',
        matcher: '^\\s*application/x-www-form-urlencoded\\s*',
      };
  }
}

const base64Matcher = (generate: string) =>
  term({ generate, matcher: '(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$' });

const basicAuthHeaderMatcher = (generate: string) =>
  term({ generate, matcher: '^Basic (?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$' });

const bearerAuthHeaderMatcher = (generate: string) => term({ generate, matcher: '^Bearer [a-f0-9]+$' });

const contentTypeMatcher = (contentType: ContentType) => term(mapContentTypeToMatcher(contentType));

export { base64Matcher, basicAuthHeaderMatcher, bearerAuthHeaderMatcher, contentTypeMatcher, ContentType };
