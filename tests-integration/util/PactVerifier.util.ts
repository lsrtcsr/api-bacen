import * as path from 'path';
import { Verifier, VerifierOptions } from '@pact-foundation/pact';

export interface PactVerifierOptions extends Partial<VerifierOptions> {
  providerBaseUrl: string;
}

const local = process.env.USER ? `-local_${process.env.USER}` : '';

export class PactVerifier {
  protected static instance: PactVerifier;

  protected readonly verifier: Verifier;

  protected constructor(opts: PactVerifierOptions) {
    this.verifier = new Verifier({
      provider: ' -api',
      logLevel: 'debug',
      pactBrokerUrl: 'https://pact-nightly.bt .app',
      publishVerificationResult: true,
      providerVersion: `${require('../../package.json').version}${local}`,
      ...opts,
    });
  }

  public static initialize(opts: PactVerifierOptions): PactVerifier {
    return (this.instance = new PactVerifier(opts));
  }

  public static getInstance(): PactVerifier {
    if (!this.instance) {
      throw new Error('PactVerifier is not initialized yet');
    }

    return this.instance;
  }

  public verifyProvider(config?: VerifierOptions): Promise<any> {
    return this.verifier.verifyProvider(config);
  }
}
