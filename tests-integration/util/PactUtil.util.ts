import * as path from 'path';
import { Pact } from '@pact-foundation/pact';
import { PactOptions } from '@pact-foundation/pact/dsl/options';

export interface PactUtilOptions extends Partial<PactOptions> {
  provider: string;
  consumer: string;
}

export class PactUtil {
  protected static instance: Pact;

  public static initialize(opts: PactUtilOptions) {
    return (this.instance = new Pact({
      port: 8989,
      log: path.resolve(process.cwd(), 'logs', 'mockserver-integration.log'),
      dir: path.resolve(process.cwd(), 'pacts'),
      spec: 2,
      cors: true,
      pactfileWriteMode: 'update',
      ...opts,
    }));
  }

  public static getInstance() {
    if (!this.instance) {
      throw new Error('PactUtil has not been initialized yet!');
    }

    return this.instance;
  }
}
