import { AxiosError } from 'axios';
import { Logger } from 'nano-errors';
import { CustodyFeature, PaymentType, TransactionType, PaymentSchema } from '@bacen/base-sdk';
import { PactUtil } from '../util';
import CDTProviderService from '../../api/services/provider/CDTProviderService';
import { walletDepositInfoInteraction, mockWalletId, walletWithdrawInteraction } from '../interactions';

const pactProvider = PactUtil.getInstance();

const paymentSchema: PaymentSchema = {
  id: 'a45f8022-9dbd-4622-9caf-b698b61cf195',
  type: PaymentType.WITHDRAWAL,
  amount: '100.01',
  transaction: {
    additionalData: {
      bankingId: '711665b8-53d5-4a31-b7da-8129c4a67669',
    } as any,
    id: '87f47d70-1a0a-4838-a5b3-41617ed6448d',
    type: TransactionType.PAYMENT,
    source: {
      id: '8bc1737c-6864-4224-bdcb-3a0526200d21',
      stellar: {
        publicKey: 'GDOYLAANTKKGB3YG74OZDWJ3FNAX4N6UYDR6WAQ3H5FVDSWI5PXDN4Y2',
        secretKey: 'SBH4OH76KP3NTIHZTAM6MB4SAUTK7YR4JSFKT5PSGE4YWSEXPFLHYTVJ',
      },
    },
  },
};

const extra = {
  account: 615880,
  accountDigit: '0',
  agency: 1,
  agencyDigit: '0',
  bank: '341',
  holderType: 'personal',
  id: '711665b8-53d5-4a31-b7da-8129c4a67669',
  name: 'Garfield Kutch',
  taxId: '32129008143',
  type: 'checking',
};

describe('integration/lib.services.CDTProviderWebService', () => {
  let provider: CDTProviderService;
  let err: AxiosError;

  beforeAll(() => {
    provider = CDTProviderService.initialize({
      baseURL: 'http://localhost:8989',
      logger: Logger.initialize(),
    });
  });

  afterAll(() => {
    CDTProviderService['instance'] = undefined;
  });

  afterEach(() => {
    err = undefined;
  });

  it("Requests a wallet's deposit info", async () => {
    await pactProvider.addInteraction(walletDepositInfoInteraction);

    const response = await provider.feature(CustodyFeature.DEPOSIT).info({ id: mockWalletId });
    expect(response).toHaveProperty('taxId');
  });

  it('Withdraws from a wallet', async () => {
    await pactProvider.addInteraction(walletWithdrawInteraction);

    const response = await provider.feature(CustodyFeature.WITHDRAW).withdraw(paymentSchema, extra);
    expect(response).toHaveProperty('id');
  });
});
