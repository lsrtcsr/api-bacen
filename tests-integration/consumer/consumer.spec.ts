import * as faker from 'faker';
import { UserSchema, WalletSchema, AccountType, ConsumerStatus, DomainRole, AddressStatus } from '@bacen/base-sdk';
import { Logger } from 'nano-errors';
import { AxiosError } from 'axios';
import { PactUtil } from '../util';
import CDTProviderService from '../../api/services/provider/CDTProviderService';
import { registerConsumerInteraction, invalidRegisterConsumerInteraction } from '../interactions';

const pactProvider = PactUtil.getInstance();

const userSchema: UserSchema = {
  id: faker.random.uuid(),
  name: `${faker.name.firstName()} ${faker.name.lastName()}`,
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  email: faker.internet.email(),
  consumer: {
    type: AccountType.PERSONAL,
    birthday: faker.date.past(),
    status: ConsumerStatus.READY,
    taxId: '00000000000',
    addresses: [
      {
        country: 'BR',
        state: 'SP',
        city: 'São Paulo',
        code: '74110090',
        street: 'Av Paulista',
        number: '111',
        neighborhood: 'Bela Vista',
        complement: 'apto 11',
        status: AddressStatus.OWN,
      },
    ],
    phones: [
      {
        code: '11',
        number: '999999999',
      },
    ],
  },
  domain: {
    id: faker.random.uuid(),
    name: 'test_domain',
    role: DomainRole.ROOT,
  },
};

const walletSchema: WalletSchema = {
  id: faker.random.uuid(),
};

const invalidWallet: WalletSchema = {
  id: faker.random.uuid(),
};

describe('integration/lib.services.CDTProviderWebService', () => {
  let provider: CDTProviderService;
  let err: AxiosError;

  beforeAll(() => {
    provider = CDTProviderService.initialize({
      baseURL: 'http://localhost:8989',
      logger: Logger.initialize(),
    });
  });

  afterAll(() => {
    CDTProviderService['instance'] = undefined;
  });

  afterEach(() => {
    err = undefined;
  });

  it('successfully register a user with CDT', async () => {
    await pactProvider.addInteraction(registerConsumerInteraction);

    const response = await provider.register(userSchema, walletSchema);
    expect(response).toBeDefined();
    expect(response).toHaveProperty('externalId');
  });

  it('rejects a consumer without a wallet', async () => {
    await pactProvider.addInteraction(invalidRegisterConsumerInteraction);

    try {
      await provider.register(userSchema, invalidWallet);
    } catch (e) {
      err = e;
    }

    expect(err).toBeDefined();
    expect(err.response.status).toBe(500);
  });
});
