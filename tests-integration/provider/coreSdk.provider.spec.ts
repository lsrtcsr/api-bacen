import { BaseRequest, BaseResponse } from 'ts-framework';
import * as sinon from 'sinon';
import { UserRole, CustodyDepositWebService, CustodyWithdrawWebService } from '@bacen/base-sdk';
import { PactVerifier, ConsumerTestUtil, OAuthTestUtil, UserTestUtil, WalletTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import { User, OAuthClient, OAuthAccessToken, Wallet } from '../../api/models';
import { genPassword } from '../../api/models/user/helpers';
import { KYCUtil } from '../../api/utils/KYCUtil';

jest.setTimeout(6000000);

describe('Pact verification', () => {
  // Models
  let client: OAuthClient;
  let user: User;
  let token: OAuthAccessToken;
  let server: MainServer;
  let mediator: User;
  let wallet: Wallet;

  // Stubs
  let KYCStub: sinon.SinonStub;
  let depositFeatureStub: sinon.SinonStub;
  let withdrawFeatureStub: sinon.SinonStub;
  let walletRegisterAsseProviderStub: sinon.SinonStub;
  let walletGetBalanceStub: sinon.SinonStub;

  // Flags
  let triggerInvalidClient = false;
  let setAccessToken = false;

  async function cleanState() {
    const createdUser = await User.findOne({ email: 'satoshi@nakamoto.com' }, { relations: ['consumer'] });

    if (createdUser) {
      await UserTestUtil.destroyUser(createdUser);
    }

    // destroyUser also deletes
    // - wallets
    // - tokens
    if (user) {
      if (user.role === UserRole.CONSUMER) {
        await ConsumerTestUtil.destroyConsumer(user, false);
      }

      await UserTestUtil.destroyUser(user);
      user = null;
    }

    if (client) {
      await OAuthTestUtil.destroyClient(client);
      client = null;
    }

    if (mediator) {
      await ConsumerTestUtil.destroyConsumer(mediator);
      await UserTestUtil.destroyUser(mediator);
      mediator = null;
    }

    if (walletRegisterAsseProviderStub) {
      walletRegisterAsseProviderStub.restore();
      walletRegisterAsseProviderStub = null;
    }

    if (walletGetBalanceStub) {
      walletGetBalanceStub.restore();
      walletGetBalanceStub = null;
    }

    triggerInvalidClient = false;
    setAccessToken = false;
  }

  beforeAll(async () => {
    server = new MainServer();

    KYCStub = sinon.stub(KYCUtil, 'fillBasicData').resolvesArg(0);
    depositFeatureStub = sinon.stub(CustodyDepositWebService.prototype, 'info').resolves({
      name: 'Satoshi Nakamoto',
      taxId: '11111111111',
      externalId: '711665b8-53d5-4a31-b7da-8129c4a67669',
      bank: {
        code: '340',
        name: 'Bradesco',
      },
      account: {
        holder: 'Bit . Ltda.',
        agency: '1234',
        number: '123456',
        digit: '2',
      },
    });

    withdrawFeatureStub = sinon.stub(CustodyWithdrawWebService.prototype, 'withdraw').resolves(true as any);

    await server.listen();
  });

  afterAll(async () => {
    await cleanState();

    depositFeatureStub.restore();
    KYCStub.restore();
    withdrawFeatureStub.restore();

    await server.close();
  });

  it('validates all pacts', async () => {
    const authenticateUser = async () => {
      client = await OAuthTestUtil.generateClient();

      user = await UserTestUtil.generateUser({
        userPartial: {
          email: 'user@company.com',
        },
        relations: ['domain'],
        role: UserRole.CONSUMER,
      });

      token = await OAuthTestUtil.generateTokenFromUser(user, client, {
        refreshToken: '98d7714411d4dee683ce61db3d5c49caa0e6e63b',
      });

      setAccessToken = true;
    };

    const stateHandlers: { [s: string]: () => Promise<any> } = {
      'Password credentials are invalid': async () => {
        await cleanState();

        client = await OAuthTestUtil.generateClient();
        const { salt, hash } = await genPassword('wrongpassword');

        user = await UserTestUtil.generateUser({
          userPartial: {
            email: 'user@company.com',
            passwordSalt: salt,
            passwordHash: hash,
          },
          relations: ['domain'],
        });
      },
      'Password credentials are valid': async () => {
        await cleanState();

        client = await OAuthTestUtil.generateClient();

        user = await UserTestUtil.generateUser({
          userPartial: {
            email: 'user@company.com',
          },
          relations: ['domain'],
        });
      },
      'Client is invalid': async () => {
        await cleanState();
        triggerInvalidClient = true;
      },
      'User is authenticated': async () => {
        await cleanState();
        await authenticateUser();
      },
      'User is unauthenticated': async () => {
        await cleanState();
      },
      'Refresh token is valid': async () => {
        await cleanState();
        await authenticateUser();
      },
      'Client credentials are invalid': async () => {
        await cleanState();
        client = await OAuthTestUtil.generateClient();
      },
      'Client credentials are valid': async () => {
        await cleanState();
        client = await OAuthTestUtil.generateClient();
      },
      'User is authenticated and domain has mediator': async () => {
        await cleanState();
        await authenticateUser();

        mediator = await UserTestUtil.generateUser({
          userDomain: user.domain,
          role: UserRole.MEDIATOR,
          userPartial: {
            email: 'mediator@company.com',
          },
        });
      },
      'User is authenticated and wallet exists': async () => {
        await cleanState();
        await authenticateUser();

        await ConsumerTestUtil.generateConsumerForUser(user);

        walletRegisterAsseProviderStub = sinon.stub(Wallet.prototype, 'registerAsset').returns(undefined);

        walletGetBalanceStub = sinon.stub(Wallet.prototype, 'getBalances').resolves([
          {
            asset_code: 'BRLD',
            asset_type: 'credit_alphanum4',
            balance: '1000.00',
          },
        ] as any);

        wallet = await WalletTestUtil.generateAndProcess(user, {
          walletId: '8bc1737c-6864-4224-bdcb-3a0526200d21',
        });
      },
      'User is authenticated, wallet and banking exists': async () => {
        await cleanState();
        await authenticateUser();

        const updatedUser = await ConsumerTestUtil.generateConsumerForUser(user);

        await ConsumerTestUtil.generateBanking({
          id: '711665b8-53d5-4a31-b7da-8129c4a67669',
          consumerId: updatedUser.consumer.id,
          taxId: updatedUser.consumer.taxId,
          name: user.name,
        });

        walletRegisterAsseProviderStub = sinon.stub(Wallet.prototype, 'registerAsset').returns(undefined);

        walletGetBalanceStub = sinon.stub(Wallet.prototype, 'getBalances').resolves([
          {
            asset_code: 'BRLD',
            asset_type: 'credit_alphanum4',
            balance: '1000.00',
          },
        ] as any);

        wallet = await WalletTestUtil.generateAndProcess(user, {
          walletId: '8bc1737c-6864-4224-bdcb-3a0526200d21',
        });
      },
      'User is authenticated, banking does not exist': async () => {
        await cleanState();
        await authenticateUser();
      },
    };

    const requestFilter = (req: BaseRequest, res: BaseResponse, next: any) => {
      if (req.path !== '/_pactSetup') {
        switch (req.path) {
          case '/oauth/token':
            if (!triggerInvalidClient) {
              const mask = `${client.clientId}:${client.clientSecret}`;
              const basicToken = Buffer.from(mask).toString('base64');

              // gotcha: headers on req must all be lowercase
              req.headers['authorization'] = `Basic ${basicToken}`;
              triggerInvalidClient = false;
            }
            break;
          default:
            if (setAccessToken) {
              req.headers['authorization'] = `Bearer ${token.accessToken}`;
              setAccessToken = false;
            }
        }
      }

      next();
    };

    const verifier = PactVerifier.initialize({
      requestFilter,
      stateHandlers,
      providerBaseUrl: 'http://localhost:3000',
      logLevel: 'info',
      timeout: 1000000,
      tags: process.env.PACT_TAGS && process.env.PACT_TAGS.split(','),
    });

    const result = await verifier.verifyProvider();
    expect(result).not.toBeUndefined();
  });
});
