// ts-jest has no global setup and jest can't speak typescript
require('source-map-support').install();
require('ts-node/register');

const { ConfigUtil } = require('../api/utils/ConfigUtil');

ConfigUtil.initialize(process.env.NODE_ENV);

const wrapper = require('@pact-foundation/pact-node').default;

module.exports = async () => {
  // used to kill any left over mock server instances
  process.on('SIGINT', () => {
    wrapper.removeAllServers();
  });
};
