import { InteractionObject } from '@pact-foundation/pact';
import { uuid, like, term, eachLike, iso8601DateTimeWithMillis } from '@pact-foundation/pact/dsl/matchers';
import { AccountType, DomainRole, ConsumerStatus, PaymentType, TransactionType } from '@bacen/base-sdk';
import { base64Matcher, contentTypeMatcher, ContentType } from '../util/PactMatchers.util';

export const mockWalletId = '8bc1737c-6864-4224-bdcb-3a0526200d21';

const registerConsumerInteraction: InteractionObject = {
  state: 'consumer is not registered',
  uponReceiving: 'A request for registering a consumer with CDT',
  withRequest: {
    method: 'POST',
    path: '/provider/register',
    headers: {
      'Content-Type': contentTypeMatcher(ContentType.JSON),
    },
    body: {
      user: {
        id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
        name: like('Satoshi Nakamoto'),
        firstName: like('Satoshi'),
        lastName: like('Nakamoto'),
        email: like('Satoshi@nakamoto.com'),
        consumer: {
          taxId: '00000000000',
          type: AccountType.PERSONAL,
          status: ConsumerStatus.READY,
          birthday: iso8601DateTimeWithMillis('2019-01-01T00:00:00.000Z'),
          phones: [
            {
              code: term({ generate: '11', matcher: '\\d{2}' }),
              number: like('999999999'),
            },
          ],
          addresses: eachLike({
            country: 'BR',
            state: 'SP',
            city: 'São Paulo',
            code: '74110090',
            street: 'Av Paulista',
            number: '111',
            neighborhood: 'Bela Vista',
            complement: 'apto 11',
            status: 'own',
          }),
        },
        domain: {
          id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
          name: 'test_domain',
          role: DomainRole.ROOT,
        },
      },
      wallet: {
        id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
      },
    },
  },
  willRespondWith: {
    status: 200,
    body: {
      externalId: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
    },
  },
};

const invalidRegisterConsumerInteraction: InteractionObject = {
  state: 'consumer is not registered',
  uponReceiving: 'An invalid request for registering a consumer with CDT',
  withRequest: {
    method: 'POST',
    path: '/provider/register',
    headers: {
      'Content-Type': contentTypeMatcher(ContentType.JSON),
    },
    body: {
      user: {
        id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
        name: like('Satoshi Nakamoto'),
        firstName: like('Satoshi'),
        lastName: like('Nakamoto'),
        email: like('Satoshi@nakamoto.com'),
        consumer: {
          taxId: '00000000000',
          type: AccountType.PERSONAL,
          status: ConsumerStatus.READY,
          birthday: iso8601DateTimeWithMillis('2019-01-01T00:00:00.000Z'),
          phones: [
            {
              code: term({ generate: '11', matcher: '\\d{2}' }),
              number: like('999999999'),
            },
          ],
          addresses: eachLike({
            country: 'BR',
            state: 'SP',
            city: 'São Paulo',
            code: '74110090',
            street: 'Av Paulista',
            number: '111',
            neighborhood: 'Bela Vista',
            complement: 'apto 11',
            status: 'own',
          }),
        },
        domain: {
          id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
          name: 'test_domain',
          role: DomainRole.ROOT,
        },
      },
      wallet: {
        id: uuid('a0f56ab4-b883-4037-a5b1-ef0ef63b4a71'),
      },
    },
  },
  willRespondWith: {
    status: 500,
    body: {
      message: term({
        generate:
          '[500] Wallet has no product associated, it may be invalid or corrupted. (stackId: 6e31db19-ec04-42f7-a856-0ee719d498e5)',
        matcher: '\\[500\\] Wallet has no product associated, it may be invalid or corrupted\\..*',
      }),
    },
  },
};

const walletDepositInfoInteraction: InteractionObject = {
  state: 'Wallet exists',
  uponReceiving: "A request for a wallet's deposit info",
  withRequest: {
    method: 'GET',
    path: `/provider/deposit/${mockWalletId}/info`,
  },
  willRespondWith: {
    status: 200,
    headers: {
      'Content-Type': contentTypeMatcher(ContentType.JSON),
    },
    body: {
      name: like('Satoshi Nakamoto'),
      taxId: like('11111111111'),
      externalId: like(2),
      bank: like({
        code: '340',
        name: 'Bradesco',
      }),
      account: like({
        holder: 'Bit . Ltda.',
        agency: '1234',
        number: '123456',
        digit: '2',
      }),
    },
  },
};

const walletWithdrawInteraction: InteractionObject = {
  state: 'Wallet exists',
  uponReceiving: 'A request for withdrawing from a wallet',
  withRequest: {
    method: 'POST',
    path: '/provider/withdraw/',
    headers: {
      'Content-Type': contentTypeMatcher(ContentType.JSON),
    },
    body: {
      payment: {
        id: uuid('a45f8022-9dbd-4622-9caf-b698b61cf195'),
        type: PaymentType.WITHDRAWAL,
        amount: like('100.01'),
        transaction: {
          additionalData: {
            bankingId: uuid('711665b8-53d5-4a31-b7da-8129c4a67669'),
          },
          id: uuid('87f47d70-1a0a-4838-a5b3-41617ed6448d'),
          type: TransactionType.PAYMENT,
          source: {
            id: uuid('8bc1737c-6864-4224-bdcb-3a0526200d21'),
            stellar: {
              publicKey: base64Matcher('GDOYLAANTKKGB3YG74OZDWJ3FNAX4N6UYDR6WAQ3H5FVDSWI5PXDN4Y2'),
              secretKey: base64Matcher('SBH4OH76KP3NTIHZTAM6MB4SAUTK7YR4JSFKT5PSGE4YWSEXPFLHYTVJ'),
            },
          },
        },
      },
      extra: like({
        account: 615880,
        accountDigit: '0',
        agency: 1,
        agencyDigit: '0',
        bank: '341',
        holderType: 'personal',
        id: uuid('711665b8-53d5-4a31-b7da-8129c4a67669'),
        name: 'Garfield Kutch',
        taxId: '32129008143',
        type: 'checking',
      }),
    },
  },
  willRespondWith: {
    status: 200,
    headers: {
      'Content-Type': contentTypeMatcher(ContentType.JSON),
    },
    body: {
      id: uuid('a45f8022-9dbd-4622-9caf-b698b61cf195'),
      type: PaymentType.WITHDRAWAL,
      amount: like('100.01'),
      transaction: {
        additionalData: {
          bankingId: uuid('711665b8-53d5-4a31-b7da-8129c4a67669'),
        },
        id: uuid('87f47d70-1a0a-4838-a5b3-41617ed6448d'),
        type: TransactionType.PAYMENT,
        source: {
          id: uuid(mockWalletId),
          root: false,
          stellar: {
            publicKey: base64Matcher('GDOYLAANTKKGB3YG74OZDWJ3FNAX4N6UYDR6WAQ3H5FVDSWI5PXDN4Y2'),
            secretKeY: base64Matcher('SBH4OH76KP3NTIHZTAM6MB4SAUTK7YR4JSFKT5PSGE4YWSEXPFLHYTVJ'),
          },
        },
      },
    },
  },
};

export {
  registerConsumerInteraction,
  invalidRegisterConsumerInteraction,
  walletDepositInfoInteraction,
  walletWithdrawInteraction,
};
