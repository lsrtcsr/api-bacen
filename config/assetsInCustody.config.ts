import * as path from 'path';

export default {
  initialAssetsConfigPath:
    process.env.INITIAL_ASSETS_CONFIG_PATH || path.join(__dirname, './env/initialAssetsConfig.json'),
};
