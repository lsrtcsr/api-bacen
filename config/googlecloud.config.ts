import { BaseError } from 'nano-errors';
import { logger } from './logger.config';

if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  if (process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production') {
    throw new BaseError('Missing google application credentials');
  }

  logger.warn('Missing google application credentials, Google Cloud features will not be available');
}

const storage = {
  bucketName: process.env.GCP_BUCKET_NAME,
};

const documentPhotoStorage = {
  downloadableLinkExpirationTime: process.env.GCP_DOCS_DOWNLOADABLE_LINK_EXPIRATION_TIME
    ? Number(process.env.GCP_DOCS_DOWNLOADABLE_LINK_EXPIRATION_TIME)
    : 7, // (days)
  bucketName: process.env.GCP_DOCS_BUCKET_NAME,
};

const pubSub = {
  topics: {
    strProvider: {
      projectId: process.env.GCP_PUBSUB_TOPICS_STRPROVIDERS_PROJECT_ID,
      registration: process.env.GCP_PUBSUB_TOPICS_STRPROVIDERS_REGISTRATION,
    },
  },
};

const topics = {
  walletPostback: process.env.WALLET_POSTBACK_QUEUE,
  transactionPostback: process.env.TRANSACTION_POSTBACK_QUEUE,
};

export default {
  storage,
  documentPhotoStorage,
  pubSub,
  projectId: process.env.GCLOUD_PROJECT_ID,
  location: process.env.GCLOUD_LOCATION || 'us-east1-b',
  topics,
};
