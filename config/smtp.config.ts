export default {
  from: process.env.MAIL_FROM || ' @bt .app',
  connectionUrl: process.env.SMTP_URL,
};
