const fifteenMinutes = 60 * 15;
const coolDownTime = 60; // secs

export default {
  maxRetries: 5,
  tokenTTL: +process.env.SMS_TOKEN_TTL_SECS || fifteenMinutes,
  coolDownTime: +process.env.SMS_COOL_DOWN_SECS || coolDownTime,
  secret: process.env.BT _2FV_SECRET,
  smsBodyPrefix: '',
  smsBodyVerificationPrefix: '',
};
