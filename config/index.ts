// This is important and must be kept as below
/* eslint import/first: 0 */
require('../api/utils/ConfigUtil').ConfigUtil.initialize(process.env.NODE_ENV);

// Leave this here to prevent problems with module loading order
import { Asset } from '../api/models';
import alerts from './alerts.config';
import { api } from './api.config';
import bigdatacorp from './bigdatacorp.config';
import cards from './cards.config';
import { database } from './database.config';
import email from './emailresetpassword.config';
import googlecloud from './googlecloud.config';
import identity from './identity.config';
import postbackDelivery from './postbackDelivery.config';
import celcoinProvider from './celcoinProvider.config';
import issues from './issues.config';
import jira from './jira.config';
import kyc from './kyc.config';
import oauth from './oauth.config';
import otp from './otp.config';
import payment from './payment.config';
import provider from './provider.config';
import queue from './queue.config';
import redis from './redis.config';
import seed from './seed';
import server from './server.config';
import slack from './slack.config';
import sms from './sms.config';
import smtp from './smtp.config';
import stellar from './stellar.config';
import { timescale } from './timescale.config';
import creditasprovider from './creditasprovider.config';
import paratiProvider from './paratiProvider.config';
import unleash from './unleash.config';
import user from './user.config';
import billing from './billing.config';
import bs2 from './bs2.config';
import assetsInCustody from './assetsInCustody.config';
import consumerLegal from "./consumer.legal";
import authorizer from './authorizer.config';
import hooksproxy from './hooksproxy.config';
import stellarDb from './horizonDb.config';
import * as asset from './assets.config';
import { pubSub, pubSubConfig } from './pubsub.config'
import ccs from './ccs.config';
import dict from './dict.config';
import metrics from './metrics.config';
import spb from './spb.config';
import { logger } from './logger.config';

export const __preventCircularImport = Asset;

export default {
  logger,
  alerts,
  authorizer,
  api,
  bigdatacorp,
  billing,
  bs2,
  cards,
  ccs,
  creditasprovider,
  celcoinProvider,
  database,
  googlecloud,
  oauth,
  payment,
  queue,
  seed,
  server,
  slack,
  smtp,
  stellar,
  timescale,
  user,
  redis,
  kyc,
  issues,
  provider,
  jira,
  unleash,
  email,
  identity,
  postbackDelivery,
  sms,
  otp,
  assetsInCustody,
  paratiProvider,
  consumerLegal,
  stellarDb,
  hooksproxy,
  asset,
  pubSub,
  pubSubConfig,
  dict,
  metrics,
  spb
};
