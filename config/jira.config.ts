export default {
  baseUrl: process.env.JIRA_BASE_URL,
  issueAPIEndpoint: process.env.JIRA_ISSUE_API_ENDPOINT || '/issue',
  auth: {
    username: process.env.JIRA_USERNAME,
    apiToken: process.env.JIRA_AUTH_TOKEN,
  },
  fields: {
    summary: undefined,
    project: {
      id: '10001',
    },
    issuetype: {
      id: '10002',
    },
    description: {
      type: 'doc',
      version: 1,
      content: [
        {
          type: 'paragraph',
          content: [
            {
              text: undefined,
              type: 'text',
            },
          ],
        },
      ],
    },
    labels: [],
  },
};
