import { TextGateway } from 'ts-framework-notification';

const brasilCode = '+55';

export default {
  countryCode: process.env.SMS_COUNTRY_CODE || brasilCode,
  gateway: {
    from: process.env.SMS_FROM,
    gateway: TextGateway.TWILIO,
    gatewayOptions: {
      accountSid: process.env.SMS_ACCOUNT_SID,
      authToken: process.env.SMS_AUTH_TOKEN,
    },
  },
};
