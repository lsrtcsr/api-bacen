export default {
  login: process.env.BIGDATACORP_LOGIN,
  password: process.env.BIGDATACORP_PASSWORD,
  accessToken: process.env.BIGDATACORP_ACCESS_TOKEN,
};
