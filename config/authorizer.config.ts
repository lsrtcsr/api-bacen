const allowedCurrencyCodes = process.env.AUTHORIZER_ALLOWED_CURRENCY_CODES?.split(',') || ['986'];

export default {
  allowedCurrencyCodes,
};
