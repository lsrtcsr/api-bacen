import * as Package from 'pjson';
import { logger, sentry } from './logger.config';
import SMSConfig from './sms.config';
import SMTPConfig from './smtp.config';

export default {
  logger,
  sentry,
  cors: true,
  userAgent: true,
  smtp: SMTPConfig,
  sms: SMSConfig,
  versioning: {
    verbose: true,
    current: Package.version,
    minimum: Package.version,
    recommended: Package.version,
  },
  security: {
    helmet: {},
    userAgent: true,
    cors: {
      exposedHeaders: [
        'x-data-length',
        'x-data-skip',
        'x-data-limit',
        'x-oauth-bearer-expiration',
        'x-oauth-bearer-scope',
      ],
    },
  },
  request: {
    bodyLimit: '3mb',
    secret: 'PLEASE_CHANGE_ME',
    multer: {
      limits: {
        fileSize: 3 * 1000 * 1024, // 3mb in bytes
      },
    },
  },
  address: process.env.ADDRESS,
  port: (process.env.PORT as any) || 3000,
  instanceId: process.env.INSTANCE_ID,
  router: {
    group404: true,
    omitStack: process.env.NODE_ENV === 'production' || !!process.env.OMIT_STACK,
  },
};
