import * as uuid from 'uuid';
import { UserRole } from '@bacen/base-sdk';

export default {
  rootAdmin: {
    firstName: 'Bit .',
    lastName: 'Issuer',
    email: 'admin@bt .app',
    username: 'admin@bt .app',
    password: uuid.v4(),
    role: UserRole.ADMIN,
  },
};
