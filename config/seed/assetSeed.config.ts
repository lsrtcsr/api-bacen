import * as path from 'path';

export default {
  rootAssetConfigPath: process.env.ROOT_ASSET_CONFIG_PATH || path.join(__dirname, '../env/rootAssetConfig.json'),
};
