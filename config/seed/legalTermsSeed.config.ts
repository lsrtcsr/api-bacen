import { LegalTermType } from '@bacen/base-sdk';
import { LegalTerm } from '../../api/models';

export default {
  legalTerms: [
    {
      type: LegalTermType.TERMS_OF_USE,
      name: 'bacen Terms of Use',
      body: '...',
    },
    {
      type: LegalTermType.PRIVACY_POLICY,
      name: 'bacen Privacy Policy',
      body: '...',
    },
  ],
};
