import { CustodyProvider, ServiceType, Periodicity, DomainRole, BillingSettings } from '@bacen/base-sdk';

export interface PlanSettings {
  name: string;
  supplier: DomainRole;
  billingSettings: BillingSettings;
}

const defaultConsumerBillingPlanSettings = {
  name: 'Default Consumer Billing Plan',
  supplier: DomainRole.DEFAULT,
  billingSettings: {
    invoiceClosing: {
      periodicity: Periodicity.MONTHLY,
      lastDayOfMonth: true,
    },
    settlementTrigger: 'invoiceClosing',
  } as BillingSettings,
} as PlanSettings;

const defaultMediatorBillingPlanSettings = {
  name: 'Default Mediator Billing Plan',
  supplier: DomainRole.ROOT,
  billingSettings: {
    invoiceClosing: {
      periodicity: Periodicity.MONTHLY,
      lastDayOfMonth: true,
    },
    settlementTrigger: 'invoiceClosing',
  } as BillingSettings,
} as PlanSettings;

const platformServices = [
  {
    name: 'Assinatura',
    description: 'Assinatura',
    type: ServiceType.SUBSCRIPTION,
    billingTrigger: 'invoice_closing',
  },
  {
    name: 'Setup',
    description: 'Setup',
    type: ServiceType.SETUP,
  },
  {
    name: 'Taxa de manutenção por cartão ativo',
    description: 'Taxa de manutenção por cartão ativo',
    type: ServiceType.ACTIVE_CARD_MAINTENANCE,
    billingTrigger: 'invoice_closing',
  },
  {
    name: 'Taxa de manutenção por conta ativa',
    description: 'Taxa de manutenção por conta ativa',
    type: ServiceType.ACTIVE_ACCOUNT_MAINTENANCE,
    billingTrigger: 'invoice_closing',
  },
  {
    name: 'Emissão de cartão físico',
    description: 'Emissão de cartão físico',
    type: ServiceType.PHYSICAL_CARD_ISSUING,
    billingTrigger: 'transaction',
  },
  {
    name: 'Emissão de segunda via de cartão físico',
    description: 'Emissão de segunda via de cartão físico',
    type: ServiceType.CARD_REISSUE,
    billingTrigger: 'transaction',
  },
  {
    name: 'Emissão de cartão virtual',
    description: 'Emissão de cartão virtual',
    type: ServiceType.VIRTUAL_CARD_ISSUING,
    billingTrigger: 'transaction',
  },
  {
    name: 'Saque',
    description: 'Saque',
    type: ServiceType.WITHDRAWAL,
    billingTrigger: 'transaction',
  },
  {
    name: 'Transferência',
    description: 'Transferência',
    type: ServiceType.TRANSFER,
    billingTrigger: 'transaction',
  },
  {
    name: 'Recarga de telefone móvel',
    description: 'Recarga de telefone móvel',
    type: ServiceType.PHONE_CREDITS_PURCHASE,
    billingTrigger: 'transaction',
  },
  {
    name: 'Emissão de boleto',
    description: 'Emissão de boleto',
    type: ServiceType.BOLETO_EMISSION,
    billingTrigger: 'transaction',
  },
  {
    name: 'Pagamento de boleto',
    description: 'Pagamento de boleto',
    type: ServiceType.BOLETO_PAYMENT,
    billingTrigger: 'transaction',
  },
  {
    name: 'Compra no cartão',
    description: 'Compra no cartão',
    type: ServiceType.CARD_PURCHASE,
    provider: CustodyProvider.CDT_VISA,
    billingTrigger: 'transaction',
  },
  {
    name: 'Saque no cartão',
    description: 'Saque no cartão',
    type: ServiceType.CARD_WITHDRAWAL,
    provider: CustodyProvider.CDT_VISA,
    billingTrigger: 'transaction',
  },
  {
    name: 'Depósito via TED',
    description: 'Depósito via TED',
    type: ServiceType.DEPOSIT,
    provider: CustodyProvider.CDT_VISA,
    billingTrigger: 'transaction',
  },
  {
    name: 'Depósito via TED',
    description: 'Depósito via TED',
    type: ServiceType.DEPOSIT,
    provider: CustodyProvider.CREDITAS_PROVIDER,
    billingTrigger: 'transaction',
  },
  {
    name: 'Depósito via boleto',
    description: 'Depósito via boleto',
    type: ServiceType.BOLETO_PAYMENT_CASH_IN,
    provider: CustodyProvider.CDT_VISA,
    billingTrigger: 'transaction',
  },
  {
    name: 'Depósito via boleto',
    description: 'Depósito via boleto',
    type: ServiceType.BOLETO_PAYMENT_CASH_IN,
    provider: CustodyProvider.CREDITAS_PROVIDER,
    billingTrigger: 'transaction',
  },
  {
    name: 'Análise manual de documentos',
    description: 'Análise manual de documentos',
    type: ServiceType.COMPLIANCE_CHECK,
    billingTrigger: 'transaction',
  },
  {
    name: 'Processamento de imagem de documentos',
    description: 'Processamento de imagem de documentos',
    type: ServiceType.OCR,
    billingTrigger: 'transaction',
  },
  {
    name: 'Reconhecimento facial',
    description: 'Reconhecimento facial',
    type: ServiceType.FACEMATCH,
    billingTrigger: 'transaction',
  },
];

export default {
  platformServices,
  defaultPlansSettings: [defaultConsumerBillingPlanSettings, defaultMediatorBillingPlanSettings],
};
