import {
  IssueType,
  SeverityLevel,
  NotificationChannel,
  AlertSchema,
  DataType,
  IssueCategory,
} from '@bacen/base-sdk';

export default {
  defaultAlerts: [
    {
      settings: {
        limitType: 'upperLimit',
        threshold: SeverityLevel.HIGH,
        value: 1,
        subscriptions: [
          // {
          //   channel: NotificationChannel.SLACK,
          //   subscribers: [],
          //   extraData: [
          //     {
          //       type: DataType.WEBHOOK_URL,
          //       value: 'https://hooks.slack.com/services/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',
          //     },
          //   ],
          // },
        ],
      },
    },
    {
      issueCategories: [IssueCategory.KYC_FLOW],
      settings: {
        limitType: 'upperLimit',
        threshold: SeverityLevel.HIGH,
        value: 1,
        subscriptions: [
          // {
          //   channel: NotificationChannel.SLACK,
          //   subscribers: [],
          //   extraData: [
          //     {
          //       type: DataType.WEBHOOK_URL,
          //       value: 'https://hooks.slack.com/services/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',
          //     },
          //   ],
          // },
          // {
          //   channel: NotificationChannel.JIRA,
          //   subscribers: [
          //     {
          //       identifier: '10001',
          //     },
          //   ],
          //   extraData: [
          //     {
          //       type: 'projectId',
          //       value: '10001',
          //     },
          //     {
          //       type: 'issuetypeId',
          //       value: '10002',
          //     },
          //     {
          //       type: 'title',
          //       value: '[KYC] Análise Manual',
          //     },
          //     {
          //       type: 'description',
          //       value: 'Cadastro não pode ser aprovado de forma automática',
          //     },
          //   ],
          // },
        ],
      },
    },
    {
      settings: {
        limitType: 'upperLimit',
        threshold: SeverityLevel.CRITICAL,
        value: 1,
        subscriptions: [
          // {
          //   channel: NotificationChannel.SLACK,
          //   subscribers: [],
          //   extraData: [
          //     {
          //       type: DataType.WEBHOOK_URL,
          //       value: 'https://hooks.slack.com/services/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',
          //     },
          //   ],
          // },
          // {
          //   channel: NotificationChannel.SMS,
          //   subscribers: [
          //     {
          //       identifier: '19991041414',
          //     },
          //   ],
          // },
        ],
      },
    },
  ],
};
