import {
  AccountType,
  ConsumerStatus,
  UserRole,
  EstablishmentFormatEnum,
  IndividualPartnerProfile,
  AddressStatus,
  Gender,
} from '@bacen/base-sdk';
import * as uuid from 'uuid';

const rootMediatorSeed = {
  rootMediator: {
    firstName: 'BIT .',
    lastName: 'LTDA',
    email: 'mediator@bacen.com.br',
    username: 'mediator@bacen.com.br',
    role: UserRole.MEDIATOR,
    password: uuid.v4(),
    consumer: {
      taxId: '29079725000107',
      type: AccountType.CORPORATE,
      birthday: new Date('2017-11-16'),
      status: ConsumerStatus.PENDING_DOCUMENTS,
      phones: [
        {
          code: '11',
          number: '42002480',
        },
      ],
      addresses: [
        {
          street: 'Av Nove de Julho',
          number: '4939',
          neighborhood: 'Jardim Paulista',
          code: '01407100',
          city: 'São Paulo',
          state: 'SP',
          country: 'BR',
          status: AddressStatus.OWN,
        },
      ],
      companyData: {
        taxId: '29079725000107',
        tradeName: 'BIT .',
        legalName: 'BIT . LTDA',
        openingDate: new Date('2017-11-16'),
        stateRegistration: '164677191686',
        establishmentFormat: EstablishmentFormatEnum.LTDA,
        partnerChanged: false,
        legalNature: '2062',
        legalStatus: 'active',
        revenue: 100000,
        activities: [
          {
            description:
              'Atividades de intermediação e agenciamento de serviços e negócios em geral, exceto imobiliários (Dispensada *)',
            code: '74.90-1-04',
            isMain: true,
          },
        ],
        partners: [
          {
            name: 'RICARDO ROESSLE GUIMARAES FILHO',
            gender: Gender.MALE,
            birthDate: new Date('1995-08-02'),
            birthCountry: 'Brasil',
            taxId: '44522219806',
            motherName: 'ELISABETH PHAELANTE GUERRA GUIMARAES',
            isPep: false,
            profile: IndividualPartnerProfile.OWNER,
            type: AccountType.PERSONAL,
            email: 'ricardo@bacen.com.br',
            phones: [
              {
                code: '11',
                number: '42002480',
              },
            ],
            addresses: [
              {
                street: 'Rua Frederic Chopin',
                number: '239',
                neighborhood: 'Jardim Paulistano',
                code: '01454030',
                city: 'São Paulo',
                state: 'SP',
                country: 'BR',
                status: AddressStatus.OWN,
              },
            ],
          },
        ],
      },
    },
  },
};

export type RootMediatorSeed = typeof rootMediatorSeed['rootMediator'];
export default rootMediatorSeed;
