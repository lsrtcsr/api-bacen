import adminSeed from './adminSeed.config';
import assetSeed from './assetSeed.config';
import domainSeed from './domainSeed.config';
import mediatorSeed from './mediatorSeed.config';
import alertsSeed from './alertsSeed.config';
import defaultLegalTerms from './legalTermsSeed.config';
import billingSeed from './billingSeed.config';

export default {
  ...adminSeed,
  ...domainSeed,
  ...assetSeed,
  ...mediatorSeed,
  ...alertsSeed,
  ...defaultLegalTerms,
  ...billingSeed,
};
