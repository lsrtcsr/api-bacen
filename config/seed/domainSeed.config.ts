import { DomainRole, DomainSchema } from '@bacen/base-sdk';

let DEFAULT_APP_URL: string;

switch (process.env.NODE_ENV) {
  case 'production':
    DEFAULT_APP_URL = 'https://testnet.bt .app';
    break;
  case 'testnet':
    DEFAULT_APP_URL = 'https://testnet.bt .app';
    break;
  default:
    DEFAULT_APP_URL = 'https://nightly.bt .app';
    break;
}

const APP_URL = process.env.APP_URL || DEFAULT_APP_URL;

export default {
  rootDomain: {
    name: 'Bit . Network',
    role: DomainRole.ROOT,
    urls: [APP_URL, 'localhost'],
    test: process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'prod',
    settings: {
      ispb: '03311443',
    },
  } as DomainSchema,
  defaultDomain: {
    name: process.env.DEFAULT_DOMAIN_NAME || 'Default Domain',
    role: DomainRole.DEFAULT,
    urls: [APP_URL, 'localhost'],
    test: process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'prod',
  } as DomainSchema,
};
