export default {
  token: process.env.SLACK_TOKEN,
  defaultChannel: process.env.SLACK_WEBHOOK_URL,
  issuesChannel: process.env.ISSUES_WEBHOOK_URL,
};
