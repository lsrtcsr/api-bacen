import * as tunnel from 'tunnel';

let httpsAgent;

const {
  CDT_PROVIDER_PROXY_URL,
  CDT_PROVIDER_PROXY_PORT,
  CDT_PROVIDER_PROXY_USER,
  CDT_PROVIDER_PROXY_PASSWORD,
} = process.env;

if (CDT_PROVIDER_PROXY_URL) {
  httpsAgent = tunnel.httpsOverHttp({
    proxy: {
      host: CDT_PROVIDER_PROXY_URL,
      port: CDT_PROVIDER_PROXY_PORT,
      proxyAuth: CDT_PROVIDER_PROXY_USER ? `${CDT_PROVIDER_PROXY_USER}:${CDT_PROVIDER_PROXY_PASSWORD}` : undefined,
    },
  });
}

export default {
  httpsAgent,

  // Accept deprecated url envs
  baseURL: process.env.CDT_PROVIDER_URL || process.env.CARD_API_BASE_URL,
};
