import * as tunnel from 'tunnel';
import { REPORT_EVENTS_TOPIC } from '@bacen/identity-sdk';

let httpsAgent;

const { IDENTITY_PROXY_URL, IDENTITY_PROXY_PORT, IDENTITY_PROXY_USER, IDENTITY_PROXY_PASSWORD } = process.env;

if (IDENTITY_PROXY_URL) {
  httpsAgent = tunnel.httpsOverHttp({
    proxy: {
      host: IDENTITY_PROXY_URL,
      port: IDENTITY_PROXY_PORT,
      proxyAuth: IDENTITY_PROXY_USER ? `${IDENTITY_PROXY_USER}:${IDENTITY_PROXY_PASSWORD}` : undefined,
    },
  });
}

const gcloudProjectId = process.env.IDENTITY_GCLOUD_PROJECT_ID || process.env.GCLOUD_PROJECT_ID;
const gcloudCredentials =
  process.env.IDENTITY_GOOGLE_APPLICATION_CREDENTIALS || process.env.GOOGLE_APPLICATION_CREDENTIALS;

export default {
  httpsAgent,
  gcloudProjectId,
  gcloudCredentials,
  baseURL: process.env.IDENTITY_URL || 'http://localhost:3031',
  reportEventsTopic: process.env.IDENTITY_REPORT_EVENTS_TOPIC || REPORT_EVENTS_TOPIC,
};
