import { DocumentType, AccountType, EstablishmentFormatEnum } from '@bacen/base-sdk';
import { DatasetType } from '@bacen/bigdatacorp-service';
import { MimeType } from 'file-type';

const recheck = {
  days: Number(process.env.KYC_RECHECK_PERIODICITY) || 1825,
};

const dataProvider = {
  coolDownTime: Number(process.env.DATA_PROVIDER_COOL_DOWN_TIME) || 60,
};

// PDF is also available but is enabled by a feature flag so is not in this list for personal individuals
const PERSONAL_DOCUMENT_CONTENT_TYPE_WHITELIST: MimeType[] = ['image/jpeg', 'image/png'];

const CORPORATE_DOCUMENT_CONTENT_TYPE_WHITELIST: MimeType[] = ['image/jpeg', 'image/png', 'application/pdf'];

type side = 'front' | 'back' | 'selfie' | 'both_sides';
export interface RequiredSidesOfDocumentType {
  type: DocumentType;
  options: [
    {
      sides: side[];
    },
  ];
}

export enum DocumentClazz {
  ADDRESS_PROOF,
  PERSON_IDENTITY_PROOF,
  COMPANY_IDENTITY_PROOF,
}

const documentClassification = new Map<DocumentClazz, DocumentType[]>();
documentClassification.set(DocumentClazz.ADDRESS_PROOF, [DocumentType.BRL_ADDRESS_STATEMENT]);
documentClassification.set(DocumentClazz.PERSON_IDENTITY_PROOF, [
  DocumentType.BRL_DRIVERS_LICENSE,
  DocumentType.BRL_INDIVIDUAL_REG,
]);
documentClassification.set(DocumentClazz.COMPANY_IDENTITY_PROOF, [
  DocumentType.ARTICLES_OF_ASSOCIATION,
  DocumentType.CCMEI,
  DocumentType.EI_REGISTRATION_REQUIREMENT,
  DocumentType.EIRELI_INCORPORATION_STATEMENT,
  DocumentType.COMPANY_BYLAWS,
]);

const establishmentFormatToDocumentType = new Map<EstablishmentFormatEnum, DocumentType>();
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.EI, DocumentType.EI_REGISTRATION_REQUIREMENT);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.ME, DocumentType.EI_REGISTRATION_REQUIREMENT);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.EIRELI, DocumentType.EIRELI_INCORPORATION_STATEMENT);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.MEI, DocumentType.CCMEI);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.EMGP, DocumentType.ARTICLES_OF_ASSOCIATION);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.EPP, DocumentType.ARTICLES_OF_ASSOCIATION);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.LTDA, DocumentType.ARTICLES_OF_ASSOCIATION);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.SS, DocumentType.ARTICLES_OF_ASSOCIATION);
establishmentFormatToDocumentType.set(EstablishmentFormatEnum.SA, DocumentType.COMPANY_BYLAWS);

export interface RequiredDocument {
  clazz: DocumentClazz;
  who: AccountType[];
}

export default {
  recheck,
  dataProvider,
  documentClassification,
  establishmentFormatToDocumentType,
  defaultDatasets: [DatasetType.BASIC_DATA, DatasetType.KYC, DatasetType.PHONE, DatasetType.ADDRESS],
  requiredDocuments: [
    {
      clazz: DocumentClazz.PERSON_IDENTITY_PROOF,
      who: [AccountType.PERSONAL, AccountType.CORPORATE],
    },
    {
      clazz: DocumentClazz.COMPANY_IDENTITY_PROOF,
      who: [AccountType.CORPORATE],
    },
  ] as RequiredDocument[],
  sidesByDocumentType: [
    {
      type: DocumentType.BRL_ADDRESS_STATEMENT,
      options: [{ sides: ['front'] }],
    },
    {
      type: DocumentType.BRL_DRIVERS_LICENSE,
      options: [{ sides: ['front', 'back', 'selfie'] }, { sides: ['both_sides', 'selfie'] }],
    },
    {
      type: DocumentType.BRL_INDIVIDUAL_REG,
      options: [{ sides: ['front', 'back', 'selfie'] }, { sides: ['both_sides', 'selfie'] }],
    },
    {
      type: DocumentType.ARTICLES_OF_ASSOCIATION,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
    {
      type: DocumentType.CCMEI,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
    {
      type: DocumentType.EI_REGISTRATION_REQUIREMENT,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
    {
      type: DocumentType.EIRELI_INCORPORATION_STATEMENT,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
    {
      type: DocumentType.COMPANY_BYLAWS,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
    {
      type: DocumentType.OTHER,
      options: [{ sides: ['front'] }, { sides: ['both_sides'] }],
    },
  ] as RequiredSidesOfDocumentType[],
};

export { CORPORATE_DOCUMENT_CONTENT_TYPE_WHITELIST, PERSONAL_DOCUMENT_CONTENT_TYPE_WHITELIST };
