export const api = {
  defaultPaginationLimit: 25,
  maxCustomFieldsSize: process.env.MAX_CUSTOM_FIELDS_SIZE || 256,
};
