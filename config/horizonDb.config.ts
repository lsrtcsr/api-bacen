export default {
  host: process.env.STELLAR_ _DB_HOST,
  port: parseInt(process.env.STELLAR_ _DB_PORT) || 5432,
  user: process.env.STELLAR_ _DB_USER,
  password: process.env.STELLAR_ _DB_PASSWORD,
  database: process.env.STELLAR_ _DB_NAME,
  poolSize: parseInt(process.env.STELLAR_ _DB_POOL_SIZE) || 10,

  /** if set to true, throws an error if stellar-  DB is not read-only */
  ensureSlave: process.env.STELLAR_ _DB_ENSURE_SLAVE ? process.env.STELLAR_ _DB_ENSURE_SLAVE !== 'false' : true,
};
