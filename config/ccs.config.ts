export default {
  baseURL: process.env.CCS_BASE_URL || 'http://localhost:3050',
  projectId: process.env.CCS_GCP_PROJECT_ID,
  userNotificationTopic: process.env.CCS_USER_NOTIFICATION_TOPIC || 'dev-ccs-user-notification',
};
