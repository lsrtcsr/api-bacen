import { OAuthClientPlatform, OAuthClientSchema, OAuthClientStatus, CustodyProvider } from '@bacen/base-sdk';
import adminScopes from './oauth/adminScopes';
import auditScopes from './oauth/auditScopes';
import consumerScopes from './oauth/consumerScopes';
import mediatorScopes from './oauth/mediatorScopes';
import operatorScopes from './oauth/operatorScopes';
import publicScopes from './oauth/publicScopes';
import authorizationScopes from './oauth/authorizationScopes';
import Scopes from './oauth/scopes';

export default {
  Scopes,

  /* Token timeout in ms */
  msTokenCacheTimeout: +(process.env.OAUTH_CACHE_MS || 60 * 1000),

  /* Available grant types */
  grantTypes: ['password', 'refresh_token', 'secret_token', 'client_credentials'],

  /* Startup oauth clients */
  clients: [
    /* - - - - - ROOT CLIENTS - - - - - */
    {
      clientId: 'root',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'hooks-api',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'cdt-cards',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: CustodyProvider.CREDITAS_PROVIDER,
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: CustodyProvider.LECCA_PROVIDER,
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: CustodyProvider.PARATI_PROVIDER,
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'reports',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'bs2-provider-api',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: CustodyProvider.CELCOIN,
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'ccs-api',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },
    {
      clientId: 'dict-api',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.ROOT,
    },

    /* - - - - - API CLIENTS - - - - - */
    {
      clientId: ' -sh',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.API,
    },

    /* - - - - - WEB CLIENTS - - - - - */
    {
      clientId: 'hopper',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.WEB,
    },
    {
      clientId: 'hamilton',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.WEB,
    },

    /* - - - - - AUTHORIZE CLIENTS - - - - - */
    {
      clientId: 'external-authorizer',
      status: OAuthClientStatus.ACTIVE,
      platform: OAuthClientPlatform.EXTERNAL_AUTHORIZER,
    },
  ] as OAuthClientSchema[],

  /* Scope definitions */
  allowEmptyScopes: true,

  scopes: {
    admin: adminScopes,
    audit: auditScopes,
    mediator: mediatorScopes,
    operator: operatorScopes,
    consumer: consumerScopes,
    public: publicScopes,
    authorization: authorizationScopes,
  },
};
