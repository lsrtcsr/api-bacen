import redisConfig from './redis.config';

export const database = {
  type: 'postgres',
  host: process.env.DATABASE_HOST || 'localhost',
  port: parseInt(process.env.DATABASE_PORT || '5432', 10),
  username: process.env.DATABASE_USER || 'bacen',
  password: process.env.DATABASE_PASSWORD || 'bacen',
  database: process.env.DATABASE_NAME || ' _api',
  cacheTimeout: 60 * 1000,
  shortCacheTimeout: 30 * 1000,
  longCacheTimeout: 150 * 1000,
  cache: {
    type: 'redis',
    all: false,
    options: {
      ...redisConfig,
    },
  },
  synchronize: false,
  logging: ['schema', 'error'],
  maxQueryExecutionTime: 1000,

  entities: ['./api/models/**/*.ts'],
  migrations: ['./api/migrations/**/*.ts'],
  cli: {
    entitiesDir: './api/models',
    migrationsDir: './api/migrations',
  },
  extra: {
    poolSize: Number(process.env.DATABASE_POOL_SIZE),
  },
};
