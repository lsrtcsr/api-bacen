export default {
  resetPassword: {
    subject: 'Recover your credentials',
    url: process.env.CLIENT_URL || 'https://testnet.bt .app/password/:token',
    expiration: 24 * 60 * 60 * 1000, // 24 hours
  },

  confirmDictEmail: {
    subject: 'Confirm your email',
    url: process.env.CLIENT_URL_DICT || 'http://localhost:3000/postbacks/dict/:token/confirmation',
    expiration: 24 * 60 * 60 * 1000, // 24 hours
  },

  // tslint:disable-next-line:max-line-length
  emailRegEx: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
  // user creation password salt rounds (1 to 25 but never use more than 11 or will take too long)
  saltRounds: Number(process.env.PASSWORD_SALT_ROUNDS || 5),

  /** Time between user being marked as pending_deletion and actual deletion in days */
  deletionDeadline: Number(process.env.USER_DELETION_DEADLINE || 30),
};
