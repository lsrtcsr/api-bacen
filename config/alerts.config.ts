export default {
  issuesAPIUrl: 'https://testnet.bt .app/issues',
  notification: {
    slack: {
      text: 'System misbehavior alert',
      title: 'The list of issues detected can be accessed clicking in the link below',
    },
    email: {
      subject: '[Issue Alert] System misbehavior alert',
      body: `
      Errors were detected whose characteristics correspond to configured alerts. <br/><br/>
      The list of issues detected can be accessed clicking in the button below.`,
      button: {
        label: 'Download the issues list',
      },
    },
  },
};
