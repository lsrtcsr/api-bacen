import { Config } from '@google-cloud/trace-agent';
import * as path from 'path';
import { name, version } from '../package.json';

const isProductionEnvironment =
  process.env.NODE_ENV && ['test', 'development', 'dev'].indexOf(process.env.NODE_ENV) === -1;

const serviceContext = {
  version,
  service:
    process.env.GCLOUD_TRACE_CUSTOM_SERVICE_NAME ||
    (isProductionEnvironment ? name : `${name}_${process.env.NODE_ENV || 'development'}`).replace('@bacen/', ''),
};

try {
  if (process.env.ENABLE_GCLOUD_TRACING || isProductionEnvironment) {
    require('@google-cloud/trace-agent').start({
      serviceContext,
      enhancedDatabaseReporting: !isProductionEnvironment,
      samplingRate: isProductionEnvironment ? undefined : 0,
      ignoreUrls: [/^\/$/, /^\/status/, /^\/metrics/], // ignore the ["/", "/status/**/*" "/metrics/**/*"] endpoints.
      plugins: {
        typeorm: path.resolve(__dirname, '../api/utils/tracing/plugins/plugin-typeorm.js'),
        amqplib: path.resolve(__dirname, '../api/utils/tracing/plugins/plugin-amqplib.js'),
      },
    } as Config);
  }

  if (isProductionEnvironment) {
    require('@google-cloud/profiler').start({ serviceContext });
    require('@google-cloud/debug-agent').start({ allowExpressions: true, serviceContext });
  }
} catch (exception) {
  console.warn('Could not load google cloud agents, skipping for now', exception);
}
