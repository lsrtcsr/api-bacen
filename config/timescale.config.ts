export const timescale = {
  type: 'postgres',
  host: process.env.TIMESCALE_HOST || 'localhost',
  port: parseInt(process.env.TIMESCALE_PORT || '5433', 10),
  username: process.env.TIMESCALE_USER || 'bacen',
  password: process.env.TIMESCALE_PASSWORD || 'bacen',
  database: process.env.TIMESCALE_NAME || 'timescale',
  synchronize: false,
  logging: ['schema', 'error'],

  // Custom queries for Timescale
  // customQueriesDir: "./api/sql",

  entities: ['./api/timescale/**/*.ts'],
  migrations: ['./api/timescale_migrations/**/*.ts'],
  cli: {
    entitiesDir: './api/timescale',
    migrationsDir: './api/timescale_migrations',
  },
};
