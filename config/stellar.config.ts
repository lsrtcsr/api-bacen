// TODO: Move this checks to a better place
if (!process.env.STELLAR_ROOT_SECRET && process.env.NODE_ENV !== 'test') {
  // TODO: Better validation of secrets with stellar sdk
  throw new Error('Stellar root wallet secret not set in environment');
}

if (
  (!process.env.STELLAR_CHANNEL_SECRETS || !process.env.STELLAR_CHANNEL_SECRETS.split) &&
  process.env.NODE_ENV !== 'test'
) {
  // TODO: Better validation of secrets with stellar sdk
  throw new Error('Stellar channel wallets secrets not set in environment');
} else if (process.env.STELLAR_CHANNEL_SECRETS?.includes(process.env.STELLAR_ROOT_SECRET)) {
  // TODO: Better validation of secrets with stellar sdk
  throw new Error('Stellar root wallet secret cannot be used as a channel account');
}

export default {
  defaultFee: Number(process.env.STELLAR_DEFAULT_FEE) || 100,
  defaultTimeout: Number(process.env.STELLAR_DEFAULT_TIMEOUT) || 5 * 60, // 5 min
  // TODO: Get this from env
  testNetwork: process.env.NODE_ENV !== 'production',
  apiServer: process.env.STELLAR_URL || 'https://horizon-testnet.stellar.org',
  networkPassphrase: process.env.STELLAR_PASSPHRASE || 'Test SDF Network ; September 2015',
  rootWallet: {
    secretKey: process.env.STELLAR_ROOT_SECRET,
  },
  // TODO: This should be handled dynamically
  initialBalance: process.env.STELLAR_DEFAULT_BALANCE || '5',
  channelAccounts: process.env.STELLAR_CHANNEL_SECRETS
    ? process.env.STELLAR_CHANNEL_SECRETS.split(',').map((secretSeed: string) => ({ secretSeed }))
    : [],
};
