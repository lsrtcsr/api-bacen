import * as tunnel from 'tunnel';

let httpsAgent;

const {
  PARATI_PROVIDER_PROXY_URL,
  PARATI_PROVIDER_PROXY_PORT,
  PARATI_PROVIDER_PROXY_USER,
  PARATI_PROVIDER_PROXY_PASSWORD,
} = process.env;

if (PARATI_PROVIDER_PROXY_URL) {
  httpsAgent = tunnel.httpsOverHttp({
    proxy: {
      host: PARATI_PROVIDER_PROXY_URL,
      port: PARATI_PROVIDER_PROXY_PORT,
      proxyAuth: PARATI_PROVIDER_PROXY_USER
        ? `${PARATI_PROVIDER_PROXY_USER}:${PARATI_PROVIDER_PROXY_PASSWORD}`
        : undefined,
    },
  });
}

export default {
  httpsAgent,
  baseURL: process.env.PARATI_PROVIDER_API_BASE_URL || process.env.STR_PROVIDER_URL,
};
