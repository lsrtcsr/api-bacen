import * as path from 'path';
import { CustodyProvider } from '@bacen/base-sdk';

export default {
  envPath: process.env.PROVIDER_ENV_PATH || path.join(__dirname, '../.env'),
  defaultCustodyProvider: (process.env.DEFAULT_CUSTODY_PROVIDER as CustodyProvider) || CustodyProvider.PARATI_PROVIDER,
  defaultMobileRechargeProvider:
    (process.env.DEFAULT_MOBILE_RECHARGE_PROVIDER as CustodyProvider) || CustodyProvider.CDT_VISA,
  defaultBoletoPaymentProvider:
    (process.env.DEFAULT_BOLETO_PAYMENT_PROVIDER as CustodyProvider) || CustodyProvider.CELCOIN,
  defaultBoletoIssuingProvider: (process.env.DEFAULT_BOLETO_ISSUING_PROVIDER as CustodyProvider) || CustodyProvider.BS2,
};
