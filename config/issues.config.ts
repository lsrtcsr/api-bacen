const defaultTimeInterval = 2 * 24 * 60 * 60 * 1000; // 172800000 === 48 hours;

const groupingWindows = {
  default: defaultTimeInterval,
  postback_delivery: defaultTimeInterval,
  provider_postback_processing: defaultTimeInterval,
  p2p_payment: defaultTimeInterval,
  wallet_creation_flow: defaultTimeInterval,
  withdrawal: defaultTimeInterval,
  boleto_payment: defaultTimeInterval,
  kyc_flow: defaultTimeInterval,
  stellar_transaction: defaultTimeInterval,
  other: defaultTimeInterval,
  str_provider: defaultTimeInterval,
  str_proxy: defaultTimeInterval,
  str_message: defaultTimeInterval,
  celsius_provider: defaultTimeInterval,
  celsius_api: defaultTimeInterval,
  cdt_provider: defaultTimeInterval,
  billing: defaultTimeInterval,
  invoice: defaultTimeInterval,
  invoice_period: defaultTimeInterval,
  billing_plan: defaultTimeInterval,
  billing_plan_subscription: defaultTimeInterval,
  invoice_state_transition: defaultTimeInterval,
  period_state_transition: defaultTimeInterval,
};

export default {
  groupingWindows,
  defaultTimeInterval,
};
