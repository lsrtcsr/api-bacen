import Scopes from '../../config/oauth/scopes';

export default [
  /* Domain scopes */
  Scopes.domains.READ,
];
