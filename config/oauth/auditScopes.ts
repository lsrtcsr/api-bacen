import Scopes from './scopes';

export default [
  /* Assets scopes */
  Scopes.assets.READ,

  /* Billing scopes */
  Scopes.services.READ,
  Scopes.invoices.READ,
  Scopes.plans.READ,
  Scopes.contracts.READ,

  /* Branches scopes */
  Scopes.branches.READ,

  /* Domain scopes */
  Scopes.domains.READ,
  Scopes.domains.METRICS,

  /* OAuthClient scopes */
  Scopes.oAuthClients.READ,

  /* Payment scopes */
  Scopes.payments.READ,

  /* Transaction scopes */
  Scopes.transactions.READ,

  /* User scopes */
  Scopes.users.READ,
  Scopes.users.READ_KYC,
  Scopes.users.READ_BANKINGS,
  Scopes.users.READ_DOCUMENTS,
  Scopes.users.READ_SECRET_TOKENS,
  Scopes.users.READ_LEGAL_TERMS,

  /* Wallet scopes */
  Scopes.wallets.READ,

  /* Issue scopes */
  Scopes.issues.READ,

  /* Alert scopes */
  Scopes.alerts.READ,

  /* Phone Credits Scopes */
  Scopes.phoneCredits.READ_PROVIDERS,

  /* Queue Scopes */
  Scopes.queues.READ,

  /* Provider Scopes */
  Scopes.providers.READ,
];
