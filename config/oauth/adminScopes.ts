import Scopes from './scopes';

export default [
  /* Assets scopes */
  Scopes.assets.READ,
  Scopes.assets.WRITE,
  Scopes.assets.EMIT,
  Scopes.assets.DESTROY,
  Scopes.assets.DELETE,

  /* Billing scopes */
  Scopes.services.READ,
  Scopes.services.WRITE,
  Scopes.services.DELETE,

  Scopes.invoices.READ,
  Scopes.invoices.WRITE,

  Scopes.plans.READ,
  Scopes.plans.WRITE,
  Scopes.plans.DELETE,

  Scopes.contracts.READ,
  Scopes.contracts.WRITE,
  Scopes.contracts.DELETE,

  /* Branches scopes */
  Scopes.branches.READ,
  Scopes.branches.WRITE,
  Scopes.branches.DELETE,

  /* Domain scopes */
  Scopes.domains.READ,
  Scopes.domains.WRITE,
  Scopes.domains.METRICS,
  Scopes.domains.DELETE,

  /* OAuthClient scopes */
  Scopes.oAuthClients.READ,
  Scopes.oAuthClients.WRITE,
  Scopes.oAuthClients.DELETE,

  /* Payment scopes */
  Scopes.payments.READ,
  Scopes.payments.WRITE,

  /* Transaction scopes */
  Scopes.transactions.READ,
  Scopes.transactions.WRITE,
  Scopes.transactions.CANCEL,
  Scopes.transactions.CONFIRM,
  Scopes.transactions.MODIFY,

  /* User scopes */
  Scopes.users.READ,
  Scopes.users.READ_KYC,
  Scopes.users.READ_BANKINGS,
  Scopes.users.READ_DOCUMENTS,
  Scopes.users.READ_SECRET_TOKENS,
  Scopes.users.READ_LEGAL_TERMS,
  Scopes.users.WRITE,
  Scopes.users.WRITE_BANKINGS,
  Scopes.users.WRITE_DOCUMENTS,
  Scopes.users.EVALUATE_DOCUMENTS,
  Scopes.users.WRITE_SECRET_TOKENS,
  Scopes.users.DELETE,
  Scopes.users.BLOCK,

  /* Wallet scopes */
  Scopes.wallets.READ,
  Scopes.wallets.WRITE,
  Scopes.wallets.DELETE,

  /* Issue scopes */
  Scopes.issues.READ,
  Scopes.issues.WRITE,

  /* Alert scopes */
  Scopes.alerts.READ,
  Scopes.alerts.WRITE,
  Scopes.alerts.DELETE,

  /* Phone Credits Scopes */
  Scopes.phoneCredits.READ_PROVIDERS,
  Scopes.phoneCredits.CREATE_ORDER,
  Scopes.phoneCredits.COMPLETE_ORDER,

  /* Queue Scopes */
  Scopes.queues.READ,
  Scopes.queues.WRITE,

  /* Provider Scopes */
  Scopes.providers.READ,

  /* Legal scopes */
  Scopes.legals.WRITE,
  Scopes.legals.ACCEPT,

  /* External Authorizer */
  Scopes.authorization.READ,
  Scopes.authorization.WRITE,
];
