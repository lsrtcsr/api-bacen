import Scopes from './scopes';

export default [
  /* Assets scopes */
  Scopes.assets.READ,

  /* Domain scopes */
  Scopes.domains.READ,
  Scopes.domains.METRICS,

  /* OAuthClient scopes */
  Scopes.oAuthClients.READ,

  /* Payment scopes */
  Scopes.payments.READ,

  /* Transaction scopes */
  Scopes.transactions.READ,

  /* User scopes */
  Scopes.users.READ,
  Scopes.users.READ_KYC,
  Scopes.users.READ_BANKINGS,
  Scopes.users.READ_DOCUMENTS,
  Scopes.users.WRITE,
  Scopes.users.WRITE_BANKINGS,
  Scopes.users.WRITE_DOCUMENTS,
  Scopes.users.EVALUATE_DOCUMENTS,
  Scopes.users.READ_DOCUMENTS,
  Scopes.users.DELETE,
  Scopes.users.BLOCK,

  /* Queue Scopes */
  Scopes.queues.WRITE,

  /* Wallet scopes */
  Scopes.wallets.READ,
  Scopes.wallets.WRITE,

  /* Transaction scopes */
  Scopes.transactions.READ,

  /* Transaction scopes */
  Scopes.transactions.CANCEL,
  Scopes.transactions.CONFIRM,

  /* Issue scopes */
  Scopes.issues.READ,

  /* Alert scopes */
  Scopes.alerts.READ,

  /* Phone Credits Scopes */
  Scopes.phoneCredits.READ_PROVIDERS,

  Scopes.wallets.WRITE,
  Scopes.payments.READ,

  /* Queue Scopes */
  Scopes.queues.WRITE,

  /* Billing scopes */
  Scopes.services.READ,
  Scopes.invoices.READ,
  Scopes.plans.READ,
  Scopes.contracts.READ,

  /* Legal scopes */
  Scopes.legals.WRITE,
  Scopes.legals.ACCEPT,

  /* Provider Scopes */
  Scopes.providers.READ,
];
