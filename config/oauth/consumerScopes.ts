import Scopes from './scopes';

export default [
  /* Assets scopes */
  Scopes.assets.READ,

  /* Billing scopes */
  Scopes.services.READ,
  Scopes.invoices.READ,
  Scopes.plans.READ,
  Scopes.contracts.READ,
  Scopes.contracts.WRITE,

  /* Domain scopes */
  Scopes.domains.READ,

  /* Users scopes */
  Scopes.users.READ,
  Scopes.users.READ_BANKINGS,
  Scopes.users.READ_DOCUMENTS,
  Scopes.users.READ_SECRET_TOKENS,
  Scopes.users.WRITE,
  Scopes.users.WRITE_BANKINGS,
  Scopes.users.WRITE_DOCUMENTS,
  Scopes.users.WRITE_SECRET_TOKENS,
  Scopes.users.DELETE,

  /* Payment scopes */
  Scopes.payments.READ,
  Scopes.payments.WRITE,

  /* Transaction scopes */
  Scopes.transactions.READ,

  /* Wallet scopes */
  Scopes.wallets.READ,
  Scopes.wallets.WRITE,
  Scopes.wallets.DELETE,

  /* Phone Credits Scopes */
  Scopes.phoneCredits.READ_PROVIDERS,
  Scopes.phoneCredits.CREATE_ORDER,
  Scopes.phoneCredits.COMPLETE_ORDER,

  /* Legal scopes */
  Scopes.legals.ACCEPT,
];
