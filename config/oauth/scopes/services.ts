export enum ServicesScopes {
  /* Read scopes */
  READ = 'services:read',
  /* Write scopes */
  WRITE = 'services:write',
  /* Delete scopes */
  DELETE = 'services:delete',
}
