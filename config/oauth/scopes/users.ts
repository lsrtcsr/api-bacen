export enum UsersScopes {
  /* Read scopes */
  READ = 'users:read',
  READ_KYC = 'users:kyc:read',
  READ_BANKINGS = 'users:bankings:read',
  READ_DOCUMENTS = 'users:documents:read',
  READ_SECRET_TOKENS = 'users:secret_tokens:read',
  READ_LEGAL_TERMS = 'users:legals:read',
  /* Write scopes */
  WRITE = 'users:write',
  WRITE_BANKINGS = 'users:bankings:write',
  WRITE_DOCUMENTS = 'users:documents:write',
  EVALUATE_DOCUMENTS = 'users:documents:evaluate',
  WRITE_SECRET_TOKENS = 'users:secret_tokens:write',
  /* Delete scopes */
  DELETE = 'users:delete',
  /* Block scopes */
  BLOCK = 'users:block',
}
