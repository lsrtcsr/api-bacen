export enum AlertsScopes {
  /* Read scopes */
  READ = 'alerts:read',
  /* Write scopes */
  WRITE = 'alerts:write',
  /* Delete scopes */
  DELETE = 'alerts:delete',
}
