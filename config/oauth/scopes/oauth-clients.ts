export enum OAuthClientScopes {
  /* Read scopes */
  READ = 'oauth-clients:read',
  /* Write scopes */
  WRITE = 'oauth-clients:write',
  /* Delete scopes */
  DELETE = 'oauth-clients:delete',
}
