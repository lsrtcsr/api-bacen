export enum ProviderScopes {
  /* Read scopes */
  READ = 'provider:read',
}
