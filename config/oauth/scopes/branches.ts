export enum BranchesScopes {
  /* Read scopes */
  READ = 'branches:read',
  /* Write scopes */
  WRITE = 'branches:write',
  /* Delete scopes */
  DELETE = 'branches:delete',
}
