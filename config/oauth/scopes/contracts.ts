export enum ContractsScopes {
  /* Read scopes */
  READ = 'contracts:read',
  /* Write scopes */
  WRITE = 'contracts:write',
  /* Delete scopes */
  DELETE = 'contracts:delete',
}
