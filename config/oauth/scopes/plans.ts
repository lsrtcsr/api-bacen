export enum PlansScopes {
  /* Read scopes */
  READ = 'plans:read',
  /* Write scopes */
  WRITE = 'plans:write',
  /* Delete scopes */
  DELETE = 'plans:delete',
}
