export enum QueueScopes {
  /* Read scopes */
  READ = 'queues:read',
  /* Write scopes */
  WRITE = 'queues:write',
}
