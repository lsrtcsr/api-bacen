export enum InvoicesScopes {
  /* Read scopes */
  READ = 'invoices:read',
  /* Write scopes */
  WRITE = 'invoices:write',
}
