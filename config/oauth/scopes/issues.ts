export enum IssuesScopes {
  /* Read scopes */
  READ = 'issues:read',
  /* Write scopes */
  WRITE = 'issues:write',
}
