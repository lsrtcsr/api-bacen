export enum PhoneCreditScopes {
  /* Read scopes */
  READ_PROVIDERS = 'phone_credit:providers:read',

  /* Write scopes */
  CREATE_ORDER = 'phone_credit:order:create',
  COMPLETE_ORDER = 'phone_credit:order:complete',
}
