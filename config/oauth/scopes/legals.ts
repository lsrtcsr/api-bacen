export enum LegalScopes {
  /* Write scopes */
  WRITE = 'legals:write',

  /* Domain-specific scopes */
  ACCEPT = 'legals:accept',
}
