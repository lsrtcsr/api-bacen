export enum TransactionsScopes {
  /* Read scopes */
  READ = 'transactions:read',
  /* Write scopes */
  WRITE = 'transactions:write',
  CONFIRM = 'transactions:confirm',
  CANCEL = 'transactions:cancel',
  MODIFY = 'transactions:modify',
}
