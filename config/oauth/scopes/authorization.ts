export enum AuthorizationsScopes {
  /* Read scopes */
  READ = 'authorizations:read',
  /* Write scopes */
  WRITE = 'authorizations:write',
}
