import { AlertsScopes } from './alerts';
import { AssetsScopes } from './assets';
import { AuthorizationsScopes } from './authorization';
import { BranchesScopes } from './branches';
import { DomainsScopes } from './domains';
import { IssuesScopes } from './issues';
import { OAuthClientScopes } from './oauth-clients';
import { PaymentsScopes } from './payments';
import { PhoneCreditScopes } from './phone_credits';
import { ProviderScopes } from './providers';
import { QueueScopes } from './queue';
import { TransactionsScopes } from './transactions';
import { UsersScopes } from './users';
import { WalletsScopes } from './wallets';
import { InvoicesScopes } from './invoices';
import { PlansScopes } from './plans';
import { ContractsScopes } from './contracts';
import { ServicesScopes } from './services';
import { LegalScopes } from './legals';

export type ScopeType =
  | AlertsScopes
  | AssetsScopes
  | AuthorizationsScopes
  | BranchesScopes
  | ContractsScopes
  | DomainsScopes
  | InvoicesScopes
  | IssuesScopes
  | LegalScopes
  | OAuthClientScopes
  | PaymentsScopes
  | PhoneCreditScopes
  | PlansScopes
  | ProviderScopes
  | QueueScopes
  | ServicesScopes
  | TransactionsScopes
  | UsersScopes
  | UsersScopes
  | WalletsScopes;

const Scopes = {
  alerts: AlertsScopes,
  assets: AssetsScopes,
  authorization: AuthorizationsScopes,
  branches: BranchesScopes,
  domains: DomainsScopes,
  issues: IssuesScopes,
  oAuthClients: OAuthClientScopes,
  payments: PaymentsScopes,
  phoneCredits: PhoneCreditScopes,
  providers: ProviderScopes,
  queues: QueueScopes,
  transactions: TransactionsScopes,
  contracts: ContractsScopes,
  plans: PlansScopes,
  invoices: InvoicesScopes,
  services: ServicesScopes,
  users: UsersScopes,
  wallets: WalletsScopes,
  legals: LegalScopes,
};

export default Scopes;
