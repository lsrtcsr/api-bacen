import Scopes from './scopes';

export default [
  /* Authorization scopes */
  Scopes.authorization.READ,
  Scopes.authorization.WRITE,
];
