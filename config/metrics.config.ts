export default {
  prefix: 'bacen_ _',
  gateway: process.env.PROMETHEUS_GATEWAY_URL,
};
