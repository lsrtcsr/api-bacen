const connection = {
  host: process.env.QUEUE_URL || 'amqp://localhost',

  // time for failed messages before requeueing
  nackTimeout: process.env.QUEUE_NACK_TIMEOUT ? Number(process.env.QUEUE_NACK_TIMEOUT) : 20 * 1000,

  // 1 hour in queue before message is considered to be dead
  messageTimeToLive: process.env.QUEUE_MESSAGE_TTL ? Number(process.env.QUEUE_MESSAGE_TTL) : 60 * 60 * 1000,
};

const defaultMaxRetries = process.env.MAX_RETRIES ? Number(process.env.MAX_RETRIES) : 1;

export default {
  consumer: {
    maxRetries: process.env.CONSUMER_MAX_RETRIES ? Number(process.env.CONSUMER_MAX_RETRIES) : defaultMaxRetries,
  },
  mediator: {
    maxRetries: process.env.MEDIATOR_MAX_RETRIES ? Number(process.env.MEDIATOR_MAX_RETRIES) : defaultMaxRetries,
  },
  transaction: {
    maxRetries: process.env.TRANSACTION_MAX_RETRIES ? Number(process.env.TRANSACTION_MAX_RETRIES) : defaultMaxRetries,
  },
  postback: {
    maxRetries: process.env.POSTBACK_MAX_RETRIES ? Number(process.env.POSTBACK_MAX_RETRIES) : defaultMaxRetries,
  },
  invoice: {
    maxRetries: process.env.INVOICE_MAX_RETRIES ? Number(process.env.INVOICE_MAX_RETRIES) : defaultMaxRetries,
  },
  event: {
    maxRetries: process.env.EVENT_MAX_RETRIES ? Number(process.env.EVENT_MAX_RETRIES) : defaultMaxRetries,
  },
  period: {
    maxRetries: process.env.PERIOD_MAX_RETRIES ? Number(process.env.PERIOD_MAX_RETRIES) : defaultMaxRetries,
  },
  amqp: {
    ...connection,
  },
};
