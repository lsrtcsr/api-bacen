import { Logger } from 'nano-errors';
import { getDefaultTransports } from '@bacen/shared-sdk';
import { version } from '../package.json';

const sentry = process.env.SENTRY_DSN ? { dsn: process.env.SENTRY_DSN, release: version } : undefined;

const logger = Logger.initialize({
  transports: getDefaultTransports(version, process.env.SENTRY_DSN),
  level: process.env.LOG_LEVEL || 'info',
});

export { logger, sentry };
