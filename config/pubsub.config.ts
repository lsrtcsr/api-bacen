import { PubSub } from '@google-cloud/pubsub';

let pubSub: PubSub;

pubSub = new PubSub({ projectId: process.env.GCLOUD_PROJECT_ID });

const pubSubConfig = {
  projectId: process.env.GCLOUD_PROJECT_ID,
  base2Subscription: process.env.PUB_SUB_BASE2_SUBSCRIPTION,
  base2ResponseTopic: process.env.PUB_SUB_BASE2_RESPONSE_TOPIC,
  postbackTopic: process.env.PUB_SUB_POSTBACK_TOPIC || 'dev-postback-topic',
  postbackSubscription: process.env.PUB_SUB_POSTBACK_SUBSCRIPTION || 'dev-postback-subscription',
};

export { pubSub, pubSubConfig };
