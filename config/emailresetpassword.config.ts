import { EmailTemplateLocals } from '../api/services';

const emailConfigPath = process.env.EMAIL_CONFIG_PATH;
const defaultParameters: EmailTemplateLocals = {
  title: 'Recover your credentials',
  preview: 'Here are the credentials you asked for in the BT   platform',
  body: `You requested a link to access the your account in the Gatekeeper plaftorm. <br/><br/>
        Please update your account with a brand new password clicking in the button below.`,
  button: {
    label: 'Set a new password',
    url: '',
  },
  section: {
    title: '',
    body: `
          If you did not ask for this credentials, ignore this e-mail or
          <a href="https://bacen.com.br" style="color: dodgerblue">contact the support</a>.`,
  },
};

export default {
  emailConfigPath,
  defaultParameters,
};
