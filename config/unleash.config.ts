import * as hat from 'hat';

export default {
  url: process.env.UNLEASH_URL || 'http://localhost:4242/api/',
  appName: 'bacen- ',
  appUrl: process.env.UNLEASH_APP_URL || 'http://localhost:4242',
  instanceId: process.env.K8S_POD_NAME || hat(),
};
