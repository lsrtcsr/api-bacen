import * as tunnel from 'tunnel';

let httpsAgent;

const {
  CREDITAS_PROVIDER_PROXY_URL,
  CREDITAS_PROVIDER_PROXY_PORT,
  CREDITAS_PROVIDER_PROXY_USER,
  CREDITAS_PROVIDER_PROXY_PASSWORD,
} = process.env;

if (CREDITAS_PROVIDER_PROXY_URL) {
  httpsAgent = tunnel.httpsOverHttp({
    proxy: {
      host: CREDITAS_PROVIDER_PROXY_URL,
      port: CREDITAS_PROVIDER_PROXY_PORT,
      proxyAuth: CREDITAS_PROVIDER_PROXY_USER
        ? `${CREDITAS_PROVIDER_PROXY_USER}:${CREDITAS_PROVIDER_PROXY_PASSWORD}`
        : undefined,
    },
  });
}

export default {
  httpsAgent,
  baseURL: process.env.CREDITAS_PROVIDER_API_BASE_URL || process.env.STR_PROVIDER_URL,
};
