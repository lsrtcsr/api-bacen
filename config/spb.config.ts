import { Arrangement, CustodyProvider } from '@bacen/base-sdk';

export default {
  defaultArrangement: (process.env.DEFAULT_ARRANGEMENT as Arrangement) || Arrangement.STR,
  spbDefaultProvider: (process.env.DEFAULT_SPB_PROVIDER as CustodyProvider) || null,
};
