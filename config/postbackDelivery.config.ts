import * as tunnel from 'tunnel';

let httpsAgent;

const { POSTBACK_PROXY_URL, POSTBACK_PROXY_PORT, POSTBACK_PROXY_USER, POSTBACK_PROXY_PASSWORD } = process.env;

const isActive = !!POSTBACK_PROXY_URL;

if (POSTBACK_PROXY_URL) {
  httpsAgent = tunnel.httpsOverHttp({
    proxy: {
      host: POSTBACK_PROXY_URL,
      port: POSTBACK_PROXY_PORT,
      proxyAuth: POSTBACK_PROXY_USER ? `${POSTBACK_PROXY_USER}:${POSTBACK_PROXY_PASSWORD}` : undefined,
    },
  });
}

export default {
  isActive,
  httpsAgent,
  baseURL: process.env.POSTBACK_URL,
};
