export default {
  baseURL: process.env.DICT_URL || 'http://localhost:3033',
  ispb: process.env.ISPB_DEFAULT_DICT || '03311443',
};
