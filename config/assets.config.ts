import { CustodyProvider } from '@bacen/base-sdk';

/** List of asset providers for which we are the source-of-truth */
const controlledBalanceProviders: CustodyProvider[] = [
  CustodyProvider.CREDITAS_PROVIDER,
  CustodyProvider.LECCA_PROVIDER,
  CustodyProvider.PARATI_PROVIDER,
];

const defaultAuthorizableAsset = process.env.DEFAULT_AUTHORIZABLE_ASSET;

export { controlledBalanceProviders, defaultAuthorizableAsset };
