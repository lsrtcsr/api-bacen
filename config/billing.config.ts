export default {
  mediatorBillingURL: process.env.MEDIATOR_BILLING_URL,
  billingAPIKey: process.env.MEDIATOR_BILLING_API_KEY,
  billingAPIUrl: 'https://testnet.bt .app/invoices',
  notification: {
    slack: {
      text: 'Invoice closing alert',
      title: 'The invoice can be accessed clicking in the link below',
    },
    email: {
      subject: '[bacen] Invoice Closing Notification',
      body: `
      The Invoice can be accessed clicking in the button below.`,
    },
  },
};
