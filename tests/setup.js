module.exports = () => {
  const { ConfigUtil } = require('../api/utils/ConfigUtil');
  ConfigUtil.initialize(process.env.NODE_ENV);
};
