import {
  WalletStatus,
  DomainRole,
  UserRole,
  TransitoryAccountType,
  CustodyProvider,
  AssetRegistrationStatus,
  CustodyBoletoPaymentWebService,
  PaymentStatus,
  TransactionStatus,
} from '@bacen/base-sdk';
import * as HttpMock from 'node-mocks-http';
import * as sinon from 'sinon';
import * as faker from 'faker';
import { BaseResponse } from 'ts-framework';
import MainServer from '../../api/MainServer';
import { User, Wallet, Asset, Domain, bacenRequest } from '../../api/models';
import {
  ServerTestUtil,
  DomainTestUtil,
  UserTestUtil,
  ConsumerTestUtil,
  WalletTestUtil,
  AssetTestUtil,
  TransactionTestUtil,
  RequestUtil,
  AssetServiceMock,
  UninstalHandle,
} from '../util';
import Controllers from '../../api/controllers';
import { ProviderUtil } from '../../api/utils';
import { TransactionPipelinePublisher, EventHandlingGateway, AssetService, LegacyWalletService } from '../../api/services';

jest.setTimeout(60000);

describe('api.controllers.BoletoController', () => {
  const controller = Controllers.BoletoController;

  let server: MainServer;
  let wallet: Wallet;
  let boletoTransitoryWallet: Wallet;
  let user: User;
  let issuer: User;
  let issuerWallet: Wallet;
  let asset: Asset;
  let rootAsset: Asset;
  let domain: Domain;
  let rootDomain: Domain;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    const assetService = AssetService.getInstance();

    domain = await DomainTestUtil.generateDomain();
    rootDomain = await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });

    user = await ConsumerTestUtil.generateConsumer({ userDomain: domain });
    issuer = await UserTestUtil.generateUser({ userDomain: rootDomain, role: UserRole.MEDIATOR });

    wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    issuerWallet = await WalletTestUtil.generateWalletAtState(issuer, WalletStatus.READY);
    boletoTransitoryWallet = await WalletTestUtil.generateWalletAtState(issuer, WalletStatus.READY, {
      isTransitoryAccount: true,
      transitoryAccountType: TransitoryAccountType.BOLETO_PAYMENT,
    });

    rootAsset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: assetService.rootAsset,
    });

    asset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: { provider: CustodyProvider.CDT_VISA },
    });

    await AssetTestUtil.linkWalletToAsset({
      asset,
      wallet: boletoTransitoryWallet,
      status: AssetRegistrationStatus.READY,
    });
    await AssetTestUtil.linkWalletToAsset({ asset: rootAsset, wallet, status: AssetRegistrationStatus.READY });
    await AssetTestUtil.linkWalletToAsset({
      asset: rootAsset,
      wallet: boletoTransitoryWallet,
      status: AssetRegistrationStatus.READY,
    });
    await AssetTestUtil.linkWalletToAsset({ asset, wallet, status: AssetRegistrationStatus.READY });
  });

  afterAll(async () => {
    await AssetTestUtil.unlinkWalletToAsset(asset, boletoTransitoryWallet);
    await AssetTestUtil.unlinkWalletToAsset(asset, wallet);
    await AssetTestUtil.destroyAsset(asset, false);

    await AssetTestUtil.unlinkWalletToAsset(rootAsset, boletoTransitoryWallet);
    await AssetTestUtil.unlinkWalletToAsset(rootAsset, wallet);
    await AssetTestUtil.destroyAsset(rootAsset, false);

    await UserTestUtil.destroyUser(issuer);
    await UserTestUtil.destroyUser(user);

    await DomainTestUtil.destroyDomain(domain);
    await DomainTestUtil.destroyDomain(rootDomain);

    await server.close();
    assetServiceUninstalHandle();
  });

  describe('pay', () => {
    let validateBoletoStub: sinon.SinonStub;
    let payBoletoStub: sinon.SinonStub;
    let chargeBoletoPaymentStub: sinon.SinonStub;
    let featureAvailableStub: sinon.SinonStub;
    let transactionPublisherStub: sinon.SinonStub;
    let eventHandlingStub: sinon.SinonStub;
    let getBalanceFromStellarStub: sinon.SinonStub;

    beforeAll(() => {
      featureAvailableStub = sinon.stub(ProviderUtil, 'throwIfFeatureNotAvailable').resolves(undefined);
      transactionPublisherStub = sinon.stub(TransactionPipelinePublisher.prototype, 'send').resolves(true);
      eventHandlingStub = sinon.stub(EventHandlingGateway.prototype, 'process').resolves(undefined);
      getBalanceFromStellarStub = sinon
        .stub(LegacyWalletService.prototype, 'getBalancesFromStellar')
        .callsFake(async (wallet, assets) => assets.map(asset => ({ code: asset.code, balance: 100 })));
    });

    afterEach(() => {
      if (validateBoletoStub) {
        validateBoletoStub.restore();
        validateBoletoStub = null;
      }

      if (payBoletoStub) {
        payBoletoStub.restore();
        payBoletoStub = null;
      }

      if (chargeBoletoPaymentStub) {
        chargeBoletoPaymentStub.restore();
        chargeBoletoPaymentStub = null;
      }
    });

    afterAll(() => {
      featureAvailableStub.restore();
      transactionPublisherStub.restore();
      eventHandlingStub.restore();
      getBalanceFromStellarStub.restore();
    });

    it('Pays a boleto when passet an asset code', async () => {
      const digitableLine = faker.finance.account(48);
      const amount = faker.finance.amount(0, 99);

      validateBoletoStub = sinon.stub(CustodyBoletoPaymentWebService.prototype, 'validate').resolves({
        paymentInfo: {
          amount,
        },
      });

      payBoletoStub = sinon
        .stub(CustodyBoletoPaymentWebService.prototype, 'pay')
        .callsFake(async (_, payment, __) => ({ id: payment.id, status: PaymentStatus.SETTLED }));

      const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
        path: '/boletos/pay',
        body: {
          amount,
          digitableLine,
          source: wallet.id,
          asset: asset.code,
          provider: asset.provider,
        },
      });
      RequestUtil.patchRequestObject(req);
      RequestUtil.patchResponseObject(res);

      try {
        await controller.payBoleto(req, res);
      } catch (e) {
        console.error('', e);
        throw e;
      }

      expect(res._isEndCalled()).toBe(true);
      expect(res._isJSON()).toBe(true);
      expect(res._getStatusCode()).toBe(200);

      const response = (res as HttpMock.MockResponse<BaseResponse>)._getJSONData();
      expect(response.id).toMatch(/^[a-f\d]{8}-([a-f\d]{4}-){3}[a-f\d]{12}$/i);
      expect(response.status).toBe(TransactionStatus.AUTHORIZED);

      await TransactionTestUtil.destroyTransaction({ id: response.id } as any);
    });
  });
});
