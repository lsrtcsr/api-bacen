import AxiosMock from 'axios-mock-adapter';
import * as supertest from 'supertest';
import { CustodyProvider, WalletStatus, CardStatus, UserRole } from '@bacen/base-sdk';
import * as sinon from 'sinon';
import MainServer from '../../api/MainServer';
import {
  ServerTestUtil,
  UserTestUtil,
  WalletTestUtil,
  OAuthTestUtil,
  ConsumerTestUtil,
  AssetTestUtil,
  CardTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../util';
import { Asset, Wallet, User, Card, OAuthAccessToken } from '../../api/models';
import { CDTProviderService, ProviderManagerService } from '../../api/services';
import { ProviderUtil } from '../../api/utils';

jest.setTimeout(60000);

describe('api.controllers.CardController', () => {
  let server: MainServer;
  let asset: Asset;
  let wallet: Wallet;
  let issuerWallet: Wallet;
  let user: User;
  let accessToken: OAuthAccessToken;
  let request: supertest.SuperTest<supertest.Test>;
  let axiosMock: AxiosMock;
  let providerAvailableStub: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    request = supertest(server.app);

    user = await ConsumerTestUtil.generateConsumer();
    accessToken = await OAuthTestUtil.generateTokenFromUser(user);
    wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    issuerWallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);

    asset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: { provider: CustodyProvider.CDT_VISA },
    });

    axiosMock = new AxiosMock(
      (ProviderManagerService.getInstance().from(CustodyProvider.CDT_VISA) as CDTProviderService)['http']['client'],
    );

    providerAvailableStub = sinon.stub(ProviderUtil, 'throwIfServiceNotAvailable').resolves(undefined);
  });

  afterEach(() => {
    axiosMock.reset();
  });

  afterAll(async () => {
    axiosMock.restore();
    providerAvailableStub.restore();

    await AssetTestUtil.destroyAsset(asset, false);
    await UserTestUtil.destroyUser(user);

    await server.close();
    assetServiceUninstallHandle();
  });

  describe('unblock', () => {
    it('Correctly handles an unblocking error', async () => {
      axiosMock.onPost(/\/provider\/cards\/[a-fA-F\d-]+\/unblock/).reply(400, {
        message: 'Card is already unblocked',
      });
      const card = await CardTestUtil.generateCard({ wallet, virtual: false, status: CardStatus.AVAILABLE });

      try {
        const response = await request
          .post(`/wallets/${wallet.id}/cards/${card.id}/unblock`)
          .set('Authorization', `Bearer ${accessToken.accessToken}`)
          .send({ password: '', asset: asset.code })
          .expect(500);

        expect(response.body.details.message).toMatch(/Card is already unblocked/i);
        const reloadedCard = await Card.findOne(card.id);
        expect(reloadedCard.status).toBe(CardStatus.AVAILABLE);
      } finally {
        await CardTestUtil.destroy(card);
      }
    });

    it('Correctly handles a successfull card unblocking', async () => {
      axiosMock.onPost(/\/provider\/cards\/[a-fA-F\d-]+\/unblock/).reply(200, true);
      const card = await CardTestUtil.generateCard({ wallet, virtual: false, status: CardStatus.BLOCKED });

      try {
        const response = await request
          .post(`/wallets/${wallet.id}/cards/${card.id}/unblock`)
          .set('Authorization', `Bearer ${accessToken.accessToken}`)
          .send({ password: '', asset: asset.code })
          .expect(200);

        const reloadedCard = await Card.findOne(card.id);
        expect(reloadedCard.status).toBe(CardStatus.AVAILABLE);
      } finally {
        await CardTestUtil.destroy(card);
      }
    });
  });
});
