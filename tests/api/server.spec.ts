import * as getPort from 'get-port';
import * as request from 'supertest';
import MainServer from '../../api/MainServer';
import { UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 10000);

describe('api.MainServer', () => {
  let server: MainServer;
  let assetServiceUnistallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUnistallHandle = AssetServiceMock.install();
    server = new MainServer({ port: await getPort({ port: 3000 }) });
    await server.listen();
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUnistallHandle();
  });

  it('should redirect to status in root url', async () => {
    const response = await request(server.app)
      .get('/')
      .redirects(1)
      .expect(200);

    expect(response.body.uptime).toBeGreaterThan(0);
  });

  it('should respond to a simple status request', async () => {
    const response = await request(server.app)
      .get('/status')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(response.body.uptime).toBeGreaterThan(0);
  });
});
