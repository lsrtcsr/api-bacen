import * as request from 'supertest';
import * as sinon from 'sinon';
import { UserRole, TransactionStatus, PaymentType, AssetRegistrationStatus, WalletStatus } from '@bacen/base-sdk';
import { StellarService } from '@bacen/stellar-service';
import MainServer from '../../api/MainServer';
import { User, OAuthAccessToken, Wallet, Asset } from '../../api/models';
import { StellarProviderService } from '../../api/services';
import {
  ServerTestUtil,
  AssetTestUtil,
  PaymentTestUtil,
  UserTestUtil,
  OAuthTestUtil,
  WalletTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../util';

jest.setTimeout(60000);

describe('api.walletController', () => {
  let server: MainServer;
  let user: User;
  let accessToken: OAuthAccessToken;
  let wallet: Wallet;
  let otherWallet: Wallet;
  let asset: Asset;
  let stellarServiceLoadAccountStub: sinon.SinonStub;
  let stellarRegisterStub: sinon.SinonStub;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstalHandle();
  });

  describe('GET /wallets/:id', () => {
    beforeAll(async () => {
      stellarRegisterStub = sinon.stub(StellarProviderService.prototype, 'register');
      stellarServiceLoadAccountStub = sinon.stub(StellarService.prototype, 'loadAccount').resolves({
        balances: [
          {
            asset_code: 'BRLD',
            balance: '1000.0000000',
          },
        ],
      } as any);

      user = await UserTestUtil.generateUser({ role: UserRole.ADMIN });
      accessToken = await OAuthTestUtil.generateTokenFromUser(user);

      wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.REGISTERED_IN_STELLAR);
      otherWallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.REGISTERED_IN_STELLAR);

      asset = await AssetTestUtil.generateAsset({ wallet, partial: { code: 'BRLD' } });

      await AssetTestUtil.linkWalletToAsset({ asset, wallet, status: AssetRegistrationStatus.READY });
    });

    afterAll(async () => {
      await AssetTestUtil.unlinkWalletToAsset(asset, wallet);
      if (asset) {
        await AssetTestUtil.destroyAsset(asset, false);
        asset = null;
      }

      if (user) {
        await UserTestUtil.destroyUser(user);
        user = null;
        accessToken = null;
        wallet = null;
        otherWallet = null;
      }

      if (stellarServiceLoadAccountStub) {
        stellarServiceLoadAccountStub.restore();
        stellarServiceLoadAccountStub = null;
      }

      if (stellarRegisterStub) {
        stellarRegisterStub.restore();
        stellarRegisterStub = null;
      }
    });

    it('Returns the correct pending and consolidated balance when there are no pending transactions', async () => {
      const response = await request(server.app)
        .get(`/wallets/${wallet.id}`)
        .set('Authorization', `Bearer ${accessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(200);

      const brldAsset = response.body.assets.find(asset => asset.code === 'BRLD');

      expect(brldAsset.pendingBalance.amount).toBe('0.0000000');
      expect(brldAsset.balance).toBe('1000.0000000');
      expect(brldAsset.consolidatedBalance).toBe('1000.0000000');
      expect(brldAsset.authorizableBalance).toBe('1000.0000000');
    });

    it('Returns the correct pending and consolidated balance when there are authorized transactions as source', async () => {
      const payment = await PaymentTestUtil.generatePayment(wallet, otherWallet, {
        accessToken,
        asset,
        type: PaymentType.TRANSFER,
        status: TransactionStatus.AUTHORIZED,
        amount: '100.00',
      });

      const response = await request(server.app)
        .get(`/wallets/${wallet.id}`)
        .set('Authorization', `Bearer ${accessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(200);

      const brldAsset = response.body.assets.find(asset => asset.code === 'BRLD');

      expect(brldAsset.pendingBalance.amount).toBe('100.0000000');
      expect(brldAsset.balance).toBe('1000.0000000');
      expect(brldAsset.consolidatedBalance).toBe('900.0000000');
      expect(brldAsset.authorizableBalance).toBe('900.0000000');

      await PaymentTestUtil.destroyPayment(payment);
    });

    it('Returns the correct pending and consolidated balance when there are authorized transactions as destination', async () => {
      const payment = await PaymentTestUtil.generatePayment(otherWallet, wallet, {
        accessToken,
        asset,
        type: PaymentType.TRANSFER,
        status: TransactionStatus.AUTHORIZED,
        amount: '100.00',
      });

      const response = await request(server.app)
        .get(`/wallets/${wallet.id}`)
        .set('Authorization', `Bearer ${accessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(200);

      const brldAsset = response.body.assets.find(asset => asset.code === 'BRLD');

      expect(brldAsset.pendingBalance.amount).toBe('-100.0000000');
      expect(brldAsset.balance).toBe('1000.0000000');
      expect(brldAsset.consolidatedBalance).toBe('1100.0000000');
      expect(brldAsset.authorizableBalance).toBe('1000.0000000');

      await PaymentTestUtil.destroyPayment(payment);
    });
  });
});
