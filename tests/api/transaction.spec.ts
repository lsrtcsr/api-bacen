import * as request from 'supertest';
import * as faker from 'faker';
import * as sinon from 'sinon';
import { UserRole, TransactionStatus, TransactionType } from '@bacen/base-sdk';
import MainServer from '../../api/MainServer';
import { User, OAuthAccessToken, Wallet } from '../../api/models';
import { TransactionPipelinePublisher } from '../../api/services';
import {
  ServerTestUtil,
  UserTestUtil,
  OAuthTestUtil,
  WalletTestUtil,
  TransactionTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../util';

jest.setTimeout(60000);

const invalidTransactionId = faker.random.uuid();

describe('api.TransactionController', () => {
  let server: MainServer;
  let user: User;
  let adminUser: User;
  let accessToken: OAuthAccessToken;
  let adminAccessToken: OAuthAccessToken;
  let wallet: Wallet;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstalHandle();
  });

  describe('POST /transactions/:id/cancel', () => {
    beforeAll(async () => {
      user = await UserTestUtil.generateUser({ role: UserRole.CONSUMER });
      adminUser = await UserTestUtil.generateUser({ role: UserRole.ADMIN });

      accessToken = await OAuthTestUtil.generateTokenFromUser(user);
      adminAccessToken = await OAuthTestUtil.generateTokenFromUser(adminUser);

      wallet = await WalletTestUtil.generate(user);
    });

    afterAll(async () => {
      if (user) {
        await UserTestUtil.destroyUser(user);
        user = null;

        // destroyUser already destroys user's accessTokens and wallets
        accessToken = null;
        wallet = null;
      }

      if (adminUser) {
        await UserTestUtil.destroyUser(adminUser);
        adminUser = null;

        // destroyUser already destroys user's accessTokens
        adminAccessToken = null;
      }
    });

    it('Rejects an unauthenticated user', async () => {
      const result = await request(server.app)
        .post(`/transactions/${invalidTransactionId}/cancel`)
        .expect('Content-Type', /json/)
        .expect(401);
    });

    it('Rejects a user with insuficcient scope', async () => {
      const result = await request(server.app)
        .post(`/transactions/${invalidTransactionId}/cancel`)
        .set('Authorization', `Bearer ${accessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(401);
    });

    it('Rejects a non-existent transactionId', async () => {
      const result = await request(server.app)
        .post(`/transactions/${invalidTransactionId}/cancel`)
        .set('Authorization', `Bearer ${adminAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(404);
    });

    it('Rejects a transaction which is already failed', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.FAILED,
        type: TransactionType.PAYMENT,
        accessToken,
      });

      const result = await request(server.app)
        .post(`/transactions/${transaction.id}/cancel`)
        .set('Authorization', `Bearer ${adminAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(400);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Rejects a transaction which is executed', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.EXECUTED,
        type: TransactionType.PAYMENT,
        accessToken,
      });

      const result = await request(server.app)
        .post(`/transactions/${transaction.id}/cancel`)
        .set('Authorization', `Bearer ${adminAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(400);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Rejects a transaction which is notified', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.NOTIFIED,
        type: TransactionType.PAYMENT,
        accessToken,
      });

      const result = await request(server.app)
        .post(`/transactions/${transaction.id}/cancel`)
        .set('Authorization', `Bearer ${adminAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(400);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Marks a transaction as canceled', async () => {
      const transactionPostbackPublisherStub = sinon.stub(TransactionPipelinePublisher.prototype, 'send');

      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.AUTHORIZED,
        type: TransactionType.PAYMENT,
        accessToken,
      });

      const result = await request(server.app)
        .post(`/transactions/${transaction.id}/cancel`)
        .set('Authorization', `Bearer ${adminAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(200);

      await TransactionTestUtil.destroyTransaction(transaction);
      transactionPostbackPublisherStub.restore();
    });
  });
});
