import * as request from 'supertest';
import { UserRole, OAuthClientPlatform } from '@bacen/base-sdk';
import MainServer from '../../api/MainServer';
import { ServerTestUtil, OAuthTestUtil, UserTestUtil, UninstalHandle, AssetServiceMock } from '../util';
import { User, OAuthAccessToken, OAuthClient } from '../../api/models';

jest.setTimeout(100000);

const authorization = (() => {
  return {
    event: 'authorization',
    type: 'card',
    asset: 'BRLP',
    amount: '10',
    walletId: 'b691b2a8-02bd-4f6b-bfc5-b8238ae59f36',
    transactionId: '9bf36a89-4164-477e-aa0b-560722c63a64',
    transitoryAccountType: 'card_transaction',
    additionalData: {
      merchantName: 'MerchantName',
      merchantCode: '123456',
      raw: {},
    },
  };
})();

describe('api.authorization', () => {
  let server: MainServer;
  let oauthClient: OAuthClient;
  let authorizationAccessToken: OAuthAccessToken;
  let mediatorUser: User;
  let mediatorAccessToken: OAuthAccessToken;
  let consumerUser: User;
  let consumerAccessToken: OAuthAccessToken;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstallHandle();
  });

  describe('authorization oauth permission', () => {
    beforeAll(async () => {
      oauthClient = await OAuthTestUtil.generateClientByPlatform(OAuthClientPlatform.EXTERNAL_AUTHORIZER);
      authorizationAccessToken = await OAuthTestUtil.generateTokenFromClient(oauthClient, [
        'authorizations:read',
        'authorizations:write',
      ]);

      mediatorUser = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR });
      mediatorAccessToken = await OAuthTestUtil.generateTokenFromUser(mediatorUser);

      consumerUser = await UserTestUtil.generateUser({ role: UserRole.CONSUMER });
      consumerAccessToken = await OAuthTestUtil.generateTokenFromUser(consumerUser);
    });

    afterAll(async () => {
      authorizationAccessToken = null;

      if (mediatorUser) {
        await UserTestUtil.destroyUser(mediatorUser);
        mediatorUser = null;
        mediatorAccessToken = null;
      }

      if (consumerUser) {
        await UserTestUtil.destroyUser(consumerUser);
        consumerUser = null;
        consumerAccessToken = null;
      }

      await OAuthTestUtil.destroyClient(oauthClient);
    });

    it('accept valid access token', async () => {
      const result = await request(server.app)
        .post(`/transactions/authorize`)
        .send(authorization)
        .set('Authorization', `Bearer ${authorizationAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(200);
    });

    it('bad request if send no body', async () => {
      const result = await request(server.app)
        .post(`/transactions/authorize`)
        .send()
        .set('Authorization', `Bearer ${authorizationAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(400);
    });

    it('reject mediator authorization', async () => {
      const result = await request(server.app)
        .post(`/transactions/authorize`)
        .send(authorization)
        .set('Authorization', `Bearer ${mediatorAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(401);
    });

    it('reject consumer authorization', async () => {
      const result = await request(server.app)
        .post(`/transactions/authorize`)
        .send(authorization)
        .set('Authorization', `Bearer ${consumerAccessToken.accessToken}`)
        .expect('Content-Type', /json/)
        .expect(401);
    });
  });
});
