import * as httpMock from 'node-mocks-http';
import * as sinon from 'sinon';
import * as faker from 'faker';
import { HttpError, BaseRequest, BaseResponse } from 'ts-framework';
import { IdempotenceStore, StoredResponse } from '../../api/services';
import { RequestUtil } from '../util';
import { Idempotent } from '../../api/decorators';
import { Ctor } from '../../api/utils';
import { bacenRequest } from '../../api/models';

export interface MockStoreFactoryStubs {
  lockStub: sinon.SinonStub;
  unlockStub: sinon.SinonStub;
  getStub: sinon.SinonStub;
  setStub: sinon.SinonStub;
}

function mockStoreFactory(
  options: Partial<MockStoreFactoryStubs> = {},
): [Ctor<IdempotenceStore, []>, MockStoreFactoryStubs] {
  const {
    lockStub = sinon.stub(),
    unlockStub = sinon.stub(),
    setStub = sinon.stub(),
    getStub = sinon.stub(),
  } = options;
  const stubs = { lockStub, unlockStub, setStub, getStub };

  class MockStore implements IdempotenceStore {
    constructor() {}

    lock = lockStub;

    unlock = unlockStub;

    get = getStub;

    set = setStub;
  }

  return [MockStore, stubs];
}

describe('api.decorators.Idempotent', () => {
  it('does nothing if there is no idempotence key', async () => {
    const [store, storeStubs] = mockStoreFactory();
    const originalImplStub = sinon.stub().resolves(undefined);
    const { value: decoratedHandler } = Idempotent({ store })(undefined as any, undefined as any, {
      value: originalImplStub,
    });

    const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>();
    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    await decoratedHandler(req, res);

    expect(originalImplStub.calledOnce).toBe(true);
    expect(storeStubs.getStub.called).toBe(false);
    expect(storeStubs.setStub.called).toBe(false);
    expect(storeStubs.lockStub.called).toBe(false);
    expect(storeStubs.unlockStub.called).toBe(false);
  });

  it('Throws an exception if the key is locked', async () => {
    const key = faker.random.uuid();

    const [store, storeStubs] = mockStoreFactory({
      lockStub: sinon
        .stub()
        .withArgs(key)
        .resolves(false),
    });

    const originalImplStub = sinon.stub().resolves(undefined);
    const { value: decoratedHandler } = Idempotent({ store })(undefined as any, undefined as any, {
      value: originalImplStub,
    });

    const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);
    let exception: HttpError;

    try {
      await decoratedHandler(req, res);
    } catch (e) {
      exception = e;
    }

    expect(exception).toBeDefined();
    expect(exception.status).toBe(423);
    expect(originalImplStub.called).toBe(false);
    expect(storeStubs.getStub.called).toBe(false);
    expect(storeStubs.lockStub.calledOnce).toBe(true);
    expect(storeStubs.lockStub.calledWith(key)).toBe(true);
    expect(storeStubs.unlockStub.calledOnce).toBe(true);
    expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
    expect(storeStubs.setStub.called).toBe(false);
  });

  it('Returns the previous response if there was one stored for the idempotence key', async () => {
    const key = faker.random.uuid();
    const mockStoredResponse: StoredResponse = {
      status: 200,
      contentType: 'application/json',
      body: {
        foo: faker.lorem.word(),
        bar: faker.lorem.word(),
      },
    };

    const [store, storeStubs] = mockStoreFactory({
      lockStub: sinon
        .stub()
        .withArgs(key)
        .resolves(true),
      getStub: sinon
        .stub()
        .withArgs(key)
        .resolves(mockStoredResponse),
    });
    const originalImplStub = sinon.stub().resolves(undefined);
    const { value: decoratedHandler } = Idempotent({ store })(undefined as any, undefined as any, {
      value: originalImplStub,
    });

    const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    await decoratedHandler(req, res);

    const sentData = (res as httpMock.MockResponse<BaseResponse>)._getData();
    const sentStaus = (res as httpMock.MockResponse<BaseResponse>)._getStatusCode();
    const sentContentType = (res as httpMock.MockResponse<BaseResponse>)._getHeaders()['content-type'];

    expect(originalImplStub.called).toBe(false);
    expect(sentData).toEqual(mockStoredResponse.body);
    expect(sentStaus).toBe(mockStoredResponse.status);
    expect(sentContentType).toBe(mockStoredResponse.contentType);
    expect(storeStubs.getStub.calledOnce).toBe(true);
    expect(storeStubs.getStub.calledWith(key)).toBe(true);
    expect(storeStubs.lockStub.calledOnce).toBe(true);
    expect(storeStubs.lockStub.calledWith(key)).toBe(true);
    expect(storeStubs.setStub.called).toBe(false);
    expect(storeStubs.unlockStub.calledOnce).toBe(true);
    expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
  });

  describe('When there is no stored response', () => {
    it('Does not record a response not matching the success response codes if recordFailure is false', async () => {
      const key = faker.random.uuid();

      const [store, storeStubs] = mockStoreFactory({
        getStub: sinon.stub().resolves(undefined),
        lockStub: sinon
          .stub()
          .withArgs(key)
          .resolves(true),
      });
      const originalImplStub = sinon.stub().callsFake((req: BaseRequest, res: BaseResponse) => {
        res.status(400).json({});
      });

      const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
      RequestUtil.patchRequestObject(req);
      RequestUtil.patchResponseObject(res);

      const { value: decoratedHandler } = Idempotent({ store })(undefined as any, undefined as any, {
        value: originalImplStub,
      });

      await decoratedHandler(req, res);

      expect(originalImplStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledWith(key)).toBe(true);
      expect(storeStubs.getStub.calledOnce).toBe(true);
      expect(storeStubs.getStub.calledWith(key)).toBe(true);
      expect(storeStubs.unlockStub.calledOnce).toBe(true);
      expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
      expect(storeStubs.setStub.called).toBe(false);
    });

    it('Records a sent response if it is a success', async () => {
      const key = faker.random.uuid();
      const fakeResponse = {
        foo: faker.lorem.words(),
        bar: faker.lorem.words(),
      };

      const [store, storeStubs] = mockStoreFactory({
        getStub: sinon.stub().resolves(undefined),
        lockStub: sinon
          .stub()
          .withArgs(key)
          .resolves(true),
      });

      const originalImplStub = sinon.stub().callsFake((req: BaseRequest, res: BaseResponse) => {
        res
          .status(200)
          .contentType('application/json')
          .send(fakeResponse);
      });
      const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
      RequestUtil.patchRequestObject(req);
      RequestUtil.patchResponseObject(res);

      const { value: decoratedHandler } = Idempotent({ store })(undefined as any, undefined as any, {
        value: originalImplStub,
      });

      await decoratedHandler(req, res);

      expect(originalImplStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledWith(key)).toBe(true);
      expect(storeStubs.getStub.calledOnce).toBe(true);
      expect(storeStubs.getStub.calledWith(key)).toBe(true);
      expect(storeStubs.unlockStub.calledOnce).toBe(true);
      expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
      expect(storeStubs.setStub.calledOnce).toBe(true);
      expect(storeStubs.setStub.args[0][0]).toBe(key);
      expect(storeStubs.setStub.args[0][1]).toEqual<StoredResponse>({
        contentType: 'application/json',
        status: 200,
        body: fakeResponse,
      });
    });

    it('Records a sent response if it is a success with a custom success code', async () => {
      const key = faker.random.uuid();
      const fakeResponse = {
        foo: faker.lorem.words(),
        bar: faker.lorem.words(),
      };

      const [store, storeStubs] = mockStoreFactory({
        getStub: sinon.stub().resolves(undefined),
        lockStub: sinon
          .stub()
          .withArgs(key)
          .resolves(true),
      });

      const originalImplStub = sinon.stub().callsFake((req: BaseRequest, res: BaseResponse) => {
        res
          .status(201)
          .contentType('application/json')
          .send(fakeResponse);
      });
      const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
      RequestUtil.patchRequestObject(req);
      RequestUtil.patchResponseObject(res);

      const { value: decoratedHandler } = Idempotent({ store, successCodes: [200, 201] })(
        undefined as any,
        undefined as any,
        { value: originalImplStub },
      );

      await decoratedHandler(req, res);

      expect(originalImplStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledWith(key)).toBe(true);
      expect(storeStubs.getStub.calledOnce).toBe(true);
      expect(storeStubs.getStub.calledWith(key)).toBe(true);
      expect(storeStubs.unlockStub.calledOnce).toBe(true);
      expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
      expect(storeStubs.setStub.calledOnce).toBe(true);
      expect(storeStubs.setStub.args[0][0]).toBe(key);
      expect(storeStubs.setStub.args[0][1]).toEqual<StoredResponse>({
        contentType: 'application/json',
        status: 201,
        body: fakeResponse,
      });
    });

    it('Records a sent failure if recordFailure is true', async () => {
      const key = faker.random.uuid();
      const fakeResponse = {
        foo: faker.lorem.words(),
        bar: faker.lorem.words(),
      };

      const [store, storeStubs] = mockStoreFactory({
        getStub: sinon.stub().resolves(undefined),
        lockStub: sinon
          .stub()
          .withArgs(key)
          .resolves(true),
      });

      const originalImplStub = sinon.stub().callsFake((req: BaseRequest, res: BaseResponse) => {
        res
          .status(500)
          .contentType('application/json')
          .send(fakeResponse);
      });
      const { req, res } = httpMock.createMocks<bacenRequest, BaseResponse>({ headers: { 'X-Idempotence-Key': key } });
      RequestUtil.patchRequestObject(req);
      RequestUtil.patchResponseObject(res);

      const { value: decoratedHandler } = Idempotent({ store, recordFailure: true })(
        undefined as any,
        undefined as any,
        { value: originalImplStub },
      );

      await decoratedHandler(req, res);

      expect(originalImplStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledOnce).toBe(true);
      expect(storeStubs.lockStub.calledWith(key)).toBe(true);
      expect(storeStubs.getStub.calledOnce).toBe(true);
      expect(storeStubs.getStub.calledWith(key)).toBe(true);
      expect(storeStubs.unlockStub.calledOnce).toBe(true);
      expect(storeStubs.unlockStub.calledWith(key)).toBe(true);
      expect(storeStubs.setStub.calledOnce).toBe(true);
      expect(storeStubs.setStub.args[0][0]).toBe(key);
      expect(storeStubs.setStub.args[0][1]).toEqual<StoredResponse>({
        contentType: 'application/json',
        status: 500,
        body: fakeResponse,
      });
    });
  });
});
