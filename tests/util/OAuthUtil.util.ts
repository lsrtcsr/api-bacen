import { OAuthClientPlatform, UserRole } from '@bacen/base-sdk';
import * as moment from 'moment';
import * as uuid from 'uuid';
import { Domain, OAuthAccessToken, OAuthClient, OAuthRefreshToken, User, RequestLog } from '../../api/models';
import Config from '../../config';
import { UserTestUtil } from './UserUtil';
import hat = require('hat');

export class OAuthTestUtil {
  public static async generateClient() {
    const client = await OAuthClient.insertAndFind({
      clientId: `testClientID_${uuid.v4()}`,
      clientSecret: 'clientSecret',
      platform: OAuthClientPlatform.API,
    });
    return client;
  }

  public static async generateToken(role?: UserRole, domain?: Domain) {
    const [client, user] = await Promise.all([
      OAuthTestUtil.generateClient(),
      UserTestUtil.generateUser({ role, userDomain: domain }),
    ]);

    const token = await OAuthAccessToken.insertAndFind({
      user,
      client,
      accessToken: uuid.v4(),
      tokenType: 'Bearer',
      expires: moment().add(1, 'hour').toDate(),
      // Add all available scopes for this role
      scope: Config.oauth.scopes[user.role],
    });

    const refreshToken = await OAuthRefreshToken.insertAndFind({
      user,
      client,
      accessToken: token,
      refreshToken: uuid.v4(),
      expires: moment().add(1, 'hour').toDate(),
    });

    return { user, accessToken: token };
  }

  public static async generateTokenFromUser(
    user: User,
    _client?: OAuthClient,
    token?: { accessToken?: string; refreshToken?: string },
  ) {
    const client = _client || (await OAuthTestUtil.generateClient());

    const accessToken = await OAuthAccessToken.insertAndFind({
      user,
      client,
      accessToken: token?.accessToken || uuid.v4(),
      tokenType: 'Bearer',
      expires: moment().add(1, 'hour').toDate(),
      // Add all available scopes for this role
      scope: Config.oauth.scopes[user.role],
    });

    const refreshToken = await OAuthRefreshToken.insertAndFind({
      user,
      client,
      accessToken,
      refreshToken: token?.refreshToken || uuid.v4(),
      expires: moment().add(1, 'hour').toDate(),
    });

    return accessToken;
  }

  public static async generateClientByPlatform(platform: OAuthClientPlatform) {
    const client = await OAuthClient.insertAndFind({
      clientId: `platformClientID_${uuid.v4()}`,
      clientSecret: 'clientSecret',
      platform,
    });
    return client;
  }

  public static async generateTokenFromClient(
    _client?: OAuthClient,
    scopes?: string[],
    token?: { accessToken?: string; refreshToken?: string },
  ) {
    const client = _client || (await OAuthTestUtil.generateClientByPlatform(OAuthClientPlatform.ROOT));

    const accessToken = await OAuthAccessToken.insertAndFind({
      client,
      accessToken: token?.accessToken || uuid.v4(),
      tokenType: 'Bearer',
      expires: moment().add(1, 'hour').toDate(),
      scope: scopes || Config.oauth.scopes[UserRole.PUBLIC],
    });

    const refreshToken = await OAuthRefreshToken.insertAndFind({
      client,
      accessToken,
      refreshToken: token?.refreshToken || uuid.v4(),
      expires: moment().add(1, 'hour').toDate(),
    });

    return accessToken;
  }

  public static async destroyClient(client: OAuthClient) {
    const oauthAccessToken = await OAuthAccessToken.findOne({ where: { client: client.id } });

    await RequestLog.createQueryBuilder()
      .delete()
      .from(RequestLog)
      .where('token = :token', { token: oauthAccessToken.id })
      .execute();

    await OAuthRefreshToken.createQueryBuilder()
      .delete()
      .from(OAuthRefreshToken)
      .where('client = :id', { id: client.id })
      .execute();

    await OAuthAccessToken.createQueryBuilder()
      .delete()
      .from(OAuthAccessToken)
      .where('client = :id', { id: client.id })
      .execute();

    await OAuthClient.remove(client);
  }

  public static async destroyToken(token: OAuthAccessToken, destroyUser = true) {
    if (destroyUser) await UserTestUtil.destroyUser(token.user);

    await this.destroyClient(token.client);
    return true;
  }
}
