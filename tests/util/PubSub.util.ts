import { Message } from '@google-cloud/pubsub';

import sinon = require('sinon');

export function mockMessage(message: object) {
  const ack = sinon.stub();
  const nack = sinon.stub;

  return {
    data: Buffer.from(JSON.stringify(message)),
    ack,
    nack,
  };
}
