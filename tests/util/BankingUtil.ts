import { DeepPartial, EntityManager, getManager } from 'typeorm';
import * as cpf from 'cpf';
import * as faker from 'faker';
import { AccountType } from '@bacen/base-sdk';
import { Consumer, Banking } from '../../api/models';

export interface GenerateBankingOptions {
  consumer: Consumer;
  partial?: DeepPartial<Banking>;
  manager?: EntityManager;
}

export class BankingTestUtil {
  public static async generate(options: GenerateBankingOptions): Promise<Banking> {
    const { consumer, partial = {}, manager = getManager() } = options;

    return Banking.insertAndFind(
      {
        consumer,
        taxId: cpf.generate(),
        holderType: faker.random.arrayElement(Object.values(AccountType)),
        name: faker.name.findName(),
        agency: faker.finance.account(4),
        agencyDigit: faker.random.alphaNumeric(1),
        account: faker.finance.account(12),
        accountDigit: faker.random.number(9).toString(),
        bank: faker.finance.account(3),
        ...partial,
      },
      { manager },
    );
  }

  public static async destroy(banking: Banking): Promise<void> {
    await Banking.delete(banking.id);
  }
}
