import {
  ConsumerStatus,
  DomainRole,
  DomainSchema,
  UserSchema,
  UserStatus,
  WalletStatus,
  UserRole,
} from '@bacen/base-sdk';
import { getManager } from 'typeorm';
import { Keypair } from 'stellar-sdk';
import {
  Address,
  Consumer,
  ConsumerState,
  Domain,
  DomainSettings,
  Phone,
  User,
  UserState,
  Wallet,
  WalletState,
  Asset,
} from '../../api/models';
import Config from '../../config';

export class RootUtil {
  public static async generateRoot() {
    await this.generateRootDomain({
      rootDomain: Config.seed.rootDomain,
      rootMediator: Config.seed.rootMediator,
    });
    await this.generateDefaultDomain({
      defaultDomain: Config.seed.defaultDomain,
    });
    await this.generateRootAdmin({
      rootAdmin: Config.seed.rootAdmin,
      rootAsset: (Config.seed as any).rootAsset,
    });
  }

  public static async generateRootAdmin(options: {
    rootAdmin: Partial<User> & { password?: string };
    rootAsset: Partial<Asset>;
  }): Promise<void> {
    let rootAdmin: User;
    let rootAsset: Asset;
    let rootStellarWallet: Wallet;

    const adminUsersCount = await User.count({ where: { role: UserRole.ADMIN } });

    if (!adminUsersCount) {
      await getManager().transaction(async (transactionalEntityManager) => {
        const rootDomain = await Domain.getRootDomain({ manager: transactionalEntityManager });

        const rootAdminSchema = User.create({
          ...(options.rootAdmin as Partial<User>),
          domain: rootDomain,
          role: UserRole.ADMIN,
        });
        await rootAdminSchema.setPassword(options.rootAdmin.password);
        const rootAdminResult = await transactionalEntityManager.insert(User, rootAdminSchema);

        // Insert admin active state
        await transactionalEntityManager.insert(UserState, [
          UserState.create({ user: { id: rootAdminResult.identifiers[0].id }, status: UserStatus.PENDING }),
          UserState.create({ user: { id: rootAdminResult.identifiers[0].id }, status: UserStatus.PROCESSING }),
          UserState.create({ user: { id: rootAdminResult.identifiers[0].id }, status: UserStatus.ACTIVE }),
        ]);

        // Create the root admin wallets
        rootAdmin = await transactionalEntityManager.findOne(User, rootAdminResult.identifiers[0].id, {
          relations: ['states'],
        });

        // TODO: Ensure wallet exists in network before continuing
        const rootKeyPair = Keypair.fromSecret((Config.stellar as any).service.rootWallet.secretKey);
        rootStellarWallet = await Wallet.insertAndFind(
          {
            user: { id: rootAdminResult.identifiers[0].id },
            stellar: {
              publicKey: rootKeyPair.publicKey(),
              secretKey: rootKeyPair.secret(),
            },
          },
          { manager: transactionalEntityManager },
        );

        await transactionalEntityManager.insert(WalletState, [
          WalletState.create({ wallet: { id: rootStellarWallet.id }, status: WalletStatus.PENDING }),
          WalletState.create({ wallet: { id: rootStellarWallet.id }, status: WalletStatus.REGISTERED_IN_STELLAR }),
        ]);

        // Create the root asset in the root admin wallet
        rootAsset = await Asset.insertAndFind(
          {
            issuer: rootStellarWallet,
            ...options.rootAsset,
          },
          { manager: transactionalEntityManager },
        );
      });
    }
  }

  public static async generateRootDomain(options: {
    rootDomain: DomainSchema;
    rootMediator: UserSchema & { password?: string };
  }): Promise<void> {
    let rootDomain = await Domain.findOne({ where: { role: DomainRole.ROOT } });

    if (!rootDomain) {
      await getManager().transaction(async (transactionalEntityManager) => {
        // Create the root domain
        rootDomain = await Domain.insertAndFind(
          { ...(options.rootDomain as any), settings: new DomainSettings() },
          { manager: transactionalEntityManager },
        );

        // Create the root domain mediator user
        const rootMediatorSchema = transactionalEntityManager.create(User, {
          domain: rootDomain,
          ...(options.rootMediator as any),
        });
        await rootMediatorSchema.setPassword(options.rootMediator.password);
        const mediatorInsertResult = await transactionalEntityManager.insert(User, rootMediatorSchema);
        const mediatorId = mediatorInsertResult.identifiers[0].id;

        // Insert mediator with pending state
        await transactionalEntityManager.insert(
          UserState,
          UserState.create({ user: { id: mediatorId }, status: UserStatus.PENDING }),
        );

        // Prepare consumer details
        const { addresses, phones, companyData, ...consumerData } = options.rootMediator.consumer;

        // Validate consumer stuff
        const consumerSchema = Consumer.create({
          ...(consumerData as any),
          companyData,
          user: { id: mediatorId },
        });

        // Create consumer information for mediator
        const consumerInsertResult = await transactionalEntityManager.insert(Consumer, consumerSchema);
        const consumerId = consumerInsertResult.identifiers[0].id;

        const createdAt = Date.now();

        // Create initial consumer states skipping document verification
        await transactionalEntityManager.insert(ConsumerState, [
          ConsumerState.create({
            createdAt: new Date(createdAt - 3),
            consumer: { id: consumerId },
            status: ConsumerStatus.PENDING_DOCUMENTS,
            additionalData: {
              source: 'RootDomainJob',
            } as any,
          }),
          ConsumerState.create({
            createdAt: new Date(createdAt - 2),
            consumer: { id: consumerId },
            status: ConsumerStatus.PROCESSING_DOCUMENTS,
            additionalData: {
              source: 'RootDomainJob',
            } as any,
          }),
          ConsumerState.create({
            createdAt: new Date(createdAt - 1),
            consumer: { id: consumerId },
            status: ConsumerStatus.PROCESSING_WALLETS,
            additionalData: {
              source: 'RootDomainJob',
            } as any,
          }),
        ] as any);

        // Create the root domain mediator wallets
        const mediatorStellarWallet = transactionalEntityManager.create(Wallet, {
          user: { id: mediatorId },
        });

        await transactionalEntityManager.insert(Wallet, mediatorStellarWallet);

        await transactionalEntityManager.insert(
          WalletState,
          WalletState.create({ wallet: { id: mediatorStellarWallet.id }, status: WalletStatus.PENDING }),
        );

        // Get address information from CEP
        if (addresses) {
          const addressSchemas = await Promise.all(
            addresses.map((address) => Address.fromCEP(address.code, { ...address, consumer: { id: consumerId } })),
          );
          await transactionalEntityManager.insert(Address, addressSchemas);
        }

        if (phones) {
          const phoneSchemas = phones.map((phone) => Phone.create({ ...phone, consumer: { id: consumerId } }));
          await transactionalEntityManager.insert(Phone, phoneSchemas);
        }

        // Get updated mediator info
        await transactionalEntityManager.findOne(User, mediatorId, {
          relations: ['states', 'wallets'],
        });
      });
    }
  }

  public static async generateDefaultDomain(options: { defaultDomain: DomainSchema }): Promise<Domain> {
    let defaultDomain = await Domain.findOne({ where: { role: DomainRole.DEFAULT } });

    if (!defaultDomain) {
      await getManager().transaction(async (transactionalEntityManager) => {
        // Create the default domain
        defaultDomain = await Domain.insertAndFind(
          { ...(options.defaultDomain as any), settings: new DomainSettings() },
          { manager: transactionalEntityManager },
        );
      });
    }

    return defaultDomain;
  }
}
