import * as CryptoJS from 'crypto-js';
import { BaseResponse } from 'ts-framework';
import ResponseHelpers from 'ts-framework/dist/components/helpers/response';
import legacyParamsMiddleware from 'ts-framework/dist/components/middlewares/legacyParams';
import { Response, Request } from 'express';
import { MockLogger } from './Logger.util';

export interface RequestSigningHeaders {
  'X-Request-Signature': string;
  'X-Request-Timestamp': string;
}

export interface RequestSigningOptions {
  method: string;
  url: string;
  body?: string;
  timestamp?: string;
}

export class RequestUtil {
  /**
   * Generates headers for request signing.
   */
  public static sign(secret: string, req: RequestSigningOptions): RequestSigningHeaders {
    const now = req.timestamp ? req.timestamp : Date.now();
    const payload = [req.method, req.url, now];

    // Check if should sign body as well
    if ((req.body && req.method.toUpperCase() === 'POST') || req.method.toUpperCase() === 'PUT') {
      payload.push(req.body);
    }

    // Generate signature using HMAC SHA 256
    const signature = CryptoJS.HmacSHA256(payload.join(','), secret);

    return {
      'X-Request-Signature': signature.toString(),
      'X-Request-Timestamp': now.toString(),
    };
  }

  public static patchResponseObject(res: Response): void {
    (res as BaseResponse).success = ResponseHelpers.success(res);
    (res as BaseResponse).error = ResponseHelpers.error(res) as any;
  }

  public static patchRequestObject(req: Request): void {
    legacyParamsMiddleware(req, undefined, () => {});

    (req as any).logger = new MockLogger();

    (req as any)._cache = {};
    (req as any).cache = {
      get: (key: string) => (req as any)._cache[key],
      set: (key: string, obj: any) => ((req as any)._cache[key] = obj),
    };
  }
}
