import { WalletStatus } from '@bacen/base-sdk';
import { EntityManager, getManager } from 'typeorm';
import * as uuid from 'uuid';
import { Wallet, User, WalletState } from '../../api/models';
import { WalletStateMachine } from '../../api/fsm';

export interface GenerateWalletOptions {
  walletId?: string;
  manager?: EntityManager;
  additionalData?: any;
}

export class WalletTestUtil {
  public static async generate(user: User, options: GenerateWalletOptions = {}): Promise<Wallet> {
    const { walletId: id = uuid.v4(), manager = getManager(), additionalData } = options;

    const wallet = await Wallet.findOne(
      { id },
      {
        relations: ['states', 'assetRegistrations', 'issuedAssets'],
      },
    );

    if (wallet) return wallet;

    return manager.transaction(async t => {
      await t.insert(Wallet, Wallet.create({ id, user, additionalData }));
      await t.insert(
        WalletState,
        WalletState.create({
          wallet: { id },
          status: WalletStatus.PENDING,
        }),
      );

      return t.findOne(Wallet, {
        where: { id },
        relations: ['states', 'assetRegistrations', 'issuedAssets'],
      });
    });
  }

  public static async generateAndRegister(
    user: User,
    options: {
      walletId?: string;
      manager?: EntityManager;
    } = {},
  ): Promise<Wallet> {
    const wallet = await this.generate(user, options);

    // Register wallet using FSM
    const fsm = new WalletStateMachine(wallet);
    await fsm.goTo(WalletStatus.REGISTERED_IN_STELLAR);
    await fsm.goTo(WalletStatus.REGISTERED_IN_PROVIDER);
    await fsm.goTo(WalletStatus.PENDING_PROVIDER_APPROVAL);

    // Return updated wallet
    return Wallet.findOne(
      { id: wallet.id },
      {
        relations: ['states', 'assetRegistrations', 'issuedAssets', 'user'],
      },
    );
  }

  public static async generateAndProcess(
    user: User,
    options: {
      walletId?: string;
      manager?: EntityManager;
    } = {},
  ): Promise<Wallet> {
    const wallet = await this.generateAndRegister(user, options);

    // Register wallet using FSM
    const fsm = new WalletStateMachine(wallet);
    await fsm.goTo(WalletStatus.READY);

    // Return updated wallet
    return Wallet.findOne(
      { id: wallet.id },
      {
        relations: ['states', 'assetRegistrations', 'issuedAssets'],
      },
    );
  }

  public static async generateWalletAtState(
    user: User,
    status: WalletStatus,
    additionalData?: Wallet['additionalData'],
  ): Promise<Wallet> {
    const wallet = await WalletTestUtil.generate(user, { additionalData });
    await WalletState.insert({ wallet, status });

    return Wallet.findOne(wallet.id, { relations: ['states'] });
  }
}
