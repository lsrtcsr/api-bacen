import { TransactionStatus, TransactionType, PaymentType } from '@bacen/base-sdk';
import { getManager } from 'typeorm';
import { Transaction, Wallet, OAuthAccessToken, TransactionStateItem, Payment, Asset } from '../../api/models';
import { TransactionTestUtil } from './TransactionUtil.util';
import { TransactionStateMachine } from '../../api/fsm';

export interface GeneratePaymentOptions {
  asset: Asset;
  type?: PaymentType;
  accessToken?: OAuthAccessToken;
  status: TransactionStatus;
  amount: string;
  execute?: boolean;
  additionalData?: any;
}

export class PaymentTestUtil {
  public static async generatePayment(
    source: Wallet,
    destination: Wallet,
    opts: GeneratePaymentOptions,
  ): Promise<Payment> {
    const manager = getManager();
    let transaction: Transaction;

    const payment = await manager.transaction(async tx => {
      transaction = await Transaction.insertAndFind(
        {
          source,
          type: TransactionType.PAYMENT,
          createdBy: opts.accessToken,
          additionalData: opts.additionalData,
        },
        { manager: tx },
      );

      await tx.insert(TransactionStateItem, {
        transaction,
        status: opts.status,
      });

      transaction = await tx.findOne(Transaction, transaction.id, {
        relations: ['states', 'source'],
      });

      const payment = await Payment.insertAndFind(
        {
          transaction,
          destination,
          asset: opts.asset,
          amount: opts.amount,
          type: opts.type,
        },
        { manager: tx },
      );

      return tx.findOne(Payment, payment.id, {
        relations: ['destination', 'transaction', 'transaction.source', 'transaction.states', 'asset'],
      });
    });

    if (opts.execute) {
      const reloadedTransaction = await Transaction.findOne(transaction.id, {
        relations: [
          'source',
          'source.user',
          'states',
          'payments',
          'payments.destination',
          'payments.destination.user',
          'payments.asset',
          'payments.asset.issuer',
        ],
      });

      const fsm = new TransactionStateMachine(reloadedTransaction);
      await fsm.goTo(TransactionStatus.EXECUTED);
    }

    return payment;
  }

  public static async destroyPayment(payment: Payment) {
    if (payment.transaction) {
      await TransactionTestUtil.destroyTransaction(payment.transaction);
    }

    await Payment.delete(payment.id);
  }
}
