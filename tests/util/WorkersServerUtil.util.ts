import Server from 'ts-framework';
import {
  TransactionWorkersServer,
  BillingWorkersServer,
  ConsumerWorkersServer,
  PostbackWorkersServer,
} from '../../api/workers/index';

export enum WorkerType {
  CONSUMER = 'consumer',
  TRANSACTION = 'transaction',
  BILLING = 'billing',
  POSTBACK = 'postback',
}

export class WorkersServerTestUtil {
  private static instances: Map<WorkerType, Server> = new Map();

  public static async initWorker(type: WorkerType.CONSUMER): Promise<ConsumerWorkersServer>;

  public static async initWorker(type: WorkerType.TRANSACTION): Promise<TransactionWorkersServer>;

  public static async initWorker(type: WorkerType.BILLING): Promise<BillingWorkersServer>;

  public static async initWorker(type: WorkerType.POSTBACK): Promise<PostbackWorkersServer>;

  public static async initWorker(type: WorkerType): Promise<Server> {
    let server: Server;

    switch (type) {
      case WorkerType.CONSUMER:
        server = new ConsumerWorkersServer();
        break;
      case WorkerType.BILLING:
        server = new BillingWorkersServer();
        break;
      case WorkerType.POSTBACK:
        server = new PostbackWorkersServer();
        break;
      case WorkerType.TRANSACTION:
        server = new TransactionWorkersServer();
        break;
    }

    await server.onInit();
    this.instances.set(type, server);
    return server;
  }

  public static getInstance(type: WorkerType.CONSUMER): ConsumerWorkersServer;

  public static getInstance(type: WorkerType.TRANSACTION): TransactionWorkersServer;

  public static getInstance(type: WorkerType.BILLING): BillingWorkersServer;

  public static getInstance(type: WorkerType.POSTBACK): PostbackWorkersServer;

  public static getInstance(type: WorkerType): Server {
    if (!this.instances.has(type)) {
      throw new Error(`Worker of type ${type} not initialized yet`);
    }

    return this.instances.get(type)!;
  }
}
