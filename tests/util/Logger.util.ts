import { LoggerInstance } from 'nano-errors';

export type LogLevel =
  | 'error'
  | 'warn'
  | 'help'
  | 'data'
  | 'info'
  | 'debug'
  | 'prompt'
  | 'http'
  | 'verbose'
  | 'input'
  | 'silly'
  | 'emerg'
  | 'alert'
  | 'crit'
  | 'warning'
  | 'notice';

class MockLoggerStorage {
  protected readonly store: Record<LogLevel, { message: string; structuredData: any }[]> = {
    error: [],
    warn: [],
    help: [],
    data: [],
    info: [],
    debug: [],
    prompt: [],
    http: [],
    verbose: [],
    input: [],
    silly: [],
    emerg: [],
    alert: [],
    crit: [],
    warning: [],
    notice: [],
  };

  public get logLevels(): LogLevel[] {
    return Object.keys(this.store) as LogLevel[];
  }

  public add(key: LogLevel, message: string, structuredData: any) {
    this.store[key].push({ message, structuredData });
  }

  public clear(key: LogLevel): void {
    this.store[key] = [];
  }

  public clearAll(): void {
    for (const key in this.store) {
      this.store[key] = [];
    }
  }

  public get(key: LogLevel): { message: string; structuredData: any }[] {
    return this.store[key];
  }

  public count(key: LogLevel): number {
    return this.store[key].length;
  }

  public countAll(): number {
    return Object.values(this.store).reduce((sum, level) => sum + level.length, 0);
  }
}

export class MockLogger {
  protected readonly storage = new MockLoggerStorage();

  public error(message: string, structuredData: any): void {
    this.storage.add('error', message, structuredData);
  }

  public warn(message: string, structuredData: any): void {
    this.storage.add('warn', message, structuredData);
  }

  public help(message: string, structuredData: any): void {
    this.storage.add('help', message, structuredData);
  }

  public data(message: string, structuredData: any): void {
    this.storage.add('data', message, structuredData);
  }

  public info(message: string, structuredData: any): void {
    this.storage.add('info', message, structuredData);
  }

  public debug(message: string, structuredData: any): void {
    this.storage.add('debug', message, structuredData);
  }

  public prompt(message: string, structuredData: any): void {
    this.storage.add('prompt', message, structuredData);
  }

  public http(message: string, structuredData: any): void {
    this.storage.add('http', message, structuredData);
  }

  public verbose(message: string, structuredData: any): void {
    this.storage.add('verbose', message, structuredData);
  }

  public input(message: string, structuredData: any): void {
    this.storage.add('input', message, structuredData);
  }

  public silly(message: string, structuredData: any): void {
    this.storage.add('silly', message, structuredData);
  }

  public emerg(message: string, structuredData: any): void {
    this.storage.add('emerg', message, structuredData);
  }

  public alert(message: string, structuredData: any): void {
    this.storage.add('alert', message, structuredData);
  }

  public crit(message: string, structuredData: any): void {
    this.storage.add('crit', message, structuredData);
  }

  public warning(message: string, structuredData: any): void {
    this.storage.add('warning', message, structuredData);
  }

  public notice(message: string, structuredData: any): void {
    this.storage.add('notice', message, structuredData);
  }

  public clear(key: LogLevel) {
    this.storage.clear(key);
  }

  public clearAll() {
    this.storage.clearAll();
  }

  public count(key: LogLevel) {
    return this.storage.count(key);
  }

  public countAll(key: LogLevel) {
    return this.storage.countAll();
  }

  public get(key: LogLevel) {
    return this.storage.get(key);
  }
}
