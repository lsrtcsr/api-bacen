import { TransactionStatus, TransactionType } from '@bacen/base-sdk';
import { Transaction, Wallet, OAuthAccessToken, TransactionStateItem, Payment, Asset } from '../../api/models';

export interface GenerateTransactionOptions {
  type?: TransactionType;
  status: TransactionStatus;
  accessToken?: OAuthAccessToken;
  additionalData?: any;
}

export class TransactionTestUtil {
  public static async generateForWallet(wallet: Wallet, opts: GenerateTransactionOptions) {
    const transaction = await Transaction.insertAndFind({
      source: wallet,
      type: opts.type,
      createdBy: opts.accessToken,
      additionalData: opts.additionalData,
    });

    await TransactionStateItem.insert({
      transaction,
      status: opts.status,
    });

    return Transaction.findOne(transaction.id, {
      relations: ['states', 'source'],
    });
  }

  public static async destroyTransaction(transaction: Transaction) {
    await Payment.createQueryBuilder()
      .delete()
      .from(Payment)
      .where({ transaction })
      .execute();

    await Transaction.createQueryBuilder()
      .delete()
      .from(TransactionStateItem)
      .where('transaction = :id', { id: transaction.id })
      .execute();

    await Transaction.delete(transaction.id);
  }
}
