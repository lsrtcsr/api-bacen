import MainServer, { MainServerOptions } from '../../api/MainServer';
import Config from '../../config';

export class ServerTestUtil {
  static server?: MainServer;

  static async init(config?: Partial<MainServerOptions>) {
    this.server = new MainServer(config as any);
    await this.server.onInit();
    await this.server.onReady();

    return this.server;
  }
}
