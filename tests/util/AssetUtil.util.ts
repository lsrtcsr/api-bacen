import * as faker from 'faker';
import { UserRole, AssetRegistrationStatus } from '@bacen/base-sdk';
import { Asset, Wallet, AssetRegistration, AssetRegistrationState } from '../../api/models';
import { UserTestUtil } from './UserUtil';

export interface GenerateAssetOptions {
  partial?: Partial<Asset>;
  wallet?: Wallet;
}

export interface LinkWalletToAssetOptions {
  asset: Asset;
  wallet: Wallet;
  status: AssetRegistrationStatus;
}

export class AssetTestUtil {
  public static async generateAsset(opts: GenerateAssetOptions = {}): Promise<Asset> {
    const createWallet = !opts.wallet;
    let wallet: Wallet;

    if (createWallet) {
      const assetUser = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR, createWallet });
      wallet = assetUser.wallets[0];
    } else {
      wallet = opts.wallet;
    }

    const asset = await Asset.insertAndFind({
      issuer: wallet,
      name: faker.finance.currencyName(),
      code: faker.finance.currencyCode(),
      ...opts.partial,
    });

    return Asset.findOne(asset.id, {
      relations: ['issuer', 'issuer.user'],
    });
  }

  public static async destroyAsset(asset: Asset, deleteUser = true) {
    await Asset.delete(asset.id);
    if (deleteUser) await UserTestUtil.destroyUser(asset.issuer.user);
  }

  public static async linkWalletToAsset(options: LinkWalletToAssetOptions): Promise<AssetRegistration> {
    const { asset, wallet, status = AssetRegistrationStatus.PENDING } = options;

    const insertResult = await AssetRegistration.insert({ asset, wallet });
    await AssetRegistrationState.insert({ assetRegistration: { id: insertResult.identifiers[0].id }, status });
    return AssetRegistration.findOne(insertResult.identifiers[0].id, {
      relations: ['states'],
    });
  }

  public static async unlinkWalletToAsset(asset: Asset, wallet: Wallet) {
    const assetRegistrations = await AssetRegistration.find({ asset, wallet });
    await AssetRegistrationState.delete(assetRegistrations.map((ar) => ar.id));
    await AssetRegistration.delete(assetRegistrations.map((ar) => ar.id));
  }
}
