import * as sinon from 'sinon';
import * as path from 'path';
import { Service } from 'ts-framework-common';
import { AssetService, AssetConfig } from '../../api/services';

const DEFAULT_ROOT_ASSET_MOCK = path.join(__dirname, '../__mock__/asset/rootAssetConfig.json');
const DEFAULT_INITIAL_ASSETS_MOCK = path.join(__dirname, '../__mock__/asset/initialAssetsConfig.json');

export interface InstallOptions {
  rootAssetMockPath?: string;
  initialAssetsMockPath?: string;
}

export type UninstalHandle = () => void;

export function stubServiceLifecycle(service: typeof Service): UninstalHandle {
  const onInitStub = sinon.stub(service.prototype, 'onInit').resolves(undefined);
  const onMount = sinon.stub(service.prototype, 'onMount').returns(undefined);
  const onReadyStub = sinon.stub(service.prototype, 'onReady').resolves(undefined);
  const onUnmountStub = sinon.stub(service.prototype, 'onUnmount').resolves(undefined);

  return () => {
    onInitStub.restore();
    onMount.restore();
    onReadyStub.restore();
    onUnmountStub.restore();
  };
}

export class AssetServiceMock {
  public static install(options: InstallOptions = {}): UninstalHandle {
    const {
      rootAssetMockPath = DEFAULT_ROOT_ASSET_MOCK,
      initialAssetsMockPath = DEFAULT_INITIAL_ASSETS_MOCK,
    } = options;

    const rootAssetConfig: AssetConfig = require(rootAssetMockPath);
    const initialAssetsConfig: AssetConfig[] = require(initialAssetsMockPath);

    const lifecycleUninstallHandle = stubServiceLifecycle(AssetService);

    const allInitialAssets = initialAssetsConfig.concat([rootAssetConfig]);
    const authorizableAssets = allInitialAssets.filter(assetConfig => assetConfig.authorizable === true);
    const requiredAssets = initialAssetsConfig
      .filter(assetConfig => assetConfig.required === true)
      .concat([rootAssetConfig]);

    const rootAssetStub = sinon.stub(AssetService.prototype, 'rootAsset').get(() => rootAssetConfig);
    const otherAssetsInCustodyStub = sinon
      .stub(AssetService.prototype, 'otherAssetsInCustody')
      .get(() => initialAssetsConfig);
    const allInitialAssetsStub = sinon.stub(AssetService.prototype, 'allInitialAssets').get(() => allInitialAssets);
    const authorizableAssetsStub = sinon
      .stub(AssetService.prototype, 'authorizableAssets')
      .get(() => authorizableAssets);
    const requiredAssetsStub = sinon.stub(AssetService.prototype, 'requiredAssets').get(() => requiredAssets);

    return () => {
      lifecycleUninstallHandle();
      rootAssetStub.restore();
      otherAssetsInCustodyStub.restore();
      allInitialAssetsStub.restore();
      authorizableAssetsStub.restore();
      requiredAssetsStub.restore();
    };
  }
}
