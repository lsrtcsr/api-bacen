import { UserRole } from '@bacen/base-sdk';
import { EntityManager, getManager, DeepPartial } from 'typeorm';
import * as path from 'path';
import * as fs from 'fs';
import { PlanService } from '../../api/services';
import { PlanRequestSchema } from '../../api/schemas';
import {
  PeriodState,
  Period,
  Invoice,
  InvoiceState,
  Entry,
  Plan,
  PriceItem,
  EntryType,
  Contract,
  ContractState,
  Domain,
  User,
  Service,
} from '../../api/models';

export class BillingTestUtil {
  public static async createServices(options: { manager?: EntityManager } = {}): Promise<Service[]> {
    const servicesData: DeepPartial<Service>[] = JSON.parse(
      fs.readFileSync(path.resolve(process.cwd(), 'tests', 'mocks', 'billing', 'services.json'), 'utf8'),
    );

    const manager = options.manager || getManager();
    const transientServiceList = servicesData.map(serviceData => Service.create(serviceData));
    const insertResult = await manager.insert(Service, transientServiceList);
    const ids: string[] = insertResult.identifiers.map(identifier => identifier.id);

    return await manager.findByIds(Service, ids);
  }

  public static async createPlan(options: {
    contractor: User;
    supplier?: Domain;
    default?: boolean;
    manager?: EntityManager;
  }): Promise<Plan> {
    const json = options.contractor.role === UserRole.CONSUMER ? '-consumer-plan.json' : '-mediator-plan.json';
    const schema: PlanRequestSchema = JSON.parse(
      fs.readFileSync(
        path.resolve(process.cwd(), 'tests', 'mocks', 'billing', (options.default ? 'default' : 'custom').concat(json)),
        'utf8',
      ),
    );

    const supplier = options.supplier || options.contractor.domain;
    const planService = PlanService.getInstance();

    if (options.default) {
      const defaultPlan = await planService.getDefaultPlan(supplier);
      if (defaultPlan) return defaultPlan;
    }

    return await planService.createPlan({ supplier, planData: schema });
  }

  public static async destroyContract(contract: Contract) {
    const states = contract.states || (await ContractState.find({ where: { contract } }));
    if (states && states.length) {
      await ContractState.remove(states);
    }

    await Contract.remove(contract);
  }

  public static async destroyPlan(plan: Plan) {
    const contracts = plan.contracts || (await Contract.find({ where: { plan } }));
    if (contracts && contracts.length) {
      for (const contract of contracts) await this.destroyContract(contract);
    }

    const items = plan.items || (await EntryType.find({ where: { plan } }));
    if (items && items.length) {
      await EntryType.remove(items);
    }

    const priceList = plan.priceList || (await PriceItem.find({ where: { plan } }));
    if (priceList && priceList.length) {
      await PriceItem.remove(priceList);
    }

    await Plan.remove(plan);
  }

  public static async destroyContractorInvoices(contractor: User) {
    const invoices = contractor.invoices || (await Invoice.find({ where: { contractor } }));
    if (invoices && invoices.length) {
      for (const invoice of invoices) await this.destroyInvoice(invoice);
    }
  }

  public static async destroyInvoice(invoice: Invoice) {
    const states = invoice.states || (await InvoiceState.find({ where: { invoice } }));
    if (states && states.length) {
      await InvoiceState.remove(states);
    }

    const periods = invoice.periods || (await Period.find({ where: { invoice } }));
    if (periods && periods.length) {
      for (const period of periods) await this.destroyPeriod(period);
    }

    await Invoice.remove(invoice);
  }

  public static async destroyPeriod(period: Period) {
    const states = period.states || (await PeriodState.find({ where: { period } }));
    if (states && states.length) {
      await PeriodState.remove(states);
    }

    const entries = period.entries || (await Entry.find({ where: { period } }));
    if (entries && entries.length) {
      await Entry.remove(entries);
    }

    await Period.remove(period);
  }
}
