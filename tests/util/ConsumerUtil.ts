import * as faker from 'faker';
import * as cpf from 'cpf';
import {
  AddressType,
  DocumentType,
  ConsumerStatus,
  UserRole,
  BankingType,
  UserStatus,
  AccountType,
} from '@bacen/base-sdk';
import { getManager, EntityManager } from 'typeorm';
import { Address, Document, Phone, Banking, User, Consumer, ConsumerState, Domain, UserState } from '../../api/models';
import { UserTestUtil } from './UserUtil';
import { DomainTestUtil } from './DomainUtil.util';

export interface GenerateConsumerOptions {
  userDomain?: Domain;
  userState?: UserStatus;
  consumerState?: ConsumerStatus;
  type?: AccountType;
  role?: UserRole;
  manager?: EntityManager;
}

export interface GenerateConsumerForUserUptions {
  user: User;
  consumerStatus: ConsumerStatus;
  type: AccountType;
  manager?: EntityManager;
}

export class ConsumerTestUtil {
  public static generateAddress(options: { consumer: Consumer; manager: EntityManager }): Address {
    const manager = options.manager || getManager();

    return manager.create(Address, {
      consumer: options.consumer,
      type: AddressType.HOME,
      street: faker.address.streetAddress(),
      state: faker.address.state(),
      city: faker.address.city(),
      code: faker.address.zipCode(),
      neighborhood: faker.address.county(),
      // Half addresses will have no complement
      complement: Math.random() > 0.5 ? faker.address.secondaryAddress() : undefined,
      country: faker.address.country(),
      number: String(Math.floor(Math.random() * 100)),
    });
  }

  public static async destroyAddress(address: Address) {
    return Address.delete(address);
  }

  public static generateDocument(options: {
    type?: DocumentType;
    consumer: Consumer;
    manager?: EntityManager;
  }): Document {
    const manager = options.manager || getManager();

    return manager.create(Document, {
      consumer: options.consumer,
      number: faker.random.alphaNumeric(12),
      type: options.type || DocumentType.BRL_INDIVIDUAL_REG,
    });
  }

  public static async destroyDocument(document: Document) {
    return Document.delete(document.id);
  }

  public static generatePhone(options: { consumer: Consumer; manager: EntityManager }): Phone {
    const manager = options.manager || getManager();

    const prefix = '99';

    return manager.create(Phone, {
      consumer: options.consumer,
      number: prefix.concat(faker.phone.phoneNumber('#######')),
      code: `0${faker.random.number(99).toString()}`.slice(-2),
      // Half phones will have no extension
      extension: Math.random() > 0.5 ? faker.random.number(99).toString() : undefined,
      verifiedAt: faker.date.past(),
    });
  }

  public static generateBanking(options: {
    id?: string;
    consumer: Consumer;
    taxId: string;
    name: string;
    manager?: EntityManager;
  }): Banking {
    const manager = options.manager || getManager();

    return manager.create(Banking, {
      id: options.id,
      consumer: options.consumer,
      taxId: options.taxId,
      name: options.name,
      account: '615880',
      accountDigit: '0',
      agency: '1',
      agencyDigit: '0',
      bank: '341',
      type: BankingType.CHECKING,
    });
  }

  public static async destroyBanking(banking: Banking) {
    return Banking.delete(banking.id);
  }

  public static async destroyPhone(phone: Phone) {
    return Phone.delete(phone.id);
  }

  public static async generateConsumerForUser(options: GenerateConsumerForUserUptions): Promise<User> {
    const { user, consumerStatus, type, manager = getManager() } = options;
    const { firstName, lastName } = user;

    const taxId = cpf.generate(false);

    const consumer = manager.create(Consumer, { taxId, type, user });

    await manager.insert(Consumer, consumer);
    user.consumer = consumer;

    // Creates between 1 and 5 addresses
    const addresses: Address[] = Array.from({ length: faker.random.number({ min: 1, max: 5 }) }, () =>
      ConsumerTestUtil.generateAddress({ consumer, manager }),
    );
    await manager.insert(Address, addresses);
    consumer.addresses = addresses;

    // Creates between 1 and 5 phones
    const phones: Phone[] = Array.from({ length: faker.random.number({ min: 1, max: 5 }) }, () =>
      ConsumerTestUtil.generatePhone({ consumer, manager }),
    );
    await manager.insert(Phone, phones);
    consumer.phones = phones;

    const banking = ConsumerTestUtil.generateBanking({
      consumer,
      taxId,
      manager,
      name: `${firstName} ${lastName}`,
    });
    await manager.insert(Banking, banking);
    consumer.bankings = [banking];

    const consumerState = manager.create(ConsumerState, {
      status: consumerStatus,
      consumer,
    });
    await manager.insert(ConsumerState, consumerState);
    consumer.states = [consumerState];

    return user;
  }

  public static async generateConsumer(options: GenerateConsumerOptions = {}): Promise<User> {
    const {
      userDomain,
      userState: userStatus = UserStatus.PENDING,
      consumerState = ConsumerStatus.PENDING_DOCUMENTS,
      role = UserRole.CONSUMER,
      type = AccountType.PERSONAL,
      manager = getManager(),
    } = options;

    let domain = userDomain;
    if (!userDomain) {
      domain = await DomainTestUtil.generateDomain({ manager });
    }

    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();

    const user = manager.create(User, {
      domain,
      firstName,
      lastName,
      email: faker.internet.email(),
      username: faker.internet.userName(),
      role,
    });

    await manager.insert(User, user);

    const userState = manager.create(UserState, {
      user,
      status: userStatus,
    });
    await manager.insert(UserState, userState);
    user.states = [userState];

    return this.generateConsumerForUser({ user, consumerStatus: consumerState, type, manager });
  }

  public static async destroyConsumer(user: User, destroyUser = true) {
    const consumer = user.consumer || (await Consumer.findOne({ where: { user } }));

    if (!consumer) return;

    const addresses = consumer.addresses || (await Address.find({ consumer }));
    if (addresses && addresses.length) await Address.delete(addresses.map((addresses) => addresses.id));

    const documents = consumer.documents || (await Document.find({ consumer }));
    if (documents && documents.length) await Document.delete(documents.map((documents) => documents.id));

    const phones = consumer.phones || (await Phone.find({ consumer }));
    if (phones && phones.length) await Phone.delete(phones.map((phones) => phones.id));

    const bankings = consumer.bankings || (await Banking.find({ consumer }));
    if (bankings && bankings.length) await Banking.delete(bankings.map((bankings) => bankings.id));

    const states = consumer.states || (await ConsumerState.find({ consumer }));
    if (states && states.length) await ConsumerState.delete(states.map((states) => states.id));

    await Consumer.delete(consumer.id);

    user.consumer = null;

    if (destroyUser) await UserTestUtil.destroyUser(user);
  }
}
