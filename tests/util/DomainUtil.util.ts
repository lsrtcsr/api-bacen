import { DomainRole } from '@bacen/base-sdk';
import * as faker from 'faker';
import { DeepPartial, EntityManager, getManager } from 'typeorm';
import { Domain, DomainSettingsLocks } from '../../api/models';

export interface DomainTestUtilOptions {
  partial?: DeepPartial<Domain>;
  manager?: EntityManager;
}

export class DomainTestUtil {
  public static async generateDomain(options: DomainTestUtilOptions = {}) {
    const { partial = {}, manager = getManager() } = options;

    const domain = await manager.create(Domain, {
      name: 'Test',
      postbackUrl: faker.internet.domainName(),
      urls: Array.from({ length: faker.random.number(5) }, () => faker.internet.domainName()),
      settings: { locks: new DomainSettingsLocks() },
      ...partial,
    });
    await manager.insert(Domain, domain);

    return domain;
  }

  public static async destroyDomain(domain: Domain) {
    await Domain.delete(domain.id);
  }
}
