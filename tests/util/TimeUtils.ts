import * as Table from 'cli-table3';

type MaybePromise<T> = T | Promise<T>;

export class TimeTestUtil {
  public readonly name: string;

  private runs: Record<string, number[]>;

  private constructor(name: string) {
    this.name = name;
    this.runs = {};
  }

  public static createSuite(name: string) {
    return new TimeTestUtil(name);
  }

  public async addRun<R>(name: string, fn: () => MaybePromise<R>): Promise<R> {
    const [result, time] = await TimeTestUtil.time(fn);
    this.runs[name] = this.runs[name] || [];
    this.runs[name].push(time);

    return result;
  }

  public report() {
    const results = Object.entries(this.runs).map(([name, times]) => {
      const size = times.length;
      const avg = times.reduce((acc, time) => time + acc, 0) / size;
      const stdDev = Math.sqrt(times.reduce((acc, time) => acc + Math.pow(time - avg, 2), 0) / size);

      return [name, size, Math.min(...times), Math.max(...times), avg, stdDev];
    });

    const table = new Table({
      head: ['name', 'samples', 'min (ms)', 'max (ms)', 'avg (ms)', 'stdDev (ms)'],
    });

    table.push(...(results as any));

    return table.toString();
  }

  public static async time<R>(fn: () => MaybePromise<R>): Promise<[R, number]> {
    const start = Date.now();
    const result = await fn();
    const end = Date.now();

    return [result, end - start];
  }
}
