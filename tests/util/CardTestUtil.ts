import { CardStatus } from '@bacen/base-sdk';
import { EntityManager, getManager, DeepPartial } from 'typeorm';
import { Card, Wallet } from '../../api/models';

export interface GenerateCardOptions {
  wallet: Wallet;
  virtual: boolean;
  status?: CardStatus;
  partial?: DeepPartial<Card>;
  manager?: EntityManager;
}

export class CardTestUtil {
  public static async generateCard(options: GenerateCardOptions): Promise<Card> {
    const { wallet, virtual, status = CardStatus.AVAILABLE, partial = {}, manager = getManager() } = options;

    return Card.insertAndFind(
      {
        wallet,
        virtual,
        status,
        ...partial,
      },
      { manager },
    );
  }

  public static async destroy(card: Card): Promise<void> {
    await Card.delete(card.id);
  }
}
