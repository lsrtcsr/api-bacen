import * as faker from 'faker';
import { getManager, EntityManager } from 'typeorm';
import { LegalTermType } from '@bacen/base-sdk';
import { LegalTerm, LegalTermAcceptance } from '../../api/models';

interface GenerateOptions {
  partial?: Partial<LegalTerm>;
  manager?: EntityManager;
}

export class LegalTermTestUtil {
  public static generate(
    type: LegalTermType,
    { partial, manager = getManager() }: GenerateOptions = {},
  ): Promise<LegalTerm> {
    return LegalTerm.insertAndFind(
      {
        type,
        name: faker.name.findName(),
        body: faker.lorem.text(),
        ...partial,
      },
      {
        manager,
      },
    );
  }

  public static async destory(term: LegalTerm): Promise<void> {
    const acceptances = term.acceptances || (await LegalTermAcceptance.find({ where: { legalTerm: term } }));

    await LegalTermAcceptance.remove(acceptances);
    await term.remove();
  }
}
