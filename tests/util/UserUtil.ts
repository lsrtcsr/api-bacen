import { UserRole, UserStatus } from '@bacen/base-sdk';
import * as faker from 'faker';
import { Any, EntityManager, getManager, In } from 'typeorm';
import {
  Domain,
  OAuthAccessToken,
  OAuthRefreshToken,
  Transaction,
  User,
  UserState,
  Wallet,
  Base2HandledTransaction,
  Payment,
  AssetRegistration,
  Asset,
  RequestLog,
} from '../../api/models';
import { ConsumerTestUtil } from './ConsumerUtil';
import { DomainTestUtil } from './DomainUtil.util';
import { WalletTestUtil } from './WalletUtil.util';

export interface GenerateUserOptions {
  role?: UserRole;
  userDomain?: Domain;
  createWallet?: boolean;
  userPartial?: Partial<User>;
  relations?: string[];
  manager?: EntityManager;
}

export class UserTestUtil {
  public static async generateUser(options: GenerateUserOptions = {}) {
    const { role, userDomain, createWallet, userPartial, manager = getManager() } = options;

    let domain = userDomain;
    if (!userDomain) {
      domain = await DomainTestUtil.generateDomain({ manager });
    }

    const user = manager.create(User, {
      domain,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      username: faker.internet.userName(),
      role: role || UserRole.AUDIT,
      ...userPartial,
    });
    await user.setPassword('satoshi');
    await manager.insert(User, user);

    if (createWallet) {
      const wallet = await WalletTestUtil.generateAndRegister(user, { manager });
      user.wallets = [wallet];
    }

    await manager.insert(UserState, {
      user,
      status: UserStatus.PENDING,
    });

    return user;
  }

  public static async destroyUser(user: User) {
    if (user.role === UserRole.CONSUMER && user.consumer) await ConsumerTestUtil.destroyConsumer(user, false);

    const wallets = await Wallet.find({ user });
    const oauthAccessToken = await OAuthAccessToken.findOne({ where: { userId: user.id } });
    const walletIds = wallets.map((wallet) => wallet.id);

    if (wallets && wallets.length) {
      await Payment.createQueryBuilder()
        .delete()
        .from(Payment)
        .where({ destination: In(walletIds) })
        .execute();

      await Transaction.createQueryBuilder()
        .delete()
        .from(Transaction)
        .where({ source: Any(walletIds) })
        .execute();

      await Base2HandledTransaction.createQueryBuilder()
        .delete()
        .from(Base2HandledTransaction)
        .where({ wallet: In(walletIds) })
        .execute();

      await AssetRegistration.createQueryBuilder()
        .delete()
        .where({ wallet: In(walletIds) })
        .execute();

      await Asset.createQueryBuilder()
        .delete()
        .where({ issuer: In(walletIds) })
        .execute();

      await Wallet.createQueryBuilder()
        .delete()
        .where({ id: In(walletIds) })
        .execute();
    }

    await UserState.createQueryBuilder().delete().from(UserState).where('user = :id', { id: user.id }).execute();

    if (oauthAccessToken) {
      await RequestLog.createQueryBuilder()
        .delete()
        .from(RequestLog)
        .where('token = :token', { token: oauthAccessToken.id })
        .execute();
    }

    await OAuthRefreshToken.createQueryBuilder()
      .delete()
      .from(OAuthRefreshToken)
      .where('user = :id', { id: user.id })
      .execute();

    await OAuthAccessToken.createQueryBuilder()
      .delete()
      .from(OAuthAccessToken)
      .where('user = :id', { id: user.id })
      .execute();

    await OAuthAccessToken.createQueryBuilder().delete().from(UserState).where('user = :id', { id: user.id }).execute();

    const userWithDomain = await User.findOne({ where: { id: user.id }, relations: ['domain'] });
    if (userWithDomain?.domain) {
      try {
        await DomainTestUtil.destroyDomain(userWithDomain.domain);
      } catch (e) {
        // Domain may have been deleted already, log it and carry on
        console.error(e);
      }
    }

    await User.delete(user.id);
  }
}
