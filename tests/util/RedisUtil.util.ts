import * as redis from 'redis';

export class RedisUtil {
  private static _redisClient: redis.RedisClient;

  public static get redisClient() {
    if (RedisUtil._redisClient) return RedisUtil._redisClient;

    RedisUtil._redisClient = redis.createClient(6379, 'localhost');

    RedisUtil._redisClient.on('error', err => console.error('ERR:REDIS:', err));

    return RedisUtil._redisClient;
  }

  private static callback<T>(done: (reply: T) => void) {
    return (err: Error, reply: T) => {
      if (err) throw err;

      done(reply);
    };
  }

  public static get(key: string) {
    return new Promise(done => {
      RedisUtil.redisClient.get(key, RedisUtil.callback(done));
    });
  }

  public static set(key: string, value: any) {
    return new Promise(done => {
      RedisUtil.redisClient.set(key, value, RedisUtil.callback(done));
    });
  }

  public static exists(key: string) {
    return new Promise(done => {
      RedisUtil.redisClient.exists(key, RedisUtil.callback(done));
    });
  }

  public static del(key: string) {
    return new Promise(done => {
      RedisUtil.redisClient.del(key, RedisUtil.callback(done));
    });
  }
}
