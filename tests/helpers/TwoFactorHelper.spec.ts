import { TwoFactorHelper } from '../../api/helpers';

import moment = require('moment');

describe('api.helpers.TwoFactorHelper', () => {
  it('should generate an otp secret an validate a token', async () => {
    const secret = TwoFactorHelper.generateSecretSeed();
    const token = TwoFactorHelper.generateToken(secret.base32, 6);
    expect(TwoFactorHelper.verifyToken(token, secret.base32)).toBe(true);
  });

  it('should deny an invalid otp secret in the token validation', async () => {
    const realSecret = TwoFactorHelper.generateSecretSeed();
    const realToken = TwoFactorHelper.generateToken(realSecret.base32, 6);

    expect(TwoFactorHelper.verifyToken(realToken, realSecret.base32)).toBe(true);
    expect(TwoFactorHelper.verifyToken('123456', realSecret.base32)).toBe(false);
  });

  it('should deny an old otp secret in the token validation', async () => {
    const realSecret = TwoFactorHelper.generateSecretSeed();
    const oldToken = TwoFactorHelper.generateTokenFromDate(
      realSecret.base32,
      moment()
        .subtract(1, 'days')
        .toDate(),
      6,
    );
    expect(TwoFactorHelper.verifyToken(oldToken, realSecret.base32)).toBe(false);
  });
});
