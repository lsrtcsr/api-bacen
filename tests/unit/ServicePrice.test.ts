import * as path from 'path';
import * as fs from 'fs';
import { ConsumerTestUtil, RootUtil, BillingTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { PlanService, BillingService } from '../../api/services';
import { PlanServiceSchema } from '../../api/schemas';
import { User, Plan, Domain, EventType } from '../../api/models';

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;
  let contractor: User;
  let defaultPlan: Plan;
  let customPlan: Plan;
  let supplier: Domain;

  let billingService: BillingService;
  let planService: PlanService;
  let updatedServices: PlanServiceSchema[];

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    planService = PlanService.getInstance();
    billingService = BillingService.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });

    await BillingTestUtil.createServices();
    defaultPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: true });
    customPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: false });

    await PlanService.getInstance().subscribe({
      prepaid: true,
      user: contractor,
    });

    updatedServices = JSON.parse(
      fs.readFileSync(
        path.resolve(process.cwd(), 'tests', 'mocks', 'billing', 'consumer-plan-update-items.json'),
        'utf8',
      ),
    );
  });

  afterAll(async () => {
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (defaultPlan) await BillingTestUtil.destroyPlan(defaultPlan);
    if (customPlan) await BillingTestUtil.destroyPlan(customPlan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Subscribe a default plan in a prepaid mode', async done => {
    const { logger } = Config.server;

    try {
      const serviceTypes = await defaultPlan.getServiceTypes(EventType.TRANSACTION);
      for (const serviceType of serviceTypes) {
        const defaultPlanPrice = await defaultPlan.getCurrentPrice(serviceType);
        const currentPrice = await (billingService as any).getCurrentPrice(contractor.id, serviceType);

        expect(defaultPlanPrice.amount).toEqual(currentPrice.amount);
      }
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it('Subscribe a custom plan ', async done => {
    const { logger } = Config.server;

    try {
      await planService.subscribe({
        prepaid: true,
        user: contractor,
      });

      const serviceTypes = await customPlan.getServiceTypes(EventType.TRANSACTION);
      for (const serviceType of serviceTypes) {
        const customPlanPrice = await customPlan.getCurrentPrice(serviceType);
        const currentPrice = await (billingService as any).getCurrentPrice(contractor.id, serviceType);

        expect(customPlanPrice.amount).toEqual(currentPrice.amount);
      }
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
