import { HttpError, BaseRequest } from 'ts-framework';
import { parseDateOr400, extractRangeFromQuery } from '../../api/utils/DateUtil';

describe('api.util.DateUtil', () => {
  describe('parseDateOr400', () => {
    it('parses a valid date', () => {
      const parsedDate = parseDateOr400('2020-01-01', { fieldName: 'date' });
      expect(parsedDate).toStrictEqual(new Date('2020-01-01'));
    });

    it('parses a valid date in the specified format', () => {
      const parsedDate = parseDateOr400('2020-13-01', { fieldName: 'date' }, 'YYYY-DD-MM');
      expect(parsedDate).toStrictEqual(new Date('2020-01-13'));
    });

    it('throws a 400 error if date is invalid', () => {
      let err: HttpError;
      try {
        parseDateOr400('2020-02-31', { fieldName: 'date' });
      } catch (e) {
        err = e;
      }

      expect(err).toBeInstanceOf(HttpError);
      expect(err.status).toBe(400);
    });
  });

  describe('extractRangeFromQuery', () => {
    it('handles start and end absent', () => {
      const { start, end } = extractRangeFromQuery({ query: {} } as BaseRequest);

      expect(start).toBeUndefined();
      expect(end).toBeUndefined();
    });

    it('parses start and handles end absent', () => {
      const { start, end } = extractRangeFromQuery({ query: { start: '2020-01-01' } } as BaseRequest);

      expect(start).toStrictEqual(new Date('2020-01-01'));
      expect(end).toBeUndefined();
    });

    it('parses end and handles start absent', () => {
      const { start, end } = extractRangeFromQuery({ query: { end: '2020-01-01' } } as BaseRequest);

      expect(start).toBeUndefined();
      expect(end).toStrictEqual(new Date('2020-01-01'));
    });

    it('parses both start and end', () => {
      const { start, end } = extractRangeFromQuery({
        query: { start: '2020-01-01', end: '2020-02-01' },
      } as BaseRequest);

      expect(start).toStrictEqual(new Date('2020-01-01'));
      expect(end).toStrictEqual(new Date('2020-02-01'));
    });

    it('throws if start is malformed', () => {
      let err: HttpError;
      try {
        extractRangeFromQuery({ query: { start: '2020' } } as BaseRequest);
      } catch (e) {
        err = e;
      }

      expect(err).toBeInstanceOf(HttpError);
      expect(err.status).toBe(400);
    });

    it('throws if end is malformed', () => {
      let err: HttpError;
      try {
        extractRangeFromQuery({ query: { end: '2020' } } as BaseRequest);
      } catch (e) {
        err = e;
      }

      expect(err).toBeInstanceOf(HttpError);
      expect(err.status).toBe(400);
    });

    it('throws if start and end are malformed', () => {
      let err: HttpError;
      try {
        extractRangeFromQuery({ query: { start: '2019', end: '2020' } } as BaseRequest);
      } catch (e) {
        err = e;
      }

      expect(err).toBeInstanceOf(HttpError);
      expect(err.status).toBe(400);
    });
  });
});
