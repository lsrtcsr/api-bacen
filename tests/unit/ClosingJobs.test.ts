import * as moment from 'moment';
import * as sinon from 'sinon';
import { BillingTestUtil, RootUtil, ConsumerTestUtil, UserTestUtil } from '../util';
import Config from '../../config';
import { PlanService, BillingService, PeriodPipelinePublisher, InvoicePipelinePublisher } from '../../api/services';
import { InvoicePeriodClosingJob, InvoiceClosingJob } from '../../api/jobs';
import { BillingWorkersServer } from '../../api/workers';
import { User, Plan, Domain, Period, Invoice } from '../../api/models';
import { WorkersServerTestUtil, WorkerType } from '../util/WorkersServerUtil.util';

jest.setTimeout(300 * 1000);

describe.skip('Invoice and period closing jobs test', () => {
  let workersServer: BillingWorkersServer;

  let firstContractor: User;
  let secondContractor: User;
  let defaultPlan: Plan;
  let customPlan: Plan;
  let supplier: Domain;

  let periodPipelinePublisher: PeriodPipelinePublisher;
  let invoicePipelinePublisher: InvoicePipelinePublisher;

  let billingService: BillingService;

  let publishPeriodToPipeline: sinon.SinonSpy;
  let publishInvoiceToPipeline: sinon.SinonSpy;

  beforeAll(async () => {
    workersServer = await WorkersServerTestUtil.initWorker(WorkerType.BILLING);

    billingService = BillingService.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    firstContractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });
    secondContractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });

    await BillingTestUtil.createServices();
    defaultPlan = await BillingTestUtil.createPlan({ supplier, contractor: firstContractor, default: true });
    customPlan = await BillingTestUtil.createPlan({ supplier, contractor: secondContractor, default: false });

    periodPipelinePublisher = PeriodPipelinePublisher.getInstance();
    publishPeriodToPipeline = sinon.spy(periodPipelinePublisher, 'send');

    invoicePipelinePublisher = InvoicePipelinePublisher.getInstance();
    publishInvoiceToPipeline = sinon.spy(invoicePipelinePublisher, 'send');

    await PlanService.getInstance().subscribe({
      prepaid: true,
      user: firstContractor,
    });

    await PlanService.getInstance().subscribe({
      plan: customPlan,
      user: secondContractor,
      prepaid: true,
    });
  });

  afterAll(async () => {
    if (publishPeriodToPipeline) publishPeriodToPipeline.restore();
    if (publishInvoiceToPipeline) publishInvoiceToPipeline.restore();

    if (firstContractor) await UserTestUtil.destroyUser(firstContractor);
    if (secondContractor) await UserTestUtil.destroyUser(secondContractor);
    if (defaultPlan) await BillingTestUtil.destroyPlan(defaultPlan);
    if (workersServer) await workersServer.close();
  });

  it.skip("Send to the state machine's pipeline the periods whose scheduled closing date was reached", async done => {
    const { logger } = Config.server;

    try {
      const now = moment();

      const periodClosingJob = new InvoicePeriodClosingJob({ logger });

      const firstContractorCurrentPeriod = await (billingService as any).getCurrentPeriod(firstContractor.id, now);
      await Period.update(firstContractorCurrentPeriod.id, { closureScheduledFor: now });

      const secondContractorCurrentPeriod = await (billingService as any).getCurrentPeriod(secondContractor.id, now);
      await Period.update(secondContractorCurrentPeriod.id, { closureScheduledFor: now });

      await periodClosingJob.run(workersServer);

      const periodIdList = publishPeriodToPipeline.getCalls().map(call => (call.args[0] as any).id);
      expect(periodIdList).toContain(firstContractorCurrentPeriod.id);
      expect(periodIdList).toContain(secondContractorCurrentPeriod.id);
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it.skip("Send to the state machine's pipeline the invoices whose scheduled closing date was reached", async done => {
    const { logger } = Config.server;

    try {
      const now = moment();

      const firstContractorCurrentInvoice = (await (billingService as any).getCurrentPeriod(firstContractor.id, now))
        .invoice;
      await Invoice.update(firstContractorCurrentInvoice.id, { closureScheduledFor: now });

      const secondContractorCurrentInvoice = (await (billingService as any).getCurrentPeriod(secondContractor.id, now))
        .invoice;
      await Invoice.update(secondContractorCurrentInvoice.id, { closureScheduledFor: now });

      const invoiceClosingJob = new InvoiceClosingJob({ logger });
      await invoiceClosingJob.run(workersServer);

      const invoiceIdList = publishInvoiceToPipeline.getCalls().map(call => (call.args[0] as any).id);
      expect(invoiceIdList).toContain(firstContractorCurrentInvoice.id);
      expect(invoiceIdList).toContain(secondContractorCurrentInvoice.id);
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
