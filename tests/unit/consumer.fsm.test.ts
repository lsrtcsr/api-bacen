import { ConsumerStatus } from '@bacen/base-sdk';
import { Bucket } from '@google-cloud/storage';
import * as sinon from 'sinon';
import { ConsumerStateMachine } from '../../api/fsm/consumer/ConsumerStateMachine';
import MainServer from '../../api/MainServer';
import { Postback, Wallet } from '../../api/models';
import { IssueHandler, PostbackDeliveryPublisher } from '../../api/services';
import { ServerTestUtil, ConsumerTestUtil, WalletTestUtil } from '../util';

jest.setTimeout(180 * 1000);

describe.skip('Test Process Consumer action', () => {
  let server: MainServer;

  let user;
  let wallet: Wallet;
  let stub: sinon.SinonStub;
  let fsm: ConsumerStateMachine;
  let postbackStub: sinon.SinonStub;

  beforeAll(async () => {
    server = await ServerTestUtil.init();
    user = await ConsumerTestUtil.generateConsumer();
    wallet = await WalletTestUtil.generateAndRegister(user);
    fsm = new ConsumerStateMachine(user);
  });

  afterAll(async () => {
    if (user) {
      await ConsumerTestUtil.destroyConsumer(user);
    }
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  beforeEach(async () => {
    if (stub) {
      stub.restore();
    }
    if (postbackStub) {
      postbackStub.restore();
    }
  });

  it('Can change state from pending to ready', async () => {
    stub = sinon.stub(PostbackDeliveryPublisher.getInstance(), 'send').returns(Promise.resolve<any>({}));

    postbackStub = sinon.stub(Postback, 'fromUser').returns(Promise.resolve<any>({}));

    await fsm.afterTransition(ConsumerStatus.PENDING_DOCUMENTS, ConsumerStatus.READY, null);

    expect(stub.calledOnce).toBeTruthy();
    expect(postbackStub.calledOnce).toBeTruthy();
    // ConsumerStatus.READY was one of the arguments
    expect(postbackStub.getCall(0).args.indexOf('ready') > -1).toBeTruthy();
  });

  it('Should create an anomaly when unexpected result happens', async () => {
    stub = sinon.stub(IssueHandler.getInstance(), 'handle').returns(Promise.resolve<any>({}));

    await fsm.afterTransition(ConsumerStatus.PENDING_DOCUMENTS, ConsumerStatus.BLOCKED, null);
    expect(stub.calledOnce).toBeTruthy();
  });

  it('Should create an anomaly when unexpected result happens', async () => {
    stub = sinon.stub(IssueHandler.getInstance(), 'handle').returns(Promise.resolve<any>({}));

    await fsm.afterTransition(ConsumerStatus.PENDING_DOCUMENTS, ConsumerStatus.PROVIDER_FAILED, null);
    expect(stub.calledOnce).toBeTruthy();
  });

  it('Should create an anomaly when unexpected result happens', async () => {
    stub = sinon.stub(IssueHandler.getInstance(), 'handle').returns(Promise.resolve<any>({}));

    await fsm.afterTransition(ConsumerStatus.PENDING_DOCUMENTS, ConsumerStatus.SUSPENDED, null);
    expect(stub.calledOnce).toBeTruthy();
  });

  it('Can store KYC data at Gcloud', async () => {
    stub = sinon.stub(Bucket.prototype, 'upload').returns(undefined as any);

    await fsm.afterTransition(ConsumerStatus.PENDING_DOCUMENTS, ConsumerStatus.PROCESSING_WALLETS, {
      data: 'fakedata',
    });
    expect(stub.calledOnce).toBeTruthy();
  });
});
