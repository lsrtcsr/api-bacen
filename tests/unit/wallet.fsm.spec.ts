import { WalletStatus } from '@bacen/base-sdk';
import * as sinon from 'sinon';
import { WalletStateMachine } from '../../api/fsm/wallet/WalletStateMachine';
import MainServer from '../../api/MainServer';
import { WalletState } from '../../api/models';
import { ServerTestUtil, WalletTestUtil, UserTestUtil } from '../util';

jest.setTimeout(180 * 1000);

describe.skip('Test Wallet State Machine', () => {
  let server: MainServer;
  let wallet;
  let user;
  let walletStateMachine: WalletStateMachine;

  beforeAll(async () => {
    server = await ServerTestUtil.init();
    user = await UserTestUtil.generateUser({});
    wallet = await WalletTestUtil.generateAndRegister(user);
    walletStateMachine = new WalletStateMachine(wallet);
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Can change state', async () => {
    const stub = sinon.stub(WalletState, 'insertAndFind');
    walletStateMachine.afterTransition(WalletStatus.READY, WalletStatus.REGISTERED_IN_STELLAR, {});

    expect(WalletState.insertAndFind['calledOnce']).toBeTruthy();
  });
});
