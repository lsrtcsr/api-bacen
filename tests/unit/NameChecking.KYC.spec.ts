import * as path from 'path';
import * as fs from 'fs';
import { matches } from '../../api/utils/StringUtil';

jest.setTimeout(180 * 1000);

interface SampleSchema {
  referenceName: string;
  shouldPass: string[];
  shouldFail: string[];
}

describe('Name checking in KYC flow testing', () => {
  let samples: SampleSchema[];

  const passed: string[] = [];
  const failed: string[] = [];

  beforeAll(async () => {
    samples = JSON.parse(
      fs.readFileSync(path.resolve(process.cwd(), 'tests', '__mock__', 'user', 'name-matching.json'), 'utf8'),
    );
  });

  afterAll(async () => {
    if (passed.length) console.log('Samples that matched but should fail', { ...passed });
    if (failed.length) console.log('Samples that failed but should match: ', { ...failed });
  });

  it('Matches all samples that should pass', async () => {
    for (const sample of samples) {
      sample.shouldPass.forEach(inputStr => {
        if (!matches(inputStr, sample.referenceName)) failed.push(inputStr);
      });
    }

    expect(failed.length).toEqual(0);
  });

  it('Check samples that should not pass', async () => {
    for (const sample of samples) {
      sample.shouldFail.forEach(inputStr => {
        if (matches(inputStr, sample.referenceName)) passed.push(inputStr);
      });
    }

    expect(passed.length).toEqual(0);
  });
});
