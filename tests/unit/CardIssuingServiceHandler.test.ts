import * as sinon from 'sinon';
import { ServiceType } from '@bacen/base-sdk';
import { RootUtil, ConsumerTestUtil, WalletTestUtil, CardTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { EventHandlingGateway } from '../../api/services';
import { ServiceConsumptionData } from '../../api/schemas';
import { User, Wallet, Domain } from '../../api/models';

import moment = require('moment');

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;

  let contractor: User;
  let wallet: Wallet;
  let supplier: Domain;

  let eventHandlingGateway: EventHandlingGateway;
  let eventPublisher: sinon.SinonSpy;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    eventHandlingGateway = EventHandlingGateway.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });
    wallet = await WalletTestUtil.generateAndRegister(contractor);

    eventPublisher = sinon.spy(eventHandlingGateway, 'publish');
  });

  afterAll(async () => {
    if (eventPublisher) eventPublisher.restore();
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it.skip('Generete an physical card issuing event for service charge', async done => {
    const { logger } = Config.server;
    const operationDate = moment();
    try {
      const card = await CardTestUtil.generateCard({ wallet, virtual: true });

      await eventHandlingGateway.process(ServiceType.PHYSICAL_CARD_ISSUING, {
        operationDate,
        user: contractor,
        serviceData: card,
      });

      const serviceData: ServiceConsumptionData = eventPublisher.getCall(0).args[0];
      expect(serviceData.type).toEqual(ServiceType.PHYSICAL_CARD_ISSUING);
      expect(serviceData.userId).toEqual(contractor.id);
      expect(serviceData.eventDate).toEqual(operationDate);
      expect(serviceData.settled).toBeFalsy();
      expect(serviceData.liability).toBeUndefined();
      expect(serviceData.payment).toBeUndefined();
      expect(serviceData.calculationRule).toBeUndefined();
      expect(serviceData.event).toHaveProperty('serviceData.id', card.id);
    } catch (exception) {
      logger.error('Account opening event generation failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it.skip('Generete an virtual card issuing event for service charge', async done => {
    const { logger } = Config.server;
    const operationDate = moment();
    try {
      const card = await CardTestUtil.generateCard({ wallet, virtual: false });

      await eventHandlingGateway.process(ServiceType.VIRTUAL_CARD_ISSUING, {
        operationDate,
        user: contractor,
        serviceData: card,
      });

      const serviceData: ServiceConsumptionData = eventPublisher.getCall(1).args[0];
      expect(serviceData.type).toEqual(ServiceType.VIRTUAL_CARD_ISSUING);
      expect(serviceData.userId).toEqual(contractor.id);
      expect(serviceData.eventDate).toEqual(operationDate);
      expect(serviceData.settled).toBeFalsy();
      expect(serviceData.liability).toBeUndefined();
      expect(serviceData.payment).toBeUndefined();
      expect(serviceData.calculationRule).toBeUndefined();
      expect(serviceData.event).toHaveProperty('serviceData.id', card.id);
    } catch (exception) {
      logger.error('Account opening event generation failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
