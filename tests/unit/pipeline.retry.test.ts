import * as sinon from 'sinon';
import {
  UserStatus,
  ConsumerStatus,
  UserRole,
  TransactionType,
  PaymentType,
  TransactionStatus,
} from '@bacen/base-sdk';
import { ConsumerPipelineJob, MediatorPipelineJob, TransactionPipelineJob } from '../../api/jobs';
import QueueConfig from '../../config/queue.config';
import { AMQPService } from '../../api/services';
import MainServer from '../../api/MainServer';
import { User, Transaction, TransactionStateItem, Payment } from '../../api/models';
import { Issue } from '../../api/models/issue/Issue';
import { FailTransactionAction } from '../../api/fsm';
import { ServerTestUtil, ConsumerTestUtil, WalletTestUtil } from '../util';

jest.setTimeout(3 * 60 * 1000);

describe.skip('Test pipeline retry system', () => {
  let ack: Function;
  let nack: Function;
  let server: MainServer;

  const emptyMessage = { properties: {}, fields: {} };

  beforeAll(async done => {
    server = await ServerTestUtil.init();
    sinon.stub(AMQPService, 'getInstance').returns({} as any);
    done();
  });

  beforeEach(() => {
    ack = sinon.spy();
    nack = sinon.spy();
  });

  afterEach(() => {
    ack = null;
    nack = null;
  });

  afterAll(async done => {
    await server.close();
    server = undefined;
    done();
  });

  it('Should retry only the allowed amount of times on the Consumer Pipeline', async done => {
    const consumerPipeline = new ConsumerPipelineJob({});
    let user = await ConsumerTestUtil.generateConsumer();

    for (let i = 0; i <= QueueConfig.consumer.maxRetries; i += 1) {
      await consumerPipeline.onFail(user, emptyMessage as any, { ack, nack } as any, {});
      user = await User.findOne(user.id, { relations: ['states', 'consumer', 'consumer.states'] });
    }

    expect(user.status).toBe(UserStatus.FAILED);
    expect(user.consumer.getStates()[1].additionalData.retries).toBe(QueueConfig.consumer.maxRetries);
    expect(user.consumer.status).toBe(ConsumerStatus.PROVIDER_FAILED);

    await ConsumerTestUtil.destroyConsumer(user);

    user = null;

    done();
  });

  it('Should retry only the allowed amount of times on the Mediator Pipeline', async done => {
    const mediatorPipeline = new MediatorPipelineJob({});
    let user = await ConsumerTestUtil.generateConsumer();
    await User.update(user.id, { role: UserRole.MEDIATOR });
    user.role = UserRole.MEDIATOR;

    for (let i = 0; i <= QueueConfig.mediator.maxRetries; i += 1) {
      await mediatorPipeline.onFail(user, emptyMessage as any, { ack, nack } as any, {});
      user = await User.findOne(user.id, { relations: ['states', 'consumer', 'consumer.states'] });
    }

    expect(user.getStates()[1].additionalData.retries).toBe(QueueConfig.mediator.maxRetries);
    expect(user.status).toBe(UserStatus.FAILED);

    await ConsumerTestUtil.destroyConsumer(user);

    done();
  });

  it('Should retry only the allowed amount of times on the Transaction Pipeline', async done => {
    const transactionPipeline = new TransactionPipelineJob({});
    const user = await ConsumerTestUtil.generateConsumer();
    const sourceWallet = await WalletTestUtil.generateAndRegister(user);
    const destinationWallet = await WalletTestUtil.generateAndRegister(user);
    const transactionInsertResult = await Transaction.insert({
      type: TransactionType.PAYMENT,
      source: sourceWallet,
    });
    const transactionId = transactionInsertResult.identifiers[0].id;

    await TransactionStateItem.insert({
      status: TransactionStatus.PENDING,
      transaction: { id: transactionId },
    });

    await Payment.insert({
      type: PaymentType.TRANSFER,
      amount: '1',
      destination: destinationWallet,
      transaction: { id: transactionId },
    });

    let transaction = await Transaction.findOne(transactionId, { relations: ['states', 'payments'] });

    for (let i = 0; i <= QueueConfig.transaction.maxRetries; i += 1) {
      await transactionPipeline.onFail(transaction, emptyMessage as any, { ack, nack } as any, {});
      transaction = await Transaction.findOne(transactionId, { relations: ['states', 'payments'] });
    }

    expect(transaction.getStates()[1].additionalData.retries).toBe(QueueConfig.transaction.maxRetries);
    expect(transaction.status).toBe(TransactionStatus.FAILED);

    await ConsumerTestUtil.destroyConsumer(user);

    done();
  });

  it('Should create anomaly if failed to delete entry on timescale on after transitioning transaction to failed', async done => {
    const transactionPipeline = new TransactionPipelineJob({});
    const user = await ConsumerTestUtil.generateConsumer();
    const sourceWallet = await WalletTestUtil.generateAndRegister(user);
    const destinationWallet = await WalletTestUtil.generateAndRegister(user);
    const transactionInsertResult = await Transaction.insert({
      type: TransactionType.PAYMENT,
      source: sourceWallet,
    });
    const transactionId = transactionInsertResult.identifiers[0].id;

    await TransactionStateItem.insert({
      status: TransactionStatus.PENDING,
      transaction: { id: transactionId },
    });

    await Payment.insert({
      type: PaymentType.TRANSFER,
      amount: '1',
      destination: destinationWallet,
      transaction: { id: transactionId },
    });

    const transaction = await Transaction.findOne(transactionId, { relations: ['states', 'payments'] });

    (new FailTransactionAction() as any).onFail(transaction, new Error());

    const updatedTransaction = await Transaction.findOne(transactionId, { relations: ['states', 'payments'] });
    expect(updatedTransaction.status).toBe(TransactionStatus.FAILED);

    const issueCount = await Issue.safeCount({
      where: {
        resource_id: transaction.id,
      },
    });

    expect(issueCount).toBe(1);

    await Issue.delete({
      resource_id: transaction.id,
    });

    await ConsumerTestUtil.destroyConsumer(user);

    done();
  });
});
