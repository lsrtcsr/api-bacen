import * as sinon from 'sinon';
import MainServer from '../../api/MainServer';
import { JiraService } from '../../api/services';
import { UninstalHandle, ServerTestUtil, AssetServiceMock } from '../util';

jest.setTimeout(180 * 1000);

describe('Jira Sender test', () => {
  let server: MainServer;
  let stub: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    stub = sinon.stub(JiraService.getInstance(), 'send').resolves(3);
  });

  afterAll(async () => {
    stub.restore();

    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstallHandle();
  });

  it('Can send to the JIRA service an issue', async () => {
    await JiraService.getInstance().createIssue({
      epicId: 'string',
      projectId: 'string;',
      issueTypeId: 'string;',
      summary: 'string;',
    });

    expect(JiraService.getInstance().send['calledOnce']).toBeTruthy();
  });
});
