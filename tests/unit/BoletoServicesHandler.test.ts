import * as sinon from 'sinon';
import * as uuid from 'uuid';
import { ServiceType } from '@bacen/base-sdk';
import { RootUtil, ConsumerTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { EventHandlingGateway } from '../../api/services';
import { ServiceConsumptionData } from '../../api/schemas';
import { Domain, User } from '../../api/models';

import moment = require('moment');

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;

  let contractor: User;
  let supplier: Domain;

  let eventHandlingGateway: EventHandlingGateway;
  let eventPublisher: sinon.SinonSpy;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    eventHandlingGateway = EventHandlingGateway.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });

    eventPublisher = sinon.spy(eventHandlingGateway, 'publish');
  });

  afterAll(async () => {
    if (eventPublisher) eventPublisher.restore();
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it.skip('Generete an event for charging the boleto emission service', async done => {
    const { logger } = Config.server;

    try {
      const operationDate = moment();

      const boleto = {
        code: uuid.v4(),
      };

      await eventHandlingGateway.process(ServiceType.BOLETO_EMISSION, {
        operationDate,
        user: contractor,
        serviceData: boleto,
      });

      const serviceData: ServiceConsumptionData = eventPublisher.getCall(0).args[0];
      expect(serviceData.type).toEqual(ServiceType.BOLETO_EMISSION);
      expect(serviceData.userId).toEqual(contractor.id);
      expect(serviceData.eventDate).toEqual(operationDate);
      expect(serviceData.liability).toBeUndefined();
      expect(serviceData.payment).toBeUndefined();
      expect(serviceData.settled).toBeFalsy();
      expect(serviceData.calculationRule).toBeUndefined();
      expect(serviceData.event).toHaveProperty('serviceData.code', boleto.code);
    } catch (exception) {
      logger.error('Account opening event generation failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it.skip('Generete an event for charging the boleto validation service', async done => {
    const { logger } = Config.server;

    try {
      const operationDate = moment();

      const boleto = {
        barcode: uuid.v4(),
      };

      await eventHandlingGateway.process(ServiceType.BOLETO_VALIDATION, {
        operationDate,
        user: contractor,
        serviceData: boleto,
      });

      const serviceData: ServiceConsumptionData = eventPublisher.getCall(1).args[0];
      expect(serviceData.type).toEqual(ServiceType.BOLETO_VALIDATION);
      expect(serviceData.userId).toEqual(contractor.id);
      expect(serviceData.eventDate).toEqual(operationDate);
      expect(serviceData.liability).toBeUndefined();
      expect(serviceData.payment).toBeUndefined();
      expect(serviceData.settled).toBeFalsy();
      expect(serviceData.calculationRule).toBeUndefined();
      expect(serviceData.event).toHaveProperty('serviceData.barcode', boleto.barcode);
    } catch (exception) {
      logger.error('Account opening event generation failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
