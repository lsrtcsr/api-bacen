import { Logger } from 'nano-errors';
import * as sinon from 'sinon';
import { groupBy } from '../../api/utils/ObjectUtil';
import { MockLogger } from '../util';

describe('api.utlils.groupBy', () => {
  const mockLogger = new MockLogger();
  const loggerStub = sinon.stub(Logger, 'getInstance').returns(mockLogger as any);

  const arr = [
    {
      id: 1,
      type: 'payment',
      asset: {
        id: 1,
        code: 'foo',
      },
    },
    {
      id: 2,
      type: 'payment',
      asset: {
        id: 1,
        code: 'foo',
      },
    },
    {
      id: 3,
      type: 'transfer',
      asset: {
        id: 1,
        code: 'foo',
      },
    },
  ];

  it('Groups an array by a string key', () => {
    const grouped = groupBy('type', arr);
    expect(grouped).toEqual({
      payment: [arr[0], arr[1]],
      transfer: [arr[2]],
    });
  });

  it('Groups an array by a number key', () => {
    const grouped = groupBy('id', arr);
    expect(grouped).toEqual({
      1: [arr[0]],
      2: [arr[1]],
      3: [arr[2]],
    });
  });

  it('throws an exception if grouping key is not an instance of string or number', () => {
    groupBy('asset', arr);
    const warnLogs = mockLogger.get('warn');
    expect(warnLogs.length).toBe(3);
    for (const log of warnLogs) {
      expect(log.message).toBe('Grouping key must be of type number | string | symbol, skipping');
    }
  });
});
