import { PaymentType } from '@bacen/base-sdk';
import * as uuid from 'uuid';
import MainServer from '../../api/MainServer';
import { Asset, Transaction, User, Wallet } from '../../api/models';
import { ServerTestUtil, WalletTestUtil, UserTestUtil } from '../util';

jest.setTimeout(180 * 1000);

describe.skip('lib.models.Transaction', () => {
  let server: MainServer;
  let user: User;
  let wallet: Wallet;
  let rootWallet: Wallet;
  let rootAsset: Asset;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    user = await UserTestUtil.generateUser({});
    wallet = await WalletTestUtil.generateAndRegister(user);
    rootWallet = await Wallet.getRootWallet();
    rootAsset = await Asset.getRootAsset();
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  describe('with previous transaction', () => {
    let transaction: Transaction;
    const externalTransactionId = uuid.v4();

    beforeEach(async () => {
      transaction = await Transaction.prepare(
        {
          source: rootWallet,
          type: PaymentType.TRANSACTION_REVERSAL,
          recipients: [
            {
              wallet,
              asset: rootAsset,
              amount: '1.00',
            },
          ],
          additionalData: {
            reversal: true,
            originalTransaction: externalTransactionId,
          },
        },
        false,
      );
    });

    afterEach(async () => {
      if (transaction) {
        await Transaction.remove(transaction);
        transaction = undefined;
      }
    });

    it('ensure transaction reversal are never duplicated', async () => {
      await expect(
        Transaction.prepare(
          {
            source: rootWallet,
            type: PaymentType.TRANSACTION_REVERSAL,
            recipients: [
              {
                wallet,
                asset: rootAsset,
                amount: '1.00',
              },
            ],
            additionalData: {
              reversal: true,
              originalTransaction: externalTransactionId,
            },
          },
          false,
        ),
      ).rejects.toThrow(/Transaction already reversed/gi);
    });
  });
});
