import * as currency from 'currency.js';
import * as moment from 'moment';
import { ServiceType } from '@bacen/base-sdk';
import { RootUtil, ConsumerTestUtil, BillingTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { PlanService, BillingService } from '../../api/services';
import { Domain, Plan, EventType, EntryStatus, User } from '../../api/models';

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;
  let contractor: User;
  let customPlan: Plan;
  let supplier: Domain;

  let billingService: BillingService;

  beforeAll(async () => {
    server = await ServerTestUtil.init();
    billingService = BillingService.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });

    await BillingTestUtil.createServices();
    customPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: false });

    await PlanService.getInstance().subscribe({
      plan: customPlan,
      prepaid: false,
      user: contractor,
    });
  });

  afterAll(async () => {
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (customPlan) await BillingTestUtil.destroyPlan(customPlan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Create invoice entries until the limit of free use', async done => {
    const { logger } = Config.server;

    try {
      const now = moment();
      const entryTypes = await customPlan.getEntryTypes(EventType.TRANSACTION as any);
      const boletoEmissionService = entryTypes.find(
        entryType => entryType.service.type === ServiceType.BOLETO_EMISSION,
      );

      for (const index = 0; index < boletoEmissionService.settings.freeUntil; index + 1) {
        await billingService.createEntry({
          userId: contractor.id,
          type: boletoEmissionService.service.type,
          eventDate: now,
          settled: false,
          liability: 'consumer',
        } as any);
      }

      const currentPeriod = await (billingService as any).getCurrentPeriod(contractor.id, now);

      const serviceConsumption = await currentPeriod.getServiceConsumption(ServiceType.BOLETO_EMISSION);
      expect(serviceConsumption).toEqual(boletoEmissionService.settings.freeUntil);

      const totalDebits = await currentPeriod.totalDebits(EntryStatus.PENDING);
      expect(currency(totalDebits)).toEqual(currency(0));

      done();
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it('Create invoice entries of an item without free use settings', async done => {
    const { logger } = Config.server;

    try {
      const now = moment();
      const entryTypes = await customPlan.getEntryTypes(EventType.TRANSACTION as any);
      const boletoEmissionService = entryTypes.find(
        entryType => entryType.service.type === ServiceType.PHYSICAL_CARD_ISSUING,
      );

      const numOfCards = 5;

      for (const index = 0; index < numOfCards; index + 1) {
        await billingService.createEntry({
          userId: contractor.id,
          type: boletoEmissionService.service.type,
          eventDate: now,
          settled: false,
          liability: 'consumer',
        } as any);
      }

      const currentPeriod = await (billingService as any).getCurrentPeriod(contractor.id, now);

      const serviceConsumption = await currentPeriod.getServiceConsumption(ServiceType.BOLETO_EMISSION);
      expect(serviceConsumption).toEqual(numOfCards);

      const currentPrice = await (billingService as any).getCurrentPrice(
        contractor.id,
        ServiceType.PHYSICAL_CARD_ISSUING,
      );
      const totalDebits = await currentPeriod.totalDebits(EntryStatus.PENDING);
      expect(currency(totalDebits)).toEqual(currency(numOfCards).multiply(currency(currentPrice.amount)));

      done();
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
