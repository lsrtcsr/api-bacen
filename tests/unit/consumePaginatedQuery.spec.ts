import * as faker from 'faker';
import MainDatabase from '../../api/MainDatabase';
import { DomainTestUtil } from '../util';
import { Domain } from '../../api/models';
import { consumePaginatedQuery } from '../../api/utils';

jest.setTimeout(60000);

describe('api.utils.consumePaginatedQuery', () => {
  let database: MainDatabase;
  let domainCount: number;
  let domains: Domain[];

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();
  });

  beforeEach(async () => {
    domainCount = faker.random.number({ min: 2, max: 10 });
    domains = await Promise.all(
      Array.from({ length: domainCount }, () => DomainTestUtil.generateDomain({ partial: { name: '__test_domain' } })),
    );
  });

  afterEach(async () => {
    await Promise.all(domains.map(domain => DomainTestUtil.destroyDomain(domain)));
    domainCount = null;
  });

  afterAll(async () => {
    await database.disconnect();
  });

  it('Iterates over query results one by one', async () => {
    let iterCount = 0;
    const retrievedDomains: Domain[] = [];
    const baseQuery = Domain.createQueryBuilder()
      .where({ name: '__test_domain' })
      .orderBy('created_at', 'DESC');

    for await (const domain of consumePaginatedQuery(baseQuery, 1)) {
      iterCount += 1;
      retrievedDomains.push(...domain);
    }

    expect(iterCount).toBe(domainCount);
    expect(retrievedDomains.length).toBe(domainCount);
    expect(retrievedDomains.map(domain => domain.id).sort()).toEqual(domains.map(domain => domain.id).sort());
  });

  it('Iterates over paginated query results', async () => {
    let iterCount = 0;
    const retrievedDomains: Domain[] = [];
    const baseQuery = Domain.createQueryBuilder()
      .where({ name: '__test_domain' })
      .orderBy('created_at', 'DESC');

    for await (const domain of consumePaginatedQuery(baseQuery, 2)) {
      iterCount += 1;
      retrievedDomains.push(...domain);
    }

    expect(iterCount).toBe(Math.ceil(domainCount / 2));
    expect(retrievedDomains.length).toBe(domainCount);
    expect(retrievedDomains.map(domain => domain.id).sort()).toEqual(domains.map(domain => domain.id).sort());
  });

  it('Yields a single time if page size is larger than number of entries', async () => {
    let iterCount = 0;
    const retrievedDomains: Domain[] = [];
    const baseQuery = Domain.createQueryBuilder()
      .where({ name: '__test_domain' })
      .orderBy('created_at', 'DESC');

    for await (const domain of consumePaginatedQuery(baseQuery, 11)) {
      iterCount += 1;
      retrievedDomains.push(...domain);
    }

    expect(iterCount).toBe(1);
    expect(retrievedDomains.length).toBe(domainCount);
    expect(retrievedDomains.map(domain => domain.id).sort()).toEqual(domains.map(domain => domain.id).sort());
  });
});
