import { RootUtil, ConsumerTestUtil, BillingTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { Domain, Plan } from '../../api/models';
import { PlanService } from '../../api/services';

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;
  let contractor;
  let defaultPlan: Plan;
  let customPlan: Plan;
  let supplier: Domain;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });

    await BillingTestUtil.createServices();
    defaultPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: true });
    customPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: false });
  });

  afterAll(async () => {
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (defaultPlan) await BillingTestUtil.destroyPlan(defaultPlan);
    if (customPlan) await BillingTestUtil.destroyPlan(customPlan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Subscribe a default plan in a prepaid mode', async done => {
    const { logger } = Config.server;

    try {
      const subscription = await PlanService.getInstance().subscribe({
        prepaid: true,
        user: contractor,
      });

      expect(subscription).not.toBeUndefined();
      expect(subscription.plan.default).toBeTruthy();
      expect(subscription.plan.id).toEqual(defaultPlan.id);
      expect(subscription.prepaid).toBeTruthy();
      done();
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });

  it('Subscribe a custom plan in a post paid mode', async done => {
    const { logger } = Config.server;

    try {
      const subscription = await PlanService.getInstance().subscribe({
        plan: customPlan,
        prepaid: false,
        user: contractor,
      });

      expect(subscription).not.toBeUndefined();
      expect(subscription.plan.default).toBeFalsy();
      expect(subscription.plan.id).toEqual(customPlan.id);
      expect(subscription.prepaid).toBeFalsy();
      done();
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
