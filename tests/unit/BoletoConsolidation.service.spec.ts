import * as sinon from 'sinon';
import * as faker from 'faker';
import * as typeorm from 'typeorm';
import { FindConditions, DeepPartial } from 'typeorm';
import { HttpError } from 'ts-framework';
import { LoggerInstance } from 'nano-errors';
import { TransactionType } from '@bacen/base-sdk';
import { Horizon } from 'stellar-sdk';
import {
  Asset,
  Wallet,
  PaymentLog,
  Transaction,
  PrepareTransactionOptions,
  Payment,
  EmitOrDestroyOptions,
} from '../../api/models';
import { BoletoConsolidationService, BalanceMap, PipelineData } from '../../api/services';

jest.setTimeout(60 * 1000);

const createMockPaymentLog = (length: number, partial?: Partial<PaymentLog>) =>
  Array(length)
    .fill(null)
    .map(
      _ =>
        new PaymentLog({
          time: faker.date.past(1),
          amount: '1.23',
          ...partial,
        }),
    );

const mockPipelineData = (partial?: DeepPartial<PipelineData>) =>
  ({
    wallet: {},
    sourceWallet: {},
    sourceAsset: {},
    targetAsset: {},
    balance: {},
    ...partial,
  } as PipelineData);

describe('api.services.consolidation.BoletoConsolidation', () => {
  const service: BoletoConsolidationService = BoletoConsolidationService.initialize();

  describe('findAssetByCode', () => {
    const assetCode = 'TEST';
    const mockAsset = new Asset({
      code: assetCode,
      name: faker.finance.currencyName(),
    });
    let assetGetByCodeStub: sinon.SinonStub;

    beforeAll(() => {
      assetGetByCodeStub = sinon.stub(Asset, 'getByCode').callsFake(async (code: string, manager?: any) => {
        if (code === assetCode) return mockAsset;

        return undefined;
      });
    });

    afterAll(() => {
      assetGetByCodeStub.restore();
    });

    it('Finds an asset given a valid code', async () => {
      const asset = await service.findAssetByCode(assetCode);
      expect(asset).toBe(mockAsset);
    });

    it('throws if there is no asset with the given code', async () => {
      let err: any;

      try {
        await service.findAssetByCode('INVALID_CODE');
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toHaveProperty('originalMessage', '[404] Asset with code INVALID_CODE not found');
    });
  });

  describe('findSourceWallet', () => {
    const mockId = faker.random.uuid();
    const rootWalletId = faker.random.uuid();
    const createMockWallet = (id: string) => new Wallet({ id });

    const mockWallet = createMockWallet(mockId);
    const mockRootWallet = createMockWallet(rootWalletId);

    let walletFindOneStub: sinon.SinonStub;
    let walletGetRootWalletStub: sinon.SinonStub;

    beforeAll(() => {
      walletFindOneStub = sinon.stub(Wallet, 'findOne').callsFake(async (id: any) => {
        if (id === mockId) return mockWallet;
        return undefined;
      });

      walletGetRootWalletStub = sinon.stub(Wallet, 'getRootWallet').resolves(mockRootWallet);
    });

    afterAll(() => {
      walletFindOneStub.restore();
      walletGetRootWalletStub.restore();
    });

    it('finds a wallet given an existing id', async () => {
      const wallet = await service.findSourceWallet(mockId);
      expect(wallet).toBe(mockWallet);
    });

    it('defaults to the root wallet if no id is passet', async () => {
      const wallet = await service.findSourceWallet();
      expect(wallet).toBe(mockRootWallet);
    });

    it('throws an error if there is no wallet with the given id', async () => {
      const invalidId = faker.random.uuid();
      let err: any;

      try {
        const wallet = await service.findSourceWallet(invalidId);
        console.log(wallet);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toHaveProperty('originalMessage', `[404] Source wallet with id ${invalidId} not found`);
    });
  });

  describe('getAssetPaymentLog', () => {
    const assetCode = 'BRLD';
    const brldLength = faker.random.number(5);
    const notBrldLength = faker.random.number(5);
    const entities = [
      ...createMockPaymentLog(brldLength, { asset: assetCode }),
      ...createMockPaymentLog(notBrldLength, { asset: 'notBRLD' }),
    ];

    const mockPaymentLogRepository = (entities: PaymentLog[]) => ({
      find: (options: typeorm.FindManyOptions<PaymentLog>) => {
        return entities.filter(entity => entity.asset === (options.where as FindConditions<PaymentLog>).asset);
      },
    });

    let paymentLogRepositoryStub: sinon.SinonStub;

    beforeAll(() => {
      paymentLogRepositoryStub = sinon.stub(typeorm, 'getConnection').returns({
        getRepository: (entity: any) => {
          return mockPaymentLogRepository(entities) as any;
        },
      } as any);
    });

    afterAll(() => {
      paymentLogRepositoryStub.restore();
    });

    it('filters the entities by asset', async () => {
      const paymentLogs = await service.getAssetPaymentLog({ code: assetCode } as Asset);
      expect(paymentLogs).toHaveProperty('length', brldLength);
    });

    it('returns an empty list for an invalid asset', async () => {
      const paymentLogs = await service.getAssetPaymentLog({ code: 'invalidAssetCode' } as Asset);
      expect(paymentLogs).toHaveProperty('length', 0);
    });
  });

  describe('getCashInsFromPaymentLog', () => {
    const cashInLength = faker.random.number({ min: 1, max: 5 });
    const otherLength = faker.random.number({ min: 1, max: 5 });
    const sourceWalletId = faker.random.uuid();
    const entities = [
      ...createMockPaymentLog(cashInLength, { asset: 'BRLD', source: sourceWalletId }),
      ...createMockPaymentLog(otherLength, { asset: 'BRLD' }),
    ];

    it('filters the cashIns from a list of payments', () => {
      const payments = service.getCashInsFromPaymentLog(sourceWalletId)(entities);
      expect(payments).toHaveProperty('length', cashInLength);
    });

    it('returns an empty list for an invalid sourceWalletId', () => {
      const payments = service.getCashInsFromPaymentLog(faker.random.uuid())(entities);
      expect(payments).toHaveProperty('length', 0);
    });

    it('Handles an empty entities list', () => {
      const payments = service.getCashInsFromPaymentLog(sourceWalletId)([]);
      expect(payments.length).toBe(0);
    });
  });

  describe('getCashOutsFromPaymentLog', () => {
    const cashOutLength = faker.random.number({ min: 1, max: 5 });
    const otherLength = faker.random.number({ min: 1, max: 5 });
    const sourceWalletId = faker.random.uuid();
    const entities = [
      ...createMockPaymentLog(cashOutLength, { asset: 'BRLD', destination: sourceWalletId }),
      ...createMockPaymentLog(otherLength, { asset: 'BRLD' }),
    ];

    it('filters the cashIns from a list of payments', () => {
      const payments = service.getCashOutsFromPaymentLog(sourceWalletId)(entities);
      expect(payments).toHaveProperty('length', cashOutLength);
    });

    it('returns an empty list for an invalid sourceWalletId', () => {
      const payments = service.getCashOutsFromPaymentLog(faker.random.uuid())(entities);
      expect(payments).toHaveProperty('length', 0);
    });

    it('Handles an empty entities list', () => {
      const payments = service.getCashOutsFromPaymentLog(sourceWalletId)([]);
      expect(payments.length).toBe(0);
    });
  });

  describe('buildWalletBalancesFromCashInsAndCashOuts', () => {
    const walletNumber = faker.random.number({ min: 1, max: 5 });
    const walletIds = Array(walletNumber)
      .fill(null)
      .map(_ => faker.random.uuid());

    // # of cashIns/out per wallet
    const cashInNumber = faker.random.number({ min: 1, max: 3 });
    const cashOutNumber = faker.random.number({ min: 1, max: 3 });

    const cashIns = walletIds.reduce(
      (acc, curr) =>
        acc.concat(
          Array(cashInNumber)
            .fill(null)
            .map(_ => ({
              id: curr,
              amount: 1.23,
            })),
        ),
      [] as { id: string; amount: number }[],
    );

    const cashOuts = walletIds.reduce(
      (acc, curr) =>
        acc.concat(
          Array(cashOutNumber)
            .fill(null)
            .map(_ => ({
              id: curr,
              amount: -1.23,
            })),
        ),
      [] as { id: string; amount: number }[],
    );

    it('returns the correct number of balances', () => {
      const balances = service.buildWalletBalancesFromCashInsAndCashOuts(cashIns, cashOuts);

      expect(Object.keys(balances).length).toBe(walletNumber);
    });

    it('calculates the corret balance for every wallet', () => {
      const balances = service.buildWalletBalancesFromCashInsAndCashOuts(cashIns, cashOuts);

      const expectedBalances = walletIds.map(walletId => ({
        id: walletId,
        balance: cashIns
          .concat(cashOuts)
          .filter(payment => payment.id === walletId)
          .reduce((acc, curr) => acc + curr.amount, 0),
      }));

      expectedBalances.forEach(expectedBalance => {
        expect(balances[expectedBalance.id]).toBe(expectedBalance.balance);
      });
    });

    it('handles both arrays empty', () => {
      const balances = service.buildWalletBalancesFromCashInsAndCashOuts([], []);
      expect(Object.values(balances).length).toBe(0);
    });
  });

  describe('removeZeroBalances', () => {
    const balanceMapLength = faker.random.number({ min: 1, max: 10 });
    const balanceMaps: BalanceMap = Array(balanceMapLength)
      .fill(null)
      .reduce((acc, _) => ({ ...acc, [faker.random.uuid()]: faker.random.arrayElement([0, 1.23]) }), {});

    it('filters only the non-zero balances', () => {
      const filteredBalances = service.removeZeroBalances(balanceMaps);

      Object.values(filteredBalances).forEach(balance => {
        expect(balance).not.toBe(0);
      });
    });

    it('handles an empty object', () => {
      const filteredBalances = service.removeZeroBalances({});
      expect(Object.values(filteredBalances).length).toBe(0);

      Object.values(filteredBalances).forEach(balance => {
        expect(balance).not.toBe(0);
      });
    });
  });

  describe('findWalletsFromBalances', () => {
    let walletFindByIdsStub: sinon.SinonStub;
    const knownWalletCount = faker.random.number({ min: 1, max: 5 });
    const unknownWalletCount = faker.random.number({ min: 1, max: 5 });
    const walletIds = Array.from({ length: knownWalletCount }, () => faker.random.uuid());

    const balanceMap: BalanceMap = (Object as any).fromEntries(walletIds.map(id => [id, 1.23]));
    const walletsList = walletIds
      .map(id => new Wallet({ id }))
      .concat(Array.from({ length: unknownWalletCount }, () => new Wallet({ id: faker.random.uuid() })));

    beforeAll(() => {
      walletFindByIdsStub = sinon.stub(Wallet, 'findByIds').callsFake(async (ids: string[]) => {
        return walletsList.filter(wallet => ids.includes(wallet.id));
      });
    });

    afterAll(() => {
      walletFindByIdsStub.restore();
    });

    it('Correctly filters for the desired wallets', async () => {
      const wallets = await service.findWalletsFromBalances(balanceMap);

      expect(wallets.length).toBe(knownWalletCount);
    });

    it('throws a 404 for an empty list', async () => {
      let err: HttpError;
      try {
        await service.findWalletsFromBalances({});
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(HttpError);
      expect(err).toHaveProperty('originalMessage', '[404] No wallets were found with the given ids');
    });

    it('throws a 404 for an unknown walletId in the balances', async () => {
      let err: HttpError;
      try {
        await service.findWalletsFromBalances({ [faker.random.uuid()]: 1.23 });
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(HttpError);
      expect(err).toHaveProperty('originalMessage', '[404] No wallets were found with the given ids');
    });
  });

  describe('loadAccountBalanceFromStellar', () => {
    const walletsCount = faker.random.number({ min: 1, max: 5 });
    const wallets = Array.from({ length: walletsCount }, () => new Wallet({ id: faker.random.uuid() }));
    const fakeAsset = new Asset({ id: faker.random.uuid(), code: faker.finance.currencyCode() });
    const assetWeWant = new Asset({ id: faker.random.uuid(), code: 'BRLD' });

    const selectedWallet = faker.random.arrayElement(wallets);
    const selectedWalletIdx = wallets.indexOf(selectedWallet);

    const stellarAccounts = wallets.map((wallet, idx) => ({
      publicKey: wallet.stellar.publicKey,
      balances: [
        {
          asset_code: fakeAsset.code,
          balance: idx.toFixed(7),
        },
        {
          asset_code: assetWeWant.code,
          balance: (1.23 * idx).toFixed(7),
        },
      ] as Horizon.BalanceLine[],
    }));

    const loadAccountStub = sinon.stub().callsFake(async (pubKey: string) => {
      if (pubKey === 'throw') throw new Error();
      return stellarAccounts.find(acc => acc.publicKey === pubKey);
    });

    afterEach(() => {
      loadAccountStub.resetHistory();
    });

    it('Returns the correct balance', async () => {
      const data = mockPipelineData({ wallet: selectedWallet, sourceAsset: assetWeWant });
      const response = await service.loadAccounBalanceFromStellar({ loadAccount: loadAccountStub } as any)(data);

      expect(response.stellarBalance).toBeDefined();
      expect(response.stellarBalance).toBe((1.23 * selectedWalletIdx).toFixed(7));
    });

    it('Throws if StellarService.loadAccountThrows', async () => {
      const dataThatThrows = mockPipelineData({ wallet: { ...selectedWallet, stellar: { publicKey: 'throw' } } });
      let err: Error;

      try {
        await service.loadAccounBalanceFromStellar({ loadAccount: loadAccountStub } as any)(dataThatThrows);
      } catch (e) {
        err = e;
      }
      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(Error);
    });
  });

  describe('assertBalanceIntegrity', () => {
    let loggerWarnStub: sinon.SinonStub;
    let fn: (data: PipelineData) => PipelineData;

    beforeAll(() => {
      loggerWarnStub = sinon.stub();
      fn = service.assertBalanceIntegrity(({ warn: loggerWarnStub } as unknown) as LoggerInstance);
    });

    afterEach(() => {
      loggerWarnStub.resetHistory();
    });

    afterAll(() => {
      fn = null;
    });

    it('warns if walletBalance does not match stellarBalance', () => {
      const wallet = { id: faker.random.uuid() };
      const balance = { [wallet.id]: '1.23' };
      const stellarBalance = '1.0000000';

      fn(mockPipelineData({ wallet, balance, stellarBalance }));
      expect(loggerWarnStub.callCount).toBe(1);
    });

    it("returns the correct balance if balances don't match", () => {
      const wallet = { id: faker.random.uuid() };
      const balance = { [wallet.id]: '1.23' };
      const stellarBalance = '1.0000000';

      const data = fn(mockPipelineData({ wallet, balance, stellarBalance }));
      expect(data.stellarBalance).toBe(stellarBalance);
    });

    it('does nothing if balances match', () => {
      const wallet = { id: faker.random.uuid() };
      const balance = { [wallet.id]: '1.23' };
      const stellarBalance = '1.2300000';

      fn(mockPipelineData({ wallet, balance, stellarBalance }));
      expect(loggerWarnStub.callCount).toBe(0);
    });
  });

  describe('prepareTransaction', () => {
    let transactionPrepareStub: sinon.SinonStub;
    const data = mockPipelineData({
      wallet: { id: faker.random.uuid() },
      targetAsset: { id: faker.random.uuid() },
      sourceWallet: { id: faker.random.uuid() },
      stellarBalance: '1.23',
      additionalData: {},
    });

    beforeAll(() => {
      transactionPrepareStub = sinon.stub(Transaction, 'prepare').callsFake(async (opts: PrepareTransactionOptions) => {
        if ((opts.additionalData as any).throw === true) throw new Error();

        return new Transaction({
          additionalData: opts.additionalData,
          source: opts.source,
          type: TransactionType.PAYMENT,
          payments: opts.recipients.map(
            rec =>
              new Payment({
                destination: rec.wallet,
                asset: rec.asset,
                amount: rec.amount,
                type: opts.type,
              }),
          ),
        });
      });
    });

    afterAll(() => {
      transactionPrepareStub.restore();
    });

    it('Captures an exception thrown from within Transaction.prepare', async () => {
      const dataThatThrows = { ...data, additionalData: { throw: true } };
      let err: Error;

      try {
        await service.prepareTransaction({} as any)(dataThatThrows);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(Error);
    });

    it('Returns a pipelineData with the transaction populated', async () => {
      const response = await service.prepareTransaction({} as any)(data);

      expect(response.transaction).toBeDefined();
      expect(response.transaction).toBeInstanceOf(Transaction);
    });
  });

  describe('destroyAsset', () => {
    let destroySourceAssetStub: sinon.SinonStub;
    const data = mockPipelineData({ sourceAsset: new Asset({ id: faker.random.uuid() }) });

    beforeAll(() => {
      destroySourceAssetStub = sinon.stub(Asset.prototype, 'destroy').callsFake(async (opts: EmitOrDestroyOptions) => {
        if ((opts.additionalData || ({} as any)).throw === true) throw new Error();
        return new Transaction({});
      });
    });

    afterEach(() => {
      destroySourceAssetStub.resetHistory();
    });

    afterAll(() => {
      destroySourceAssetStub.restore();
    });

    it('returns the same object it was passed (deep comparison)', async () => {
      const response = await service.destroyAsset({} as any)(data);

      expect(response).toEqual(data);
      expect(destroySourceAssetStub.callCount).toBe(1);
    });

    it('Throws if Asset.destroy throws', async () => {
      const dataThatThrows = { ...data, additionalData: { throw: true } };
      let err: Error;

      try {
        await service.destroyAsset({} as any)(dataThatThrows);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(Error);
      expect(destroySourceAssetStub.callCount).toBe(1);
    });
  });

  describe('publishTransaction', () => {
    const sendStub = sinon.stub().callsFake(async (transaction: Transaction) => {
      if (transaction) throw new Error();
    });
    const data = mockPipelineData();

    afterEach(() => {
      sendStub.resetHistory();
    });

    it('returns the same object it was passed (deep comparison)', async () => {
      const response = await service.publishTransaction({ send: sendStub } as any)(data);

      expect(response).toEqual(data);
      expect(sendStub.callCount).toBe(1);
    });

    it('Throws if TransactionPipelinePublish.send throws', async () => {
      const dataThatThrows = { ...data, transaction: new Transaction({}) };
      let err: Error;

      try {
        await service.publishTransaction({ send: sendStub } as any)(dataThatThrows);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toBeInstanceOf(Error);
      expect(sendStub.callCount).toBe(1);
    });
  });
});
