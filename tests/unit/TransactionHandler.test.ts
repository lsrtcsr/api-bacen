import * as sinon from 'sinon';
import { PaymentType, ServiceType } from '@bacen/base-sdk';
import {
  ConsumerTestUtil,
  RootUtil,
  BillingTestUtil,
  WalletTestUtil,
  OAuthTestUtil,
  UserTestUtil,
  ServerTestUtil,
} from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { PlanService, EventHandlingGateway } from '../../api/services';
import { ServiceConsumptionData } from '../../api/schemas';
import { OAuthAccessToken, User, Wallet, Plan, Domain, Asset } from '../../api/models';

jest.setTimeout(3000 * 1000);

describe.skip('Unit tests for service charge event generator', () => {
  let server: MainServer;

  let accessToken: OAuthAccessToken;
  let contractor: User;
  let wallet: Wallet;
  let defaultPlan: Plan;
  let supplier: Domain;

  let eventHandlingGateway: EventHandlingGateway;
  let eventPublisher: sinon.SinonSpy;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    eventHandlingGateway = EventHandlingGateway.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getDefaultDomain();
    contractor = await ConsumerTestUtil.generateConsumer({ userDomain: supplier });
    wallet = await WalletTestUtil.generateAndRegister(contractor);
    accessToken = await OAuthTestUtil.generateTokenFromUser(contractor);

    await BillingTestUtil.createServices();
    defaultPlan = await BillingTestUtil.createPlan({ supplier, contractor, default: true });

    await PlanService.getInstance().subscribe({
      prepaid: false,
      user: contractor,
    });

    eventPublisher = sinon.spy(eventHandlingGateway, 'publish');
  });

  afterAll(async () => {
    if (eventPublisher) eventPublisher.restore();
    if (contractor) await UserTestUtil.destroyUser(contractor);
    // if (defaultPlan) await BillingTestUtil.destroyPlan(defaultPlan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Generete boleto payment event for service charge', async done => {
    const { logger } = Config.server;
    try {
      const rootAsset = await Asset.getRootAsset();
      const transaction = await rootAsset.destroy({
        amount: '1.00',
        createdBy: accessToken,
        targetWallet: wallet,
        paymentType: PaymentType.BOLETO,
      });

      await eventHandlingGateway.process(ServiceType.BOLETO_PAYMENT, transaction);

      const serviceData: ServiceConsumptionData = eventPublisher.getCall(0).args[0];
      expect(serviceData.type).toEqual(ServiceType.BOLETO_PAYMENT);
      expect(serviceData.userId).toEqual(contractor.id);
      expect(serviceData.liability).toBeUndefined();
      expect(serviceData.payment).toBeUndefined();
      expect(serviceData.settled).toBeFalsy();
      expect(serviceData.calculationRule).toBeUndefined();
      expect(serviceData.event).toHaveProperty('id', transaction.id);
    } catch (exception) {
      logger.error('Boleto payment event generation failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
