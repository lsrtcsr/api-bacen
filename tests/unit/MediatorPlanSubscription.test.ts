import { UserRole } from '@bacen/base-sdk';
import { RootUtil, BillingTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { Domain, Plan } from '../../api/models';
import { PlanService } from '../../api/services';

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;
  let contractor;
  let defaultPlan: Plan;
  let supplier: Domain;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    await RootUtil.generateRoot();
    supplier = await Domain.getRootDomain();
    contractor = await UserTestUtil.generateUser({
      userDomain: supplier,
      role: UserRole.MEDIATOR,
      createWallet: true,
    });

    await BillingTestUtil.createServices();
    defaultPlan = await BillingTestUtil.createPlan({ contractor, supplier, default: true });
  });

  afterAll(async () => {
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (defaultPlan) await BillingTestUtil.destroyPlan(defaultPlan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('Subscribe a default plan in a post paid mode', async done => {
    const { logger } = Config.server;
    try {
      const subscription = await PlanService.getInstance().subscribe({
        prepaid: false,
        user: contractor,
      });

      expect(subscription).not.toBeUndefined();
      expect(subscription.plan.default).toBeTruthy();
      expect(subscription.plan.id).toEqual(defaultPlan.id);
      expect(subscription.prepaid).toBeFalsy();
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
