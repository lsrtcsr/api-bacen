import { DomainRole, ConsumerStatus, AccountType, UserRole, TransitoryAccountType } from '@bacen/base-sdk';
import MainDatabase from '../../api/MainDatabase';
import { Domain, User, Wallet } from '../../api/models';
import { DomainTestUtil, WalletTestUtil, ConsumerTestUtil, UserTestUtil } from '../util';

jest.setTimeout(60000);

describe('api.models.wallet', () => {
  let database: MainDatabase;
  let rootDomain: Domain;
  let rootMediator: User;
  let rootCardTransitoryAccountWallet: Wallet;
  let otherMediator: User;
  let otherTransitoryAccountWallet: Wallet;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    rootDomain = await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
    otherMediator = await ConsumerTestUtil.generateConsumer({
      consumerState: ConsumerStatus.READY,
      type: AccountType.CORPORATE,
      role: UserRole.MEDIATOR,
    });

    rootMediator = await ConsumerTestUtil.generateConsumer({
      consumerState: ConsumerStatus.READY,
      type: AccountType.CORPORATE,
      role: UserRole.MEDIATOR,
      userDomain: rootDomain,
    });

    otherTransitoryAccountWallet = await WalletTestUtil.generate(otherMediator, {
      additionalData: {
        isTransitoryAccount: true,
        transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
      },
    });

    rootCardTransitoryAccountWallet = await WalletTestUtil.generate(rootMediator, {
      additionalData: {
        isTransitoryAccount: true,
        transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
      },
    });
  });

  afterAll(async () => {
    await UserTestUtil.destroyUser(rootMediator);
    await UserTestUtil.destroyUser(otherMediator);
    await DomainTestUtil.destroyDomain(rootDomain);

    await database.disconnect();
  });

  describe('getRootTransitoryAccountWallet', () => {
    it('returns a transitoryAccountWallet from the root mediator', async () => {
      const transitoryWallet = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

      expect(transitoryWallet.id).toBe(rootCardTransitoryAccountWallet.id);
    });
  });
});
