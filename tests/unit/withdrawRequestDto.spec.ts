import 'reflect-metadata';
import * as faker from 'faker';
import * as cpf from 'cpf';
import { ValidationError } from 'class-validator';
import { BankingType, AccountType, Banking } from '@bacen/base-sdk';
import { WithdrawRequestDto } from '../../api/schemas/dto';

describe('api.schema.dto.WithdrawRequestDto', () => {
  describe('create', () => {
    const basePayload: Partial<WithdrawRequestDto> = {
      asset: 'root',
      amount: '23.99',
    };

    it('Requires either banking or bankingId to be defined', async () => {
      const bankingExpectedError = 'Either bank or bankingId should be defined';
      const keyExpectedError = 'key must be a string';
      let err: ValidationError[];

      try {
        await WithdrawRequestDto.create(basePayload);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err).toHaveProperty('length', 3);

      expect(err[0].constraints['isString']).toBe(keyExpectedError);
      expect(err[1].constraints['isDefined']).toBe(bankingExpectedError);
      expect(err[2].constraints['isDefined']).toBe(bankingExpectedError);
    });

    it('validates the payload when passed a bankingId', async () => {
      const res = await WithdrawRequestDto.create({ ...basePayload, bankingId: faker.random.uuid() });
      expect(res).toBeInstanceOf(WithdrawRequestDto);
    });

    describe('when passed a bank', () => {
      let bank: Banking;

      beforeEach(() => {
        bank = {
          bank: faker.finance.account(3),
          agency: faker.finance.account(3),
          account: faker.finance.account(8),
          agencyDigit: faker.random.number().toString(),
          accountDigit: faker.random.number().toString(),
          type: BankingType.CHECKING,
          name: faker.name.findName(),
          taxId: cpf.generate().replace(/[\.-]/g, ''),
          holderType: AccountType.PERSONAL,
        };
      });

      it('handles a valid payload', async () => {
        const res = await WithdrawRequestDto.create({ ...basePayload, bank });
        expect(res).toBeInstanceOf(WithdrawRequestDto);
      });

      it.skip('Throws an error if destination account is not present', async () => {
        delete bank.account;

        let err: any;
        try {
          await WithdrawRequestDto.create({ ...basePayload, bank });
        } catch (e) {
          err = e;
        }

        expect(err).toBeDefined();
      });

      it.skip('Throws an error if destination account is not present', async () => {
        delete bank.account;

        let err: any;
        try {
          await WithdrawRequestDto.create({ ...basePayload, bank });
        } catch (e) {
          err = e;
        }

        expect(err).toBeDefined();
      });

      it.skip('Throws an error if destination account equal to zero', async () => {
        bank.account = '0';

        let err: any;
        try {
          await WithdrawRequestDto.create({ ...basePayload, bank });
        } catch (e) {
          err = e;
        }

        expect(err).toBeDefined();
      });
    });

    it.skip('throws for a past scheduledFor', async () => {
      const payload: Partial<WithdrawRequestDto> = {
        ...basePayload,
        bankingId: faker.random.uuid(),
        // 2 min ago
        extra: {
          scheduleFor: new Date(Date.now() - 2 * 60 * 1000),
        },
      };

      let err;

      try {
        await WithdrawRequestDto.create(payload);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
    });
  });
});
