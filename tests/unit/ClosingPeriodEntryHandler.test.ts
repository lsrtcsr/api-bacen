import * as currency from 'currency.js';
import * as moment from 'moment';
import { UserRole, ServiceType } from '@bacen/base-sdk';
import { RootUtil, BillingTestUtil, UserTestUtil, ServerTestUtil } from '../util';
import MainServer from '../../api/MainServer';
import Config from '../../config';
import { PlanService, BillingService, ClosingEntryHandler } from '../../api/services';
import { Plan, Domain, EventType, EntryStatus } from '../../api/models';

jest.setTimeout(300 * 1000);

describe.skip('Pact with Our Provider', () => {
  let server: MainServer;
  let contractor;
  let plan: Plan;
  let supplier: Domain;

  let billingService: BillingService;
  let planService: PlanService;

  beforeAll(async () => {
    server = await ServerTestUtil.init();

    billingService = BillingService.getInstance();
    planService = PlanService.getInstance();

    await RootUtil.generateRoot();
    supplier = await Domain.getRootDomain();
    contractor = await UserTestUtil.generateUser({
      userDomain: supplier,
      role: UserRole.MEDIATOR,
      createWallet: true,
    });

    await BillingTestUtil.createServices();
    plan = await BillingTestUtil.createPlan({ contractor, supplier, default: true });

    await planService.subscribe({
      plan,
      prepaid: false,
      user: contractor,
    });
  });

  afterAll(async () => {
    if (contractor) await BillingTestUtil.destroyContractorInvoices(contractor);
    if (contractor) await UserTestUtil.destroyUser(contractor);
    if (plan) await BillingTestUtil.destroyPlan(plan);
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it.skip('Create invoice entries until the limit of free use', async done => {
    const { logger } = Config.server;

    try {
      const now = moment();
      const currentPeriod = await (billingService as any).getCurrentPeriod(contractor.id, now);

      const serviceTypes = await planService.getServiceTypes(contractor.id, EventType.PERIOD_CLOSING);

      const eventHandler = ClosingEntryHandler.getInstance();
      const types: ServiceType[] = [];
      for (const type of serviceTypes) {
        const serviceData = await eventHandler.handle(currentPeriod.invoice.contractor, type);
        if (!serviceData.calculationRule) {
          await (billingService as any).createEntry(serviceData, [EventType.PERIOD_CLOSING], currentPeriod);
          types.push(serviceData.type as any);
        }
      }

      const periodClosingServicesSum = plan.priceList
        .filter(item => types.includes(item.service.type as any))
        .map(item => currency(item.amount))
        .reduce((sum, current) => sum.add(current), currency(0, { precision: 2 }));

      const balance = await currentPeriod.balance(EntryStatus.PENDING);
      expect(currency(balance)).toEqual(periodClosingServicesSum);
    } catch (exception) {
      logger.error('Plan subscription failed', { ...exception });
      throw exception;
    } finally {
      done();
    }
  });
});
