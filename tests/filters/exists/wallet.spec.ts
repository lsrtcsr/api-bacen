import * as sinon from 'sinon';
import * as faker from 'faker';
import { HttpError, BaseResponse } from 'ts-framework';
import * as HttpMock from 'node-mocks-http';
import Exists from '../../../api/filters/exists';
import MainDatabase from '../../../api/MainDatabase';
import { Wallet, User, bacenRequest } from '../../../api/models';
import { WalletTestUtil, UserTestUtil, RequestUtil } from '../../util';

jest.setTimeout(60000);

describe('api.filters.exists.wallet', () => {
  const nextFnStub = sinon.stub();
  let database: MainDatabase;
  let activeWallet: Wallet;
  let deletedWallet: Wallet;
  let activeWalletWithDeletedUser: Wallet;
  let activeUser: User;
  let deletedUser: User;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    activeUser = await UserTestUtil.generateUser();
    deletedUser = await UserTestUtil.generateUser();
    await User.update(deletedUser.id, { deletedAt: new Date() });

    activeWallet = await WalletTestUtil.generate(activeUser);
    activeWalletWithDeletedUser = await WalletTestUtil.generate(deletedUser);
    deletedWallet = await WalletTestUtil.generate(activeUser);
    await Wallet.update(deletedWallet.id, { deletedAt: new Date() });
  });

  afterAll(async () => {
    await UserTestUtil.destroyUser(activeUser);
    await UserTestUtil.destroyUser(deletedUser);

    await database.disconnect();
  });

  afterEach(() => {
    nextFnStub.resetHistory();
  });

  it('throws a 404 if there is no wallet with the given id', async () => {
    let exception: HttpError;
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      params: { id: faker.random.uuid() },
    });

    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    try {
      await Exists.Wallet()(req, res, nextFnStub as any);
    } catch (err) {
      exception = err;
    }

    expect(exception.status).toBe(404);
    expect(nextFnStub.called).toBe(false);
  });

  it('Calls next if there is an active wallet with an active user with the given id', async () => {
    let exception: HttpError;
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      params: { id: activeWallet.id },
    });

    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    try {
      await Exists.Wallet()(req, res, nextFnStub as any);
    } catch (err) {
      exception = err;
    }

    expect(exception).toBeUndefined();
    expect(nextFnStub.calledOnce).toBe(true);
  });

  it('throws a 404 if the wallet is deleted', async () => {
    let exception: HttpError;
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      params: { id: deletedWallet.id },
    });

    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    try {
      await Exists.Wallet()(req, res, nextFnStub as any);
    } catch (err) {
      exception = err;
    }

    expect(exception.status).toBe(404);
    expect(nextFnStub.called).toBe(false);
  });

  it("throws a 404 if the wallet is active but it's user is deleted", async () => {
    let exception: HttpError;
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      params: { id: activeWalletWithDeletedUser.id },
    });

    RequestUtil.patchRequestObject(req);
    RequestUtil.patchResponseObject(res);

    try {
      await Exists.Wallet()(req, res, nextFnStub as any);
    } catch (err) {
      exception = err;
    }

    expect(exception.status).toBe(404);
    expect(nextFnStub.called).toBe(false);
  });
});
