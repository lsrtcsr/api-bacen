import * as HttpMocks from 'node-mocks-http';
import * as faker from 'faker';
import * as sinon from 'sinon';
import { BaseResponse, HttpError } from 'ts-framework';
import {
  WalletStatus,
  CustodyProvider,
  AssetRegistrationStatus,
  TransactionStatus,
  PaymentType,
} from '@bacen/base-sdk';
import { StellarService } from '@bacen/stellar-service';
import { hasUserEnoughBalance } from '../../../api/filters/phone-credits/balance';
import {
  ServerTestUtil,
  WalletTestUtil,
  UserTestUtil,
  PaymentTestUtil,
  AssetTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../../util';
import { Wallet, Asset, User, bacenRequest } from '../../../api/models';
import MainServer from '../../../api/MainServer';

jest.setTimeout(60000);

describe('api.filters.phone-credits.balance', () => {
  let server: MainServer;
  let nextStub: sinon.SinonStub;
  let user: User;
  let sourceWallet: Wallet;
  let issuerWallet: Wallet;
  let asset: Asset;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    nextStub = sinon.stub();

    user = await UserTestUtil.generateUser();
    sourceWallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    issuerWallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    asset = await AssetTestUtil.generateAsset({ wallet: issuerWallet, partial: { provider: CustodyProvider.STELLAR } });
  });

  afterEach(() => {
    nextStub.resetHistory();
  });

  afterAll(async () => {
    await AssetTestUtil.destroyAsset(asset, false);
    await UserTestUtil.destroyUser(user);

    await server.close();
    assetServiceUninstalHandle();
  });

  describe('middleware', () => {
    it.skip('Returns 404 if wallet does not exist', async () => {
      const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
        path: '/phone-credits/order/:phoneCreditOrderId/complete',
        method: 'POST',
        body: {
          provider: asset.provider,
          source: faker.random.uuid(),
        },
      });
      let err: HttpError;

      try {
        await hasUserEnoughBalance(req, res, nextStub);
      } catch (e) {
        err = e;
      }

      expect(err.status).toBe(404);
      expect(err.message).toMatch(/Wallet not found/);
      expect(nextStub.notCalled).toBeTruthy();
    });

    it.skip('Returns 404 if asset does not exist', async () => {
      const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
        path: '/phone-credits/order/:phoneCreditOrderId/complete',
        method: 'POST',
        body: {
          provider: CustodyProvider.LECCA_PROVIDER,
        },
      });
      let err: HttpError;

      try {
        await hasUserEnoughBalance(req, res, nextStub);
      } catch (e) {
        err = e;
      }

      expect(err.status).toBe(404);
      expect(err.message).toMatch(/Asset not found/);
      expect(nextStub.notCalled).toBeTruthy();
    });

    it.skip('Returns 400 if asset is not registerd on wallet', async () => {
      const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
        path: '/phone-credits/order/:phoneCreditOrderId/complete',
        method: 'POST',
        body: {
          provider: asset.provider,
          source: sourceWallet.id,
        },
      });
      let err: HttpError;

      try {
        await hasUserEnoughBalance(req, res, nextStub);
      } catch (e) {
        err = e;
      }

      expect(err.status).toBe(400);
      expect(err.message).toMatch(/Asset is not registered on source wallet/);
      expect(nextStub.notCalled).toBeTruthy();
    });

    describe('Asset is registered on wallet', () => {
      beforeAll(async () => {
        await AssetTestUtil.linkWalletToAsset({ wallet: sourceWallet, asset, status: AssetRegistrationStatus.READY });
      });

      afterAll(async () => {
        await AssetTestUtil.unlinkWalletToAsset(asset, sourceWallet);
      });

      it.skip("Returns 412 if the user's balance is not enough to complete the operation", async () => {
        const blockchainBalanceStub = sinon.stub(StellarService.prototype, 'loadAccount').resolves({
          balances: [
            {
              asset_code: asset.code,
              balance: (5).toFixed(7),
            },
          ],
        } as any);

        const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
          path: '/phone-credits/order/:phoneCreditOrderId/complete',
          method: 'POST',
          body: {
            provider: asset.provider,
            source: sourceWallet.id,
            amount: 10.0,
          },
        });
        let err: HttpError;

        try {
          await hasUserEnoughBalance(req, res, nextStub);
        } catch (e) {
          err = e;
        }

        expect(err.status).toBe(412);
        expect(err.message).toMatch(/Wallet does not have enough balance to perform this operation/);
        expect(nextStub.notCalled).toBeTruthy();

        blockchainBalanceStub.restore();
      });

      it.skip("Returns 412 if the user's balance is enough to complete the operation but the authorizableBalance is not", async () => {
        const blockchainBalanceStub = sinon.stub(StellarService.prototype, 'loadAccount').resolves({
          balances: [
            {
              asset_code: asset.code,
              balance: (12).toFixed(7),
            },
          ],
        } as any);

        const payment = await PaymentTestUtil.generatePayment(sourceWallet, issuerWallet, {
          amount: '5',
          status: TransactionStatus.AUTHORIZED,
          type: PaymentType.CARD,
          asset,
        });

        const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
          path: '/phone-credits/order/:phoneCreditOrderId/complete',
          method: 'POST',
          body: {
            provider: asset.provider,
            source: sourceWallet.id,
            amount: 10.0,
          },
        });
        let err: HttpError;

        try {
          await hasUserEnoughBalance(req, res, nextStub);
        } catch (e) {
          err = e;
        } finally {
          await PaymentTestUtil.destroyPayment(payment);
          blockchainBalanceStub.restore();
        }

        expect(err.status).toBe(412);
        expect(err.message).toMatch(/Wallet does not have enough balance to perform this operation/);
        expect(nextStub.notCalled).toBeTruthy();
      });

      it.skip('Calls next if the authorized balance is enought to execute the operation', async () => {
        const blockchainBalanceStub = sinon.stub(StellarService.prototype, 'loadAccount').resolves({
          balances: [
            {
              asset_code: asset.code,
              balance: (17).toFixed(7),
            },
          ],
        } as any);

        const payment = await PaymentTestUtil.generatePayment(sourceWallet, issuerWallet, {
          amount: '5',
          status: TransactionStatus.AUTHORIZED,
          type: PaymentType.CARD,
          asset,
        });

        const { req, res } = HttpMocks.createMocks<bacenRequest, BaseResponse>({
          path: '/phone-credits/order/:phoneCreditOrderId/complete',
          method: 'POST',
          body: {
            provider: asset.provider,
            source: sourceWallet.id,
            amount: 10.0,
          },
        });

        try {
          await hasUserEnoughBalance(req, res, nextStub);
        } finally {
          await PaymentTestUtil.destroyPayment(payment);
          blockchainBalanceStub.restore();
        }

        expect(nextStub.calledOnce).toBeTruthy();
      });
    });
  });
});
