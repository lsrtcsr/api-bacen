import { UnleashUtil } from '@bacen/shared-sdk';
import * as sinon from 'sinon';
import * as faker from 'faker';
import * as HttpMock from 'node-mocks-http';
import { HttpError, BaseResponse, BaseRequest } from 'ts-framework';
import { isPhysicalCardEmissionEnabled } from '../../../api/filters/feature';
import { Wallet, Asset } from '../../../api/models';

describe('api.filters.feature.PhysicalCard', () => {
  let findWalletStub: sinon.SinonStub;
  let findAssetStub: sinon.SinonStub;
  let unleashStub: sinon.SinonStub;
  let requestMock: BaseRequest;
  let responseMock: BaseResponse;

  beforeAll(() => {
    const walletId = faker.random.uuid();
    const assetCode = faker.finance.currencyCode();
    findWalletStub = sinon
      .stub(Wallet, 'findOne')
      .resolves({ id: walletId, user: { id: faker.random.uuid(), domainId: faker.random.uuid() } } as any);
    findAssetStub = sinon.stub(Asset, 'getByIdOrCode').resolves({ code: assetCode } as any);
    ({ req: requestMock, res: responseMock } = HttpMock.createMocks({
      path: '/wallets/:walletId/cards/physical',
      method: 'POST',
      params: { walletId },
      body: {
        asset: assetCode,
      },
    }));
  });

  afterEach(() => {
    if (unleashStub) {
      unleashStub.restore();
    }
  });

  afterAll(() => {
    findWalletStub.restore();
    findAssetStub.restore();
  });

  describe('middleware', () => {
    it('Returns 400 if the feature flag is enabled', async () => {
      unleashStub = sinon.stub(UnleashUtil, 'isEnabled').returns(true);
      let err: HttpError;
      const next = sinon.stub();

      try {
        await isPhysicalCardEmissionEnabled(requestMock as any, responseMock as any, next);
      } catch (e) {
        err = e;
      }

      expect(err).toBeDefined();
      expect(err.status).toBe(400);
      expect(err.message).toMatch(/Physical card emission is disabled/i);
      expect(next.notCalled).toBeTruthy();
    });

    it('Calls the next middleware if the feature flag is not enabled', async () => {
      unleashStub = sinon.stub(UnleashUtil, 'isEnabled').returns(false);
      let err: HttpError;
      const next = sinon.stub();

      await isPhysicalCardEmissionEnabled(requestMock as any, responseMock as any, next);
      expect(next.called).toBeTruthy();
    });
  });
});
