import * as httpMocks from 'node-mocks-http';
import { spy } from 'sinon';
import { BaseResponse } from 'ts-framework';
import { OAuth } from '../../api/filters';
import MainServer from '../../api/MainServer';
import { bacenRequest, OAuthOTPToken, User } from '../../api/models';
import { ConsumerTestUtil, ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 10000);

describe('api.MainServer', () => {
  let user: User;
  let otp: OAuthOTPToken;
  let server: MainServer;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  beforeEach(async () => {
    user = await ConsumerTestUtil.generateConsumer();
    otp = await OAuthOTPToken.generate(user);
    assetServiceUninstalHandle();
  });

  afterEach(async () => {
    if (otp) {
      await OAuthOTPToken.remove(otp);
    }
    if (user) {
      await ConsumerTestUtil.destroyConsumer(user);
    }
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }
  });

  it('should ignore OTP for an existing user', async () => {
    const request: bacenRequest = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
    });

    // Mock user with disabled two factor
    request.user = { ...user, twoFactorSecret: undefined, twoFactorRequired: false } as User;

    const response = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    await expect(OAuth.otp()(request, response, next)).resolves.not.toThrow();
    expect(next.calledOnce).toBe(true);
  });

  it('should reject empty OTP for an existing user with valid configuration', async () => {
    const request = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
    });

    // Mock user with enabled two factor
    request.user = { ...user, twoFactorSecret: otp, twoFactorRequired: true } as User;

    const response = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    await expect(OAuth.otp()(request, response, next)).rejects.toThrow(/you need a two factor OTP token/gi);
    expect(next.called).toBe(false);
  });

  it('should reject invalid OTP for an existing user with valid configuration', async () => {
    const request = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      headers: {
        'x-bacen-otp': '123456',
      },
    });

    // Mock user with enabled two factor
    request.user = { ...user, twoFactorSecret: otp, twoFactorRequired: true } as User;

    const response = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    await expect(OAuth.otp()(request, response, next)).rejects.toThrow(/Invalid or expired OTP token/gi);
    expect(next.called).toBe(false);
  });

  it('should accept valid OTP for an existing user with valid configuration', async () => {
    const request = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      headers: {
        'x-bacen-otp': await otp.token(),
      },
    });

    // Mock user with enabled two factor
    request.user = { ...user, twoFactorSecret: otp, twoFactorRequired: true } as User;

    const response = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    await expect(OAuth.otp()(request, response, next)).resolves.not.toThrow();
    expect(next.called).toBe(true);
  });

  // describe('validating with another user otp', () => {
  //   let otherUser: User;
  //   let otherOtp: OAuthOTPToken;

  //   beforeEach(async () => {
  //     otherUser = await ConsumerTestUtil.generateConsumer();
  //     otherOtp = await OAuthOTPToken.generate(user);
  //   });

  //   afterEach(async () => {
  //     if (otp) {
  //       await OAuthOTPToken.remove(otp);
  //     }
  //     if (user) {
  //       await ConsumerTestUtil.destroyConsumer(user);
  //     }
  //   });

  //   it('should accept valid OTP for an existing user with valid configuration', async () => {
  //     const request = httpMocks.createRequest<bacenRequest>({
  //       method: 'POST',
  //       url: '/payments',
  //       headers: {
  //         'x-bacen-otp': await otp.token()
  //       }
  //     });

  //     // Mock user with enabled two factor
  //     request.user = { ...user, twoFactorSecret: otp, twoFactorRequired: true } as User;

  //     const response = httpMocks.createResponse<BaseResponse>();
  //     const next = spy();

  //     await expect(OAuth.otp()(request, response, next)).resolves.not.toThrow();
  //     expect(next.called).toBe(true);
  //   });
  // })
});
