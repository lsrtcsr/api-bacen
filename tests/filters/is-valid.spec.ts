import * as httpMocks from 'node-mocks-http';
import { restore, spy, stub } from 'sinon';
import { BaseResponse } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { isValidAmount } from '../../api/filters/amount/is-valid';
import { bacenRequest } from '../../api/models';

describe('api.filters.amount.isValid', () => {
  afterEach(() => restore());

  it('should accept valid integer string', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: '1' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    isValidAmount(req, res, next);
    expect(next.calledOnce).toBeTruthy();
  });

  it('should accept valid float string', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: '1.234567' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    isValidAmount(req, res, next);
    expect(next.calledOnce).toBeTruthy();
  });

  it('should accept valid number strings in recipients array', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: {
        recipients: [{ amount: '1' }, { amount: '100.01' }, { amount: '1.234567' }],
      },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    isValidAmount(req, res, next);
    expect(next.calledOnce).toBeTruthy();
  });

  it('should accept valid number if flag is not enabled', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: 1.234567 },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    // Mock flag util
    stub(UnleashUtil, 'isEnabled').returns(false);

    isValidAmount(req, res, next);
    expect(next.calledOnce).toBeTruthy();
  });

  it('should deny invalid number string', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: 'abc' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    try {
      isValidAmount(req, res, next);
      fail();
    } catch (exception) {
      expect(exception.message).toMatch(/invalid parameter or body field/gi);
    }
    expect(next.calledOnce).toBeFalsy();
  });

  it('should deny invalid number in recipients array', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: {
        recipients: [{ amount: '1' }, { amount: '100.01' }, { amount: '1.234567' }, { amount: 'abc' }],
      },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    try {
      isValidAmount(req, res, next);
      fail();
    } catch (exception) {
      expect(exception.message).toMatch(/invalid parameter or body field/gi);
    }
    expect(next.calledOnce).toBeFalsy();
  });

  it('should deny number string with decimals exceeding max', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: '1.2345678901234567890' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    try {
      isValidAmount(req, res, next);
      fail();
    } catch (exception) {
      expect(exception.message).toMatch(/invalid parameter or body field/gi);
    }
    expect(next.calledOnce).toBeFalsy();
  });

  it('should deny invalid number if flag is enabled', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: 'abc' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    // Mock flag util
    stub(UnleashUtil, 'isEnabled').returns(true);

    try {
      isValidAmount(req, res, next);
      fail();
    } catch (exception) {
      expect(exception.message).toMatch(/invalid parameter or body field/gi);
    }
    expect(next.calledOnce).toBeFalsy();
  });

  it('should deny invalid mixed string', async () => {
    const req = httpMocks.createRequest<bacenRequest>({
      method: 'POST',
      url: '/payments',
      body: { amount: '10/r' },
    });

    const res = httpMocks.createResponse<BaseResponse>();
    const next = spy();

    try {
      isValidAmount(req, res, next);
      fail();
    } catch (exception) {
      expect(exception.message).toMatch(/invalid parameter or body field/gi);
    }
    expect(next.calledOnce).toBeFalsy();
  });
});
