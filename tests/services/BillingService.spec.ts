import {
  AssetRegistrationStatus,
  ConsumerStatus,
  CustodyProvider,
  PaymentType,
  TransactionStatus,
  TransactionType,
  UserStatus,
  WalletStatus,
  CustodyPaymentWebService,
  PaymentStatus,
} from '@bacen/base-sdk';
import * as faker from 'faker';
import * as sinon from 'sinon';
import MainServer from '../../api/MainServer';
import { Asset, Domain, Payment, Transaction, User, Wallet } from '../../api/models';
import { BillingService, ProviderManagerService } from '../../api/services';
import {
  ConsumerTestUtil,
  DomainTestUtil,
  MockLogger,
  ServerTestUtil,
  WalletTestUtil,
  UserTestUtil,
  TransactionTestUtil,
  AssetTestUtil,
  AssetServiceMock,
  UninstalHandle,
} from '../util';
import Config from '../../config';
import { ProviderUtil } from '../../api/utils';

jest.setTimeout(60000);

describe('api.services.BillingService', () => {
  let assetServiceMockUninstalHandle: UninstalHandle;
  let server: MainServer;
  let service: BillingService;
  let logger: MockLogger;
  let domain: Domain;
  let issuer: User;
  let issuerWallet: Wallet;
  let user: User;
  let mediator: User;
  let userWallet: Wallet;
  let serviceFeeWallet: Wallet;
  let throwIfFeatureNotAvailableStub: sinon.SinonStub;

  beforeAll(async () => {
    assetServiceMockUninstalHandle = AssetServiceMock.install();
    logger = new MockLogger();
    server = await ServerTestUtil.init();
    BillingService.initialize({ logger: logger as any });
    service = BillingService.getInstance();

    domain = await DomainTestUtil.generateDomain();
    user = await ConsumerTestUtil.generateConsumer({
      userDomain: domain,
      userState: UserStatus.ACTIVE,
      consumerState: ConsumerStatus.READY,
    });
    mediator = await ConsumerTestUtil.generateConsumer({
      userDomain: domain,
      userState: UserStatus.ACTIVE,
      consumerState: ConsumerStatus.READY,
    });
    issuer = await ConsumerTestUtil.generateConsumer({
      userDomain: domain,
      userState: UserStatus.ACTIVE,
      consumerState: ConsumerStatus.READY,
    });
    userWallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    serviceFeeWallet = await WalletTestUtil.generateWalletAtState(mediator, WalletStatus.READY);
    issuerWallet = await WalletTestUtil.generateWalletAtState(issuer, WalletStatus.READY);
    throwIfFeatureNotAvailableStub = sinon.stub(ProviderUtil, 'throwIfFeatureNotAvailable');
  });

  afterAll(async () => {
    throwIfFeatureNotAvailableStub.restore();

    await UserTestUtil.destroyUser(issuer);
    await UserTestUtil.destroyUser(user);
    await UserTestUtil.destroyUser(mediator);
    await DomainTestUtil.destroyDomain(domain);

    await server.close();
    assetServiceMockUninstalHandle();
  });

  afterEach(() => {
    logger.clearAll();
  });

  describe('chargeServiceFee', () => {
    let providerPaymentStub: sinon.SinonStub;
    let transaction: Transaction;
    let assetWithControlledBalance: Asset;
    let assetWithoutControlledBalance: Asset;
    let providerManagerFromStub: sinon.SinonStub;

    const providerWithControlledBalance = faker.random.arrayElement(
      (Object.values(CustodyProvider) as CustodyProvider[]).filter(provider =>
        Config.asset.controlledBalanceProviders.includes(provider),
      ),
    );
    const providerWithoutControlledBalance = faker.random.arrayElement(
      (Object.values(CustodyProvider) as CustodyProvider[]).filter(
        provider => !Config.asset.controlledBalanceProviders.includes(provider),
      ),
    );

    beforeAll(async () => {
      assetWithControlledBalance = await AssetTestUtil.generateAsset({
        wallet: issuerWallet,
        partial: { provider: providerWithControlledBalance },
      });
      assetWithoutControlledBalance = await AssetTestUtil.generateAsset({
        wallet: issuerWallet,
        partial: { provider: providerWithoutControlledBalance },
      });

      await AssetTestUtil.linkWalletToAsset({
        wallet: userWallet,
        asset: assetWithControlledBalance,
        status: AssetRegistrationStatus.READY,
      });
      await AssetTestUtil.linkWalletToAsset({
        wallet: serviceFeeWallet,
        asset: assetWithControlledBalance,
        status: AssetRegistrationStatus.READY,
      });
      await AssetTestUtil.linkWalletToAsset({
        wallet: userWallet,
        asset: assetWithoutControlledBalance,
        status: AssetRegistrationStatus.READY,
      });
      await AssetTestUtil.linkWalletToAsset({
        wallet: serviceFeeWallet,
        asset: assetWithoutControlledBalance,
        status: AssetRegistrationStatus.READY,
      });

      providerPaymentStub = sinon
        .stub()
        .callsFake(async payments => payments.map(payment => ({ id: payment.id, status: PaymentStatus.SETTLED })));

      providerManagerFromStub = sinon.stub(ProviderManagerService.prototype, 'from').returns({
        feature: () => ({ payment: providerPaymentStub }),
      } as any);
    });

    afterAll(async () => {
      providerManagerFromStub.restore();

      await AssetTestUtil.unlinkWalletToAsset(assetWithControlledBalance, userWallet);
      await AssetTestUtil.unlinkWalletToAsset(assetWithControlledBalance, serviceFeeWallet);
      await AssetTestUtil.unlinkWalletToAsset(assetWithoutControlledBalance, userWallet);
      await AssetTestUtil.unlinkWalletToAsset(assetWithoutControlledBalance, serviceFeeWallet);

      await AssetTestUtil.destroyAsset(assetWithControlledBalance);
      await AssetTestUtil.destroyAsset(assetWithoutControlledBalance);
    });

    beforeEach(async () => {
      transaction = await TransactionTestUtil.generateForWallet(userWallet, {
        type: TransactionType.PAYMENT,
        status: TransactionStatus.AUTHORIZED,
      });
    });

    afterEach(async () => {
      providerPaymentStub.resetHistory();
      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Authorizes the service fee on the provider for an asset without controlled balance', async () => {
      console.log(`Start Time - GetServiceFee: ${Date.now()}`);

      const feePayment = Payment.create({
        amount: faker.finance.amount(),
        destination: serviceFeeWallet,
        asset: assetWithoutControlledBalance,
        type: PaymentType.SERVICE_FEE,
      });

      const feeTransaction = await service.chargeServiceFee(feePayment, transaction);

      console.log(`Start End - GetServiceFee: ${Date.now()}`);
      expect(feeTransaction.payments.length).toBe(1);
      expect(feeTransaction.payments[0].type).toBe(PaymentType.SERVICE_FEE);
      expect(providerPaymentStub.calledOnce).toBe(true);
    });

    it('Skips authorizing the service fee on the provider for an asset with controlled balance', async () => {
      const feePayment = Payment.create({
        amount: faker.finance.amount(),
        destination: serviceFeeWallet,
        asset: assetWithControlledBalance,
        type: PaymentType.SERVICE_FEE,
      });

      const feeTransaction = await service.chargeServiceFee(feePayment, transaction);

      expect(feeTransaction.payments.length).toBe(1);
      expect(feeTransaction.payments[0].type).toBe(PaymentType.SERVICE_FEE);
      expect(providerPaymentStub.called).toBe(false);
    });
  });
});
