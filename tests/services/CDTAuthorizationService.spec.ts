import { WalletStatus, TransactionStatus } from '@bacen/base-sdk';
import * as faker from 'faker';
import * as sinon from 'sinon';
import {
  ServerTestUtil,
  UserTestUtil,
  WalletTestUtil,
  PaymentTestUtil,
  AssetTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../util';
import MainServer from '../../api/MainServer';
import { User, Wallet, Asset, Transaction } from '../../api/models';
import { CDTAuthorizationService, AuthorizationService, CDTAuthorizationResponseCodes } from '../../api/services';

jest.setTimeout(60000);
jest.useFakeTimers();

describe('api.services.CDTAuthorizationService', () => {
  let service: CDTAuthorizationService;
  let server: MainServer;
  let issuerUser: User;
  let user: User;
  let issuerWallet: Wallet;
  let wallet: Wallet;
  let asset: Asset;
  let authorizationServiceStub: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();

    service = CDTAuthorizationService.getInstance();

    issuerUser = await UserTestUtil.generateUser();
    user = await UserTestUtil.generateUser();
    issuerWallet = await WalletTestUtil.generateWalletAtState(issuerUser, WalletStatus.READY);
    wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
    asset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });

    authorizationServiceStub = sinon.stub(AuthorizationService.prototype, 'authorize').resolves({ authorized: true });
  });

  afterAll(async () => {
    authorizationServiceStub.restore();

    await AssetTestUtil.destroyAsset(asset, false);
    await UserTestUtil.destroyUser(user);
    await UserTestUtil.destroyUser(issuerUser);
    await server.close();
    assetServiceUninstallHandle();
  });

  describe('revert', () => {
    it('Does not authorize a reversal of a reversed transaction', async () => {
      const reversedTransaction = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
        status: TransactionStatus.REVERSED,
        asset,
        amount: faker.finance.amount(),
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      try {
        const authorizationResponse = await service.revert(
          (reversedTransaction.transaction.additionalData as any).externalId,
        );
        expect(authorizationResponse.result).toBe(CDTAuthorizationResponseCodes.CANCELED_TRANSACTION);
      } finally {
        await Transaction.delete(reversedTransaction.transaction.id);
      }
    });

    it('Does not authorize a reversal of an executed transaction which already has a reversal', async () => {
      const executedTransaction = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
        status: TransactionStatus.EXECUTED,
        asset,
        amount: faker.finance.amount(),
        additionalData: {
          hash: faker.finance.bitcoinAddress(),
          externalId: faker.random.uuid(),
        },
      });

      const transactionReversal = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
        status: TransactionStatus.EXECUTED,
        asset,
        amount: executedTransaction.amount,
        additionalData: {
          reversal: true,
          originalTransaction: executedTransaction.transaction.additionalData.hash,
        },
      });

      try {
        const authorizationResponse = await service.revert(
          (executedTransaction.transaction.additionalData as any).externalId,
        );
        expect(authorizationResponse.result).toBe(CDTAuthorizationResponseCodes.CANCELED_TRANSACTION);
      } finally {
        await Transaction.delete(executedTransaction.transaction.id);
        await Transaction.delete(transactionReversal.transaction.id);
      }
    });
  });
});
