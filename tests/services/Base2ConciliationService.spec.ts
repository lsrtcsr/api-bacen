import {
  ResolvedBase2Record,
  TransactionTypeFriendly as Base2TransactionType,
  TransactionStatus,
  TransactionType,
  TransitoryAccountType,
  PaymentType,
  PaymentStatus,
  CustodyPaymentWebService,
  TransactionTypeFriendly,
  DomainRole,
  UserRole,
  ConsumerStatus,
  UserStatus,
  WalletStatus,
} from '@bacen/base-sdk';
import * as path from 'path';
import MainServer from '../../api/MainServer';
import { Base2ConciliationService, TransactionPipelinePublisher, IssueHandler } from '../../api/services';
import {
  WalletTestUtil,
  UserTestUtil,
  TransactionTestUtil,
  MockLogger,
  mockMessage,
  ServerTestUtil,
  AssetServiceMock,
  UninstalHandle,
  DomainTestUtil,
  ConsumerTestUtil,
  AssetTestUtil,
} from '../util';
import { Wallet, User, Transaction, Payment, Asset, Base2HandledTransaction, Domain } from '../../api/models';
import { ProviderUtil } from '../../api/utils';

import sinon = require('sinon');
import faker = require('faker');

jest.setTimeout(100000);

const logger = new MockLogger();

describe.skip('api.services.Base2ConciliationService', () => {
  let server: MainServer;
  let service: Base2ConciliationService;
  let wallet: Wallet;
  let user: User;
  let rootDomain: Domain;
  let rootMediator: User;
  let issuerWallet: Wallet;
  let defaultAuthorizableAsset: Asset;
  let transactionPublisherMock: sinon.SinonStub;
  let paymentFeatureMock: sinon.SinonStub;
  let walletBalanceMock: sinon.SinonStub;
  let issueHandlerMock: sinon.SinonStub;
  let throwIfFeatureNotAvailableMock: sinon.SinonStub;
  let pubsubPublishMock: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  function getAmount(transaction: Transaction): string {
    return transaction.payments!.reduce((total, current) => total + Number(current.amount), 0).toFixed(2);
  }

  function createRecordReversal(uuid: string, type?: Base2TransactionType) {
    return {
      externalId: uuid,
      amount: faker.commerce.price(),
      walletId: wallet.id,
      type:
        type ||
        faker.random.arrayElement([
          Base2TransactionType.CREDIT_VOUCHER_REVERSAL,
          Base2TransactionType.PURCHASE_REVERSAL,
          Base2TransactionType.WITHDRAW_REVERSAL,
        ]),
    } as ResolvedBase2Record;
  }

  function createRecordConfirmation(uuid: string, amount?: string, type?: Base2TransactionType) {
    return {
      externalId: uuid,
      amount: amount || faker.commerce.price(),
      walletId: wallet.id,
      type: type || faker.random.arrayElement([Base2TransactionType.PURCHASE, Base2TransactionType.WITHDRAW]),
    } as ResolvedBase2Record;
  }

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    service = new Base2ConciliationService({ responseTopic: 'test_topic', logger: logger as any });

    rootDomain = await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
    rootMediator = await ConsumerTestUtil.generateConsumer({
      userDomain: rootDomain,
      role: UserRole.MEDIATOR,
      consumerState: ConsumerStatus.READY,
      userState: UserStatus.ACTIVE,
    });

    issuerWallet = await WalletTestUtil.generateWalletAtState(rootMediator, WalletStatus.READY, {
      isTransitoryAccount: true,
      transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
    });

    defaultAuthorizableAsset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: require(path.join(__dirname, '../__mock__/asset/rootAssetConfig.json')),
    });

    user = await UserTestUtil.generateUser();
    wallet = await WalletTestUtil.generate(user);

    throwIfFeatureNotAvailableMock = sinon.stub(ProviderUtil, 'throwIfFeatureNotAvailable');
    transactionPublisherMock = sinon.stub(TransactionPipelinePublisher.prototype, 'send');
    issueHandlerMock = sinon.stub(IssueHandler.prototype, 'handle');
    pubsubPublishMock = sinon.stub(Base2ConciliationService.prototype, 'publishResponse');
    paymentFeatureMock = sinon.stub(CustodyPaymentWebService.prototype, 'payment').callsFake(() => {
      return Promise.resolve([
        {
          id: faker.random.uuid(),
          status: PaymentStatus.SETTLED,
        },
      ]);
    });
    walletBalanceMock = sinon.stub(Wallet.prototype, 'getAssetBalance').callsFake(() => {
      return Promise.resolve(faker.commerce.price());
    });
  });

  afterAll(async () => {
    transactionPublisherMock.restore();
    paymentFeatureMock.restore();
    issueHandlerMock.restore();
    walletBalanceMock.restore();
    pubsubPublishMock.restore();
    throwIfFeatureNotAvailableMock.restore();

    await UserTestUtil.destroyUser(user);
    await server.close();

    await AssetTestUtil.destroyAsset(defaultAuthorizableAsset, false);
    await UserTestUtil.destroyUser(rootMediator);
    await DomainTestUtil.destroyDomain(rootDomain);
    assetServiceUninstallHandle();
  });

  afterEach(() => {
    logger.clearAll();
    transactionPublisherMock.resetHistory();
    paymentFeatureMock.resetHistory();
    issueHandlerMock.resetHistory();
    walletBalanceMock.resetHistory();
  });

  describe('messageHandler', () => {
    let handleReversionsStub: sinon.SinonStub;
    let handleConfirmationsStub: sinon.SinonStub;
    let handleCreditVoucherStub: sinon.SinonStub;

    beforeAll(() => {
      handleReversionsStub = sinon.stub(service, 'handleReversion');
      handleConfirmationsStub = sinon.stub(service, 'handleConfirmation');
      handleCreditVoucherStub = sinon.stub(service, 'handleCreditVoucher');
    });

    afterAll(() => {
      handleReversionsStub.restore();
      handleConfirmationsStub.restore();
      handleCreditVoucherStub.restore();
    });

    afterEach(() => {
      handleReversionsStub.resetHistory();
      handleConfirmationsStub.resetHistory();
      handleCreditVoucherStub.resetHistory();
    });

    it('handles a duplicated message', async () => {
      const duplicatedRecord = createRecordConfirmation(faker.random.uuid());
      const message = mockMessage(duplicatedRecord);
      const persistedTransaction = await Base2HandledTransaction.insertAndFind({
        type: duplicatedRecord.type,
        confirmedAmount: duplicatedRecord.amount,
        originalAmount: duplicatedRecord.amount,
        externalId: duplicatedRecord.externalId,
        additionalData: {
          raw: duplicatedRecord.raw,
        },
      });

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(false);
      expect(handleConfirmationsStub.called).toBe(false);
      expect(handleCreditVoucherStub.called).toBe(false);
      expect(message.ack.called).toBe(true);

      await persistedTransaction.remove();
    });

    it('handles a PURCHASE_REVERSAL', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.PURCHASE_REVERSAL);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(true);
      expect(handleConfirmationsStub.called).toBe(false);
      expect(handleCreditVoucherStub.called).toBe(false);
      expect(message.ack.called).toBe(true);
    });

    it('handles a WITHDRAW_REVERSAL', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.WITHDRAW_REVERSAL);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(true);
      expect(handleConfirmationsStub.called).toBe(false);
      expect(message.ack.called).toBe(true);
    });

    it('handles a CREDIT_VOUCHER_REVERSAL', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.CREDIT_VOUCHER_REVERSAL);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(true);
      expect(handleConfirmationsStub.called).toBe(false);
      expect(handleCreditVoucherStub.called).toBe(false);
      expect(message.ack.called).toBe(true);
    });

    it('ignores a WITHDRAW', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.WITHDRAW);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(false);
      expect(handleConfirmationsStub.called).toBe(true);
      expect(handleCreditVoucherStub.called).toBe(false);
      expect(message.ack.called).toBe(true);
    });

    it('ignores a PURCHASE', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.PURCHASE);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(false);
      expect(handleConfirmationsStub.called).toBe(true);
      expect(handleCreditVoucherStub.called).toBe(false);
      expect(message.ack.called).toBe(true);
    });

    it('ignores a CREDIT_VOUCHER', async () => {
      const record = createRecordReversal(faker.random.uuid(), Base2TransactionType.CREDIT_VOUCHER);
      const message = mockMessage(record);

      await service.messageHandler(message as any);
      expect(handleReversionsStub.called).toBe(false);
      expect(handleConfirmationsStub.called).toBe(false);
      expect(handleCreditVoucherStub.called).toBe(true);
      expect(message.ack.called).toBe(true);
    });
  });

  describe('handleReversions', () => {
    it('Generates a new reversed transaction for a record not present in the DB', async () => {
      const recordNotInDb = createRecordReversal(faker.random.uuid());
      await service.handleReversion(recordNotInDb);

      const transaction = await Transaction.findByExternalId(recordNotInDb.externalId);
      const totalAmount = transaction.payments.reduce((sum, payment) => sum + Number(payment.amount), 0).toFixed(2);

      expect(transaction).toBeDefined();
      expect(transaction.status).toBe(TransactionStatus.REVERSED);
      expect(totalAmount).toBe(recordNotInDb.amount);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Does nothing if the transaction exists and is already reversed', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.REVERSED,
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordReversal((transaction.additionalData as any).externalId);
      await service.handleReversion(record);

      const debugLogs = logger.get('debug');

      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Transaction ok',
        structuredData: {
          externalId: record.externalId,
          type: record.type,
          amount: record.amount,
        },
      });

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Reverses the transaction if it exists and is authorized', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.AUTHORIZED,
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordReversal((transaction.additionalData as any).externalId);
      await service.handleReversion(record);

      const reloadedTransaction = await Transaction.findByExternalId((transaction.additionalData as any).externalId);
      expect(reloadedTransaction.status).toBe(TransactionStatus.REVERSED);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Does nothing if the transaction exists, is settled and has a settled reversal', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: faker.random.arrayElement([TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED]),
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
          hash: faker.finance.bitcoinAddress(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const transitoryWallet = await Wallet.getTransitoryAccountByType({
        type: TransitoryAccountType.CARD_TRANSACTION,
      });
      const reversal = await TransactionTestUtil.generateForWallet(transitoryWallet, {
        status: faker.random.arrayElement([TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED]),
        type: TransactionType.PAYMENT,
        additionalData: {
          originalTransaction: transaction.additionalData.hash,
        },
      });

      const record = createRecordReversal((transaction.additionalData as any).externalId);
      await service.handleReversion(record);

      const debugLogs = logger.get('debug');

      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Transaction ok',
        structuredData: {
          externalId: record.externalId,
          type: record.type,
          amount: record.amount,
        },
      });

      await TransactionTestUtil.destroyTransaction(transaction);
      await TransactionTestUtil.destroyTransaction(reversal);
    });

    it('Publishes the reversal to the transaction queue if the transaction exists and has an authorized reversal', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: faker.random.arrayElement([TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED]),
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
          hash: faker.finance.bitcoinAddress(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const transitoryWallet = await Wallet.getTransitoryAccountByType({
        type: TransitoryAccountType.CARD_TRANSACTION,
      });
      const reversal = await TransactionTestUtil.generateForWallet(transitoryWallet, {
        status: TransactionStatus.AUTHORIZED,
        type: TransactionType.PAYMENT,
        additionalData: {
          originalTransaction: transaction.additionalData.hash,
        },
      });

      const record = createRecordReversal((transaction.additionalData as any).externalId);
      await service.handleReversion(record);

      expect(transactionPublisherMock.lastCall.args[0].id).toBe(reversal.id);

      await TransactionTestUtil.destroyTransaction(transaction);
      await TransactionTestUtil.destroyTransaction(reversal);
    });

    it('Creates an authorized reversal and publishes it to the transaction queue if the transaction is settled and there is no reversal', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: faker.random.arrayElement([TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED]),
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
          hash: faker.finance.bitcoinAddress(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordReversal((transaction.additionalData as any).externalId);
      await service.handleReversion(record);

      const reversal = await Transaction.findTransactionReversal(transaction);

      expect(reversal.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.lastCall.args[0].id).toBe(reversal.id);

      await TransactionTestUtil.destroyTransaction(transaction);
      await TransactionTestUtil.destroyTransaction(reversal);
    });
  });

  describe('handleConfirmations', () => {
    it('Publishes transaction if it exists, has same value and is notified', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.NOTIFIED,
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordConfirmation((transaction.additionalData as any).externalId, payment.amount);
      await service.handleConfirmation(record);

      const debugLogs = logger.get('debug');

      expect(transactionPublisherMock.lastCall.args[0].id).toBe(transaction.id);
      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Successfully processed transaction from base2',
        structuredData: {
          externalId: record.externalId,
          type: record.type,
          amount: record.amount,
        },
      });

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Publishes transaction if it exists, has same value and is executed', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.EXECUTED,
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordConfirmation((transaction.additionalData as any).externalId, payment.amount);
      await service.handleConfirmation(record);

      const debugLogs = logger.get('debug');

      expect(transactionPublisherMock.lastCall.args[0].id).toBe(transaction.id);
      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Successfully processed transaction from base2',
        structuredData: {
          externalId: record.externalId,
          type: record.type,
          amount: record.amount,
        },
      });
    });

    it('Publishes transaction if it exists, has same value and is authorized', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.AUTHORIZED,
        type: TransactionType.PAYMENT,
        additionalData: {
          externalId: faker.random.uuid(),
        },
      });

      const payment = await Payment.insertAndFind({
        amount: faker.commerce.price(),
        asset: defaultAuthorizableAsset,
        transaction,
      });

      const record = createRecordConfirmation((transaction.additionalData as any).externalId, payment.amount);
      await service.handleConfirmation(record);

      const debugLogs = logger.get('debug');

      expect(transactionPublisherMock.lastCall.args[0].id).toBe(transaction.id);
      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Successfully processed transaction from base2',
        structuredData: {
          externalId: record.externalId,
          type: record.type,
          amount: record.amount,
        },
      });
    });

    it('Adjusts the transaction value if it exists, is notified but has different value', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.NOTIFIED,
        type: TransactionType.PAYMENT,
        additionalData: {
          hash: faker.finance.bitcoinAddress(),
          externalId: faker.random.uuid(),
        },
      });

      const transitoryWallet = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);
      const amount = faker.commerce.price();

      await Payment.insert({
        type: PaymentType.AUTHORIZED_CARD,
        asset: defaultAuthorizableAsset,
        amount,
        destination: transitoryWallet,
        status: PaymentStatus.SETTLED,
        transaction,
      });

      const record = createRecordConfirmation(
        (transaction.additionalData as any).externalId,
        (Number(amount) + 1).toFixed(2),
      );

      await service.handleConfirmation(record);

      const base2Transaction = await Base2HandledTransaction.findByExternalId(record.externalId);
      const reversalTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.reversal, {
        relations: ['states', 'payments'],
      });
      const adjustmentTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.adjustment, {
        relations: ['states', 'payments'],
      });

      const reversalTransactionAmount = getAmount(reversalTransaction);

      expect(base2Transaction.additionalData.transactions.original).toBe(transaction.id);
      expect(reversalTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(reversalTransactionAmount).toBe(amount);
      expect(adjustmentTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.callCount).toBe(3);
      expect(transactionPublisherMock.args[0][0].id).toBe(transaction.id);
      expect(transactionPublisherMock.args[1][0].id).toBe(reversalTransaction.id);
      expect(transactionPublisherMock.args[2][0].id).toBe(adjustmentTransaction.id);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Adjusts the transaction value if it exists, is executed but has different value', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.EXECUTED,
        type: TransactionType.PAYMENT,
        additionalData: {
          hash: faker.finance.bitcoinAddress(),
          externalId: faker.random.uuid(),
        },
      });

      const transitoryWallet = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);
      const amount = faker.commerce.price();

      await Payment.insert({
        type: PaymentType.AUTHORIZED_CARD,
        asset: defaultAuthorizableAsset,
        amount,
        destination: transitoryWallet,
        status: PaymentStatus.SETTLED,
        transaction,
      });

      const record = createRecordConfirmation(
        (transaction.additionalData as any).externalId,
        (Number(amount) + 1).toFixed(2),
      );

      await service.handleConfirmation(record);

      const base2Transaction = await Base2HandledTransaction.findByExternalId(record.externalId);
      const reversalTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.reversal, {
        relations: ['states', 'payments'],
      });
      const adjustmentTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.adjustment, {
        relations: ['states', 'payments'],
      });

      const reversalTransactionAmount = getAmount(reversalTransaction);

      expect(base2Transaction.additionalData.transactions.original).toBe(transaction.id);
      expect(reversalTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(reversalTransactionAmount).toBe(amount);
      expect(adjustmentTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.callCount).toBe(3);
      expect(transactionPublisherMock.args[0][0].id).toBe(transaction.id);
      expect(transactionPublisherMock.args[1][0].id).toBe(reversalTransaction.id);
      expect(transactionPublisherMock.args[2][0].id).toBe(adjustmentTransaction.id);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Adjusts the transaction value if it exists, is authorized but has different value', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.AUTHORIZED,
        type: TransactionType.PAYMENT,
        additionalData: {
          hash: faker.finance.bitcoinAddress(),
          externalId: faker.random.uuid(),
        },
      });

      const transitoryWallet = await Wallet.getTransitoryAccountByType({
        type: TransitoryAccountType.CARD_TRANSACTION,
      });
      const asset = defaultAuthorizableAsset;

      const amount = faker.commerce.price();

      await Payment.insert({
        amount,
        asset,
        type: PaymentType.AUTHORIZED_CARD,
        destination: transitoryWallet,
        status: PaymentStatus.SETTLED,
        transaction: { id: transaction.id },
      });

      const record = createRecordConfirmation(
        (transaction.additionalData as any).externalId,
        (Number(amount) + 1).toFixed(2),
      );

      await service.handleConfirmation(record);

      const base2Transaction = await Base2HandledTransaction.findByExternalId(record.externalId);
      const reloadedTransaction = await Transaction.findByExternalId(record.externalId);
      const adjustmentTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.adjustment, {
        relations: ['states', 'payments'],
      });

      expect(base2Transaction.additionalData.transactions.original).toBe(transaction.id);
      expect(reloadedTransaction.status).toBe(TransactionStatus.REVERSED);
      expect(adjustmentTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.callCount).toBe(1);
      expect(transactionPublisherMock.args[0][0].id).toBe(adjustmentTransaction.id);

      await TransactionTestUtil.destroyTransaction(transaction);
    });

    it('Creates a new transaction if transaction does not exist', async () => {
      const record = createRecordConfirmation(faker.random.uuid());
      await service.handleConfirmation(record);

      const base2Transaction = await Base2HandledTransaction.findByExternalId(record.externalId);
      const createdTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.created, {
        relations: ['states'],
      });

      expect(createdTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.callCount).toBe(1);
      expect(transactionPublisherMock.lastCall.args[0].id).toBe(createdTransaction.id);
    });

    it('Creates a new transaction if transaction exists and is reversed', async () => {
      const transaction = await TransactionTestUtil.generateForWallet(wallet, {
        status: TransactionStatus.REVERSED,
        type: TransactionType.PAYMENT,
        additionalData: {
          hash: faker.finance.bitcoinAddress(),
          externalId: faker.random.uuid(),
        },
      });

      const transitoryWallet = await Wallet.getTransitoryAccountByType({
        type: TransitoryAccountType.CARD_TRANSACTION,
      });
      const asset = defaultAuthorizableAsset;

      const amount = faker.commerce.price();

      await Payment.insert({
        amount,
        asset,
        type: PaymentType.AUTHORIZED_CARD,
        destination: transitoryWallet,
        status: PaymentStatus.SETTLED,
        transaction: { id: transaction.id },
      });

      const record = createRecordConfirmation((transaction.additionalData as any).externalId);
      await service.handleConfirmation(record);

      const base2Transaction = await Base2HandledTransaction.findByExternalId(record.externalId);
      const createdTransaction = await Transaction.findOne(base2Transaction.additionalData.transactions.confirmation, {
        relations: ['states'],
      });

      expect(base2Transaction.additionalData.transactions.original).toBe(transaction.id);
      expect(createdTransaction.status).toBe(TransactionStatus.AUTHORIZED);
      expect(transactionPublisherMock.callCount).toBe(1);
      expect(transactionPublisherMock.lastCall.args[0].id).toBe(createdTransaction.id);
    });
  });

  describe('handleCreditVoucher', () => {
    it('Emit asset to user', async () => {
      const user = await UserTestUtil.generateUser();
      const wallet = await WalletTestUtil.generate(user);

      const externalId = faker.random.uuid();
      const type = TransactionTypeFriendly.CREDIT_VOUCHER;
      const amount = faker.commerce.price();
      const walletId = wallet.id;

      await service.handleCreditVoucher({ externalId, type, amount, walletId } as ResolvedBase2Record);

      const debugLogs = logger.get('debug');

      expect(debugLogs.length).toBe(1);
      expect(debugLogs[0]).toEqual({
        message: 'Succefully gave credit to user',
        structuredData: {
          externalId,
          type,
          amount,
        },
      });
    });
  });
});
