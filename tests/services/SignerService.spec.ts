import * as sinon from 'sinon';
import * as path from 'path';
import { AssetServiceMock, ServerTestUtil, UninstalHandle } from '../util';
import MainServer from '../../api/MainServer';
import { CacheService, SignerService, SignerServiceOptions } from '../../api/services';

jest.setTimeout(60000);

describe('api.services.SignerService', () => {
  let service: SignerService;
  let server: MainServer;
  let lpoError: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    const options: SignerServiceOptions = {
      channelAccounts: [
        {
          secretSeed: 'tree',
        },
        {
          secretSeed: 'two',
        },
        {
          secretSeed: 'one',
        },
      ],
    };

    SignerService.initialize(options);
    server = await ServerTestUtil.init();
    service = SignerService.getInstance();
  });

  afterAll(async () => {
    assetServiceUninstallHandle();
  });

  describe('Perform roud robin properly for channel accounts', () => {
    it('Run roud robin properly using redis', async () => {
      const channelOne = await service.getNextChannelAccount();
      const channelTwo = await service.getNextChannelAccount();
      const channelTree = await service.getNextChannelAccount();

      expect(channelOne).toBe('one');
      expect(channelTwo).toBe('two');
      expect(channelTree).toBe('tree');
    });

    it('Continue running roud robin properly after a redis failure', async () => {
      lpoError = sinon.stub(CacheService.prototype, 'lpop').throws();

      const channelOne = await service.getNextChannelAccount();
      const channelTwo = await service.getNextChannelAccount();
      const channelTree = await service.getNextChannelAccount();

      expect(channelOne).toBe('one');
      expect(channelTwo).toBe('two');
      expect(channelTree).toBe('tree');
    });
  });
});
