import * as sinon from 'sinon';
import * as faker from 'faker';
import {
  TransactionStatus,
  UserStatus,
  ConsumerStatus,
  CustodyPaymentWebService,
  PaymentStatus,
  WalletStatus,
  CustodyProvider,
  AccountType,
  UnleashFlags,
  TransitoryAccountType,
  DomainRole,
  UserRole,
} from '@bacen/base-sdk';
import { getManager } from 'typeorm';
import { BaseError } from 'nano-errors';
import * as path from 'path';
import { UnleashUtil } from '@bacen/shared-sdk';
import MainServer from '../../api/MainServer';
import {
  AuthorizationService,
  AuthorizationRequestPayload,
  AuthorizerEvent,
  AuthorizationErrorMessage,
  AuthorizerQueryService,
  StellarQueryService,
  TransactionPipelinePublisher,
  AssetService,
  LimitSettingService,
} from '../../api/services';
import { Asset, Wallet, User, Payment, Transaction, UserState, ConsumerState, Domain } from '../../api/models';
import ParatiProviderService from '../../api/services/provider/ParatiProviderService';
import {
  ServerTestUtil,
  WalletTestUtil,
  UninstalHandle,
  UserTestUtil,
  ConsumerTestUtil,
  PaymentTestUtil,
  AssetServiceMock,
  AssetTestUtil,
  DomainTestUtil,
} from '../util';
import { TransactionStateMachine } from '../../api/fsm';
import { AuthorizationRequestDto } from '../../api/schemas/dto/AuthorizationRequestDto';

jest.setTimeout(100000);

describe('api.services.AuthorizationService', () => {
  let issuerWallet: Wallet;
  let authorizableAssets: Asset[];
  let server: MainServer;
  let user: User;
  let rootMediator: User;
  let rootDomain: Domain;
  let wallet: Wallet;
  let service: AuthorizationService;
  let registerProviderStub: sinon.SinonStub;
  let paymentFeatureStub: sinon.SinonStub;
  let stellarQueryServiceStub: sinon.SinonStub;
  let authorizableAssetsStub: sinon.SinonStub;
  let skipLimitsStub: sinon.SinonStub;
  let limitStub: sinon.SinonStub;
  let consumedStub: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    registerProviderStub = sinon
      .stub(ParatiProviderService.prototype, 'register')
      .resolves({ externalId: faker.random.uuid() });

    paymentFeatureStub = sinon.stub(CustodyPaymentWebService.prototype, 'payment').callsFake(() => {
      return Promise.resolve([
        {
          id: faker.random.uuid(),
          status: PaymentStatus.SETTLED,
        },
      ]);
    });

    stellarQueryServiceStub = sinon.stub(StellarQueryService.prototype, 'getBalanceForMultipleAssets').resolves([
      { balance: 100, code: 'BRLP' },
      { balance: 100, code: 'VAVR' },
    ]);

    skipLimitsStub = sinon
      .stub(UnleashUtil, 'isEnabled')
      .callsFake((flag: UnleashFlags, context?: any, fallback?: boolean): boolean => {
        if (flag === UnleashFlags.DISABLE_AUTHORIZER_LIMITS) return false;
        return Boolean(fallback);
      });

    const _user = await UserTestUtil.generateUser({ createWallet: false });
    rootDomain = await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
    rootMediator = await ConsumerTestUtil.generateConsumer({
      userDomain: rootDomain,
      role: UserRole.MEDIATOR,
      consumerState: ConsumerStatus.READY,
    });

    user = await ConsumerTestUtil.generateConsumerForUser({
      user: _user,
      consumerStatus: ConsumerStatus.READY,
      type: AccountType.PERSONAL,
    });
    issuerWallet = await WalletTestUtil.generateWalletAtState(rootMediator, WalletStatus.READY, {
      isTransitoryAccount: true,
      transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
    });
    authorizableAssets = [];

    const rootAsset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: require(path.join(__dirname, '../__mock__/asset/rootAssetConfig.json')),
    });

    const addAsset = require(path.join(__dirname, '../__mock__/asset/initialAssetsConfig.json'));
    const additionalAsset = await AssetTestUtil.generateAsset({
      wallet: issuerWallet,
      partial: addAsset[0],
    });

    authorizableAssets.push(rootAsset);
    authorizableAssets.push(additionalAsset);

    service = AuthorizationService.getInstance();
  });

  afterAll(async () => {
    registerProviderStub.restore();
    stellarQueryServiceStub.restore();
    paymentFeatureStub.restore();
    skipLimitsStub.restore();

    await UserTestUtil.destroyUser(user);
    await UserTestUtil.destroyUser(rootMediator);
    await DomainTestUtil.destroyDomain(rootDomain);
    await server.close();

    assetServiceUninstallHandle();
  });

  describe('authorize', () => {
    let payload: AuthorizationRequestPayload;

    it('throws if an unrecognized event is sent', async () => {
      let err: BaseError;

      const request = {
        event: 'invalidevent',
        asset: authorizableAssets[0].code,
        amount: '50.00',
        walletId: faker.random.uuid(),
        additionalData: {
          merchantName: faker.name.firstName(),
          merchantCode: faker.random.number(1000),
        },
      };

      try {
        await service.authorize(request as any);
      } catch (e) {
        err = e;
      }

      expect(err.originalMessage).toBe(`Unrecognized event ${request.event}`);
    });

    describe('When user, consumer or wallet are not in the correct state', () => {
      it('Denies authorization if user is not in the correct status', async () => {
        const consumerState = await UserState.insertAndFind({
          status: UserStatus.FAILED,
          user,
        });
        const wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);

        const result = await service.authorize({ event: AuthorizerEvent.AUTHORIZATION, walletId: wallet.id } as any);
        expect(result.authorized).toEqual(false);
        expect(result.reason).toContain(AuthorizationErrorMessage.USER_NOT_READY);

        await ConsumerState.delete(consumerState.id);
      });

      it('Denies authorization if consumer is not in the correct status', async () => {
        const consumerState = await ConsumerState.insertAndFind({
          status: ConsumerStatus.SUSPENDED,
          consumer: user.consumer,
        });
        const wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);

        const result = await service.authorize({ event: AuthorizerEvent.AUTHORIZATION, walletId: wallet.id } as any);

        expect(result.authorized).toEqual(false);
        expect(result.reason).toContain(AuthorizationErrorMessage.USER_NOT_READY);

        await consumerState.remove();
      });

      it('Denies authorization if wallet is not in the correct status', async () => {
        const consumerState = await ConsumerState.insertAndFind({
          status: ConsumerStatus.READY,
          consumer: user.consumer,
        });
        const userState = await UserState.insertAndFind({ status: UserStatus.ACTIVE, user });
        const wallet = await WalletTestUtil.generate(user);

        const result = await service.authorize({ event: AuthorizerEvent.AUTHORIZATION, walletId: wallet.id } as any);
        expect(result).toEqual({
          authorized: false,
          reason: [AuthorizationErrorMessage.WALLET_NOT_READY],
        });

        await consumerState.remove();
        await userState.remove();
      });
    });

    describe('AuthorizationService()', () => {
      let consumedStub: sinon.SinonStub;
      let limitStub: sinon.SinonStub;

      beforeAll(async () => {
        consumedStub = sinon.stub(LimitSettingService.prototype, 'getValueConsumed');
        limitStub = sinon.stub(LimitSettingService.prototype, 'getLimits');

        await getManager().transaction(async tx => {
          await tx.insert(UserState, { status: UserStatus.ACTIVE, user });
          await tx.insert(ConsumerState, { status: ConsumerStatus.READY, consumer: user.consumer });
        });

        authorizableAssetsStub = sinon.stub(AuthorizerQueryService.prototype, 'getAuthorizableAssets').resolves([
          {
            id: faker.random.uuid(),
            code: faker.finance.currencyCode(),
            provider: CustodyProvider.CDT_VISA,
            name: faker.finance.currencyName(),
            issuer: issuerWallet,
          },
          {
            id: faker.random.uuid(),
            code: faker.finance.currencyCode(),
            provider: null,
            name: faker.finance.currencyName(),
            issuer: issuerWallet,
          },
        ] as Asset[]);
      });

      beforeEach(async () => {
        wallet = await WalletTestUtil.generateWalletAtState(user, WalletStatus.READY);
      });

      afterEach(async () => {
        consumedStub.resetHistory();
        consumedStub.resetBehavior();
        limitStub.resetHistory();
        limitStub.resetBehavior();

        const { transactions, received: paymentsReceived } = await Wallet.findOne(wallet.id, {
          relations: ['transactions', 'transactions.payments', 'received', 'received.transaction'],
        });

        const paymentsMade = transactions.reduce((acc: Payment[], curr) => acc.concat(curr.payments), []);

        for (const payment of paymentsMade.concat(paymentsReceived)) {
          await PaymentTestUtil.destroyPayment(payment);
        }
      });

      afterAll(() => {
        authorizableAssetsStub.restore();
        consumedStub.restore();
        limitStub.restore();
      });

      describe('When given an authorization request payload', () => {
        beforeEach(() => {
          payload = {
            event: AuthorizerEvent.AUTHORIZATION,
            asset: authorizableAssets[0].code,
            amount: '50.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000),
            },
          };
        });

        it('Does not authorize the transaction for invalid amount', async () => {
          payload.amount = '0.00';

          const authorization = await service.authorize(payload);

          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.BAD_REQUEST],
          });
        });

        it('Does not authorize the transaction if the wallet has insufficient balance', async () => {
          payload.amount = '210.00';

          const authorization = await service.authorize(payload);

          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.INSUFICCIENT_BALANCE],
          });
        });

        it('Does not Authorizes a transaction if limit has exceded even if the wallet has sufficient balance', async () => {
          consumedStub.resolves(10);
          limitStub.resolves(10);

          const authorization = await service.authorize(payload);

          expect(authorization.authorized).toBe(false);
          expect(authorization).toEqual({
            asset: 'BRLP',
            authorized: false,
            reason: [AuthorizationErrorMessage.EXCEEDS_DAILY_LIMIT],
          });
        });

        it('Authorizes a transaction if the wallet has sufficient balance', async () => {
          const authorization = await service.authorize(payload);

          expect(authorization.authorized).toBe(true);
          expect(authorization.transactionId.match(/^([A-F]\d){8}-(([A-F]\d){4}-){3}([A-F]\d){12}$/i));

          const transaction = await Transaction.findOne(authorization.transactionId, { relations: ['states'] });
          expect(transaction).toBeDefined();
          expect(transaction.status).toBe(TransactionStatus.AUTHORIZED);
        });

        it('Does not authorize the transaction if the wallet has insuficcient balance minus pending transactions', async () => {
          await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '100.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          payload.amount = '200.00';

          const authorization = await service.authorize(payload);

          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.PENDING_TRANSACTIONS],
          });
        });

        it('Does not authorize the transaction if an internal error occurs', async () => {
          const errorThrowingStub = sinon
            .stub(AuthorizerQueryService.prototype, 'getWalletPendingDebts')
            .throws('Unknown error');

          await PaymentTestUtil.generatePayment(issuerWallet, wallet, {
            amount: '100.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.PENDING,
          });

          const authorization = await service.authorize(payload);

          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
          });

          errorThrowingStub.restore();
        });

        it('Authorize multiple assets', async () => {
          const payload = {
            event: AuthorizerEvent.AUTHORIZATION,
            asset: `${authorizableAssets[0].code},${authorizableAssets[1].code}`,
            amount: '101.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000).toString(),
            },
          };

          sinon.mock(AssetService.prototype.authorizableAssets);

          consumedStub.resolves(0);
          const prepareTransactionStub = sinon
            .stub(AuthorizerQueryService.prototype, 'prepareTransaction')
            .resolves({ id: faker.random.uuid() } as Transaction);

          const authorization = await service.authorize(payload);

          expect(authorization.authorized).toBe(true);
          prepareTransactionStub.restore();
        });

        it('Does not Authorizes a Authorize multiple assets transaction if limit has exceded even if the wallet has sufficient balance', async () => {
          const payload = {
            event: AuthorizerEvent.AUTHORIZATION,
            asset: `${authorizableAssets[0].code},${authorizableAssets[1].code}`,
            amount: '50.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000).toString(),
            },
          };

          sinon.mock(AssetService.prototype.authorizableAssets);

          consumedStub.resolves(10);
          limitStub.resolves(10);

          const prepareTransactionStub = sinon
            .stub(AuthorizerQueryService.prototype, 'prepareTransaction')
            .resolves({ id: faker.random.uuid() } as Transaction);

          const authorization = await service.authorize(payload);

          expect(authorization).toEqual({
            asset: `${authorizableAssets[0].code}`,
            authorized: false,
            reason: [AuthorizationErrorMessage.EXCEEDS_DAILY_LIMIT],
          });
          consumedStub.restore();
          limitStub.restore();
          prepareTransactionStub.restore();
        });

        it('Fail transaction not authorizable asset', async () => {
          const payload: AuthorizationRequestDto = {
            event: AuthorizerEvent.AUTHORIZATION,
            asset: faker.random.alphaNumeric(4),
            amount: '50.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000).toString(),
            },
          };

          const authorization = await service.authorize(payload as any);

          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.ASSET_NOT_AUTHORIZABLE],
          });
        });
      });

      describe('When given an authorization reversal request payload', () => {
        beforeEach(() => {
          payload = {
            event: AuthorizerEvent.AUTHORIZATION_REVERSAL,
            asset: authorizableAssets[0].code,
            amount: '50.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000),
            },
          };
        });

        it('Expects the referenced transactionId to exist', async () => {
          const response = await service.authorize({ ...payload, transactionId: faker.random.uuid() });
          expect(response).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
          });
        });

        it('Expects the referenced transactionId to be in authorized or pending state', async () => {
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.EXECUTED,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.NON_REVERSIBLE_STATE],
          });
        });

        it('Authorizes the reversal if the transaction is in the correct state', async () => {
          const goToReversedStub = sinon.stub(TransactionStateMachine.prototype, 'goTo').resolves(true);

          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });

          goToReversedStub.restore();

          expect(result).toEqual({
            transactionId: payment.transaction.id,
            authorized: true,
          });

          const transaction = await Transaction.findOne(payment.transaction.id, { relations: ['states'] });
          expect(transaction.deletedAt).toBeDefined();

          goToReversedStub.restore();
        });

        it('Denies the authorization if an internal error occurs', async () => {
          const errorThrowingStub = sinon.stub(TransactionStateMachine.prototype, 'goTo').throws();
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
          });

          errorThrowingStub.restore();
        });
      });

      describe('When given a settlement authorization request payload', () => {
        beforeEach(() => {
          payload = {
            event: AuthorizerEvent.SETTLEMENT,
            asset: authorizableAssets[0].code,
            amount: '50.00',
            walletId: wallet.id,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000),
            },
          };
        });

        it('Expects the referenced transactionId to exist', async () => {
          const response = await service.authorize({ ...payload, transactionId: faker.random.uuid() });
          expect(response).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
          });
        });

        it('Expects the referenced transactionId to be authorized', async () => {
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.PENDING,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.NOT_AUTHORIZED],
          });
        });

        it('Authorizes the settlement if the transaction is in the correct state', async () => {
          // Fund wallet, otherwise settlement fails due to underfunding
          await PaymentTestUtil.generatePayment(issuerWallet, wallet, {
            amount: '100.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.PENDING,
          });

          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: true,
            transactionId: payment.transaction.id,
          });
        });

        it('Denies the authorization if an internal error occurs', async () => {
          const errorThrowingStub = sinon.stub(TransactionPipelinePublisher.prototype, 'send').throws();

          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
          });

          errorThrowingStub.restore();
        });
      });

      describe('When given a refund authorization request payload', () => {
        beforeEach(() => {
          payload = {
            event: AuthorizerEvent.REFUND,
            asset: authorizableAssets[0].code,
            amount: '50.00',
            walletId: wallet.id,
            transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
            additionalData: {
              merchantName: faker.name.firstName(),
              merchantCode: faker.random.number(1000),
            },
          };
        });

        it('Expects the referenced transactionId to exist', async () => {
          const response = await service.authorize({ ...payload, transactionId: faker.random.uuid() });
          expect(response).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
          });
        });

        it('Expects the referenced transactionId to be executed', async () => {
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.PENDING,
          });

          const result = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(result).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.NON_REFUNDABLE_STATE],
          });
        });

        it('Authorizes the refund if the transaction is in the correct state', async () => {
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.EXECUTED,
          });

          const authorization = await service.authorize({ ...payload, transactionId: payment.transaction.id });

          expect(authorization.authorized).toBe(true);
          expect(authorization.transactionId.match(/^([A-F]\d){8}-(([A-F]\d){4}-){3}([A-F]\d){12}$/i));

          const transaction = await Transaction.findOne(authorization.transactionId, { relations: ['states'] });
          expect(transaction).toBeDefined();
          expect(transaction.additionalData.reversal).toBe(true);
          expect(transaction.additionalData.originalTransaction).toBe(payment.transaction.additionalData.hash);
          expect(transaction.status).toBe(TransactionStatus.AUTHORIZED);
        });

        it('Denies the authorization if an internal error occurs', async () => {
          const getRootTransitoryAccountWalletThrowinStub = sinon
            .stub(Wallet, 'getRootTransitoryAccountWallet')
            .throws('Unknown error');

          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '50.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.EXECUTED,
          });

          const authorization = await service.authorize({ ...payload, transactionId: payment.transaction.id });
          expect(authorization).toEqual({
            authorized: false,
            reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
          });

          getRootTransitoryAccountWalletThrowinStub.restore();
        });

        it('Dry run denies the authorization when insufficient balance', async () => {
          const payment = await PaymentTestUtil.generatePayment(wallet, issuerWallet, {
            amount: '100150.00',
            asset: authorizableAssets[0],
            status: TransactionStatus.AUTHORIZED,
          });

          const result = await service.dryRun({ ...payload, transactionId: payment.transaction.id });

          expect(result.authorized).toEqual(false);
          expect(result.errors).toBeDefined();
          expect(result.errors[0]).toBeDefined();
          expect(result.errors[0].reason).toEqual(AuthorizationErrorMessage.INSUFICCIENT_BALANCE);
          expect(result.errors[0].explanation).toEqual('Authorizable balance is less than transaction amount');
        });
      });
    });
  });
});
