import { getConnection, QueryRunner } from 'typeorm';
import * as faker from 'faker';
import { PgIdempotenceStore, StoredResponse } from '../../api/services';
import MainDatabase from '../../api/MainDatabase';
import { Request } from '../../api/models';

jest.setTimeout(60_000);

describe('api.services.idempotence.PgStore', () => {
  const db: MainDatabase = MainDatabase.getInstance();
  let qr: QueryRunner;
  let store: PgIdempotenceStore;

  beforeAll(async () => {
    await db.connect();
  });

  afterAll(async () => {
    await db.disconnect();
  });

  beforeEach(() => {
    qr = getConnection().createQueryRunner();
    store = new PgIdempotenceStore(qr);
  });

  afterEach(async () => {
    try {
      // qr may already be released by running the unlock method
      await qr.release();
    } catch {}
  });

  describe('lock', () => {
    it('Successfully locks a resource', async () => {
      const key = faker.random.uuid();
      const didLock = await store.lock(key);

      expect(didLock).toBe(true);
    });

    it('returns false if the resource is already locked by a different session', async () => {
      const key = faker.random.uuid();
      const otherQr = getConnection().createQueryRunner();
      const otherStore = new PgIdempotenceStore(otherQr);

      const didLock = await store.lock(key);
      const alreadyLocked = await otherStore.lock(key);

      expect(didLock).toBe(true);
      expect(alreadyLocked).toBe(false);

      await otherQr.release();
    });
  });

  describe('unlock', () => {
    it('Successfully unlocks a resource which was locked', async () => {
      const key = faker.random.uuid();

      const didLock = await store.lock(key);
      const didUnlock = await store.unlock(key);

      expect(didLock).toBe(true);
      expect(didUnlock).toBe(true);
      expect(qr.isReleased).toBe(true);
    });

    it('Returns false if the resource was not locked', async () => {
      const key = faker.random.uuid();

      const didUnlock = await store.unlock(key);

      expect(didUnlock).toBe(false);
      expect(qr.isReleased).toBe(true);
    });
  });

  describe('get', () => {
    it('Returns undefined if the resource does not exist', async () => {
      const key = faker.random.uuid();
      const resource = await store.get(key);

      expect(resource).toBeUndefined();
    });

    it('Returns the resource from the database', async () => {
      const key = faker.random.uuid();
      const storedResponse: Partial<Request> = {
        idempotenceKey: key,
        status: 200,
        contentType: 'application/json',
        body: {
          foo: faker.lorem.words(),
          bar: faker.lorem.words(),
        },
      };

      const insertResponse = await Request.insert(storedResponse);

      const resource = await store.get(key);

      expect(resource.status).toBe(storedResponse.status);
      expect(resource.contentType).toBe(storedResponse.contentType);
      expect(resource.body).toEqual(storedResponse.body);

      await Request.delete(insertResponse.identifiers[0].id);
    });
  });

  describe('set', () => {
    it('Successfully stores a resource', async () => {
      const key = faker.random.uuid();
      const storedResponse: StoredResponse = {
        status: 200,
        contentType: 'application/json',
        body: {
          foo: faker.lorem.words(),
          bar: faker.lorem.words(),
        },
      };

      await store.set(key, storedResponse);
      const resource = await store.get(key);

      expect(resource.status).toBe(storedResponse.status);
      expect(resource.contentType).toBe(storedResponse.contentType);
      expect(resource.body).toEqual(storedResponse.body);

      await Request.delete({ idempotenceKey: key });
    });
  });
});
