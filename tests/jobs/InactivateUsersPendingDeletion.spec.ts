import { UserStatus, ConsumerStatus } from '@bacen/base-sdk';
import { In } from 'typeorm';
import { MockLogger } from '../util/LoggerUtil';
import { ConsumerTestUtil, UserTestUtil } from '../util';
import { InactivateUsersPendingDeletionJob } from '../../api/jobs';
import MainDatabase from '../../api/MainDatabase';
import { User } from '../../api/models';

import faker = require('faker');

jest.setTimeout(600000);

describe('api.jobs.inactivateUsersPendingDeletion', () => {
  const mockLogger = new MockLogger();
  const job = new InactivateUsersPendingDeletionJob({ logger: mockLogger as any });
  let database: MainDatabase;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();
  });

  afterAll(async () => {
    await database.disconnect();
  });

  describe('run', () => {
    afterEach(() => {
      mockLogger.clearAll();
    });

    it('Does nothing if there are no users', async () => {
      await job.run();

      const infoLogs = mockLogger.get('info');
      expect(infoLogs).toEqual([
        {
          message: 'InactivateUsersPendingDeletionJob: No active users pending deletion found. Exiting.',
        },
      ]);
    });

    it('Does nothing if there are no active consumers pending deletion', async () => {
      const inactiveConsumerPendingDeletion = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.INACTIVE,
        consumerState: ConsumerStatus.PENDING_DELETION,
      });

      await job.run();

      const infoLogs = mockLogger.get('info');
      expect(infoLogs).toEqual([
        {
          message: 'InactivateUsersPendingDeletionJob: No active users pending deletion found. Exiting.',
        },
      ]);

      await UserTestUtil.destroyUser(inactiveConsumerPendingDeletion);
    });

    it('Sends the user to inactive if there is one inactive user pending deletion', async () => {
      const activeConsumerPendingDeletion = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.PENDING_DELETION,
      });

      await job.run();

      const reloadedUser = await User.findOne(activeConsumerPendingDeletion.id, {
        relations: ['states', 'consumer', 'consumer.states'],
      });
      expect(reloadedUser.status).toBe(UserStatus.INACTIVE);
      expect(reloadedUser.consumer.status).toBe(ConsumerStatus.PENDING_DELETION);

      await UserTestUtil.destroyUser(reloadedUser);
    });

    it('Sends the all user to inactive if there is more than one inactive user pending deletion', async () => {
      const activeConsumersPendingDeletion = await Promise.all(
        Array.from({ length: faker.random.number({ min: 2, max: 5 }) }, () =>
          ConsumerTestUtil.generateConsumer({
            userState: UserStatus.ACTIVE,
            consumerState: ConsumerStatus.PENDING_DELETION,
          }),
        ),
      );

      await job.run();

      const reloadedUsers = await User.find({
        where: { id: In(activeConsumersPendingDeletion.map(user => user.id)) },
        relations: ['states', 'consumer', 'consumer.states'],
      });
      expect(reloadedUsers.every(user => user.status === UserStatus.INACTIVE)).toBe(true);
      expect(reloadedUsers.every(user => user.consumer.status === ConsumerStatus.PENDING_DELETION)).toBe(true);

      await Promise.all(reloadedUsers.map(user => UserTestUtil.destroyUser(user)));
    });

    it('inactivates the active users and does nothing to the others if there is a mix of active and inactive users', async () => {
      const activeConsumersPendingDeletion = await Promise.all(
        Array.from({ length: faker.random.number({ min: 2, max: 5 }) }, () =>
          ConsumerTestUtil.generateConsumer({
            userState: UserStatus.ACTIVE,
            consumerState: ConsumerStatus.PENDING_DELETION,
          }),
        ),
      );

      const inactiveConsumersPendingDeletion = await Promise.all(
        Array.from({ length: faker.random.number({ min: 2, max: 5 }) }, () =>
          ConsumerTestUtil.generateConsumer({
            userState: UserStatus.INACTIVE,
            consumerState: ConsumerStatus.PENDING_DELETION,
          }),
        ),
      );

      await job.run();

      const reloadedActiveUsers = await User.find({
        where: { id: In(activeConsumersPendingDeletion.map(user => user.id)) },
        relations: ['states', 'consumer', 'consumer.states'],
      });
      const reloadedInactiveUsers = await User.find({
        where: { id: In(inactiveConsumersPendingDeletion.map(user => user.id)) },
        relations: ['states', 'consumer', 'consumer.states'],
      });

      expect(reloadedActiveUsers.every(user => user.status === UserStatus.INACTIVE)).toBe(true);
      expect(reloadedActiveUsers.every(user => user.consumer.status === ConsumerStatus.PENDING_DELETION)).toBe(true);

      expect(reloadedActiveUsers.every(user => user.status === UserStatus.INACTIVE)).toBe(true);
      expect(reloadedActiveUsers.every(user => user.consumer.status === ConsumerStatus.PENDING_DELETION)).toBe(true);

      const infoLogs = mockLogger.get('info');
      expect(infoLogs.length).toBe(1);
      expect(infoLogs[0].message).toBe(`Inactivated ${reloadedActiveUsers.length} users. Exiting`);

      await Promise.all(reloadedActiveUsers.map(user => UserTestUtil.destroyUser(user)));
      await Promise.all(reloadedInactiveUsers.map(user => UserTestUtil.destroyUser(user)));
    });
  });
});
