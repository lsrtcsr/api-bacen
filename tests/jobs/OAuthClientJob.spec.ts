import { OAuthClientPlatform } from '@bacen/base-sdk';
import * as hat from 'hat';
import * as sinon from 'sinon';
import { OAuthClientJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { OAuthClient } from '../../api/models';
import { ServerTestUtil, AssetServiceMock, UninstalHandle } from '../util';

jest.setTimeout(90 * 1000);

describe('tests.jobs.OAuthClientJob', () => {
  let server: MainServer;
  let job: OAuthClientJob;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init({ children: [] });
  });

  beforeEach(async () => {
    job = new OAuthClientJob({
      name: 'TestOAuthClientJob',
      clients: [
        {
          clientId: `unit_test_ ${hat()}`,
          clientSecret: `unit_test`,
          platform: OAuthClientPlatform.WEB,
        },
      ],
    });
  });

  afterEach(async () => {
    job = undefined;
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstallHandle();
  });

  describe('without oauth clients in db', () => {
    let clientSafeFindStub: sinon.SinonStub;
    let clientInsertSpy: sinon.SinonSpy;

    beforeAll(async () => {
      clientSafeFindStub = sinon.stub(OAuthClient, 'find').resolves([]);
      clientInsertSpy = sinon.spy(OAuthClient, 'create');
    });

    afterAll(async () => {
      if (clientSafeFindStub) {
        clientSafeFindStub.restore();
        clientSafeFindStub = undefined;
      }

      if (clientInsertSpy) {
        clientInsertSpy.restore();
        clientInsertSpy = undefined;
      }
    });

    it('should create when root is not available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(clientSafeFindStub.calledOnce).toBeTruthy();

      // TODO: Check if wallet and consumer for mediator were correctfully created using expects
      expect(clientInsertSpy.calledOnce).toBeTruthy();
    });
  });
});
