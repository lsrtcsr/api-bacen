import * as hat from 'hat';
import * as cpf from 'cpf';
import * as sinon from 'sinon';
import { DomainRole } from '@bacen/base-sdk';
import { RootDomainJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { Domain } from '../../api/models';
import Config from '../../config';
import { ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

import getPort = require('get-port');

jest.setTimeout(90 * 1000);

describe('tests.jobs.RootDomainJob', () => {
  let server: MainServer;
  let job: RootDomainJob;
  let jobCheckSpy: sinon.SinonSpy;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init({ children: [] });
  });

  beforeEach(async () => {
    job = new RootDomainJob({
      name: 'TestRootDomainJob',
      rootMediator: {
        ...Config.seed.rootMediator,
        email: `test_${hat()}@platform.bt .app`,
        consumer: {
          ...Config.seed.rootMediator.consumer,
          taxId: cpf.generate(false),
          phones: [
            {
              code: '11',
              number: `9${Math.floor(Math.random() * 10000000)}}`,
            },
          ],
        },
      },
      // Even inside testing, we should not create as root unless we want to test it explicitly
      rootDomain: { ...Config.seed.rootDomain, role: DomainRole.COMMON },
    });
  });

  afterEach(async () => {
    job = undefined;

    if (jobCheckSpy) {
      jobCheckSpy.restore();
      jobCheckSpy = undefined;
    }
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstallHandle();
  });

  describe('with a root domain', () => {
    let domainCountStub: sinon.SinonStub;
    let domainInsertSpy: sinon.SinonSpy;

    beforeAll(async () => {
      domainCountStub = sinon.stub(Domain, 'safeCount').resolves(1);
      domainInsertSpy = sinon.spy(Domain, 'insertAndFind');
    });

    afterAll(async () => {
      if (domainCountStub) {
        domainCountStub.restore();
        domainCountStub = undefined;
      }

      if (domainInsertSpy) {
        domainInsertSpy.restore();
        domainInsertSpy = undefined;
      }
    });

    it('should not try to create when root is available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(domainCountStub.calledOnce).toBeTruthy();
      expect(domainInsertSpy.notCalled).toBeTruthy();
    });
  });

  describe('without a root domain', () => {
    let domainCountStub: sinon.SinonStub;
    let domainInsertSpy: sinon.SinonSpy;

    beforeAll(async () => {
      domainCountStub = sinon.stub(Domain, 'safeCount').resolves(0);
      domainInsertSpy = sinon.spy(Domain, 'insertAndFind');
    });

    afterAll(async () => {
      if (domainCountStub) {
        domainCountStub.restore();
        domainCountStub = undefined;
      }

      if (domainInsertSpy) {
        domainInsertSpy.restore();
        domainInsertSpy = undefined;
      }
    });

    it('should create when root is not available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(domainCountStub.calledOnce).toBeTruthy();

      // TODO: Check if wallet and consumer for mediator were correctfully created using expects
      expect(domainInsertSpy.calledOnce).toBeTruthy();
    });
  });
});
