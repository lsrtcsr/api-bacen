import { UserRole } from '@bacen/base-sdk';
import * as cpf from 'cpf';
import * as hat from 'hat';
import * as sinon from 'sinon';
import { RootAdminJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { User } from '../../api/models';
import Config from '../../config';
import { ServerTestUtil, UserTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 1000);

describe('tests.jobs.RootAdminJob', () => {
  let server: MainServer;
  let job: RootAdminJob;
  let assetUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init({ children: [] });
  });

  beforeEach(async () => {
    job = new RootAdminJob({
      name: 'TestRootAdminJob',
      rootAdmin: {
        ...Config.seed.rootAdmin,
        email: `test_${hat()}@platform.bt .app`,
        consumer: {
          ...Config.seed.rootMediator.consumer,
          taxId: cpf.generate(false),
          phones: [
            {
              code: '11',
              number: `9${Math.floor(Math.random() * 10000000)}}`,
            },
          ],
        },
      },
    });
  });

  afterEach(async () => {
    job = undefined;
  });

  afterAll(async () => {
    const adminUser = await User.findOne({
      where: { role: UserRole.ADMIN },
      relations: ['wallets', 'wallets.issuedAssets'],
    });

    if (adminUser) {
      await UserTestUtil.destroyUser(adminUser);
    }

    if (server) {
      await server.close();
      server = undefined;
    }

    assetUninstalHandle();
  });

  describe('without a root admin', () => {
    let usersCountStub: sinon.SinonStub;
    let usersCreateSpy: sinon.SinonStub;

    beforeAll(async () => {
      usersCountStub = sinon.stub(User, 'safeCount').resolves(0);
      usersCreateSpy = sinon.stub(User, 'create').callsFake((schema: any) => new User(schema));
    });

    afterAll(async () => {
      if (usersCountStub) {
        usersCountStub.restore();
        usersCountStub = undefined;
      }

      if (usersCreateSpy) {
        usersCreateSpy.restore();
        usersCreateSpy = undefined;
      }
    });

    it('should try to create when default is not available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(usersCountStub.calledOnce).toBeTruthy();
      expect(usersCreateSpy.calledOnce).toBeTruthy();
    });
  });
});
