import * as sinon from 'sinon';
import { DeepPartial } from 'typeorm';
import { BaseError } from 'nano-errors';
import { DomainRole, UserRole, TransitoryAccountType } from '@bacen/base-sdk';
import MainServer from '../../api/MainServer';
import { User, Wallet } from '../../api/models';
import { TransitoryAccountJob } from '../../api/jobs';
import { UninstalHandle, AssetServiceMock, ServerTestUtil, UserTestUtil, DomainTestUtil } from '../util';

describe('api.jobs.seed.TransitoryAccount.job', () => {
  let server: MainServer;
  let job: TransitoryAccountJob;
  let user: User;
  let assetServiceUninstallHandle: UninstalHandle;

  // Logger stub
  const infoStub = sinon.stub();

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
    job = new TransitoryAccountJob({ logger: { info: infoStub } as any });

    user = await UserTestUtil.generateUser();
  });

  afterEach(() => {
    infoStub.resetHistory();
  });

  afterAll(async () => {
    if (user) await UserTestUtil.destroyUser(user);
    await server.close();
    assetServiceUninstallHandle();
  });

  it.skip('Does nothing if a transitoryAccount already exits', async () => {
    const transitoryAcc = await Wallet.insertAndFind(
      Wallet.create({
        user,
        additionalData: { isTransitoryAccount: true },
      } as DeepPartial<Wallet>),
    );

    await job.run(server);
    expect(infoStub.calledOnceWith('Transitory account wallet already exists, no actions needed.')).toBe(true);

    await transitoryAcc.remove();
  });

  it('throws if rootDomainMediator does not exist', async () => {
    let err: BaseError;
    const transitoryAccBefore = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

    try {
      await job.run(server);
    } catch (e) {
      err = e;
    }

    const transitoryAccAfter = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

    expect(transitoryAccBefore).toBeUndefined();
    expect(transitoryAccAfter).toBeUndefined();
    expect(err).toHaveProperty('originalMessage', 'Root mediator does not exist, exiting...');
  });

  it('Creates a transitory account given that none exist', async () => {
    const rootDomain = await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
    const rootDomainMediator = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR, userDomain: rootDomain });

    const transitoryAccBefore = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

    await job.run(server);

    const transitoryAccAfter = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);
    expect(transitoryAccBefore).toBeUndefined();
    expect(transitoryAccAfter).toBeDefined();

    await transitoryAccAfter.remove();
    await UserTestUtil.destroyUser(rootDomainMediator);
    await DomainTestUtil.destroyDomain(rootDomain);
  });
});
