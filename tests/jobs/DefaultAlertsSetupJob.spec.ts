import { NotificationChannel, SeverityLevel } from '@bacen/base-sdk';
import * as sinon from 'sinon';
import { DefaultAlertsSetupJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { Alert } from '../../api/models';
import { ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 1000);

describe('tests.jobs.DefaultAlertsSetupJob', () => {
  let server: MainServer;
  let job: DefaultAlertsSetupJob;
  let assetServiceUninstalHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstalHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init({ children: [] });
  });

  beforeEach(async () => {
    job = new DefaultAlertsSetupJob({
      name: 'TestDefaultAlertsSetupJob',
      defaultAlerts: [
        {
          settings: {
            limitType: 'upperLimit',
            timeWindow: 0,
            threshold: SeverityLevel.HIGH,
            value: 1,
            subscriptions: [
              {
                channel: NotificationChannel.EMAIL,
                subscribers: [],
                extraData: [],
              },
            ],
          },
        },
      ],
    });
  });

  afterEach(async () => {
    job = undefined;
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstalHandle();
  });

  describe('without default alerts', () => {
    let alertCountStub: sinon.SinonStub;
    let alertCreateSpy: sinon.SinonSpy;

    beforeAll(async () => {
      alertCountStub = sinon.stub(Alert, 'safeCount').resolves(0);
      alertCreateSpy = sinon.spy(Alert, 'create');
    });

    afterAll(async () => {
      if (alertCountStub) {
        alertCountStub.restore();
        alertCountStub = undefined;
      }

      if (alertCreateSpy) {
        alertCreateSpy.restore();
        alertCreateSpy = undefined;
      }
    });

    it('should try to create when default is not available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(alertCountStub.calledOnce).toBeTruthy();
      expect(alertCreateSpy.calledOnce).toBeTruthy();
    });
  });
});
