import { UserStatus, ConsumerStatus } from '@bacen/base-sdk';
import { MockLogger } from '../util/LoggerUtil';
import { ConsumerTestUtil, UserTestUtil } from '../util';
import { RemoveConsumersMarkedForDeletionJob } from '../../api/jobs';
import { User, ConsumerState } from '../../api/models';
import MainServer from '../../api/MainServer';
import { ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(600000);

describe('api.jobs.inactivateUsersPendingDeletion', () => {
  const mockLogger = new MockLogger();
  const job = new RemoveConsumersMarkedForDeletionJob({
    logger: mockLogger as any,
    markedForDeletionStates: [ConsumerStatus.PENDING_DELETION],
  });
  let server: MainServer;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    await server.close();
    assetServiceUninstallHandle();
  });

  describe('run', () => {
    afterEach(() => {
      mockLogger.clearAll();
    });

    it('Does nothing if there are no users', async () => {
      await job.run();

      const infoLogs = mockLogger.get('info');
      expect(infoLogs).toEqual([
        {
          message: 'RemoveConsumersMarkedForDeletionJob: No consumers marked for deletion. Continuing...',
        },
      ]);
    });

    it('Does nothing if there are no consumers pending deletion', async () => {
      const activeUser = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });

      await job.run();

      const infoLogs = mockLogger.get('info');
      expect(infoLogs).toEqual([
        {
          message: 'RemoveConsumersMarkedForDeletionJob: No consumers marked for deletion. Continuing...',
        },
      ]);

      const reloadedUser = await User.findOne(activeUser.id, {
        relations: ['states', 'consumer', 'consumer.states'],
      });

      expect(reloadedUser.status).toBe(UserStatus.ACTIVE);
      expect(reloadedUser.consumer.persistentStatus).toBe(ConsumerStatus.READY);

      await UserTestUtil.destroyUser(activeUser);
    });

    it('Does nothing if there are consumers pending deletion but their deadline has not passed', async () => {
      const userPendingDeletion = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
      });
      await ConsumerState.insert({
        consumer: userPendingDeletion.consumer,
        status: ConsumerStatus.PENDING_DELETION,
        additionalData: {
          deleteBy: new Date(Date.now() + 3600 * 1000),
        } as any,
      });

      await job.run();

      const infoLogs = mockLogger.get('info');
      expect(infoLogs).toEqual([
        {
          message: 'RemoveConsumersMarkedForDeletionJob: No consumers marked for deletion. Continuing...',
        },
      ]);

      const reloadedUser = await User.findOne(userPendingDeletion.id, {
        relations: ['states', 'consumer', 'consumer.states'],
      });

      expect(reloadedUser.status).toBe(UserStatus.ACTIVE);
      expect(reloadedUser.consumer.status).toBe(ConsumerStatus.PENDING_DELETION);

      await UserTestUtil.destroyUser(userPendingDeletion);
    });

    it('Deletes consumers pending deletion whose deletion deadline has expired', async () => {
      const userPendingDeletion = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
      });
      await ConsumerState.insert({
        consumer: userPendingDeletion.consumer,
        status: ConsumerStatus.PENDING_DELETION,
        additionalData: {
          deleteBy: new Date(Date.now() - 24 * 3600 * 1000),
        } as any,
      });

      await job.run();

      const reloadedUser = await User.findOne(userPendingDeletion.id, {
        relations: ['states', 'consumer', 'consumer.states', 'wallets'],
      });

      expect(reloadedUser.status).toBe(UserStatus.INACTIVE);
      expect(reloadedUser.consumer.status).toBe(ConsumerStatus.DELETED);
      expect(reloadedUser.deletedAt).not.toBeNull();
      expect(reloadedUser.consumer.deletedAt).not.toBeNull();
      reloadedUser.wallets.forEach(wallet => {
        expect(wallet.deletedAt).not.toBeNull();
      });

      await UserTestUtil.destroyUser(userPendingDeletion);
    });

    it('Only deletes consumers pending deletion whose deletion deadline has expired when there is a mix of users that should and should not be deleted', async () => {
      const userPendingDeletion = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
      });
      const userNotExpired = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
      });

      await ConsumerState.insert({
        consumer: userPendingDeletion.consumer,
        status: ConsumerStatus.PENDING_DELETION,
        additionalData: {
          deleteBy: new Date(Date.now() - 24 * 3600 * 1000),
        } as any,
      });
      await ConsumerState.insert({
        consumer: userNotExpired.consumer,
        status: ConsumerStatus.PENDING_DELETION,
        additionalData: {
          deleteBy: new Date(),
        } as any,
      });

      await job.run();

      const reloadedUser = await User.findOne(userPendingDeletion.id, {
        relations: ['states', 'consumer', 'consumer.states', 'wallets'],
      });

      const reloadedNotExpiredUser = await User.findOne(userNotExpired.id, {
        relations: ['states', 'consumer', 'consumer.states', 'wallets'],
      });

      expect(reloadedUser.status).toBe(UserStatus.INACTIVE);
      expect(reloadedUser.consumer.status).toBe(ConsumerStatus.DELETED);
      expect(reloadedUser.deletedAt).not.toBeNull();
      expect(reloadedUser.consumer.deletedAt).not.toBeNull();
      reloadedUser.wallets.forEach(wallet => {
        expect(wallet.deletedAt).not.toBeNull();
      });

      expect(reloadedNotExpiredUser.status).toBe(UserStatus.ACTIVE);
      expect(reloadedNotExpiredUser.consumer.status).toBe(ConsumerStatus.PENDING_DELETION);
      expect(reloadedNotExpiredUser.deletedAt).toBeNull();
      expect(reloadedNotExpiredUser.consumer.deletedAt).toBeNull();
      reloadedNotExpiredUser.wallets.forEach(wallet => {
        expect(wallet.deletedAt).toBeNull();
      });

      await UserTestUtil.destroyUser(userPendingDeletion);
      await UserTestUtil.destroyUser(userNotExpired);
    });
  });
});
