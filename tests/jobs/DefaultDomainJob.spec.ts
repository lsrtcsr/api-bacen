import * as sinon from 'sinon';
import { DomainRole } from '@bacen/base-sdk';
import { DefaultDomainJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { Domain } from '../../api/models';
import Config from '../../config';
import { ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 1000);

describe('tests.jobs.DefaultDomainJob', () => {
  let server: MainServer;
  let job: DefaultDomainJob;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init({ children: [] });
  });

  beforeEach(async () => {
    job = new DefaultDomainJob({
      name: 'TestDefaultDomainJob',
      defaultDomain: { ...Config.seed.rootDomain, role: DomainRole.COMMON },
    });
  });

  afterEach(async () => {
    job = undefined;
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }

    assetServiceUninstallHandle();
  });

  describe('with default domain', () => {
    let domainCountStub: sinon.SinonStub;
    let domainInsertSpy: sinon.SinonSpy;

    beforeAll(async () => {
      domainCountStub = sinon.stub(Domain, 'safeCount').resolves(1);
      domainInsertSpy = sinon.spy(Domain, 'insertAndFind');
    });

    afterAll(async () => {
      if (domainCountStub) {
        domainCountStub.restore();
        domainCountStub = undefined;
      }

      if (domainInsertSpy) {
        domainInsertSpy.restore();
        domainInsertSpy = undefined;
      }
    });

    it('should try to create when default is available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(domainCountStub.calledOnce).toBeTruthy();
      expect(domainInsertSpy.notCalled).toBeTruthy();
    });
  });

  describe('without a default domain', () => {
    let domainCountStub: sinon.SinonStub;
    let domainInsertSpy: sinon.SinonSpy;

    beforeAll(async () => {
      domainCountStub = sinon.stub(Domain, 'safeCount').resolves(0);
      domainInsertSpy = sinon.spy(Domain, 'insertAndFind');
    });

    afterAll(async () => {
      if (domainCountStub) {
        domainCountStub.restore();
        domainCountStub = undefined;
      }

      if (domainInsertSpy) {
        domainInsertSpy.restore();
        domainInsertSpy = undefined;
      }
    });

    it('should try to create when default is not available', async () => {
      await expect(job.run(server)).resolves.not.toThrow();
      expect(domainCountStub.calledOnce).toBeTruthy();
      expect(domainInsertSpy.calledOnce).toBeTruthy();
    });
  });
});
