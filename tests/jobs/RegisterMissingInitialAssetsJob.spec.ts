import * as sinon from 'sinon';
import {
  WalletStatus,
  UserStatus,
  ConsumerStatus,
  AssetRegistrationStatus,
  UserRole,
  UnleashFlags,
} from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import {
  MockLogger,
  WalletTestUtil,
  UserTestUtil,
  ConsumerTestUtil,
  AssetTestUtil,
  ServerTestUtil,
  UninstalHandle,
  AssetServiceMock,
} from '../util';
import { RegisterMissingInitialAssetsJob } from '../../api/jobs';
import MainServer from '../../api/MainServer';
import { AssetService, MediatorPipelinePublisher, ConsumerPipelinePublisher } from '../../api/services';
import { WalletState, User, Wallet } from '../../api/models';

jest.setTimeout(90 * 10000);

describe.skip('api.jobs.RegisterMissingInitialAssetsJob', () => {
  const mockLogger = new MockLogger();
  const service = new RegisterMissingInitialAssetsJob({ logger: mockLogger as any });
  let server: MainServer;
  let unleashUtilStub: sinon.SinonStub;
  let mediatorPipelinePublisherStub: sinon.SinonStub;
  let consumerPipelinePublisherStub: sinon.SinonStub;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();

    unleashUtilStub = sinon
      .stub(UnleashUtil, 'isEnabled')
      .resolves(flag => flag === UnleashFlags.ENABLE_REGISTER_MISSING_ASSETS);
    mediatorPipelinePublisherStub = sinon.stub(MediatorPipelinePublisher.prototype, 'send').resolves(true);
    consumerPipelinePublisherStub = sinon.stub(ConsumerPipelinePublisher.prototype, 'send').resolves(true);
  });

  afterAll(async () => {
    await server.close();

    unleashUtilStub.restore();
    mediatorPipelinePublisherStub.restore();
    consumerPipelinePublisherStub.restore();

    assetServiceUninstallHandle();
  });

  afterEach(() => {
    mockLogger.clearAll();
    mediatorPipelinePublisherStub.resetHistory();
    consumerPipelinePublisherStub.resetHistory();
  });

  describe('run', () => {
    it('Does nothing if there are no required initital assets', async () => {
      const assetsServiceStub = sinon.stub(AssetService, 'getInstance').returns({ requiredAssets: [] } as any);
      const user = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });
      const wallet = await WalletTestUtil.generate(user);
      await WalletState.insert({ wallet, status: WalletStatus.READY });

      try {
        await service.run(server);

        const infoLogs = mockLogger.get('info');
        const reloadedUser = await User.findOne(user.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWallet = await Wallet.findOne(wallet.id, { relations: ['states', 'assetRegistrations'] });

        expect(infoLogs.some(({ message }) => /No wallets missing assets/gi.test(message))).toBe(true);
        expect(reloadedUser.status).toBe(UserStatus.ACTIVE);
        expect(reloadedUser.consumer.status).toBe(ConsumerStatus.READY);
        expect(reloadedWallet.status).toBe(WalletStatus.READY);
        expect(reloadedWallet.assetRegistrations.length).toBe(0);
      } finally {
        await UserTestUtil.destroyUser(user);
        assetsServiceStub.restore();
      }
    });

    it('Does nothing if all wallets have all required assets', async () => {
      const assetIssuer = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR });
      const issuerWallet = await WalletTestUtil.generate(assetIssuer);

      const requiredAsset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });
      const initialAssets = [
        { name: requiredAsset.name, code: requiredAsset.code, authorizable: true, required: true },
      ];
      const assetsServiceStub = sinon
        .stub(AssetService, 'getInstance')
        .returns({ requiredAssets: initialAssets } as any);

      const userOne = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });
      const userTwo = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });

      const walletFromUserOne = await WalletTestUtil.generate(userOne);
      const walletFromUserTwo = await WalletTestUtil.generate(userTwo);
      const anotherWalletFromUserTwo = await WalletTestUtil.generate(userTwo);
      await Promise.all(
        [walletFromUserOne, walletFromUserTwo, anotherWalletFromUserTwo].map(wallet =>
          Promise.all([
            WalletState.insert({ wallet, status: WalletStatus.READY }),
            AssetTestUtil.linkWalletToAsset({ wallet, asset: requiredAsset, status: AssetRegistrationStatus.READY }),
          ]),
        ),
      );

      try {
        await service.run(server);

        const infoLogs = mockLogger.get('info');
        const reloadedUserOne = await User.findOne(userOne.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWalletFromUserOne = await Wallet.findOne(walletFromUserOne.id, {
          relations: ['states', 'assetRegistrations'],
        });
        const reloadedUserTwo = await User.findOne(userTwo.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWalletFromUserTwo = await Wallet.findOne(walletFromUserTwo.id, {
          relations: ['states', 'assetRegistrations'],
        });
        const reloadedAnotherWalletFromUserTwo = await Wallet.findOne(anotherWalletFromUserTwo.id, {
          relations: ['states', 'assetRegistrations'],
        });

        expect(infoLogs.some(({ message }) => /No wallets missing assets/gi.test(message))).toBe(true);
        expect(reloadedUserOne.status).toBe(UserStatus.ACTIVE);
        expect(reloadedUserOne.consumer.status).toBe(ConsumerStatus.READY);
        expect(reloadedUserTwo.status).toBe(UserStatus.ACTIVE);
        expect(reloadedUserTwo.consumer.status).toBe(ConsumerStatus.READY);
        expect(reloadedWalletFromUserOne.status).toBe(WalletStatus.READY);
        expect(reloadedWalletFromUserOne.assetRegistrations.length).toBe(1);
        expect(reloadedWalletFromUserTwo.status).toBe(WalletStatus.READY);
        expect(reloadedWalletFromUserTwo.assetRegistrations.length).toBe(1);
        expect(reloadedAnotherWalletFromUserTwo.status).toBe(WalletStatus.READY);
        expect(reloadedAnotherWalletFromUserTwo.assetRegistrations.length).toBe(1);
      } finally {
        assetsServiceStub.restore();
        await UserTestUtil.destroyUser(userOne);
        await UserTestUtil.destroyUser(userTwo);
        await AssetTestUtil.destroyAsset(requiredAsset);
      }
    });

    it('Registers missing assets on all wallets', async () => {
      const assetIssuer = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR });
      const issuerWallet = await WalletTestUtil.generate(assetIssuer);

      const requiredAsset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });
      const otherRequiredAsset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });
      const initialAssets = [
        { name: requiredAsset.name, code: requiredAsset.code, authorizable: true, required: true },
        { name: otherRequiredAsset.name, code: otherRequiredAsset.code, authorizable: true, required: true },
      ];
      const assetsServiceStub = sinon
        .stub(AssetService, 'getInstance')
        .returns({ requiredAssets: initialAssets } as any);

      const userOne = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });
      const userTwo = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });

      const walletFromUserOne = await WalletTestUtil.generate(userOne);
      const walletFromUserTwo = await WalletTestUtil.generate(userTwo);
      const anotherWalletFromUserTwo = await WalletTestUtil.generate(userTwo);
      await Promise.all(
        [walletFromUserOne, walletFromUserTwo, anotherWalletFromUserTwo].map(wallet =>
          Promise.all([
            WalletState.insert({ wallet, status: WalletStatus.READY }),
            AssetTestUtil.linkWalletToAsset({ wallet, asset: requiredAsset, status: AssetRegistrationStatus.READY }),
          ]),
        ),
      );

      // anotherWalletFromUserTwo already has the asset registered
      await AssetTestUtil.linkWalletToAsset({
        wallet: anotherWalletFromUserTwo,
        asset: otherRequiredAsset,
        status: AssetRegistrationStatus.READY,
      });

      try {
        await service.run(server);

        const infoLogs = mockLogger.get('info');
        const reloadedUserOne = await User.findOne(userOne.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWalletFromUserOne = await Wallet.findOne(walletFromUserOne.id, {
          relations: ['states', 'assetRegistrations'],
        });
        const reloadedUserTwo = await User.findOne(userTwo.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWalletFromUserTwo = await Wallet.findOne(walletFromUserTwo.id, {
          relations: ['states', 'assetRegistrations'],
        });
        const reloadedAnotherWalletFromUserTwo = await Wallet.findOne(anotherWalletFromUserTwo.id, {
          relations: ['states', 'assetRegistrations'],
        });

        expect(reloadedUserOne.status).toBe(UserStatus.ACTIVE);
        expect(reloadedUserOne.consumer.status).toBe(ConsumerStatus.PROCESSING_WALLETS);
        expect(reloadedUserTwo.status).toBe(UserStatus.ACTIVE);
        expect(reloadedUserTwo.consumer.status).toBe(ConsumerStatus.PROCESSING_WALLETS);
        expect(reloadedWalletFromUserOne.status).toBe(WalletStatus.REGISTERED_IN_STELLAR);
        expect(reloadedWalletFromUserOne.assetRegistrations.length).toBe(2);
        expect(reloadedWalletFromUserTwo.status).toBe(WalletStatus.REGISTERED_IN_STELLAR);
        expect(reloadedWalletFromUserTwo.assetRegistrations.length).toBe(2);
        expect(reloadedAnotherWalletFromUserTwo.status).toBe(WalletStatus.READY);
        expect(reloadedAnotherWalletFromUserTwo.assetRegistrations.length).toBe(2);
        expect(consumerPipelinePublisherStub.calledTwice).toBeTruthy();
        expect(mediatorPipelinePublisherStub.called).toBeFalsy();
      } finally {
        assetsServiceStub.restore();
        await UserTestUtil.destroyUser(userOne);
        await UserTestUtil.destroyUser(userTwo);
        await AssetTestUtil.destroyAsset(requiredAsset, false);
        await AssetTestUtil.destroyAsset(otherRequiredAsset);
      }
    });

    it('ignores users that are not ready', async () => {
      const assetIssuer = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR });
      const issuerWallet = await WalletTestUtil.generate(assetIssuer);

      const requiredAsset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });
      const initialAssets = [
        { name: requiredAsset.name, code: requiredAsset.code, authorizable: true, required: true },
      ];
      const assetsServiceStub = sinon
        .stub(AssetService, 'getInstance')
        .returns({ requiredAssets: initialAssets } as any);

      const user = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.PROCESSING_DOCUMENTS,
      });
      const wallet = await WalletTestUtil.generate(user);
      await WalletState.insert({ wallet, status: WalletStatus.READY });

      try {
        await service.run(server);

        const infoLogs = mockLogger.get('info');
        const reloadedUser = await User.findOne(user.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWallet = await Wallet.findOne(wallet.id, { relations: ['states', 'assetRegistrations'] });

        expect(infoLogs.some(({ message }) => /No wallets missing assets/gi.test(message))).toBe(true);
        expect(reloadedUser.consumer.status).toBe(ConsumerStatus.PROCESSING_DOCUMENTS);
        expect(reloadedWallet.status).toBe(WalletStatus.READY);
        expect(reloadedWallet.assetRegistrations.length).toBe(0);
      } finally {
        assetsServiceStub.restore();
        await UserTestUtil.destroyUser(user);
        await AssetTestUtil.destroyAsset(requiredAsset);
      }
    });

    it('ignores wallets that are not ready', async () => {
      const assetIssuer = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR });
      const issuerWallet = await WalletTestUtil.generate(assetIssuer);

      const requiredAsset = await AssetTestUtil.generateAsset({ wallet: issuerWallet });
      const initialAssets = [
        { name: requiredAsset.name, code: requiredAsset.code, authorizable: true, required: true },
      ];
      const assetsServiceStub = sinon
        .stub(AssetService, 'getInstance')
        .returns({ requiredAssets: initialAssets } as any);

      const user = await ConsumerTestUtil.generateConsumer({
        userState: UserStatus.ACTIVE,
        consumerState: ConsumerStatus.READY,
      });
      const wallet = await WalletTestUtil.generate(user);
      await WalletState.insert({ wallet, status: WalletStatus.REGISTERED_IN_STELLAR });

      try {
        await service.run(server);

        const infoLogs = mockLogger.get('info');
        const reloadedUser = await User.findOne(user.id, {
          relations: ['states', 'consumer', 'consumer.states'],
        });
        const reloadedWallet = await Wallet.findOne(wallet.id, { relations: ['states', 'assetRegistrations'] });

        expect(infoLogs.some(({ message }) => /No wallets missing assets/gi.test(message))).toBe(true);
        expect(reloadedUser.consumer.status).toBe(ConsumerStatus.READY);
        expect(reloadedWallet.status).toBe(WalletStatus.REGISTERED_IN_STELLAR);
        expect(reloadedWallet.assetRegistrations.length).toBe(0);
      } finally {
        assetsServiceStub.restore();
        await UserTestUtil.destroyUser(user);
        await AssetTestUtil.destroyAsset(requiredAsset);
      }
    });
  });
});
