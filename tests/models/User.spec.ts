import { DomainRole, UserRole } from '@bacen/base-sdk';
import MainServer from '../../api/MainServer';
import { User, Domain } from '../../api/models';
import { ServerTestUtil, DomainTestUtil, UserTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(60000);

describe('api.models.user', () => {
  let server: MainServer;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    await server.close();
    assetServiceUninstallHandle();
  });

  describe('getRootMediator', () => {
    it('Returns undefined if the root domain does not exist', async () => {
      const result = await User.getRootMediator();
      const rootDomain = await Domain.getRootDomain();

      expect(rootDomain).toBeUndefined();
      expect(result).toBeUndefined();
    });

    it('Returns undefined if root domain exists but has no mediator', async () => {
      await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
      const result = await User.getRootMediator();
      const rootDomain = await Domain.getRootDomain();

      expect(rootDomain).toBeDefined();
      expect(result).toBeUndefined();

      await DomainTestUtil.destroyDomain(rootDomain);
    });

    it('Returns the mediator of the root domain if both exist', async () => {
      await DomainTestUtil.generateDomain({ partial: { role: DomainRole.ROOT } });
      const rootDomain = await Domain.getRootDomain();
      const rootMediator = await UserTestUtil.generateUser({ role: UserRole.MEDIATOR, userDomain: rootDomain });

      const result = await User.getRootMediator();

      expect(rootDomain).toBeDefined();
      expect(result.id).toBe(rootMediator.id);

      await UserTestUtil.destroyUser(rootMediator);
      await DomainTestUtil.destroyDomain(rootDomain);
    });
  });
});
