import MainServer from '../../api/MainServer';
import { User, OAuthOTPToken } from '../../api/models';
import { ConsumerTestUtil, ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(90 * 10000);

describe('api.MainServer', () => {
  let user: User;
  let server: MainServer;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  beforeEach(async () => {
    user = await ConsumerTestUtil.generateConsumer();
  });

  afterEach(async () => {
    if (user) {
      await ConsumerTestUtil.destroyConsumer(user);
    }
  });

  afterAll(async () => {
    if (server) {
      await server.close();
      server = undefined;
    }
    assetServiceUninstallHandle();
  });

  it.skip('should configure OTP for an existing user', async () => {
    const otp = await OAuthOTPToken.generate(user);

    // Sanity check
    expect(otp.secret).toBeDefined();
    expect(otp.secret.length).toBeGreaterThan(0);

    // Sample token validation
    const token = await otp.token();
    await expect(otp.verify(token)).resolves.toBe(true);

    // Ensure user is configured
    const updatedUser = await User.safeFindOne({ where: { id: user.id }, relations: ['twoFactorSecret'] });
    expect(updatedUser.twoFactorSecret.id).toBe(otp.id);
    expect(updatedUser.twoFactorRequired).toBe(true);
  });

  it.skip('should reject invalid OTP for an existing user', async () => {
    const otp = await OAuthOTPToken.generate(user);

    // Sanity check
    expect(otp.secret).toBeDefined();
    expect(otp.secret.length).toBeGreaterThan(0);

    // Invalid token validation
    const failedAttempts = 0 + otp.failedAttempts;
    await expect(otp.verify('123456')).resolves.toBe(false);

    // Make sure failed attempts was increased
    const updatedOtp = await OAuthOTPToken.safeFindOne({ where: { id: otp.id } });
    expect(updatedOtp.failedAttempts).toBeGreaterThan(failedAttempts);

    // Ensure user is configured
    const updatedUser = await User.safeFindOne({ where: { id: user.id }, relations: ['twoFactorSecret'] });
    expect(updatedUser.twoFactorSecret.id).toBe(otp.id);
    expect(updatedUser.twoFactorRequired).toBe(true);
  });

  it.skip('should deny OTP after a number of failed attempts', async () => {
    const otp = await OAuthOTPToken.generate(user);

    // Sanity check
    expect(otp.secret).toBeDefined();
    expect(otp.secret.length).toBeGreaterThan(0);

    // Invalid token validation
    const failedAttempts = 0 + otp.failedAttempts;
    await expect(otp.verify('123456')).resolves.toBe(false);

    // Make sure failed attempts was increased
    const updatedOtp = await OAuthOTPToken.safeFindOne({ where: { id: otp.id } });
    expect(updatedOtp.failedAttempts).toBeGreaterThan(failedAttempts);

    // Ensure user is configured
    const updatedUser = await User.safeFindOne({ where: { id: user.id }, relations: ['twoFactorSecret'] });
    expect(updatedUser.twoFactorSecret.id).toBe(otp.id);
    expect(updatedUser.twoFactorRequired).toBe(true);
  });
});
