import * as faker from 'faker';
import { count } from 'console';
import MainDatabase from '../../api/MainDatabase';
import { Phone } from '../../api/models';

describe('api.models.Banking', () => {
  let database: MainDatabase;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();
  });

  afterAll(async () => {
    await database.disconnect();
  });

  describe('getPhoneFromString', () => {
    it('Should get country code, area code and number from Brasil number', async () => {
      const countryCode = '55';
      const number = '35999999999';
      const fullNumber = `+${countryCode}${number}`;

      const phone = Phone.getPhoneFromString(fullNumber);

      expect(phone.countryCode).toBe(countryCode);
      expect(phone.number).toBe(number);
    });

    it('Should get country code, area code and number from US number', async () => {
      const countryCode = '1';
      const number = '2133734253';
      const fullNumber = `+${countryCode}${number}`;

      const phone = Phone.getPhoneFromString(fullNumber);

      expect(phone.countryCode).toBe(countryCode);
      expect(phone.number).toBe(number);
    });

    it('Should get country code, area code and number from Australia number', async () => {
      const countryCode = '33';
      const number = '788604735';
      const fullNumber = `+${countryCode}${number}`;

      const phone = Phone.getPhoneFromString(fullNumber);

      expect(phone.countryCode).toBe(countryCode);
      expect(phone.number).toBe(number);
    });
  });
});
