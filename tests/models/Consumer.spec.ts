import { AssetRegistrationStatus, ConsumerStatus, ConsumerTransientStatus, WalletTransientStatus } from '@bacen/base-sdk';
import { Consumer, ConsumerState } from '../../api/models/consumer';
import { Wallet } from '../../api/models/wallet';
import { User } from '../../api/models/user';

describe('api.models.consumer.Consumer', () => {
  describe('status', () => {
    it('returns undefined if consumer\'s states are not loaded', () => {
      const consumer = new Consumer({ states: undefined });
      expect(consumer.status).toBe(undefined);
    });

    it('returns pending_phone_verification if the consumer\'s persistentStatus is pending_phone_verification', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.PENDING_PHONE_VERIFICATION })]});
      expect(consumer.status).toBe(ConsumerTransientStatus.PENDING_PHONE_VERIFICATION);
    });

    it('returns pending_legal_acceptance if the consumer\'s persistentStatus is pending_legal_acceptance', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.PENDING_LEGAL_ACCEPTANCE })]});
      expect(consumer.status).toBe(ConsumerTransientStatus.PENDING_LEGAL_ACCEPTANCE);
    });

    it('returns pending_billing_subscription if the consumer\'s persistentStatus is pending_billing_subscription', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION })]});
      expect(consumer.status).toBe(ConsumerTransientStatus.PENDING_BILLING_PLAN_SUBSCRIPTION);
    });

    it('returns pending_deletion if the consumer\'s persistentStatus is pending_deletion', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.PENDING_DELETION })]});
      expect(consumer.status).toBe(ConsumerTransientStatus.PENDING_DELETION);
    });

    it('returns deleted if the consumer\'s persistentStatus is deleted', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.DELETED })]});
      expect(consumer.status).toBe(ConsumerTransientStatus.DELETED);
    });

    describe('when consumer\'s persistentStatus is Ready', () => {
      const consumer = new Consumer({ states: [new ConsumerState({ status: ConsumerStatus.READY })]});

      it('returns pending_documents if the user\'s most advandec wallet is pending_documents', () => {
        const wallet1 = { status: WalletTransientStatus.PENDING_DOCUMENTS };
        const wallet2 = { status: WalletTransientStatus.REJECTED };

        consumer.user = { wallets: [wallet1, wallet2] } as User
        expect(consumer.status).toBe(ConsumerTransientStatus.PENDING_DOCUMENTS);
      });

      it('returns processing if the user\'s most advandec wallet is processing', () => {
        const wallet1 = { status: WalletTransientStatus.PROCESSING };
        const wallet2 = { status: WalletTransientStatus.PENDING_DOCUMENTS};

        consumer.user = { wallets: [wallet1, wallet2] } as User
        expect(consumer.status).toBe(ConsumerTransientStatus.PROCESSING);
      });

      it('returns ready if the user\'s most advandec wallet is ready', () => {
        const wallet1 = { status: WalletTransientStatus.READY };
        const wallet2 = { status: WalletTransientStatus.PROCESSING };

        consumer.user = { wallets: [wallet1, wallet2] } as User
        expect(consumer.status).toBe(ConsumerTransientStatus.READY);
      });

      it('returns rejected if the user\'s most advandec wallet is rejected', () => {
        const wallet1 = { status: WalletTransientStatus.REJECTED };
        const wallet2 = { status: WalletTransientStatus.FAILED };

        consumer.user = { wallets: [wallet1, wallet2] } as User
        expect(consumer.status).toBe(ConsumerTransientStatus.REJECTED);
      });

      it('returns failed if the user\'s most advandec wallet is failed', () => {
        const wallet1 = { status: WalletTransientStatus.FAILED };
        const wallet2 = { status: WalletTransientStatus.FAILED };

        consumer.user = { wallets: [wallet1, wallet2] } as User
        expect(consumer.status).toBe(ConsumerTransientStatus.FAILED);
      });
    });
  });
});
