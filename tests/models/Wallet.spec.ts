import { DeepPartial } from 'typeorm';
import { AssetRegistrationStatus, TransitoryAccountType, WalletStatus, WalletTransientStatus } from '@bacen/base-sdk';
import MainServer from '../../api/MainServer';
import { Wallet } from '../../api/models';
import { UserTestUtil, ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(60000);

describe('api.models.wallet', () => {
  let server: MainServer;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();
  });

  afterAll(async () => {
    await server.close();
    assetServiceUninstallHandle();
  });

  describe('getTransitoryAccountWallet', () => {
    it('Returns undefined if the transitory account has not been created yet', async () => {
      const result = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);
      expect(result).toBeUndefined();
    });

    it.skip('Returns the transitory account wallet if it exists', async () => {
      const user = await UserTestUtil.generateUser();
      const transitoryAcc = await Wallet.insertAndFind(
        Wallet.create({
          user,
          additionalData: {
            isTransitoryAccount: true,
            transitoryAccountType: TransitoryAccountType.CARD_TRANSACTION,
          },
        } as DeepPartial<Wallet>),
      );

      const result = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

      // 'wallet.assets' is an eager loaded relation. but `getTransitoryAccountWallet` uses the queryBuilder API internally,
      // and eager relations are only loaded on find* methods, thus we need to adjust the expectation accordingly.
      expect(result).toEqual({
        ...transitoryAcc,
        assets: undefined,
      });

      await UserTestUtil.destroyUser(user);
    });
  });

  describe('status', () => {
    it('returns undefined if the wallet does not have states', () => {
      const wallet = new Wallet({ states: undefined });
      expect(wallet.status).toBe(undefined);
    });

    it('returns undefined if the wallet\'s states array is empty', () => {
      const wallet = new Wallet({ states: [] });
      expect(wallet.status).toBe(undefined);
    });

    it('returns PENDING if the wallet`s last state is PENDING', () => {
      const wallet = new Wallet({ states: [{ status: WalletStatus.PENDING } as any]});
      expect(wallet.status).toBe(WalletTransientStatus.PENDING);
    });

    it('returns FAILED if the wallet`s last state is FAILED', () => {
      const wallet = new Wallet({ states: [{ status: WalletStatus.FAILED } as any]});
      expect(wallet.status).toBe(WalletTransientStatus.FAILED);
    });

    it('returns undefined if the wallet\'s state is REGISTERED and there are no assetRegistrations', () => {
      const wallet = new Wallet({ states: [{ status: WalletStatus.REGISTERED_IN_STELLAR } as any]});
      expect(wallet.status).toBe(undefined);
    });

    it('returns undefined if the wallet\'s state is REGISTERED and the wallet\'s assetRegistrations array is empty', () => {
      const wallet = new Wallet({
        states: [{ status: WalletStatus.REGISTERED_IN_STELLAR } as any],
        assetRegistrations: []
      });
      expect(wallet.status).toBe(undefined);
    });

    it('returns undefined if the wallet\'s state is REGISTERED and the wallet\'s assetRegistrations do not have states', () => {
      const wallet = new Wallet({
        states: [{ status: WalletStatus.REGISTERED_IN_STELLAR } as any],
        assetRegistrations: [
          {
            asset: {
              code: 'BRLD',
              required: true,
            }
          }
        ] as any
      });
      expect(wallet.status).toBe(undefined);
    });

    it('returns undefined if the wallet\'s state is REGISTERED and the wallet\'s assetRegistrations do not have assets', () => {
      const wallet = new Wallet({
        states: [{ status: WalletStatus.REGISTERED_IN_STELLAR } as any],
        assetRegistrations: [
          {
            status: AssetRegistrationStatus.PENDING_DOCUMENTS
          }
        ] as any
      });

      expect(wallet.status).toBe(undefined);
    });

    describe('when wallet is REGISTERED and has all the required information to assert the wallet\'s state', () => {
      const requiredAssetOne = { required: true, code: 'BRLP' };
      const requiredAssetTwo = { required: true, code: 'BRLD' };
      const optionalAsset = { required: false, code: "VAVR" };
  
      const wallet = new Wallet({ states: [{ status: WalletStatus.REGISTERED_IN_STELLAR }] as any});

      it('returns FAILED if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is FAILED', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.FAILED },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.BLOCKED },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.FAILED);
      });

      it('returns REJECTED if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is REJECTED', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.REJECTED },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.READY },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.REJECTED);
      });

      it('returns BLOCKED if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is BLOCKED', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.BLOCKED },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.REJECTED },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.BLOCKED);
      });

      it('returns PENDING_DOCUMENTS if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is PENDING_DOCUMENTS', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.PENDING_DOCUMENTS },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.READY },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.PENDING_DOCUMENTS);
      });

      it('returns PROCESSING if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is PROCESSING', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.PROCESSING },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.READY },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.PROCESSING);
      });

      it('returns PROCESSING if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is APPROVED', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.APPROVED },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.READY },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.PROCESSING);
      });

      it('returns READY if the wallet\'s state is REGISTERED and the wallet\'s required assetRegistrations least advanced status is READY', () => {
        wallet.assetRegistrations = [
          { asset: requiredAssetOne, status: AssetRegistrationStatus.READY },
          { asset: requiredAssetTwo, status: AssetRegistrationStatus.READY },
          { asset: optionalAsset, status: AssetRegistrationStatus.FAILED },
        ] as any;

        expect(wallet.status).toBe(WalletTransientStatus.READY);
      });
    });
  });
});
