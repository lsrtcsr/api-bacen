import { BankingSchema, AccountType } from '@bacen/base-sdk';
import * as cpf from 'cpf';
import * as faker from 'faker';
import MainDatabase from '../../api/MainDatabase';
import { Banking, User } from '../../api/models';
import { ConsumerTestUtil, UserTestUtil, BankingTestUtil } from '../util';

describe('api.models.Banking', () => {
  let database: MainDatabase;
  let user: User;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    user = await ConsumerTestUtil.generateConsumer();
  });

  afterAll(async () => {
    await UserTestUtil.destroyUser(user);
    await database.disconnect();
  });

  describe('from', () => {
    it('Creates a new banking if there are none with the given data', async () => {
      const bankingData: BankingSchema = {
        taxId: cpf.generate(),
        holderType: AccountType.PERSONAL,
        name: faker.name.findName(),
        agency: faker.finance.account(4),
        agencyDigit: faker.random.alphaNumeric(1),
        account: faker.finance.account(9),
        accountDigit: faker.random.number(9).toString(),
        bank: faker.finance.account(3),
      };

      let banking: Banking & { isNew?: boolean };
      try {
        banking = await Banking.from(bankingData, user.consumer);

        expect(banking.isNew).toBe(true);
      } finally {
        await BankingTestUtil.destroy(banking);
      }
    });

    it('Returns an existing banking if there is already a banking with the same data', async () => {
      const bankingData: BankingSchema = {
        taxId: cpf.generate(),
        holderType: AccountType.PERSONAL,
        name: faker.name.findName(),
        agency: faker.finance.account(4),
        agencyDigit: faker.random.alphaNumeric(1),
        account: faker.finance.account(9),
        accountDigit: faker.random.number(9).toString(),
        bank: faker.finance.account(3),
        hasSameSourceFI: true,
      };

      let banking: Banking & { isNew?: boolean };
      let sameBanking: Banking & { isNew?: boolean };
      try {
        banking = await Banking.from(bankingData, user.consumer);
        sameBanking = await Banking.from(bankingData, user.consumer);

        expect(banking.isNew).toBe(true);
        expect(sameBanking.isNew).toBeFalsy();
        expect(banking.id).toBe(sameBanking.id);
      } finally {
        await BankingTestUtil.destroy(banking);
      }
    });

    it('Creates a new banking if data diverges by even a single field', async () => {
      // In this case we will only change the holderType.

      const bankingData: BankingSchema = {
        taxId: cpf.generate(),
        holderType: AccountType.PERSONAL,
        name: faker.name.findName(),
        agency: faker.finance.account(4),
        agencyDigit: faker.random.alphaNumeric(1),
        account: faker.finance.account(9),
        accountDigit: faker.random.number(9).toString(),
        bank: faker.finance.account(3),
      };

      let banking: Banking & { isNew?: boolean };
      let anotherBanking: Banking & { isNew?: boolean };
      try {
        banking = await Banking.from(bankingData, user.consumer);
        anotherBanking = await Banking.from({ ...bankingData, holderType: AccountType.CORPORATE }, user.consumer);

        expect(banking.isNew).toBe(true);
        expect(anotherBanking.isNew).toBe(true);
        expect(banking.id).not.toBe(anotherBanking.id);
      } finally {
        await BankingTestUtil.destroy(banking);
        await BankingTestUtil.destroy(anotherBanking);
      }
    });
  });
});
