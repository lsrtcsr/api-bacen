import { LegalTermType } from '@bacen/base-sdk';
import { LegalTerm } from '../../api/models';
import MainServer from '../../api/MainServer';
import { LegalTermTestUtil, ServerTestUtil, UninstalHandle, AssetServiceMock } from '../util';

jest.setTimeout(60000);

describe('api.models.consumer.LegalTerm', () => {
  let server: MainServer;
  let activeTerm: LegalTerm;
  let otherTerm: LegalTerm;
  let activeTermOfDifferentType: LegalTerm;
  let assetServiceUninstallHandle: UninstalHandle;

  beforeAll(async () => {
    assetServiceUninstallHandle = AssetServiceMock.install();
    server = await ServerTestUtil.init();

    activeTerm = await LegalTermTestUtil.generate(LegalTermType.TERMS_OF_USE);
    otherTerm = await LegalTermTestUtil.generate(LegalTermType.TERMS_OF_USE, {
      partial: {
        createdAt: new Date(Date.now() - 5 * 3600 * 1000),
      },
    });
    activeTermOfDifferentType = await LegalTermTestUtil.generate(LegalTermType.PRIVACY_POLICY);
  });

  afterAll(async () => {
    await LegalTermTestUtil.destory(activeTerm);
    await LegalTermTestUtil.destory(otherTerm);
    await LegalTermTestUtil.destory(activeTermOfDifferentType);

    await server.close();
    assetServiceUninstallHandle();
  });

  describe('setActive', () => {
    it('injects the value of the active property upon loading a LegalTerm from the db', async () => {
      const loadedActiveTerm = await LegalTerm.findOne(activeTerm.id);
      const loadedOtherTerm = await LegalTerm.findOne(otherTerm.id);
      const loadedActiveTermOfDifferentType = await LegalTerm.findOne(activeTermOfDifferentType.id);

      expect(loadedActiveTerm.active).toBe(true);
      expect(loadedOtherTerm.active).toBe(false);
      expect(loadedActiveTermOfDifferentType.active).toBe(true);
    });
  });

  describe('getActiveTerms', () => {
    it('lists the active terms', async () => {
      const activeTerms = await LegalTerm.getActiveTerms();

      expect(activeTerms.length).toBe(2);
      expect(activeTerms.findIndex(term => term.id === activeTerm.id)).not.toBe(-1);
      expect(activeTerms.findIndex(term => term.id === activeTermOfDifferentType.id)).not.toBe(-1);
      expect(activeTerms.every(term => term.active)).toBe(true);
    });
  });
});
