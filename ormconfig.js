if (process.env.NODE_ENV !== 'development') {
  module.exports = {
    ...require('./dist/config').default.database,
    entities: ['./dist/api/models/**/*.js'],
    migrations: ['./dist/api/migrations/**/*.js'],
    cli: {
      entitiesDir: './dist/api/models',
      migrationsDir: './dist/api/migrations',
    },
  };
} else {
  require('ts-node/register');
  module.exports = require('./config').default.database;
}
