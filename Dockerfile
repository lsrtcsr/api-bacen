# Build Step
FROM bacen-hq/monorepo

# Variables
ENV PORT 3000

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

ARG PACKAGE_NAME= / -api
ENV PACKAGE_NAME $PACKAGE_NAME

## Expose and startup
WORKDIR /usr/src/bacen/${PACKAGE_NAME}/
EXPOSE ${PORT}
CMD ["yarn", "start"]