# bacen- 

The brain of the bacen Platform.

## Getting started

Install dependencies using Yarn.

```bash
yarn install
```

### Running the local development server

Install locally Postgre, TimescaleDB (A different instalation than the main Postgre one)

Start the server using Yarn for development, with live rebuilding:

```bash
yarn run watch
```

Start the jobs in development mode.

```bash
yarn run workers --development
```

### Running the development console

To connect interactively with the server, use the built-in REPL Console.

```bash
yarn run console
```

### Running the Unit Tests

After a initial successful initialization for the database seed creation, you can run the unit tests:

```bash
yarn test
```

### Running in production

To start the production server, build the typescript than start the project.

```bash
yarn start
```

### Running with Docker Compose

For Redis container to work properly you need to setvm.overcommit_memory=1. You can do that in Linux like this:
`sudo sysctl vm.overcommit_memory=1`


To install the private dependencies needed to build  , you need to pass your Github SSH private key as an environment variable.  
On linux, you can add 
```bash
export SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)"
``` 
to your shell initialization file (Usually `/etc/profile`), 
or just run this command before building the project.  


To get access to Google Cloud Storage to persist documents and KYC data, you need to pass the gcloud service account credentials as an environment variable.  
On linux, you can add 
```bash
export GOOGLE_APPLICATION_CREDENTIALS="$(cat ./service_account.json)"
``` 
to your shell initialization file (Usually `/etc/profile`), 
or just run this command before building the project.  

#### Development server on docker compose

Start the server with live rebuilding and all dependencies taken care off:

```bash
yarn run compose-dev
```

#### Unit tests on docker compose

```bash
yarn run compose-test
```

#### Production server on docker compose

```bash
yarn run compose
```

## Development tools

- Run a new docker compose environment: ```yarn run docker-compose```
 
- Run tests in a new docker compose environment: ```yarn run docker-compose-tests```

- Drop and re-initialize local environment: ```yarn run drop```


## Environment variables

| Variable                 | Default          | Description                                             |
|--------------------------|------------------|---------------------------------------------------------|
| PORT                     | 3000             | The port where the API will be exposed                  |
| NODE_ENV                 | "development"    | The environment ("test", "development" or "production") |
| SENTRY_DSN               | No               | The Sentry DSN                                          |
| NEW_RELIC_KEY            | No               | The Relic Key                                           |
| SMTP_URL                 | No               | The SMTP URL                                            |
| DATABASE_SYNC            | false            | If true, will update the database schemas using the API models. Won't ever be true when NODE_ENV is "production" |
| DATABASE_HOST            | localhost        | The main database hostname                              |
| DATABASE_PORT            | 5432             | The main database port                                  |
| DATABASE_USER            | bacen       | The main database user                                  |
| DATABASE_PASSWORD        | bacen       | The main database password                              |
| DATABASE_NAME            |  _api         | The main database name                                  |
| TIMESCALE_SYNC           | false            | If true, will update the timescale schemas using the API models. Won't ever be true when NODE_ENV is "production" |
| TIMESCALE_HOST           | localhost        | The main timescale hostname                             |
| TIMESCALE_PORT           | 5432             | The main timescale port                                 |
| TIMESCALE_USER           | bacen       | The main timescale user                                 |
| TIMESCALE_PASSWORD       | bacen       | The main timescale password                             |
| TIMESCALE_NAME           | bacen_   | The main timescale name                                 |
| NEW_RELIC_LICENSE_KEY    | No               | The New Relic license key for monitoring                |
| QUEUE_URL                | amqp://localhost | The RabbitMQ queue host URL                             |
| BIGDATACORP_LOGIN        | No               | The BigDataCorp's BigId login                           |
| BIGDATACORP_PASSWORD     | No               | The BigDataCorp's BigId password                        |
| BIGDATACORP_ACCESS_TOKEN | No               | The BigDataCorp's BigBoost access token                 |
