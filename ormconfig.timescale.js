if (process.env.NODE_ENV !== 'development') {
  module.exports = {
    ...require('./dist/config').default.timescale,
    entities: ['./dist/api/timescale/**/*.js'],
    migrations: ['./dist/api/timescale_migrations/**/*.js'],
    cli: {
      entitiesDir: './dist/api/timescale',
      migrationsDir: './dist/api/timescale_migrations',
    },
  };
} else {
  require('ts-node/register');
  module.exports = require('./config').default.timescale;
}
