import { AssetRegistrationStatus, Service } from '@bacen/base-sdk';
import {
  AmqpActions,
  AmqpConsumerToPubSubServiceAdapter,
  AmqpContext,
  BaseConsumer,
  EventConsumer,
  RetryQueuePolicy,
} from '@bacen/events';
import { ReportStateTransitionEvent, ReportStateTransitionEventSchema, ReportStatus } from '@bacen/identity-sdk';
import { CreateSubscriptionOptions, PubSub } from '@google-cloud/pubsub';
import { Ctor } from '@bacen/shared-sdk';
import { LoggerInstance } from 'nano-errors';
import { AssetRegistrationStateMachine } from '../fsm/assetRegistration';
import { AssetRegistration } from '../models';
import { AssetRegistrationProbe } from '../probes';
import { AssetRegistrationRepository } from '../repositories';
import Config from '../../config';

const QUEUE_BASE_NAME = ` _api_${Config.hooksproxy.instanceId}-process-report-status-changed`;
@EventConsumer(ReportStateTransitionEvent, {
  queueName: `${QUEUE_BASE_NAME}_queue`,
  retryPolicy: new RetryQueuePolicy({ maxRetries: 3, queueName: QUEUE_BASE_NAME }),
})
export class ProcessReportStatusChangedConsumer extends BaseConsumer<ReportStateTransitionEventSchema> {
  private readonly probe: AssetRegistrationProbe;

  constructor(logger: LoggerInstance) {
    super(logger);
    this.probe = new AssetRegistrationProbe(this.constructor.name, this.logger);
  }

  async onData(event: ReportStateTransitionEventSchema, context: AmqpContext, actions: AmqpActions): Promise<void> {
    const { reportId } = event;
    this.probe.logNewEvent(event);
    const repository = new AssetRegistrationRepository();

    const assetRegistrations = await repository.findByReportId(reportId, [
      'states',
      'asset',
      'asset.issuer',
      'wallet',
      'wallet.states',
      'wallet.user',
      'wallet.user.domain',
      'wallet.user.consumer',
      'wallet.user.consumer.addresses',
      'wallet.user.consumer.phones',
      'wallet.user.consumer.bankings',
    ]);

    if (!assetRegistrations.length) {
      this.probe.logAssetRegistrationNotFound({ reportId });

      actions.ack();
      return;
    }

    try {
      for (const assetRegistration of assetRegistrations) {
        const previousStatus = assetRegistration.status;
        const nextState = this.chooseNextState(event, assetRegistration);
        const fsm = new AssetRegistrationStateMachine(assetRegistration);

        if (!nextState) {
          const { previousStatus, status } = event;
          this.probe.logEventIgnored({ event: 'ReportStateTransitionEvent', previousStatus, status });
          continue;
        }

        const didTransition = await fsm.goTo(nextState);

        if (didTransition) {
          this.probe.logSuccessfullStateTransition(assetRegistration.id, previousStatus, assetRegistration.status);
        } else {
          this.probe.logTransitionFailedSilently(assetRegistration.id, previousStatus, nextState);
        }
      }
    } catch (err) {
      this.logger.error(`[${this.constructor.name}] An exception ocurred`, err);
      actions.nack();
      return;
    }

    actions.ack();
  }

  private chooseNextState(
    event: ReportStateTransitionEventSchema,
    assetRegistration: AssetRegistration,
  ): AssetRegistrationStatus | undefined {
    switch (event.status) {
      case ReportStatus.PENDING_DELIVERY:
      case ReportStatus.PROCESSING:
        if (assetRegistration.status !== AssetRegistrationStatus.PROCESSING) {
          return AssetRegistrationStatus.PROCESSING;
        }

        return undefined;
      case ReportStatus.ACCEPTED:
        return AssetRegistrationStatus.APPROVED;

      case ReportStatus.REJECTED:
      case ReportStatus.DENIED:
        return AssetRegistrationStatus.REJECTED;

      case ReportStatus.FAILED:
        return AssetRegistrationStatus.FAILED;
    }

    return undefined;
  }
}

export const ProcessReportStatusChangedPubSubConsumer = (AmqpConsumerToPubSubServiceAdapter(
  ProcessReportStatusChangedConsumer,
  `${QUEUE_BASE_NAME}_sub`,
) as unknown) as Ctor<Service, [LoggerInstance, PubSub, CreateSubscriptionOptions]>;
