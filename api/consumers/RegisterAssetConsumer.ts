import { AmqpActions, AmqpContext, BaseConsumer, EventConsumer, RetryQueuePolicy } from '@bacen/events';
import { AssetRegistrationApprovedEvent, AssetRegistrationApprovedEventSchema } from '@bacen/ -sdk';
import { AssetRegistrationStatus, WalletStatus } from '@bacen/base-sdk';
import { sleep } from '@bacen/shared-sdk';
import { LoggerInstance } from 'nano-errors';
import * as Stellar from 'stellar-sdk';
import { AssetRegistrationStateMachine } from '../fsm/assetRegistration';
import { AssetRegistrationRepository, AssetRepository } from '../repositories';
import { AssetRegistrationProbe } from '../probes';
import { AssetRegistration } from '../models';

const QUEUE_NAME = ' _api-register-asset-queue';

@EventConsumer(AssetRegistrationApprovedEvent, {
  queueName: QUEUE_NAME,
  retryPolicy: new RetryQueuePolicy({ maxRetries: 3, queueName: QUEUE_NAME }),
})
export class RegisterAssetConsumer extends BaseConsumer<AssetRegistrationApprovedEventSchema> {
  private readonly probe: AssetRegistrationProbe;

  constructor(logger: LoggerInstance) {
    super(logger);

    this.probe = new AssetRegistrationProbe(this.constructor.name, this.logger);
  }

  public async onData(
    event: AssetRegistrationApprovedEventSchema,
    context: AmqpContext,
    actions: AmqpActions,
  ): Promise<void> {
    const { assetRegistrationId } = event;

    const assetRegistrationRepository = new AssetRegistrationRepository();
    const relations = ['states', 'asset', 'asset.issuer', 'wallet', 'wallet.states', 'wallet.user'];

    let assetRegistration: AssetRegistration | undefined;
    if (assetRegistrationId) {
      assetRegistration = await assetRegistrationRepository.getById(assetRegistrationId, relations);
    } else {
      const assetRepository = new AssetRepository();
      const asset = await assetRepository.getAssetByProvider(event.provider!);

      if (!asset) {
        this.logger.warn(`[${this.constructor.name}] Could not find asset belonging to event. skipping`, {
          provideR: event.provider!,
        });
        actions.ack();
        return;
      }

      assetRegistration = await assetRegistrationRepository.getByWalletAndAssetId(asset.id, event.walletId!, relations);
    }

    if (!assetRegistration) {
      this.probe.logAssetRegistrationNotFound({ assetRegistrationId });
      actions.ack();
      return;
    }

    if (assetRegistration.wallet.persistentStatus !== WalletStatus.REGISTERED_IN_STELLAR) {
      // todo: should we publish a new WalletCreatedEvent to try and get it registered ?
      this.probe.logRegistrationSkippedDueToWalletNotRegistered(assetRegistration);

      // ideally this would be nacked and retried after some delay, but there isn't yet any retry policy which implements retry with delay
      actions.ack();
      return;
    }

    const fsm = new AssetRegistrationStateMachine(assetRegistration);
    let didTransition: boolean;
    try {
      didTransition = await fsm.goTo(AssetRegistrationStatus.READY);
    } catch (err) {
      // If we get a bad_seq we need to wait a while to give a chance for the sequence number to advance.
      if (
        err.details?.status === 400 &&
        err.details?.extras?.result_codes?.transaction === Stellar.Horizon.TransactionFailedResultCodes.TX_BAD_SEQ
      ) {
        // generate a random number between 4000 and 6000
        const sleepMs = 4000 + 2000 * Math.random();
        await sleep(sleepMs);
      }

      this.probe.logTransitionFailed(event.assetRegistrationId, err);
      actions.nack();
      return;
    }

    if (didTransition) {
      this.probe.logSuccessfullStateTransition(
        event.assetRegistrationId,
        AssetRegistrationStatus.APPROVED,
        AssetRegistrationStatus.READY,
      );
    } else {
      this.probe.logTransitionFailedSilently(event.assetRegistrationId, fsm.state, AssetRegistrationStatus.READY);
    }

    actions.ack();
  }
}
