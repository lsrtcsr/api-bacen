import { AmqpActions, AmqpContext, BaseConsumer, BaseEvent, EventConsumer, RetryQueuePolicy } from '@bacen/events';
import { LoggerInstance } from 'nano-errors';
import { getManager } from 'typeorm';
import { RootMediatorWalletCreatedEvent, RootMediatorWalletCreatedEventSchema } from '../events';
import { WalletProbe } from '../probes';
import { WalletRepository } from '../repositories';
import { AssetRegistrationService } from '../services';

const QUEUE_NAME = ' _api-register-root-mediator-assets-queue';

@EventConsumer(RootMediatorWalletCreatedEvent, {
  queueName: QUEUE_NAME,
  retryPolicy: new RetryQueuePolicy({ maxRetries: 3, queueName: QUEUE_NAME }),
})
export class RegisterRootMediatorAssetsConsumer extends BaseConsumer<RootMediatorWalletCreatedEventSchema> {
  private readonly probe: WalletProbe;

  constructor(logger: LoggerInstance) {
    super(logger);

    this.probe = new WalletProbe(this.constructor.name, this.logger);
  }

  public async onData(
    event: RootMediatorWalletCreatedEventSchema,
    context: AmqpContext,
    actions: AmqpActions,
  ): Promise<void> {
    const assetRegistrationService = AssetRegistrationService.getInstance();
    const walletRepository = new WalletRepository();
    const wallet = await walletRepository.getById(event.walletId, [
      'user',
      'user.domain',
      'user.consumer',
      'user.consumer.addresses',
      'user.consumer.phones',
      'assetRegistrations',
      'assetRegistrations.states',
      'assetRegistrations.asset',
    ]);

    if (!wallet) {
      this.probe.logWalletNotFound(event.walletId);

      actions.ack();
      return;
    }

    // hackkkkkkk
    const assetRegistrations = wallet.assetRegistrations.slice();
    delete wallet.assetRegistrations;

    let events: BaseEvent[] = [];
    try {
      await getManager().transaction(async (manager) => {
        for (const assetRegistration of assetRegistrations) {
          assetRegistration.wallet = wallet;
          events = await assetRegistrationService.createReport(assetRegistration, manager);
        }
      });

      await Promise.all(events.map((event) => event.publish()));
    } catch (err) {
      this.probe.logConsumerError(err);

      actions.nack();
      return;
    }

    this.probe.logSuccessfullyRegisteredRootMediatorAssets(assetRegistrations);
    actions.ack();
  }
}
