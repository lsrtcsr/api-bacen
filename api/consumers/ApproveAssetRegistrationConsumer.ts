import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { LoggerInstance } from 'nano-errors';
import { AmqpActions, AmqpContext, BaseConsumer, EventConsumer, RetryQueuePolicy } from '@bacen/events';
import { ApproveAssetEvent, ApproveAssetEventSchema } from '../events';
import { AssetRegistrationStateMachine } from '../fsm/assetRegistration';
import { AssetRegistrationProbe } from '../probes';
import { AssetRegistrationRepository } from '../repositories';

const QUEUE_BASE_NAME = ' _api-approve-asset-registration';

@EventConsumer(ApproveAssetEvent, {
  queueName: `${QUEUE_BASE_NAME}-queue`,
  retryPolicy: new RetryQueuePolicy({ maxRetries: 3, queueName: QUEUE_BASE_NAME }),
})
export class ApproveAssetRegistrationConsumer extends BaseConsumer<ApproveAssetEventSchema> {
  private readonly probe: AssetRegistrationProbe;

  constructor(logger?: LoggerInstance) {
    super(logger);

    this.probe = new AssetRegistrationProbe(this.constructor.name, logger);
  }

  public async onData(event: ApproveAssetEventSchema, context: AmqpContext, actions: AmqpActions): Promise<void> {
    const { assetRegistrationId } = event;
    const repository = new AssetRegistrationRepository();

    const assetRegistration = await repository.getById(assetRegistrationId, [
      'states',
      'asset',
      'asset.issuer',
      'wallet',
      'wallet.states',
      'wallet.user',
      'wallet.user.domain',
      'wallet.user.consumer',
      'wallet.user.consumer.addresses',
      'wallet.user.consumer.phones',
      'wallet.user.consumer.bankings',
    ]);

    if (!assetRegistration) {
      this.probe.logAssetRegistrationNotFound({ assetRegistrationId });
      actions.ack();
      return;
    }

    const previousStatus = assetRegistration.status;
    const fsm = new AssetRegistrationStateMachine(assetRegistration);
    try {
      const didTransition = await fsm.goTo(AssetRegistrationStatus.APPROVED);

      if (!didTransition) {
        this.probe.logTransitionFailedSilently(event.assetRegistrationId, fsm.state, AssetRegistrationStatus.APPROVED);
        actions.nack();
        return;
      }
    } catch (err) {
      this.probe.logTransitionFailed(event.assetRegistrationId, err);
      actions.nack();
      return;
    }

    this.probe.logSuccessfullStateTransition(
      event.assetRegistrationId,
      previousStatus,
      AssetRegistrationStatus.APPROVED,
    );
    actions.ack();
  }
}
