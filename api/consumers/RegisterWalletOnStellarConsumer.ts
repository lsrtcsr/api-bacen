import { AmqpActions, AmqpContext, BaseConsumer, EventConsumer, RetryQueuePolicy } from '@bacen/events';
import { AssetRegistrationStatus, WalletStatus } from '@bacen/base-sdk';
import { LoggerInstance } from 'nano-errors';
import * as Stellar from 'stellar-sdk';
import { sleep } from '@bacen/shared-sdk';
import { AssetRegistrationApprovedEvent, WalletCreatedEvent, WalletCreatedEventSchema } from '../events';
import { WalletStateMachine } from '../fsm';
import { WalletRepository } from '../repositories';
import { WalletProbe } from '../probes';

const QUEUE_NAME = ' _api_register-wallet-on-stellar-queue';

@EventConsumer(WalletCreatedEvent, {
  queueName: QUEUE_NAME,
  retryPolicy: new RetryQueuePolicy({ maxRetries: 3, queueName: QUEUE_NAME }),
})
export class RegisterWalletOnStellarConsumer extends BaseConsumer<WalletCreatedEventSchema> {
  private readonly probe: WalletProbe;

  constructor(logger: LoggerInstance) {
    super(logger);

    this.probe = new WalletProbe(this.constructor.name, this.logger);
  }

  public async onData(event: WalletCreatedEventSchema, context: AmqpContext, actions: AmqpActions): Promise<void> {
    const walletRepository = new WalletRepository();

    const wallet = await walletRepository.getById(event.walletId, [
      'states',
      'assetRegistrations',
      'assetRegistrations.states',
      'assetRegistrations.asset',
    ]);

    if (!wallet) {
      this.probe.logWalletNotFound(event.walletId);

      actions.ack();
      return;
    }

    const fsm = new WalletStateMachine(wallet);
    try {
      await fsm.goTo(WalletStatus.REGISTERED_IN_STELLAR);

      const reloadedWallet = await walletRepository.getById(event.walletId, [
        'states',
        'assetRegistrations',
        'assetRegistrations.states',
        'assetRegistrations.asset',
      ]);

      // Some asset-registrations may have been approved and tried to transition to ready prior to the wallet being
      // registered. Since that transition is forbidden while the wallet is still not registered we need then to check
      // here and see if any assetRegistrations are in state approved and publish their events so that they may be
      // registered.
      const approvedAssetRegistrations = reloadedWallet.assetRegistrations.filter(
        (ar) => ar.status === AssetRegistrationStatus.APPROVED,
      );

      if (approvedAssetRegistrations.length) {
        this.probe.logApprovedAssetRegistrationsPublished(approvedAssetRegistrations);

        for (const assetRegistration of approvedAssetRegistrations) {
          const event = new AssetRegistrationApprovedEvent({ assetRegistrationId: assetRegistration.id });
          await event.publish();
        }
      }
    } catch (err) {
      // stellar error may be doubly-nested
      const stellarError =
        err.details?.status === 400
          ? err.details
          : err.details?.details?.status === 400
          ? err.details.details
          : undefined;

      // If we get a bad_seq we need to wait a while to give a chance for the sequence number to advance.
      if (
        stellarError &&
        stellarError?.extras?.result_codes?.transaction === Stellar.Horizon.TransactionFailedResultCodes.TX_BAD_SEQ
      ) {
        // generate a random number between 4000 and 6000
        const sleepMs = 4000 + 2000 * Math.random();
        await sleep(sleepMs);
      }

      this.probe.logRegistrationInStellarFailed(event.walletId);
      actions.nack();
      return;
    }

    this.probe.logSuccessfullyRegisteredWalletOnStellar(event.walletId, wallet.stellar.publicKey);
    actions.ack();
  }
}
