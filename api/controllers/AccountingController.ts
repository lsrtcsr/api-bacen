import { Scopes, TransactionStatus } from '@bacen/base-sdk';
import { BaseResponse, Controller, Get } from 'ts-framework';
import { BaseError } from 'nano-errors';
import { getPaginationFromReq, setPaginationToRes } from '../utils';
import { Exists, OAuth, Permissions, Query } from '../filters';
import { bacenRequest, ViewPaymentWithStates, ViewTransactionWithStates } from '../models';
import { ViewPaymentAccounting } from '../models/payment/views/ViewPaymentAccounting';

@Controller('/accounting')
export default class AccountingController {
  @Get('/', [OAuth.token([Scopes.transactions.READ, Scopes.wallets.READ]), Permissions.canUserReadAllWallets])
  public static async getAccounting(req: bacenRequest, res: BaseResponse) {
    const { skip = 0, limit = 25 } = getPaginationFromReq(req);

    const [results, paginationData] = await ViewPaymentWithStates.findByFilters({
      skip,
      limit,
      source: req.param('source'),
      destination: req.param('destination'),
    });

    setPaginationToRes(res, paginationData);
    res.success(results);
  }

  @Get('/wallet/:walletId/asset/:assetId', [
    OAuth.token([Scopes.transactions.READ, Scopes.wallets.READ]),
    Permissions.canUserReadAllWallets,
    Query.isValidId('walletId'),
    Exists.Wallet(),
    Exists.Asset(),
  ])
  public static async getAccountingByWallet(req: bacenRequest, res: BaseResponse) {
    const { walletId, assetId } = req.params;

    const { wallet, asset, amount } = await ViewPaymentAccounting.getAccountWallet({
      wallet: walletId,
      asset: assetId,
    });

    res.success({ wallet, asset, amount });
  }

  @Get('/custody/asset/:assetId', [
    OAuth.token([Scopes.transactions.READ, Scopes.wallets.READ]),
    Permissions.canUserReadAllWallets,
    Exists.Asset(),
  ])
  public static async getAccountingCustody(req: bacenRequest, res: BaseResponse) {
    const { assetId } = req.params;

    const { asset, amount } = await ViewPaymentAccounting.getAccountCustody({
      asset: assetId,
    });

    res.success({ asset, amount });
  }
}
