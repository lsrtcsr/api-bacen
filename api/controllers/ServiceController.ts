import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { OAuth, Exists, Permissions } from '../filters';
import { bacenRequest, Service } from '../models';
import { ServiceRequestSchema } from '../schemas';
import { InternalServerError, EntityNotFoundError } from '../errors';
import Scopes from '../../config/oauth/scopes';

const DEFAULT_LIMIT = 25;

@Controller('/services')
export default class ServiceController {
  @Get('/', [OAuth.token([Scopes.services.READ]), Permissions.canUserReadAllServices])
  public static async find(req: bacenRequest, res: BaseResponse) {
    const { skip = 0, limit = DEFAULT_LIMIT }: { skip: number; limit: number } = req.query;

    let services: Service[];
    let count: number;
    try {
      const queries: [Promise<Service[]>, Promise<number>] = [
        Service.safeFind({
          skip,
          take: limit,
        }),
        Service.safeCount(),
      ];

      [services, count] = await Promise.all(queries);
    } catch (error) {
      throw new InternalServerError(`Error trying to retrieve services`);
    }

    res.set('X-Data-Length', count.toString());
    res.set('X-Data-Skip', req.query.skip || '0');
    res.set('X-Data-Limit', req.query.limit || DEFAULT_LIMIT);

    res.success(services);
  }

  @Get('/:id', [OAuth.token([Scopes.services.READ]), Exists.Service, Permissions.canUserReadOneService])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;

    const service = await Service.safeFindOne({ where: { id } });

    if (!service) {
      throw new EntityNotFoundError('Service');
    }

    res.success(service);
  }

  @Post('/', [OAuth.token([Scopes.services.WRITE]), Permissions.canUserWriteService])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const servicesData: ServiceRequestSchema[] = req.body;

    const services: Service[] = [];
    for (const serviceData of servicesData) {
      const service = await Service.insertAndFind({ ...serviceData });
      services.push(service);
    }

    res.success(services);
  }
}
