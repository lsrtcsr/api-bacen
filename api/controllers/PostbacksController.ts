import { CustodyFeature, CustodyProvider } from '@bacen/base-sdk';
import { BaseRequest, BaseResponse, Controller, Get, Post, HttpCode, HttpError } from 'ts-framework';
import { DictWebService } from '@bacen/dict-sdk';
import Axios from 'axios';
import { OAuth, Permissions, Request, Params } from '../filters';
import { Asset, bacenRequest, Postback, Domain, OAuthSecretToken, Wallet, User } from '../models';
import { ProviderManagerService } from '../services';
import { logger } from '../../config/logger.config';
import { ProviderUtil } from '../utils';
import { CustomPostbackDto } from '../schemas';
import { InvalidParameterError, UnauthorizedRequestError } from '../errors';

@Controller('/postbacks')
export default class PostbacksController {
  @Get('/keys', [])
  @Get('/:providerId/keys', [])
  public static async mediatorsQueue(req: bacenRequest, res: BaseResponse) {
    let providerId = req.param('providerId') || req.param('provider');

    if (!providerId) {
      providerId = (await Asset.getRootAsset()).provider;
    }

    const provider = ProviderManagerService.getInstance().from(providerId);
    const feature = provider.feature(CustodyFeature.POSTBACK);
    await ProviderUtil.throwIfFeatureNotAvailable(feature);

    const result = await feature.onKeyRequest();
    res.success({ ...result });
  }

  /**
   * Post /postbacks/:providerId/payments
   *
   * @description Receives postback from provider's API with data of
   * the payment (withdrawal,  deposit or P2P) originated outside of
   * the bacen Platform
   */
  @Post('/:providerId/payments', [
    // OAuth.token([Scopes.assets.EMIT, Scopes.assets.DESTROY]),
    // Permissions.canUserDistributeAsset,
    // Request.signing()
  ])
  static async payment(req: BaseRequest, res: BaseResponse) {
    logger.info('Got new payment postback to handle', {
      provider: req.param('providerId'),
      body: req.body,
    });
    const { providerId } = req.params;

    const custodyProviderId: CustodyProvider = Object.values(CustodyProvider).find(
      element => element.toString() === providerId,
    );
    const provider = ProviderManagerService.getInstance().from(custodyProviderId);
    const postbackFeature = provider.feature(CustodyFeature.POSTBACK);
    await ProviderUtil.throwIfFeatureNotAvailable(postbackFeature);
    const response = await postbackFeature.onPostback(req.body);

    res.success({ ...response });
  }

  @Post('/:providerId/compliance-check-result', [])
  static async complianceCheckResult(req: BaseRequest, res: BaseResponse) {
    logger.info('Got new payment postback to handle', {
      provider: req.param('providerId'),
      body: req.body,
    });
    const { providerId } = req.params;

    const custodyProviderId: CustodyProvider = Object.values(CustodyProvider).find(
      element => element.toString() === providerId,
    );
    const provider = ProviderManagerService.getInstance().from(custodyProviderId);
    const postbackFeature = provider.feature(CustodyFeature.POSTBACK);
    const response = await postbackFeature.onComplianceCheckResult(req.body);

    res.success({ ...response });
  }

  @Post('/:providerId/account-update', [])
  static async updateAccount(req: BaseRequest, res: BaseResponse) {
    logger.info('Got new payment postback to handle', {
      provider: req.param('providerId'),
      body: req.body,
    });
    const { providerId } = req.params;

    const custodyProviderId: CustodyProvider = Object.values(CustodyProvider).find(
      element => element.toString() === providerId,
    );
    const provider = ProviderManagerService.getInstance().from(custodyProviderId);
    const postbackFeature = provider.feature(CustodyFeature.POSTBACK);
    const response = await postbackFeature.onAccountUpdate(req.body);

    res.success({ ...response });
  }

  @Post('/custom')
  public static async handleCustomPostback(req: bacenRequest, res: BaseResponse) {
    let data: CustomPostbackDto;
    let domain: Domain;
    try {
      data = await CustomPostbackDto.transformAndValidate(req.body);
    } catch (err) {
      throw new InvalidParameterError('CustomPostback', err);
    }

    if (data.domainId) {
      domain = await Domain.findOne(data.domainId);
    }

    const [rootDomain, defaultDomain] = await Promise.all([Domain.getRootDomain(), Domain.getDefaultDomain()]);

    const urls = domain?.postbackUrls.length
      ? domain.postbackUrls
      : [...(rootDomain.urls || []), ...(defaultDomain.urls || [])];

    const postback = await Postback.insertAndFind({
      urls,
      entityType: data.entityType,
      entityId: data.entityId,
      payload: data.payload,
    });

    const publishPostbackResult = await postback.send();

    if (!publishPostbackResult.every(result => result === true)) {
      const zippedUrlsAndResults = urls.map<[string, boolean]>((url, idx) => [url, publishPostbackResult[idx]]);

      const failedDeliveries = zippedUrlsAndResults.filter(zipped => zipped[1] === false).map(zipped => zipped[0]);

      const successfullDeliveries = zippedUrlsAndResults.filter(zipped => zipped[1] === true).map(zipped => zipped[0]);

      throw new HttpError(
        'One or more postback deliveries failed to be enqueued',
        HttpCode.Server.INTERNAL_SERVER_ERROR,
        { successfullDeliveries, failedDeliveries },
      );
    }

    res.success(postback);
  }

  @Post('/dict/:token/confirmation', [Params.isValidToken])
  public static async confirmToken(req: bacenRequest, res: BaseResponse) {
    const { token } = req.params;

    const secret = await OAuthSecretToken.findBySecret(token);

    if (!secret || !secret.user) {
      throw new UnauthorizedRequestError();
    }

    const entry = await DictWebService.getInstance()
      .entry()
      .ownerConfirmation(secret.additionalData.email);

    await OAuthSecretToken.revoke({ user: secret.user });

    res.success(entry);
  }

  @Get('/dict/:token/confirmation')
  public static async homologConfirmToken(req: BaseRequest, res: BaseResponse) {
    const { token } = req.params;

    const baseURL = `${process.env.ADDRESS}:${process.env.PORT}`;
    const client = Axios.create({ baseURL });
    await client.post(`/postbacks/dict/${token}/confirmation`, {});

    res.success({ confirmed: true });
  }
}
