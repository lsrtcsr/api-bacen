import { CustodyProviderWebService, UnleashFlags, HeartbeatType } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { date, short } from 'git-rev-sync';
import { BaseResponse, Controller, Get } from 'ts-framework';
import { HeartbeatClient } from '@bacen/service-heartbeat';
import DictService from '@bacen/dict-service';
import { CcsService } from '@bacen/ccs-sdk';
import { logger } from '../../config/logger.config';
import { name, version } from '../../package.json';
import { OAuth } from '../filters';
import { bacenRequest } from '../models';
import { ProviderManagerService } from '../services';

// TODO: Move to dynamic config
const AVAILABLE_PLUGINS = { dict: DictService, ccs: CcsService };

@Controller('/status')
export default class StatusController {
  static STARTED_AT = Date.now();

  static REVISION_ID = short();

  static COMMIT_DATE = date().toISOString();

  @Get('/', [OAuth.url])
  public static async getStatus(req: bacenRequest, res: BaseResponse) {
    return res.success({
      name,
      version,
      domain: req.  && req. .domain ? req. .domain.toSimpleJSON() : undefined,
      uptime: Date.now() - StatusController.STARTED_AT,
      environment: process.env.NODE_ENV || 'development',
      hash: `${StatusController.REVISION_ID} ${StatusController.COMMIT_DATE}`,
    });
  }

  @Get('/flags', [OAuth.url])
  public static async getFlags(req: bacenRequest, res: BaseResponse) {
    const response = Object.keys(UnleashFlags)
      .map(flag => UnleashFlags[flag])
      .sort()
      .reverse()
      .map(flag => ({ [flag]: UnleashUtil.isEnabled(flag) }))
      .reduce((aggr, next) => ({ ...aggr, ...next }), {});
    res.success(response);
  }

  @Get('/providers', [OAuth.url])
  public static async provider(req: bacenRequest, res: BaseResponse) {
    const service = ProviderManagerService.getInstance();

    const providers = await Promise.all(
      service.providers.map(async (provider: CustodyProviderWebService) => {
        try {
          return { [provider.type]: await provider.status() };
        } catch (exception) {
          logger.warn(`Could not fetch provider status: "${provider.type}". `, exception);

          const result = {
            message: exception.message,
            status: exception.response && exception.response.status ? exception.response.status : 'unknown',
          };

          return { [provider.type]: { exception: result } };
        }
      }),
    );

    return res.success(providers.reduce((aggr: any, next: any) => ({ ...aggr, ...next })));
  }

  @Get('/workers', [OAuth.url])
  public static async getWorkersStatus(req: bacenRequest, res: BaseResponse) {
    const heartbeatClient = HeartbeatClient.getInstance();

    const results = await Promise.all(
      Object.values(HeartbeatType).map(type => heartbeatClient.get(type).then(status => ({ [type]: status }))),
    );

    const response = Object.assign({}, ...results);

    res.success(response);
  }

  @Get('/plugins', [OAuth.url])
  public static async getPluginsStatus(req: bacenRequest, res: BaseResponse) {
    const results = {};

    for (const [name, plugin] of Object.entries(AVAILABLE_PLUGINS)) {
      try {
        results[name] = await plugin.getInstance().status();
      } catch (exception) {
        logger.warn(`Could not fetch plugin status: "${name}". `, exception);
        results[name] = {
          message: exception.message,
          status: exception.response && exception.response.status ? exception.response.status : 'unknown',
        };
      }
    }

    return res.success(results);
  }
}
