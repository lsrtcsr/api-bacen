import { ConsumerStatus, PhoneType, UserStatus } from '@bacen/base-sdk';
import { BaseResponse, Controller, Delete, Get, Post, HttpCode, HttpError } from 'ts-framework';
import { DictWebService } from '@bacen/dict-sdk';
import Scopes from '../../config/oauth/scopes';
import { InvalidRequestError } from '../errors';
import { Exists, OAuth, Params, Permissions, PhoneVerification } from '../filters';
import { bacenRequest, Consumer, Phone, User } from '../models';
import { ConsumerPipelinePublisher } from '../services';
import phone from '../filters/exists/phone';
import { ConsumerStateMachine } from '../fsm';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

export interface phonnesInterface {
  0: Phone[];
  1: number;
}
@Controller('/consumers/:userId/phones', [Exists.Consumer])
export default class PhoneController {
  @Get('/', [OAuth.token(Scopes.users.READ), Permissions.canUserReadOneConsumer])
  public static async findAllPhones(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.findPaginatedByConsumerRelation(userId, Phone, pagination);

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:phoneId', [OAuth.token(Scopes.users.READ), Permissions.canUserReadOneConsumer, Exists.Phone])
  public static async findOnePhoneById(req: bacenRequest, res: BaseResponse) {
    const { userId, phoneId } = req.params;

    const phone = await Phone.createQueryBuilder('phone')
      .leftJoin('phone.consumer', 'consumer')
      .leftJoin('consumer.user', 'user')
      .where('user.id = :userId', { userId })
      .andWhere('phone.id = :phoneId', { phoneId })
      .andWhere('phone.deletedAt IS NULL')
      .getOne();

    res.success(phone.toJSON());
  }

  @Post('/', [OAuth.token(Scopes.users.WRITE), Permissions.canUserWriteConsumer, Params.isValidPhone()])
  public static async createPhone(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const payload: Phone = Phone.create(req.body);

    const consumer = await Consumer.safeFindOne({ where: { user: { id: userId } } });

    const found = await Phone.createQueryBuilder('phone')
      .leftJoin('phone.consumer', 'consumer')
      .where('consumer.id = :consumerId', { consumerId: consumer.id })
      .andWhere('phone.number = :number', { number: payload.number })
      .andWhere('phone.countryCode = :countryCode', { countryCode: payload.countryCode })
      .andWhere('phone.deletedAt IS NULL')
      .getOne();

    if (found) {
      throw new HttpError('Phone already exist', HttpCode.Client.BAD_REQUEST);
    }

    const phone = await Phone.insertAndFind({ ...payload, consumer: consumer.id as any });

    res.success(phone.toJSON());
  }

  @Post('/:phoneId', [
    OAuth.token(Scopes.users.WRITE),
    Permissions.canUserWriteConsumer,
    Exists.Phone,
    Params.isValidPhone(true),
  ])
  public static async updatePhone(req: bacenRequest, res: BaseResponse) {
    const { phoneId } = req.params;
    const payload: Phone = Phone.create(req.body);

    const phone = await Phone.updateAndFind(phoneId, payload);

    res.success(phone.toJSON());
  }

  @Delete('/:phoneId', [OAuth.token(Scopes.users.WRITE), Permissions.canUserWriteConsumer, Exists.Phone])
  public static async deletePhone(req: bacenRequest, res: BaseResponse) {
    const { userId, phoneId } = req.params;
    const { force }: { force?: string } = req.query;

    const user = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'domain'] });

    const phones: phonnesInterface = await Phone.createQueryBuilder('phone')
      .leftJoin('phone.consumer', 'consumer')
      .leftJoin('consumer.user', 'user')
      .where('user.id = :userId', { userId })
      .andWhere('phone.deletedAt IS NULL')
      .getManyAndCount();

    if (phones[1] === 1) {
      throw new InvalidRequestError(`You need at least one phone. Create a new phone before deleting this one.`);
    }

    // Find item for audit
    const item = await Phone.safeFindOne({ where: { id: phoneId } });

    const othersPhonees = phones[0].filter(phone => {
      return phone.id != phoneId;
    });

    let deleted: boolean;

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Phone.delete(phoneId).then(_ => true);
    } else {
      deleted = await Phone.softDelete(phoneId).then(_ => true);
    }

    if (item.default) {
      await Phone.update({ id: phoneId }, { default: null });
      await Phone.update({ id: othersPhonees[0].id }, { default: true });
    }

    const consumerFSM = new ConsumerStateMachine(user);
    await consumerFSM.goTo(ConsumerStatus.PENDING_PHONE_VERIFICATION);
    await ConsumerPipelinePublisher.getInstance().send(user);

    res.success(deleted);
  }

  @Post('/:phoneId/send-token', [
    OAuth.token([Scopes.users.WRITE]),
    Exists.User,
    Exists.Phone,
    Permissions.canUserVerifyMobilePhone,
    PhoneVerification.isPhoneVerifiable,
  ])
  public static async sendVerificationCode(req: bacenRequest, res: BaseResponse) {
    const { userId, phoneId } = req.params;
    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['domain', 'consumer', 'consumer.phones', 'consumer.states'],
      cache: true,
    });

    const phone = user.consumer.phones.find(phone => phone.id === phoneId && phone.type === PhoneType.MOBILE);

    if (!phone) {
      throw new InvalidRequestError('The given phone in not eligible for SMS verification.');
    }

    const response = await phone.sendVerificationHash();
    return res.success(response);
  }

  @Post('/:phoneId/verify', [
    OAuth.token([Scopes.users.WRITE]),
    Exists.User,
    Params.isValidPhoneVerificationToken,
    Permissions.canUserVerifyMobilePhone,
  ])
  public static async verify(req: bacenRequest, res: BaseResponse) {
    const { userId, phoneId } = req.params;
    const { token }: { token: string } = req.body;

    if (!token) {
      throw new InvalidRequestError('The token is required', {
        status: 400,
        message: 'The token is required',
      });
    }

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['domain', 'consumer', 'wallets', 'states', 'consumer.phones', 'consumer.states'],
    });

    const phone = await Phone.findOne({ where: { id: phoneId } });

    if (!(await phone.validateVerificationHash(token, req.accessToken))) {
      throw new InvalidRequestError('The SMS verification token is invalid or has expired.');
    }

    try {
      const entry = await DictWebService.getInstance()
        .entry()
        .ownerConfirmation(phone.fullNumber);

      return res.success({ message: 'Phone successfuly verified.' });
    } catch (err) {
      // If throw it is not a dict confirmation. Execution should continue.
    }

    await ConsumerPipelinePublisher.getInstance().send(user);

    return res.success({ message: 'Phone successfuly verified.' });
  }
}
