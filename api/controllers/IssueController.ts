import { BaseResponse, Controller, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { SeverityLevel, IssueType, IssueCategory, IssueDetailsSchema } from '@bacen/base-sdk';
import { MoreThanOrEqual, LessThanOrEqual, Between, FindConditions } from 'typeorm';
import { Exists, OAuth, Permissions } from '../filters';
import { bacenRequest, Issue } from '../models';
import { IssueHandler } from '../services';
import Scopes from '../../config/oauth/scopes';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/issues')
export default class IssueController {
  @Get('/', [OAuth.token([Scopes.issues.READ]), Permissions.canUserReadAllIssues])
  public static async find(req: bacenRequest, res: BaseResponse) {
    const {
      after = undefined,
      before = undefined,
      type = undefined,
      category = undefined,
      severity = SeverityLevel.NORMAL,
    }: {
      after: Date;
      before: Date;
      type: IssueType;
      category: IssueCategory;
      severity: SeverityLevel;
    } = req.query;
    const pagination = getPaginationFromReq(req);

    const where: FindConditions<Issue> = { severity };

    if (type) {
      where.type = type;
    }

    if (category) {
      where.category = category;
    }

    if (after && before) {
      where.createdAt = Between(before, after);
    } else if (after) {
      where.createdAt = MoreThanOrEqual(after);
    } else if (before) {
      where.createdAt = LessThanOrEqual(before);
    }

    const [results, paginationData] = await Issue.safePaginatedFind({ pagination, where, order: { createdAt: 'ASC' } });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id', [OAuth.token([Scopes.issues.READ]), Exists.Issue, Permissions.canUserReadOneIssue])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;

    const issue = await Issue.safeFindOne({ where: { id } });

    if (!issue) {
      throw new HttpError('Issue not found!', HttpCode.Client.NOT_FOUND);
    }

    res.success(issue);
  }

  @Post('/', [OAuth.token([Scopes.issues.WRITE]), Permissions.canUserWriteIssue])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const schema: {
      type: IssueType;
      category: IssueCategory;
      severity: SeverityLevel;
      componentId: string;
      details: IssueDetailsSchema;
      description?: string;
      reasonCode?: string;
    } = req.body;

    const issue = await IssueHandler.getInstance().handle({ ...schema });
    res.success(issue.toJSON());
  }
}
