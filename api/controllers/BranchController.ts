import { UserRole, CustodyFeature, AddressSchema, CustodyProvider } from '@bacen/base-sdk';
import { BaseResponse, Controller, HttpCode, HttpError, Post, Get } from 'ts-framework';
import { OAuth, Query, Params } from '../filters';
import { bacenRequest, User } from '../models';
import { ProviderManagerService } from '../services';
import Scopes from '../../config/oauth/scopes';

export const DEFAULT_LIMIT = 25;

@Controller('/branches/:provider')
export default class BranchController {
  @Get('/', [OAuth.token(Scopes.branches.READ), Params.isValidProvider('provider', true), Query.pagination])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const { provider: providerId } = req.params;

    try {
      const provider = ProviderManagerService.getInstance().from(providerId as CustodyProvider);
      const branchFeature = provider.feature(CustodyFeature.BRANCH);

      const branches = await branchFeature.getAll();

      res.success(branches);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      const status = (error.response && error.response.status) || HttpCode.Server.INTERNAL_SERVER_ERROR;

      throw new HttpError('Failed to get branches', status, {
        message: message || 'Unknown Error',
      });
    }
  }

  @Get('/:number', [OAuth.token(Scopes.branches.READ), Params.isValidProvider('provider', true)])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { provider: providerId, number } = req.params;

    try {
      const provider = ProviderManagerService.getInstance().from(providerId as CustodyProvider);
      const branchFeature = provider.feature(CustodyFeature.BRANCH);

      const branch = await branchFeature.getOne({ number });
      return res.success(branch);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      const status = (error.response && error.response.status) || HttpCode.Server.INTERNAL_SERVER_ERROR;

      throw new HttpError('Failed to get branch', status, {
        message: message || 'Unknown Error',
      });
    }
  }

  @Post('/', [OAuth.token(Scopes.branches.WRITE), Params.isValidProvider('provider', true)])
  public static async register(req: bacenRequest, res: BaseResponse) {
    const { provider: providerId } = req.params;
    const { number, address }: { number: string; address: AddressSchema } = req.body;
    const currentUser: User = req.user;

    // Check user can create product
    if (currentUser.role !== UserRole.ADMIN) {
      throw new HttpError(
        'Only root users can create branches, for more info contact the support',
        HttpCode.Client.FORBIDDEN,
      );
    }

    try {
      const provider = ProviderManagerService.getInstance().from(providerId as CustodyProvider);
      const branchFeature = provider.feature(CustodyFeature.BRANCH);

      const branch = await branchFeature.register({ number, address });
      return res.success(branch);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      const status = (error.response && error.response.status) || HttpCode.Server.INTERNAL_SERVER_ERROR;

      throw new HttpError('The branch registration failed', status, {
        message: message || 'Unknown Error',
      });
    }
  }

  @Post('/:number', [OAuth.token(Scopes.branches.WRITE), Params.isValidProvider('provider', true)])
  public static async update(req: bacenRequest, res: BaseResponse) {
    const { provider: providerId, number } = req.params;
    const { id, address }: { id: string; address: AddressSchema } = req.body;

    const currentUser: User = req.user;

    // Check user can create product
    if (currentUser.role !== UserRole.ADMIN) {
      throw new HttpError(
        'Only root users can create products, for more info contact the support',
        HttpCode.Client.FORBIDDEN,
      );
    }

    try {
      const provider = ProviderManagerService.getInstance().from(providerId as CustodyProvider);
      const branchFeature = provider.feature(CustodyFeature.BRANCH);

      const branch = await branchFeature.update({ id, number, address });
      return res.success(branch);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      const status = (error.response && error.response.status) || HttpCode.Server.INTERNAL_SERVER_ERROR;

      throw new HttpError('The branch update failed', status, {
        message: message || 'Unknown Error',
      });
    }
  }
}
