import { BaseResponse, Controller, Get, HttpError, HttpCode } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { UnleashFlags } from '@bacen/base-sdk';
import Scopes from '../../config/oauth/scopes';
import { OAuth, Permissions } from '../filters';
import {
  bacenRequest,
  ViewConsumerByStates,
  ViewConsumerWithStates,
  ViewTransactionByStates,
  ViewWalletByStates,
} from '../models';
import { MetricsService } from '../services';

@Controller('/metrics')
export default class ConsumerMetricsController {
  @Get('/')
  public static async getMetrics(req: bacenRequest, res: BaseResponse) {
    if (!UnleashUtil.isEnabled(UnleashFlags.ENABLE_METRICS)) {
      throw new HttpError('Metrics are not available', HttpCode.Client.PRECONDITION_FAILED);
    }

    // Send as prometheus mime-type by default
    res.set('Content-Type', MetricsService.getInstance().getContentType());
    res.send(await MetricsService.getInstance().text());
  }

  @Get('/consumers', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadAllConsumers])
  public static async getConsumersSummary(req: bacenRequest, res: BaseResponse) {
    const [viewConsumerByStates, pendingDeletion] = await Promise.all([
      ViewConsumerByStates.find(),
      ViewConsumerWithStates.findPendingConsumersCount(),
    ]);

    return res.success({
      actions: { name: 'pending_deletion', count: pendingDeletion },
      status: viewConsumerByStates,
    });
  }

  @Get('/transactions', [OAuth.token([Scopes.transactions.READ]), Permissions.canUserReadAllWallets])
  public static async getTransactionsSummary(req: bacenRequest, res: BaseResponse) {
    const viewTransactionByStates = await ViewTransactionByStates.find();
    res.success({ status: viewTransactionByStates });
  }

  @Get('/wallets', [OAuth.token([Scopes.transactions.READ]), Permissions.canUserReadAllWallets])
  public static async getWalletsSummary(req: bacenRequest, res: BaseResponse) {
    const viewWalletByStates = await ViewWalletByStates.find();
    res.success({ status: viewWalletByStates });
  }
}
