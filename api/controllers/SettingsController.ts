import { BaseResponse, Controller, Delete, Get, Post } from 'ts-framework';
import { EntityNotFoundError } from '../errors';
import { OAuth, Query } from '../filters';
import { bacenRequest, Settings, SettingsFields } from '../models';
import Scopes from '../../config/oauth/scopes';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/settings')
export default class SettingsController {
  @Get('/', [OAuth.token([Scopes.domains.READ]), Query.pagination])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Settings.safePaginatedFind({ pagination });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:field', [OAuth.token([Scopes.domains.READ]), Query.pagination])
  public static async findOneByField(req: bacenRequest, res: BaseResponse) {
    const { field } = req.params as { field: SettingsFields };
    const found = await Settings.safeFindOne({ where: { field } });

    if (found) {
      res.success(found);
    }

    throw new EntityNotFoundError('Settings');
  }

  @Post('/:field', [OAuth.token([Scopes.domains.WRITE]), Query.pagination])
  public static async setOneByField(req: bacenRequest, res: BaseResponse) {
    const { field } = req.params as { field: SettingsFields };
    const found = await Settings.safeFindOne({ where: { field } });

    if (!found) {
      throw new EntityNotFoundError('Settings');
    }

    found.value = req.param('value');
    const updated = await Settings.updateAndFind(found.id, found);
    res.success(updated);
  }

  @Delete('/:field', [OAuth.token([Scopes.domains.WRITE])])
  public static async deleteById(req: bacenRequest, res: BaseResponse) {
    const { field } = req.params as { field: SettingsFields };
    const { force }: { force?: string } = req.query;

    const item = await Settings.safeFindOne({ where: { field } });

    if (!item) {
      throw new EntityNotFoundError('Settings');
    }

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      await Settings.createQueryBuilder()
        .delete()
        .where({ field });
    } else {
      // TODO: Create soft delete based on WHERE clause
      await Settings.update({ field } as Settings, { deletedAt: new Date() });
    }
    res.success({});
  }
}
