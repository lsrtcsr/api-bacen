import { BaseResponse, Controller, Get } from 'ts-framework';
import { getConnection } from 'typeorm';
import { OAuth, Permissions } from '../filters';
import { bacenRequest } from '../models';
import { PaymentLog } from '../timescale';
import Scopes from '../../config/oauth/scopes';

@Controller('/domains/:domainId/metrics', [OAuth.token([Scopes.domains.METRICS]), Permissions.canUserReadDomainMetrics])
export default class DomainMetricsController {
  /**
   * Gets the cumulative sum of payment amounts grouped by time
   *
   * Query params (All optional):
   * start        ISODate           Start of date range for metrics
   * end          ISODate           End of date range for metrics
   * source       User ID           Get metrics for a single source
   * destination  User ID           Get metrics for a single destination
   * asset        Asset ID          Get metrics for a single asset
   * type         PaymentLogType    Get metrics for a single payment type
   */
  @Get('/payments/amount')
  static async getPaymentAmountMetrics(req: bacenRequest, res: BaseResponse) {
    const { domainId } = req.params;
    const { start, end, source, destination, asset, type } = req.query;

    let query = getConnection('timescale')
      .getRepository(PaymentLog)
      .createQueryBuilder('payment')
      .select('time, SUM(SUM(amount::decimal)) OVER(ORDER BY time) AS total_amount')
      .groupBy('time')
      .where({ domainId });

    if (start) {
      query = query.andWhere('time > :start', { start });
    }

    if (end) {
      query = query.andWhere('time < :start', { start });
    }

    if (source) {
      query = query.andWhere('source = :source', { source });
    }

    if (destination) {
      query = query.andWhere('destination = :destination', { destination });
    }

    if (asset) {
      query = query.andWhere('asset = :asset', { asset });
    }

    if (type) {
      query = query.andWhere('type = :type', { type });
    }

    const result = await query.getRawMany();

    res.success(result);
  }

  /**
   * Gets the cumulative count of payments grouped by time
   *
   * Query params (All optional):
   * start        ISODate           Start of date range for metrics
   * end          ISODate           End of date range for metrics
   * source       User ID           Get metrics for a single source
   * destination  User ID           Get metrics for a single destination
   * asset        Asset ID          Get metrics for a single asset
   * type         PaymentLogType    Get metrics for a single payment type
   */
  @Get('/payments/count')
  static async getPaymentCountMetrics(req: bacenRequest, res: BaseResponse) {
    const { domainId } = req.params;
    const { start, end, source, destination, asset, type } = req.query;

    let query = getConnection('timescale')
      .getRepository(PaymentLog)
      .createQueryBuilder('payment')
      .select('time AS time, SUM(COUNT(amount)) OVER(ORDER BY time) AS count')
      .groupBy('time')
      .where({ domainId });

    if (start) {
      query = query.andWhere('time > :start', { start });
    }

    if (end) {
      query = query.andWhere('time < :start', { start });
    }

    if (source) {
      query = query.andWhere('source = :source', { source });
    }

    if (destination) {
      query = query.andWhere('destination = :destination', { destination });
    }

    if (asset) {
      query = query.andWhere('asset = :asset', { asset });
    }

    if (type) {
      query = query.andWhere('type = :type', { type });
    }

    const result = await query.getRawMany();

    res.success(result);
  }

  /**
   * Gets the cumulative count of active users grouped by time
   *
   * Query params (All optional):
   * start      ISODate           Start of date range for metrics
   * end        ISODate           End of date range for metrics
   * source     User ID           Get metrics for a single source
   * asset      Asset ID          Get metrics for a single asset
   * type       PaymentLogType    Get metrics for a single payment type
   */
  @Get('/users/count')
  static async getUserCountMetrics(req: bacenRequest, res: BaseResponse) {
    const { domainId } = req.params;
    const { start, end, source, asset, type } = req.query;

    // FIX: Find a way to make this work on TypeORM.
    // We shouldn't ever need to use raw SQL. This is a BAD THING. Please do not reproduce.
    let query = `SELECT time, SUM(COUNT(source)) OVER(ORDER BY time) AS count 
      FROM (SELECT DISTINCT ON (source) time, source, "domainId" FROM payment) AS payment
      WHERE "domainId" = $1 `;
    const parameters = [domainId];

    if (start) {
      query += ` AND time > ${parameters.length}`;
      parameters.push(start);
    }

    if (end) {
      query += ` AND time < ${parameters.length}`;
      parameters.push(end);
    }

    if (source) {
      query += ` AND source = ${parameters.length}`;
      parameters.push(source);
    }

    if (asset) {
      query += ` AND asset = ${parameters.length}`;
      parameters.push(asset);
    }

    if (type) {
      query += ` AND type = ${parameters.length}`;
      parameters.push(type);
    }

    query += ` GROUP BY time`;

    const result = await getConnection('timescale')
      .getRepository(PaymentLog)
      .query(query, parameters);

    res.success(result);
  }

  /**
   * Gets the cumulative balance grouped by time
   *
   * Query params (All optional):
   * start      ISODate           Start of date range for metrics
   * end        ISODate           End of date range for metrics
   * source     User ID           Get metrics for a single source
   * asset      Asset ID          Get metrics for a single asset
   * type       PaymentLogType    Get metrics for a single payment type
   */
  @Get('/balance')
  static async getBalanceMetrics(req: bacenRequest, res: BaseResponse) {
    const { domainId } = req.params;
    const { start, end, source, asset, type } = req.query;

    // Get the running sum of emit and destroy payments (Emit as positive, Destroy as negative)
    let query = getConnection('timescale')
      .getRepository(PaymentLog)
      .createQueryBuilder('payment')
      .select(
        "time, SUM(SUM(CASE WHEN type = 'emit' THEN amount ELSE - amount END)) OVER(ORDER BY time) AS total_amount",
      )
      .groupBy('time')
      .where({ domainId })
      .andWhere("type = 'emit' OR type = 'destroy'");

    if (start) {
      query = query.andWhere('time > :start', { start });
    }

    if (end) {
      query = query.andWhere('time < :start', { start });
    }

    if (source) {
      query = query.andWhere('source = :source', { source });
    }

    if (asset) {
      query = query.andWhere('asset = :asset', { asset });
    }

    if (type) {
      query = query.andWhere('type = :type', { type });
    }

    const result = await query.getRawMany();

    res.success(result);
  }
}
