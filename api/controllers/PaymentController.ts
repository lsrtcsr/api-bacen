import {
  CustodyFeature,
  CustodyProvider,
  PaymentRequestSchema,
  PaymentType,
  PaymentStatus,
  UserRole,
  WalletStatus,
  ConsumerStatus,
  TransactionStatus,
  UnleashFlags,
  ServiceType,
} from '@bacen/base-sdk';
import { validate } from 'class-validator';
import { BaseResponse, Controller, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { In, getManager } from 'typeorm';
import { UnleashUtil } from '@bacen/shared-sdk';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError, ForbiddenRequestError, InvalidParameterError, InvalidRequestError } from '../errors';
import { Amount, Exists, Limits, OAuth, Params, Payments, Permissions, Request, Status } from '../filters';
import {
  Asset,
  bacenRequest,
  Domain,
  Payment,
  Transaction,
  User,
  Wallet,
  TRANSACTION_REVERSIBLE_STATES,
} from '../models';
import { OfflinePaymentRequest, PaymentRequest } from '../schemas';
import {
  ProviderManagerService,
  TransactionPipelinePublisher,
  EventHandlingGateway,
  BillingService,
  PgIdempotenceStore,
} from '../services';
import { getServiceTypeFromPaymentType, isUUID, ProviderUtil } from '../utils';
import { ServiceCharged, UseLock, ReleaseBalance, Idempotent } from '../decorators';
import { TransactionStateMachine } from '../fsm';
import * as Serializers from '../serializers';
import { checkIfAssetIsRegisteredOnWallet } from '../filters/assets/IsAssetRegistered';

// TODO: Move to config
export const DEFAULT_BOLETO_EXPIRATION = 3 * 24 * 60 * 60 * 1000; // 3 days

@Controller('/payments', [OAuth.token([Scopes.payments.READ])])
export default class PaymentController {
  @Get('/:id', [OAuth.token([Scopes.payments.READ]), Exists.Payment('id'), Permissions.canUserReadOnePayment])
  public static async getPayment(req: bacenRequest, res: BaseResponse) {
    const payment = await Payment.findOne(req.param('id'), {
      relations: ['transaction'],
    });

    if (!payment) {
      throw new EntityNotFoundError('Payment');
    }

    return res.success(payment.toJSON());
  }

  @Idempotent({ store: PgIdempotenceStore })
  @UseLock('source')
  @ServiceCharged(ServiceType.TRANSFER)
  @Post('/', [
    OAuth.token([Scopes.payments.WRITE]),
    Request.signing(),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.body.source },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Permissions.canUserWritePayment('source'),
    Status.isConsumerInStatus('source', [ConsumerStatus.READY], 'Wallet'),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.body.source,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Amount.isValidAmount,
    Payments.isSourceOneOfRecipients(),
    Limits.transactionAmount,
    Params.isInArray({
      paramName: 'type',
      requiredArray: [PaymentType.TRANSFER, PaymentType.BOLETO_IN],
      allowEmpty: true,
    }),
  ])
  public static async createPayment(req: bacenRequest, res: BaseResponse) {
    const currentUser: User = req.user;
    let { source: sourceId, recipients, extra, type }: PaymentRequestSchema = req.body;
    type = type ?? PaymentType.TRANSFER;
    // tslint:disable-next-line
    const errors = await validate(new PaymentRequest({ source: sourceId, recipients }));
    if (errors.length > 0) {
      throw new InvalidParameterError('Invalid Payment', errors);
    }

    // Gather all payment assets
    const assets: { [key: string]: Asset } = {};

    // Improve performance of queries inside this array
    for (const r of recipients) {
      const key = r.asset;

      if (!key || key === 'root') {
        assets[key] = await Asset.getRootAsset();
      } else if (isUUID(key)) {
        assets[key] = await Asset.findOne(key, { relations: ['issuer'] });
      } else {
        assets[key] = await Asset.createQueryBuilder('asset')
          .leftJoinAndSelect('asset.issuer', 'issuer')
          .where('asset.code = :code', { code: key.toUpperCase() })
          .getOne();
      }

      // Ensure requested asset exists
      if (!assets[key]) {
        throw new EntityNotFoundError('Asset', { asset: key });
      }
    }

    // Get source wallet from database
    const sourceWallet = await Wallet.safeFindOne({
      where: { id: sourceId },
      relations: ['user', 'user.domain'],
    });

    if (!sourceWallet) {
      throw new EntityNotFoundError('Source wallet', { sourceId });
    }

    if (currentUser.role === UserRole.CONSUMER && currentUser.id !== sourceWallet.user.id) {
      throw new ForbiddenRequestError('Source wallet not owned by requesting user');
    }

    // Get destination wallets from database
    const destinationWallets = await Wallet.safeFind({
      where: { id: In(recipients.map((each) => each.destination)) },
      relations: ['user', 'user.domain', 'assetRegistrations', 'assetRegistrations.states', 'assetRegistrations.asset'],
    });

    // Ensure the payment runs in a single domain
    if (currentUser.role !== UserRole.ADMIN && UnleashUtil.isEnabled(UnleashFlags.DISABLE_MULTI_DOMAIN_PAYMENTS)) {
      const walletsFromOtherDomains = destinationWallets.filter(
        (wallet) => wallet.user.domain.id !== currentUser.domain.id,
      );
      if (walletsFromOtherDomains.length !== 0) {
        const walletIds = walletsFromOtherDomains.map((wallet) => wallet.id);
        throw new InvalidRequestError(`All recipients must belong to source domain`, { invalid: walletIds });
      }
    }

    // Prepare destination map for payments with populated wallets
    const destinationMap: any[] = recipients.map((recipient, idx) => {
      const wallet = destinationWallets.find((wallet) => wallet.id === recipient.destination);

      if (!wallet) {
        throw new InvalidParameterError(`recipients[${idx}].destination`);
      }

      const asset = assets[recipient.asset];
      checkIfAssetIsRegisteredOnWallet(asset, wallet);

      return {
        asset,
        wallet,
        amount: recipient.amount,
      };
    });

    try {
      // Prepare transaction for pipeline
      const transaction = await getManager().transaction(async (manager) => {
        let trx = await Transaction.prepare(
          {
            manager,
            source: sourceWallet,
            recipients: destinationMap,
            type,
            createdBy: req.accessToken,
          },
          true,
        );

        if (req.serviceFee) {
          trx = await BillingService.getInstance().chargeServiceFee(req.serviceFee, trx, manager);
        }

        return trx;
      });

      await EventHandlingGateway.getInstance().process(getServiceTypeFromPaymentType(type), transaction);

      // Send transaction to execution pipeline
      await TransactionPipelinePublisher.getInstance().send(transaction);

      // Return updated payment results
      res.success(Serializers.transactionSerializer(transaction));
    } catch (err) {
      throw err;
    }
  }

  @Post('/:id/settlement', [
    OAuth.token([Scopes.transactions.CONFIRM]),
    Exists.Payment('id'),
    Request.signing(),
    Permissions.canUserConfirmPayment('source'),
  ])
  public static async settle(req: bacenRequest, res: BaseResponse) {
    const { id: paymentId } = req.params;
    const { extra }: { source: string; extra?: any } = req.body;

    const transaction = await Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('transaction.states', 'states')
      .where('transaction.id = (SELECT "transactionId" FROM payments WHERE id = :paymentId)', { paymentId })
      .getOne();

    const payment = transaction.payments.find((payment) => payment.id === paymentId);

    if (payment.status !== PaymentStatus.AUTHORIZED) {
      throw new InvalidRequestError('Payment is not in the required state for the requested operation', {
        entityId: payment.id,
        requiredStatus: PaymentStatus.AUTHORIZED,
      });
    }

    await getManager().transaction(async (manager) => {
      await manager.update(Payment, payment.id, { status: PaymentStatus.SETTLED });
      payment.status = PaymentStatus.SETTLED;

      if (extra && Object.keys(extra).length > 0) {
        manager.update(Transaction, transaction.id, {
          additionalData: { ...transaction.additionalData, ...extra },
        });
        transaction.additionalData = { ...transaction.additionalData, ...extra };
      }
    });

    const transactionFsm = new TransactionStateMachine(transaction);
    const didTransition = await transactionFsm.goTo(TransactionStatus.ACCEPTED);

    // If transition fails it means that not all payments are settled and the transaction is not ready to me sent to
    // the pipeline yet.
    if (didTransition) {
      await TransactionPipelinePublisher.getInstance().send(transaction);
    }

    res.success(payment);
  }

  @Post('/:id/cancel', [
    OAuth.token([Scopes.transactions.CANCEL]),
    Exists.Payment('id'),
    Request.signing(),
    Permissions.canUserCancelPayment('id'),
  ])
  public static async cancel(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { extra }: { extra?: any } = req.body;

    const payment = await Payment.safeFindOne({
      where: { id },
      relations: [
        'asset',
        'transaction',
        'transaction.source',
        'transaction.source.user',
        'transaction.source.user.domain',
      ],
    });

    try {
      const provider = ProviderManagerService.getInstance().from(payment.asset.provider);
      const withdrawalFeature = provider.feature(CustodyFeature.WITHDRAW);
      const response = await withdrawalFeature.cancel(payment.transaction.source.id, payment.id, extra);

      res.success(response);
    } catch (error) {
      if (error instanceof HttpError) throw error;

      const message = error.message || (error.data && error.data.message);
      throw new HttpError('Withdrawal cancelation failed', HttpCode.Server.INTERNAL_SERVER_ERROR, {
        message: message || 'Unknown Error',
      });
    }
  }

  @Post('/:id/reversal', [
    OAuth.token([Scopes.transactions.CANCEL]),
    Exists.Payment('id'),
    Request.signing(),
    Permissions.canUserCancelPayment('id'),
  ])
  public static async reverse(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { source, extra }: { source: string; extra?: any } = req.body;

    let payment = await Payment.safeFindOne({
      where: { id },
      relations: ['transaction', 'transaction.states'],
    });

    if (!TRANSACTION_REVERSIBLE_STATES.includes(payment.transaction.status)) {
      throw new InvalidRequestError('Transaction is not in the required state for the requested operation', {
        entityId: payment.transaction.id,
        requiredStatus: TRANSACTION_REVERSIBLE_STATES,
      });
    }

    let transaction = await Transaction.safeFindOne({
      where: { id: payment.transaction.id },
      relations: ['payments', 'states'],
    });

    transaction = await getManager().transaction(async (manager) => {
      payment = await Payment.updateAndFind(
        payment.id,
        { status: PaymentStatus.FAILED },
        { manager, findOptions: { relations: ['transaction', 'transaction.states'] } },
      );

      const fees = transaction.payments
        .filter((payment) => payment.type === PaymentType.SERVICE_FEE && payment.status === PaymentStatus.AUTHORIZED)
        .map((payment) => payment.id);

      if (fees && fees.length) await manager.update(Payment, fees, { status: PaymentStatus.FAILED });

      if (extra && Object.keys(extra).length > 0) {
        return Transaction.updateAndFind(
          payment.transaction.id,
          { additionalData: { ...payment.transaction.additionalData, ...extra } },
          {
            manager,
            findOptions: {
              relations: [
                'source',
                'source.user',
                'source.user.domain',
                'payments',
                'payments.destination',
                'payments.asset',
                'states',
              ],
            },
          },
        );
      }

      return manager.findOne(Transaction, {
        where: { id: payment.transaction.id },
        relations: [
          'source',
          'source.user',
          'source.user.domain',
          'payments',
          'payments.destination',
          'payments.asset',
          'states',
        ],
      });
    });

    if (transaction.payments.every((payment) => payment.status === PaymentStatus.FAILED)) {
      const fsm = new TransactionStateMachine(transaction);
      await fsm.goTo(TransactionStatus.FAILED, extra);
    } else {
      await TransactionPipelinePublisher.getInstance().send(transaction);
    }

    res.success(payment);
  }

  @ReleaseBalance()
  @Post('/authorization', [
    OAuth.token([Scopes.assets.DESTROY]),
    Status.isWalletInStatus({ walletIdFn: (req) => req.body.source, requiredStatus: WalletStatus.READY }),
    Permissions.canUserWritePayment('source'),
    Payments.hasUserEnoughBalance,
    Request.signing(),
  ])
  static async authorize(req: bacenRequest, res: BaseResponse) {
    const {
      source,
      amount,
      assetCode,
      type,
      additionalData,
    }: { source: string; amount: string; type?: PaymentType; assetCode?: string; additionalData?: any } = req.body;

    // Emit to source or use mediator as default
    const domain = req.user.domain ? req.user.domain : await Domain.getDefaultDomain();
    const sourceWallet = await (source
      ? Wallet.findOne({ where: { id: source }, relations: ['user', 'user.domain', 'product'] })
      : Wallet.getMediatorWallet(domain));

    if (!sourceWallet) {
      throw new EntityNotFoundError('Source wallet');
    }

    let asset: Asset;

    if (assetCode === 'root') {
      asset = await Asset.getRootAsset();
    } else {
      asset = await Asset.createQueryBuilder('asset')
        .leftJoinAndSelect('asset.issuer', 'issuer')
        .where('asset.code = :code', { code: assetCode.toUpperCase() })
        .getOne();
    }

    // Check asset exists
    if (!asset) {
      throw new EntityNotFoundError('Asset');
    }

    // Check user can emit or destroy this asset
    if (!asset.emittable && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenRequestError('Only root users can emit root assets, for more info contact the support');
    }

    const transaction = await Transaction.prepare(
      {
        additionalData,
        type: type || PaymentType.WITHDRAWAL,
        source: sourceWallet,
        recipients: [{ amount, asset, wallet: asset.issuer }],
        createdBy: req.accessToken,
      },
      false,
    );

    await EventHandlingGateway.getInstance().process(ServiceType.TRANSFER, transaction);

    // Return updated payment results
    res.success(Serializers.transactionSerializer(transaction));
  }

  @Post('/offline/failed', [
    OAuth.token([Scopes.assets.DESTROY]),
    Request.signing(),
    Status.isWalletInStatus({ walletIdFn: (req) => req.body.source, requiredStatus: WalletStatus.READY }),
  ])
  static async recordFailedPayment(req: bacenRequest, res: BaseResponse) {
    const {
      source,
      amount,
      assetCode,
      type,
      additionalData,
    }: { source: string; amount: string; type: PaymentType; assetCode?: string; additionalData?: any } = req.body;

    // Emit to source or use mediator as default
    const domain = req.user.domain ? req.user.domain : await Domain.getDefaultDomain();
    const sourceWallet = await (source
      ? Wallet.findOne({ where: { id: source }, relations: ['user', 'user.domain'] })
      : Wallet.getMediatorWallet(domain));

    if (!sourceWallet) {
      throw new EntityNotFoundError('Source wallet');
    }

    let asset: Asset;

    if (assetCode === 'root') {
      asset = await Asset.getRootAsset();
    } else {
      asset = await Asset.createQueryBuilder('asset')
        .leftJoinAndSelect('asset.issuer', 'issuer')
        .where('asset.code = :code', { code: assetCode.toUpperCase() })
        .getOne();
    }

    // Check asset exists
    if (!asset) {
      throw new EntityNotFoundError('Asset');
    }

    // Check user can emit or destroy this asset
    if (!asset.emittable && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenRequestError('Only root users can emit root assets, for more info contact the support');
    }

    const transaction = await Transaction.recordFailedTransaction({
      type,
      additionalData,
      source: sourceWallet,
      recipients: [{ amount, asset, wallet: asset.issuer }],
      createdBy: req.accessToken,
    });

    res.success(transaction);
  }

  /**
   * Post /payments/offline/:providerId
   *
   * @description Receives the data of a payment (withdrawal, deposit or P2P)
   * originated outside of the bacen Platform whose postback was not sent
   * by the provider or was not processed by some internal error
   */
  @Post('/offline/:providerId', [
    OAuth.token([Scopes.assets.EMIT, Scopes.assets.DESTROY]),
    Permissions.canUserDistributeAsset,
    Request.signing(),
  ])
  static async processOfflinePayment(req: bacenRequest, res: BaseResponse) {
    const { providerId } = req.params;
    const payload: OfflinePaymentRequest = req.body;
    const currentUser: User = req.user;

    if (!payload.reason) {
      throw new HttpError(
        'You must provide the reason for request manually the offline payment processing',
        HttpCode.Client.BAD_REQUEST,
      );
    }

    let payment;
    try {
      const custodyProviderId = CustodyProvider[providerId] as CustodyProvider;
      const provider = ProviderManagerService.getInstance().from(custodyProviderId);
      const postbackFeature = provider.feature(CustodyFeature.POSTBACK);
      await ProviderUtil.throwIfFeatureNotAvailable(postbackFeature);
      payment = await postbackFeature.onPostback({
        sentManually: true,
        responsible: currentUser.id,
        ...payload,
      });
    } catch (error) {
      if (error instanceof HttpError) throw error;

      const message = error.message || (error.data && error.data.message);
      throw new HttpError("Balance adjustment in the user's wallet failed", HttpCode.Server.INTERNAL_SERVER_ERROR, {
        message: message || 'Unknown Error',
      });
    }

    res.success(payment);
  }
}
