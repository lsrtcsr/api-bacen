import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, Controller, Delete, Get, Post } from 'ts-framework';
import { getManager } from 'typeorm';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError } from '../errors';
import { Exists, OAuth, Params, Permissions, Query } from '../filters';
import { bacenRequest, Domain, DomainSettings, User, DomainSettingsLog, settingsToSave } from '../models';
import { DomainSettingRequestSchema } from '../schemas';
import { isUUID, getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/domains', [])
export default class DomainController {
  @Get('/', [OAuth.token(Scopes.domains.READ), Query.pagination, Permissions.canUserReadAllDomains])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Domain.safePaginatedFind({ pagination });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id', [OAuth.token([Scopes.domains.READ]), OAuth.url, Permissions.canUserReadOneDomain])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const isId = id && isUUID(id);
    let found: Domain;

    if (isId) {
      found = await Domain.safeFindOne({ where: { id } });
    } else if (id === 'root') {
      found = await Domain.getRootDomain();
    } else if (id === 'default') {
      found = await Domain.getDefaultDomain();
    }

    if (!found) {
      throw new EntityNotFoundError('Domain');
    }

    res.success(found.toJSON());
  }

  @Get('/:id/mediators', [
    OAuth.token([Scopes.users.READ]),
    Query.isValidId(),
    Permissions.canUserReadDomainUsers,
    Exists.Domain,
  ])
  public static async findOneMediators(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.safePaginatedFind({
      pagination,
      where: {
        domain: { id },
        role: UserRole.MEDIATOR,
      },
      relations: ['domain'],
    });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id/consumers', [
    OAuth.token([Scopes.users.READ]),
    Query.isValidId(),
    Permissions.canUserReadDomainUsers,
    Exists.Domain,
  ])
  public static async findOneConsumers(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.safePaginatedFind({
      pagination,
      where: {
        domain: { id },
        role: UserRole.CONSUMER,
      },
      relations: ['domain'],
    });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Post('/', [OAuth.token([Scopes.domains.WRITE]), Permissions.canUserWriteDomain, Params.isValidDomain()])
  public static async insert(req: bacenRequest, res: BaseResponse) {
    const { ...payload }: Domain = req.body;

    if (req.user.role !== UserRole.ADMIN) {
      delete payload.systemPostbackUrls;
    }

    const result = await getManager().transaction(async transaction => {
      const domainInsertResults = await transaction.insert(
        Domain,
        Domain.create({
          ...payload,
          settings: new DomainSettings(),
        }),
      );

      return transaction.findOne(Domain, {
        where: { id: domainInsertResults.identifiers[0].id },
      });
    });

    res.success(result.toJSON());
  }

  @Post('/:id/settings', [
    OAuth.token([Scopes.domains.WRITE]),
    Query.isValidId(),
    Permissions.canUserWriteDomain,
    Exists.Domain,
  ])
  public static async updateSettings(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const newSettings: DomainSettingRequestSchema[] = req.body;

    const domain = await Domain.safeFindOne({ where: { id } });
    const currentSettings = new DomainSettings(domain.settings);

    for await (const newSetting of newSettings) {
      if (settingsToSave.includes(newSetting.type)) {
        const setting = DomainSettingsLog.create({
          setting: newSetting.type,
          userId: req.user.id,
          oldValue: currentSettings.locks[newSetting.type],
          newValue: newSetting.value,
        });
        await DomainSettingsLog.save(setting);
      }

      currentSettings.locks.update(newSetting.type, newSetting.value);
    }

    await Domain.createQueryBuilder('domain')
      .update(Domain)
      .set({ settings: currentSettings })
      .where('id = :id', { id })
      .execute();

    res.success(currentSettings);
  }

  @Post('/:id', [
    OAuth.token([Scopes.domains.WRITE]),
    Query.isValidId(),
    Permissions.canUserWriteDomain,
    Exists.Domain,
    Params.isValidDomain(true),
  ])
  public static async updateById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const payload: Domain = req.body;

    if (req.user.role !== UserRole.ADMIN) {
      delete payload.systemPostbackUrls;
    }

    const domainToUpdate = await Domain.safeFindOne({ where: { id } });
    payload.settings = {
      ispb: payload?.settings?.ispb,
      locks: domainToUpdate.settings?.locks,
      fees: domainToUpdate.settings?.fees,
    };

    const domain = await Domain.updateAndFind(id, payload);

    res.success(domain.toJSON());
  }

  @Delete('/:id', [
    OAuth.token([Scopes.domains.DELETE]),
    Query.isValidId(),
    Permissions.canUserDeleteDomain,
    Exists.Domain,
  ])
  public static async deleteById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { force }: { force?: string } = req.query;

    let deleted;
    const item = await Domain.safeFindOne({ where: { id } });

    if (!item) {
      throw new EntityNotFoundError('Domain');
    }

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Domain.delete(id).then(_ => true);
    } else {
      deleted = await Domain.softDelete(id).then(_ => true);
    }

    res.success(deleted);
  }
}
