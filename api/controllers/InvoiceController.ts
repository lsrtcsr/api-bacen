import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { FindConditions } from 'typeorm';
import { InvalidRequestError } from 'oauth2-server';
import { UserRole } from '@bacen/base-sdk';
import { OAuth, Exists, Permissions } from '../filters';
import { bacenRequest, Invoice, User } from '../models';
import { BillingService } from '../services';
import { InternalServerError, EntityNotFoundError } from '../errors';
import Scopes from '../../config/oauth/scopes';

const DEFAULT_LIMIT = 25;

@Controller('/invoices')
export default class InvoiceController {
  @Get('/', [OAuth.token([Scopes.invoices.READ]), Permissions.canUserReadAllInvoices])
  public static async find(req: bacenRequest, res: BaseResponse) {
    const { userId, skip = 0, limit = DEFAULT_LIMIT }: { userId: string; skip: number; limit: number } = req.query;

    const currentUser: User = req.user;

    let where: FindConditions<Invoice> = {};
    if (userId) {
      where = { contractor: { id: userId }, current: true };
    } else if ([UserRole.ADMIN, UserRole.AUDIT].includes(currentUser.role)) {
      where = { current: true };
    } else if (currentUser.role === UserRole.MEDIATOR) {
      where = { contractor: { domain: { id: currentUser.domain.id } }, current: true };
    } else if (currentUser.role === UserRole.CONSUMER) {
      where = { contractor: { id: currentUser.id }, current: true };
    }

    let invoices: Invoice[];
    let count: number;
    try {
      const queries: [Promise<Invoice[]>, Promise<number>] = [
        Invoice.safeFind({
          where,
          skip,
          take: limit,
          relations: [
            'contractor',
            'contractor.domain',
            'periods',
            'periods.entries',
            'periods.entries.type',
            'periods.entries.type.service',
          ],
        }),
        Invoice.safeCount({ where }),
      ];

      [invoices, count] = await Promise.all(queries);
    } catch (error) {
      throw new InternalServerError(`Error trying to retrieve invoices`);
    }

    res.set('X-Data-Length', count.toString());
    res.set('X-Data-Skip', req.query.skip || '0');
    res.set('X-Data-Limit', req.query.limit || DEFAULT_LIMIT);

    res.success(invoices);
  }

  @Get('/:id', [OAuth.token([Scopes.invoices.READ]), Exists.Invoice, Permissions.canUserReadOneInvoice])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;

    const invoice = await Invoice.safeFindOne({
      where: { id },
      relations: ['contractor', 'periods', 'periods.entries', 'periods.entries.type', 'periods.entries.type.service'],
    });

    if (!invoice) {
      throw new EntityNotFoundError('Invoice');
    }

    res.success(invoice);
  }

  @Post('/:id/close', [OAuth.token([Scopes.services.WRITE]), Exists.Invoice, Permissions.canUserWriteInvoice])
  public static async close(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { force }: { force: boolean } = req.body;

    const invoice = await Invoice.safeFindOne({ where: { id, current: true }, relations: ['contractor'] });

    if (!invoice) {
      throw new InvalidRequestError('The requested invoice is not the current one!');
    }

    await BillingService.getInstance().closeCurrentInvoice(invoice.contractor, force);

    res.success();
  }
}
