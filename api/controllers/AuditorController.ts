import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, Controller, HttpCode, HttpError, Post } from 'ts-framework';
import Scopes from '../../config/oauth/scopes';
import { InvalidRequestError } from '../errors/InvalidRequestError';
import { OAuth, Params, Permissions } from '../filters';
import { bacenRequest, Domain, User } from '../models';

@Controller('/auditors', [])
export default class AuditorController {
  @Post('/', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteUser, Params.isValidUser()])
  public static async createAuditor(req: bacenRequest, res: BaseResponse) {
    const { password, domain = req.user.domain, ...payload }: User & { password?: string } = req.body;

    // ignores the role parameter, if sent, and fix its value to AUDIT
    payload.role = UserRole.AUDIT;

    if (!req.user || !req.user.role || req.user.role !== UserRole.ADMIN) {
      throw new HttpError('Only admin users can create auditors', HttpCode.Client.FORBIDDEN);
    }

    const emailCount = await User.safeCount({ where: { email: payload.email } });
    if (emailCount > 0) {
      throw new HttpError('Email is already in use', HttpCode.Client.BAD_REQUEST);
    }

    // Determining domain
    const found = await Domain.safeFindOne({ where: { id: (domain.id || domain) as string } });
    if (!found) {
      throw new InvalidRequestError('Domain', { domain });
    }

    // Creates the user instance
    let user = User.create({ ...payload, domain });

    // Prepare user password, if any
    if (password) {
      await user.setPassword(password);
    }

    // Insert user in the database
    user = await User.insertAndFind(user);
    res.success(user);
  }
}
