import {
  AccountType,
  Arrangement,
  AssetRegistrationStatus,
  BankingSchema,
  BankingType,
  ConsumerStatus,
  CustodyFeature,
  CustodyProvider,
  PaymentStatus,
  PaymentType,
  TransactionType,
  TransitoryAccountType,
  UnleashFlags,
  UserRole,
  UserStatus,
  WalletStatus,
  WalletTransientStatus,
  ServiceType,
} from '@bacen/base-sdk';
import { DictWebService, EntrySchema } from '@bacen/dict-sdk';
import { Account, ClaimType, EntryReason, KeyType } from '@bacen/dict-service';
import { HooksService } from '@bacen/hooks-sdk';
import { deepCopy, UnleashContext, UnleashUtil } from '@bacen/shared-sdk';
import { StellarService } from '@bacen/stellar-service';
import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post, Put } from 'ts-framework';
import { Brackets, getManager } from 'typeorm';
import Config from '../../config';
import { logger } from '../../config/logger.config';
import Scopes from '../../config/oauth/scopes';
import { Idempotent, ReleaseBalance, ServiceCharged, UseLock } from '../decorators';
import {
  EntityNotFoundError,
  ForbiddenRequestError,
  InvalidParameterError,
  InvalidRequestError,
  UniqueParameterViolationError,
  wrapError,
} from '../errors';
import { Assets, Exists, Limits, OAuth, Payments, Permissions, Query, Request, Status, Amount } from '../filters';
import { defaultAmountsFn } from '../filters/payments';
import { ConsumerStateMachine, UserStateMachine } from '../fsm';
import {
  Asset,
  AssetRegistration,
  Banking,
  bacenRequest,
  Domain,
  OAuthSecretToken,
  Payment,
  Phone,
  Transaction,
  TransactionStateItem,
  User,
  Wallet,
  WalletPendingBalanceView,
  WalletState,
} from '../models';
import { Statement, StatementEntry } from '../schemas';
import { WithdrawRequestDto } from '../schemas/dto';
import {
  BillingService,
  ConsumerPipelinePublisher,
  EmailService,
  EmailTemplateService,
  EmailTemplateType,
  EventHandlingGateway,
  MediatorPipelinePublisher,
  PgIdempotenceStore,
  ProviderManagerService,
  SPBParticipantsService,
} from '../services';
import { getPaginationFromReq, groupBy, mergeDeep, ProviderUtil, setPaginationToRes } from '../utils';
import * as Serializers from '../serializers';
import { checkIfAssetIsRegisteredOnWallet } from '../filters/assets/IsAssetRegistered';

// TODO: Remove this and create a more inteliggent way of using the Virtual participant
const SPI_VIRTUAL_PARTICIPANT = '99999004';

@Controller('/wallets')
export default class WalletController {
  @Get('/', [OAuth.token([Scopes.wallets.READ]), Permissions.canUserReadAllWallets, Query.pagination])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);
    const {
      // Filter by Stellar public key
      hash,
      // TODO: Deprecated, should be moved to GET consumers/ and GET users/
      email,
      // TODO: Deprecated, should be moved to GET consumers/ and GET users/
      name,
    } = req.query;

    let includedDomains;

    if (!UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)) {
      if (req.user.role === UserRole.MEDIATOR) {
        includedDomains = req.user.domain.id;
      } else if (req.user.role === UserRole.CONSUMER) {
        includedDomains = req.user.id;
      }
    }

    const qb = Wallet.createQueryBuilder('wallet')
      .innerJoinAndSelect('wallet.user', 'user')
      .innerJoinAndSelect('user.domain', 'domain')
      .orderBy('wallet.createdAt', 'DESC')
      .skip(pagination.skip)
      .take(pagination.limit);

    qb.where('wallet.deletedAt is null');

    if (includedDomains) {
      qb.where('user.domain = :domain', { domain: includedDomains });
    }

    // TODO: Deprecated, should be moved to GET consumers/ and GET users/
    if (email) {
      qb.where('user.email = :email', { email: email.toLowerCase() });
    }

    // TODO: Deprecated, should be moved to GET consumers/ and GET users/
    if (name) {
      qb.where("user.firstName || ' ' || user.lastName ILIKE :name", { name: `${name}%` });
    }

    if (email) {
      qb.where('user.email = :email', { email: email.toLowerCase() });
    }

    if (hash || req.param('publicKey')) {
      qb.where(`stellar->>'publicKey' = :hash`, { hash: hash || req.param('publicKey') });
    }

    const [wallets, count] = await qb.getManyAndCount();

    setPaginationToRes(res, {
      dataLength: count,
      dataLimit: pagination.limit,
      dataSkip: pagination.skip,
    });

    res.success(wallets);
  }

  @Get('/:id', [
    OAuth.token([Scopes.wallets.READ]),
    Query.isValidId(),
    Exists.Wallet(),
    Permissions.canUserReadOneWallet,
  ])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    let found: Wallet;

    // TODO: Move to filter
    let domainInstance;
    if (req.user && req.user.role !== UserRole.ADMIN) {
      domainInstance = req.user.domain;
    } else {
      domainInstance = await Domain.getDefaultDomain();
    }

    if (id === 'root') {
      found = await Wallet.getRootWallet();
    } else if (id === 'mediator') {
      found = await Wallet.getMediatorWallet(domainInstance);
    } else {
      found = await Wallet.safeFindOne({
        where: { id },
        relations: [
          'user',
          'user.domain',
          'issuedAssets',
          'states',
          'assetRegistrations',
          'assetRegistrations.asset',
          'assetRegistrations.states',
        ],
      });
    }

    if (!found) {
      throw new EntityNotFoundError('Wallet');
    }

    // Check if is another consumer from thfoude same domain
    const isConsumer = req.user.role === UserRole.CONSUMER;
    const isNotSame = found.user.id !== req.user.id;
    const isSameDomain = !req.user.domain
      ? true
      : found.user.domain
      ? found.user.domain.id === req.user.domain.id
      : true;

    if (isConsumer && isNotSame && isSameDomain) {
      // Return reduced dataset
      return res.json(found.toSimpleJSON());
    }

    if (found.stellar && found.persistentStatus === WalletStatus.REGISTERED_IN_STELLAR) {
      // Ignore assets field from wallet since we will pull it from stellar network.
      const { assets: ignored, ...rest } = found.toJSON();
      const service = StellarService.getInstance();
      const info = await service.loadAccount(found.stellar.publicKey);
      const pendingBalances = await WalletPendingBalanceView.getWalletPendingBalance(found.id);
      const balanceByAsset = groupBy('asset', pendingBalances);

      // TODO: This should be moved to a service
      const assets = found.assetRegistrations
        .filter((registration) => registration.status === AssetRegistrationStatus.READY)
        .map(({ asset: _asset }) => {
          // Do a deep copy of _asset, otherwise if we mutate it we are also mutating assetRegistration.asset
          const asset = mergeDeep({}, _asset) as Asset & {
            balance: string;
            authorizableBalance: string;
            consolidatedBalance: string;
            pendingBalance: {
              amount: string;
              payments?: string[];
            };
          };

          const balance = info.balances.find((b) => asset.code === (b as any).asset_code)?.balance ?? '0';
          const pendingCredit = balanceByAsset?.[asset.id]?.[0]?.pendingCreditAmount ?? 0;
          const pendingDebts = balanceByAsset?.[asset.id]?.[0]?.pendingDebtAmount ?? 0;

          asset.consolidatedBalance = (Number(balance) - Number(pendingDebts) + Number(pendingCredit)).toFixed(7);
          asset.authorizableBalance = (Number(balance) - Number(pendingDebts)).toFixed(7);
          asset.balance = balance;
          asset.pendingBalance = { amount: (Number(pendingDebts) - Number(pendingCredit)).toFixed(7) };

          return asset;
        });
      return res.success({ assets, ...rest });
    }

    return res.success(found.toJSON());
  }

  @Get('/:id/assets', [
    OAuth.token([Scopes.wallets.READ]),
    Query.isValidId(),
    Permissions.canUserReadOneWallet,
    Exists.Wallet(),
  ])
  public static async getWalletAssets(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    let found: Wallet;

    // TODO: Move to filter
    let domainInstance;
    if (req.user && req.user.role !== UserRole.ADMIN) {
      domainInstance = req.user.domain;
    } else {
      domainInstance = await Domain.getDefaultDomain();
    }

    if (id === 'root') {
      found = await Wallet.getRootWallet();
    } else if (id === 'mediator') {
      found = await Wallet.getMediatorWallet(domainInstance);
    } else {
      found = await Wallet.safeFindOne({
        where: { id },
        relations: [
          'user',
          'user.domain',
          'issuedAssets',
          'states',
          'assetRegistrations',
          'assetRegistrations.asset',
          'assetRegistrations.states',
        ],
      });
    }

    if (!found) {
      throw new EntityNotFoundError('Wallet');
    }

    // Check if is another consumer from the same domain
    const isConsumer = req.user.role === UserRole.CONSUMER;
    const isNotSame = found.user.id !== req.user.id;
    const isSameDomain = !req.user.domain
      ? true
      : found.user.domain
      ? found.user.domain.id === req.user.domain.id
      : true;

    if (isConsumer && isNotSame && isSameDomain) {
      // Return reduced dataset
      return res.json(found.toSimpleJSON());
    }

    if (found.stellar && (found.status === WalletStatus.READY || found.status === WalletStatus.REGISTERED_IN_STELLAR)) {
      // Ignore assets field from wallet since we will pull it from stellar network.
      const { assets: ignored, ...rest } = found.toJSON();
      const assetRegistrations = found.assetRegistrations
        ? found.assetRegistrations.map((ar) => ar.toJSON())
        : undefined;

      return res.success({ assetRegistrations, ...rest });
    }

    return res.success(found.toJSON());
  }

  @Get('/:id/transactions', [
    OAuth.token([Scopes.transactions.READ]),
    Query.isValidId(),
    Permissions.canUserReadOneWallet,
    Exists.Wallet(),
  ])
  public static async getWalletTransactions(req: bacenRequest, res: BaseResponse) {
    const { id: walletId } = req.params;
    const {
      filters: rawFilters,
      initialDate,
      endDate,
      minAmount,
      maxAmount,
      status,
    }: {
      filters?: string;
      initialDate?: string;
      endDate?: string;
      minAmount?: string;
      maxAmount?: string;
      status: string;
    } = req.query;
    const pagination = getPaginationFromReq(req);

    let filters = {};
    if (rawFilters) {
      try {
        filters = JSON.parse(rawFilters);
      } catch (e) {
        throw new InvalidRequestError('Invalid filters', e);
      }
    }

    let query = Transaction.createQueryBuilder('t')
      .leftJoinAndSelect('t.source', 'source')
      .leftJoinAndSelect('source.user', 'source_user')
      .leftJoinAndSelect('t.payments', 'p')
      .leftJoinAndSelect('p.asset', 'a')
      .leftJoinAndSelect('p.destination', 'd')
      .leftJoinAndSelect('d.user', 'dest_user')
      .leftJoinAndSelect('t.states', 'states')
      .orderBy('t.createdAt', 'DESC')
      .where(
        new Brackets((qb) =>
          qb.where('t.source = :walletId', { walletId }).orWhere('p.destination = :walletId', { walletId }),
        ),
      )
      .skip(pagination.skip)
      .take(pagination.limit);

    if (initialDate) {
      query = query.andWhere('t.createdAt >= :initialDate', { initialDate });
    }

    if (endDate) {
      query = query.andWhere('t.createdAt <= :endDate', { endDate });
    }

    if (minAmount) {
      const payments = Payment.createQueryBuilder('payments')
        .select('payments."transactionId"')
        .groupBy('"transactionId"')
        .having('sum(amount::float) >= :minAmount')
        .getQuery();

      query = query.andWhere(`t.id IN (${payments})`).setParameter('minAmount', minAmount);
    }

    if (maxAmount) {
      const payments = Payment.createQueryBuilder('payments')
        .select('payments."transactionId"')
        .groupBy('"transactionId"')
        .having('sum(amount::float) <= :maxAmount')
        .getQuery();

      query = query.andWhere(`t.id IN (${payments})`).setParameter('maxAmount', maxAmount);
    }

    let idsByStatus: string[];

    if (status) {
      const transactionsByStatus = await Transaction.createQueryBuilder('transaction')
        .leftJoinAndSelect('transaction.states', 'transaction_states')
        .innerJoin(
          (qb) =>
            qb
              .select('"transactionId"')
              .addSelect('MAX(created_at)', 'created_at')
              .from(TransactionStateItem, 'transaction_states')
              .groupBy('"transactionId"'),
          'last_states',
          'last_states."transactionId" = transaction_states."transactionId" AND last_states.created_at = transaction_states.created_at',
        )
        .where('transaction_states.status = :status', { status })
        .getMany();

      idsByStatus = transactionsByStatus.map((transaction) => transaction.id);
    }

    if (filters && Object.keys(filters).length) {
      query = query.andWhere(
        new Brackets((qb) =>
          qb
            .where('t.additionalData @> :additionalData', { additionalData: { ...filters } })
            .orWhere('t.additionalData @> :additionalData', { userDefined: { userDefined: filters } }),
        ),
      );
    }

    let [result, count] = await query.getManyAndCount();

    if (status) {
      result = result.filter((transaction) => idsByStatus.includes(transaction.id));
    }

    // const [result, count] = await Transaction.getMany(pagination, {
    //   filters: { walletId },
    //   relations: [
    //     'source',
    //     'source.user',
    //     'payments',
    //     'payments.asset',
    //     'payments.destination',
    //     'payments.destination.user',
    //     'states',
    //   ],
    //   orderBy: { createdAt: 'DESC' },
    //   userFilters: filters,
    // });

    // Set pagination headers
    setPaginationToRes(res, { dataLength: count, dataLimit: pagination.limit, dataSkip: pagination.skip });

    res.success(result);
  }

  @Get('/:walletId/payments', [OAuth.token([Scopes.payments.READ]), Permissions.canUserReadOneWallet, Exists.Wallet()])
  public static async getWalletPayments(req: bacenRequest, res: BaseResponse) {
    const {
      filters: rawFilters,
      paymentType,
      initialDate,
      endDate,
      minAmount,
      maxAmount,
      status,
    }: {
      filters?: string;
      initialDate?: string;
      endDate?: string;
      minAmount?: string;
      maxAmount?: string;
      status: string;
      paymentType: string;
    } = req.query;
    const { walletId } = req.params;
    const pagination = getPaginationFromReq(req);

    let filters = {};
    if (rawFilters) {
      try {
        filters = JSON.parse(rawFilters);
      } catch (e) {
        throw new InvalidRequestError('Invalid filters', e);
      }
    }
    let query = Transaction.createQueryBuilder('t')
      .leftJoinAndSelect('t.source', 'source')
      .leftJoinAndSelect('source.user', 'source_user')
      .leftJoinAndSelect('t.banking', 'b')
      .leftJoinAndSelect('t.payments', 'p')
      .leftJoinAndSelect('p.asset', 'a')
      .leftJoinAndSelect('p.destination', 'd')
      .leftJoinAndSelect('d.user', 'dest_user')
      .leftJoinAndSelect('t.states', 'states')
      .orderBy('t.createdAt', 'DESC')
      .where(
        new Brackets((qb) =>
          qb.where('t.source = :walletId', { walletId }).orWhere('p.destination = :walletId', { walletId }),
        ),
      )
      .andWhere('t.type = :type', { type: TransactionType.PAYMENT })
      .skip(pagination.skip)
      .take(pagination.limit);

    if (initialDate) {
      query = query.andWhere('t.createdAt >= :initialDate', { initialDate });
    }

    if (endDate) {
      query = query.andWhere('t.createdAt <= :endDate', { endDate });
    }

    if (minAmount) {
      const payments = Payment.createQueryBuilder('payments')
        .select('payments."transactionId"')
        .groupBy('"transactionId"')
        .having('sum(amount::float) >= :minAmount')
        .getQuery();

      query = query.andWhere(`t.id IN (${payments})`).setParameter('minAmount', minAmount);
    }

    if (maxAmount) {
      const payments = Payment.createQueryBuilder('payments')
        .select('payments."transactionId"')
        .groupBy('"transactionId"')
        .having('sum(amount::float) <= :maxAmount')
        .getQuery();

      query = query.andWhere(`t.id IN (${payments})`).setParameter('maxAmount', maxAmount);
    }

    let idsByStatus: string[];

    if (status) {
      const transactionsByStatus = await Transaction.createQueryBuilder('transaction')
        .leftJoinAndSelect('transaction.states', 'transaction_states')
        .innerJoin(
          (qb) =>
            qb
              .select('"transactionId"')
              .addSelect('MAX(created_at)', 'created_at')
              .from(TransactionStateItem, 'transaction_states')
              .groupBy('"transactionId"'),
          'last_states',
          'last_states."transactionId" = transaction_states."transactionId" AND last_states.created_at = transaction_states.created_at',
        )
        .where('transaction_states.status = :status', { status })
        .getMany();

      idsByStatus = transactionsByStatus.map((transaction) => transaction.id);
    }

    if (filters && Object.keys(filters).length) {
      query = query.andWhere(
        new Brackets((qb) =>
          qb
            .where('t.additionalData @> :additionalData', { additionalData: { ...filters } })
            .orWhere('t.additionalData @> :additionalData', { userDefined: { userDefined: filters } }),
        ),
      );
    }

    if (paymentType) {
      query = query.andWhere('p.type = :paymentType', { paymentType });
    }

    // Query and set pagination headers
    let [result, count] = await query.getManyAndCount();

    if (status) {
      result = result.filter((transaction) => idsByStatus.includes(transaction.id));
    }

    setPaginationToRes(res, { dataLength: count, dataLimit: pagination.limit, dataSkip: pagination.skip });
    return res.success(result);
  }

  @Get('/:walletId/ledger', [
    OAuth.token([Scopes.wallets.READ]),
    Permissions.canUserReadPaymentsFromLedger,
    Exists.Wallet(),
  ])
  public static async getStatementFromLedger(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const { cursor } = req.query;

    const wallet = await Wallet.safeFindOne({ where: { id: walletId } });

    const response = await StellarService.getInstance().getPayments(wallet.stellar.publicKey, cursor);
    const finalCursor = response.cursor;
    const entryPromises = response.records
      .filter((record) => record.type === 'payment')
      .map((record) => StatementEntry.from(record, wallet));

    const entries = await Promise.all(entryPromises);
    const groupedByAsset = groupBy('asset', entries as any[]);
    const accountStatement = Object.entries(groupedByAsset).map(([key, values]) => Statement.from(values as any));

    res.success({ accountStatement, finalCursor });
  }

  @Get('/:walletId/depositInfo', [
    OAuth.token([Scopes.wallets.READ]),
    Query.isValidId('walletId'),
    Permissions.canUserReadOneWallet,
    Exists.Wallet(),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.params.walletId,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
  ])
  public static async getDepositInfo(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const { provider }: { provider: CustodyProvider } = req.query;

    const wallet = await Wallet.safeFindOne({
      where: { id: walletId },
      relations: ['assetRegistrations', 'assetRegistrations.states', 'assetRegistrations.asset'],
    });
    if (provider && !wallet.assets.some((asset) => asset.provider === provider)) {
      throw new InvalidParameterError(`The given wallet were not registered in the custody provider: ${provider}`);
    }

    let providers: CustodyProvider[];
    if (provider) {
      providers = [provider];
    } else {
      providers = wallet.assetRegistrations
        .filter((reg) => reg.status === AssetRegistrationStatus.READY && !!reg.asset.provider)
        .map((reg) => reg.asset.provider);
    }

    const accountsInfo = [];
    for (const custodyProvider of providers) {
      const providerInstance = ProviderManagerService.getInstance().from(custodyProvider);
      if (!providerInstance.implements(CustodyFeature.DEPOSIT)) continue;

      const depositFeature = providerInstance.feature(CustodyFeature.DEPOSIT);
      await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
      accountsInfo.push(await depositFeature.info(wallet));
    }

    res.success(accountsInfo);
  }

  @Post('/', [OAuth.token([Scopes.wallets.WRITE]), Permissions.canUserWriteWallet])
  public static async createWallet(req: bacenRequest, res: BaseResponse) {
    const schema: { isTransitoryAccount: boolean; transitoryAccountType: TransitoryAccountType; extra: any } & Wallet =
      req.body;
    const extra = schema?.extra ?? {};

    if (req.user.role === UserRole.CONSUMER) {
      // So consumers can't create wallets for other users
      schema.user = req.user;
    } else {
      // Create wallets for the requesting user by default
      schema.user = schema.user || req.user;
    }

    let acceptProviderLegalTerms = false;
    if (UnleashUtil.isEnabled(UnleashFlags.AUTO_ACCEPT_PROVIDER_TERMS_CONSUMER_CREATION)) {
      acceptProviderLegalTerms = true;
    } else {
      const userId = typeof schema.user === 'string' ? schema.user : schema.user.id;
      const user = await User.safeFindOne({ where: { id: userId }, relations: ['wallets', 'wallets.states'] });
      const defaultWalletFirstState = user.wallets[0].states.find(
        (walletState) => walletState.status === WalletStatus.PENDING,
      );

      acceptProviderLegalTerms = defaultWalletFirstState.additionalData.hasOwnProperty('acceptProviderLegalTerms')
        ? defaultWalletFirstState.additionalData.acceptProviderLegalTerms
        : false;
    }

    if (extra && (extra.number || extra.bankingType)) {
      let available: boolean;
      // TODO: This may be done for assets other than the root-asset
      const asset = await Asset.getRootAsset();
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_HOOKS)) {
        const provider = ProviderManagerService.getInstance().from(asset.provider);
        available = await provider.isAccountNumberAvailable({
          accountNumber: extra.number,
          branch: extra.branch,
          bankingType: extra.bankingType,
        });
      } else {
        available = await HooksService.getInstance().wallets().isAccountNumberAvailable({
          asset: asset.code,
          branch: extra.branch,
          accountNumber: extra.number,
          bankingType: extra.bankingType,
        });
      }

      if (!available) {
        throw new UniqueParameterViolationError('Account with same number and/or type', {
          branch: extra.branch,
          number: extra.number,
          bankingType: extra.bankingType,
        });
      }
    }

    const wallet = await getManager().transaction(async (manager) => {
      let additionalData: any = {};
      if (schema.hasOwnProperty('isTransitoryAccount')) {
        if (!schema.transitoryAccountType) throw new InvalidRequestError('The transitory account type is required');

        additionalData = {
          isTransitoryAccount: schema.isTransitoryAccount,
          transitoryAccountType: schema.transitoryAccountType,
        };
      }

      const transientWallet = await Wallet.insertAndFind({ additionalData, ...schema }, { manager });

      await manager.insert(
        WalletState,
        WalletState.create({
          wallet: { id: transientWallet.id },
          status: WalletStatus.PENDING,
          additionalData: {
            ...extra,
            acceptProviderLegalTerms,
            fingerprint: `${(req as any).useragent.source}#${req.ip}`,
          } as any,
        }),
      );

      return manager.findOne(Wallet, {
        where: { id: transientWallet.id },
        relations: ['user', 'issuedAssets', 'states', 'user.consumer', 'user.domain'],
      });
    });

    if (wallet.user.role === UserRole.CONSUMER) {
      const fsm = new ConsumerStateMachine(wallet.user);
      await fsm.goTo(ConsumerStatus.PROCESSING_WALLETS);
      await ConsumerPipelinePublisher.getInstance().send(wallet.user);
    } else if (wallet.user.role === UserRole.MEDIATOR) {
      const consumerFSM = new ConsumerStateMachine(wallet.user);
      await consumerFSM.goTo(ConsumerStatus.PROCESSING_WALLETS);

      const userFSM = new UserStateMachine(wallet.user);
      await userFSM.goTo(UserStatus.PROCESSING);
      await MediatorPipelinePublisher.getInstance().send(wallet.user);
    }

    res.success(wallet);
  }

  @Idempotent({
    store: PgIdempotenceStore,
    keyFn: (req) => req.get('X-Idempotence-Key') || req.get('X-EndToEnd-Id'),
  })
  @UseLock('walletId', (req) => {
    const context: UnleashContext = {
      userId: req.user.id,
      properties: {
        domainId: req.user.domainId,
        walletId: req.params.walletId,
        assetCode: req.body.asset,
      },
    };
    return UnleashUtil.isEnabled(UnleashFlags.ENABLE_WITHDRAW_WALLET_LOCK, context, false);
  })
  @ServiceCharged(ServiceType.WITHDRAWAL)
  @Post('/:walletId/withdraw', [
    OAuth.token([Scopes.payments.WRITE]),
    Query.isValidId('walletId'),
    Exists.Wallet({
      cache: true,
      relations: ['states', 'user', 'user.domain', 'user.twoFactorSecret', 'user.consumer', 'user.consumer.states'],
    }),
    Amount.isValidAmount,
    Request.signing(),
    Assets.hasProvider('asset', false),
    Permissions.canUserWritePayment('walletId'),
    OAuth.otp(false, async (req) => {
      const wallet =
        req.cache.get<Wallet>(`wallet_${req.params.walletId}`) ||
        (await Wallet.safeFindOne({
          where: { id: req.params.walletId },
          relations: ['user', 'user.twoFactorSecret'],
        }));

      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }

      return wallet.user;
    }),
    Limits.transactionAmount,
    Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.params.walletId,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
  ])
  public static async withdraw(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;

    // extract optional params from header
    const requestTimestamp = req.get('X-Request-Timestamp');
    const requestedAt = requestTimestamp ? new Date(requestTimestamp) : new Date();

    const endToEndId = req.get('X-EndToEnd-Id');
    const userTransactionId = req.get('X-Transaction-Id');

    let schema: WithdrawRequestDto;

    try {
      schema = await WithdrawRequestDto.create(req.body);
    } catch (err) {
      throw new InvalidParameterError('WithdrawRequest', err);
    }

    // TODO: Put this in the DTO
    if (
      typeof schema.additionalData === 'object' &&
      JSON.stringify(schema.additionalData).length > Config.api.maxCustomFieldsSize
    ) {
      throw new InvalidParameterError(`The field "additionalData" is too large`);
    }

    let asset = await Asset.getByIdOrCode(schema.asset || 'root', { relations: ['issuer'] });

    if (!asset) {
      asset = await Asset.getRootAsset();
    }

    // Check asset exists
    if (!asset) {
      throw new EntityNotFoundError('Asset');
    }

    // Check if asset has a provider
    if (!asset.provider) {
      throw new HttpError(
        `Not available provider to asset ${asset.code} for this operation`,
        HttpCode.Client.BAD_REQUEST,
      );
    }

    if (!schema.arrangement || ![Arrangement.STR, Arrangement.PIX].includes(schema.arrangement)) {
      schema.arrangement = Config.spb.defaultArrangement;
    }

    if (schema.bank) {
      // Makes sure agency digit it not required, we'll put "0" if undefined
      schema.bank.agencyDigit =
        schema.bank.agencyDigit && schema.bank.agencyDigit.length > 0 ? schema.bank.agencyDigit : '0';
    }

    /*
     * We could have cached the wallet already with the assetRegistrations relation, but this proved to be slower than
     * fetching the assetRegistrations in a separate query. This boils down to the fact that the number of rows
     * returned by a query grows combinatorially with the number of joined relations, so a query with many joined
     * relations will get slow very easily, due to the work in generating the SQL and the sheer amount of data returned
     * by it.
     */
    const wallet = req.cache.get<Wallet>(`wallet_${walletId}`);
    wallet.assetRegistrations =
      req.cache.get<AssetRegistration[]>(`wallet_asset_registrations_${walletId}`) ||
      (await AssetRegistration.createQueryBuilder('assetRegistration')
        .leftJoinAndSelect('assetRegistration.states', 'states')
        .leftJoinAndSelect('assetRegistration.asset', 'asset')
        .where('assetRegistration.wallet = :walletId', { walletId })
        .getMany());

    // throws 400 if asset is not registered on wallet.
    checkIfAssetIsRegisteredOnWallet(asset, wallet);

    const { consumer } = wallet.user;

    let destinationBank: Banking;

    let sourceFIISPB = wallet.user.domain.settings.ispb;
    let destinationFIISPB: string;

    const participants = await SPBParticipantsService.getInstance().getAll();

    if (schema.bankingId) {
      // If banking does not exist findById already throws a not found exception.
      destinationBank = await Banking.findById(schema.bankingId);

      if (destinationBank.bank.length === 8) {
        destinationFIISPB = destinationBank.bank;
      } else {
        const destinationFI = participants.find(
          (participant) => Number(participant.code) === Number(destinationBank.bank),
        );

        if (!destinationFI) throw new EntityNotFoundError('Destination Financial Institution');

        destinationFIISPB = destinationFI.ispb;
      }
    } else if (schema.bank && consumer) {
      if (schema.bank.bank.length === 8) {
        destinationFIISPB = schema.bank.bank;
      } else if (!destinationFIISPB) {
        const destinationFI = participants.find(
          (participant) => Number(participant.code) === Number(schema.bank?.bank),
        );

        if (!destinationFI) throw new EntityNotFoundError('Destination Financial Institution');

        destinationFIISPB = destinationFI.ispb;
      }

      if (schema.bank.agency && schema.bank.account && UnleashUtil.isEnabled(UnleashFlags.ENABLE_DICT)) {
        const response = await DictWebService.getInstance()
          .account()
          .isInternalFinancialInstitution({ ispb: destinationFIISPB });

        if (response.isInternal) {
          try {
            const dictAccount: Account = await DictWebService.getInstance()
              .account()
              .getAccount({
                ispb: destinationFIISPB,
                branch: String(schema.bank.agency),
                number: String(schema.bank.account),
              });

            if (Number(schema.bank.taxId.replace(/\D/g, '')) !== Number(dictAccount.user.taxId.replace(/\D/g, ''))) {
              throw new InvalidRequestError("Destination account holder's taxId mismatch");
            }

            schema.bank.hasSameSourceFI = true;
          } catch (error) {
            if (error instanceof InvalidRequestError) throw error;

            throw error?.response?.status === 404
              ? new EntityNotFoundError('Destination Account')
              : new HttpError(
                  error.originalMessage ||
                    error.response?.data?.message ||
                    error.message ||
                    'Error retrieving destination account data from DICT API',
                  error?.response?.status || HttpCode.Server.INTERNAL_SERVER_ERROR,
                );
          }
        }
      }
      destinationBank = await getManager().transaction(async (manager) => Banking.from(schema.bank, consumer, manager));
    } else if (
      UnleashUtil.isEnabled(UnleashFlags.ENABLE_DICT) &&
      schema.arrangement === Arrangement.PIX &&
      schema?.key
    ) {
      let dictResponse: {
        entry: EntrySchema;
        internal: boolean;
        endToEndId: string;
      };
      try {
        dictResponse = await DictWebService.getInstance().entry().getEntryInternal({ key: schema.key });
      } catch (error) {
        if (error?.response?.status !== 404) {
          const message =
            error.originalMessage ||
            error.response?.data?.message ||
            error.message ||
            'Error retrieving destination account data from DICT API';
          throw new HttpError(message, error?.response?.status || HttpCode.Server.INTERNAL_SERVER_ERROR);
        }
      }

      if (!dictResponse) {
        try {
          dictResponse = await DictWebService.getInstance()
            .entry()
            .getEntry({
              ispb: wallet.user.domain.settings.ispb,
              key: schema.key,
              userId: schema.extra?.accountExternalId || wallet.user.id,
            });
        } catch (error) {
          if (error?.response?.status !== 404) {
            const message =
              error.originalMessage ||
              error.response?.data?.message ||
              error.message ||
              'Error retrieving destination account data from DICT API';
            throw new HttpError(message, error?.response?.status || HttpCode.Server.INTERNAL_SERVER_ERROR);
          }
        }
      }

      if (dictResponse) {
        // TODO: We should think on what this might implicate
        // if (!endToEndId) endToEndId = dictResponse.endToEndId;

        destinationFIISPB = dictResponse.entry.account.ispb;

        destinationBank = await getManager().transaction(async (manager) =>
          Banking.from(
            {
              key: schema.key,
              holderType: dictResponse.entry.account.user.type,
              taxId: dictResponse.entry.account.user.taxId,
              type: dictResponse.entry.account.type,
              bank: dictResponse.entry.account.ispb,
              name: dictResponse?.entry?.account?.user?.name,
              account: dictResponse.entry.account.number,
              agency: dictResponse.entry.account.branch,
              hasSameSourceFI: dictResponse.internal,
            },
            consumer,
            manager,
          ),
        );
      }
    }

    if (!destinationBank) {
      throw new EntityNotFoundError(schema.key ? 'Key' : 'Banking information');
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);

    let source: BankingSchema & { externalId?: string };
    if (UnleashUtil.isEnabled(UnleashFlags.ENABLE_DICT) && schema?.extra?.accountExternalId) {
      let dictAccount: Account;
      try {
        dictAccount = await DictWebService.getInstance().account().getAccount({
          ispb: wallet.user.domain.settings.ispb,
          externalId: schema.extra.accountExternalId,
        });
      } catch (error) {
        if (error?.response?.status !== 404) {
          const message =
            error.originalMessage ||
            error.response?.data?.message ||
            error.message ||
            `Error retrieving source account data associated to externalId ${schema.extra.accountExternalId} from DICT API`;
          throw new HttpError(message, error?.response?.status ?? HttpCode.Server.INTERNAL_SERVER_ERROR);
        }
      }

      if (dictAccount) {
        source = {
          externalId: schema.extra.accountExternalId,
          holderType: dictAccount.user.type,
          name: dictAccount.user.name,
          taxId: dictAccount.user.taxId,
          type: dictAccount.type,
          bank: dictAccount.ispb,
          account: dictAccount.number,
          agency: dictAccount.branch,
        };
        sourceFIISPB = dictAccount.ispb;
      } else {
        throw new EntityNotFoundError('Source account', { accountExternalId: schema.extra.accountExternalId });
      }
    } else {
      const depositFeature = provider.feature(CustodyFeature.DEPOSIT);
      await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
      const sourceAccountInfo: {
        name: string;
        taxId: string;
        bank: {
          ispb: string;
          code: string;
          name: string;
        };
        account: {
          holder: string;
          agency: string;
          number: string;
          type: string;
          status: string;
          createdAt: string;
        };
        asset: string;
      } = await depositFeature.info(wallet);

      source = {
        holderType: consumer.type,
        name: sourceAccountInfo.name,
        taxId: sourceAccountInfo.taxId,
        type: sourceAccountInfo.account.type as any,
        bank: sourceAccountInfo.bank.code,
        account: sourceAccountInfo.account.number,
        agency: sourceAccountInfo.account.agency,
      };
    }

    const transaction = await getManager().transaction(async (manager) => {
      let trx = await asset.destroy({
        manager,
        amount: String(schema.amount),
        createdBy: req.accessToken,
        targetWallet: wallet,
        scheduleFor: schema?.extra?.scheduleFor ?? undefined,
        banking: destinationBank,
        additionalData: {
          endToEndId,
          userTransactionId,
          instructionId: endToEndId,
          arrangement: schema.arrangement,
          type: destinationBank.hasSameSourceFI ? 'internal-transfer' : undefined,
          accountExternalId: schema?.extra?.accountExternalId ?? undefined,
          purpose: schema?.extra?.purpose ?? undefined,
          remittanceInfo: schema?.extra?.remittanceInfo ?? undefined,
          pix:
            schema.arrangement === Arrangement.PIX && schema?.extra?.priority
              ? { priority: schema.extra.priority }
              : undefined,
          str:
            schema.arrangement === Arrangement.STR && schema?.extra?.creditAgreementId
              ? { creditAgreementId: schema.extra.creditAgreementId }
              : undefined,
          source: source
            ? {
                ispb: sourceFIISPB,
                bankNumber: source.bank,
                branchNumber: source.agency,
                accountNumber: source.account,
                accountHolderName: source.name,
                accountHolderTaxId: source.taxId,
                accountHolderType: source.holderType,
              }
            : undefined,
          destination: {
            ispb: destinationFIISPB,
            bankNumber: destinationBank.bank,
            branchNumber: destinationBank.agency,
            accountNumber: destinationBank.account,
            accountHolderName: destinationBank.name,
            accountHolderTaxId: destinationBank.taxId,
            accountHolderType: destinationBank.holderType,
          },
          customFields: schema.additionalData,
        } as any,
      });

      const withdrawal = trx.payments.find((p) => p.type === PaymentType.WITHDRAWAL);
      /*
       * The payment passed to the withdraw feature must have the properties payment.transaction.id and
       * payment.transaction.source.id. We deep-clone the withdrawal payment obj and set these properties inline in
       * order to avoid a new query returning the payment with these relations populated.
       */
      const payment = deepCopy(withdrawal);
      payment.transaction = {
        id: trx.id,
        source: { id: trx.source.id } as Wallet,
      } as Transaction;

      const withdrawFeature = provider.feature(CustodyFeature.WITHDRAW);
      await ProviderUtil.throwIfFeatureNotAvailable(withdrawFeature);
      const response = await withdrawFeature.withdraw(
        {
          ...payment,
          creditAgreementId: schema?.extra?.creditAgreementId ?? undefined,
          purpose: schema?.extra?.purpose ?? undefined,
          scheduleFor: schema?.extra?.scheduleFor ?? undefined,
        },
        {
          requestedAt,
          endToEndId,
          transactionId: userTransactionId,
          source,
          remittanceInfo: schema?.extra?.remittanceInfo,
          arrangement: schema.arrangement,
          type: destinationBank.hasSameSourceFI ? 'internal-transfer' : undefined,
          internalDestination: destinationBank.hasSameSourceFI,
          destination: {
            ...destinationBank,
            taxIdNumber: destinationBank.taxId,
          },
          pix:
            schema.arrangement === Arrangement.PIX && schema?.extra?.priority
              ? { priority: schema.extra.priority }
              : undefined,
        },
      );

      if (response.id === payment.id && response.status === PaymentStatus.SETTLED) {
        await manager.update(Payment, payment.id, { status: PaymentStatus.SETTLED });
        payment.status === PaymentStatus.SETTLED;
      }

      if (req.serviceFee) {
        trx = await BillingService.getInstance().chargeServiceFee(req.serviceFee, trx, manager);
      }

      return trx;
    });

    await EventHandlingGateway.getInstance().process(ServiceType.WITHDRAWAL, transaction);
    res.success(Serializers.transactionSerializer(transaction));
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ReleaseBalance()
  @UseLock('walletId')
  @Post('/:walletId/ip-account-recharge', [
    OAuth.token([Scopes.payments.WRITE]),
    Query.isValidId('walletId'),
    Amount.isValidAmount,
    Request.signing(),
    Assets.hasProvider('asset', false),
    Permissions.canUserWritePayment('walletId'),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.params.walletId },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Limits.transactionAmount,
    Status.isWalletInStatus({ walletIdFn: (req) => req.params.walletId, requiredStatus: WalletStatus.READY }),
    Payments.hasUserEnoughBalance(undefined, defaultAmountsFn('asset')),
  ])
  public static async instantPaymentAccountRecharge(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const data: { asset?: string; amount: string; ispb?: string } = req.body;

    const transaction = await this.transferBetweenSettlementAccounts({
      walletId,
      accessToken: req.accessToken,
      operationType: 'recharge',
      ...data,
    });

    res.success(Serializers.transactionSerializer(transaction));
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ReleaseBalance()
  @UseLock('walletId')
  @Post('/:walletId/ip-account-withdrawal', [
    OAuth.token([Scopes.payments.WRITE]),
    Query.isValidId('walletId'),
    Amount.isValidAmount,
    Request.signing(),
    Assets.hasProvider('asset', false),
    Permissions.canUserWritePayment('walletId'),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.params.walletId },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Limits.transactionAmount,
    Status.isWalletInStatus({ walletIdFn: (req) => req.params.walletId, requiredStatus: WalletStatus.READY }),
    Payments.hasUserEnoughBalance(undefined, defaultAmountsFn('asset')),
  ])
  public static async instantPaymentAccountWithdrawal(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const data: { amount: string; asset?: string; ispb?: string; bank?: Banking; bankingId?: string } = req.body;

    const wallet = await Wallet.safeFindOne({
      where: { id: walletId },
      relations: ['user', 'user.consumer'],
    });

    let destination: Banking;

    if (data.bankingId) {
      destination = await Banking.findById(data.bankingId);
    } else if (data.bank) {
      destination = await getManager().transaction(async (manager) =>
        Banking.from(
          {
            ...data.bank,
            holderType: AccountType.FINANCIAL_INSTITUTION,
          },
          wallet.user.consumer,
          manager,
        ),
      );
    }

    const transaction = await this.transferBetweenSettlementAccounts({
      walletId,
      destination,
      accessToken: req.accessToken,
      operationType: 'withdraw',
      ...data,
    });

    res.success(Serializers.transactionSerializer(transaction));
  }

  public static async transferBetweenSettlementAccounts(data: {
    walletId: string;
    accessToken: any;
    asset?: string;
    amount: string;
    ispb?: string;
    destination?: Banking;
    operationType: 'withdraw' | 'recharge';
  }): Promise<Transaction> {
    let asset = await Asset.getByIdOrCode(data.asset || 'root', { relations: ['issuer'] });

    if (!asset) {
      asset = await Asset.getRootAsset();
    }

    // Check asset exists
    if (!asset) {
      throw new EntityNotFoundError('Asset');
    }

    const wallet = await Wallet.safeFindOne({
      where: { id: data.walletId },
      relations: ['user', 'user.domain', 'assetRegistrations', 'assetRegistrations.states', 'assetRegistrations.asset'],
    });

    const providerInstance = ProviderManagerService.getInstance().from(asset.provider);
    const depositFeature = providerInstance.feature(CustodyFeature.DEPOSIT);
    await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
    const sourceAccountInfo = await depositFeature.info(wallet);

    if (data.operationType === 'withdraw' && sourceAccountInfo.account.type !== BankingType.INSTANT_PAYMENT) {
      throw new InvalidRequestError('The source account must be a instant payment account');
    } else if (data.operationType === 'recharge' && sourceAccountInfo.account.type !== BankingType.SETTLEMENT) {
      throw new InvalidRequestError('The source account must be a settlement account');
    }

    let destination: BankingSchema;
    if (data.destination) {
      destination = data.destination;
    } else {
      destination = {
        bank: data.ispb ?? sourceAccountInfo.bank.ispb,
        type:
          sourceAccountInfo.account.type === BankingType.SETTLEMENT
            ? BankingType.INSTANT_PAYMENT
            : BankingType.SETTLEMENT,
      };
    }

    return getManager().transaction(async (manager) => {
      const trx = await asset.destroy({
        manager,
        amount: String(data.amount),
        createdBy: data.accessToken,
        targetWallet: wallet,
        additionalData: {
          destination: { bankNumber: destination.bank, accountType: destination.type },
        } as any,
      });

      const withdrawal = trx.payments.find((p) => p.type === PaymentType.WITHDRAWAL);
      const payment = await manager.findOne(Payment, {
        where: { id: withdrawal.id },
        relations: ['transaction', 'transaction.source'],
      });

      const provider = ProviderManagerService.getInstance().from(asset.provider);

      const withdrawFeature = provider.feature(CustodyFeature.WITHDRAW);
      await ProviderUtil.throwIfFeatureNotAvailable(withdrawFeature);
      const response = await withdrawFeature.withdraw(payment, {
        destination,
        arrangement: Arrangement.STR,
      });

      if (response.id === payment.id && response.status === PaymentStatus.SETTLED) {
        await manager.update(Payment, payment.id, { status: PaymentStatus.SETTLED });
      }

      return trx;
    });
  }

  @Post('/:walletId/check-timescale')
  public static async checkTimescale(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;

    const wallet = await Wallet.safeFindOne({ where: { id: walletId } });
    await wallet.checkBalanceWithTimescale();

    res.success();
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:walletId/dict', [
    OAuth.token([Scopes.wallets.READ]),
    Exists.Wallet(),
    Query.isValidId('walletId'),
    // TODO: Uncomment this, this is just a workaround for the 05/10/2020 launch.
    // Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    // Status.isWalletInStatus('walletId', WalletStatus.READY),
  ])
  public static async createEntryDict(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    let {
      key,
      keyType,
      account,
      provider = Config.provider.defaultCustodyProvider,
      ispb,
      reason,
    }: {
      key: string;
      keyType: string;
      account: Account;
      provider: CustodyProvider;
      ispb: string;
      reason: EntryReason;
    } = req.body;

    try {
      const wallet = await Wallet.findOne({
        where: { id: walletId },
        relations: ['user', 'user.consumer', 'user.consumer.phones'],
      });
      const domain = await Domain.findOne(wallet.user.domainId);

      if (!account?.externalId && !account) {
        const providerInstance = ProviderManagerService.getInstance().from(provider);
        const depositFeature = providerInstance.feature(CustodyFeature.DEPOSIT);
        await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
        const depositInfo = await depositFeature.info(wallet);

        account = {
          number: depositInfo.account.number,
          branch: depositInfo.account.agency,
          ispb,
          type: depositInfo.account.type,
          openingDate: depositInfo.account.createdAt,
          walletId,
          user: {
            name: wallet.user.name,
            type: wallet.user.consumer.type,
            taxId: wallet.user.consumer.taxId,
            tradeName: wallet.user.consumer.companyData?.tradeName,
          },
        };
      } else if (account.externalId && !account) {
        throw new HttpError('Required parameter account', HttpCode.Client.PRECONDITION_FAILED);
      }

      const response = await DictWebService.getInstance()
        .entry()
        .create({
          key,
          keyType,
          account: { ...account, walletId, ispb: account.ispb || ispb || Config.dict.ispb },
          domainId: domain.id,
          reason,
          instanceId: Config.server.instanceId,
        });

      const confirmationEnabled = UnleashUtil.isEnabled(UnleashFlags.ENABLE_EMAIL_PHONE_CONFIRMATION);

      if (confirmationEnabled) {
        if (keyType === KeyType.EMAIL) {
          const token = await OAuthSecretToken.generateRecoveryToken(wallet.user, [Scopes.wallets.WRITE], {
            email: key,
          });
          const emailService = EmailService.getInstance();

          const emailLocals = EmailTemplateService.getInstance().getConfig({
            type: EmailTemplateType.CONFIRMATION_DICT,
            domain: wallet.user.domainId,
          });

          // Send an email with instructions
          await emailService.send({
            to: key,
            subject: Config.user.confirmDictEmail.subject,
            text: `Confirm your email: ${Config.user.confirmDictEmail.url.replace(
              new RegExp(':token', 'ig'),
              token.secretToken,
            )}`,
            locals: {
              ...emailLocals,
              button: {
                label: emailLocals.button.label,
                url: Config.user.confirmDictEmail.url.replace(new RegExp(':token', 'ig'), token.secretToken),
              },
            },
          });
        }

        if (keyType === KeyType.PHONE) {
          const phone = await Phone.insertAndFind({
            ...Phone.getPhoneFromString(key),
            consumer: wallet.user.consumer,
          });

          await phone.sendVerificationHash();
        }
      }

      res.success(response);
    } catch (error) {
      wrapError(error);
    }
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Put('/:walletId/dict/:key', [
    OAuth.token([Scopes.wallets.READ]),
    Exists.Wallet(),
    Query.isValidId('walletId'),
    // TODO: Uncomment this, this is just a workaround for the 05/10/2020 launch.
    // Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    // Status.isWalletInStatus('walletId', WalletStatus.READY),
  ])
  public static async updateEntryDict(req: bacenRequest, res: BaseResponse) {
    const { walletId, key } = req.params;
    const { account, reason } = req.body;

    try {
      const response = await DictWebService.getInstance().entry().update(key, { account, walletId, reason });

      res.success(response);
    } catch (error) {
      wrapError(error);
    }
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Delete('/:walletId/dict/:idOrKey', [
    OAuth.token([Scopes.wallets.READ]),
    Exists.Wallet(),
    Query.isValidId('walletId'),
    // TODO: Uncomment this, this is just a workaround for the 05/10/2020 launch.
    // Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    // Status.isWalletInStatus('walletId', WalletStatus.READY),
  ])
  public static async deleteEntryDict(req: bacenRequest, res: BaseResponse) {
    const { idOrKey } = req.params;
    let { reason, ispb } = req.query;

    if (!ispb) {
      const user = await User.safeFindOne({ where: { id: req.user.id }, relations: ['domain'] });
      ispb = user.domain.settings.ispb;
    }

    try {
      const response = await DictWebService.getInstance().entry().deleteEntry({ idOrKey, reason, ispb });

      res.success(response);
    } catch (error) {
      wrapError(error);
    }
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:walletId/claims', [
    OAuth.token([Scopes.wallets.READ]),
    Exists.Wallet(),
    Query.isValidId('walletId'),
    // TODO: Uncomment this, this is just a workaround for the 05/10/2020 launch.
    // Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    // Status.isWalletInStatus('walletId', WalletStatus.READY),
  ])
  public static async createClaimDict(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    let {
      key,
      keyType,
      type,
      account,
      provider = Config.provider.defaultCustodyProvider,
      ispb,
      reason,
    }: {
      key: string;
      keyType: string;
      type: ClaimType;
      account: Account;
      provider: CustodyProvider;
      ispb: string;
      reason: EntryReason;
    } = req.body;

    try {
      const wallet = await Wallet.findOne({
        where: { id: walletId },
        relations: ['user', 'user.consumer', 'user.consumer.phones'],
      });

      const domain = await Domain.findOne(wallet.user.domainId);

      if (!account?.externalId && !account) {
        const providerInstance = ProviderManagerService.getInstance().from(provider);
        const depositFeature = providerInstance.feature(CustodyFeature.DEPOSIT);
        await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
        const depositInfo = await depositFeature.info(wallet);

        account = {
          number: depositInfo.account.number,
          branch: depositInfo.account.agency,
          ispb,
          type: depositInfo.account.type,
          openingDate: depositInfo.account.createdAt,
          walletId,
          user: {
            name: wallet.user.name,
            type: wallet.user.consumer.type,
            taxId: wallet.user.consumer.taxId,
            tradeName: wallet.user.consumer.companyData?.tradeName,
          },
        };
      } else if (account.externalId && !account) {
        throw new HttpError('Required parameter account', HttpCode.Client.PRECONDITION_FAILED);
      }

      const response = await DictWebService.getInstance()
        .claim()
        .create({
          key,
          keyType,
          type,
          account: { ...account, walletId, ispb: account.ispb || ispb || Config.dict.ispb },
          domainId: domain.id,
          reason,
          instanceId: Config.server.instanceId,
        });

      res.success(response);
    } catch (error) {
      wrapError(error);
    }
  }

  @Post('/register-on-dict', [OAuth.token()])
  public static async createAccountsOnDict(req: bacenRequest, res: BaseResponse) {
    const { ispb, instanceId, ids }: { ispb: string; instanceId: string; ids: string[] } = req.body;

    if (!req.user || req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenRequestError('Only admins can register wallets on Dict API');
    }

    const asset = await Asset.getByIdOrCode('root', { relations: ['issuer'] });

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const depositFeature = provider.feature(CustodyFeature.DEPOSIT);

    let qb = Wallet.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.user', 'user')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('wallet.states', 'wallet.states')
      .where('wallet.deletedAt IS NULL')
      .andWhere('user.deletedAt IS NULL')
      .andWhere('user.role = :role', { role: UserRole.CONSUMER });

    if (ids) {
      qb = qb.andWhere('wallet.id IN (:...ids)', { ids });
    }

    const wallets = await qb.getMany();

    const readyWallets = wallets.filter((wallet) => wallet.status === WalletStatus.READY);

    const dictClient = DictWebService.getInstance();

    const successfullyCreated = [];
    const failed = [];
    for (const wallet of readyWallets) {
      try {
        const sourceAccountInfo: {
          name: string;
          taxId: string;
          bank: {
            ispb: string;
            code: string;
            name: string;
          };
          account: {
            holder: string;
            agency: string;
            number: string;
            type: string;
            status: string;
            createdAt: string;
          };
          asset: string;
        } = await depositFeature.info(wallet);

        await dictClient.account().create({
          instanceId: instanceId ?? Config.server.instanceId,
          account: {
            walletId: wallet.id,
            ispb: ispb ?? wallet.user.domain.settings.ispb,
            type: sourceAccountInfo.account.type as any,
            branch: sourceAccountInfo.account.agency,
            number: sourceAccountInfo.account.number,
            user: {
              name: sourceAccountInfo.name,
              taxId: sourceAccountInfo.taxId,
              type: wallet.user.consumer.type,
            },
            openingDate: wallet.createdAt.toISOString(),
          },
        });

        successfullyCreated.push(wallet.id);
      } catch (error) {
        logger?.warn(error.message);
        failed.push(wallet.id);
      }
    }

    res.success({
      totalCreated: successfullyCreated.length,
      totalFailed: failed.length,
      failed,
    });
  }
}
