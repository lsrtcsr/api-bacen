const path = require('path');

module.exports = {
  rules: {
    '@typescript-eslint/explicit-function-return-type': 0,
    'import/prefer-default-export': 0
  }
};