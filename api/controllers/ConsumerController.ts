import {
  ConsumerStatus,
  CustodyFeature,
  CustodyProvider,
  LegalTermAcceptanceConsumerData,
  LegalTermSchema,
  PaginationData,
  UnleashFlags,
  UserAccountInfoSchema,
  UserRole,
  AssetRegistrationStatus,
  UserStatus,
} from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import * as dotProp from 'dot-prop';
import { Logger } from 'nano-errors';
import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { FindConditions, getManager } from 'typeorm';
import { logger } from '../../config/logger.config';
import Scopes from '../../config/oauth/scopes';
import { Idempotent } from '../decorators';
import { EntityNotFoundError, ForbiddenParameterError, InvalidRequestError } from '../errors';
import { Exists, Limits, OAuth, Params, Permissions, Query, Status } from '../filters';
import { ConsumerStateMachine } from '../fsm';
import {
  AnalysisResult,
  bacenRequest,
  Consumer,
  ConsumerState,
  Domain,
  LegalTerm,
  User,
  ViewConsumerWithStates,
} from '../models';
import {
  ConsumerPipelinePublisher,
  ProviderManagerService,
  MediatorPipelinePublisher,
  PgIdempotenceStore,
  UserService,
} from '../services';
import { extractRangeFromQuery, getPaginationFromReq, setPaginationToRes, stringify } from '../utils';
import { checkRequiredData } from '../utils/ValidationUtil';
import { ConsumerRepository } from '../repositories';

@Controller('/consumers')
export default class ConsumerController {
  @Get('/', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadAllConsumers, Query.pagination])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const { status, email, taxId }: { status?: string; taxId?: string; email?: string } = req.query;
    const pagination = getPaginationFromReq(req);

    let results: User[];
    let paginationData: PaginationData;
    if (status || taxId || email) {
      const where: { status: string; taxId?: string; domainId?: string; email?: string } = { status, taxId, email };

      if (
        req.user.role !== UserRole.ADMIN &&
        req.user.role !== UserRole.OPERATOR &&
        !UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)
      ) {
        where.domainId = req.user.domain.id;
      }

      // Remove undefined keys from where, it will break our query
      Object.keys(where).forEach((key) => where[key] === undefined && delete where[key]);

      [results, paginationData] = await ViewConsumerWithStates.paginatedFindConsumers({
        pagination,
        where,
        relations: ['states', 'domain', 'consumer', 'consumer.states', 'wallets', 'wallets.states'],
      });
    } else {
      let domainCondition: { domain?: FindConditions<Domain>; taxId?: string } = { taxId };
      if (
        req.user.role !== UserRole.ADMIN &&
        req.user.role !== UserRole.OPERATOR &&
        !UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)
      ) {
        domainCondition = { domain: { id: req.user.domain.id } };
      }

      [results, paginationData] = await User.safePaginatedFind({
        pagination,
        where: { role: UserRole.CONSUMER, ...domainCondition },
        relations: ['states', 'domain', 'consumer', 'consumer.states', 'wallets', 'wallets.states'],
      });
    }

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/pending-deletion', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadAllConsumers, Query.pagination])
  public static async findPendingDeletion(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);
    const results = await ViewConsumerWithStates.findPendingConsumers(getManager(), pagination.skip, pagination.limit);
    res.success(results);
  }

  @Get('/by-status-date', [OAuth.token(Scopes.users.READ), Permissions.canUserReadAllConsumers, Query.pagination])
  public static async findConsumerByReadyDate(req: bacenRequest, res: BaseResponse) {
    const ACCEPTED_STATUS_VALUES = [ConsumerStatus.READY, ConsumerStatus.DELETED];
    const range = extractRangeFromQuery(req);
    const { status, pagination } = req.query;

    if (!status) {
      throw new HttpError('Query param status is required', HttpCode.Client.BAD_REQUEST, {
        field: 'status',
        required: true,
        values: ACCEPTED_STATUS_VALUES,
      });
    }

    if (!ACCEPTED_STATUS_VALUES.includes(status)) {
      throw new HttpError('Query param status is invalid', HttpCode.Client.BAD_REQUEST, {
        field: 'status',
        required: true,
        values: ACCEPTED_STATUS_VALUES,
      });
    }

    const consumerRepository = new ConsumerRepository();
    const [users, count] = await consumerRepository.findConsumerByStatusDate({
      status,
      range,
      pagination,
    });

    setPaginationToRes(res, {
      dataLength: count,
      dataLimit: pagination.limit,
      dataSkip: pagination.skip,
    });

    res.success(users);
  }

  @Get('/by-account-branch', [OAuth.token(Scopes.users.READ)])
  public static async getAccountRelated(req: bacenRequest, res: BaseResponse) {
    const { account, branch, provider } = req.query;

    const custodyProvider = ProviderManagerService.getInstance().from(provider);
    const accountFeature = custodyProvider.feature(CustodyFeature.ACCOUNT_INFO);
    const response = await accountFeature.getUserByAccount({ account, branch });
    let user = null;

    if (response.userId) {
      user = await User.findOne({ where: { id: response.userId }, relations: ['consumer'] });
    }

    res.success(user);
  }

  @Get('/:userId', [
    OAuth.token([Scopes.users.READ]),
    Query.isValidId('userId'),
    Exists.Consumer,
    Permissions.canUserReadOneConsumer,
  ])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const user = await User.findConsumerByIdAndPopulate(userId);

    // If user has no consumer associated, should not be accessible in this endpoint
    if (!user?.consumer) {
      throw new HttpError('Consumer not found', HttpCode.Client.NOT_FOUND);
    }

    res.success(user);
  }

  @Get('/:userId/kyc', [
    OAuth.token([Scopes.users.READ]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserReadOneConsumer,
  ])
  public static async getManualVerificationData(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;

    // Getting the consumer database entry
    const consumerUser = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'consumer.states'] });
    const lastKYCCheckReport = await consumerUser.consumer.getLastKYCCheckReport();

    if (!lastKYCCheckReport) {
      throw new EntityNotFoundError('KYC file not found', { user: consumerUser.id });
    }

    const fileContents = await stringify(lastKYCCheckReport);
    res.success(JSON.parse(fileContents));
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/', [OAuth.token([Scopes.users.WRITE]), Limits.consumerLegal])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const {
      password,
      domain,
      extra,
      consumer = {} as any,
      legalTermAcceptanceConsumerData,
      ...payload
    }: User & {
      password?: string;
      consumer: Consumer;
      extra?: any;
      legalTermAcceptanceConsumerData?: LegalTermAcceptanceConsumerData;
    } = req.body;

    let domainInstance: Domain;

    if (req.user && req.user.role === UserRole.ADMIN && domain) {
      // Only admins can choose the domain
      domainInstance = await Domain.safeFindOne({ where: { id: domain.id || (domain as any) } });
    } else if (req.user && req.user.domain) {
      // If the user has a domain, use it
      domainInstance = await Domain.safeFindOne({ where: { id: req.user.domain.id } });
    } else {
      // Else, use the default domain
      domainInstance = await Domain.getDefaultDomain();
    }

    const mediator = await User.safeFindOne({ where: { domain: domainInstance, role: UserRole.MEDIATOR } });

    if (!mediator) {
      throw new InvalidRequestError('Can only create consumers on domains having a mediator', {
        status: 400,
        message: 'Can only create consumers on domains having a mediator',
      });
    }

    if (extra && extra.externalId && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenParameterError(
        'Consumer',
        [extra.externalId],
        'Only admins can create consumers with external ID',
      );
    }

    if (!req.body.consumer || !req.body.consumer.type) {
      throw new InvalidRequestError('Consumer type is required', {
        value: req.body.consumer.type,
      });
    }

    const acceptProviderLegalTerms = UnleashUtil.isEnabled(UnleashFlags.AUTO_ACCEPT_PROVIDER_TERMS_CONSUMER_CREATION);
    const fingerprint = legalTermAcceptanceConsumerData
      ? `${legalTermAcceptanceConsumerData.userAgent}#${legalTermAcceptanceConsumerData.ip}`
      : `${(req as any).useragent.source}#${req.ip}`;

    const userService = UserService.getInstance();
    const user = await userService.prepare({
      password,
      extra,
      payload,
      consumer,
      additionalData: { ...extra },
      acceptProviderLegalTerms,
      fingerprint,
      role: UserRole.CONSUMER,
      domain: domainInstance,
    });

    try {
      // Send consumer to remote async pipeline
      await ConsumerPipelinePublisher.getInstance().send(user);
    } catch (exception) {
      // TODO: We should handle this cases properly
      Logger.getInstance().warn('Could not send consumer to pipeline. ', exception);
    }

    res.success(user.toJSON());
  }

  @Post('/:userId', [
    OAuth.token([Scopes.users.WRITE]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserWriteConsumer,
    Params.isValidUser(true),
  ])
  public static async updateById(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const { retry } = req.query;
    const { extra, ...payload }: Partial<User> & { extra?: any } = req.body;
    const invalidParams = [];

    // TODO: This should be a whitelist, not a blacklist!
    const forbiddenParams = [
      'role', // [Security] Prevent user role elevation
      'status',
      'states',
      'password',
      'twoFactorRequired',
      'twoFactorSecrets',
      'consumer.addresses',
      'consumer.bankings',
      'consumer.documents',
      'consumer.phones',
      'consumer.states',
    ];

    for (const key of forbiddenParams) {
      if (dotProp.get(payload, key)) {
        invalidParams.push(key);
      }
    }

    // Ensure forbidden parameters are not in the request
    if (invalidParams.length) {
      throw new ForbiddenParameterError('Consumer', invalidParams);
    }

    if (payload.domainId || payload.domain) {
      const domainInstance = await Domain.safeFindOne({
        where: { id: payload.domainId ?? ((payload.domain as any) as string) },
      });

      const mediator = await User.safeCount({ where: { domain: domainInstance, role: UserRole.MEDIATOR } });

      if (!mediator) {
        throw new InvalidRequestError('Can only create consumers on domains having a mediator', {
          status: 400,
          message: 'Can only create consumers on domains having a mediator',
        });
      }
    }

    // Update user instance
    const user = await User.updateWithRelation(userId, payload);

    if (user?.wallets?.length) {
      logger.debug('User has wallets to be updated, providers will be notified', {
        user: user.id,
        wallets: user?.wallets,
      });

      const providersAlreadyUpdated = new Set<CustodyProvider>();

      // Update user info in providers
      for (const wallet of user.wallets) {
        if (wallet.additionalData?.isTransitoryAccount) continue;

        const root = wallet.assetRegistrations.find(
          (a) => a.asset.root && a.status !== AssetRegistrationStatus.PENDING,
        );
        const otherAssets = wallet.assetRegistrations.filter(
          (a) => !a.asset.root && a.status !== AssetRegistrationStatus.PENDING,
        );

        const manager = ProviderManagerService.getInstance();
        const rootProvider = root?.asset?.provider ? manager.from(root?.asset?.provider) : undefined;

        if (rootProvider) {
          // Update root asset registration
          logger.debug('Updating user and wallet information in root asset provider', {
            user: user.id,
            wallet: wallet.id,
            asset: root?.asset?.code,
          });

          await rootProvider.update(user, wallet, extra);
          providersAlreadyUpdated.add(root.asset.provider);

          // Update other assets
          await Promise.all(
            otherAssets
              .filter((other) => !!other.asset.provider && !providersAlreadyUpdated.has(other.asset.provider))
              .map(async (other) => {
                const otherProvider = manager.from(other.asset.provider);
                providersAlreadyUpdated.add(other.asset.provider);

                logger.debug('Updating user and wallet information in asset provider', {
                  user: user.id,
                  wallet: wallet.id,
                  asset: root?.asset?.code,
                });

                return otherProvider.update(user, wallet, extra);
              }),
          );
        } else {
          logger.warn("Root provider not available, consumer won't be updated", { user: user.id, wallet: wallet.id });
        }
      }
    } else {
      logger.debug("User has no wallets, providers won't be notified", {
        user: user.id,
      });
    }

    if (retry) {
      await user.removeFailedState();
      if (user.role === UserRole.CONSUMER) await ConsumerPipelinePublisher.getInstance().send(user);
      else if (user.role === UserRole.MEDIATOR) await MediatorPipelinePublisher.getInstance().send(user);
    }

    res.success(user.toJSON());
  }

  @Post('/:userId/block', [
    OAuth.token([Scopes.users.BLOCK]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserWriteConsumer,
  ])
  public static async blockConsumer(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;

    const consumerUser = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'consumer.states'] });

    const consumerFSM = new ConsumerStateMachine(consumerUser);
    await consumerFSM.goTo(ConsumerStatus.BLOCKED);

    const updatedUser = await User.safeFindOne({
      where: { id: userId },
      relations: ['consumer', 'consumer.states', 'states'],
    });

    res.success(updatedUser.toJSON());
  }

  @Post('/:userId/unblock', [
    OAuth.token([Scopes.users.BLOCK]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserWriteConsumer,
  ])
  public static async unblockConsumer(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['domain', 'consumer', 'consumer.states'],
    });

    if (user.consumer?.status !== ConsumerStatus.BLOCKED) {
      throw new InvalidRequestError('Consumer is already unblocked');
    }

    const statuses = await ConsumerState.find({
      where: { consumer: user.consumer.id },
      order: { createdAt: 'DESC' },
    });

    // for Unblock case we need last status before the blocked state
    const validStatusRange = statuses.length - 1;

    let previousState;
    for (let index = 1; index < validStatusRange; index++) {
      // discard current state
      if (statuses[index].status !== ConsumerStatus.KYC_RECHECKING) {
        // exclude intermediary state
        previousState = statuses[index];
        break;
      }
    }

    if (!previousState) {
      throw new EntityNotFoundError('Previous State');
    }

    const consumerFSM = new ConsumerStateMachine(user);
    await consumerFSM.goTo(previousState.status);
    await ConsumerPipelinePublisher.getInstance().send(user);

    const updatedUser = await User.safeFindOne({
      where: { id: userId },
      relations: ['consumer', 'consumer.states', 'states'],
    });

    res.success(updatedUser.toJSON());
  }

  /**
   * @deprecated use DELETE /users/:userId instead
   */
  @Delete('/:userId', [OAuth.token([Scopes.users.DELETE])])
  public static async deleteConsumer(req: bacenRequest, res: BaseResponse) {
    throw new HttpError(
      'Endpoint has been deprecated, use DELETE /users/:userId instead.',
      405, // METHOD NOT ALLOWED
    );
  }

  @Post('/:userId/kyc', [
    OAuth.token([Scopes.users.WRITE]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserWriteConsumer,
    Status.isConsumerInStatus('userId', [ConsumerStatus.MANUAL_VERIFICATION, ConsumerStatus.INVALID_DOCUMENTS]),
  ])
  public static async updateKYCStatus(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const {
      approved,
      reason,
    }: {
      approved: boolean;
      reason: string;
      name?: string;
      taxId?: string;
      motherName?: string;
      address?: string;
      phone?: string;
      birthday?: string;
    } = req.body;

    checkRequiredData(req.body, ['approved', 'reason']);

    const consumerUser = await User.safeFindOne({
      where: { id: userId },
      relations: ['domain', 'consumer', 'consumer.states'],
    });

    const accepted = approved ? AnalysisResult.APPROVED : AnalysisResult.DENIED;

    // Explicitly changing the consumer's state
    const consumerFSM = new ConsumerStateMachine(consumerUser);
    await consumerFSM.goTo(approved ? ConsumerStatus.MANUALLY_APPROVED : ConsumerStatus.REJECTED, {
      reason,
      accepted,
      by: req.user.id,
    });

    // Send user to pipeline
    await ConsumerPipelinePublisher.getInstance().send(consumerUser);

    res.success({
      id: consumerUser.id,
      approvedBy: req.user.id,
      state: consumerUser.consumer.getStates()[0].toJSON(),
    });
  }

  // TODO: Should the consumer be able to see his own pending legal terms ?
  // WARNING: Does not handle user having more than one wallet.
  @Get('/:userId/legal-terms', [OAuth.token(Scopes.users.READ_LEGAL_TERMS), Exists.User])
  public static async getConsumerPendingTerms(req: bacenRequest, res: BaseResponse) {
    const { accepted: _accepted }: { accepted?: 'true' | 'false' } = req.query;
    const accepted = _accepted && ['true', 'false'].includes(_accepted) ? _accepted : undefined;

    const consumerUser = await User.findOne(req.params.userId, { relations: ['consumer', 'domain', 'wallets'] });
    const context = { userId: consumerUser.id, properties: { domainId: consumerUser.domain.id } };

    const skipLegalTerms = UnleashUtil.isEnabled(UnleashFlags.SKIP_LEGAL_TERMS_ACCEPTANCE, context, false);
    if (skipLegalTerms) return res.success([]);

    const primaryWallet = await consumerUser.getPrimaryWallet({ relations: ['assetRegistrations'] });
    const providerTerms = await primaryWallet.getProviderTerms(accepted);
    const activeTerms = await LegalTerm.getActiveTerms();

    const filteredTerms = activeTerms
      .map<LegalTermSchema & { accepted: boolean }>((term) => ({
        ...term.toJSON(),
        accepted: term.hasConsumerAcceptedSync(consumerUser.consumer),
      }))
      .filter((term) => {
        if (accepted === undefined) return true;
        return term.accepted.toString() === accepted;
      }) as LegalTermSchema[];

    res.success(filteredTerms.concat(providerTerms));
  }

  @Post('/:userId/legal-terms/:legalTermId', [
    OAuth.token(Scopes.legals.ACCEPT),
    Exists.User,
    Exists.LegalTerm(),
    Permissions.canUserAcceptLegalTerm,
  ])
  public static async acceptLegalTerms(req: bacenRequest, res: BaseResponse) {
    const { userId, legalTermId } = req.params;
    const { ip = req.ip, userAgent = (req as any).useragent.source }: LegalTermAcceptanceConsumerData = req.body as any;

    const [consumerUser, legalTerm] = await Promise.all([
      User.findOne(userId, { relations: ['domain', 'states', 'consumer', 'consumer.states'] }),
      LegalTerm.findOne(legalTermId, { relations: ['acceptances', 'acceptances.consumer'] }),
    ]);

    const hasUserAcceptedTerm = legalTerm.hasConsumerAcceptedSync(consumerUser.consumer);
    if (hasUserAcceptedTerm)
      throw new HttpError('User has already accepted term', HttpCode.Client.BAD_REQUEST, { userId, legalTermId });

    const result = await getManager().transaction(async (tx) => {
      const acceptance = await legalTerm.accept(consumerUser.consumer, { ip, userAgent }, tx);

      if (legalTerm.provider) {
        const provider = ProviderManagerService.getInstance().from(legalTerm.provider);
        const legalFeature = provider.feature(CustodyFeature.LEGAL);

        const primaryWallet = await consumerUser.getPrimaryWallet({ manager: tx });
        await legalFeature.accept(legalTermId, primaryWallet, { ip, userAgent });
      }

      return acceptance;
    });

    const consumerPipeline = ConsumerPipelinePublisher.getInstance();

    await consumerPipeline.send(consumerUser);

    res.success(result);
  }

  @Get('/:userId/accounts', [OAuth.token([Scopes.users.READ]), Exists.User, Permissions.canUserReadAllConsumers])
  public static async getAccounts(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;

    const { provider: custodyProvider }: { provider: CustodyProvider } = req.query;
    const user = await User.findOne(userId, { relations: ['consumer'] });

    const accountFeature = ProviderManagerService.getInstance()
      .from(custodyProvider)
      .feature(CustodyFeature.ACCOUNT_INFO);
    const accounts = await accountFeature.getByUser({ userId });
    const partners = (user.consumer.companyData?.partners || []).map((partner) => ({
      type: partner.type,
      profile: partner.profile,
      name: partner.name,
      taxId: partner.taxId,
    }));

    const result: UserAccountInfoSchema = {
      accounts,
      partners,
      type: user.consumer.type,
      startDate: user.createdAt,
      endDate: user.deletedAt,
      name: user.name,
    };
    res.success(result);
  }
}
