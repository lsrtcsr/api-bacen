import { Controller, BaseResponse, Get, HttpError, HttpCode, Post } from 'ts-framework';
import { LegalTermDTO, ConsumerStatus, LegalTermType } from '@bacen/base-sdk';
import { bacenRequest, LegalTerm } from '../models';

import { OAuth } from '../filters';
import Scopes from '../../config/oauth/scopes';
import { InvalidParameterError } from '../errors';
import { ConsumerStateMachine } from '../fsm';

@Controller('/legal-terms', [])
export default class LegalTermController {
  @Get('/', [])
  @Get('/:type', [])
  public static async getLegalTermByType(req: bacenRequest, res: BaseResponse) {
    const { type } = req.params;

    const hasType = !!type;
    const hasValidType = type === LegalTermType.TERMS_OF_USE || type === LegalTermType.PRIVACY_POLICY;

    // TODO: Move to filters
    if (hasType && !hasValidType) {
      throw new HttpError(
        `Invalid param, "type" must be one of "${LegalTermType.PRIVACY_POLICY}" or "${LegalTermType.TERMS_OF_USE}"`,
        HttpCode.Client.BAD_REQUEST,
        { type },
      );
    } else if (hasType) {
      // Get currently active legal term by type
      const term = await LegalTerm.getActiveTermByType(type as LegalTermType);
      res.success(term.toJSON());
    } else {
      // Get all active legal terms
      const terms = await LegalTerm.safeFind({ where: { active: true } });
      res.success(terms);
    }
  }

  @Post('/', [OAuth.token(Scopes.legals.WRITE)])
  public static async createLegalTerms(req: bacenRequest, res: BaseResponse) {
    const { requireAcceptance } = req.query;
    let legalTermDTO: LegalTermDTO;

    try {
      legalTermDTO = await LegalTermDTO.build(req.body);
    } catch (errs) {
      throw new InvalidParameterError('LegalTerm', errs);
    }

    const previousActiveTerm = await LegalTerm.getActiveTermByType(legalTermDTO.type);
    const newLegalTerm = await LegalTerm.insertAndFind(legalTermDTO);

    if (requireAcceptance === 'true') {
      // Send all users which had accepted the previously active term back to `pending_legal_acceptance`
      // There is an edge cases around this which need to be better thought out:
      //   - if skip_legal_term was enabled, previously active users would not necessarily have signed the
      //     previously active LegalTerms therefore they would not be sent to pending_legal_acceptance.
      //     perhaps it would be easier to create an endpoint which accepts a list of userIds and sends them all
      //     to pending_legal_acceptance, I'm somewhat hesitant to resort to just sending all users to pending_legal
      //     everytime the legalTerms are updated, I feel this would probably have more edge cases.
      //
      // already loads relations for FSM.
      const acceptingUsers = await previousActiveTerm.getAcceptingConsumers();

      await Promise.all(
        acceptingUsers.map(user => {
          const fsm = new ConsumerStateMachine(user);
          return fsm.goTo(ConsumerStatus.PENDING_LEGAL_ACCEPTANCE);
        }),
      );
    }

    res.success(newLegalTerm);
  }
}
