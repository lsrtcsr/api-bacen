import { InvalidRequestError } from 'oauth2-server';
import { BaseResponse, Controller, Post } from 'ts-framework';
import Config from '../../config';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError } from '../errors';
import { OAuth, Params, Permissions } from '../filters';
import { bacenRequest, OAuthOTPToken, User } from '../models';

@Controller('/two-factor', [])
export default class TwoFactorController {
  /**
   * @description Updates an user logged exclusively to set 2FA
   */
  @Post('/configure', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteUser, Params.isValidUser(true)])
  public static async configureTwoFactor(req: bacenRequest, res: BaseResponse) {
    const userId = req.body.user || req.user.id;

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['twoFactorSecret'],
    });

    if (!user) {
      throw new EntityNotFoundError('User');
    }

    if (user.twoFactorSecret && user.twoFactorSecret.secret) {
      throw new InvalidRequestError('Two factor is already configured');
    }

    // Enables two factor in all user authentication requests
    const otp = await OAuthOTPToken.generate(user);

    // TODO: Enable Google Authenticator with a 2fa "type" param
    // return res.json({ type: 'google-authenticator', qrcodeUrl: otp.qrcodeUrl });

    // For SMS 2fa, don't let the secret get exposed!
    return res.json({ type: 'sms', userId, otpId: otp.id, twoFactorRequired: true });
  }

  /**
   * Disables OTP verification in OAuth 2.0 requests.
   *
   * TODO:
   * - Ensure user has OTP authenticated token
   * - Maybe require a new token confirmation?
   */
  @Post('/disable', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteUser, Params.isValidUser(true)])
  public static async disableTwoFactor(req: bacenRequest, res: BaseResponse) {
    const userId = req.body.user || req.user.id;

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['twoFactorSecret'],
    });

    if (!user) {
      throw new EntityNotFoundError('User');
    } else if (!user.twoFactorSecret) {
      throw new InvalidRequestError('Two factor is not configured for user account');
    }

    // TODO: Should we also remove the current OTP seed? I will keep it for now...
    // Disable two factor in user authentication requests
    await OAuthOTPToken.disable(user);
    return res.json({ twoFactorRequired: false });
  }

  /**
   * Sends an OTP over SMS.
   *
   */
  @Post('/sms', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteUser, Params.isValidUser(true)])
  public static async sendOverSMS(req: bacenRequest, res: BaseResponse) {
    const userId = req.body.user || req.user.id;

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['twoFactorSecret', 'consumer', 'consumer.phones'],
    });

    if (!user) {
      throw new EntityNotFoundError('User');
    } else if (!user.twoFactorSecret || !user.twoFactorSecret.secret) {
      throw new InvalidRequestError('Two factor is not configured for user account');
    } else if (!user.consumer || !user.consumer.phones) {
      throw new InvalidRequestError('User has no available phones for two factor over SMS');
    }

    // Gets default user phone
    let phone = user.consumer.phones.find(phone => phone['default']);
    phone = phone || user.consumer.phones[0];

    if (!phone) {
      throw new InvalidRequestError('User has no available phones for two factor over SMS');
    }

    // Gets current OTP using Two Factor secret instance
    const secret = user.twoFactorSecret;
    const token = await secret.token();

    // Sends token over SMS
    // TODO: Put config in domain for custom prefix
    await phone.sms(`${Config.otp.smsBodyVerificationPrefix}${token}`);

    // TODO: Implement cooldown for token sending
    return res.json({ phone: phone.id });
  }
}
