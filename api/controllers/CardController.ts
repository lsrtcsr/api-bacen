import {
  CardBaseRequestWithPasswordSchema,
  CardBlockRequestSchema,
  CardCancellationRequestSchema,
  CardStatus,
  CardUnblockRequestSchema,
  CustodyFeature,
  UserRole,
  WalletStatus,
  ServiceType,
  ConsumerStatus,
} from '@bacen/base-sdk';
import * as moment from 'moment';
import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { getManager } from 'typeorm';
import Scopes from '../../config/oauth/scopes';
import {
  ForbiddenParameterError,
  ForbiddenRequestError,
  InternalServerError,
  InvalidParameterError,
  EntityNotFoundError,
} from '../errors';
import { Exists, Limits, OAuth, Permissions, Query, Request, Status, Applicable, Assets, Feature } from '../filters';
import { Asset, bacenRequest, Card, Payment, Wallet } from '../models';
import { PgIdempotenceStore, ProviderManagerService } from '../services';
import { ProviderUtil, getPaginationFromReq, setPaginationToRes } from '../utils';
import { Idempotent, ServiceCharged } from '../decorators';
import Config from '../../config';
import { checkIfAssetIsRegisteredOnWallet } from '../filters/assets/IsAssetRegistered';

@Controller('/wallets/:walletId/cards')
export default class CardController {
  @Get('/', [OAuth.token([Scopes.wallets.READ]), Exists.Wallet(), Permissions.canUserReadOneWallet])
  public static async getCards(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Card.safePaginatedFind({
      pagination,
      where: { wallet: { id: walletId } },
      relations: ['wallet'],
    });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:cardId', [
    OAuth.token([Scopes.wallets.READ]),
    Query.isValidId('cardId'),
    Exists.Wallet(),
    Exists.Card,
    Permissions.canUserReadOneWallet,
  ])
  public static async getOneCard(req: bacenRequest, res: BaseResponse) {
    const { walletId, cardId } = req.params;

    // Finds card locally and populate with provider data
    const card = await Card.safeFindOne({ where: { id: cardId, wallet: { id: walletId } } });
    if (!card) {
      throw new EntityNotFoundError('Card');
    }

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${card.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);
    const raw = await cardFeature.findById(card.id);

    res.success({ raw, ...card.toJSON() });
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ServiceCharged(ServiceType.VIRTUAL_CARD_ISSUING)
  @Post('/code', [
    OAuth.token([Scopes.wallets.WRITE]),
    Request.signing(),
    Assets.hasProvider('asset', false),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.params.walletId },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Exists.Wallet(),
    Permissions.canUserWriteWallet,
    Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.params.walletId,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Limits.numberOfCards,
    Applicable.card.cardLength,
  ])
  public static async emitFromCode(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const payload: { unblockCode: string; expirationDate?: string; asset?: string } = req.body;

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    const wallet = await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user', 'user.domain', 'assetRegistrations', 'assetRegistrations.asset', 'assetRegistrations.states'] });
    checkIfAssetIsRegisteredOnWallet(asset, wallet);

    let expirationDate;
    if (payload.expirationDate) {
      expirationDate = new Date(payload.expirationDate);
    } else if (wallet.user && wallet.user.domain && wallet.user.domain.settings.locks.card_expiration_time > 0) {
      expirationDate = moment().add(wallet.user.domain.settings.locks.card_expiration_time, 'month').toDate();
    } else {
      throw new InvalidParameterError('expirationDate');
    }

    const cardId: string = await getManager().transaction(async (transactionalEntityManager) => {
      const cardSchema = Card.create({ wallet, virtual: false, status: CardStatus.BLOCKED });
      const insertResult = await transactionalEntityManager.insert(Card, cardSchema);

      const cardId = insertResult.identifiers[0].id;
      await cardFeature.emitFromCode(cardId, payload.unblockCode, wallet.id);

      return cardId;
    });

    const card = await Card.safeFindOne({ where: { id: cardId } });
    res.success(card.toJSON());
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ServiceCharged(ServiceType.VIRTUAL_CARD_ISSUING)
  @Post('/virtual', [
    OAuth.token([Scopes.wallets.WRITE]),
    Request.signing(),
    Assets.hasProvider('asset', false),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.params.walletId },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Exists.Wallet(),
    Permissions.canUserWriteWallet,
    Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.params.walletId,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Limits.numberOfCards,
  ])
  public static async emitVirtual(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const payload: { asset: string; externalId?: string; expirationDate?: string } = req.body;

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    const wallet = await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user', 'user.domain', 'assetRegistrations', 'assetRegistrations.asset', 'assetRegistrations.states'] });
    checkIfAssetIsRegisteredOnWallet(asset, wallet);

    let expirationDate;
    if (payload.expirationDate) {
      expirationDate = new Date(payload.expirationDate);
    } else if (wallet.user && wallet.user.domain && wallet.user.domain.settings.locks.card_expiration_time > 0) {
      expirationDate = moment().add(wallet.user.domain.settings.locks.card_expiration_time, 'month').toDate();
    } else {
      throw new InvalidParameterError('expirationDate');
    }

    let cardId: string;
    await getManager().transaction(async (transactionalEntityManager) => {
      const cardSchema = Card.create({
        wallet,
        asset: asset.code,
        virtual: true,
        status: CardStatus.BLOCKED,
      });

      const insertResult = await transactionalEntityManager.insert(Card, cardSchema);
      cardId = insertResult.identifiers[0].id;

      await cardFeature.emitVirtual({
        cardId,
        expirationDate,
        walletId,
        externalId: payload.externalId,
      });
    });
    const card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ServiceCharged(ServiceType.PHYSICAL_CARD_ISSUING)
  @Post('/physical', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Assets.hasProvider('asset', false),
    Feature.isPhysicalCardEmissionEnabled,
    Permissions.canUserWriteWallet,
    Status.isConsumerInStatus('walletId', [ConsumerStatus.READY], 'Wallet'),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.params.walletId,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Limits.numberOfCards,
  ])
  public static async emitPhysical(req: bacenRequest, res: BaseResponse) {
    const { walletId } = req.params;
    const { externalId, asset: assetIdOrCode } = req.body as { externalId: string; asset?: string };

    if (externalId && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenParameterError('Card', [externalId], 'Only admins can create cards with external ID');
    }

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(assetIdOrCode || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${assetIdOrCode} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    const wallet = await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user', 'user.domain', 'assetRegistrations', 'assetRegistrations.asset', 'assetRegistrations.states'] });
    checkIfAssetIsRegisteredOnWallet(asset, wallet);

    let cardId: string;
    await getManager().transaction(async (transactionalEntityManager) => {
      const cardSchema = Card.create({
        wallet,
        asset: asset.code,
        virtual: false,
        status: CardStatus.BLOCKED,
      });

      const insertResult = await transactionalEntityManager.insert(Card, cardSchema);
      cardId = insertResult.identifiers[0].id;

      await cardFeature.emitPhysical({
        cardId,
        walletId,
        externalId,
      });
    });
    const card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:cardId/activate', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Exists.Card,
    Assets.hasProvider('asset', false),
    Permissions.canUserWriteWallet,
    Applicable.card.activation,
    Applicable.card.changePassword,
  ])
  public static async activate(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const payload: CardBaseRequestWithPasswordSchema = req.body;

    let card = await Card.safeFindOne({ where: { id: cardId } });

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    try {
      await getManager().transaction(async (transactionalEntityManager) => {
        await cardFeature.activate(cardId, payload);
        await transactionalEntityManager.update<Card>(Card, cardId, {
          status: CardStatus.AVAILABLE,
          activatedAt: moment(),
        });
      });
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('The card activation failed', {
        message: message || 'Unknown Error',
      });
    }
    // Reload card
    card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Post('/:cardId/password', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Exists.Card,
    Assets.hasProvider('asset', false),
    Permissions.canUserWriteWallet,
    Applicable.card.changePassword,
  ])
  public static async changePassword(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const payload: CardBaseRequestWithPasswordSchema = req.body;

    let card = await Card.safeFindOne({ where: { id: cardId } });

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    try {
      await cardFeature.password(cardId, payload);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('The card password change failed', {
        message: message || 'Unknown Error',
      });
    }
    // Reload card
    card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Post('/:cardId/block', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Exists.Card,
    Assets.hasProvider('asset', false),
    Permissions.canUserWriteWallet,
  ])
  public static async block(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const payload: CardBlockRequestSchema = req.body;

    let card = await Card.safeFindOne({ where: { id: cardId } });

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    try {
      await getManager().transaction(async (transactionalEntityManager) => {
        await cardFeature.block(cardId, payload);
        await transactionalEntityManager.update(Card, cardId, { status: CardStatus.BLOCKED });
      });
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('The card blocking failed', {
        message: message || 'Unknown Error',
      });
    }
    // Reload card
    card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Post('/:cardId/cancel', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Exists.Card,
    Assets.hasProvider('asset', false),
    Permissions.canUserWriteWallet,
  ])
  public static async cancel(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const payload: CardCancellationRequestSchema = req.body;

    let card = await Card.safeFindOne({ where: { id: cardId } });

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);

    try {
      await getManager().transaction(async (transactionalEntityManager) => {
        await cardFeature.cancel(cardId, payload);
        await transactionalEntityManager.update<Card>(Card, cardId, {
          status: CardStatus.CANCELLED,
          cancelledAt: moment(),
        });
      });
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('The card blocking failed', {
        message: message || 'Unknown Error',
      });
    }
    // Reload card
    card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Post('/:cardId/unblock', [
    OAuth.token([Scopes.wallets.WRITE]),
    Exists.Wallet(),
    Exists.Card,
    Assets.hasProvider('asset', false),
    Permissions.canUserWriteWallet,
  ])
  public static async unblock(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const payload: CardUnblockRequestSchema = req.body;

    let card = await Card.safeFindOne({ where: { id: cardId } });

    // Get card provider from asset, defaulting to root if not specified
    const asset = await Asset.getByIdOrCode(payload.asset || card.asset || 'root');
    if (!asset) {
      throw new EntityNotFoundError(`Asset with code ${payload.asset} not found`);
    }

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const cardFeature = provider.feature(CustodyFeature.CARD);
    await ProviderUtil.throwIfFeatureNotAvailable(cardFeature);

    try {
      await getManager().transaction(async (transactionalEntityManager) => {
        await cardFeature.unblock(cardId, payload);
        await transactionalEntityManager.update<Card>(Card, cardId, { status: CardStatus.AVAILABLE });
      });
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('The card activation failed', {
        message: message || 'Unknown Error',
      });
    }
    // Reload card
    card = await Card.safeFindOne({ where: { id: cardId } });

    res.success(card.toJSON());
  }

  @Get('/:cardId/payments', [
    OAuth.token([Scopes.payments.READ]),
    Exists.Wallet(),
    Exists.Card,
    Permissions.canUserReadOneWallet,
    Query.pagination,
  ])
  public static async getPayments(req: bacenRequest, res: BaseResponse) {
    const { cardId } = req.params;
    const {
      after = undefined,
      before = undefined,
      skip = 0,
      limit = Config.api.defaultPaginationLimit,
    }: { after: Date; before: Date; skip: number; limit: number } = req.query;

    const filters = { before, after };

    let payments: Payment[];
    let count: number;
    try {
      [payments, count] = await this.findPaymentsByPeriod(cardId, filters, skip, limit);
    } catch (error) {
      const message = error.message || (error.data && error.data.message);
      throw new InternalServerError('Error trying to retrieve payments', {
        message: message || 'Unknown Error',
      });
    }

    res.set('X-Data-Length', count.toString());
    res.set('X-Data-Skip', req.query.skip || '0');
    res.set('X-Data-Limit', limit.toString());

    res.success(payments);
  }

  public static async findPaymentsByPeriod(
    cardId: string,
    filters: any = {},
    skip?: number,
    limit?: number,
  ): Promise<[Payment[], number]> {
    let qb = Payment.createQueryBuilder('payment')
      .innerJoinAndSelect('payment.transaction', 'transaction')
      .innerJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.states', 'states')
      .leftJoinAndSelect('payment.card', 'card')
      .where('card.id = :id', { id: cardId });

    if (filters.after && filters.before) {
      qb = qb.andWhere('transaction.createdAt BETWEEN :after AND :before', {
        after: moment(filters.after).startOf('day').toDate(),
        before: moment(filters.before).endOf('day').toDate(),
      });
    } else if (filters.after) {
      qb = qb.andWhere('transaction.createdAt >= :date', {
        date: moment(filters.after).startOf('day').toDate(),
      });
    } else if (filters.before) {
      qb = qb.andWhere('transaction.createdAt <= :date', {
        date: moment(filters.before).endOf('day').toDate(),
      });
    }

    return qb.skip(skip).take(limit).orderBy('transaction.createdAt', 'DESC').getManyAndCount();
  }
}
