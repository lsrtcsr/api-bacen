import {
  CustodyFeature,
  PaymentType,
  PaymentStatus,
  ConsumerStatus,
  CustodyProvider,
  ServiceType,
} from '@bacen/base-sdk';
import { BaseResponse, Controller, Get, Post, HttpError } from 'ts-framework';
import { getManager } from 'typeorm';
import Scopes from '../../config/oauth/scopes';
import {
  Amount,
  Limits,
  OAuth,
  Permissions,
  PhoneCredits,
  Request,
  PhoneVerification,
  Status,
  Params,
} from '../filters';
import { Asset, bacenRequest, Wallet, Payment } from '../models';
import {
  ProviderManagerService,
  TransactionPipelinePublisher,
  EventHandlingGateway,
  BillingService,
} from '../services';
import { ProviderUtil } from '../utils';
import { ServiceCharged } from '../decorators';
import Config from '../../config';
import { CreateOrderDto, CompleteOrderDto } from '../schemas/dto/phoneCredits';
import * as Serializers from '../serializers';

@Controller('/phone-credits')
export default class PhoneCreditsController {
  @Get('/providers', [
    PhoneCredits.isDisabled,
    OAuth.token([Scopes.phoneCredits.READ_PROVIDERS]),
    Permissions.canUserReadAllProviders,
    Params.isValidProvider('provider', false),
  ])
  public static async getProviders(req: bacenRequest, res: BaseResponse) {
    const { provider: providerCode = Config.provider.defaultMobileRechargeProvider } = req.query;

    let response;
    try {
      const provider = ProviderManagerService.getInstance().from(providerCode);
      const phoneChargeFeature = provider.feature(CustodyFeature.PHONE_CREDITS);
      await ProviderUtil.throwIfFeatureNotAvailable(phoneChargeFeature);
      response = await phoneChargeFeature.getMobileCreditProviders();
    } catch (exception) {
      throw new HttpError('Failed getting phone providers.', exception.response.status, {
        message: exception.response.data.message,
        stackId: exception.response.data.stackId,
        ...exception.response.data.details,
      });
    }

    return res.success(response);
  }

  @Get('/order/history/:walletId', [
    PhoneCredits.isDisabled,
    OAuth.token([Scopes.phoneCredits.READ_PROVIDERS]),
    Permissions.canUserReadAllProviders,
    Params.isValidProvider('provider', false),
  ])
  public static async getOrderHistory(req: bacenRequest, res: BaseResponse) {
    const { provider: providerCode = Config.provider.defaultMobileRechargeProvider } = req.query;

    let response;
    try {
      const provider = ProviderManagerService.getInstance().from(providerCode);
      const phoneChargeFeature = provider.feature(CustodyFeature.PHONE_CREDITS);
      await ProviderUtil.throwIfFeatureNotAvailable(phoneChargeFeature);
      response = await phoneChargeFeature.getOrderHistory(req.params.walletId);
    } catch (exception) {
      throw new HttpError('Failed getting order history.', exception.response.status, {
        message: exception.response.data.message,
        stackId: exception.response.data.stackId,
        ...exception.response.data.details,
      });
    }

    return res.success(response);
  }

  @Post('/order/create', [
    PhoneCredits.isDisabled,
    OAuth.token([Scopes.phoneCredits.CREATE_ORDER]),
    Permissions.canUserCreateOrderProviders,
    PhoneCredits.createOrderDtoValidator,
  ])
  public static async createOrder(req: bacenRequest, res: BaseResponse) {
    const {
      provider = Config.provider.defaultMobileRechargeProvider,
      source,
      phoneCode,
      phoneNumber,
      providerCode,
    } = req.body as CreateOrderDto;

    const providerInstance = ProviderManagerService.getInstance().from(provider);
    const phoneChargeFeature = providerInstance.feature(CustodyFeature.PHONE_CREDITS);
    await ProviderUtil.throwIfFeatureNotAvailable(phoneChargeFeature);
    const response = await phoneChargeFeature.createOrder(source, phoneCode, phoneNumber, providerCode);

    return res.success(response);
  }

  @ServiceCharged(ServiceType.PHONE_CREDITS_PURCHASE)
  @Post('/order/:phoneCreditOrderId/complete', [
    PhoneCredits.isDisabled,
    OAuth.token([Scopes.phoneCredits.COMPLETE_ORDER]),
    Request.signing(),
    Permissions.canUserCompleteOrderProviders,
    PhoneCredits.completeOrderDtoValidator,
    Status.isConsumerInStatus('source', [ConsumerStatus.READY], 'Wallet'),
    Amount.isValidAmount,
    Params.isValidProvider('provider', false),
    Limits.phoneCreditAmount,
    Limits.transactionAmount,
    PhoneVerification.checkPhoneVerificationStatus,
  ])
  public static async completeOrder(req: bacenRequest, res: BaseResponse) {
    const {
      amount,
      source,
      provider: providerCode = Config.provider.defaultMobileRechargeProvider,
    } = req.body as CompleteOrderDto;
    const { phoneCreditOrderId } = req.params;

    const wallet: Wallet = await Wallet.safeFindOne({
      where: { id: source },
      relations: ['assetRegistrations', 'assetRegistrations.states', 'assetRegistrations.asset'],
    });

    const asset = await Asset.getByProvider(providerCode as CustodyProvider);

    const transaction = await getManager().transaction(async (manager) => {
      let trx = await asset.destroy({
        manager,
        amount,
        createdBy: req.accessToken,
        targetWallet: wallet,
        paymentType: PaymentType.PHONE_CREDIT,
        additionalData: {
          orderId: phoneCreditOrderId,
        },
      });
      const payment = await manager.findOne(Payment, {
        where: { id: trx.payments[0].id },
        relations: ['transaction', 'transaction.source'],
      });

      const provider = ProviderManagerService.getInstance().from(providerCode);
      const phoneChargeFeature = provider.feature(CustodyFeature.PHONE_CREDITS);
      await ProviderUtil.throwIfFeatureNotAvailable(phoneChargeFeature);
      const response = await phoneChargeFeature.completeOrder(payment, phoneCreditOrderId);

      if (response.id === payment.id && response.status === PaymentStatus.SETTLED) {
        await manager.update(Payment, payment.id, { status: PaymentStatus.SETTLED });
      }

      if (req.serviceFee) {
        trx = await BillingService.getInstance().chargeServiceFee(req.serviceFee, trx, manager);
      }

      return trx;
    });
    await TransactionPipelinePublisher.getInstance().send(transaction);

    await EventHandlingGateway.getInstance().process(ServiceType.PHONE_CREDITS_PURCHASE, transaction);

    return res.success(Serializers.transactionSerializer(transaction));
  }
}
