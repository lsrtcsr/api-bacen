import { OAuthClientPlatform } from '@bacen/base-sdk';
import * as Bluebird from 'bluebird';
import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { OAuth, Query } from '../filters';
import { bacenRequest, Domain, OAuthClient } from '../models';
import { isUUID, getPaginationFromReq, setPaginationToRes } from '../utils';
import Scopes from '../../config/oauth/scopes';

@Controller('/oauth/clients', [])
export default class OAuthClientController {
  @Get('/', [OAuth.token(Scopes.oAuthClients.READ), Query.pagination])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await OAuthClient.safePaginatedFind({ pagination });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id', [OAuth.token(Scopes.oAuthClients.READ), OAuth.url])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    let found: OAuthClient;
    const isId = id && isUUID(id);

    if (isId) {
      found = await OAuthClient.safeFindOne({ where: { id } });
    } else {
      found = await OAuthClient.safeFindOne({ where: { clientId: id } });
    }

    if (found) {
      return res.success(found.toJSON());
    }

    throw new HttpError('OAuth client not found', HttpCode.Client.NOT_FOUND);
  }

  @Post('/', [OAuth.token(Scopes.oAuthClients.WRITE)])
  public static async insert(req: bacenRequest, res: BaseResponse) {
    const payload: OAuthClient = req.body;
    let domain: Domain;

    // Prevent root client creation, it should come in config seed
    if (payload.platform === OAuthClientPlatform.ROOT) {
      throw new HttpError(
        'Root oauth clients should be created during server initialization',
        HttpCode.Client.BAD_REQUEST,
      );
    }

    // Ensure oauth client id is not being used
    const count = await OAuthClient.count({ where: { clientId: payload.clientId } });

    if (count > 0) {
      throw new HttpError('Client ID not available, please try another one', HttpCode.Client.BAD_REQUEST);
    }

    // Prepare oauth client domain reference, if present
    if (payload.domain) {
      domain = await Domain.findOne(payload.domain);

      if (!domain) {
        throw new HttpError('Domain was not found', HttpCode.Client.NOT_FOUND, {
          domain: payload.domain,
        });
      }
    }

    const client = await OAuthClient.insertAndFind(payload);

    res.success({
      ...client.toJSON(),
      clientSecret: payload.clientSecret,
    });
  }

  @Delete('/:id', [OAuth.token(Scopes.oAuthClients.DELETE)])
  public static async deleteById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { force }: { force?: string } = req.query;

    let deleted;
    const item = await OAuthClient.safeFindOne({ where: { id } });

    if (!item) {
      throw new HttpError('OAuth Client not found', HttpCode.Client.NOT_FOUND);
    }

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await OAuthClient.delete(id).then(_ => true);
    }

    deleted = await OAuthClient.softDelete(id).then(_ => true);
    res.success(deleted);
  }
}
