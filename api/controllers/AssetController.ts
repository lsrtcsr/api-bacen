import {
  AssetRegistrationState,
  AssetRegistrationStatus,
  PaymentStatus,
  UserRole,
  WalletStatus,
  Arrangement,
  TransactionStatus,
} from '@bacen/base-sdk';
import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { getManager } from 'typeorm';
import { StellarService } from '@bacen/stellar-service';
import * as currency from 'currency.js';
import Scopes from '../../config/oauth/scopes';
import { OfflineServiceCharged } from '../decorators';
import { EntityNotFoundError, ForbiddenRequestError, InvalidRequestError, InvalidParameterError } from '../errors';
import {
  Amount,
  OAuth,
  Params,
  Payments,
  Permissions,
  Query,
  Request,
  Status,
  Exists,
  Assets,
  AssetRegistration as AssetRegistrationFilter,
} from '../filters';
import { TransactionStateMachine } from '../fsm';
import { Asset, AssetRegistration, bacenRequest, Domain, Wallet } from '../models';
import { AssetRegistrationService, TransactionPipelinePublisher } from '../services';
import { getPaginationFromReq, isUUID, setPaginationToRes } from '../utils';
import * as Serializers from '../serializers';

@Controller('/assets', [])
export default class AssetController {
  @Get('/', [OAuth.token([Scopes.assets.READ]), Query.pagination, Permissions.canUserReadAllAssets])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Asset.safePaginatedFind({ pagination });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id', [OAuth.token([Scopes.assets.READ]), Permissions.canUserReadOneAsset])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    let found: Asset;
    const isId = id && isUUID(id);

    if (isId) {
      found = await Asset.safeFindOne({ where: { id } });
    } else if (id === 'root') {
      found = await Asset.getRootAsset();
    } else {
      found = await Asset.createQueryBuilder('asset')
        .leftJoinAndSelect('asset.issuer', 'issuer')
        .where('asset.code = :code', { code: id.toUpperCase() })
        .getOne();
    }

    if (found) {
      return res.success(found.toJSON());
    }

    throw new EntityNotFoundError('Asset');
  }

  @Post('/', [OAuth.token([Scopes.assets.WRITE]), Permissions.canUserWriteAsset, Params.isValidAsset])
  static async createAsset(req: bacenRequest, res: BaseResponse) {
    const { name, domain, code } = req.body;

    let domainInstance: Domain;

    if (req.user && req.user.role === UserRole.ADMIN && domain) {
      // Only admins can choose the domain
      // It may be a Domain or a string
      domainInstance = await Domain.findOne(domain.id || ((domain as any) as string));
    } else if (req.user && req.user.domain) {
      // If the user has a domain, use it
      domainInstance = await Domain.findOne(req.user.domain.id);
    }

    const rootWallet = await Wallet.getRootWallet();
    const wallet = await Wallet.getMediatorWallet(domainInstance);

    if (!wallet || !wallet.stellar) {
      throw new InvalidRequestError('Mediator wallet is not available for creating the trustline');
    }

    // This is important, we have to protect internal operational codes
    if (code === 'root') throw new InvalidRequestError('Asset cannot be named "root"');

    const exists = await Asset.findOne({ where: { code } });
    if (exists) throw new InvalidRequestError(`Asset with code ${code} already exists`);

    let asset: Asset;
    await getManager().transaction(async transactionalEntityManager => {
      const assetSchema = Asset.create({ name, code, issuer: rootWallet.id as any });
      await assetSchema.validate();

      const assetInsertResult = await transactionalEntityManager.insert(Asset, assetSchema);
      const assetId = assetInsertResult.identifiers[0].id;
      asset = await transactionalEntityManager.findOne(Asset, assetId, { relations: ['issuer'] });

      const assetRegistration = await AssetRegistration.insertAndFind(
        { wallet, asset },
        { manager: transactionalEntityManager },
      );

      const rootWalletAssetRegistration = await AssetRegistration.insertAndFind(
        { asset, wallet: rootWallet },
        { manager: transactionalEntityManager },
      );

      await transactionalEntityManager.insert(AssetRegistrationState, {
        assetRegistration: assetRegistration.id as any,
        status: AssetRegistrationStatus.PENDING,
      });

      await transactionalEntityManager.insert(AssetRegistrationState, {
        assetRegistration: rootWalletAssetRegistration as any,
        status: AssetRegistrationStatus.READY,
      });

      await wallet.registerAsset(asset, { manager: transactionalEntityManager });

      await transactionalEntityManager.insert(AssetRegistrationState, {
        assetRegistration: assetRegistration.id as any,
        status: AssetRegistrationStatus.READY,
      });
    });

    res.success(asset.toJSON());
  }

  @Post('/:id/register', [
    OAuth.token([Scopes.wallets.WRITE]),
    Request.signing(),
    Permissions.canUserRegisterWalletOnAsset,
    Exists.Wallet({
      walletKeyFn: req => req.body.wallet,
      cache: true,
      relations: [
        'user',
        'user.domain',
        'user.consumer',
        'user.consumer.phones',
        'user.consumer.addresses',
        'states',
        'assetRegistrations',
        'assetRegistrations.asset',
        'assetRegistrations.states',
      ],
    }),
    Status.isWalletInStatus({
      walletIdFn: req => req.body.wallet,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Exists.Asset({ cache: true, assetKeyFn: req => req.params.id }),
    AssetRegistrationFilter.CheckIsDuplicated(),
  ])
  static async registerAsset(req: bacenRequest, res: BaseResponse) {
    const { id: assetIdOrCode } = req.params;
    const { wallet: walletId } = req.body;

    const asset = req.cache.get<Asset>(`asset_${assetIdOrCode}`)!;
    const wallet = req.cache.get<Wallet>(`wallet_${walletId}`)!;

    const assetRegistrationService = AssetRegistrationService.getInstance();

    const assetRegistration = await getManager().transaction(async manager => {
      const assetRegistration = await assetRegistrationService.create(wallet, asset, manager);
      const events = await assetRegistrationService.createReport(assetRegistration, manager);

      Promise.all(events.map(e => e.publish()));

      return assetRegistration;
    });

    res.success(assetRegistration);
  }

  @OfflineServiceCharged()
  @Post('/:id/emit', [
    OAuth.token([Scopes.assets.EMIT]),
    Request.signing(),
    Permissions.canUserDistributeAsset,
    Exists.Asset({ cache: true, assetKeyFn: req => req.params.id }),
    Exists.Wallet({
      walletKeyFn: req => req.body.destination,
      cache: true,
      relations: [
        'user',
        'user.domain',
        'states',
        'assetRegistrations',
        'assetRegistrations.asset',
        'assetRegistrations.states',
      ],
    }),
    Amount.isValidAmount,
    Status.isWalletInStatus({
      walletIdFn: req => req.body.destination,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
      allowMissingWallet: true,
    }),
    Assets.IsAssetRegistered({ walletIdFn: req => req.body.destination, assetKeyFn: req => req.params.id }),
  ])
  static async emitAssets(req: bacenRequest, res: BaseResponse) {
    const { id: assetId } = req.params;
    const { amount, paymentType, destination, status, additionalData, bypassReversalCheck } = req.body;

    // Emit to destination or use mediator as default
    const domain = req.user.domain ? req.user.domain : await Domain.getDefaultDomain();
    const destinationWallet =
      req.cache.get<Wallet>(`wallet_${destination}`) || (await Wallet.getMediatorWallet(domain));

    if (!destinationWallet) {
      throw new EntityNotFoundError('Destination Wallet');
    }

    const asset = req.cache.get<Asset>(`asset_${assetId}`);

    // Check user can emit or destroy this asset
    if (!asset.emittable && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenRequestError('Only root users can emit root assets, contact the support for more information');
    }

    const transaction = await getManager().transaction(async manager => {
      return asset.emit({
        manager,
        amount,
        paymentType,
        additionalData,
        bypassReversalCheck,
        status: status || PaymentStatus.SETTLED,
        createdBy: req.accessToken,
        targetWallet: destinationWallet,
      });
    });

    if (asset.provider) {
      const transactionFSM = new TransactionStateMachine(transaction);
      await transactionFSM.goTo(TransactionStatus.ACCEPTED);
    }

    await TransactionPipelinePublisher.getInstance().send(transaction);

    res.success(Serializers.transactionSerializer(transaction));
  }

  @OfflineServiceCharged()
  @Post('/:id/destroy', [
    OAuth.token([Scopes.assets.DESTROY]),
    Permissions.canUserDistributeAsset,
    Request.signing(),
    Exists.Asset({ cache: true, assetKeyFn: req => req.params.id }),
    Exists.Wallet({
      walletKeyFn: req => req.body.source,
      cache: true,
      allowFailure: true,
      relations: [
        'user',
        'user.domain',
        'states',
        'assetRegistrations',
        'assetRegistrations.asset',
        'assetRegistrations.states',
      ],
    }),
    Amount.isValidAmount,
    Status.isWalletInStatus({
      walletIdFn: req => req.body.source,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Assets.IsAssetRegistered({
      walletIdFn: req => req.body.source,
      assetKeyFn: req => req.params.id,
    }),
    Payments.hasUserEnoughBalance(),
  ])
  static async destroyAssets(req: bacenRequest, res: BaseResponse) {
    const { id: assetId } = req.params;
    const { amount, paymentType, source, status, additionalData, bypassReversalCheck } = req.body;

    // Emit to source or use mediator as default
    const domain = req.user.domain ? req.user.domain : await Domain.getDefaultDomain();
    const sourceWallet = req.cache.get<Wallet>(`wallet_${source}`) || (await Wallet.getMediatorWallet(domain));

    if (!sourceWallet) {
      throw new EntityNotFoundError('Source wallet');
    }

    const asset = req.cache.get<Asset>(`asset_${assetId}`);

    // Check user can emit or destroy this asset
    if (!asset.emittable && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenRequestError('Only root users can emit root assets, for more info contact the support');
    }

    const transaction = await getManager().transaction(async manager => {
      return asset.destroy({
        manager,
        amount,
        paymentType,
        additionalData,
        bypassReversalCheck,
        status: status || PaymentStatus.SETTLED,
        createdBy: req.accessToken,
        targetWallet: sourceWallet,
      });
    });

    // Send consumer to remote async pipeline to create the stellar wallet
    await TransactionPipelinePublisher.getInstance().send(transaction);
    res.success(Serializers.transactionSerializer(transaction));
  }

  @Get('/:id/balance', [OAuth.token([Scopes.wallets.READ]), Permissions.canUserReadAllWallets])
  public static async getBalanceByAsset(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const {
      arrangement,
      type = 'available',
      includes = [],
      excludes = [],
      domains = [],
    }: {
      arrangement?: Arrangement;
      type?: 'available' | 'blocked';
      includes?: string[];
      excludes?: string[];
      domains?: string[];
    } = req.query;

    const asset = await Asset.getByIdOrCode(id, { relations: ['issuer'] });

    if (!asset) throw new InvalidParameterError('assetId');

    const currentUser = req.user;
    const includedDomains = currentUser.role === UserRole.MEDIATOR ? [currentUser.domain.id] : domains;

    let walletFilterSupplied = true;

    let qb = Wallet.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.user', 'user')
      .leftJoinAndSelect('user.domain', 'domain');

    if (includes.length > 0) {
      qb = qb.where('wallet.id IN (:...includes)', { includes });
    } else if (includedDomains.length > 0) {
      qb = qb.where('domain.id IN (:...includedDomains)', { includedDomains });
    } else {
      walletFilterSupplied = false;
    }

    let wallets = await qb.getMany();

    if (wallets && wallets.length > 0 && excludes.length > 0) {
      wallets = wallets.filter(wallet => !excludes.includes(wallet.id));
    }

    let balance: string;
    if (type === 'blocked') {
      const blockedBalance = await Wallet.getBlockedBalanceByAsset({
        arrangement,
        asset,
        wallets: walletFilterSupplied && wallets && wallets.length > 0 ? wallets.map(wallet => wallet.id) : undefined,
      });
      balance = blockedBalance.toString();
    } else if (type === 'available') {
      if (!wallets || !wallets.length) {
        throw new InvalidRequestError('Source wallet not found for supplied parameters');
      }

      const service = StellarService.getInstance();
      const balancePromises = wallets.map(wallet => wallet.getAssetBalance(asset.code, service));
      const balances = await Promise.all(balancePromises);
      const availableBalance = balances
        .map(balance => currency(balance))
        .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }));

      balance = availableBalance.toString();
    } else {
      throw new InvalidParameterError('type');
    }

    return res.success({ balance });
  }
}
