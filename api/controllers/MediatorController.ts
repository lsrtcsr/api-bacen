import { LegalTermAcceptanceConsumerData, UnleashFlags, UserRole } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseResponse, Controller, HttpCode, HttpError, Post } from 'ts-framework';
import Scopes from '../../config/oauth/scopes';
import { ForbiddenParameterError } from '../errors';
import { Exists, OAuth, Params, Permissions } from '../filters';
import { bacenRequest, Domain, User } from '../models';
import { MediatorPipelinePublisher, UserService } from '../services';

@Controller('/mediators', [])
export default class MediatorController {
  @Post('/', [OAuth.token([Scopes.users.WRITE]), Exists.Domain, Permissions.canUserWriteUser])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const {
      password,
      domain,
      extra,
      consumer = {} as any,
      legalTermAcceptanceConsumerData,
      ...payload
    }: User & {
      password?: string;
      extra?: any;
      legalTermAcceptanceConsumerData?: LegalTermAcceptanceConsumerData;
    } = req.body;

    let domainInstance: Domain;

    if (req.user && req.user.role === UserRole.ADMIN && domain) {
      // Only admins can choose the domain
      domainInstance = await Domain.safeFindOne({ where: { id: domain.id || (domain as any) } });
    } else if (req.user && req.user.domain) {
      // If the user has a domain, use it
      domainInstance = await Domain.safeFindOne({ where: { id: req.user.domain.id } });
    } else {
      // Else, use the default domain
      domainInstance = await Domain.getDefaultDomain();
    }

    if (extra && extra.externalId && req.user.role !== UserRole.ADMIN) {
      throw new ForbiddenParameterError(
        'Mediator',
        [extra.externalId],
        'Only admins can create consumers with external ID',
      );
    }

    if (!req.user || !req.user.role || req.user.role !== UserRole.ADMIN) {
      throw new HttpError('Only admin users can create mediators', HttpCode.Client.FORBIDDEN);
    }

    // ignores the role parameter, if sent
    payload.role = UserRole.MEDIATOR;

    const acceptProviderLegalTerms = UnleashUtil.isEnabled(UnleashFlags.AUTO_ACCEPT_PROVIDER_TERMS_CONSUMER_CREATION);
    const fingerprint = legalTermAcceptanceConsumerData
      ? `${legalTermAcceptanceConsumerData.userAgent}#${legalTermAcceptanceConsumerData.ip}`
      : `${(req as any).useragent.source}#${req.ip}`;

    const userService = UserService.getInstance();
    const user = await userService.prepare({
      password,
      extra,
      payload,
      consumer,
      acceptProviderLegalTerms,
      fingerprint,
      role: UserRole.MEDIATOR,
      domain: domainInstance,
    });

    // Send mediator to remote async pipeline
    await MediatorPipelinePublisher.getInstance().send(user);
    res.success(user.toJSON());
  }
}
