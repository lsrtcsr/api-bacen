import {
  IssueCategory,
  IssueDetails,
  IssueType,
  SeverityLevel,
  UnleashFlags,
  UserRole,
  ServiceType,
} from '@bacen/base-sdk';
import * as moment from 'moment';
import * as Package from 'pjson';
import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { bacenRequest, Event, Payment, User } from '../models';
import { BillingService, ContractService, EventPipelinePublisher, IssueHandler } from '../services';

@Controller('/billing')
export default class BillingController {
  // TODO check API Key from request header
  @Post('/', [])
  public static async createEntry(req: bacenRequest, res: BaseResponse) {
    const data: {
      userId: string;
      type: ServiceType;
      event?: any;
      eventDate?: moment.Moment;
      payment?: Payment;
      settled?: boolean;
      liability?: 'consumer' | 'mediator';
    } = req.body;

    const event = await Event.insertAndFind({ data });
    await EventPipelinePublisher.getInstance().send(event);

    res.success(event);
  }

  @Get('/users/:userId/type/:serviceType/fee', [])
  public static async getServiceFee(req: bacenRequest, res: BaseResponse) {
    const { userId, serviceType } = req.params;
    const { provider, transactionAmount }: { provider: string; transactionAmount: string } = req.query;

    const user = await User.safeFindOne({ where: { id: userId }, relations: ['domain'] });

    // Prepare feature flag context
    let forceBilling = false;
    const context = { userId: user.id, properties: { domainId: user.domain.id } };
    if (user.role === UserRole.CONSUMER) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_CONSUMER_BILLING, context, false);
    } else if (user.role === UserRole.MEDIATOR) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false);
    }

    const currentContract = await ContractService.getInstance().getCurrentContract(userId);

    if (!forceBilling && !currentContract) return res.success();

    if (forceBilling && !currentContract) {
      await IssueHandler.getInstance().handle({
        type: IssueType.NOT_FOUND,
        category: IssueCategory.BILLING_PLAN_SUBSCRIPTION,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `error when trying to calculate service ${serviceType} fee for the user ${user.id}`,
        details: IssueDetails.from(user),
      });
      return res.success();
    }

    const serviceFee = await BillingService.getInstance().getServiceFee({
      provider,
      transactionAmount,
      serviceType: serviceType as ServiceType,
      user: currentContract.contractor,
      contract: currentContract,
    });

    let response: any;
    if (serviceFee && Number(serviceFee.amount) !== 0) {
      const entryType = await currentContract.plan.getChargeSettingsByService(serviceType as ServiceType, provider);

      response = {
        liability: entryType.settings.liability,
        ...serviceFee,
      };
    }

    res.success(response);
  }
}
