import { DocumentSide, DocumentType, UserRole } from '@bacen/base-sdk';
import IdentityService from '@bacen/identity-sdk';
import { BaseResponse, Controller, Delete, Get, Post } from 'ts-framework';
import { getManager, IsNull } from 'typeorm';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError } from '../errors';
import { Exists, OAuth, Params, Permissions, Query } from '../filters';
import { bacenRequest, Consumer, Document, User } from '../models';
import { DocumentRepository } from '../repositories';
import { getPaginationFromReq, isUUID, setPaginationToRes } from '../utils';

@Controller('/consumers/:userId/documents', [Exists.User])
export default class DocumentController {
  @Get('/', [OAuth.token(Scopes.users.READ_DOCUMENTS), Permissions.canUserReadOneConsumer])
  public static async findAllDocuments(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);
    const requestUser: User = req.user;

    const user = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'domain'] });

    let results: Document[];
    let count: number;
    if (requestUser.role === UserRole.OPERATOR || requestUser.role === UserRole.ADMIN) {
      const resultsPromise = Document.find({
        where: { consumer: user.consumer.id, deletedAt: IsNull() },
        skip: pagination.skip,
        take: pagination.limit,
      });
      const countPromise = Document.count({
        where: { consumer: user.consumer.id, deletedAt: IsNull() },
      });

      [results, count] = await Promise.all([resultsPromise, countPromise]);
    } else {
      const resultsPromise = Document.find({
        where: { consumer: user.consumer.id, deletedAt: IsNull(), isActive: true },
        skip: pagination.skip,
        take: pagination.limit,
      });
      const countPromise = Document.count({
        where: { consumer: user.consumer.id, deletedAt: IsNull(), isActive: true },
      });

      [results, count] = await Promise.all([resultsPromise, countPromise]);
    }

    setPaginationToRes(res, { dataLength: count, dataLimit: pagination.limit, dataSkip: pagination.skip });

    res.success((results as Document[]).map((r) => r.toSimpleJSON()));
  }

  @Get('/:documentIdOrType', [OAuth.token(Scopes.users.READ_DOCUMENTS), Permissions.canUserReadOneConsumer])
  public static async findOneDocumentById(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId, documentIdOrType } = req.params;

    let documentQuery = Document.createQueryBuilder('document')
      .leftJoinAndSelect('document.consumer', 'consumer')
      .leftJoinAndSelect('consumer.user', 'user')
      .leftJoinAndSelect('document.states', 'document_states')
      .where('user.id = :userId', { userId })
      .orderBy('"isActive"', 'DESC')
      .andWhere('document.deletedAt IS NULL');

    if (isUUID(documentIdOrType)) {
      // Is Id
      documentQuery = documentQuery.andWhere('document.id = :documentId', { documentId: documentIdOrType });
    } else {
      // Is type
      if (!Object.values(DocumentType).includes(documentIdOrType as DocumentType)) {
        throw new EntityNotFoundError('Document typeOrId');
      }

      documentQuery = documentQuery.andWhere('document.type = :documentType', { documentType: documentIdOrType });
    }

    const document = await documentQuery.getOne();

    if (!document) throw new EntityNotFoundError('Document', { userId, documentIdOrType });

    res.success(document.toJSON());
  }

  @Get('/:documentIdOrType/sides', [
    OAuth.token([Scopes.users.READ_DOCUMENTS]),
    Permissions.canUserReadOneConsumer,
    Exists.Document,
  ])
  public static async getExistingSides(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId, documentIdOrType } = req.params;

    let documentQuery = Document.createQueryBuilder('document')
      .leftJoinAndSelect('document.consumer', 'consumer')
      .leftJoinAndSelect('consumer.user', 'user')
      .leftJoinAndSelect('document.states', 'document_states')
      .where('user.id = :userId', { userId })
      .orderBy('"isActive"', 'DESC')
      .andWhere('document.deletedAt IS NULL');

    if (isUUID(documentIdOrType)) {
      // Is Id
      documentQuery = documentQuery.andWhere('document.id = :documentId', { documentId: documentIdOrType });
    } else {
      // Is type
      if (!Object.values(DocumentType).includes(documentIdOrType as DocumentType)) {
        throw new EntityNotFoundError('Document typeOrId');
      }

      documentQuery = documentQuery.andWhere('document.type = :documentType', { documentType: documentIdOrType });
    }

    const document = await documentQuery.getOne();

    if (!document) throw new EntityNotFoundError('Document', { userId, documentIdOrType });

    const sides = await document.existingSides();

    res.success({ sides });
  }

  @Get('/:documentId/metadata', [
    OAuth.token([Scopes.users.READ_DOCUMENTS]),
    Query.isValidId('documentId'),
    Permissions.canUserReadOneConsumer,
    Exists.User,
    Exists.Document,
  ])
  public static async getExistingMetadata(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId, documentId } = req.params;

    const user = await User.findOne({ where: { id: userId }, relations: ['consumer'] });
    const document = await Document.findOne({
      where: { id: documentId, consumer: user.consumer.id },
      relations: ['states', 'consumer'],
    });
    res.success(document.metadata);
  }

  @Post('/', [
    OAuth.token(Scopes.users.WRITE_DOCUMENTS),
    Permissions.canUserWriteConsumer,
    Exists.UserHasConsumer({
      userIdParam: 'userId',
      relations: ['user', 'user.wallets', 'user.wallets.assetRegistrations'],
    }),
    Params.isValidDocument,
  ])
  public static async createDocument(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId } = req.params;

    const consumer = req.cache.get<Consumer>(`consumer_${userId}`)!;
    const identityDocumentService = IdentityService.getInstance().documents();

    const document = await getManager().transaction(async (manager) => {
      const repository = new DocumentRepository(manager);
      const document = await repository.upsertDocument(consumer, req.body);
      const { type, number, issuer, issuerState, expiresAt: expirationDate } = document;
      const payload = { type, number, issuer, issuerState, expirationDate };

      const reportIds = consumer.user.wallets
        .flatMap((wallet) => wallet.assetRegistrations.map((ar) => ar.reportId))
        .filter((id) => Boolean(id));
      const { documents: identityIds } = await identityDocumentService.bulkCreateDocuments(reportIds, payload);

      await repository.setDocumentIdentityIds(document, identityIds);
      return document;
    });

    res.success(document);
  }

  @Post('/:type/:side', [
    OAuth.token([Scopes.users.WRITE_DOCUMENTS]),
    Permissions.canUserWriteConsumer,
    Exists.UserHasConsumer(),
  ])
  public static async createOrUpdateDocument(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { userId, type, side } = req.params as { userId: string; type: DocumentType; side: DocumentSide };
    const { file } = req;

    const identityDocumentService = IdentityService.getInstance().documents();
    const consumer = req.cache.get<Consumer>(`consumer_${userId}`)!;

    const document = await getManager().transaction(async (manager) => {
      const repository = new DocumentRepository(manager);
      const document = await repository.findDocumentByConsumerAndType(consumer, type);

      if (!document) {
        throw new EntityNotFoundError('document');
      }

      await identityDocumentService.bulkUploadDocumentsFile(
        document.identityIds,
        {
          buffer: file.buffer,
          filename: file.originalname,
          contentType: file.mimetype,
        },
        side,
      );
      return document;
    });

    res.success(document);
  }

  @Delete('/:documentId', [
    OAuth.token(Scopes.users.WRITE_DOCUMENTS),
    Query.isValidId('documentId'),
    Permissions.canUserWriteConsumer,
    Exists.Document,
  ])
  public static async deleteDocument(req: bacenRequest, res: BaseResponse): Promise<void> {
    const { documentId } = req.params;
    const { force }: { force?: string } = req.query;

    let deleted: boolean;

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Document.delete(documentId).then((_) => true);
    } else {
      deleted = await Document.softDelete(documentId).then((_) => true);
    }

    res.success(deleted);
  }
}
