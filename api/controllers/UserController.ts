import { ConsumerStatus, UserRole, WalletStatus } from '@bacen/base-sdk';
import { CreateUserWithoutConsumerDto } from '@bacen/ -sdk';
import * as dotProp from 'dot-prop';
import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post, Put } from 'ts-framework';
import { getManager, Not } from 'typeorm';
import Config from '../../config';
import { logger } from '../../config/logger.config';
import Scopes from '../../config/oauth/scopes';
import {
  EntityNotFoundError,
  ForbiddenParameterError,
  ForbiddenRequestError,
  UnauthorizedRequestError,
} from '../errors';
import { Exists, OAuth, Params, Permissions, Query, Request, UserFilter } from '../filters';
import { ConsumerDeleteReason, ConsumerStateMachine } from '../fsm';
import {
  bacenRequest,
  Domain,
  FindConditions,
  Invoice,
  OAuthAccessToken,
  OAuthSecretToken,
  User,
  Wallet,
} from '../models';
import {
  EmailService,
  EmailTemplateService,
  EmailTemplateType,
  ProviderManagerService,
  UserService,
} from '../services';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/users', [])
export default class UserController {
  @Get('/', [OAuth.token([Scopes.users.READ]), Query.pagination, Permissions.canUserReadAllUsers])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const { role, domain, email }: { role?: UserRole; domain?: string; email?: string } = req.query;
    const pagination = getPaginationFromReq(req);
    const where: FindConditions<User> = {};

    if (role) {
      where.role = role;
    }

    if (email) {
      where.email = email;
    }

    if (domain) {
      where.domain = { id: domain };
    }

    const [results, paginationData] = await User.safePaginatedFind({
      pagination,
      where,
      order: {
        createdAt: 'DESC',
      },
      relations: ['consumer', 'domain', 'wallets', 'states', 'consumer.states'],
    });

    setPaginationToRes(res, paginationData);
    res.success(results);
  }

  @Post('/', [
    OAuth.token([Scopes.users.WRITE]),
    Request.Domain((req) => req.body.domain),
    UserFilter.CreateUserFilter(),
  ])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const { firstName, lastName, email, password, role, username }: CreateUserWithoutConsumerDto = req.body;
    const userService = UserService.getInstance();

    const user = await getManager().transaction(async (manager) => {
      const user = await userService.createUserWithoutConsumer(
        {
          firstName,
          lastName,
          email,
          password,
          username,
          role,
          domain: req.cache.get<Domain>('request_domain')!,
        },
        manager,
      );

      return user;
    });

    res.success(user);
  }

  @Get('/me', [OAuth.token()])
  public static async current(req: bacenRequest, res: BaseResponse) {
    // Adds access token information to response headers
    const accessToken: OAuthAccessToken = res.locals.oauth.token;
    res.set('X-OAuth-Bearer-Scope', accessToken.scope.join(','));
    res.set('X-OAuth-Bearer-Expiration', accessToken.expires.toISOString());

    if (req.user.virtual) {
      return res.success(req.user);
    }

    const found = await User.findUserByIdAndPopulate(req.user.id);

    if (found) {
      if (Config.server?.instanceId) res.set('X-bacen-Instance-Id', Config.server.instanceId);
      if (found.domain?.id) res.set('X-bacen-Domain-Id', found.domain.id);
      if (found.domain?.settings?.ispb) res.set('X-bacen-ISPB', found.domain.settings.ispb);

      return res.success(found.toJSON());
    }

    throw new EntityNotFoundError('User');
  }

  @Get('/:userId', [
    OAuth.token([Scopes.users.READ]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserReadOneUser,
  ])
  public static async findOneById(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const user = await User.findUserByIdAndPopulate(userId);

    if (!user) {
      throw new EntityNotFoundError('User');
    }

    res.success(user);
  }

  @Get('/role/:role', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadAllUsers, Query.pagination])
  public static async findByRole(req: bacenRequest, res: BaseResponse) {
    const { role } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.safePaginatedFind({
      pagination,
      where: { role: role ? (role.toLowerCase() as UserRole) : undefined },
      order: {
        createdAt: 'DESC',
      },
      relations: ['consumer', 'domain', 'wallets'],
    });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:userId/wallets', [
    OAuth.token([Scopes.wallets.READ]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserReadOneUser,
  ])
  public static async findOneWallets(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Wallet.safePaginatedFind({
      pagination,
      where: { user: { id: userId } },
      relations: ['user'],
    });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Post('/reset', [Params.isValidEmail])
  public static async resetPassword(req: bacenRequest, res: BaseResponse) {
    const user = await User.findByEmail(req.body.email);

    if (!user) {
      throw new EntityNotFoundError('User');
    }

    // TODO: Specify password change only scope for this token
    const token = await OAuthSecretToken.generateRecoveryToken(user);
    const emailService = EmailService.getInstance();

    const emailLocals = EmailTemplateService.getInstance().getConfig({
      type: EmailTemplateType.PASSWORD_RESET,
      domain: user.domainId,
    });

    // Send an email with instructions
    await emailService.send({
      to: user.email,
      subject: Config.user.resetPassword.subject,
      text: `Reset your password: ${Config.user.resetPassword.url.replace(
        new RegExp(':token', 'ig'),
        token.secretToken,
      )}`,
      locals: {
        ...emailLocals,
        button: {
          label: emailLocals.button.label,
          url: Config.user.resetPassword.url.replace(new RegExp(':token', 'ig'), token.secretToken),
        },
      },
    });

    res.success({ email: true });
  }

  @Post('/password', [Params.isValidPassword, Params.isValidToken])
  public static async renewPassword(req: bacenRequest, res: BaseResponse) {
    // TODO: Move this to a filter and get a one time only req.user while revoking the token
    const secret = await OAuthSecretToken.findBySecret(req.body.token);

    if (!secret || !secret.user) {
      throw new UnauthorizedRequestError();
    }

    // Clear all user secret tokens before setting the new password for safety
    await OAuthSecretToken.revoke({ user: secret.user });

    // Set and save new password
    await secret.user.setPassword(req.body.password);
    await User.update(secret.user.id, {
      passwordHash: secret.user.passwordHash,
      passwordSalt: secret.user.passwordSalt,
    });

    res.success({ reset: true });
  }

  @Post('/:userId/password', [OAuth.token(null, true)], Exists.User, Params.isValidPassword)
  public static async renewUserPassword(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const { token, password }: { token?: string; password: string } = req.body;

    if (req.user && req.user.role !== UserRole.ADMIN) {
      // TODO: Move this to a filter and get a one time only req.user while revoking the token
      const secret = await OAuthSecretToken.findBySecret(token);

      if (!secret || !secret.user || secret.user.id !== userId) {
        throw new UnauthorizedRequestError();
      }
    }

    const user = await User.safeFindOne({ where: { id: userId } });

    // Clear all user secret tokens before setting the new password for safety
    await OAuthSecretToken.revoke({ user });

    // Set and save new password
    await user.setPassword(password);
    await User.update(user.id, {
      passwordHash: user.passwordHash,
      passwordSalt: user.passwordSalt,
    });

    res.success({ reset: true });
  }

  @Post('/:userId', [
    OAuth.token([Scopes.users.WRITE]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserWriteUser,
    Params.isValidUser(true),
  ])
  public static async updateById(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const payload: User = req.body;
    const invalidParams = [];

    // TODO: This should be a whitelist, not a blacklist!
    const forbiddenParams = [
      'role', // [Security] Prevent user role elevation
    ];

    for (const key of forbiddenParams) {
      if (dotProp.get(payload, key)) {
        invalidParams.push(key);
      }
    }

    // Ensure forbidden parameters are not in the request
    if (invalidParams.length) {
      throw new ForbiddenParameterError('User', invalidParams);
    }

    if ((payload as any).password || payload.passwordHash || payload.passwordSalt) {
      throw new HttpError('Cannot update password without an email confirmation', HttpCode.Client.FORBIDDEN);
    }

    const user = await User.safeFindOne({ where: { id: userId } });

    if (user.role === UserRole.CONSUMER) {
      throw new ForbiddenRequestError('Consumers should be updated in their own API endpoint');
    }

    const filteredPayload = { firstName: payload.firstName, lastName: payload.lastName, email: payload.email };
    // Remove undefineds
    Object.keys(filteredPayload).forEach((key) =>
      filteredPayload[key] === undefined ? delete filteredPayload[key] : '',
    );

    // Ensure user was not a consumer
    await User.createQueryBuilder()
      .update()
      .where({ id: userId, role: Not(UserRole.CONSUMER) })
      .set(filteredPayload)
      .execute();

    await user.reload();

    res.success(user);
  }

  @Put('/:userId/domain/:domainId', [
    OAuth.token([Scopes.users.WRITE]),
    Exists.User,
    Permissions.canUserChangeUserDomain,
  ])
  public static async changeUserDomain(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    let { domainId } = req.params;
    const extra = req.param('extra');

    if (domainId === 'root') {
      domainId = (await Domain.getRootDomain()).id;
    } else if (domainId === 'default') {
      domainId = (await Domain.getDefaultDomain()).id;
    } else if (!(await Domain.safeCount({ where: { id: domainId } }))) {
      throw new HttpError(`Domain ${domainId} not found`, HttpCode.Client.NOT_FOUND);
    }

    await User.update(userId, { domain: { id: domainId } });

    const user = await User.safeFindOne({
      where: { id: userId },
      relations: [
        'domain',
        'states',
        'consumer',
        'consumer.addresses',
        'consumer.phones',
        'consumer.states',
        'wallets',
        'wallets.assetRegistrations',
        'wallets.assetRegistrations.asset',
      ],
    });

    if (user.wallets) {
      // Update user info in providers
      for (const wallet of user.wallets) {
        const root = wallet.assetRegistrations.find((a) => a.asset.root);
        const otherAssets = wallet.assetRegistrations.filter((a) => !a.asset.root);

        const manager = ProviderManagerService.getInstance();
        const rootProvider = manager.from(root.asset.provider);

        // Update root asset registration
        logger.debug('Updating user and wallet information in root asset provider', {
          user: user.id,
          wallet: wallet.id,
          asset: root.asset.code,
        });
        await rootProvider.update(user, wallet, extra);

        // Update other assets
        await Promise.all(
          otherAssets
            .filter((other) => !!other.asset.provider)
            .map(async (other) => {
              const otherProvider = manager.from(other.asset.provider);

              logger.debug('Updating user and wallet information in asset provider', {
                user: user.id,
                wallet: wallet.id,
                asset: root.asset.code,
              });
              await otherProvider.update(user, wallet, extra);
            }),
        );
      }
    }

    res.success(user);
  }

  @Delete('/:userId', [
    OAuth.token([Scopes.users.DELETE]),
    Query.isValidId('userId'),
    Exists.User,
    Permissions.canUserDeleteUser,
  ])
  public static async deleteById(req: bacenRequest, res: BaseResponse) {
    const user = await User.findUserByIdAndPopulate(req.param('userId') || req.param('id'));

    // Fetch balances from wallets already registered in the ledger
    const balances = await Promise.all(
      user.wallets?.filter((wallet) => wallet.status !== WalletStatus.PENDING).map((wallet) => wallet.getBalances()) ||
        [],
    );

    const allBalancesZeroed = balances
      .flat()
      .filter((balance) => balance.asset_type !== 'native')
      .every((balance) => Number(balance.balance) === 0);

    if (user.consumer && allBalancesZeroed) {
      const consumerFSM = new ConsumerStateMachine(user);
      const deleteBy = new Date(Date.now() + Config.user.deletionDeadline * 24 * 60 * 60 * 1000);
      const success = await consumerFSM.goTo(ConsumerStatus.PENDING_DELETION, {
        deleteBy,
        reason: ConsumerDeleteReason.REQUESTED_BY_USER,
      });

      if (success) {
        const reloadedUser = await User.safeFindOne({
          where: { id: req.params.userId },
          relations: ['states'],
        });

        res.success({
          ...reloadedUser,
          deleteBy,
        });
      } else {
        throw new HttpError(
          'An unknown error prevented the user from being deleted',
          HttpCode.Server.INTERNAL_SERVER_ERROR,
          {
            id: user.id,
          },
        );
      }
    } else if (user.wallets?.length === 0) {
      // User has no wallet, just delete it
      // Mainly used for operators and auditors
      // TODO: Use FSM and move to inactive
      const result = await User.softDelete(user.id);
      return res.success(result);
    } else {
      const walletToBalanceMap: Record<string, Record<string, string>> = {};
      for (let i = 0; i < balances.length; i += 1) {
        const walletBalances: Record<string, string> = {};

        for (const balance of balances[i]) {
          if (balance.asset_type === 'native') continue;
          walletBalances[balance.asset_code] = balance.balance;
        }
        walletToBalanceMap[user.wallets[i].id] = walletBalances;
      }

      throw new HttpError(
        'User balance must be zero on all wallets before being marked for deletion',
        HttpCode.Client.BAD_REQUEST,
        { balances: walletToBalanceMap },
      );
    }
  }

  @Get('/:userId/invoices', [OAuth.token([Scopes.invoices.READ]), Query.pagination])
  public static async findAllUserInvoices(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Invoice.findAllByUser(userId, pagination);

    setPaginationToRes(res, paginationData);
    res.success(results.map((result) => result.toSimpleJSON()));
  }

  @Get('/:userId/invoices/:id', [
    OAuth.token([Scopes.invoices.READ]),
    Exists.Invoice,
    Permissions.canUserReadOneInvoice,
  ])
  public static async findOneInvoice(req: bacenRequest, res: BaseResponse) {
    const { userId, id } = req.params;

    res.success(await Invoice.findOneByUser(userId, id));
  }
}
