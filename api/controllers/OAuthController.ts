import { BaseRequest, BaseResponse, Controller, HttpError, Post } from 'ts-framework';
import { MoreThan } from 'typeorm';
import Config from '../../config';
import Scopes from '../../config/oauth/scopes';
import { UnauthorizedRequestError } from '../errors';
import { OAuth } from '../filters';
import { OAuthAccessToken, OAuthSecretToken, User } from '../models';
import { CacheService } from '../services/cache';

@Controller('/oauth', [])
export default class OAuthController {
  @Post('/introspect', [])
  public static async introspect(req, res) {
    const authToken = req.headers['authorization'] ? req.headers['authorization'].split('Bearer ').join('') : '';
    const accessToken = req.param('token') || req.param('accessToken') || req.param('access_token') || authToken;

    const cache = CacheService.getInstance();
    const cacheKey = `oauth_introspect_${accessToken}`;

    let result: any = await cache.get(cacheKey);

    if (!result) {
      const token = await OAuthAccessToken.safeFindOne({
        where: { accessToken, expires: MoreThan(new Date()) },
        relations: ['client', 'user', 'user.domain'],
      });

      if (!token) {
        throw new UnauthorizedRequestError('Credentials are invalid or missing');
      }

      result = {
        active: true,
        scope: token.scope,
        token_type: token.tokenType,
        client_id: token.client?.id,
        user_id: token.client?.id,
        domain_id: token?.user?.domain?.id,
        instance_id: Config.server.instanceId,
        exp: token.expires?.getTime(),
        iat: token.createdAt?.getTime(),
        ispb: token?.user?.domain?.settings?.ispb,
      };

      // Set cache without waiting to respond (in seconds)
      cache.set(cacheKey, result, Config.oauth.msTokenCacheTimeout / 1000);
    }

    if (result.instance_id) res.set('X-bacen-Instance-Id', result.instance_id);
    if (result.domain_id) res.set('X-bacen-Domain-Id', result.domain_id);
    if (result.ispb) res.set('X-bacen-ISPB', result.ispb);

    res.success(result);
  }

  /**
   * Generates a new secret token for the currently authenticated user.
   *
   * @param req The client request
   * @param res The client response
   */
  @Post('/secret', [OAuth.token(Scopes.users.WRITE_SECRET_TOKENS), OAuth.hasRequestedValidScopes])
  public static async secret(req, res) {
    const { token } = res.locals.oauth;
    const scopes = req.param('scopes') || token.scope;
    const resources = req.param('resources') || [];

    // Get desired scopes from the the body, already validated in a previous filter
    const secret = await OAuthSecretToken.generateToken(req.user, scopes, resources);

    // This is the only time we return the secret token as a response
    return res.json({
      ...secret.toJSON(),
      secretToken: secret.secretToken,
    });
  }

  /**
   * Revokes the current token, or all active ones, based on request body.
   *
   * @param req The client request
   * @param res The client response
   */
  @Post('/revoke', [OAuth.token([Scopes.users.WRITE]), OAuth.hasValidRevoke])
  public static async revoke(req: BaseRequest, res: BaseResponse) {
    try {
      // Always ensure the token being revoked belongs to current user
      let response = { ok: false } as any;
      const queryCond: Partial<OAuthAccessToken> = { user: { id: req.user.id } as User };

      if (req.query.accessToken) {
        queryCond.accessToken = req.query.accessToken;
      }

      response = await OAuthAccessToken.revoke(queryCond);
      await CacheService.getInstance().removeCache(response.keys);
      // TODO: Save response in audit
      res.success(response.response);
    } catch (e) {
      throw new HttpError(e.message, e.code);
    }
  }
}
