import { ConsumerStatus, DocumentStatus } from '@bacen/base-sdk';
import IdentityService from '@bacen/identity-sdk';
import { ImageService } from '@bacen/image-tools';
import { BaseError } from 'nano-errors';
import { BaseResponse, Controller, Get, HttpCode, HttpError, Post } from 'ts-framework';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError } from '../errors';
import { Exists, OAuth, Permissions } from '../filters';
import { DocumentStateMachine } from '../fsm';
import { AnalysisResult, bacenRequest, Document, User } from '../models';
import { ConsumerPipelinePublisher, StorageService } from '../services';
import { checkRequiredData } from '../utils/ValidationUtil';

import sharp = require('sharp');

@Controller('/kyc')
export default class KycController {
  @Get('/people/preview', [OAuth.token([Scopes.users.READ, Scopes.users.READ_KYC]), Permissions.canUserPreview])
  public static async previewPeople(req: bacenRequest, res: BaseResponse) {
    const { taxId }: { taxId: string } = { ...req.params, ...req.query };

    try {
      const [basic, report] = await Promise.all([
        IdentityService.getInstance()
          .people()
          .basic(taxId),
        IdentityService.getInstance()
          .people()
          .report(taxId),
      ]);

      res.success({ report, basic, accepted: report.accepted });
    } catch (exception) {
      const error = exception as BaseError;
      throw new HttpError(error.message, HttpCode.Server.INTERNAL_SERVER_ERROR, error.details);
    }
  }

  @Get('/companies/preview', [OAuth.token([Scopes.users.READ, Scopes.users.READ_KYC]), Permissions.canUserPreview])
  public static async previewCompanies(req: bacenRequest, res: BaseResponse) {
    const { taxId }: { taxId: string } = { ...req.params, ...req.query };

    try {
      const [basic, report] = await Promise.all([
        IdentityService.getInstance()
          .companies()
          .basic(taxId),
        IdentityService.getInstance()
          .companies()
          .report(taxId),
      ]);

      res.success({ report, basic, accepted: report.accepted });
    } catch (exception) {
      const error = exception as BaseError;
      throw new HttpError(error.message, HttpCode.Server.INTERNAL_SERVER_ERROR, error.details);
    }
  }

  // TODO: Protect for operators only
  @Get('/manual', [OAuth.token([Scopes.users.READ, Scopes.users.READ_KYC]), Permissions.canUserReadAllConsumers])
  public static async getPendingUsers(req: bacenRequest, res: BaseResponse) {
    const result = await User.findByLatestStatusAndDomain(ConsumerStatus.MANUAL_VERIFICATION);
    res.success(result.map(user => user.toSimpleJSON()));
  }

  @Get('/manual/:userId/:documentId/:side', [
    OAuth.token([Scopes.users.READ_DOCUMENTS]),
    Permissions.canUserReadOneConsumer,
    Exists.User,
    Exists.Document,
  ])
  public static async getDocument(req: bacenRequest, res: BaseResponse) {
    const { userId, documentId, side } = req.params as {
      userId: string;
      documentId: string;
      side: 'front' | 'back' | 'both_sides' | 'selfie';
    };

    const format = req.param('format');
    const user = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'domain'] });

    const document = await Document.findOne({
      where: { id: documentId, consumer: user.consumer.id },
      relations: ['consumer', 'consumer.user'],
    });

    if (!document) {
      throw new EntityNotFoundError('Document not found', { side });
    }

    const storageService = StorageService.getInstance();
    const fileName = document.getImageFileName(side);
    const requestedJpeg = ['jpg', 'jpeg', 'image/jpeg'].indexOf(format) >= 0;

    if (!(await storageService.fileExists({ fileName }))) {
      throw new EntityNotFoundError('Document not found', { side });
    }

    // Load file from storage and get its basic info
    const stream = await storageService.getFileAsStream({ fileName });
    const image = await ImageService.fromStream(stream);

    if (requestedJpeg) {
      try {
        // Try to convert image to JPEG using streams
        res.set('Content-type', 'image/jpeg');
        return image.toJPEGStream(res);
      } catch (exception) {
        // Something went wront, probably image is not suitable for conversion
        throw new HttpError('Could not convert image to JPEG', HttpCode.Client.BAD_REQUEST, exception);
      }
    } else {
      // Pipe original file and content type
      res.set('Content-type', image.options.mime);
      return image.pipe(res);
    }
  }

  @Post('/manual/:userId/:documentId', [
    OAuth.token([Scopes.users.EVALUATE_DOCUMENTS]),
    Permissions.canUserWriteConsumer,
    Exists.User,
    Exists.Document,
  ])
  public static async evaluateDocument(req: bacenRequest, res: BaseResponse) {
    const { userId, documentId } = req.params;
    const { approved, reason }: { approved: boolean; reason: string } = req.body;

    checkRequiredData(req.body, ['approved', 'reason']);

    // Building result information object
    const analysisResult = {
      reason,
      result: approved ? AnalysisResult.APPROVED : AnalysisResult.DENIED,
      at: new Date(),
      by: req.user.id,
    };

    const user = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'domain'] });

    const document = await Document.findOne({
      where: { id: documentId, consumer: user.consumer.id },
      relations: ['consumer', 'consumer.user'],
    });

    if (!document) {
      throw new EntityNotFoundError('document');
    }

    const fsm = new DocumentStateMachine(document);
    await fsm.goTo(
      approved ? DocumentStatus.MANUALLY_VERIFIED : DocumentStatus.FAILED_MANUAL_VERIFICATION,
      analysisResult,
    );

    await ConsumerPipelinePublisher.getInstance().send(user);

    res.success();
  }
}
