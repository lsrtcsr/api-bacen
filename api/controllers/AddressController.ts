import { Controller, Post, BaseResponse, Delete, Get } from 'ts-framework';
import { InvalidRequestError } from 'oauth2-server';
import { UserRole } from '@bacen/base-sdk';
import { Address, Consumer, bacenRequest, User } from '../models';
import { OAuth, Exists, Params, Permissions, Request, Query } from '../filters';
import { ForbiddenRequestError } from '../errors';
import Scopes from '../../config/oauth/scopes';
import { setPaginationToRes, getPaginationFromReq } from '../utils';

@Controller('/consumers/:userId/addresses', [Exists.Consumer])
export default class AddressController {
  @Get('/', [OAuth.token(Scopes.users.READ), Permissions.canUserReadOneConsumer])
  public static async findAllAddresses(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.findPaginatedByConsumerRelation(userId, Address, pagination);

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:addressId', [
    OAuth.token(Scopes.users.READ),
    Query.isValidId('addressId'),
    Permissions.canUserReadOneConsumer,
    Exists.Address,
  ])
  public static async findOneAddressById(req: bacenRequest, res: BaseResponse) {
    const { userId, addressId } = req.params;

    const address = await Address.createQueryBuilder('address')
      .leftJoin('address.consumer', 'consumer')
      .leftJoin('consumer.user', 'user')
      .where('user.id = :userId', { userId })
      .andWhere('address.id = :addressId', { addressId })
      .andWhere('address.deletedAt IS NULL')
      .getOne();

    res.success(address.toJSON());
  }

  @Post('/', [OAuth.token(Scopes.users.WRITE), Permissions.canUserWriteConsumer, Params.isValidAddress()])
  public static async createAddress(req: bacenRequest, res: BaseResponse) {
    // Checking if the user has permission to create a new address
    if (req.user.role === UserRole.CONSUMER) {
      // The user is allowed to create only his first address by himself
      const userId = req.user.id;
      const addresses = await Address.createQueryBuilder('address')
        .leftJoin('address.consumer', 'consumer')
        .leftJoin('consumer.user', 'user')
        .where('user.id = :userId', { userId })
        .andWhere('address.deletedAt IS NULL')
        .getCount();

      if (addresses > 0) {
        throw new ForbiddenRequestError('You may only register one address.');
      }
    }

    const { userId } = req.params;
    const payload: Address = Address.create(req.body);

    const consumer = await Consumer.safeFindOne({ where: { user: { id: userId } } });

    const addressSchema = await Address.fromCEP(payload.code, payload);

    const address = await Address.insertAndFind({ ...addressSchema, consumer: consumer.id as any });

    res.success(address.toJSON());
  }

  @Post('/:addressId', [
    OAuth.token(Scopes.users.WRITE),
    Query.isValidId('addressId'),
    Permissions.canUserWriteConsumer,
    Exists.Address,
    Params.isValidAddress(true),
  ])
  public static async updateAddress(req: bacenRequest, res: BaseResponse) {
    // Checking if the user has permission to edit his own address
    if (req.user.role === UserRole.CONSUMER) {
      throw new ForbiddenRequestError('You cannot update your own address.');
    }

    const { addressId } = req.params;
    const payload: Address = Address.create(req.body);

    payload.id = addressId;
    const address = await Address.updateAndFind(addressId, payload);

    res.success(address.toJSON());
  }

  @Delete('/:addressId', [
    OAuth.token(Scopes.users.WRITE),
    Query.isValidId('addressId'),
    Permissions.canUserWriteConsumer,
    Exists.Address,
  ])
  public static async deleteAddress(req: bacenRequest, res: BaseResponse) {
    const { userId, addressId } = req.params;
    const { force }: { force?: string } = req.query;

    const numberOfAddresses = await Address.safeCount({ where: { consumer: { user: { id: userId } } } });

    if (numberOfAddresses === 1) {
      throw new InvalidRequestError(`You need at least one address. Create a new address before deleting this one.`);
    }

    let deleted: boolean;

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Address.delete(addressId).then(_ => true);
    } else {
      deleted = await Address.softDelete(addressId).then(_ => true);
    }

    res.success(deleted);
  }
}
