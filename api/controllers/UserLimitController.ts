import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { plainToClass } from 'class-transformer';
import { Exists, OAuth, Permissions } from '../filters';
import { bacenRequest, User, UserLimit, LimitType, DomainSettingType, Asset } from '../models';
import { LimitSettingService } from '../services';
import Scopes from '../../config/oauth/scopes';
import { InvalidParameterError, InvalidRequestError } from '../errors';

@Controller('/consumers/:userId/limits', [Exists.User])
export default class UserLimitController {
  @Get('/', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadAllLimits])
  public static async findAllUserLimits(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;

    const limitSettingService = LimitSettingService.getInstance();
    const limits = [];

    const user = await User.safeFindOne({ where: { id: userId }, relations: ['domain'] });
    const userLimits = await UserLimit.safeFind({ where: { user: { id: userId }, current: true } });

    userLimits.forEach((userLimit) => {
      let limitItem;
      if (userLimit.asset) {
        limitItem = {
          type: userLimit.type,
          asset: userLimit.asset,
          value: userLimit.value,
        };
      } else {
        limitItem = {
          type: userLimit.type,
          value: userLimit.value,
        };
      }

      limits.push(limitItem);
    });

    for (const type in DomainSettingType) {
      const globalLimitType = DomainSettingType[type] as DomainSettingType;
      const userLimitType = limitSettingService.domainSettingToLimitTypeMap.get(globalLimitType);
      if (!userLimitType) continue;

      const userLimit = userLimits.find((limit) => limit.type === userLimitType);

      if (!userLimit) {
        limits.push({
          type: userLimitType.valueOf(),
          value: Reflect.get(user.domain.settings.locks, globalLimitType),
        });
      }
    }

    res.success(limits);
  }

  @Get('/:type', [OAuth.token([Scopes.users.READ]), Permissions.canUserReadOneLimit])
  public static async findOneByType(req: bacenRequest, res: BaseResponse) {
    const { userId, type } = req.params as { userId: string; type: LimitType };
    const { asset } = req.body as { asset: string };
    const user = await User.safeFindOne({
      where: { id: userId },
      relations: ['wallets'],
    });

    const response = {
      type: type.valueOf(),
      value: await LimitSettingService.getInstance().getLimits(type, user, asset),
    };

    res.success(response);
  }

  @Post('/:type', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteLimit])
  public static async setOneByType(req: bacenRequest, res: BaseResponse) {
    const { userId, type } = req.params as { userId: string; type: LimitType };
    let { value, asset }: { value: number; asset: string } = req.body;

    const currentUser = req.user;

    const user = await User.safeFindOne({ where: { id: userId } });

    if (!asset) {
      asset = (await Asset.getRootAsset()).code;
    }

    const limitRequest = {
      type,
      value,
      user,
      asset,
      createdBy: currentUser,
    };

    const dto = plainToClass(UserLimit, limitRequest);
    const validationErrors = await dto.validate();

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('UserLimit', validationErrors);
    }

    const assetExists = await Asset.findOne({ where: { code: limitRequest.asset } });
    if (!assetExists) throw new InvalidRequestError(`Asset with code ${limitRequest.asset} does not exist`);

    const userLimits = await UserLimit.safeFindOne({ where: { user: { id: userId }, asset } });
    let userLimit: UserLimit;
    if (userLimits) {
      userLimit = await LimitSettingService.getInstance().update(limitRequest);
    } else {
      userLimit = await LimitSettingService.getInstance().create(limitRequest);
    }

    res.success(userLimit.toSimpleJSON());
  }

  @Delete('/:type/:asset', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserDeleteLimit])
  public static async deleteByType(req: bacenRequest, res: BaseResponse) {
    const { userId, type, asset } = req.params as { userId: string; type: LimitType; asset: string };
    const { force }: { force?: string } = req.query;

    const userLimit = await UserLimit.safeFindOne({
      where: { type, user: { id: userId }, asset, current: true },
    });

    if (!userLimit) {
      throw new HttpError('User limit not found', HttpCode.Client.NOT_FOUND);
    }

    let deleted;
    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await UserLimit.delete(userLimit.id).then((_) => true);
    } else {
      deleted = await UserLimit.softDelete(userLimit.id).then((_) => true);
    }

    res.success(deleted);
  }
}
