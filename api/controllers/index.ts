import AddressController from './AddressController';
import AccountingController from './AccountingController';
import AlertController from './AlertController';
import AssetController from './AssetController';
import AuditorController from './AuditorController';
import BankingController from './BankingController';
import BillingController from './BillingController';
import BoletoController from './BoletoController';
import BranchController from './BranchController';
import CardController from './CardController';
import ConsumerController from './ConsumerController';
import DiscoveryController from './DiscoveryController';
import DocumentController from './DocumentController';
import DomainController from './DomainController';
import DomainMetricsController from './DomainMetricsController';
import InvoiceController from './InvoiceController';
import IssueController from './IssueController';
import KycController from './KycController';
import LegalTermController from './LegalTermController';
import MediatorController from './MediatorController';
import MetricsController from './MetricsController';
import OAuthClientController from './OAuthClientController';
import OAuthController from './OAuthController';
import OperatorController from './OperatorController';
import PaymentController from './PaymentController';
import PhoneController from './PhoneController';
import PhoneCreditsController from './PhoneCreditsController';
import PlanController from './PlanController';
import PostbacksController from './PostbacksController';
import ProviderFeatureProxyController from './ProviderFeatureProxyController';
import QueueController from './QueueController';
import ServiceController from './ServiceController';
import SettingsController from './SettingsController';
import StatusController from './StatusController';
import TransactionController from './TransactionController';
import TwoFactorController from './TwoFactorController';
import UserController from './UserController';
import UserLimitController from './UserLimitController';
import WalletController from './WalletController';

export default {
  AccountingController,
  AddressController,
  AlertController,
  AssetController,
  AuditorController,
  BankingController,
  BillingController,
  BoletoController,
  BranchController,
  CardController,
  ConsumerController,
  MetricsController,
  DiscoveryController,
  DocumentController,
  DomainController,
  DomainMetricsController,
  InvoiceController,
  IssueController,
  KycController,
  LegalTermController,
  MediatorController,
  OAuthClientController,
  OAuthController,
  OperatorController,
  PaymentController,
  PhoneController,
  PhoneCreditsController,
  PlanController,
  PostbacksController,
  ProviderFeatureProxyController,
  QueueController,
  ServiceController,
  SettingsController,
  StatusController,
  TransactionController,
  TwoFactorController,
  UserController,
  UserLimitController,
  WalletController,
};
