import { BankingSchema } from '@bacen/base-sdk';
import { BaseResponse, Controller, Delete, Get, Post } from 'ts-framework';
import { getManager } from 'typeorm';
import Scopes from '../../config/oauth/scopes';
import { InvalidRequestError, EntityNotFoundError } from '../errors';
import { Exists, OAuth, Params, Permissions, Query } from '../filters';
import { Banking, bacenRequest, Consumer, User } from '../models';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/consumers/:userId/bankings', [Exists.User])
export default class BankingController {
  @Get('/', [OAuth.token(Scopes.users.READ_BANKINGS), Permissions.canUserReadOneConsumer])
  public static async findAllBankings(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await User.findPaginatedByConsumerRelation(userId, Banking, pagination);

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:bankingId', [
    OAuth.token(Scopes.users.READ_BANKINGS),
    Query.isValidId('bankingId'),
    Permissions.canUserReadOneConsumer,
    Exists.Banking,
  ])
  public static async findOneBankingById(req: bacenRequest, res: BaseResponse) {
    const { userId, bankingId } = req.params;

    const banking = await Banking.createQueryBuilder('banking')
      .leftJoin('banking.consumer', 'consumer')
      .leftJoin('consumer.user', 'user')
      .where('user.id = :userId', { userId })
      .andWhere('banking.id = :bankingId', { bankingId })
      .andWhere('banking.deletedAt IS NULL')
      .getOne();

    if (!banking) {
      throw new EntityNotFoundError('Banking');
    }

    res.success(banking.toJSON());
  }

  @Post('/', [OAuth.token([Scopes.users.WRITE_BANKINGS]), Permissions.canUserWriteConsumer, Params.isValidBanking()])
  public static async createBanking(req: bacenRequest, res: BaseResponse) {
    const { userId } = req.params;
    const schema: BankingSchema = req.body;

    const consumer = await Consumer.safeFindOne({ where: { user: { id: userId } } });

    if (schema && consumer) {
      const bankingData = await getManager().transaction(async manager => Banking.from(schema, consumer, manager));
      res.success(bankingData.toJSON());
    } else {
      throw new InvalidRequestError('Banking information error');
    }
  }

  @Post('/:bankingId', [
    OAuth.token([Scopes.users.WRITE_BANKINGS]),
    Query.isValidId('bankingId'),
    Permissions.canUserWriteConsumer,
    Exists.Banking,
    Params.isValidBanking(true),
  ])
  public static async updateBanking(req: bacenRequest, res: BaseResponse) {
    const { bankingId } = req.params;
    const payload: Banking = Banking.create(req.body);
    const banking = await Banking.updateAndFind(bankingId, payload);
    res.success(banking.toJSON());
  }

  @Delete('/:bankingId', [
    OAuth.token([Scopes.users.WRITE_BANKINGS]),
    Query.isValidId('bankingId'),
    Permissions.canUserWriteConsumer,
    Exists.Banking,
  ])
  public static async deleteBanking(req: bacenRequest, res: BaseResponse) {
    const { bankingId } = req.params;
    const { force }: { force?: string } = req.query;

    // Find banking for audit
    const banking = await Banking.safeFindOne({ where: { id: bankingId } });

    if (!banking) {
      throw new EntityNotFoundError('Banking');
    }

    let deleted: boolean;

    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Banking.delete(bankingId).then(_ => true);
    } else {
      deleted = await Banking.softDelete(bankingId).then(_ => true);
    }

    res.success(deleted);
  }
}
