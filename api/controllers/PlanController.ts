import { FindConditions, getManager, DeepPartial } from 'typeorm';
import { BaseResponse, Controller, Get, Post, Delete, Put, HttpCode, HttpError } from 'ts-framework';
import * as moment from 'moment';
import { UserRole, ServiceType, ConsumerStatus } from '@bacen/base-sdk';
import { OAuth, Exists, Permissions, Status } from '../filters';
import {
  bacenRequest,
  Plan,
  PlanStatus,
  User,
  EntryType,
  BillingSettingsSchema,
  Domain,
  PriceItem,
  Contract,
  ContractStatus,
  ContractState,
} from '../models';
import { PlanService, ContractService } from '../services';
import { PlanServiceSchema } from '../schemas';
import { EntityNotFoundError, InternalServerError, InvalidParameterError, InvalidRequestError } from '../errors';
import Scopes from '../../config/oauth/scopes';
import { isUUID } from '../utils';
import { ContractStateMachine, ConsumerStateMachine } from '../fsm';

const DEFAULT_LIMIT = 25;

@Controller('/plans')
export default class PlanController {
  @Get('/', [OAuth.token([Scopes.plans.READ]), Permissions.canUserReadAllPlans])
  public static async find(req: bacenRequest, res: BaseResponse) {
    const { skip = 0, limit = DEFAULT_LIMIT }: { skip: number; limit: number } = req.query;
    const currentUser: User = req.user;

    const where: FindConditions<Plan> = { status: PlanStatus.AVAILABLE };
    if ([UserRole.MEDIATOR, UserRole.CONSUMER].includes(currentUser.role)) {
      where.supplier = { id: currentUser.domain.id };
    }

    let plans: Plan[];
    let count: number;
    try {
      const queries: [Promise<Plan[]>, Promise<number>] = [
        Plan.safeFind({
          where,
          skip,
          take: limit,
          relations: ['supplier'],
        }),
        Plan.safeCount({ where, relations: ['supplier'] }),
      ];

      [plans, count] = await Promise.all(queries);
    } catch (error) {
      throw new InternalServerError(`Error trying to retrieve plans`);
    }

    res.set('X-Data-Length', count.toString());
    res.set('X-Data-Skip', req.query.skip || '0');
    res.set('X-Data-Limit', req.query.limit || DEFAULT_LIMIT);

    res.success(plans);
  }

  @Get('/:id', [OAuth.token([Scopes.plans.READ]), Exists.Plan, Permissions.canUserReadOnePlan])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;

    const plan = await Plan.safeFindOne({ where: { id }, relations: ['supplier', 'priceList', 'items', 'contracts'] });

    if (!plan) {
      throw new EntityNotFoundError('Plan');
    }

    res.success(plan);
  }

  @Post('/:id/subscribe', [OAuth.token([Scopes.contracts.WRITE]), Exists.Plan, Permissions.canUserWriteContract])
  public static async subscribe(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const {
      subscriber,
      from,
      prepaid,
      force,
    }: { subscriber: string; from: Date; prepaid: boolean; force: boolean } = req.body;

    let contractor: User;
    if (!subscriber) {
      contractor = await User.safeFindOne({
        where: { id: req.user.id },
        relations: ['domain', 'contracts', 'consumer', 'consumer.states', 'states'],
      });
    } else {
      contractor = await User.safeFindOne({
        where: { id: subscriber },
        relations: ['domain', 'contracts', 'consumer', 'consumer.states', 'states'],
      });
    }

    if (!contractor) throw new EntityNotFoundError('Subscriber');

    let result = {};

    const currentContract = await ContractService.getInstance().getCurrentContract(contractor.id);

    if (currentContract && currentContract.plan.id === id) {
      throw new InvalidRequestError(
        `The user ${contractor.id} already has an active subscription of the given plan ${id}.`,
      );
    } else if (currentContract && force) {
      const consumerFSM = new ConsumerStateMachine(contractor);
      await consumerFSM.goTo(ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION, { plan: { id, prepaid } });
      result = {
        message: 'The current subscription will end and the current invoice will be closed. The subscription '.concat(
          'for the selected plan will be created at the end, as well as a new invoice.',
        ),
      };
    } else if (currentContract) {
      throw new InvalidRequestError(
        `The user ${contractor.id} already has an active subscription `
          .concat(`of another plan: ID ${currentContract.plan.id}. If you want to end the current `)
          .concat('subscription and create a new one for the plan whose ID was provided, it is ')
          .concat('necessary to pass the force property with the value true in the request payload'),
      );
    } else {
      result = await PlanService.getInstance().subscribe({
        prepaid,
        plan: id,
        user: contractor,
        validFrom: from ? moment(from) : moment(),
      });
    }

    res.success(result);
  }

  @Post('/:id/subscriptions/:subscriptionId', [
    OAuth.token([Scopes.contracts.WRITE]),
    Exists.Plan,
    Exists.Contract,
    Permissions.canUserWriteContract,
  ])
  public static async changeSubscriptionStatus(req: bacenRequest, res: BaseResponse) {
    const { id, subscriptionId } = req.params;
    const { destination, expirationDate }: { destination: ContractStatus; expirationDate?: Date } = req.body;

    if (!destination) {
      throw new InvalidRequestError('Destination status parameter is required');
    }

    if (expirationDate && moment(expirationDate).isSameOrBefore(moment())) {
      throw new InvalidParameterError('expirationDate', undefined, {
        reason: 'The expirationDate must be a future date',
      });
    }

    let warning: string;
    if (expirationDate && destination !== ContractStatus.ACTIVE) {
      warning = 'The expirationData parameter is only appliable '.concat(
        'for subscription renewing, so it will be ignored',
      );
    }

    let contract = await Contract.findOne({
      where: { id: subscriptionId, plan: { id } },
      relations: ['plan', 'contractor', 'states'],
    });

    if (!new ContractStateMachine(contract).canGoTo(destination)) {
      throw new InvalidRequestError(`State transition from ${contract.status} to ${destination} not allowed`);
    }

    await getManager().transaction(async manager => {
      if (expirationDate && destination === ContractStatus.ACTIVE) {
        const payload: DeepPartial<Contract> = { validUntil: moment(expirationDate) };
        contract = await Contract.updateAndFind(contract.id, payload, {
          manager,
          findOptions: { relations: ['plan', 'contractor', 'states'] },
        });
      }

      const currentState = contract.getStates()[0];
      await manager.update(ContractState, currentState.id, {
        additionalData: {
          stateTransitionRequest: {
            to: destination,
            at: new Date(),
            by: req.user.id,
            force: true,
          },
        } as any,
      });

      const contractFSM = new ContractStateMachine(contract);
      await contractFSM.goTo(destination, { force: true });
    });

    res.success({ warning });
  }

  @Put('/:id', [OAuth.token([Scopes.plans.WRITE]), Permissions.canUserWritePlan])
  public static async update(req: bacenRequest, res: BaseResponse) {
    const { name, status, validUntil }: { name?: string; status?: PlanStatus; validUntil?: string } = req.body;
    const { id } = req.params;

    if (validUntil && !moment(validUntil).isValid())
      throw new HttpError('Invalid date validUntil', HttpCode.Client.BAD_REQUEST);

    const now = moment();

    if (validUntil && moment(validUntil).isBefore(now))
      throw new HttpError('validUntil cannot be before today', HttpCode.Client.BAD_REQUEST);

    const plan = await PlanService.getInstance().updateBasicInfo({
      id,
      name,
      status,
      validUntil: validUntil ? moment(validUntil) : null,
    });

    res.success(plan);
  }

  @Put('/:id/subscriptions/:subscriptionId', [
    OAuth.token([Scopes.contracts.WRITE]),
    Exists.Plan,
    Exists.Contract,
    Status.isContractInStatus('subscriptionId', ContractStatus.ACTIVE),
    Permissions.canUserWriteContract,
  ])
  public static async updateSubscription(req: bacenRequest, res: BaseResponse) {
    const { id, subscriptionId } = req.params;
    const { expirationDate, prepaid }: { expirationDate: Date; prepaid: boolean } = req.body;

    if (expirationDate && moment(expirationDate).isSameOrBefore(moment())) {
      throw new InvalidParameterError('expirationDate', undefined, {
        reason: 'The expirationDate must be a future date',
      });
    }

    if (!expirationDate && !req.body.hasOwnProperty('prepaid')) {
      throw new InvalidRequestError('At least one parameter must be provided');
    }

    const contract = await Contract.findOne({
      where: { id: subscriptionId, plan: { id } },
      relations: ['states'],
    });

    const payload: DeepPartial<Contract> = {};
    if (expirationDate) payload.validUntil = moment(expirationDate);
    if (req.body.hasOwnProperty('prepaid')) payload.prepaid = prepaid;

    const updated = await Contract.updateAndFind(contract.id, payload);

    res.success(updated);
  }

  @Post('/:id/items', [OAuth.token([Scopes.plans.READ]), Exists.Plan, Permissions.canUserWritePlan])
  public static async addItem(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const items: PlanServiceSchema[] = req.body;

    const result = await PlanService.getInstance().updatePlan(id, items);

    res.success(result);
  }

  @Post('/:id/items/:itemId/invalidate', [OAuth.token([Scopes.plans.READ]), Exists.Plan, Permissions.canUserWritePlan])
  public static async invalidateItem(req: bacenRequest, res: BaseResponse) {
    const { id, itemId } = req.params;

    const entryType = await EntryType.safeFindOne({
      where: { id: itemId, plan: { id } },
      relations: ['plan', 'service'],
    });
    if (!entryType) {
      throw new EntityNotFoundError('EntryType', { id: itemId, plan: id });
    }

    const priceItem = await entryType.plan.getCurrentPrice(entryType.service.type, entryType.service.provider);
    await getManager().transaction(async manager => {
      await manager.update(EntryType, entryType.id, {
        validUntil: moment(),
        valid: false,
      });

      if (priceItem) {
        await manager.update(PriceItem, priceItem.id, {
          validUntil: moment(),
          current: false,
        });
      }
    });

    res.success();
  }

  @Delete('/:id/services/:serviceId', [OAuth.token([Scopes.plans.READ]), Exists.Plan, Permissions.canUserWritePlan])
  public static async deleteService(req: bacenRequest, res: BaseResponse) {
    const { id, serviceId } = req.params;
    const { provider } = req.query;

    const plan = await Plan.findOne({ where: { id }, relations: ['priceList', 'items'] });

    const entryType = isUUID(serviceId)
      ? await EntryType.safeFindOne({ where: { service: { id: serviceId }, plan: { id }, valid: true } })
      : await plan.getChargeSettingsByService(serviceId as ServiceType, provider);

    if (!entryType) {
      throw new EntityNotFoundError('EntryType', { id: serviceId, plan: id });
    }

    const priceItem = await plan.getCurrentPrice(entryType.service.type, entryType.service.provider);
    await getManager().transaction(async manager => {
      await manager.update(EntryType, entryType.id, {
        validUntil: moment(),
        valid: false,
      });

      if (priceItem) {
        await manager.update(PriceItem, priceItem.id, {
          validUntil: moment(),
          current: false,
        });
      }
    });

    res.success();
  }

  @Post('/', [OAuth.token([Scopes.plans.WRITE]), Permissions.canUserWritePlan])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const schema: {
      name: string;
      validFrom: moment.Moment;
      items: PlanServiceSchema[];
      billingSettings: BillingSettingsSchema;
      domain: string;
      default: boolean;
    } = req.body;
    const currentUser: User = req.user;

    const domainId = schema.domain;
    const isId = domainId && isUUID(domainId);
    let supplier: Domain;

    if (isId) {
      supplier = await Domain.safeFindOne({ where: { id: domainId } });
    } else if (domainId === 'root') {
      supplier = await Domain.getRootDomain();
    } else if (domainId === 'default') {
      supplier = await Domain.getDefaultDomain();
    }

    const plan = await PlanService.getInstance().createPlan({ supplier, planData: schema, createdBy: currentUser });

    res.success(plan);
  }
}
