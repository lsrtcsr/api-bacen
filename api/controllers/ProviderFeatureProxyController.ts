import { BaseResponse, Controller, Get } from 'ts-framework';
import { CustodyFeature, CustodyProvider } from '@bacen/base-sdk';
import { bacenRequest, Asset } from '../models';
import { ProviderManagerService } from '../services';
import { ProviderUtil } from '../utils';
import { Params } from '../filters';

@Controller('/')
export default class ProviderFeatureProxyController {
  @Get('/financial-institutions', [Params.isValidProvider('provider', false)])
  public static async getFinancialInstitutions(req: bacenRequest, res: BaseResponse) {
    const { provider, ...filters }: { provider: CustodyProvider } = req.query;

    const rootAsset = await Asset.getRootAsset();

    const custodyProvider = ProviderManagerService.getInstance().from(provider || rootAsset.provider);
    const feature = custodyProvider.feature(CustodyFeature.PARTICIPATING_INSTITUTION);
    await ProviderUtil.throwIfFeatureNotAvailable(feature);
    const participants = await feature.getAll(filters);

    res.success(participants);
  }
}
