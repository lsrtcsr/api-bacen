import { ConsumerStatus, UserRole, UserStatus, EventStatus } from '@bacen/base-sdk';
import { BaseResponse, Controller, Get, Post } from 'ts-framework';
import { In, FindConditions } from 'typeorm';
import * as moment from 'moment';
import Scopes from '../../config/oauth/scopes';
import { EntityNotFoundError, InvalidRequestError } from '../errors';
import { OAuth } from '../filters';
import {
  bacenRequest,
  ConsumerState,
  Transaction,
  User,
  UserState,
  Event,
  PostbackDelivery,
  Postback,
} from '../models';
import {
  ConsumerPipelinePublisher,
  MediatorPipelinePublisher,
  TransactionPipelinePublisher,
  EventPipelinePublisher,
} from '../services';

@Controller('/queues', [])
export default class QueueController {
  @Get('/mediators', [OAuth.token(Scopes.queues.READ)])
  public static async mediatorsQueue(req: bacenRequest, res: BaseResponse) {
    const service = MediatorPipelinePublisher.getInstance();
    const info = await service._channel.assertQueue(service.queue.name, {});

    res.success({
      subscribers: info.consumerCount,
      length: info.messageCount,
      name: service.queue.name,
    });
  }

  @Get('/consumers', [OAuth.token(Scopes.queues.READ)])
  public static async consumersQueue(req: bacenRequest, res: BaseResponse) {
    const service = ConsumerPipelinePublisher.getInstance();
    const info = await service._channel.assertQueue(service.queue.name, {});

    res.success({
      subscribers: info.consumerCount,
      length: info.messageCount,
      name: service.queue.name,
    });
  }

  @Get('/transactions', [OAuth.token(Scopes.queues.READ)])
  public static async transactionsQueue(req: bacenRequest, res: BaseResponse) {
    const service = TransactionPipelinePublisher.getInstance();
    const info = await service._channel.assertQueue(service.queue.name, {});

    res.success({
      subscribers: info.consumerCount,
      length: info.messageCount,
      name: service.queue.name,
    });
  }

  @Post('/users', [OAuth.token(Scopes.queues.WRITE)])
  public static async pushToQueue(req: bacenRequest, res: BaseResponse) {
    const { user, retry = false } = req.body;

    if (!user) {
      throw new InvalidRequestError('User is not defined');
    }

    const instance = await User.safeFindOne({
      where: { id: user },
      relations: ['consumer', 'consumer.states', 'states', 'domain'],
    });

    if (!instance) {
      throw new EntityNotFoundError('User not found', { user });
    }

    if (retry) {
      await instance.removeFailedState();
    }

    if (instance.role === UserRole.CONSUMER) {
      await ConsumerPipelinePublisher.getInstance().send(instance);

      return res.json({
        user: {
          id: instance.id,
          role: instance.role,
          status: instance.status,
          consumer: {
            status: instance.consumer.status,
          },
        },
      });
    }

    if (instance.role === UserRole.MEDIATOR) {
      await MediatorPipelinePublisher.getInstance().send(instance);

      return res.json({
        user: {
          id: instance.id,
          role: instance.role,
          status: instance.status,
        },
      });
    }

    throw new InvalidRequestError('Cannot push user to pipeline, role is not supported by queue services', {
      role: instance.role,
    });
  }

  @Post('/transactions', [OAuth.token(Scopes.queues.WRITE)])
  public static async pushTransactionToQueue(req: bacenRequest, res: BaseResponse) {
    const { transaction, forceAuthorizeOnProvider = false } = req.body;

    if (!transaction) {
      throw new InvalidRequestError('Transaction is not defined');
    }

    const instance = await Transaction.safeFindOne({
      where: { id: transaction },
      relations: [
        'states',
        'payments',
        'source',
        'source.user',
        'source.user.domain',
        'payments.asset',
        'payments.destination',
      ],
    });

    if (!instance) {
      throw new EntityNotFoundError('Transaction not found', { transaction });
    }

    if (forceAuthorizeOnProvider) {
      const { logger } = req;
      const providerName = instance.payments.find(payment => payment.asset.provider)?.asset?.provider;

      if (providerName) {
        logger.debug(
          'forceAuthorizeOnProvider flag was set. The transaction will be authorized on provider before being sent to the queue',
          {
            providerName,
          },
        );
        await instance.authorizeOnProvider({ providerName });
      } else {
        logger.warn(
          'forceAuthorizeOnProvider flag was set but none of the transactions payments had an asset with a provider, so it will be just sent to the queue',
        );
      }
    }

    await TransactionPipelinePublisher.getInstance().send(instance);

    return res.json({
      transaction: {
        id: instance.id,
        type: instance.type,
        status: instance.status,
      },
    });
  }

  @Post('/transactions/postbacks', [OAuth.token(Scopes.queues.WRITE)])
  public static async pushTransactionPostbackToQueue(req: bacenRequest, res: BaseResponse) {
    const { ids, after, before }: { ids: string[]; after: Date; before: Date } = req.body;

    if (!ids && !after) {
      throw new InvalidRequestError('Transaction id list or afeter date parameters is required');
    }

    let qb = Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.banking', 'banking')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('source.user', 'sourceuser')
      .leftJoinAndSelect('sourceuser.domain', 'sourceuserdomain')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.asset', 'paymentasset')
      .leftJoinAndSelect('payments.destination', 'destination')
      .leftJoinAndSelect('destination.user', 'destinationuser')
      .leftJoinAndSelect('destinationuser.domain', 'destinationuserdomain')
      .leftJoinAndSelect('transaction.states', 'transactionstates')
      .where('transaction.id IN (:...ids)', { ids });

    if (after && before) {
      qb = qb.andWhere('transaction.createdAt BETWEEN :after AND :before', {
        after: moment(after)
          .startOf('day')
          .toDate(),
        before: moment(before)
          .endOf('day')
          .toDate(),
      });
    } else if (after) {
      qb = qb.andWhere('transaction.createdAt >= :date', {
        date: moment(after)
          .startOf('day')
          .toDate(),
      });
    } else if (before) {
      qb = qb.andWhere('transaction.createdAt <= :date', {
        date: moment(before)
          .endOf('day')
          .toDate(),
      });
    }

    const transactions = await qb.getMany();

    if (!transactions || transactions.length === 0) {
      throw new EntityNotFoundError('Transaction not found');
    }

    const promises = transactions
      .map(transaction => Postback.fromTransaction(transaction, transaction.status))
      .reduce(
        (accumulator, current) => (current ? accumulator.add(current) : accumulator),
        new Set<Promise<Postback<any>>>(),
      );

    const postbacks = await Promise.all(Array.from(promises));
    postbacks.forEach(postback => postback.send());

    return res.success();
  }

  @Post('/consumers/postbacks', [OAuth.token(Scopes.queues.WRITE)])
  public static async pushConsumerPostbackToQueue(req: bacenRequest, res: BaseResponse) {
    const { ids, after, before }: { ids: string[]; after: Date; before: Date } = req.body;

    if (!ids && !after) {
      throw new InvalidRequestError('Consumer ids or afeter date parameters is required');
    }

    let qb = User.createQueryBuilder('user')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumerstates')
      .leftJoinAndSelect('consumer.documents', 'documents')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.wallets', 'wallets')
      .leftJoinAndSelect('wallets.states', 'walletstates')
      .leftJoinAndSelect('user.states', 'userstates')
      .where('user.id IN (:...ids)', { ids });

    if (after && before) {
      qb = qb.andWhere('user.createdAt BETWEEN :after AND :before', {
        after: moment(after)
          .startOf('day')
          .toDate(),
        before: moment(before)
          .endOf('day')
          .toDate(),
      });
    } else if (after) {
      qb = qb.andWhere('user.createdAt >= :date', {
        date: moment(after)
          .startOf('day')
          .toDate(),
      });
    } else if (before) {
      qb = qb.andWhere('user.createdAt <= :date', {
        date: moment(before)
          .endOf('day')
          .toDate(),
      });
    }

    const users = await qb.getMany();

    if (!users || users.length === 0) {
      throw new EntityNotFoundError('Transaction not found');
    }

    const promises = users
      .map(user => Postback.fromUser(user, user.consumer.status))
      .reduce(
        (accumulator, current) => (current ? accumulator.add(current) : accumulator),
        new Set<Promise<Postback<any>>>(),
      );

    const postbacks = await Promise.all(Array.from(promises));
    postbacks.forEach(postback => postback.send());

    return res.success();
  }

  @Post('/events', [OAuth.token(Scopes.queues.WRITE)])
  public static async pushEventsToQueue(req: bacenRequest, res: BaseResponse) {
    const { events }: { events: string[] } = req.body;
    const { status } = req.query;

    if (!events && !status) {
      throw new InvalidRequestError('Events list is required');
    }

    const where: FindConditions<Event> =
      events && events.length > 0 ? { id: In(events) } : { status: status as EventStatus };
    const instances = await Event.safeFind({ where });

    if (!instances) {
      throw new EntityNotFoundError('Events not found');
    }

    instances.forEach(event => event.publishToPipeline());

    return res.success(instances.map(event => event.toSimpleJSON()));
  }
}
