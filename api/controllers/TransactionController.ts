/* tslint:disable */
import { AuthorizerEvent, PaymentType, TransactionStatus, CustodyFeature } from '@bacen/base-sdk';
import { DictWebService } from '@bacen/dict-sdk';
import { BaseResponse, Controller, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { getManager } from 'typeorm';
import { InfractionType } from '@bacen/dict-service';
import dictConfig from '../../config/dict.config';
import serverConfig from '../../config/server.config';
import Scopes from '../../config/oauth/scopes';
import { Idempotent, UseLock } from '../decorators';
import { EntityNotFoundError, InvalidRequestError, wrapError } from '../errors';
import { OAuth, Params, Permissions, Request, Exists, Amount } from '../filters';
import {
  bacenRequest,
  LogType,
  OAuthAccessToken,
  RequestLog,
  Payment,
  Transaction,
  Asset,
  Domain,
  TRANSACTION_REFUNDABLE_STATES,
  TRANSACTION_NON_CANCELABLE_STATES,
} from '../models';
import { AuthorizationRequestDto } from '../schemas/dto/AuthorizationRequestDto';
import {
  AssetService,
  AuthorizationErrorMessage,
  AuthorizationService,
  PgIdempotenceStore,
  ProviderManagerService,
} from '../services';
import { CDTAuthorizationService } from '../services/authorization/CDTAuthorizationService';
import { RequestLogService } from '../services/RequestLogService';
import Config from '../../config';
import { ProviderUtil, isUUID } from '../utils';
import * as Serializers from '../serializers';

@Controller('/transactions')
export default class TransactionController {
  @Get('/:id', [
    OAuth.token([Scopes.transactions.READ]),
    Exists.Transaction('id'),
    Permissions.canUserReadOneTransaction,
  ])
  public static async findById(req: bacenRequest, res: BaseResponse) {
    const id = req.param('id');

    let qb = Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.banking', 'banking')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('source.user', 'sourceuser')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.asset', 'asset')
      .leftJoinAndSelect('payments.destination', 'destination')
      .leftJoinAndSelect('destination.user', 'destinationuser')
      .where('transaction.deletedAt IS NULL');

    if (isUUID(id)) {
      qb = qb.andWhere('transaction.id = :id', { id });
    } else {
      // We should think about moving these two fields to columns in SQL instead of using them inside json columns.
      qb = qb.andWhere(
        "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
        { hash: id, instructionId: id },
      );
    }
    const transaction = await qb.getOne();

    return res.success(transaction.toJSON());
  }

  @Post('/:id/cancel', [OAuth.token([Scopes.transactions.MODIFY])])
  public static async cancelTransaction(req: bacenRequest, res: BaseResponse) {
    const transaction = await Transaction.findOne(req.param('id'), {
      relations: ['states'],
    });

    if (!transaction) {
      throw new EntityNotFoundError('Transaction', { id: req.param('id') });
    }

    if (TRANSACTION_NON_CANCELABLE_STATES.includes(transaction.status)) {
      throw new InvalidRequestError('The transaction is already in a final state and cannot be canceled', {
        transaction,
      });
    }

    const failedTransaction = await transaction.manuallyFailTransaction({
      reason: 'manual',
    });

    res.success(failedTransaction.toJSON());
  }

  @UseLock('walletId')
  @Post('/authorize/cdt', [Amount.isValidAmount])
  public static async authorizeTransactionCdt(req: bacenRequest, res: BaseResponse) {
    res.send(
      await CDTAuthorizationService.getInstance().authorize({
        ...req.body,
        asset: req.body?.asset || AssetService.getInstance().rootAsset.code,
      }),
    );
  }

  @Post('/authorize/cdt/dry-run', [Amount.isValidAmount])
  public static async dryRunAuthorizeTransactionCdt(req: bacenRequest, res: BaseResponse) {
    res.json(
      await CDTAuthorizationService.getInstance().dryRun({
        ...req.body,
        asset: req.body?.asset || AssetService.getInstance().rootAsset.code,
      }),
    );
  }

  @Post('/authorize/cdt/revert', [])
  public static async revertTransactionCdt(req: bacenRequest, res: BaseResponse) {
    res.send(await CDTAuthorizationService.getInstance().revert(req.body.transactionId));
  }

  @Idempotent({ store: PgIdempotenceStore })
  @UseLock('walletId')
  @Post('/authorize', [OAuth.token([Scopes.authorization.WRITE]), Amount.isValidAmount, Params.isValidAuthorization()])
  public static async authorizeTransaction(req: bacenRequest, res: BaseResponse) {
    const authorizationRequestDto: AuthorizationRequestDto = req.body;

    // Ensure additional data is a valid object
    authorizationRequestDto.additionalData = authorizationRequestDto.additionalData || {};

    // Incident response: We cannot allow "hash" in additional data
    if (authorizationRequestDto.additionalData.hash) {
      authorizationRequestDto.additionalData.externalHash =
        authorizationRequestDto.additionalData.externalHash || authorizationRequestDto.additionalData.hash || undefined;
      authorizationRequestDto.additionalData.hash = undefined;
    }

    if (
      authorizationRequestDto.type !== PaymentType.CARD &&
      authorizationRequestDto.type !== PaymentType.TRANSACTION_REVERSAL &&
      authorizationRequestDto.event === AuthorizerEvent.SETTLEMENT &&
      authorizationRequestDto.type !== undefined
    ) {
      throw new HttpError('Payment types allowed: card and transaction_reversal', HttpCode.Client.BAD_REQUEST);
    }

    const result = await AuthorizationService.getInstance().authorize(authorizationRequestDto);

    res.success(result);

    const { authorization } = req.headers;
    const [type, token] = authorization.split(' ');
    const oauthAccessToken = await OAuthAccessToken.findOne({ where: { accessToken: token, tokenType: type } });

    await RequestLogService.getInstance().log({
      token: oauthAccessToken,
      type: LogType.AUTHORIZATION,
      request: authorizationRequestDto,
      response: result,
    } as RequestLog);
  }

  @Post('/authorize/dry-run', [OAuth.token([Scopes.authorization.READ]), Params.isValidAuthorization()])
  public static async dryRunAuthorizeTransaction(req: bacenRequest, res: BaseResponse) {
    const authorizationRequestDto: AuthorizationRequestDto = req.body;

    if (
      authorizationRequestDto.type !== PaymentType.CARD &&
      authorizationRequestDto.type !== PaymentType.TRANSACTION_REVERSAL
    ) {
      throw new HttpError('Payment types allowed: card and transaction_reversal', HttpCode.Client.BAD_REQUEST);
    }

    const result = await AuthorizationService.getInstance().dryRun(authorizationRequestDto);

    res.success(result);
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:id/refund', [OAuth.token([Scopes.transactions.CANCEL]), Exists.Transaction('id'), Request.signing()])
  public static async refund(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const endToEndId = req.get('X-EndToEnd-Id');
    const {
      reasonCode,
      reason,
      amount: refundedAmount,
    }: {
      reasonCode?: string;
      reason?: string;
      amount?: string;
    } = req.body;

    if (!reason && !reasonCode) {
      throw new InvalidRequestError('reason or reasonCode must be provided');
    }

    let qb = Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('source.user', 'sourceuser')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.asset', 'asset')
      .leftJoinAndSelect('payments.destination', 'destination')
      .leftJoinAndSelect('destination.user', 'destinationuser')
      .leftJoinAndSelect('destination.assetRegistrations', 'assetregistrations')
      .leftJoinAndSelect('assetregistrations.asset', 'assetregistrationsasset')
      .leftJoinAndSelect('assetregistrations.states', 'assetregistrationsassetstates')
      .where('transaction.deletedAt IS NULL');

    if (isUUID(id)) {
      qb = qb.andWhere('transaction.id = :id', { id });
    } else {
      qb = qb.andWhere(
        "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
        { hash: id, instructionId: id },
      );
    }

    const transaction = await qb.getOne();

    if (!TRANSACTION_REFUNDABLE_STATES.includes(transaction.status)) {
      throw new InvalidRequestError('Transaction is not in the required state for the requested operation', {
        entityId: transaction.id,
        requiredStatus: TRANSACTION_REFUNDABLE_STATES,
      });
    }

    const deposit = transaction.payments.find((p) => p.type === PaymentType.DEPOSIT);
    if (!deposit) {
      throw new InvalidRequestError('Deposit not found in the given transaction');
    }

    const amount = refundedAmount ? String(refundedAmount) : deposit.amount;
    const { type } = transaction.additionalData;

    const asset = await Asset.getByIdOrCode(deposit.asset.id, { relations: ['issuer'] });

    const refundTransaction = await getManager().transaction(async (manager) => {
      const trx = await asset.destroy({
        manager,
        amount,
        bypassReversalCheck: true,
        paymentType: PaymentType.TRANSACTION_REVERSAL,
        createdBy: req.accessToken,
        targetWallet: deposit.destination,
        additionalData: {
          reasonCode,
          reason,
          type,
          endToEndId: transaction?.additionalData?.endToEndId,
          instructionId: endToEndId,
          reversal: true,
          arrangement: transaction.additionalData.arrangement ?? Config.spb.defaultArrangement,
          originalTransaction: transaction.additionalData.hash,
          source: (transaction.additionalData as any)?.destination ?? undefined,
          destination: (transaction.additionalData as any)?.source ?? undefined,
        } as any,
      });

      const thinPayment = trx.payments.find((p) => p.type === PaymentType.TRANSACTION_REVERSAL);
      const refundPayment = await manager.findOne(Payment, {
        where: { id: thinPayment.id },
        relations: ['transaction', 'transaction.source'],
      });

      const provider = ProviderManagerService.getInstance().from(deposit.asset.provider);
      const depositFeature = provider.feature(CustodyFeature.DEPOSIT);
      await ProviderUtil.throwIfFeatureNotAvailable(depositFeature);
      await depositFeature.refund({
        amount,
        reason,
        reasonCode,
        extra: { type, endToEndId },
        refundPayment: refundPayment as any,
        originalPaymentId: deposit.id,
      });

      return trx;
    });

    res.success(Serializers.transactionSerializer(refundTransaction));
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:id/dict/infraction', [OAuth.token([Scopes.transactions.READ]), Permissions.canUserReadOneTransaction])
  public static async createInfraction(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { ispb, infractionType, reportDetails } = req.body;

    try {
      const transaction = await Transaction.createQueryBuilder('transaction')
        .leftJoinAndSelect('transaction.source', 'source')
        .leftJoinAndSelect('source.user', 'sourceUser')
        .leftJoinAndSelect('transaction.payments', 'payments')
        .leftJoinAndSelect('payments.destination', 'destination')
        .leftJoinAndSelect('destination.user', 'destinationUser')
        .where('transaction.deletedAt IS NULL')
        .where('transaction.id = :transactionId', { transactionId: id })
        .getOne();

      if (!transaction) {
        throw new HttpError('Could not find transaction', HttpCode.Client.BAD_REQUEST);
      }

      let domainId: string;

      const [payment] = transaction.payments;
      const paymentTypes = transaction.payments.map((payment) => payment.type);

      domainId = paymentTypes.includes(PaymentType.DEPOSIT)
        ? payment.destination.user.domainId
        : transaction.source.user.domainId;

      if (!transaction.additionalData?.endToEndId) {
        throw new HttpError('Invalid transaction, could not find external id', HttpCode.Client.BAD_REQUEST);
      }

      if (!domainId) {
        throw new HttpError('Could not find domain', HttpCode.Server.INTERNAL_SERVER_ERROR);
      }

      const domain = await Domain.findOne(domainId);

      const response = await DictWebService.getInstance()
        .infraction()
        .create({
          ispb: ispb || dictConfig.ispb,
          infractionType,
          reportDetails,
          transactionId: transaction.additionalData?.endToEndId,
          bacenTransactionId: transaction.id,
          instanceId: serverConfig.instanceId,
          domainId: domain.id,
        });

      res.success(response);
    } catch (error) {
      wrapError(error);
    }
  }

  // NOTE: This is a temporary code, this should be erased in 2021 after Creditas upgrades some of their services
  @Idempotent({ store: PgIdempotenceStore })
  @UseLock('walletId')
  @Post('/authorize/creditas', [OAuth.token([Scopes.authorization.WRITE]), Params.isValidAuthorization()])
  public static async authorizeTransactionCreditas(req: bacenRequest, res: BaseResponse) {
    const authorizationRequestDto: AuthorizationRequestDto = req.body;

    // Ensure additional data is a valid object
    authorizationRequestDto.additionalData = authorizationRequestDto.additionalData || {};

    // Incident response: We cannot allow "hash" in additional data
    if (authorizationRequestDto.additionalData.hash) {
      authorizationRequestDto.additionalData.externalHash =
        authorizationRequestDto.additionalData.externalHash || authorizationRequestDto.additionalData.hash || undefined;
      authorizationRequestDto.additionalData.hash = undefined;
    }

    if (
      authorizationRequestDto.type !== PaymentType.CARD &&
      authorizationRequestDto.type !== PaymentType.TRANSACTION_REVERSAL &&
      authorizationRequestDto.event === AuthorizerEvent.SETTLEMENT &&
      authorizationRequestDto.type !== undefined
    ) {
      throw new HttpError('Payment types allowed: card and transaction_reversal', HttpCode.Client.BAD_REQUEST);
    }

    let result = await AuthorizationService.getInstance().authorize(authorizationRequestDto);
    // This is a logic custom made for Creditas. Basically if the VAVR authorization returns an error
    // We should try to authorize LMTC instead. Here, we'll just do two authorizations.
    // Because authorizations with errors are really quick, we shouldn't see much of performance degradation
    let { asset } = authorizationRequestDto;
    if (
      !result.authorized &&
      authorizationRequestDto.asset === 'VAVR' &&
      authorizationRequestDto.event === AuthorizerEvent.AUTHORIZATION
    ) {
      // Here we ignore if the authorization error is insuficient balance or anything else.
      // In case is another type of error, like User not ready, this flow will execute and the same errror
      // will be thrown in the function below, as validation errors are really quick, i did not check the error to
      // be explictly insuficient balance to avoid iterating through an array searching for 2 items.
      // If thats not the best idea, we should also check if the authorization reason is
      // AuthorizationErrorMessage.PENDING_TRANSACTIONS || AuthorizationErrorMessage.INSUFICCIENT_BALANCE
      result = await AuthorizationService.getInstance().authorize({ ...authorizationRequestDto, asset: 'LMTC' });
      asset = 'LMTC';
    }

    // We should create a better handle for this point.
    // Basically, if i send BRLP for a authorization_reversal of a VAVR transaction, it'll revert the VAVR and return BRLP
    // this is because if the statement above does not execute, it'll basically foward what the user sent in the request, and
    // today the AuthorizationService only ignores the asset sent for this operations.
    res.send({ ...result, asset });

    const { authorization } = req.headers;
    const [type, token] = authorization.split(' ');
    const oauthAccessToken = await OAuthAccessToken.findOne({ where: { accessToken: token, tokenType: type } });

    await RequestLogService.getInstance().log({
      token: oauthAccessToken,
      type: LogType.AUTHORIZATION,
      request: authorizationRequestDto,
      response: result,
    } as RequestLog);
  }
}
