import {
  AccountType,
  BoletoEmissionRequest,
  CustodyFeature,
  CustodyProvider,
  PaymentStatus,
  PaymentType,
  TransactionType,
  TransitoryAccountType,
  UserSchema,
  WalletStatus,
  ServiceType,
} from '@bacen/base-sdk';
import { isValid as isValidCnpj } from '@fnando/cnpj/dist/node';
import * as cpf from 'cpf';
import { BaseRequest, BaseResponse, Controller, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { getManager, getConnection } from 'typeorm';
import Config from '../../config';
import Scopes from '../../config/oauth/scopes';
import { Idempotent, ServiceCharged, UseLock } from '../decorators';
import {
  EntityNotFoundError,
  ForbiddenRequestError,
  InternalServerError,
  InvalidParameterError,
  InvalidRequestError,
} from '../errors';
import { Amount, Assets, Limits, OAuth, Params, Permissions, Request, Status } from '../filters';
import { Address, Asset, bacenRequest, Payment, Phone, Transaction, User, Wallet } from '../models';
import {
  BillingService,
  EventHandlingGateway,
  PgIdempotenceStore,
  ProviderManagerService,
  TransactionPipelinePublisher,
} from '../services';
import { BoletoConsolidationService } from '../services/conciliation/BoletoConsolidationService';
import { ProviderUtil } from '../utils';
import { KYCUtil } from '../utils/KYCUtil';
import { DEFAULT_BOLETO_EXPIRATION } from './PaymentController';
import * as Serializers from '../serializers';
import { checkIfAssetIsRegisteredOnWallet } from '../filters/assets/IsAssetRegistered';

@Controller('/boletos')
export default class BoletoController {
  @Get('/find', [OAuth.token([Scopes.payments.READ]), Params.isValidProvider('provider', false)])
  public static async find(req: bacenRequest, res: BaseResponse) {
    const {
      walletId,
      start,
      provider: providerCode = Config.provider.defaultBoletoIssuingProvider,
      end = new Date(),
      skip = 0,
      limit = Config.api.defaultPaginationLimit,
      asset: assetIdOrCode,
      ...extra
    }: {
      walletId: string;
      start: Date;
      provider?: string;
      end: Date;
      skip: number;
      limit: number;
      asset: string;
    } = req.query;

    if (!walletId) {
      throw new InvalidRequestError('The walletId parameter is required');
    }

    if (req.user.role === 'consumer') {
      const isConsumerOwnerOfWallet = !!(await Wallet.safeCount({
        where: { id: walletId, user: { id: req.user.id } },
      }));

      if (!isConsumerOwnerOfWallet) {
        throw new InvalidRequestError('Requesting consumer is not owner of wallet', {
          userId: req.user.id,
          walletId,
        });
      }
    }

    if (!start) {
      throw new InvalidRequestError('The start date of the query period is required');
    }

    const asset = await Asset.getByIdOrCode(assetIdOrCode || 'root');

    if (!asset) throw new EntityNotFoundError(`Asset with code ${assetIdOrCode} not found`);

    const provider = ProviderManagerService.getInstance().from(asset.provider);
    const boleto = provider.feature(CustodyFeature.BOLETO_EMISSION);

    try {
      const { boletos, count } = await boleto.getByDateRange(walletId, start, end, { skip, limit, ...extra });

      res.set('X-Data-Length', count.toString());
      res.set('X-Data-Skip', req.query.skip || '0');
      res.set('X-Data-Limit', req.query.limit || Config.api.defaultPaginationLimit);

      return res.success(boletos);
    } catch (exception) {
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Error attempting to retrieve Boletos from the given period, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else {
        throw new InternalServerError('Could not find the Boleto, unknown error', { exception });
      }
    }
  }

  // @ServiceCharged(ServiceType.BOLETO_VALIDATION)
  @Get('/validate', [OAuth.token([Scopes.payments.READ]), Params.isValidProvider('provider', false)])
  public static async validateBoleto(req: BaseRequest, res: BaseResponse) {
    const {
      digitableLine,
      provider: providerParam = Config.provider.defaultBoletoPaymentProvider,
      asset: assetIdOrCode,
      ...extra
    }: { digitableLine: string; provider?: CustodyProvider; asset: string } = req.query;

    if (!digitableLine || !digitableLine.length) {
      throw new InvalidRequestError('digitableLine is required');
    }

    // Get source asset for payment, defaults to root
    const paymentAsset = await (assetIdOrCode ? Asset.getByIdOrCode(assetIdOrCode) : Asset.getRootAsset());
    if (!paymentAsset) throw new EntityNotFoundError(`Asset with code "${assetIdOrCode}"`);

    // Get provider asset for payment processing
    const providerAssetCode = providerParam || paymentAsset.provider;
    const providerAsset = await Asset.getByProvider(providerAssetCode);
    if (!providerAsset) throw new EntityNotFoundError(`Transitory asset for provider "${providerAssetCode}"`);

    // Get boleto feature from provider
    const provider = ProviderManagerService.getInstance().from(providerAsset.provider);
    const boleto = provider.feature(CustodyFeature.BOLETO_PAYMENT);
    await ProviderUtil.throwIfFeatureNotAvailable(boleto);

    try {
      const digitableLineDigitsOnly = digitableLine.replace(/\D/g, '');
      const response = await boleto.validate(digitableLineDigitsOnly, extra);
      return res.success(response);
    } catch (exception) {
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Could not validate the Boleto, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else {
        throw new InternalServerError('Could not validate the Boleto, unknown error');
      }
    }
  }

  @Get('/:boletoId', [OAuth.token([Scopes.payments.READ]), Params.isValidProvider('provider', false)])
  @Get('/:boletoId/:provider', [OAuth.token([Scopes.payments.READ]), Params.isValidProvider('provider', false)])
  public static async getBoleto(req: BaseRequest, res: BaseResponse) {
    const { boletoId, provider: providerParam = Config.provider.defaultBoletoIssuingProvider, ...extra } = req.params;
    const { asset: assetIdOrCode } = req.query;

    const asset = await Asset.getByIdOrCode(assetIdOrCode || 'root');

    if (!asset) throw new EntityNotFoundError(`Asset with code ${assetIdOrCode} not found`);

    // TODO: Ensure user can see this boleto
    const provider = assetIdOrCode
      ? ProviderManagerService.getInstance().from(asset.provider)
      : ProviderManagerService.getInstance().from(providerParam as CustodyProvider);

    const boleto = provider.feature(CustodyFeature.BOLETO_EMISSION);
    await ProviderUtil.throwIfFeatureNotAvailable(boleto);

    try {
      const response = await boleto.getById(boletoId, extra);
      return res.success(response);
    } catch (exception) {
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Could not find the Boleto, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else {
        throw new InternalServerError('Could not find the Boleto, unknown error', { exception });
      }
    }
  }

  @Idempotent({ store: PgIdempotenceStore })
  @ServiceCharged(ServiceType.BOLETO_EMISSION)
  @Post('/emit', [
    OAuth.token([Scopes.payments.WRITE]),
    Request.signing(),
    Permissions.canUserWritePayment('destination'),
    Params.isValidProvider('provider', false),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.body.destination,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Amount.isValidAmount,
  ])
  public static async createBoleto(req: bacenRequest, res: BaseResponse) {
    let payload: BoletoEmissionRequest;

    try {
      payload = await BoletoEmissionRequest.fromPayload(req.body);
    } catch (err) {
      throw new InvalidParameterError('Payload', err);
    }

    const asset = await Asset.getByIdOrCode(payload.asset || 'root');
    if (!asset) throw new EntityNotFoundError('asset', { provider: payload.provider });

    const provider = ProviderManagerService.getInstance().from(
      payload.provider || Config.provider.defaultBoletoIssuingProvider,
    );
    const boleto = provider.feature(CustodyFeature.BOLETO_EMISSION);
    await ProviderUtil.throwIfFeatureNotAvailable(boleto);

    try {
      // only necessary for CDT provider
      if (payload.payer && provider.type === CustodyProvider.CDT_VISA) {
        if (!payload.payer.consumer || !payload.payer.consumer.taxId) {
          throw new InvalidRequestError('The third party info is required');
        }

        if (!payload.payer.consumer.type || payload.payer.consumer.type === AccountType.PERSONAL) {
          payload.payer.consumer.type = AccountType.PERSONAL;
          const payerInfo = await this.validatePersonalPayer(payload.payer);
          payload.payer.name = payerInfo.name;
          payload.payer.consumer.birthday = payerInfo.consumer.birthday;
          payload.payer.consumer.motherName = payerInfo.consumer.motherName;
        } else {
          if (!isValidCnpj(payload.payer.consumer.taxId)) {
            throw new InvalidRequestError('The CNPJ is not valid', { taxId: payload.payer.consumer.taxId });
          }
          const companyInfo = await KYCUtil.fillBasicCompanyData(payload.payer);
          payload.payer.consumer.companyData = companyInfo.consumer.companyData;
          payload.payer.name = companyInfo.consumer.companyData.tradeName;
        }
      }

      // Get wallet information and emit the boleto using the provider service
      const destinationWallet = await Wallet.createQueryBuilder('wallet')
        .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
        .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
        .leftJoinAndSelect('assetRegistrations.asset', 'assets')
        .where('wallet.id = :id', { id: payload.destination })
        .andWhere('wallet.deletedAt IS NULL')
        .getOne();

      if (!destinationWallet) {
        return res.error(new InvalidRequestError('Destination wallet was not found'));
      }

      // throws 400 if asset is not registered on wallet.
      checkIfAssetIsRegisteredOnWallet(asset, destinationWallet);

      const response = await boleto.emit(payload.amount, destinationWallet, {
        expiresAt: payload.expiresAt || new Date(Date.now() + DEFAULT_BOLETO_EXPIRATION),
        payer: payload.payer,
        fine: payload.fine,
        discount: payload.discount,
        message: payload.message,
      });

      return res.success(response);
    } catch (exception) {
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Could not emit the Boleto, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else if (exception.details && exception.details.responseStatus) {
        throw new HttpError(exception.details.message, exception.details.responseStatus, {
          ...exception.details,
        });
      } else if (exception instanceof HttpError) {
        throw exception;
      } else {
        throw new InternalServerError('Could not emit the Boleto, unknown error', { exception });
      }
    }
  }

  @Idempotent({ store: PgIdempotenceStore })
  @Post('/:boletoId/cancel', [
    OAuth.token([Scopes.payments.WRITE]),
    Request.signing(),
    Params.isValidProvider('provider', false),
    // TODO: Ensure boleto belongs to user
  ])
  public static async cancelBoleto(req: bacenRequest, res: BaseResponse) {
    const { boletoId } = req.params;
    const {
      provider: providerParam = Config.provider.defaultBoletoIssuingProvider,
      reason,
      details,
      asset: assetIdOrCode,
    }: { provider: CustodyProvider; reason: string; details: string; asset: string } = req.body;

    const asset = await Asset.getByIdOrCode(assetIdOrCode || 'root');
    if (!asset) throw new EntityNotFoundError(`Asset with code ${assetIdOrCode} not found`);

    const provider = ProviderManagerService.getInstance().from(providerParam);
    const boleto = provider.feature(CustodyFeature.BOLETO_EMISSION);
    await ProviderUtil.throwIfFeatureNotAvailable(boleto);

    try {
      const response = await boleto.cancel(boletoId, { reason, details });
      return res.success(response);
    } catch (exception) {
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Could not cancel the Boleto, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else {
        throw new InternalServerError('Could not cancel the Boleto, unknown error', { exception });
      }
    }
  }

  static async validatePersonalPayer(user: UserSchema): Promise<User> {
    // TODO: Support Corporate payers
    if (!cpf.isValid(user.consumer.taxId)) {
      throw new InvalidRequestError('The CPF is not valid.', { taxId: user.consumer.taxId });
    }

    if (user.consumer.phones && user.consumer.phones.length) {
      for (const phoneSchema of user.consumer.phones) {
        const phone = Phone.create({
          type: phoneSchema.type,
          number: phoneSchema.number,
          code: phoneSchema.code,
          extension: phoneSchema.extension,
        });
        const errors = await phone.validate();
        if (errors.length) {
          throw new InvalidRequestError('The payer phone is not valid.', { taxId: user.consumer.taxId });
        }
      }
    } else {
      throw new InvalidRequestError('The payer phone is required.');
    }

    if (user.consumer.addresses && user.consumer.addresses.length) {
      for (const addressSchema of user.consumer.addresses) {
        await Address.fromCEP(addressSchema.code, { ...(addressSchema as Address) }, true);
      }
    } else {
      throw new InvalidRequestError('The payer address is required.');
    }

    return User.create(user as User);
  }

  @Idempotent({ store: PgIdempotenceStore })
  @UseLock('source')
  @ServiceCharged(ServiceType.BOLETO_PAYMENT)
  @Post('/pay', [
    OAuth.token([Scopes.payments.WRITE]),
    Request.signing(),
    Assets.hasProvider('code', false),
    Permissions.canUserWritePayment('source'),
    OAuth.otp(false, async (req) => {
      const wallet = await Wallet.safeFindOne({
        where: { id: req.body.source },
        relations: ['user', 'user.twoFactorSecret'],
      });
      if (!wallet || !wallet.user) {
        throw new ForbiddenRequestError('Could not find user informaton');
      }
      return wallet.user;
    }),
    Status.isWalletInStatus({
      walletIdFn: (req) => req.body.source,
      requiredStatus: WalletStatus.REGISTERED_IN_STELLAR,
    }),
    Amount.isValidAmount,
    Limits.transactionAmount,
  ])
  public static async payBoleto(req: bacenRequest, res: BaseResponse) {
    const {
      source,
      digitableLine,
      asset: assetIdOrCode,
      provider: providerParam = Config.provider.defaultBoletoPaymentProvider,
      amount,
      autoConfirm = true,
      opConfirm = null,
      extra = {},
    } = req.body;

    if (!amount) {
      throw new InvalidParameterError('Amount');
    }

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();

    try {
      await queryRunner.connect();
      await queryRunner.startTransaction();

      const boletoConsolidationService = BoletoConsolidationService.getInstance();

      // Get source asset for payment, defaults to root
      const paymentAsset = await (assetIdOrCode ? Asset.getByIdOrCode(assetIdOrCode) : Asset.getRootAsset());
      if (!paymentAsset) throw new EntityNotFoundError(`Asset with code "${assetIdOrCode}"`);

      // Get provider asset for payment processing
      const providerAssetCode = providerParam || paymentAsset.provider;
      const providerAsset = await Asset.getByProvider(providerAssetCode);
      if (!providerAsset) throw new EntityNotFoundError(`Transitory asset for provider "${providerAssetCode}"`);

      // Get boleto feature from provider
      const provider = ProviderManagerService.getInstance().from(providerAsset.provider);
      const boleto = provider.feature(CustodyFeature.BOLETO_PAYMENT);
      await ProviderUtil.throwIfFeatureNotAvailable(boleto);

      // Check if asset conversion is needed before boleto payment
      const recipientIsIssuer = paymentAsset.provider === providerAsset.provider;

      // Get wallet information and emit the boleto using the provider service
      const sourceWallet = await Wallet.safeFindOne({
        where: { id: source },
        relations: [
          'assetRegistrations',
          'assetRegistrations.states',
          'assetRegistrations.asset',
          'user',
          'user.consumer',
        ],
      });

      // throws 400 if asset is not registered on wallet.
      checkIfAssetIsRegisteredOnWallet(paymentAsset, sourceWallet);

      const { taxId } = sourceWallet.user.consumer;
      const digitableLineDigitsOnly = digitableLine.replace(/\D/g, '');

      if (opConfirm) {
        const response = await boleto.pay(
          digitableLineDigitsOnly,
          { type: PaymentType.BOLETO, amount },
          { extra: { taxId, autoConfirm, opConfirm, ...extra } },
        );
        return res.success(response);
      }

      const response = await boleto.validate(digitableLineDigitsOnly, { ...extra });

      if (!response) {
        throw new HttpError('Error validating boleto.', HttpCode.Client.BAD_REQUEST);
      }

      // TODO use currency.js
      if (
        response?.paymentInfo?.totalAmount &&
        parseFloat(parseFloat(response?.paymentInfo?.totalAmount).toFixed(2)) !==
          parseFloat(parseFloat(amount).toFixed(2)) &&
        !response?.paymentInfo?.allowChangeValue
      ) {
        throw new HttpError(`Value to be paid is ${response?.paymentInfo?.totalAmount}`, HttpCode.Client.BAD_REQUEST);
      }

      let transitoryWallet: Wallet;
      let transaction: Transaction;

      if (recipientIsIssuer) {
        transaction = await paymentAsset.destroy({
          manager: queryRunner.manager,
          amount,
          paymentType: PaymentType.BOLETO,
          createdBy: req.accessToken,
          targetWallet: sourceWallet,
        });
      } else {
        transitoryWallet = await Wallet.getTransitoryAccountByType({ type: TransitoryAccountType.BOLETO_PAYMENT });

        if (!transitoryWallet)
          throw new HttpError('Cannot find card transitory wallet', HttpCode.Server.INTERNAL_SERVER_ERROR);

        const transactionSchema = Transaction.create({
          source,
          createdBy: req.accessToken,
          type: TransactionType.PAYMENT,
        });

        const paymentSchema = Payment.create({
          asset: paymentAsset,
          type: PaymentType.BOLETO,
          amount,
          destination: transitoryWallet,
        });

        transaction = await boletoConsolidationService.chargeBoletoPayment({
          paymentSchema,
          transactionSchema,
          manager: queryRunner.manager,
        });
      }

      if (req.serviceFee) {
        transaction = await BillingService.getInstance().chargeServiceFee(
          req.serviceFee,
          transaction,
          queryRunner.manager,
        );
      }

      const boletoPayment = transaction.payments.find((p) => p.type === PaymentType.BOLETO);
      const payment = await queryRunner.manager.findOne(Payment, {
        where: { id: boletoPayment.id },
        relations: ['transaction', 'transaction.source', 'asset'],
      });
      const result = await boleto.pay(digitableLineDigitsOnly, payment, {
        extra: { taxId, autoConfirm, opConfirm, ...extra },
      });

      if (result.id === payment.id && result.status === PaymentStatus.SETTLED) {
        await queryRunner.manager.update(Payment, payment.id, { status: result.status });
        await queryRunner.commitTransaction();

        await TransactionPipelinePublisher.getInstance().send(transaction);
        await EventHandlingGateway.getInstance().process(ServiceType.BOLETO_PAYMENT, transaction);
      } else {
        await queryRunner.rollbackTransaction();
        throw new HttpError('Could not pay the Boleto', HttpCode.Server.INTERNAL_SERVER_ERROR);
      }

      return res.success(Serializers.transactionSerializer(transaction));
    } catch (exception) {
      await queryRunner.rollbackTransaction();
      if (exception.response && exception.response.data) {
        throw new HttpError(
          'Could not pay the Boleto, something went wrong with the external provider',
          exception.response.status,
          {
            message: exception.response.data.message,
            stackId: exception.response.data.stackId,
            ...exception.response.data.details,
          },
        );
      } else {
        throw new InternalServerError('Could not pay the Boleto, unknown error', { exception });
      }
    }
  }

  @Post('/consolidate', [
    OAuth.token([Scopes.payments.WRITE]),
    Permissions.canUserWritePayment('source'),
    Status.isWalletInStatus({ walletIdFn: (req) => req.body.source, requiredStatus: WalletStatus.READY }),
  ])
  public static async consolidateBole(req: bacenRequest, res: BaseResponse) {
    const {
      source,
      sourceAsset,
      targetAsset = 'root',
    }: { source?: string; sourceAsset: string; targetAsset?: string } = req.body;
    const boletoConsolidationService = BoletoConsolidationService.getInstance();

    // TODO: Don't make this validations by hand, use class-validator for the DTO
    const errors: string[] = [];
    if (!sourceAsset || typeof sourceAsset !== 'string') {
      errors.push("Body param 'sourceAsset' must be specified and must be a string");
    }

    if (source && typeof source !== 'string') {
      errors.push("Body param 'source' must be a string");
    }

    if (typeof targetAsset !== 'string') {
      errors.push("body param 'targetAsset' must be a string");
    }

    if (errors.length)
      throw new HttpError('Invalid payload', HttpCode.Client.BAD_REQUEST, {
        errors,
      });

    const balances = await boletoConsolidationService.getWalletsWithExecutedBalance(source, sourceAsset);

    const stellarBalances = await getManager().transaction(
      async (tx) =>
        await boletoConsolidationService.consolidateWalletsBallance(balances, sourceAsset, targetAsset, source, tx),
    );

    res.success(stellarBalances);
  }
}
