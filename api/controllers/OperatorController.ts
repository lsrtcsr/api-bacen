import { Controller, Post, BaseResponse } from 'ts-framework';
import { UserRole } from '@bacen/base-sdk';
import { OAuth, Permissions, Params } from '../filters';
import Scopes from '../../config/oauth/scopes';
import { bacenRequest, User, Domain } from '../models';
import { UniqueParameterViolationError, InvalidRequestError } from '../errors';
import { isUUID } from '../utils';

@Controller('/operators')
export default class OperatorController {
  /**
   * POST /
   *
   * @description Creates an operator
   */
  @Post('/', [OAuth.token([Scopes.users.WRITE]), Permissions.canUserWriteUser, Params.isValidUser()])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const {
      firstName,
      lastName,
      email,
      password,
      domain,
    }: { firstName: string; lastName: string; email: string; password: string; domain: Domain } = req.body;

    let domainInstance: Domain;

    if (req.user && req.user.role !== UserRole.ADMIN) {
      domainInstance = req.user.domain;
    } else if (!req.user) {
      domainInstance = await Domain.getRootDomain();
    } else {
      domainInstance = domain || (await Domain.getRootDomain());
    }

    // Determining domain
    const found =
      domainInstance instanceof Domain ? domainInstance : await Domain.safeFindOne({ where: { id: domainInstance } });
    if (!found) {
      throw new InvalidRequestError('Domain', { domainInstance });
    }

    // Checking if the email is unique
    const emailCount = await User.safeCount({ where: { email } });
    if (emailCount > 0) {
      throw new UniqueParameterViolationError('Email', { email });
    }

    // Instantiating new user
    const userInstance = new User();
    userInstance.firstName = firstName;
    userInstance.lastName = lastName;
    userInstance.email = email;
    userInstance.role = UserRole.OPERATOR;
    userInstance.domain = domainInstance;
    await userInstance.setPassword(password);

    // Creating new user
    const user = await User.insertAndFind(userInstance);

    res.success(user);
  }
}
