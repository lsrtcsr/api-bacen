import { Controller, Post, Get, BaseResponse } from 'ts-framework';
import { OAuth } from '../filters';
import { bacenRequest } from '../models';
import { DiscoveryType, DiscoveryService } from '../services';
import { InvalidRequestError } from '../errors';

@Controller('/discovery', [OAuth.token()])
export default class DiscoveryController {
  @Get('/')
  public static async list(req: bacenRequest, res: BaseResponse) {
    const promises = Object.values(DiscoveryType).map(type =>
      DiscoveryService.getInstance()
        .status(type)
        .then(status => ({ [type]: status })),
    );

    const result = await Promise.all(promises);

    res.success(result);
  }

  @Get('/:type')
  public static async getOne(req: bacenRequest, res: BaseResponse) {
    const { type } = req.params as { type: DiscoveryType };
    const status = await DiscoveryService.getInstance().status(type);
    res.success({ status });
  }

  @Post('/:type/up')
  public static async up(req: bacenRequest, res: BaseResponse) {
    const { type } = req.params as { type: DiscoveryType };
    if (!Object.values(DiscoveryType).includes(type)) throw new InvalidRequestError('type is not known');
    await DiscoveryService.getInstance().up(type);
    res.success();
  }

  @Post('/:type/down')
  public static async down(req: bacenRequest, res: BaseResponse) {
    const { type } = req.params as { type: DiscoveryType };
    if (!Object.values(DiscoveryType).includes(type)) throw new InvalidRequestError('type is not known');
    await DiscoveryService.getInstance().down(type);
    res.success();
  }
}
