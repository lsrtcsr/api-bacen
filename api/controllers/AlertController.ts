import { BaseResponse, Controller, Delete, Get, HttpCode, HttpError, Post } from 'ts-framework';
import { IssueType, IssueCategory, AlertSettingsSchema } from '@bacen/base-sdk';
import { Exists, OAuth, Permissions, Query } from '../filters';
import { Alert, bacenRequest } from '../models';
import Scopes from '../../config/oauth/scopes';
import { getPaginationFromReq, setPaginationToRes } from '../utils';

@Controller('/alerts')
export default class AlertController {
  @Get('/', [OAuth.token([Scopes.alerts.READ]), Permissions.canUserReadAllAlerts])
  public static async findAll(req: bacenRequest, res: BaseResponse) {
    const pagination = getPaginationFromReq(req);

    const [results, paginationData] = await Alert.safePaginatedFind({ pagination });

    setPaginationToRes(res, paginationData);

    res.success(results);
  }

  @Get('/:id', [OAuth.token([Scopes.alerts.READ]), Query.isValidId(), Exists.Alert, Permissions.canUserReadOneAlert])
  public static async findOne(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;

    const alert = await Alert.safeFindOne({ where: { id } });

    res.success(alert);
  }

  @Post('/', [OAuth.token([Scopes.alerts.WRITE]), Permissions.canUserWriteAlert])
  public static async create(req: bacenRequest, res: BaseResponse) {
    const schema: {
      issueTypes?: IssueType[];
      issueCategories?: IssueCategory[];
      settings: AlertSettingsSchema;
    } = req.body;
    const currentUser = req.user;

    const alert = await Alert.insertAndFind(
      Alert.create({
        createdBy: currentUser,
        ...schema,
      }),
    );

    res.success(alert);
  }

  @Delete('/:id', [OAuth.token([Scopes.alerts.WRITE]), Query.isValidId(), Permissions.canUserDeleteAlert])
  public static async delete(req: bacenRequest, res: BaseResponse) {
    const { id } = req.params;
    const { force }: { force?: string } = req.query;

    const alert = await Alert.safeFindOne({ where: { id } });

    if (!alert) {
      throw new HttpError('Alert not found', HttpCode.Client.NOT_FOUND);
    }

    let deleted;
    if (force === 'true') {
      // Deletes the row from the table
      // May cascade and break things
      deleted = await Alert.delete(alert.id).then(_ => true);
    } else {
      deleted = await Alert.softDelete(alert.id).then(_ => true);
    }

    res.success(deleted);
  }
}
