import { CompanyDataSchema, UnleashFlags, UserSchema } from '@bacen/base-sdk';
import { CompanyKYCReportSchema, CompanyTaxIdStatus, IdentityService } from '@bacen/identity-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import Config from '../../config';
import { InvalidRequestError } from '../errors';
import { User } from '../models';
import { StorageService } from '../services';

export enum KYCResultType {
  APPROVAL = 'approval',
  MANUAL = 'manual',
  DECLINED = 'declined',
}

export interface KYCSubResult {
  type: KYCResultType;
  raw: any;
  reason?: string[];
  requiresAttention?: string[];
  constraintViolations?: string[];
}

export class KYCUtil {
  public static async fillBasicCompanyData(user: UserSchema): Promise<User> {
    if (
      UnleashUtil.isEnabled(
        UnleashFlags.KYC_SKIP_FILL_BASIC,
        { userId: user.id, properties: { domainId: user.domain.id } },
        false,
      )
    ) {
      return User.create(user as User);
    }

    const response = await IdentityService.getInstance()
      .companies()
      .basic(user.consumer.taxId);

    if (response.taxIdStatus && response.taxIdStatus !== CompanyTaxIdStatus.ACTIVE) {
      throw new InvalidRequestError('The company registration is not active with the authorities');
    }

    // Fill company data from bureau
    const companyData = {
      tradeName: response.registrationName || response.name,
      openingDate: response.birthDate,
      activities: response.activities,
      // TODO: Fill this information properly
      partners: response.partners.map(partner => ({
        name: partner.name,
        taxId: partner.taxId,
      })),
    } as CompanyDataSchema;

    return User.create({
      ...user,
      name: response.name,
      consumer: {
        ...user.consumer,
        companyData,
      },
    } as User);
  }

  public static async verifyCorporateConsumer(user: User): Promise<CompanyKYCReportSchema & { kycFileName?: string }> {
    const { accepted, ...report } = await IdentityService.getInstance()
      .companies()
      .report(user.consumer.taxId);

    // Prepare for report storage
    const storageService = StorageService.getInstance();
    const kycFileName = `${user.id}-${new Date().getTime()}.json`;
    const additionalData = { accepted, report, kycFileName };

    // Save report in storage for audit
    await storageService.uploadFile({
      bucketName: Config.googlecloud.storage.bucketName,
      fileName: kycFileName,
      data: JSON.stringify(additionalData),
    });

    return { accepted, kycFileName, ...report };
  }
}
