export function removeAccents(str: string): string;
export function removeAccents(str: string[]): string[];
export function removeAccents(str: string | string[]): string | string[] {
  const normalize = (str: string) => str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

  if (Array.isArray(str)) return str.map(s => normalize(s));

  return normalize(str);
}

export function splitName(str: string): [string, string] {
  const split = str.split(' ');

  return [split[0], split[split.length - 1]];
}

export function isSameName(name1: string, name2: string): boolean {
  const [firstName1, lastName1] = removeAccents(splitName(name1));
  const [firstName2, lastName2] = removeAccents(splitName(name2));

  return firstName1 === firstName2 && lastName1 === lastName2;
}

export function matches(input: string, referenceStr: string): boolean {
  const inputStrNormalized = removeAccents(removeAbbreviations(input.toLowerCase()).split(/\s+/));
  let inputFirstPiece = inputStrNormalized.shift();
  let inputSecondPiece = inputStrNormalized.join(' ');

  let referenceStrNormalized = removeAccents(removeAbbreviations(referenceStr.toLowerCase()));
  if (
    (containsExcludedTerms(inputFirstPiece) || containsExcludedTerms(inputSecondPiece)) &&
    !containsExcludedTerms(referenceStr)
  ) {
    return false;
  }

  referenceStrNormalized = removeExcludedTerms(referenceStrNormalized);
  inputFirstPiece = removeExcludedTerms(inputFirstPiece);
  inputSecondPiece = removeExcludedTerms(inputSecondPiece);
  return (
    inputFirstPiece &&
    referenceStrNormalized.startsWith(inputFirstPiece) &&
    inputSecondPiece &&
    referenceStrNormalized.endsWith(inputSecondPiece.split(/\s+/).pop())
  );
}

const termsToExclude: string[] = ['jr', 'jr.', 'sr', 'sr.', 'filho', 'neto'];

function removeExcludedTerms(str: string): string {
  const pieces = str.split(/\s+/);

  let newStr = '';
  for (const piece of pieces) {
    if (!termsToExclude.includes(piece.toLowerCase())) {
      newStr = newStr.length ? `${newStr} ${piece}` : piece;
    }
  }

  return newStr.trim();
}

function containsExcludedTerms(str: string): boolean {
  return str.split(/\s+/).some(piece => termsToExclude.includes(piece.toLowerCase()));
}

function removeAbbreviations(str: string): string {
  const pieces = str.split(/\s+/);

  let newStr;
  for (const piece of pieces) {
    const result = termsToExclude.includes(piece) ? piece : piece.replace(/^\w{1,2}\.?$/, '');
    newStr = newStr ? `${newStr} ${result}` : result;
  }

  return newStr;
}

export function removeLeadingSlash(str: string): string {
  return str.startsWith('/') ? removeLeadingSlash(str.slice(1)) : str;
}

/**
 * Returns the index of all ocurrences of the '.' char in a string
 * @param str The string to be searched
 */
export function findAllDots(str: string): number[] {
  const ocurrences: number[] = [];
  let idx = -1;

  while (true) {
    idx = str.indexOf('.', idx + 1);
    if (idx === -1) break;
    ocurrences.push(idx);
  }

  return ocurrences;
}
