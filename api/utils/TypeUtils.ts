import { BaseResponse } from 'ts-framework';
import { bacenRequest } from '../models';

export type MaybePromise<T> = Promise<T> | T;

export type MaybeArray<T> = T[] | T;

export type Ctor<T, A extends any[] = any[]> = new (...args: A) => T;

export type RouteHandler = (req: bacenRequest, res: BaseResponse) => Promise<void>;

export interface StatePartial<TStatus, TAdditionalData = any> {
  status: TStatus;

  additionalData?: TAdditionalData;

  createdAt?: Date;
}
