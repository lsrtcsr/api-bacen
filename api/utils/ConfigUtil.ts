import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as path from 'path';
import { logger } from '../../config/logger.config';

export class ConfigUtil {
  public static initialize(environment = 'development') {
    const distEnvPath = path.join(process.cwd(), '../config/env', `${environment}.env`);
    const baseEnvPath = path.join(process.cwd(), './config/env', `${environment}.env`);

    if (fs.existsSync(distEnvPath)) {
      dotenv.config({
        path: distEnvPath,
        debug: process.env.NODE_ENV === 'development',
      });
      console.debug(`Environment config loaded successfully from "${environment}.env"`);
    } else if (fs.existsSync(baseEnvPath)) {
      dotenv.config({
        path: baseEnvPath,
        debug: process.env.NODE_ENV === 'development',
      });
      console.debug(`Environment config loaded successfully from "${environment}.env"`);
    } else console.warn(`Could not locate environment file at "${environment}.env"`);

    // Try to load hooks configuration automatically
    const { INSTANCE_ENV_PATH = '../../../../.bacen' } = process.env;

    const bacenHooksEnvPath = path.resolve(__dirname, INSTANCE_ENV_PATH, `localhost.env`);
    const bacenHooksLocalEnvPath = path.resolve(__dirname, '../', INSTANCE_ENV_PATH, `localhost.env`);

    if (bacenHooksEnvPath && fs.existsSync(bacenHooksEnvPath)) {
      dotenv.config({
        path: bacenHooksEnvPath,
        debug: process.env.NODE_ENV === 'development',
      });
      logger.debug(`bacen instance credentials loaded successfully from "${bacenHooksEnvPath}"`);
    } else if (bacenHooksLocalEnvPath && fs.existsSync(bacenHooksLocalEnvPath)) {
      dotenv.config({
        path: bacenHooksLocalEnvPath,
        debug: process.env.NODE_ENV === 'development',
      });
      logger.debug(`bacen instance credentials loaded successfully from "${bacenHooksLocalEnvPath}"`);
    } else {
      logger.warn('Could not locate bacen instance credentials environment file');
    }
  }
}
