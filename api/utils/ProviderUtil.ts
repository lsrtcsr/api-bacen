import { DiscoveryStatus } from 'nano-discovery';
import { BaseCustodyFeature } from '@bacen/base-sdk';
import { DiscoveryService, DiscoveryType } from '../services';
import { ProviderUnavailableError } from '../errors';

export class ProviderUtil {
  public static throwIfFeatureNotAvailable(feature: BaseCustodyFeature): Promise<void> {
    return this.throwIfServiceNotAvailable(`${feature.provider}_provider_${feature.type}` as DiscoveryType);
  }

  public static async throwIfServiceNotAvailable(service: DiscoveryType): Promise<void> {
    const serviceStatus = await DiscoveryService.getInstance().status(service);
    if (serviceStatus !== DiscoveryStatus.UP) {
      throw new ProviderUnavailableError(service, { serviceStatus });
    }
  }
}
