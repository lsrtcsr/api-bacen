import * as fs from 'fs';
import * as path from 'path';
import { DocumentType, DocumentSide } from '@bacen/base-sdk';

export function mockProviderIndividualDocuments(walletId: string, mockPath: string = '../../static/cnh.jpg') {
  const file = fs.readFileSync(path.join(__dirname, mockPath)).toString('base64');

  return [DocumentSide.FRONT, DocumentSide.BACK, DocumentSide.SELFIE].map(side => ({
    file,
    side,
    walletId,
    document: { type: DocumentType.BRL_DRIVERS_LICENSE },
  }));
}

export function mockProviderCorporateDocuments(walletId: string, mockPath: string = '../../static/dummy.pdf') {
  const file = fs.readFileSync(path.join(__dirname, mockPath)).toString('base64');

  return [
    {
      file,
      walletId,
      document: { type: DocumentType.OTHER },
    },
  ];
}
