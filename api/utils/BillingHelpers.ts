import { BaseError } from 'ts-framework-common';
import * as moment from 'moment';
import { PaymentType, ServiceType } from '@bacen/base-sdk';
import { Transaction, EntryClazz, OperationType } from '../models';

export function getServiceTypeFromPaymentType(paymentType: PaymentType): ServiceType {
  const map = [
    [PaymentType.TRANSFER, ServiceType.TRANSFER],
    [PaymentType.BOLETO_IN, ServiceType.BOLETO_PAYMENT_CASH_IN][(PaymentType.BOLETO, ServiceType.BOLETO_PAYMENT)],
    [PaymentType.DEPOSIT, ServiceType.DEPOSIT],
    [PaymentType.WITHDRAWAL, ServiceType.WITHDRAWAL],
  ];
  return map[paymentType];
}

export function getPaymentTypeFromServiceType(serviceType: ServiceType): PaymentType {
  const map = [
    [ServiceType.BOLETO_PAYMENT, PaymentType.BOLETO],
    [ServiceType.PHONE_CREDITS_PURCHASE, PaymentType.PHONE_CREDIT],
    [ServiceType.WITHDRAWAL, PaymentType.WITHDRAWAL],
    [ServiceType.TRANSFER, PaymentType.TRANSFER],
    [ServiceType.CARD_PURCHASE, PaymentType.CARD],
    [ServiceType.CARD_WITHDRAWAL, PaymentType.WITHDRAWAL],
    [ServiceType.DEPOSIT, PaymentType.DEPOSIT],
    [ServiceType.BOLETO_PAYMENT_CASH_IN, PaymentType.DEPOSIT],
  ];
  return map[serviceType];
}

export async function getServiceTypeFromTransaction(instance: Transaction): Promise<ServiceType> {
  const transaction =
    instance && instance.payments
      ? instance
      : await Transaction.safeFindOne({
          where: { id: instance.id },
          relations: ['payments'],
        });

  if (!transaction || !transaction.payments) {
    throw new BaseError(`There is no transaction or it has no payments associated`);
  }

  const payment = transaction.payments.find((payment) => payment.type !== PaymentType.SERVICE_FEE);

  return getServiceTypeFromPaymentType(payment.type);
}

export function fromDateToMoment(date: Date): moment.Moment {
  return moment(date.toISOString());
}

export function hasFinancialFlow(type: ServiceType): boolean {
  switch (type) {
    case ServiceType.ACCOUNT_CLOSING:
    case ServiceType.ACCOUNT_OPENING:
    case ServiceType.ACTIVE_ACCOUNT_MAINTENANCE:
    case ServiceType.ACTIVE_CARD_MAINTENANCE:
    case ServiceType.BOLETO_EMISSION:
    case ServiceType.BOLETO_VALIDATION:
    case ServiceType.CARD_REISSUE:
    case ServiceType.COMPLIANCE_CHECK:
    case ServiceType.CONSUMER_DATA_QUERY:
    case ServiceType.EMAIL:
    case ServiceType.PHYSICAL_CARD_ISSUING:
    case ServiceType.SETUP:
    case ServiceType.SMS:
    case ServiceType.SUBSCRIPTION:
    case ServiceType.VIRTUAL_CARD_ISSUING:
      return false;

    case ServiceType.BOLETO_PAYMENT:
    case ServiceType.DEPOSIT:
    case ServiceType.MASS_TRANSPORT_CREDITS_PURCHASE:
    case ServiceType.PHONE_CREDITS_PURCHASE:
    case ServiceType.TRANSFER:
    case ServiceType.WITHDRAWAL:
      return true;

    default:
      return false;
  }
}

export function getOperationTypeFromEntryClazz(clazz: EntryClazz): OperationType {
  switch (clazz) {
    case EntryClazz.INCENTIVE:
    case EntryClazz.COMMISSION:
    case EntryClazz.PROGRESSIVE_DISCOUNT:
      return OperationType.CREDIT;
    case EntryClazz.TAX:
    case EntryClazz.PENALTY:
    case EntryClazz.SERVICE_CHARGE:
      return OperationType.DEBIT;
    default:
      throw new Error('Clazz not supported!');
  }
}
