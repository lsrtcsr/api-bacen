import { BaseError } from 'nano-errors';
import { MaybePromise } from './TypeUtils';

export type UnaryFn<I, O> = (arg: I) => O;

export type BinaryFn<I1, I2, O> = (arg1: I1, args: I2) => O;

export type HomogenousBinaryFn<I, O> = BinaryFn<I, I, O>;

/**
 * Composes a binary function with the output from two unary functions.
 * @param binaryFunction the binary function which will take as parameters the outputs from both the unary functions.
 * @param firstUnaryFunction the unary function whose output will be the first parameter to the binary function.
 * @param secondUnaryFuncion the unary function whose output will be the first parameter to the binary function.
 */
const lift2 = <T, I1, I2, R>(
  binaryFunction: BinaryFn<I1, I2, R>,
  firstUnaryFunction: UnaryFn<T, I1>,
  secondUnaryFuncion: UnaryFn<T, I2>,
) => (arg: T) => binaryFunction(firstUnaryFunction(arg), secondUnaryFuncion(arg));

/**
 * Composes left-to-right a list of functions into a pipe. All functions must be unary
 * @param fns The list of functions to be composed
 */
function pipe<R>(...fns: [() => R]): () => R;
function pipe<I, R>(...fns: [(x: I) => R]): (x: I) => R;

function pipe<T, R>(...fns: [() => T, (x: T) => R]): () => R;
function pipe<I, T, R>(...fns: [(x: I) => T, (x: T) => R]): (x: I) => R;

function pipe<T1, T2, R>(...fns: [() => T1, (x: T1) => T2, (x: T2) => R]): () => R;
function pipe<I, T1, T2, R>(...fns: [(x: I) => T1, (x: T1) => T2, (x: T2) => R]): (x: I) => R;

function pipe<T1, T2, T3, R>(...fns: [() => T1, (x: T1) => T2, (x: T2) => T3, (x: T3) => R]): () => R;
function pipe<I, T1, T2, T3, R>(...fns: [(x: I) => T1, (x: T1) => T2, (x: T2) => T3, (x: T3) => R]): (x: I) => R;

function pipe<T>(...fns: ((x: T) => T)[]): T;
function pipe(...fns: Function[]) {
  return (val: any) => fns.reduce((val, nextFn) => nextFn(val), val);
}

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<R>(...fn: [() => MaybePromise<R>]): () => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<I, R>(...fn: [(x: I) => MaybePromise<R>]): (x: I) => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<T, R>(...fns: [() => MaybePromise<T>, (x: T) => MaybePromise<R>]): () => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<I, T, R>(...fns: [(x: I) => MaybePromise<T>, (x: T) => MaybePromise<R>]): (x: I) => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<T1, T2, R>(
  ...fns: [() => MaybePromise<T1>, (x: T1) => MaybePromise<T2>, (x: T2) => MaybePromise<R>]
): () => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<I, T1, T2, R>(
  ...fns: [(x: I) => MaybePromise<T1>, (x: T1) => MaybePromise<T2>, (x: T2) => MaybePromise<R>]
): (x: I) => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<T1, T2, T3, R>(
  ...fns: [() => MaybePromise<T1>, (x: T1) => MaybePromise<T2>, (x: T2) => MaybePromise<T3>, (x: T3) => MaybePromise<R>]
): () => Promise<R>;

/**
 * Composes left-to-right a list of async functions into a pipe. All functions must be unary.
 * Also accepts sync functions
 * @param fns The list of async functions to be composed
 */
function asyncPipe<I, T1, T2, T3, R>(
  ...fns: [
    (x: I) => MaybePromise<T1>,
    (x: T1) => MaybePromise<T2>,
    (x: T2) => MaybePromise<T3>,
    (x: T3) => MaybePromise<R>,
  ]
): (x: I) => Promise<R>;
function asyncPipe<T>(...fns: ((x: T) => MaybePromise<T>)[]): (x: T) => Promise<T>;
function asyncPipe(...fns: Function[]) {
  return (val: any) => fns.reduce(async (defferredVal, nextFn) => nextFn(await defferredVal), Promise.resolve(val));
}

export { pipe, lift2, asyncPipe };
