import { Pagination, PaginationData } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { SelectQueryBuilder } from 'typeorm';
import { bacenRequest } from '../models';
import Config from '../../config';

export function getPaginationFromReq(req: bacenRequest): Pagination {
  const { skip = 0, limit = Config.api.defaultPaginationLimit } = req.query;
  return { skip, limit };
}

export function setPaginationToRes(res: BaseResponse, data: PaginationData): void {
  // Set pagination headers
  res.set('X-Data-Length', data.dataLength.toString());
  res.set('X-Data-Skip', data.dataSkip?.toString() || '0');
  res.set('X-Data-Limit', data.dataLimit?.toString() || Config.api.defaultPaginationLimit.toString());
}

export async function* consumePaginatedQuery<T>(
  query: SelectQueryBuilder<T>,
  pageSize: number,
): AsyncIterableIterator<T[]> {
  let currentOffset = 0;
  while (true) {
    const results = await query
      .take(pageSize)
      .skip(currentOffset)
      .getMany();
    if (!results.length) {
      return;
    }

    yield results;
    currentOffset += pageSize;
  }
}
