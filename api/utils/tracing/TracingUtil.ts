import { PluginTypes, get as getTracer } from '@google-cloud/trace-agent';
import { MaybePromise } from '../TypeUtils';

export function traceAsyncCall<T extends (...args: any) => MaybePromise<any>>(
  fn: T,
  options: PluginTypes.SpanOptions,
): (...args: Parameters<T>) => ReturnType<T> {
  return function(this: ThisParameterType<T>, ...args: any[]) {
    const tracer = getTracer();
    const span = tracer.createChildSpan(options);

    if (!tracer.isRealSpan(span)) {
      return fn.apply(this, args);
    }

    const fnReturn = fn.apply(this, args);
    const resultPromise = fnReturn instanceof Promise ? fnReturn : Promise.resolve(fnReturn);
    return patchPromise(resultPromise, () => span.endSpan());
  };
}

export function Trace(options: Omit<PluginTypes.SpanOptions, 'name'> & { name?: string } = {}) {
  return function(
    target: Record<string, any>,
    propertyName: string,
    descriptor: TypedPropertyDescriptor<(...args: any[]) => MaybePromise<any>>,
  ) {
    const name = options.name || `${target.constructor.name}.${propertyName}`;

    const originalImpl = descriptor.value;
    descriptor.value = traceAsyncCall(originalImpl, { ...options, name });
    return descriptor;
  };
}

export function patchPromise<T>(promise: Promise<T>, endSpan: () => void): Promise<T> {
  return promise.then(
    res => {
      endSpan();
      return res;
    },
    err => {
      endSpan();
      throw err;
    },
  );
}
