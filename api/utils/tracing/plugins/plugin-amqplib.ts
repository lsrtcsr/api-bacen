import { Message, MessageFields } from 'amqplib';
import { Plugin, Tracer, Span, RootSpanOptions } from '@google-cloud/trace-agent/build/src/plugin-types';
import * as shimmer from 'shimmer';
import { EventEmitter } from 'events';
import { MaybePromise } from '../../TypeUtils';
import { patchPromise } from '../TracingUtil';

declare class BaseChannel extends EventEmitter {
  dispatchMessage: (fields: MessageFields, message: Message) => MaybePromise<void>;
}

// partial types of /amqplib/lib/channel.js exports
interface BaseChannelMod {
  BaseChannel: typeof BaseChannel;
}

function patchAmqplib(mod: BaseChannelMod, agent: Tracer) {
  shimmer.wrap(mod.BaseChannel.prototype, 'dispatchMessage', originalImpl => {
    return function(this: BaseChannel, fields: MessageFields, message: Message) {
      const name = fields.exchange;
      const options: RootSpanOptions = {
        name,
        url: `rabbitmq/${name}`,
        skipFrames: 1,
      };

      agent.runInRootSpan(options, rootSpan => {
        if (!agent.isRealSpan(rootSpan)) {
          return originalImpl.apply(this, arguments);
        }

        const dispatchResult = originalImpl.apply(this, arguments);
        const dispatchPromise = dispatchResult instanceof Promise ? dispatchResult : Promise.resolve(dispatchResult);

        return patchPromise(dispatchPromise, () => {
          rootSpan.addLabel('exchange', fields.exchange);
          rootSpan.addLabel('routingKey', fields.routingKey);
          rootSpan.addLabel('consumerTag', fields.consumerTag);
          rootSpan.addLabel('deliveryTag', fields.deliveryTag);

          rootSpan.endSpan();
        });
      });
    };
  });
}

function unpatchAmqplib(mod: BaseChannelMod) {
  shimmer.unwrap(mod.BaseChannel.prototype, 'dispatchMessage');
}

const plugin: Plugin = [
  {
    file: 'lib/channel.js',
    versions: '^0.5.5',
    patch: patchAmqplib,
    unpatch: unpatchAmqplib,
  },
];

export = plugin;
