import { PubSub } from '@google-cloud/pubsub';
import { Logger } from 'nano-errors';
import GCloudConfig from '../../config/googlecloud.config';

export class PubSubUtil {
  /**
   * Publish a message to GCloud PubSub
   *
   * @param {string} topic The message topic or channel
   * @param {*} message The message data
   * @returns {Promise<string>} The published message id
   */
  public static async publish(options: { projectId: string; topicId: string; message: unknown }): Promise<string> {
    const topicName = PubSubUtil.resourceName(options.projectId || GCloudConfig.projectId, 'topic', options.topicId);

    const pubsub = new PubSub({ projectId: options.projectId || GCloudConfig.projectId });
    const [topics] = await pubsub.getTopics();
    if (!topics.find(topic => topic.name === topicName)) {
      Logger.getInstance().warn(`Topic ${topicName} not found!`);
      return undefined;
    }

    const data = JSON.stringify(options.message);
    const dataBuffer = Buffer.from(data);

    return pubsub.topic(options.topicId).publish(dataBuffer);
  }

  /**
   * Listen and get messages published in a GCloud PubSub
   *
   * @param {string} topic The message topic or channel
   * @param {string} subscription The topic subscription
   * @param {Function} handler The callback method that will handle the message
   */
  public static subscribe(options: { projectId: string; topicId: string; subscriptionId: string; handler: Function }) {
    const pubsub = new PubSub({ projectId: options.projectId || GCloudConfig.projectId });
    const subscription = pubsub.subscription(options.subscriptionId);

    subscription.on('message', async message => options.handler(message));
  }

  public static resourceName(projectId: string, resourceType: 'topic' | 'subscription', resourceId: string): string {
    const basePath = 'projects';
    const separator = '/';
    const collection = resourceType === 'topic' ? 'topics' : 'subscriptions';

    return basePath
      .concat(separator)
      .concat(projectId)
      .concat(separator)
      .concat(collection)
      .concat(separator)
      .concat(resourceId);
  }
}
