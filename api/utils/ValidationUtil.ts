// tslint:disable:function-name
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import * as cpf from 'cpf';
import { isValid as isValidCnpj } from '@fnando/cnpj/dist/node';
import { Wallet } from '../models';
import { InvalidParameterError } from '../errors';

@ValidatorConstraint({ name: 'IsCPF', async: false })
class IsCPFConstraint implements ValidatorConstraintInterface {
  validate(text: string, validationArguments: ValidationArguments) {
    return cpf.isValid(text);
  }

  defaultMessage(args: ValidationArguments) {
    return 'Value ($value) is not a valid CPF.';
  }
}

export function IsCPF(validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      propertyName,
      target: object.constructor,
      options: validationOptions,
      constraints: [],
      validator: IsCPFConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'IsCNPJ', async: false })
class IsCNPJConstraint implements ValidatorConstraintInterface {
  validate(text: string, validationArguments: ValidationArguments) {
    return isValidCnpj(text);
  }

  defaultMessage(args: ValidationArguments) {
    return 'Value ($value) is not a valid CNPJ.';
  }
}

@ValidatorConstraint({ name: 'IsNotRootWallet', async: true })
class IsNotRootWalletConstraint implements ValidatorConstraintInterface {
  async validate(walletId: string, validationArguments: ValidationArguments) {
    return !(await Wallet.isIdFromRootWallet(walletId));
  }

  defaultMessage(args: ValidationArguments) {
    return 'Value ($value) is a root wallet';
  }
}

export function IsCNPJ(validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      propertyName,
      target: object.constructor,
      options: validationOptions,
      constraints: [],
      validator: IsCNPJConstraint,
    });
  };
}

export function IsNotRootWallet(validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      propertyName,
      target: object.constructor,
      options: validationOptions,
      constraints: [],
      validator: IsNotRootWalletConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'IsNotRootDestination', async: true })
class IsNotRootDestinationConstraint implements ValidatorConstraintInterface {
  async validate(destination: any, validationArguments: ValidationArguments) {
    const rootWallet = await Wallet.getRootWallet();
    let isValid = true;

    if (destination?.length) {
      destination.forEach(element => {
        if (element.destination === rootWallet.id) {
          isValid = false;
        }
      });
    }

    return isValid;
  }

  defaultMessage(args: ValidationArguments) {
    return 'Destination is a root wallet';
  }
}
export function IsNotRootDestination(validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      propertyName,
      target: object.constructor,
      options: validationOptions,
      constraints: [],
      validator: IsNotRootDestinationConstraint,
    });
  };
}

/**
 * Checks if the source contains the required fields / properties
 *
 * @param source the object to be verified
 * @param requiredData an array containing the names of the required fields / properties
 */
export function checkRequiredData(source: any, requiredData: string[], baseMessage?: string): boolean {
  const initialStatement = baseMessage || 'Required data not provided';

  let errorsFound;
  const addError = (newError: string) => (errorsFound = errorsFound ? errorsFound.concat(`; ${newError}`) : newError);

  for (const datum of requiredData) {
    if (!source.hasOwnProperty(datum) || source[datum] === undefined) addError(datum);
  }

  if (errorsFound) throw new InvalidParameterError(`${initialStatement}: ${errorsFound}`);

  return true;
}
