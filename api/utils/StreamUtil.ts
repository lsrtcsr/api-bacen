import { Readable } from 'stream';

/**
 * Reads a stream into a string variable asynchronously.
 */
export async function stringify(stream: Readable): Promise<string> {
  const chunks = [];
  return new Promise<string>((resolve, reject) => {
    stream.on('data', chunk => chunks.push(chunk));
    stream.on('error', reject);
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  });
}
