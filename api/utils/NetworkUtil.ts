import { networkInterfaces } from 'os';

export const getLocalExternalIP = () =>
  Object.values(networkInterfaces())
    .flat()
    .filter(details => details.family === 'IPv4' && !details.internal)
    .pop().address;

export const getDefaultFingerprint = () => `bacen- #${getLocalExternalIP()}`;
