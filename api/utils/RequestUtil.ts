import { BaseResponse } from 'ts-framework';
import { bacenRequest } from '../models';

export function requestCache() {
  return (req: bacenRequest, res: BaseResponse, next: (err?: any) => void): void => {
    (req as any)._cache = {};
    req.cache = {
      get: (key: string) => (req as any)._cache[key],
      set: (key: string, obj: any) => ((req as any)._cache[key] = obj),
    };

    next();
  };
}
