import { BaseRequest, HttpError } from 'ts-framework';

import moment = require('moment');

export interface DateRange {
  start: Date;
  end: Date;
}

export interface ParseErrorConfig {
  fieldName: string;
  code?: number;
}

export function isSameDate(date1: Date, date2: Date) {
  const withoutTime = (date: Date) => {
    const newDate = new Date(date);
    newDate.setHours(0, 0, 0, 0);
    return newDate;
  };

  return withoutTime(date1).getTime() === withoutTime(date2).getTime();
}

export function extractRangeFromQuery(req: BaseRequest): Partial<DateRange> {
  let start: Date | undefined;
  let end: Date | undefined;

  const { start: qStart, end: qEnd } = req.query as Record<string, string>;

  if (qStart) start = parseDateOr400(qStart, { fieldName: 'start ' });
  if (qEnd) end = parseDateOr400(qEnd, { fieldName: 'end' });

  return { start, end };
}

export function parseDateOr400(dateString: string, errConfig: ParseErrorConfig, format: string = 'YYYY-MM-DD'): Date {
  const date = moment.utc(dateString, format, true);

  if (!date.isValid()) {
    const { fieldName, code = 400 } = errConfig;
    throw new HttpError(`${fieldName} is not a valid date`, code, {
      value: dateString,
      format,
    });
  }

  return date.toDate();
}
