import { AxiosError } from 'axios';
import { BaseError, Logger } from 'nano-errors';

export interface HandleAxiosErrorOptions {
  log: boolean;
}

const defaultHandleAxiosError: HandleAxiosErrorOptions = {
  log: false,
};

export function handleAxiosError(axiosError: AxiosError, opts: HandleAxiosErrorOptions): BaseError {
  const options = { ...defaultHandleAxiosError, ...opts };

  const { message, stack } = axiosError;
  const meta = axiosError.response && axiosError.response.data;
  const status = axiosError.response && axiosError.response.status;

  const payload = { ...meta, status, stack };

  if (options.log) {
    Logger.getInstance().error(message, payload);
  }

  return new BaseError(message, { ...payload });
}
