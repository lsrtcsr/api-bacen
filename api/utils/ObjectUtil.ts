import { BaseError, Logger } from 'nano-errors';

// https://stackoverflow.com/questions/27936772/how-to-deep-merge-instead-of-shallow-merge

/**
 * Simple object check.
 */
export function isObject(item: any): item is object {
  return item && typeof item === 'object' && !Array.isArray(item);
}

/**
 * Deep merge two objects.
 */
export function mergeDeep(target: object, ...sources: object[]): object {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }

  return mergeDeep(target, ...sources);
}

/**
 * Checks if is UUID.
 *
 * @param check The string to be checked
 */
export function isUUID(check: string) {
  return check.match(/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i);
}

export function groupBy<K extends keyof T, T>(key: K, array: T[]): Record<T[K] & string, T[]> {
  const grouped = {} as Record<T[K] & string, T[]>;

  for (const element of array) {
    const groupKey = element[key];
    if (!(typeof groupKey === 'string' || typeof groupKey === 'number')) {
      Logger.getInstance().warn(
        'Grouping key must be of type number | string | symbol, skipping',
        new BaseError('Grouping key must be of type number | string | symbol, skipping', {
          groupKey,
          type: groupKey && typeof groupKey,
        }),
      );
      continue;
    }

    grouped[groupKey as string] = grouped[groupKey as string] || [];
    grouped[groupKey as string].push(element);
  }

  return grouped;
}
