import * as fs from 'fs';
import * as url from 'url';
import Axios from 'axios';
import { BaseError } from 'nano-errors';
import { StorageService } from '../services';
import { removeLeadingSlash } from './StringUtil';

export interface LoadFileOptions {
  /** The content-type of the file being loaded, must be a valid MIME type.  Default: text/plain */
  contentType: string;
}

export const DEFAULT_LOAD_FILE_OPTIONS: LoadFileOptions = {
  contentType: 'text/plain',
};

export const PLAINTEXT_TYPES = [
  'text/plain',
  'text/html',
  'text/css',
  'text/javascript',
  'application/json',
  'application/javascript',
];

/**
 * Loads a file from a given path and returns it as a string. Path can be either a filesystem path or a URL, also handles loading from GCS.
 * Currently supports only text-files (encoded as utf-8) or images (encoded as base64)
 *
 * @param path The path to the file
 * @param options The file loading options
 */
export async function loadFile(path: string, _options: Partial<LoadFileOptions> = {}): Promise<string> {
  // Shallow merge
  const options = {
    ...DEFAULT_LOAD_FILE_OPTIONS,
    ..._options,
  };

  const urlParts = path ? url.parse(path) : undefined;
  const isUrl = urlParts && urlParts.host;

  if (!isUrl && fs.existsSync(path)) {
    const file = fs.readFileSync(path);

    if (PLAINTEXT_TYPES.includes(options.contentType)) {
      return file.toString('utf8');
    }
    if (/^image\//.test(options.contentType)) {
      return file.toString('base64');
    }
    throw new BaseError(`Unsupportet content-type: ${options.contentType}`);
  }

  if (isUrl) {
    const fileName = removeLeadingSlash(urlParts.path);

    if (urlParts.protocol && urlParts.protocol.startsWith('gs')) {
      const bucket = urlParts.host;
      const storageService = StorageService.getInstance();
      return storageService.getFile({ bucketName: bucket, fileName });
    }

    const baseURL = `${urlParts.protocol}//${urlParts.host}`;
    const client = Axios.create({ baseURL });
    const result = await client.get(urlParts.path);

    return result.data;
  }

  throw new BaseError(`Could not resolve file ${path}`);
}
