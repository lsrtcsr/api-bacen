import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropLastKYCRecheckAndChangeConsumerStateEnumValues1565292804510 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "lastKYCCheck"`);
    await queryRunner.query(
      `ALTER TYPE "public"."consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'pending_phone_verification', 'pending_documents', 'pending_email_verification', 'pending_deletion', 'processing_documents', 'processing_wallets', 'kyc_rechecking', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('blocked', 'deleted', 'invalid_documents', 'manual_verification', 'pending_deletion', 'pending_documents', 'pending_kyc_recheck', 'pending_phone_verification', 'pending_selfie', 'processing_documents', 'processing_wallets', 'provider_failed', 'ready', 'rejected', 'suspended')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum_old" RENAME TO  "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TABLE "consumers" ADD "lastKYCCheck" TIMESTAMP`);
  }
}
