import { MigrationInterface, QueryRunner } from 'typeorm';

export class TrimEmail1574684982064 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`UPDATE users SET email = lower(trim(email))`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
