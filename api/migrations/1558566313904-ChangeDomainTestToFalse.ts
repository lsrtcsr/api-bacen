import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeDomainTestToFalse1558566313904 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "test" SET DEFAULT false`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "test" SET DEFAULT true`);
  }
}
