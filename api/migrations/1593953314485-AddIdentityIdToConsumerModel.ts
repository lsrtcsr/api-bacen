import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddIdentityIdToConsumerModel1593953314485 implements MigrationInterface {
  name = 'AddIdentityIdToConsumerModel1593953314485';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" ADD "files" jsonb`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" ADD "identity_id" uuid`, undefined);
    await queryRunner.query(
      `ALTER TABLE "consumers" ADD CONSTRAINT "UQ_5fd714b75bf43b86c59228b6db1" UNIQUE ("identity_id")`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "postback_urls" SET DEFAULT '[]'`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "system_postback_urls" SET DEFAULT '[]'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "system_postback_urls" DROP DEFAULT`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" ALTER COLUMN "postback_urls" DROP DEFAULT`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" DROP CONSTRAINT "UQ_5fd714b75bf43b86c59228b6db1"`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "identity_id"`, undefined);
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "files"`, undefined);
  }
}
