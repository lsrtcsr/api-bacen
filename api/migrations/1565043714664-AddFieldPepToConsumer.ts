import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddFieldPepToConsumer1565043714664 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ADD "pep" boolean NOT NULL DEFAULT false`);
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`);
    await queryRunner.query(`ALTER TYPE "public"."documents_type_enum" RENAME TO "documents_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('brl_individual_reg', 'brl_drivers_license', 'brl_address_statement', 'passport', 'other')`,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("consumerId", "type") WHERE deleted_at IS NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('brl_address_statement', 'brl_drivers_license', 'brl_identity', 'brl_individual_reg', 'other', 'passport', 'tax_id')`,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO  "documents_type_enum"`);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("type", "consumerId") WHERE (deleted_at IS NULL)`,
    );
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "pep"`);
  }
}
