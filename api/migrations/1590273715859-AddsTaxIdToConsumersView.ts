import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddsTaxIdToConsumersView1590273715859 implements MigrationInterface {
  name = 'AddsTaxIdToConsumersView1590273715859';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_consumer_with_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."taxId",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    WHERE users.role = 'consumer' AND users.deleted_at IS NULL
    ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_consumer_with_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  users.id as "userId",\n            consumers.id as "consumerId",\n            latest_states.status,\n            users."firstName",\n            users."lastName",\n            users.email,\n            latest_states."additionalData",\n            consumers."taxId",\n            consumers."birthday",\n            consumers."companyData",\n            users.domain_user as "domainId"\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    WHERE users.role = \'consumer\' AND users.deleted_at IS NULL\n    ORDER BY latest_states.created_at DESC',
      ],
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_consumer_with_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    ORDER BY latest_states.created_at DESC
    WHERE users.role = 'consumer' AND users.deleted_at IS NULL`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_consumer_with_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  users.id as "userId",\n            consumers.id as "consumerId",\n            latest_states.status,\n            users."firstName",\n            users."lastName",\n            users.email,\n            latest_states."additionalData",\n            consumers."birthday",\n            consumers."companyData",\n            users.domain_user as "domainId"\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    ORDER BY latest_states.created_at DESC\n    WHERE users.role = \'consumer\' AND users.deleted_at IS NULL',
      ],
    );
  }
}
