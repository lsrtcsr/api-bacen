import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCardActivatedAndCancelledAt1580736396705 implements MigrationInterface {
  name = 'AddCardActivatedAndCancelledAt1580736396705';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" ADD "activated_at" TIMESTAMP`, undefined);
    await queryRunner.query(`ALTER TABLE "cards" ADD "cancelled_at" TIMESTAMP`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" DROP COLUMN "cancelled_at"`, undefined);
    await queryRunner.query(`ALTER TABLE "cards" DROP COLUMN "activated_at"`, undefined);
  }
}
