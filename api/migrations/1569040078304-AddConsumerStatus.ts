import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddConsumerStatus1569040078304 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`);
    await queryRunner.query(
      `ALTER TYPE "public"."consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'manually_approved', 'pending_phone_verification', 'pending_documents', 'pending_email_verification', 'pending_deletion', 'processing_documents', 'processing_wallets', 'kyc_rechecking', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`);
    await queryRunner.query(`ALTER TYPE "public"."alerts_issue_types_enum" RENAME TO "alerts_issue_types_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum"[] USING "issue_types"::"text"::"alerts_issue_types_enum"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum_old"[] USING "issue_types"::"text"::"alerts_issue_types_enum_old"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_types_enum_old" RENAME TO  "alerts_issue_types_enum"`);
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "birthday" SET NOT NULL`);
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('blocked', 'deleted', 'invalid_documents', 'kyc_rechecking', 'manual_verification', 'pending_deletion', 'pending_documents', 'pending_email_verification', 'pending_phone_verification', 'processing_documents', 'processing_wallets', 'provider_failed', 'ready', 'rejected', 'suspended')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum_old" RENAME TO  "consumer_states_status_enum"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`);
  }
}
