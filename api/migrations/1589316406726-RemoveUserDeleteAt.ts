import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveUserDeleteAt1589316406726 implements MigrationInterface {
  name = 'RemoveUserDeleteAt1589316406726';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "deleteAt"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "users" ADD "deleteAt" TIMESTAMP WITH TIME ZONE`, undefined);
  }
}
