import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddMaxLengthToBankingName1561819252705 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" SET DATA TYPE character varying(30)`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" SET DATA TYPE character varying`);
  }
}
