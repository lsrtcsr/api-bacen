import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddEventModelAndRelationships1576158976737 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "events_status_enum" AS ENUM('pending', 'processed')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "events" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "data" jsonb NOT NULL, "retries" integer NOT NULL DEFAULT 0, "status" "events_status_enum" NOT NULL DEFAULT 'pending', CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "entries" ADD "eventId" uuid`, undefined);
    await queryRunner.query(
      `ALTER TABLE "entries" ADD CONSTRAINT "FK_adf998740b11f756a797268177d" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_adf998740b11f756a797268177d"`, undefined);
    await queryRunner.query(`ALTER TABLE "entries" DROP COLUMN "eventId"`, undefined);
    await queryRunner.query(`DROP TABLE "events"`, undefined);
    await queryRunner.query(`DROP TYPE "events_status_enum"`, undefined);
  }
}
