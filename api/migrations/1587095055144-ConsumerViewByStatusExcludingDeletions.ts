import { MigrationInterface, QueryRunner } from 'typeorm';

export class ConsumerViewByStatusExcludingDeletions1587095055144 implements MigrationInterface {
  name = 'ConsumerViewByStatusExcludingDeletions1587095055144';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE OR REPLACE VIEW "view_consumer_by_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(users.id) as count
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    WHERE users.deleted_at is null
    GROUP BY latest_states.status
  `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE OR REPLACE VIEW "view_consumer_by_states" AS 
        WITH latest_states AS (
          SELECT
              DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
          FROM
              consumer_states
          ORDER BY
              "consumerId", created_at DESC
        )
        SELECT  latest_states.status,
                COUNT(users.id) as count
        FROM consumers
        JOIN users on users.id = consumers."userId"
        JOIN latest_states on latest_states."consumerId" = consumers.id
        GROUP BY latest_states.status
      `,
      undefined,
    );
  }
}
