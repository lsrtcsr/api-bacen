import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveProduct1570643005335 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "wallets" DROP CONSTRAINT "FK_53ac7cc8bf8053c108a443203de"`, undefined);
    await queryRunner.query(`ALTER TABLE "wallets" DROP COLUMN "productId"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "wallets" ADD "productId" uuid`, undefined);
    await queryRunner.query(
      `ALTER TABLE "wallets" ADD CONSTRAINT "FK_53ac7cc8bf8053c108a443203de" FOREIGN KEY ("productId") REFERENCES "products"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
      undefined,
    );
  }
}
