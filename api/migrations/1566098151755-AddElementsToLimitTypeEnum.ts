import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddElementsToLimitTypeEnum1566098151755 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."user_limits_type_enum" RENAME TO "user_limits_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "user_limits_type_enum" AS ENUM('num_cards_active', 'num_boletos_per_day', 'sum_boletos_current_day', 'sum_boletos_last_24hours', 'transaction_sum_current_day', 'transaction_sum_current_month', 'transaction_sum_current_week', 'received_current_day', 'received_current_month', 'received_current_week', 'per_transaction', 'mobile_credit_sum_current_day')`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "type" TYPE "user_limits_type_enum" USING "type"::"text"::"user_limits_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "user_limits_type_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "user_limits_type_enum_old" AS ENUM('mobile_credit_sum_current_day', 'num_boletos_per_day', 'num_cards_active', 'sum_boletos_current_day', 'sum_boletos_last_24hours', 'transaction_sum_current_day', 'transaction_sum_current_month')`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "type" TYPE "user_limits_type_enum_old" USING "type"::"text"::"user_limits_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "user_limits_type_enum"`);
    await queryRunner.query(`ALTER TYPE "user_limits_type_enum_old" RENAME TO  "user_limits_type_enum"`);
  }
}
