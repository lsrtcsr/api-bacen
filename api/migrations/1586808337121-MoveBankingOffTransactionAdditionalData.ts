import { MigrationInterface, QueryRunner } from 'typeorm';

export class MoveBankingOffTransactionAdditionalData1586808337121 implements MigrationInterface {
  name = 'MoveBankingOffTransactionAdditionalData1586808337121';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "transactions" ADD "bankingId" uuid`, undefined);
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "FK_3c73d272ee1159d384de6f7d0ae" FOREIGN KEY ("bankingId") REFERENCES "bankings"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(`
          UPDATE transactions
          SET "bankingId" = (additional_data->>'bankingId')::uuid
          WHERE additional_data->>'bankingId' is not null;
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "transactions" DROP CONSTRAINT "FK_3c73d272ee1159d384de6f7d0ae"`, undefined);
    await queryRunner.query(`ALTER TABLE "transactions" DROP COLUMN "bankingId"`, undefined);
  }
}
