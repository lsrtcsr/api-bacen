import { MigrationInterface, QueryRunner } from 'typeorm';

export class WalletAdditionalData1562340024299 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "wallets" ADD "additionalData" jsonb NOT NULL DEFAULT '{}'`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "wallets" DROP COLUMN "additionalData"`);
  }
}
