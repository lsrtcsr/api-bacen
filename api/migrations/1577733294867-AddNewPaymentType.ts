import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewPaymentType1577733294867 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'phone_credit', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'phone_credit', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'manual_adjustment', 'phone_credits', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'manual_adjustment', 'phone_credits', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
  }
}
