import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNotNullToPaymentDestinationId1593716207699 implements MigrationInterface {
  name = 'AddNotNullToPaymentDestinationId1593716207699';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "destinationId" SET NOT NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "destinationId" DROP NOT NULL`, undefined);
  }
}
