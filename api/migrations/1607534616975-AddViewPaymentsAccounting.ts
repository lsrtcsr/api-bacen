import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddViewPaymentsAccounting1607534616975 implements MigrationInterface {
  name = 'AddViewPaymentsAccounting1607534616975';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE VIEW "view_payment_accounting" AS 
  with latest_states as (
    select distinct on (transaction_states."transactionId")
         transaction_states."transactionId"
       , transaction_states.status
       , transaction_states.created_at
       , transaction_states."additionalData"
    from transaction_states
order by transaction_states."transactionId", transaction_states.created_at desc
)
, payment_with_states as (
    select   p.id as id
           , t.id as transaction_id
           , ts.status as transaction_status
           , p.type as type
           , p.status as status
           , p."assetId" as asset
           , p.amount
           , p."destinationId" as destination
           , ud.id as destination_user
           , ud."firstName" as destination_user_first_name
           , ud."lastName" as destination_user_last_name
           , ud.role as destination_user_role
           , t."sourceId" as source
           , us.id as source_user
           , us."firstName" as source_user_first_name
           , us."lastName" as source_user_last_name
           , us.role as source_user_role
           , ts."additionalData" as transaction_additional_data
           , p.deleted_at
           , t.updated_at
    from payments p
    join transactions t on t.id = p."transactionId"
    join latest_states ts on ts."transactionId" = t.id
    join wallets wd on wd.id = p."destinationId"
    join users ud on ud.id = wd."userId"
    join wallets ws on ws.id = t."sourceId"
    join users us on us.id = ws."userId"
order by ts.created_at desc
)
    select account
         , transaction_id
         , id
         , status
         , updated_at
         , deleted_at
         , wallet
         , user_first_name
         , user_last_name
         , user_role
         , user_type
         , asset
         , case
               when user_type in ('boleto', 'withdrawal', 'card', 'authorized_card', 'phone_credit', 'service_fee')
                   or (user_type = 'transfer' and account = 'source') then amount::numeric(16,6) * -1
               else amount::numeric(16,6)
        end as amount
    from (
             select 'source' as account                    
                  , transaction_id
                  , id
                  , status
                  , asset
                  , updated_at
                  , deleted_at
                  , source  as wallet
                  , source_user_first_name as user_first_name
                  , source_user_last_name as user_last_name
                  , source_user_role as user_role
                  , type user_type
                  , amount::numeric(16,6)
             from payment_with_states
            union all
             select 'destination'
                  , transaction_id
                  , id
                  , status
                  , asset                  
                  , updated_at
                  , deleted_at                      
                  , destination
                  , destination_user_first_name
                  , destination_user_last_name
                  , destination_user_role
                  , type
                  , amount::numeric(16,6)
             from payment_with_states
         ) t
      `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_payment_accounting',
        'with latest_states as (\n    select distinct on (transaction_states."transactionId")\n         transaction_states."transactionId"\n       , transaction_states.status\n       , transaction_states.created_at\n       , transaction_states."additionalData"\n    from transaction_states\norder by transaction_states."transactionId", transaction_states.created_at desc\n)\n, payment_with_states as (\n    select   p.id as id\n           , t.id as transaction_id\n           , ts.status as transaction_status\n           , p.type as type\n           , p.status as status\n           , p."assetId" as asset\n           , p.amount\n           , p."destinationId" as destination\n           , ud.id as destination_user\n           , ud."firstName" as destination_user_first_name\n           , ud."lastName" as destination_user_last_name\n           , ud.role as destination_user_role\n           , t."sourceId" as source\n           , us.id as source_user\n           , us."firstName" as source_user_first_name\n           , us."lastName" as source_user_last_name\n           , us.role as source_user_role\n           , ts."additionalData" as transaction_additional_data\n           , p.deleted_at\n           , t.updated_at\n    from payments p\n    join transactions t on t.id = p."transactionId"\n    join latest_states ts on ts."transactionId" = t.id\n    join wallets wd on wd.id = p."destinationId"\n    join users ud on ud.id = wd."userId"\n    join wallets ws on ws.id = t."sourceId"\n    join users us on us.id = ws."userId"\norder by ts.created_at desc\n)\n    select account\n         , transaction_id\n         , id\n         , status\n         , updated_at\n         , deleted_at\n         , wallet\n         , user_first_name\n         , user_last_name\n         , user_role\n         , user_type\n         , asset\n         , case\n               when user_type in (\'boleto\', \'withdrawal\', \'card\', \'authorized_card\', \'phone_credit\', \'service_fee\')\n                   or (user_type = \'transfer\' and account = \'source\') then amount::numeric(16,6) * -1\n               else amount::numeric(16,6)\n        end as amount\n    from (\n             select \'source\' as account                    \n                  , transaction_id\n                  , id\n                  , status\n                  , asset\n                  , updated_at\n                  , deleted_at\n                  , source  as wallet\n                  , source_user_first_name as user_first_name\n                  , source_user_last_name as user_last_name\n                  , source_user_role as user_role\n                  , type user_type\n                  , amount::numeric(16,6)\n             from payment_with_states\n            union all\n             select \'destination\'\n                  , transaction_id\n                  , id\n                  , status\n                  , asset                  \n                  , updated_at\n                  , deleted_at                      \n                  , destination\n                  , destination_user_first_name\n                  , destination_user_last_name\n                  , destination_user_role\n                  , type\n                  , amount::numeric(16,6)\n             from payment_with_states\n         ) t',
      ],
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_payment_accounting',
    ]);
    await queryRunner.query(`DROP VIEW "view_payment_accounting"`, undefined);
  }
}
