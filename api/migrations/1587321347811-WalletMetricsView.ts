import { MigrationInterface, QueryRunner } from 'typeorm';
import { logger } from '../../config/logger.config';

export class WalletMetricsView1587321347811 implements MigrationInterface {
  name = 'WalletMetricsView1587321347811';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // FIXES TYPEORM VIEW MIGRATION
    // GITHUB ISSUE 4923
    // Reference: https://github.com/typeorm/typeorm/issues/4923#issue-508925209

    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "typeorm_metadata" ("type" varchar(255) NOT NULL,"database" varchar(255) DEFAULT NULL,"schema" varchar(255) DEFAULT NULL,"table" varchar(255) DEFAULT NULL,"name" varchar(255) DEFAULT NULL,"value" text)`,
    );
    try {
      await queryRunner.query(
        `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
        [
          'VIEW',
          'public',
          'wallet_pending_balance_view',
          'SELECT\n  wallet.id AS "walletId",\n  transactions.payment_asset AS asset,\n  SUM(\n    CASE\n      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingDebtAmount",\n  SUM (\n    CASE\n      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingCreditAmount"\nFROM wallets wallet\nLEFT OUTER JOIN (\n  SELECT\n    transactions.id AS transaction_id,\n    transactions."sourceId" AS source_id,\n    payments.status AS payment_status,\n    payments."destinationId" AS destination_id,\n    payments.amount AS payment_amount,\n    payments."assetId" AS payment_asset,\n    payments.id AS payment_id\n  FROM transactions\n  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id\n) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id\nLEFT OUTER JOIN (\n  SELECT\n    "transactionId",\n    MAX(created_at) AS created_at\n  FROM transaction_states\n  GROUP BY 1\n) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"\nLEFT OUTER JOIN transaction_states ON (\n  transaction_states."transactionId"=last_transaction_state."transactionId"\n  AND transaction_states.created_at=last_transaction_state.created_at\n)\nWHERE transaction_states.status IN (\'pending\', \'AUTHORIZED\')\nAND transactions.payment_status IN (\'authorized\', \'settled\')\nGROUP BY 1, 2',
        ],
      );
      await queryRunner.query(
        `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
        [
          'VIEW',
          'public',
          'view_transaction_by_states',
          'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"\n      FROM\n      transaction_states\n      ORDER BY\n          "transactionId", created_at DESC\n    )\n    SELECT  latest_states.status,\n            COUNT(transactions.id) as count\n    FROM transactions\n    JOIN latest_states on latest_states."transactionId" = transactions.id\n    WHERE transactions.deleted_at is null\n    GROUP BY latest_states.status',
        ],
      );
      await queryRunner.query(
        `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
        [
          'VIEW',
          'public',
          'view_consumer_by_states',
          'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  latest_states.status,\n            COUNT(users.id) as count\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    WHERE users.deleted_at is null\n    GROUP BY latest_states.status',
        ],
      );
      await queryRunner.query(
        `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
        [
          'VIEW',
          'public',
          'view_consumer_with_states',
          'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  users.id as "userId",\n            consumers.id as "consumerId",\n            latest_states.status,\n            users."firstName",\n            users."lastName",\n            users.email,\n            latest_states."additionalData",\n            consumers."birthday",\n            consumers."companyData",\n            users.domain_user as "domainId"\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    ORDER BY latest_states.created_at DESC\n    WHERE users.role = \'consumer\' AND users.deleted_at IS NULL',
        ],
      );
    } catch (exception) {
      logger.warn('Ignoring typeorm existing views metadata', exception);
    }

    // Normal query runners
    await queryRunner.query(
      `CREATE VIEW "view_wallet_by_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("walletId") "walletId", "status", created_at, "additionalData"
      FROM
      wallet_states
      ORDER BY
          "walletId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(wallets.id) as count
    FROM wallets
    JOIN latest_states on latest_states."walletId" = wallets.id
    WHERE wallets.deleted_at is null
    GROUP BY latest_states.status
  `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_wallet_by_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("walletId") "walletId", "status", created_at, "additionalData"\n      FROM\n      wallet_states\n      ORDER BY\n          "walletId", created_at DESC\n    )\n    SELECT  latest_states.status,\n            COUNT(wallets.id) as count\n    FROM wallets\n    JOIN latest_states on latest_states."walletId" = wallets.id\n    WHERE wallets.deleted_at is null\n    GROUP BY latest_states.status',
      ],
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_wallet_by_states"`, undefined);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_wallet_by_states',
    ]);
  }
}
