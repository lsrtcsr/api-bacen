import { MigrationInterface, QueryRunner } from 'typeorm';

export class AlertSystemAndDomainSettings1560476379847 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "notifications_channel_enum" AS ENUM('slack', 'email', 'sms', 'jira')`);
    await queryRunner.query(
      `CREATE TABLE "notifications" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "channel" "notifications_channel_enum" NOT NULL, "destination" character varying NOT NULL, "confirmed" boolean NOT NULL DEFAULT false, "sentAt" TIMESTAMP WITH TIME ZONE NOT NULL, "sourceId" uuid, CONSTRAINT "PK_6a72c3c0f683f6462415e653c3a" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "alerts" DROP COLUMN "issue_type"`);
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error')`,
    );
    await queryRunner.query(`ALTER TABLE "alerts" ADD "issue_types" "alerts_issue_types_enum" array`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_categories_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'cdt_provider')`,
    );
    await queryRunner.query(`ALTER TABLE "alerts" ADD "issue_categories" "alerts_issue_categories_enum" array`);
    await queryRunner.query(`ALTER TYPE "settings_field_enum" RENAME TO "settings_field_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "settings_field_enum" AS ENUM('initialized', 'root_domain', 'default_domain', 'maintenance', 'test_hash')`,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "field" TYPE "settings_field_enum" USING "field"::"text"::"settings_field_enum"`,
    );
    await queryRunner.query(`DROP TYPE "settings_field_enum_old"`);
    await queryRunner.query(`ALTER TABLE "alerts" DROP CONSTRAINT "FK_779c7c43268165afb5a947e0562"`);
    await queryRunner.query(`ALTER TABLE "alerts" DROP CONSTRAINT "REL_779c7c43268165afb5a947e056"`);
    await queryRunner.query(
      `ALTER TABLE "notifications" ADD CONSTRAINT "FK_24eedfb5f07d3be349d235b8f95" FOREIGN KEY ("sourceId") REFERENCES "alerts"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ADD CONSTRAINT "FK_779c7c43268165afb5a947e0562" FOREIGN KEY ("created_by") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "alerts" DROP CONSTRAINT "FK_779c7c43268165afb5a947e0562"`);
    await queryRunner.query(`ALTER TABLE "notifications" DROP CONSTRAINT "FK_24eedfb5f07d3be349d235b8f95"`);
    await queryRunner.query(
      `ALTER TABLE "alerts" ADD CONSTRAINT "REL_779c7c43268165afb5a947e056" UNIQUE ("created_by")`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ADD CONSTRAINT "FK_779c7c43268165afb5a947e0562" FOREIGN KEY ("created_by") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `CREATE TYPE "settings_field_enum_old" AS ENUM('initialized', 'root_domain', 'maintenance', 'test_hash')`,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "field" TYPE "settings_field_enum_old" USING "field"::"text"::"settings_field_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "settings_field_enum"`);
    await queryRunner.query(`ALTER TYPE "settings_field_enum_old" RENAME TO "settings_field_enum"`);
    await queryRunner.query(`ALTER TABLE "alerts" DROP COLUMN "issue_categories"`);
    await queryRunner.query(`DROP TYPE "alerts_issue_categories_enum"`);
    await queryRunner.query(`ALTER TABLE "alerts" DROP COLUMN "issue_types"`);
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error')`,
    );
    await queryRunner.query(`ALTER TABLE "alerts" ADD "issue_type" "alerts_issue_type_enum" NOT NULL`);
    await queryRunner.query(`DROP TABLE "notifications"`);
    await queryRunner.query(`DROP TYPE "notifications_channel_enum"`);
  }
}
