import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropUniqueIdentityId1594208482194 implements MigrationInterface {
  name = 'DropUniqueIdentityId1594208482194';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" DROP CONSTRAINT "UQ_5fd714b75bf43b86c59228b6db1"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "consumers" ADD CONSTRAINT "UQ_5fd714b75bf43b86c59228b6db1" UNIQUE ("identity_id")`,
      undefined,
    );
  }
}
