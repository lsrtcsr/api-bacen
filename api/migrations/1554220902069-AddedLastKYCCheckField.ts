import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedLastKYCCheckField1554220902069 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ADD "lastKYCCheck" TIMESTAMP`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'pending_documents', 'pending_selfie', 'pending_deletion', 'pending_kyc_recheck', 'processing_documents', 'processing_wallets', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('ready', 'rejected', 'pending_documents', 'pending_selfie', 'pending_deletion', 'processing_documents', 'processing_wallets', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum_old" RENAME TO "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "lastKYCCheck"`);
  }
}
