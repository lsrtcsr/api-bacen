import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateLegalTermModels1575496827510 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "legal_terms" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" character varying NOT NULL, "name" character varying NOT NULL, "body" character varying NOT NULL, "provider" character varying NOT NULL, CONSTRAINT "PK_0188fd7703b5316d4f672f54780" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "legal_term_acceptances" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "consumerInfo" jsonb NOT NULL, "legalTermId" uuid NOT NULL, "consumerId" uuid NOT NULL, CONSTRAINT "PK_5757e18c9c89be43e55f467656a" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TYPE "public"."consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'manually_approved', 'pending_phone_verification', 'pending_documents', 'pending_email_verification', 'pending_deletion', 'pending_legal_acceptance', 'processing_documents', 'processing_wallets', 'kyc_rechecking', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "legal_term_acceptances" ADD CONSTRAINT "FK_81cb5ccfba2c95b0534930351b3" FOREIGN KEY ("legalTermId") REFERENCES "legal_terms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "legal_term_acceptances" ADD CONSTRAINT "FK_e5a8a62302832f2967475830e47" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "legal_term_acceptances" DROP CONSTRAINT "FK_e5a8a62302832f2967475830e47"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "legal_term_acceptances" DROP CONSTRAINT "FK_81cb5ccfba2c95b0534930351b3"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('blocked', 'deleted', 'invalid_documents', 'kyc_rechecking', 'manual_verification', 'manually_approved', 'pending_deletion', 'pending_documents', 'pending_email_verification', 'pending_phone_verification', 'processing_documents', 'processing_wallets', 'provider_failed', 'ready', 'rejected', 'suspended')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "consumer_states_status_enum_old" RENAME TO  "consumer_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TABLE "legal_term_acceptances"`, undefined);
    await queryRunner.query(`DROP TABLE "legal_terms"`, undefined);
  }
}
