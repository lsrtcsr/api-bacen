import { MigrationInterface, QueryRunner } from 'typeorm';

export class MarkCardStatusAsEnum1555528708536 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" DROP COLUMN "status"`);
    await queryRunner.query(`CREATE TYPE "cards_status_enum" AS ENUM('available', 'blocked', 'cancelled')`);
    await queryRunner.query(`ALTER TABLE "cards" ADD "status" "cards_status_enum" NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" DROP COLUMN "status"`);
    await queryRunner.query(`DROP TYPE "cards_status_enum"`);
    await queryRunner.query(`ALTER TABLE "cards" ADD "status" character varying NOT NULL`);
  }
}
