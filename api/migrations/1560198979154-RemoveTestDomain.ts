import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveTestDomain1560198979154 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domains" DROP COLUMN "test"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domains" ADD "test" boolean NOT NULL DEFAULT false`);
  }
}
