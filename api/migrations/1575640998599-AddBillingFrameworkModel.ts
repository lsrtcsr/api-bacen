import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddBillingFrameworkModel1575640998599 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "contract_states_status_enum" AS ENUM('active', 'expired', 'cancelled', 'finished')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "contract_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "contract_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "contractId" uuid NOT NULL, CONSTRAINT "PK_0c0536ae77847b13b30df90680e" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "period_states_status_enum" AS ENUM('active', 'closed', 'canceled')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "period_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "period_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "periodId" uuid NOT NULL, CONSTRAINT "PK_f34a14b525313aa78a01c98ae13" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "invoice_states_status_enum" AS ENUM('active', 'closing', 'closed', 'pending_settlement', 'settled', 'canceled')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "invoice_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "invoice_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "invoiceId" uuid NOT NULL, CONSTRAINT "PK_195f81796bb391ef925f7f737e6" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "invoices" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "start_at" TIMESTAMP WITH TIME ZONE NOT NULL, "closure_scheduled_for" TIMESTAMP WITH TIME ZONE NOT NULL, "closed_at" TIMESTAMP WITH TIME ZONE, "current" boolean NOT NULL DEFAULT true, "contractorId" uuid NOT NULL, "payment_proof" uuid, CONSTRAINT "REL_0c6a575c98b46988443ab6def6" UNIQUE ("payment_proof"), CONSTRAINT "PK_668cef7c22a427fd822cc1be3ce" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "periods" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "start_at" TIMESTAMP WITH TIME ZONE NOT NULL, "closure_scheduled_for" TIMESTAMP WITH TIME ZONE NOT NULL, "closed_at" TIMESTAMP WITH TIME ZONE, "current" boolean NOT NULL DEFAULT true, "invoiceId" uuid NOT NULL, CONSTRAINT "PK_86c6afb6c818d97dc321898627c" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`CREATE TYPE "entries_status_enum" AS ENUM('pending', 'settled', 'canceled')`, undefined);
    await queryRunner.query(`CREATE TYPE "entries_operationtype_enum" AS ENUM('credit', 'debit')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "entries" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "additional_data" jsonb, "amount" character varying NOT NULL, "status" "entries_status_enum" NOT NULL DEFAULT 'pending', "operationType" "entries_operationtype_enum" NOT NULL DEFAULT 'debit', "typeId" uuid NOT NULL, "periodId" uuid, "priceItemId" uuid, CONSTRAINT "PK_23d4e7e9b58d9939f113832915b" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "price_items" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "amount" character varying NOT NULL, "valid_from" TIMESTAMP WITH TIME ZONE NOT NULL, "valid_until" TIMESTAMP WITH TIME ZONE, "current" boolean NOT NULL DEFAULT true, "serviceId" uuid NOT NULL, "planId" uuid NOT NULL, CONSTRAINT "PK_56a8fdc66fe1056f3db3a2d871d" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "services_type_enum" AS ENUM('physical_card_issuing', 'card_reissue', 'virtual_card_issuing', 'active_card_maintenance', 'active_account_maintenance', 'account_opening', 'account_closing', 'withdrawal', 'deposit', 'transfer', 'boleto_payment', 'boleto_emission', 'boleto_validation', 'setup', 'subscription', 'hosting', 'phone_credits_purchase', 'mass_transport_credits_purchase', 'sms', 'email', 'consumer_data_query', 'cdq_address_dataset', 'cdq_phone_dataset', 'cdq_email_dataset', 'cdq_kyc_dataset', 'cdq_person_basic_dataset', 'cdq_company_basic_dataset', 'cdq_company_partners', 'ocr', 'facematch', 'compliance_check')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "services" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" character varying NOT NULL, "description" character varying NOT NULL, "type" "services_type_enum" NOT NULL, CONSTRAINT "PK_ba2d347a3168a296416c6c5ccb2" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "entry_types" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" character varying NOT NULL, "settings" jsonb NOT NULL DEFAULT '{}', "valid_from" TIMESTAMP WITH TIME ZONE NOT NULL, "valid_until" TIMESTAMP WITH TIME ZONE, "valid" boolean NOT NULL DEFAULT true, "serviceId" uuid NOT NULL, "planId" uuid NOT NULL, CONSTRAINT "PK_906493f182c8c4e1e142edac8e6" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`CREATE TYPE "plans_status_enum" AS ENUM('available', 'unavailable')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "plans" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" character varying NOT NULL, "valid_from" TIMESTAMP WITH TIME ZONE NOT NULL, "valid_until" TIMESTAMP WITH TIME ZONE, "status" "plans_status_enum" NOT NULL DEFAULT 'available', "default" boolean NOT NULL DEFAULT true, "billing_settings" jsonb NOT NULL, "supplierId" uuid NOT NULL, CONSTRAINT "PK_3720521a81c7c24fe9b7202ba61" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`CREATE TYPE "contracts_payment_method_enum" AS ENUM('boleto', 'transfer')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "contracts" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "valid_from" TIMESTAMP WITH TIME ZONE NOT NULL, "valid_until" TIMESTAMP WITH TIME ZONE, "current" boolean NOT NULL DEFAULT true, "prepaid" boolean NOT NULL DEFAULT true, "payment_method" "contracts_payment_method_enum" NOT NULL DEFAULT 'transfer', "supplierId" uuid NOT NULL, "contractorId" uuid NOT NULL, "planId" uuid NOT NULL, CONSTRAINT "PK_2c7b8f3a7b1acdd49497d83d0fb" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'plan_subscription_error', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."issues_category_enum" RENAME TO "issues_category_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider', 'identity_data_provider', 'custody_provider', 'billing', 'invoice', 'invoice_period', 'invoice_state_transition', 'period_state_transition', 'billing_plan_subscription')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum" USING "category"::"text"::"issues_category_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum_old"`, undefined);
    await queryRunner.query(`ALTER TABLE "issues" ALTER COLUMN "resource_id" DROP NOT NULL`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credits', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credits', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."alerts_issue_types_enum" RENAME TO "alerts_issue_types_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'plan_subscription_error', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum"[] USING "issue_types"::"text"::"alerts_issue_types_enum"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."alerts_issue_categories_enum" RENAME TO "alerts_issue_categories_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_categories_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider', 'identity_data_provider', 'custody_provider', 'billing', 'invoice', 'invoice_period', 'invoice_state_transition', 'period_state_transition', 'billing_plan_subscription')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_categories" TYPE "alerts_issue_categories_enum"[] USING "issue_categories"::"text"::"alerts_issue_categories_enum"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_categories_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "contract_states" ADD CONSTRAINT "FK_3634f3bdd7f8af6395626921886" FOREIGN KEY ("contractId") REFERENCES "contracts"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "period_states" ADD CONSTRAINT "FK_7bce51eb6b1bf00827e82f56f08" FOREIGN KEY ("periodId") REFERENCES "periods"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "invoice_states" ADD CONSTRAINT "FK_d75a0d9bf69cf2f9dfa4dac5e22" FOREIGN KEY ("invoiceId") REFERENCES "invoices"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "invoices" ADD CONSTRAINT "FK_42029329889d0ac216e7305c9fa" FOREIGN KEY ("contractorId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "invoices" ADD CONSTRAINT "FK_0c6a575c98b46988443ab6def61" FOREIGN KEY ("payment_proof") REFERENCES "transactions"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "periods" ADD CONSTRAINT "FK_47399814de481ef07e2e521b25c" FOREIGN KEY ("invoiceId") REFERENCES "invoices"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "entries" ADD CONSTRAINT "FK_30ef4fd0d2baad5b7300cd62e95" FOREIGN KEY ("typeId") REFERENCES "entry_types"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "entries" ADD CONSTRAINT "FK_159b533f50418abb3881d20402d" FOREIGN KEY ("periodId") REFERENCES "periods"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "entries" ADD CONSTRAINT "FK_28ed8d289ed8c091d104867b93c" FOREIGN KEY ("priceItemId") REFERENCES "price_items"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "price_items" ADD CONSTRAINT "FK_a3221e6d317b8070f53808c51dc" FOREIGN KEY ("serviceId") REFERENCES "services"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "price_items" ADD CONSTRAINT "FK_973e7c53260d749ee1d73e81dab" FOREIGN KEY ("planId") REFERENCES "plans"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "entry_types" ADD CONSTRAINT "FK_c636d8aafc75c7f41d119b03971" FOREIGN KEY ("serviceId") REFERENCES "services"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "entry_types" ADD CONSTRAINT "FK_7370068bf4c742ba4d7f0d6b70a" FOREIGN KEY ("planId") REFERENCES "plans"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "plans" ADD CONSTRAINT "FK_4397320f8264df63b2d83bdaa9d" FOREIGN KEY ("supplierId") REFERENCES "domains"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "contracts" ADD CONSTRAINT "FK_690b42c3ec45394fb4ede7f956c" FOREIGN KEY ("supplierId") REFERENCES "domains"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "contracts" ADD CONSTRAINT "FK_25e8a897e43bfc4dde1f0918995" FOREIGN KEY ("contractorId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "contracts" ADD CONSTRAINT "FK_418d75c2b95757a330a3ca34001" FOREIGN KEY ("planId") REFERENCES "plans"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "contracts" DROP CONSTRAINT "FK_418d75c2b95757a330a3ca34001"`, undefined);
    await queryRunner.query(`ALTER TABLE "contracts" DROP CONSTRAINT "FK_25e8a897e43bfc4dde1f0918995"`, undefined);
    await queryRunner.query(`ALTER TABLE "contracts" DROP CONSTRAINT "FK_690b42c3ec45394fb4ede7f956c"`, undefined);
    await queryRunner.query(`ALTER TABLE "plans" DROP CONSTRAINT "FK_4397320f8264df63b2d83bdaa9d"`, undefined);
    await queryRunner.query(`ALTER TABLE "entry_types" DROP CONSTRAINT "FK_7370068bf4c742ba4d7f0d6b70a"`, undefined);
    await queryRunner.query(`ALTER TABLE "entry_types" DROP CONSTRAINT "FK_c636d8aafc75c7f41d119b03971"`, undefined);
    await queryRunner.query(`ALTER TABLE "price_items" DROP CONSTRAINT "FK_973e7c53260d749ee1d73e81dab"`, undefined);
    await queryRunner.query(`ALTER TABLE "price_items" DROP CONSTRAINT "FK_a3221e6d317b8070f53808c51dc"`, undefined);
    await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_28ed8d289ed8c091d104867b93c"`, undefined);
    await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_159b533f50418abb3881d20402d"`, undefined);
    await queryRunner.query(`ALTER TABLE "entries" DROP CONSTRAINT "FK_30ef4fd0d2baad5b7300cd62e95"`, undefined);
    await queryRunner.query(`ALTER TABLE "periods" DROP CONSTRAINT "FK_47399814de481ef07e2e521b25c"`, undefined);
    await queryRunner.query(`ALTER TABLE "invoices" DROP CONSTRAINT "FK_0c6a575c98b46988443ab6def61"`, undefined);
    await queryRunner.query(`ALTER TABLE "invoices" DROP CONSTRAINT "FK_42029329889d0ac216e7305c9fa"`, undefined);
    await queryRunner.query(`ALTER TABLE "invoice_states" DROP CONSTRAINT "FK_d75a0d9bf69cf2f9dfa4dac5e22"`, undefined);
    await queryRunner.query(`ALTER TABLE "period_states" DROP CONSTRAINT "FK_7bce51eb6b1bf00827e82f56f08"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "contract_states" DROP CONSTRAINT "FK_3634f3bdd7f8af6395626921886"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_categories_enum_old" AS ENUM('boleto_payment', 'cdt_provider', 'celsius_api', 'celsius_provider', 'custody_provider', 'identity_data_provider', 'kyc_flow', 'other', 'p2p_payment', 'postback_delivery', 'provider_postback_processing', 'stellar_transaction', 'str_message', 'str_provider', 'str_proxy', 'transaction_refund', 'transaction_reverse', 'wallet_creation_flow', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_categories" TYPE "alerts_issue_categories_enum_old"[] USING "issue_categories"::"text"::"alerts_issue_categories_enum_old"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_categories_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "alerts_issue_categories_enum_old" RENAME TO  "alerts_issue_categories_enum"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum_old"[] USING "issue_types"::"text"::"alerts_issue_types_enum_old"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "alerts_issue_types_enum_old" RENAME TO  "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "issues" ALTER COLUMN "resource_id" SET NOT NULL`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum_old" AS ENUM('boleto_payment', 'cdt_provider', 'celsius_api', 'celsius_provider', 'custody_provider', 'identity_data_provider', 'kyc_flow', 'other', 'p2p_payment', 'postback_delivery', 'provider_postback_processing', 'stellar_transaction', 'str_message', 'str_provider', 'str_proxy', 'transaction_refund', 'transaction_reverse', 'wallet_creation_flow', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum_old" USING "category"::"text"::"issues_category_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_category_enum_old" RENAME TO  "issues_category_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "contracts"`, undefined);
    await queryRunner.query(`DROP TYPE "contracts_payment_method_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "plans"`, undefined);
    await queryRunner.query(`DROP TYPE "plans_status_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "entry_types"`, undefined);
    await queryRunner.query(`DROP TABLE "services"`, undefined);
    await queryRunner.query(`DROP TYPE "services_type_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "price_items"`, undefined);
    await queryRunner.query(`DROP TABLE "entries"`, undefined);
    await queryRunner.query(`DROP TYPE "entries_operationtype_enum"`, undefined);
    await queryRunner.query(`DROP TYPE "entries_status_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "periods"`, undefined);
    await queryRunner.query(`DROP TABLE "invoices"`, undefined);
    await queryRunner.query(`DROP TABLE "invoice_states"`, undefined);
    await queryRunner.query(`DROP TYPE "invoice_states_status_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "period_states"`, undefined);
    await queryRunner.query(`DROP TYPE "period_states_status_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "contract_states"`, undefined);
    await queryRunner.query(`DROP TYPE "contract_states_status_enum"`, undefined);
  }
}
