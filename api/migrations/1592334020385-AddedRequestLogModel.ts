import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedRequestLogModel1592334020385 implements MigrationInterface {
  name = 'AddedRequestLogModel1592334020385';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "request_logs_type_enum" AS ENUM('authorization')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "request_logs" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "request_logs_type_enum" NOT NULL, "request" jsonb, "response" jsonb, "userId" uuid NOT NULL, CONSTRAINT "PK_1edd3815ae37a9b9511f5a26dca" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "request_logs" ADD CONSTRAINT "FK_96ca790725861f7b54ce7636f40" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "request_logs" DROP CONSTRAINT "FK_96ca790725861f7b54ce7636f40"`, undefined);
    await queryRunner.query(`DROP TABLE "request_logs"`, undefined);
    await queryRunner.query(`DROP TYPE "request_logs_type_enum"`, undefined);
  }
}
