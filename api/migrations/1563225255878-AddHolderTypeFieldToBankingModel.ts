import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddHolderTypeFieldToBankingModel1563225255878 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "bankings_holder_type_enum" AS ENUM('corporate', 'personal')`);
    await queryRunner.query(
      `ALTER TABLE "bankings" ADD "holder_type" "bankings_holder_type_enum" NOT NULL DEFAULT 'personal'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" DROP COLUMN "holder_type"`);
    await queryRunner.query(`DROP TYPE "bankings_holder_type_enum"`);
  }
}
