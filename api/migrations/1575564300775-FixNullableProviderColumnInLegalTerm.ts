import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixNullableProviderColumnInLegalTerm1575564300775 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "legal_terms" DROP COLUMN "type"`, undefined);
    await queryRunner.query(`CREATE TYPE "legal_terms_type_enum" AS ENUM('terms_of_use', 'privacy_policy')`, undefined);
    await queryRunner.query(`ALTER TABLE "legal_terms" ADD "type" "legal_terms_type_enum" NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "legal_terms" ALTER COLUMN "provider" DROP NOT NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "legal_terms" ALTER COLUMN "provider" SET NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "legal_terms" DROP COLUMN "type"`, undefined);
    await queryRunner.query(`DROP TYPE "legal_terms_type_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "legal_terms" ADD "type" character varying NOT NULL`, undefined);
  }
}
