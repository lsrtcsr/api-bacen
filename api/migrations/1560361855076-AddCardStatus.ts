import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCardStatus1560361855076 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "cards_status_enum" RENAME TO "cards_status_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "cards_status_enum" AS ENUM('available', 'blocked', 'blocked_invalid_password', 'blocked_external', 'cancelled', 'suspect')`,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "status" TYPE "cards_status_enum" USING "status"::"text"::"cards_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "cards_status_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "cards_status_enum_old" AS ENUM('available', 'blocked', 'cancelled')`);
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "status" TYPE "cards_status_enum_old" USING "status"::"text"::"cards_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "cards_status_enum"`);
    await queryRunner.query(`ALTER TYPE "cards_status_enum_old" RENAME TO "cards_status_enum"`);
  }
}
