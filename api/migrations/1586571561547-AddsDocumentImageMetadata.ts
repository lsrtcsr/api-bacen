import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddsDocumentImageMetadata1586571561547 implements MigrationInterface {
  name = 'AddsDocumentImageMetadata1586571561547';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" ADD "metadata" jsonb`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "metadata"`, undefined);
  }
}
