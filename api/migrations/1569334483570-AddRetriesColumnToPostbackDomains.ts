import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRetriesColumnToPostbackDomains1569334483570 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumer_postbacks" ADD "retries" integer NOT NULL DEFAULT 0`);
    await queryRunner.query(`ALTER TABLE "transaction_postbacks" ADD "retries" integer NOT NULL DEFAULT 0`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "transaction_postbacks" DROP COLUMN "retries"`);
    await queryRunner.query(`ALTER TABLE "consumer_postbacks" DROP COLUMN "retries"`);
  }
}
