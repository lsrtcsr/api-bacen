import { MigrationInterface, QueryRunner } from 'typeorm';

export class UnifyMigrations1581369116208 implements MigrationInterface {
  name = 'UnifyMigrations1581369116208';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "postback_deliveries" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" character varying NOT NULL DEFAULT 'mediator', "retries" integer NOT NULL DEFAULT 0, "maxRetries" integer NOT NULL DEFAULT 3, "lastResponse" jsonb, "status" character varying NOT NULL DEFAULT 'pending', "url" character varying NOT NULL, "postbackId" uuid, CONSTRAINT "PK_a5d2ec42ab50916aa99b540cb3d" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`CREATE TYPE "postbacks_status_enum" AS ENUM('pending', 'executed', 'failed')`, undefined);
    await queryRunner.query(
      `CREATE TABLE "postbacks" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "executed_at" TIMESTAMP, "sent_at" TIMESTAMP, "status" "postbacks_status_enum" NOT NULL DEFAULT 'pending', "payload" jsonb NOT NULL, "entityType" character varying NOT NULL, "entityId" character varying NOT NULL, "urls" jsonb, "systemUrls" jsonb, CONSTRAINT "PK_92cc34835cbccae222fc8181bd4" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "postbacks" ("id", "created_at", "updated_at", "deleted_at", "executed_at", "status", "payload", "sent_at", "entityId", "entityType") SELECT "id", "created_at", "updated_at", "deleted_at", "executed_at", "status"::text::postbacks_status_enum, "payload", "sent_at", "consumerId" AS "entityId", 'consumer' AS "entityType" FROM "consumer_postbacks"`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "postback_deliveries" ("postbackId", "status", "created_at", "lastResponse", "retries", "maxRetries", "url") SELECT "id" AS "postbackId", "status"::text::postbacks_status_enum, "sent_at"  AS "created_at", "response", '0' AS "retries", '3' AS "maxRetries", 'unknown' AS "url" FROM "consumer_postbacks"`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "postbacks" ("id", "created_at", "updated_at", "deleted_at", "executed_at", "status", "payload", "sent_at", "entityId", "entityType") SELECT "id", "created_at", "updated_at", "deleted_at", "executed_at", "status"::text::postbacks_status_enum, "payload", "sent_at", "transactionId" AS "entityId", 'transaction' AS "entityType" FROM "transaction_postbacks"`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "postback_deliveries" ("postbackId", "status", "created_at", "lastResponse", "retries", "maxRetries", "url") SELECT "id" AS "postbackId", "status"::text::postbacks_status_enum, "sent_at"  AS "created_at", "response", '0' AS "retries", '3' AS "maxRetries", 'unknown' AS "url" FROM "transaction_postbacks"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "domains" ADD "postback_urls" jsonb`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" ADD "system_postback_urls" jsonb`, undefined);
    await queryRunner.query(`UPDATE "domains" SET "postback_urls" = to_jsonb(array["postbackUrl"])`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" DROP COLUMN "postbackUrl"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "postback_deliveries" ADD CONSTRAINT "FK_26623cc655d596abc79fca59619" FOREIGN KEY ("postbackId") REFERENCES "postbacks"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "postback_deliveries" DROP CONSTRAINT "FK_26623cc655d596abc79fca59619"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "domains" DROP COLUMN "system_postback_urls"`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" DROP COLUMN "postback_urls"`, undefined);
    await queryRunner.query(`ALTER TABLE "domains" ADD "postbackUrl" text`, undefined);
    await queryRunner.query(`DROP TABLE "postbacks"`, undefined);
    await queryRunner.query(`DROP TYPE "postbacks_status_enum"`, undefined);
    await queryRunner.query(`DROP TABLE "postback_deliveries"`, undefined);
    /* queryRunner.query(
      `INSERT INTO "consumer_postbacks" SELECT ("created_at", "updated_at", "deleted_at", "executed_at", "status", "payload", "response", "sent_at", "entityId" AS consumerId) FROM "postbacks" WHERE "entityType" = 'consumer'`,
    );
    queryRunner.query(
      `INSERT INTO "transaction_postbacks" SELECT ("created_at", "updated_at", "deleted_at", "executed_at", "status", "payload", "response", "sent_at", "entityId" AS transactionId) FROM "postbacks"  WHERE "entityType" = 'transaction'`,
    ); */
  }
}
