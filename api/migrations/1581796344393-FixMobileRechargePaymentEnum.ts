import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixMobileRechargePaymentEnum1581796344393 implements MigrationInterface {
  name = 'FixMobileRechargePaymentEnum1581796344393';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`UPDATE "payments" SET type='phone_credit' WHERE type='mobile_recharge'`);
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'authorized_card', 'authorized_card_reversal', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'authorized_card', 'authorized_card_reversal', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("consumerId", "type") WHERE deleted_at IS NULL`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('authorized_card', 'authorized_card_reversal', 'balance_adjustment', 'boleto', 'card', 'deposit', 'manual_adjustment', 'mobile_recharge', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('authorized_card', 'authorized_card_reversal', 'balance_adjustment', 'boleto', 'card', 'deposit', 'manual_adjustment', 'mobile_recharge', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
  }
}
