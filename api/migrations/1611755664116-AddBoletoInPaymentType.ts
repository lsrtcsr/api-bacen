import { MigrationInterface, QueryRunner } from 'typeorm';
import { AddViewPaymentsAccounting1607534616975 } from './1607534616975-AddViewPaymentsAccounting';

export class AddBoletoInPaymentType1611755664116 implements MigrationInterface {
  name = 'AddBoletoInPaymentType1611755664116';

  undoRedoMigrations: MigrationInterface[] = [
    new AddViewPaymentsAccounting1607534616975(), // Why?: For update paymentType
  ];

  public async up(queryRunner: QueryRunner): Promise<any> {
    // Undo Migrations
    this.undoRedoMigrations.forEach(async (migration) => await migration.down(queryRunner));

    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('balance_adjustment', 'boleto', 'boleto_in', 'card', 'authorized_card', 'authorized_card_reversal', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);

    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('balance_adjustment', 'boleto', 'boleto_in', 'card', 'authorized_card', 'authorized_card_reversal', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);

    // Redo Migrations
    this.undoRedoMigrations.forEach(async (migration) => await migration.up(queryRunner));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // Undo Migrations
    this.undoRedoMigrations.forEach(async (migration) => await migration.down(queryRunner));

    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('authorized_card', 'authorized_card_reversal', 'balance_adjustment', 'boleto', 'card', 'deposit', 'manual_adjustment', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);

    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('authorized_card', 'authorized_card_reversal', 'balance_adjustment', 'boleto', 'card', 'deposit', 'manual_adjustment', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);

    // Redo Migrations
    this.undoRedoMigrations.forEach(async (migration) => await migration.up(queryRunner));
  }
}
