import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateWalletAndAssetRegistrationEnums1580245484060 implements MigrationInterface {
  name = 'UpdateWalletAndAssetRegistrationEnums1580245484060';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."asset_registration_states_status_enum" RENAME TO "asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum" AS ENUM('pending', 'registered_in_provider', 'pending_provider_approval', 'ready', 'failed', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum" USING "status"::"text"::"asset_registration_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum_old"`, undefined);
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."wallet_states_status_enum" RENAME TO "wallet_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "wallet_states_status_enum" AS ENUM('pending', 'registered', 'registered_in_provider', 'pending_provider_approval', 'ready', 'failed')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "status" TYPE "wallet_states_status_enum" USING "status"::"text"::"wallet_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" SET DEFAULT 'pending'`, undefined);
    await queryRunner.query(`DROP TYPE "wallet_states_status_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("walletId", "status") `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "wallet_states_status_enum_old" AS ENUM('failed', 'pending', 'ready', 'registered')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "status" TYPE "wallet_states_status_enum_old" USING "status"::"text"::"wallet_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" SET DEFAULT 'pending'`, undefined);
    await queryRunner.query(`DROP TYPE "wallet_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "wallet_states_status_enum_old" RENAME TO  "wallet_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("status", "walletId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum_old" AS ENUM('failed', 'pending', 'processing', 'registered', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum_old" USING "status"::"text"::"asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "asset_registration_states_status_enum_old" RENAME TO  "asset_registration_states_status_enum"`,
      undefined,
    );
  }
}
