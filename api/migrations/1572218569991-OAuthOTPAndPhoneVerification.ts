import { MigrationInterface, QueryRunner } from 'typeorm';

export class OAuthOTPAndPhoneVerification1572218569991 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_c4384651f6a28da298612aaa74"`, undefined);
    await queryRunner.query(
      `CREATE TABLE "oauth_otp_tokens" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "secret_seed" character varying NOT NULL, "secret_encoding" character varying NOT NULL DEFAULT 'base32', "qrcode_url" character varying, "expires_at" TIMESTAMP WITH TIME ZONE, "failed_attempts" integer DEFAULT 0, "userId" uuid NOT NULL, CONSTRAINT "PK_f92a4d89fb8e3d8be5125306ebe" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "twoFactorSecret"`, undefined);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "require2FA"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "phones" ADD "countryCode" character varying NOT NULL DEFAULT '55'`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "phones" ADD "verificationSalt" character varying`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" ADD "verificationHash" character varying`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" ADD "generatedVerificationAt" TIMESTAMP WITH TIME ZONE`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" ADD "failed_verification_attempts" integer DEFAULT 0`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" ADD "verified_by" uuid`, undefined);
    await queryRunner.query(`ALTER TABLE "users" ADD "two_factor_required" boolean NOT NULL DEFAULT false`, undefined);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "agencyDigit" DROP NOT NULL`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_a3191cbe48d1a8cd291bd71b7d" ON "phones" ("countryCode", "code", "number", "consumerId") `,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ADD CONSTRAINT "FK_cde263b7a79c328d565ee533225" FOREIGN KEY ("verified_by") REFERENCES "oauth_access_token"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_otp_tokens" ADD CONSTRAINT "FK_c0b42d8f3233cdceeac0fdd47ed" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "oauth_otp_tokens" ADD "last_failed_attempt" TIMESTAMP`, undefined);
    await queryRunner.query(
      `ALTER TABLE "oauth_otp_tokens" ADD "last_failed_attempt_token" character varying`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_otp_tokens" ADD "last_successful_attempt_token" character varying`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_otp_tokens" DROP COLUMN "last_successful_attempt_token"`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_otp_tokens" DROP COLUMN "last_failed_attempt_token"`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_otp_tokens" DROP COLUMN "last_failed_attempt"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "oauth_otp_tokens" DROP CONSTRAINT "FK_c0b42d8f3233cdceeac0fdd47ed"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "phones" DROP CONSTRAINT "FK_cde263b7a79c328d565ee533225"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_a3191cbe48d1a8cd291bd71b7d"`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "agencyDigit" SET NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`, undefined);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "two_factor_required"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "verified_by"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "failed_verification_attempts"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "generatedVerificationAt"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "verificationHash"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "verificationSalt"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "countryCode"`, undefined);
    await queryRunner.query(`ALTER TABLE "users" ADD "require2FA" boolean NOT NULL DEFAULT false`, undefined);
    await queryRunner.query(`ALTER TABLE "users" ADD "twoFactorSecret" character varying`, undefined);
    await queryRunner.query(`DROP TABLE "oauth_otp_tokens"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_c4384651f6a28da298612aaa74" ON "phones" ("code", "number") `,
      undefined,
    );
  }
}
