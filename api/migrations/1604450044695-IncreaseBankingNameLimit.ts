import { MigrationInterface, QueryRunner } from 'typeorm';

export class IncreaseBankingNameLimit1604450044695 implements MigrationInterface {
  name = 'IncreaseBankingNameLimit1604450044695';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" type character varying(80)`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" type character varying(30)`, undefined);
  }
}
