import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateEnumTypesValues1579692127832 implements MigrationInterface {
  name = 'UpdateEnumTypesValues1579692127832';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'user_sent_to_manual_verification', 'rate_limit_exceeded')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."payments_status_enum" RENAME TO "payments_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" TYPE "text"`, undefined);
    await queryRunner.query(`UPDATE "payments" SET "status" = 'settled' WHERE "status" = 'confirmed'`, undefined);
    await queryRunner.query(`UPDATE "payments" SET "status" = 'failed' WHERE "status" = 'reversed'`, undefined);
    await queryRunner.query(`CREATE TYPE "payments_status_enum" AS ENUM('authorized', 'settled', 'failed')`, undefined);
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "status" TYPE "payments_status_enum" USING "status"::"text"::"payments_status_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" SET DEFAULT 'authorized'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_status_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."alerts_issue_types_enum" RENAME TO "alerts_issue_types_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'user_sent_to_manual_verification', 'rate_limit_exceeded')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum"[] USING "issue_types"::"text"::"alerts_issue_types_enum"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum_old"[] USING "issue_types"::"text"::"alerts_issue_types_enum_old"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "alerts_issue_types_enum_old" RENAME TO  "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_status_enum_old" AS ENUM('authorized', 'confirmed', 'failed', 'reversed')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "status" TYPE "payments_status_enum_old" USING "status"::"text"::"payments_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" SET DEFAULT 'authorized'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_status_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_status_enum_old" RENAME TO  "payments_status_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'phone_credit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`, undefined);
  }
}
