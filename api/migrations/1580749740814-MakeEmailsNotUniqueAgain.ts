import { MigrationInterface, QueryRunner } from 'typeorm';

export class MakeEmailsNotUniqueAgain1580749740814 implements MigrationInterface {
  name = 'MakeEmailsNotUniqueAgain1580749740814';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_b383987bfa6e6a8745085621d0"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_b383987bfa6e6a8745085621d0" ON "users" ("email") WHERE (deleted_at IS NULL)`,
      undefined,
    );
  }
}
