import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRequestTable1604611862474 implements MigrationInterface {
  name = 'AddRequestTable1604611862474';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "requests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "idempotenceKey" text NOT NULL, "status" integer NOT NULL, "contentType" text NOT NULL DEFAULT 'plain/text', "body" jsonb NOT NULL DEFAULT '{}', CONSTRAINT "PK_0428f484e96f9e6a55955f29b5f" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_d5860e693170ef7c7e264f033c" ON "requests" ("idempotenceKey") WHERE deleted_at IS NULL`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_d5860e693170ef7c7e264f033c"`, undefined);
    await queryRunner.query(`DROP TABLE "requests"`, undefined);
  }
}
