import { MigrationInterface, QueryRunner } from 'typeorm';

export class ForeignRelationsIndices1606405632631 implements MigrationInterface {
  name = 'ForeignRelationsIndices1606405632631';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE INDEX "IDX_7d7dfbe1e1e0268e255a5bad43" ON "transaction_states" ("transactionId") `,
      undefined,
    );
    await queryRunner.query(`CREATE INDEX "IDX_387f8a712ecc551a9e07270139" ON "transactions" ("sourceId") `, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_3c73d272ee1159d384de6f7d0a" ON "transactions" ("bankingId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_c39d78e8744809ece8ca95730e" ON "payments" ("transactionId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_4e23f9a12cc44828e1c7857285" ON "payments" ("destinationId") `,
      undefined,
    );
    await queryRunner.query(`CREATE INDEX "IDX_eae0ab7f66dae5e3ebbab43845" ON "payments" ("assetId") `, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_54693054283a3cecbb1315ad90" ON "asset_registration_states" ("assetRegistrationId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_801fce09522062495c83a0ba3a" ON "asset_registrations" ("assetId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_665770c6105c5b5a1e6753c1e6" ON "asset_registrations" ("walletId") `,
      undefined,
    );
    await queryRunner.query(`CREATE INDEX "IDX_2ecdb33f23e9a6fc392025c0b9" ON "wallets" ("userId") `, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_6384dd32824b7f6b83d4eb6743" ON "wallet_states" ("walletId") `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_6384dd32824b7f6b83d4eb6743"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_2ecdb33f23e9a6fc392025c0b9"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_665770c6105c5b5a1e6753c1e6"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_801fce09522062495c83a0ba3a"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_54693054283a3cecbb1315ad90"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_eae0ab7f66dae5e3ebbab43845"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_4e23f9a12cc44828e1c7857285"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_c39d78e8744809ece8ca95730e"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_3c73d272ee1159d384de6f7d0a"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_387f8a712ecc551a9e07270139"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_7d7dfbe1e1e0268e255a5bad43"`, undefined);
  }
}
