import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeRequestLogUserToToken1594687732190 implements MigrationInterface {
  name = 'ChangeRequestLogUserToToken1594687732190';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "request_logs" DROP CONSTRAINT "FK_96ca790725861f7b54ce7636f40"`, undefined);
    await queryRunner.query(
      `UPDATE request_logs SET "userId"=subquery.id FROM (SELECT id FROM oauth_access_token LIMIT 1) AS subquery`,
    );
    await queryRunner.query(`ALTER TABLE "request_logs" RENAME COLUMN "userId" TO "tokenId"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "request_logs" ADD CONSTRAINT "FK_7b0beff66706cfbeff54e3aa1f6" FOREIGN KEY ("tokenId") REFERENCES "oauth_access_token"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "request_logs" DROP CONSTRAINT "FK_7b0beff66706cfbeff54e3aa1f6"`, undefined);
    await queryRunner.query(`ALTER TABLE "request_logs" RENAME COLUMN "tokenId" TO "userId"`, undefined);
    await queryRunner.query(
      `ALTER TABLE "request_logs" ADD CONSTRAINT "FK_96ca790725861f7b54ce7636f40" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }
}
