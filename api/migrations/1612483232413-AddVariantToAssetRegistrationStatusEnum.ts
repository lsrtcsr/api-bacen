import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddVariantToAssetRegistrationStatusEnum1612483232413 implements MigrationInterface {
  name = 'AddVariantToAssetRegistrationStatusEnum1612483232413';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."asset_registration_states_status_enum" RENAME TO "asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum" AS ENUM('pending_registration', 'pending_documents', 'processing', 'approved', 'ready', 'rejected', 'blocked', 'failed', 'pending', 'registered_in_provider', 'pending_provider_approval')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum" USING "status"::"text"::"asset_registration_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum_old" AS ENUM('approved', 'blocked', 'failed', 'pending', 'pending_documents', 'pending_provider_approval', 'processing', 'ready', 'registered_in_provider', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum_old" USING "status"::"text"::"asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "asset_registration_states_status_enum_old" RENAME TO  "asset_registration_states_status_enum"`,
      undefined,
    );
  }
}
