import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixTimestampDefault1559067400214 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_postbacks" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_postbacks" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "products" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "products" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "domains" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "domains" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_postbacks" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_postbacks" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "document_states" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "document_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "adressess" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "adressess" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_states" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_secret_token" ALTER COLUMN "created_at" TYPE TIMESTAMP WITH TIME ZONE USING "created_at" AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT clock_timestamp() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_secret_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP WITH TIME ZONE USING "updated_at" AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT clock_timestamp() `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "oauth_secret_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_secret_token" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_states" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "adressess" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "adressess" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "document_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "document_states" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_postbacks" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_postbacks" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "domains" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "domains" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "products" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "products" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_postbacks" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_postbacks" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "updated_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "updated_at" SET DEFAULT now() `,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ALTER COLUMN "created_at" TYPE TIMESTAMP AT TIME ZONE 'UTC', ALTER COLUMN "created_at" SET DEFAULT now() `,
    );
  }
}
