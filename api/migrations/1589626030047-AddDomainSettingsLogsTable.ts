import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddDomainSettingsLogsTable1589626030047 implements MigrationInterface {
  name = 'AddDomainSettingsLogsTable1589626030047';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "domain_settings_logs" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "setting" character varying NOT NULL, "userId" character varying NOT NULL, "oldValue" integer NOT NULL, "newValue" integer NOT NULL, CONSTRAINT "PK_431e1f08ede4626e4c909d5cfe3" PRIMARY KEY ("id"))`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "domain_settings_logs"`, undefined);
  }
}
