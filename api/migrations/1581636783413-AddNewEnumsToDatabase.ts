import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewEnumsToDatabase1581636783413 implements MigrationInterface {
  name = 'AddNewEnumsToDatabase1581636783413';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."documents_type_enum" RENAME TO "documents_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('brl_individual_reg', 'brl_drivers_license', 'CCMEI', 'EI_REGISTRATION_REQUIREMENT', 'EIRELI_INCORPORATION_STATEMENT', 'COMPANY_BYLAWS', 'ARTICLES_OF_ASSOCIATION', 'brl_address_statement', 'signature', 'other')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`, undefined);

    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('brl_address_statement', 'brl_drivers_license', 'brl_individual_reg', 'other', 'signature')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO  "documents_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("type", "consumerId") WHERE (deleted_at IS NULL)`,
      undefined,
    );
  }
}
