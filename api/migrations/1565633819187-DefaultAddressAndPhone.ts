import { MigrationInterface, QueryRunner } from 'typeorm';

export class DefaultAddressAndPhone1565633819187 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "phones" ADD "default" boolean`);
    await queryRunner.query(`ALTER TABLE "adressess" ADD "default" boolean`);
    await queryRunner.query(`ALTER TABLE "adressess" ADD "status" character varying NOT NULL DEFAULT 'own'`);
    await queryRunner.query(`CREATE UNIQUE INDEX "phone_default_unique" ON "phones" ("consumerId", "default") `);
    await queryRunner.query(`CREATE UNIQUE INDEX "address_default_unique" ON "adressess" ("consumerId", "default") `);
    // Ensure at least one address and one phone is default per consumer, defaulting to oldest one
    await queryRunner.query(`UPDATE adressess SET "default" = true WHERE adressess.created_at IN
            (SELECT DISTINCT MIN(a.created_at) as created_at FROM adressess a
            WHERE a."default" IS NULL
            GROUP BY a."default", a."consumerId")`);
    await queryRunner.query(`UPDATE phones SET "default" = true WHERE phones.created_at IN
            (SELECT DISTINCT MIN(p.created_at) as created_at FROM phones p
            WHERE p."default" IS NULL
            GROUP BY p."default", p."consumerId")`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "address_default_unique"`);
    await queryRunner.query(`DROP INDEX "phone_default_unique"`);
    await queryRunner.query(`ALTER TABLE "adressess" DROP COLUMN "status"`);
    await queryRunner.query(`ALTER TABLE "adressess" DROP COLUMN "default"`);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "default"`);
  }
}
