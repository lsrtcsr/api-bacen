import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropUniqueTaxIdInConsumer1559965727127 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" DROP CONSTRAINT "UQ_fc884f17076bcdf35603d6e24f4"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ADD CONSTRAINT "UQ_fc884f17076bcdf35603d6e24f4" UNIQUE ("taxId")`);
  }
}
