import { MigrationInterface, QueryRunner } from 'typeorm';
import { TransactionAdditionalData } from '@bacen/base-sdk';

export class ChangeTypeOfBranchAndNumberColumnsOfBankingTable1604725131766 implements MigrationInterface {
  name = 'ChangeTypeOfBranchAndNumberColumnsOfBankingTable1604725131766';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "agency" TYPE character varying(4)`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "account" TYPE character varying(20)`, undefined);

    const transactions: { id: string; additional_data: TransactionAdditionalData }[] = await queryRunner.query(
      `SELECT transactions."id", transactions."additional_data" FROM transactions WHERE transactions."additional_data" IS NOT NULL AND (transactions."additional_data" ? 'destination' OR transactions."additional_data" ? 'source')`,
    );

    for (const transaction of transactions) {
      const additionalData = {
        ...transaction.additional_data,
        source: transaction.additional_data?.source
          ? {
              ...transaction.additional_data.source,
              branchNumber: transaction.additional_data.source?.branchNumber?.toString(),
              accountNumber: transaction.additional_data.source?.accountNumber?.toString(),
            }
          : undefined,
        destination: transaction.additional_data?.destination
          ? {
              ...transaction.additional_data.destination,
              branchNumber: transaction.additional_data.destination?.branchNumber?.toString(),
              accountNumber: transaction.additional_data.destination?.accountNumber?.toString(),
            }
          : undefined,
      };
      await queryRunner.query(`UPDATE "transactions" SET "additional_data" = $1 WHERE "id" = '${transaction.id}'`, [
        additionalData,
      ]);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const transactions: { id: string; additionalData: TransactionAdditionalData }[] = await queryRunner.query(
      `SELECT transactions."id", transactions."additional_data" FROM transactions WHERE transactions."additional_data" IS NOT NULL AND (transactions."additional_data" ? 'destination' OR transactions."additional_data" ? 'source')`,
    );

    for (const transaction of transactions) {
      const additionalData = {
        ...transaction.additionalData,
        source: transaction.additionalData?.source
          ? {
              ...transaction.additionalData.source,
              branchNumber: transaction.additionalData.source?.branchNumber
                ? Number(transaction.additionalData.source.branchNumber)
                : undefined,
              accountNumber: transaction.additionalData.source?.accountNumber
                ? Number(transaction.additionalData.source.accountNumber)
                : undefined,
            }
          : undefined,
        destination: transaction.additionalData?.destination
          ? {
              ...transaction.additionalData.destination,
              branchNumber: transaction.additionalData.destination?.branchNumber
                ? Number(transaction.additionalData.destination.branchNumber)
                : undefined,
              accountNumber: transaction.additionalData.destination?.accountNumber
                ? Number(transaction.additionalData.destination.accountNumber)
                : undefined,
            }
          : undefined,
      };
      await queryRunner.query(`UPDATE "transactions" SET "additional_data" = $1 WHERE "id" = '${transaction.id}'`, [
        additionalData,
      ]);
    }

    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "agency" TYPE numeric(4,0) using code::numeric`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "account" TYPE numeric(20,0) using code::numeric`,
      undefined,
    );
  }
}
