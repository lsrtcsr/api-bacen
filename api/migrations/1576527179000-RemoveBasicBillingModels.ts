import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveBasicBillingModels1576527179000 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "domain_fees" DROP CONSTRAINT "FK_a8dd1e2d74fe17f83845809ba0e"`, undefined);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`, undefined);
    await queryRunner.query(`DROP TABLE "billing_entries"`, undefined);
    await queryRunner.query(`DROP TABLE "domain_fees"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "domain_fees" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" character varying NOT NULL, "fixed" character varying, "percentage" integer, "domainId" uuid NOT NULL, "walletId" uuid NOT NULL, CONSTRAINT "PK_8b4fc8e045f516cef81d318dd2b" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "billing_entries" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "amount" character varying NOT NULL, CONSTRAINT "PK_1aa53e6d7cbc3df6e268260e9b8" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`, undefined);
    await queryRunner.query(
      `ALTER TABLE "domain_fees" ADD CONSTRAINT "FK_a8dd1e2d74fe17f83845809ba0e" FOREIGN KEY ("walletId") REFERENCES "wallets"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
  }
}
