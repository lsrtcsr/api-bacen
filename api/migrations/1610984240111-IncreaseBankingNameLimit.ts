import { MigrationInterface, QueryRunner } from 'typeorm';

export class IncreaseBankingNameLimit1610984240111 implements MigrationInterface {
  name = 'IncreaseBankingNameLimit1610984240111';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" TYPE character varying(100)`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" TYPE character varying(80)`, undefined);
  }
}
