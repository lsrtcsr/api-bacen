import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddAdditionalDataToOauthSecretAndChangedPhoneCodeToNullable1599926657718 implements MigrationInterface {
  name = 'AddAdditionalDataToOauthSecretAndChangedPhoneCodeToNullable1599926657718';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_d8619dce6e396bdab85fde6783"`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_secret_token" ADD "additionalData" jsonb`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" ADD "_code" character varying(3)`, undefined);
    await queryRunner.query(`UPDATE "phones" SET "_code"  = "code"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "code"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" RENAME COLUMN "_code" TO "code"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "phones" ADD "_code" character varying(2)`, undefined);
    await queryRunner.query(`UPDATE "phones" SET "_code"  = "code"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" DROP COLUMN "code"`, undefined);
    await queryRunner.query(`ALTER TABLE "phones" RENAME COLUMN "_code" TO "code"`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_secret_token" DROP COLUMN "additionalData"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_d8619dce6e396bdab85fde6783" ON "phones" ("number", "consumerId", "countryCode", "code") WHERE (deleted_at IS NULL)`,
      undefined,
    );
  }
}
