import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeUserLimitLiabilityRelationship1565843155854 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user_limits" DROP CONSTRAINT "FK_8bc6532d6e8727943de6373a784"`);
    await queryRunner.query(`ALTER TABLE "user_limits" DROP CONSTRAINT "REL_8bc6532d6e8727943de6373a78"`);
    await queryRunner.query(
      `ALTER TABLE "user_limits" ADD CONSTRAINT "FK_8bc6532d6e8727943de6373a784" FOREIGN KEY ("created_by") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user_limits" DROP CONSTRAINT "FK_8bc6532d6e8727943de6373a784"`);
    await queryRunner.query(
      `ALTER TABLE "user_limits" ADD CONSTRAINT "REL_8bc6532d6e8727943de6373a78" UNIQUE ("created_by")`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ADD CONSTRAINT "FK_8bc6532d6e8727943de6373a784" FOREIGN KEY ("created_by") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
