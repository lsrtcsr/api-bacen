import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedNewBankingInfo1553798323056 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ADD "taxId" character varying NOT NULL`);
    await queryRunner.query(`ALTER TABLE "bankings" ADD "name" character varying NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" DROP COLUMN "name"`);
    await queryRunner.query(`ALTER TABLE "bankings" DROP COLUMN "taxId"`);
  }
}
