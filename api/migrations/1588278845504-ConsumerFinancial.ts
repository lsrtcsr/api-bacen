import { MigrationInterface, QueryRunner } from 'typeorm';

export class ConsumerFinancial1588278845504 implements MigrationInterface {
  name = 'ConsumerFinancial1588278845504';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ADD "financialEquity" character varying NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" ADD "financialProfit" character varying NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "financialProfit"`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "financialEquity"`, undefined);
  }
}
