import { MigrationInterface, QueryRunner } from 'typeorm';
import { TransitoryAccountType } from '@bacen/base-sdk';
import { Wallet } from '../models';

export class AddDefaultTypeToTransitoryAccountsWithoutType1584458052616 implements MigrationInterface {
  name = 'AddDefaultTypeToTransitoryAccountsWithoutType1584458052616';

  public async up(queryRunner: QueryRunner): Promise<any> {
    const transitoryAccountsWithoutType: Wallet[] = await queryRunner.query(
      `SELECT * FROM wallets WHERE "additionalData"->>'isTransitoryAccount'='true' AND "additionalData"->>'transitoryAccountType' IS NULL`,
    );
    for (const wallet of transitoryAccountsWithoutType) {
      await queryRunner.query(
        `UPDATE wallets SET "additionalData"=jsonb_set("additionalData", '{transitoryAccountType}', '"${TransitoryAccountType.CARD_TRANSACTION}"'::jsonb) WHERE "id"=$1`,
        [wallet.id],
      );
    }
  }

  // NOOP - irreversible migration.
  public async down(queryRunner: QueryRunner): Promise<any> {}
}
