import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCelcoinToAssetProviderEnum1585165544427 implements MigrationInterface {
  name = 'AddCelcoinToAssetProviderEnum1585165544427';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."assets_provider_enum" RENAME TO "assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum" AS ENUM('stellar', 'cdt-visa', 'celsius-network', 'celcoin-provider', 'lecca-provider', 'parati-provider', 'creditas-provider', 'bs2')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum" USING "provider"::"text"::"assets_provider_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum_old" AS ENUM('bs2', 'cdt-visa', 'celsius-network', 'creditas-provider', 'lecca-provider', 'parati-provider', 'stellar')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum_old" USING "provider"::"text"::"assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "assets_provider_enum_old" RENAME TO  "assets_provider_enum"`, undefined);
  }
}
