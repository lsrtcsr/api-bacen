import { MigrationInterface, QueryRunner } from 'typeorm';

export class addAssetCodeToUsersLimits1606428329313 implements MigrationInterface {
  name = 'addAssetCodeToUsersLimits1606428329313';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user_limits" ADD "asset" character varying`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
