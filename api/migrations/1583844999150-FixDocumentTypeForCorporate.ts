import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixDocumentTypeForCorporate1583844999150 implements MigrationInterface {
  name = 'FixDocumentTypeForCorporate1583844999150';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."documents_type_enum" RENAME TO "documents_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('brl_individual_reg', 'brl_drivers_license', 'ccmei', 'ei_registration_requirement', 'eireli_incorporation_statement', 'company_bylaws', 'articles_of_association', 'brl_address_statement', 'signature', 'other')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("consumerId", "type") WHERE deleted_at IS NULL`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('ARTICLES_OF_ASSOCIATION', 'CCMEI', 'COMPANY_BYLAWS', 'EIRELI_INCORPORATION_STATEMENT', 'EI_REGISTRATION_REQUIREMENT', 'brl_address_statement', 'brl_drivers_license', 'brl_individual_reg', 'other', 'signature')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO  "documents_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("type", "consumerId") WHERE (deleted_at IS NULL)`,
      undefined,
    );
  }
}
