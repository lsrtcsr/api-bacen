import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddsConsumerMetricsViews1586717733437 implements MigrationInterface {
  name = 'AddsConsumerMetricsViews1586717733437';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE VIEW "view_consumer_by_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(users.id) as count
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    GROUP BY latest_states.status
  `,
      undefined,
    );
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as userId,
            consumers.id as consumerId,
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."birthday",
            consumers."companyData"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_consumer_with_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_consumer_by_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_consumer_by_states"`, undefined);
  }
}
