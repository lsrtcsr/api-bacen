import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddValuesToCustodyProviderEnum1571238236371 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."assets_provider_enum" RENAME TO "assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum" AS ENUM('stellar', 'cdt-visa', 'celsius-network', 'lecca-provider', 'parati-provider', 'creditas-provider')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum" USING "provider"::"text"::"assets_provider_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."bankings_holder_type_enum" RENAME TO "bankings_holder_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "bankings_holder_type_enum" AS ENUM('corporate', 'personal', 'financial_institution')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "holder_type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "holder_type" TYPE "bankings_holder_type_enum" USING "holder_type"::"text"::"bankings_holder_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "holder_type" SET DEFAULT 'personal'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_holder_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."consumers_type_enum" RENAME TO "consumers_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "consumers_type_enum" AS ENUM('corporate', 'personal', 'financial_institution')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "type" TYPE "consumers_type_enum" USING "type"::"text"::"consumers_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" SET DEFAULT 'personal'`, undefined);
    await queryRunner.query(`DROP TYPE "consumers_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "consumers_type_enum_old" AS ENUM('corporate', 'personal')`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "type" TYPE "consumers_type_enum_old" USING "type"::"text"::"consumers_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" SET DEFAULT 'personal'`, undefined);
    await queryRunner.query(`DROP TYPE "consumers_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "consumers_type_enum_old" RENAME TO  "consumers_type_enum"`, undefined);
    await queryRunner.query(`CREATE TYPE "bankings_holder_type_enum_old" AS ENUM('corporate', 'personal')`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "holder_type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "holder_type" TYPE "bankings_holder_type_enum_old" USING "holder_type"::"text"::"bankings_holder_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "holder_type" SET DEFAULT 'personal'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_holder_type_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "bankings_holder_type_enum_old" RENAME TO  "bankings_holder_type_enum"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum_old" AS ENUM('cdt-visa', 'celsius-network', 'stellar')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum_old" USING "provider"::"text"::"assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "assets_provider_enum_old" RENAME TO  "assets_provider_enum"`, undefined);
  }
}
