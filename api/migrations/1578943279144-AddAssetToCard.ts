import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddAssetToCard1578943279144 implements MigrationInterface {
  name = 'AddAssetToCard1578943279144';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" ADD "asset" character varying NOT NULL DEFAULT 'root'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "cards" DROP COLUMN "asset"`, undefined);
  }
}
