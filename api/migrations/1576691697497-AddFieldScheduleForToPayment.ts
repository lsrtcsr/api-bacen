import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddFieldScheduleForToPayment1576691697497 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payments" ADD "schedule_for" date`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payments" DROP COLUMN "schedule_for"`, undefined);
  }
}
