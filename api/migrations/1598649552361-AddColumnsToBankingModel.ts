import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddColumnsToBankingModel1598649552361 implements MigrationInterface {
  name = 'AddColumnsToBankingModel1598649552361';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "bankings" ADD "has_same_source_fi" boolean NOT NULL DEFAULT false`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ADD "key" character varying(77)`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."bankings_type_enum" RENAME TO "bankings_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum" AS ENUM('checking', 'savings', 'payment', 'deposit', 'investment', 'guaranteed', 'settlement', 'salary')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum" USING "type"::"text"::"bankings_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" DROP NOT NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "name" SET NOT NULL`, undefined);
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum_old" AS ENUM('checking', 'deposit', 'guaranteed', 'investment', 'payment', 'savings', 'settlement')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum_old" USING "type"::"text"::"bankings_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "bankings_type_enum_old" RENAME TO  "bankings_type_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" DROP COLUMN "key"`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" DROP COLUMN "has_same_source_fi"`, undefined);
  }
}
