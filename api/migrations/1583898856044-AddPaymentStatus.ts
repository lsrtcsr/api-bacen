import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPaymentStatus1583898856044 implements MigrationInterface {
  name = 'AddPaymentStatus1583898856044';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."payments_status_enum" RENAME TO "payments_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "payments_status_enum" AS ENUM('authorized', 'settled', 'reversed', 'failed')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "status" TYPE "payments_status_enum" USING "status"::"text"::"payments_status_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" SET DEFAULT 'authorized'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_status_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payments_status_enum_old" AS ENUM('authorized', 'settled', 'failed')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "status" TYPE "payments_status_enum_old" USING "status"::"text"::"payments_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "status" SET DEFAULT 'authorized'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_status_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_status_enum_old" RENAME TO  "payments_status_enum"`, undefined);
  }
}
