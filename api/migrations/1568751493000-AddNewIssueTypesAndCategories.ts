import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewIssueTypesAndCategories1568751493000 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "public"."issues_category_enum" RENAME TO "issues_category_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider', 'identity_data_provider', 'custody_provider')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum" USING "category"::"text"::"issues_category_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum_old"`);
    await queryRunner.query(`ALTER TYPE "public"."alerts_issue_types_enum" RENAME TO "alerts_issue_types_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'rate_limit_exceeded')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum"[] USING "issue_types"::"text"::"alerts_issue_types_enum"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum_old"`);
    await queryRunner.query(
      `ALTER TYPE "public"."alerts_issue_categories_enum" RENAME TO "alerts_issue_categories_enum_old"`,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_categories_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider', 'identity_data_provider', 'custody_provider')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_categories" TYPE "alerts_issue_categories_enum"[] USING "issue_categories"::"text"::"alerts_issue_categories_enum"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_categories_enum_old"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_categories_enum_old" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_categories" TYPE "alerts_issue_categories_enum_old"[] USING "issue_categories"::"text"::"alerts_issue_categories_enum_old"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_categories_enum"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_categories_enum_old" RENAME TO  "alerts_issue_categories_enum"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum_old"[] USING "issue_types"::"text"::"alerts_issue_types_enum_old"[]`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_types_enum_old" RENAME TO  "alerts_issue_types_enum"`);
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum_old" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum_old" USING "category"::"text"::"issues_category_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_category_enum_old" RENAME TO  "issues_category_enum"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`);
  }
}
