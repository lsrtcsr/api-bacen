import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCompositeAssetWalletUniqueConstraintOnAssetRegistration1583274011015 implements MigrationInterface {
  name = 'CreateCompositeAssetWalletUniqueConstraintOnAssetRegistration1583274011015';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" ADD CONSTRAINT "asset_registration_asset_wallet_unique" UNIQUE ("assetId", "walletId")`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" DROP CONSTRAINT "asset_registration_asset_wallet_unique"`,
      undefined,
    );
  }
}
