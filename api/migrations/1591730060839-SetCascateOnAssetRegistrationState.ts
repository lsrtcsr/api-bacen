import { MigrationInterface, QueryRunner } from 'typeorm';

export class SetCascateOnAssetRegistrationState1591730060839 implements MigrationInterface {
  name = 'SetCascateOnAssetRegistrationState1591730060839';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" DROP CONSTRAINT "FK_54693054283a3cecbb1315ad906"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ADD CONSTRAINT "FK_54693054283a3cecbb1315ad906" FOREIGN KEY ("assetRegistrationId") REFERENCES "asset_registrations"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" DROP CONSTRAINT "FK_54693054283a3cecbb1315ad906"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ADD CONSTRAINT "FK_54693054283a3cecbb1315ad906" FOREIGN KEY ("assetRegistrationId") REFERENCES "asset_registrations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }
}
