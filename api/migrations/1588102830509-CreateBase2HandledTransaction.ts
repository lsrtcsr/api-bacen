import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateBase2HandledTransaction1588102830509 implements MigrationInterface {
  name = 'CreateBase2HandledTransaction1588102830509';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "base2_handled_transactions_type_enum" AS ENUM('purchase', 'credit_voucher', 'withdraw', 'purchase_reversal', 'credit_voucher_reversal', 'withdraw_reversal')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "base2_handled_transactions_action_enum" AS ENUM('none', 'transaction_created', 'transaction_reversed', 'transaction_executed', 'reversal_executed', 'reversal_undone', 'amount_adjusted')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "base2_handled_transactions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "base2_handled_transactions_type_enum" NOT NULL, "action" "base2_handled_transactions_action_enum", "externalId" uuid NOT NULL, "originalAmount" numeric NOT NULL, "confirmedAmount" numeric NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "walletId" uuid, "assetId" uuid, CONSTRAINT "PK_18a1d5e85d9137eddab1bea6953" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_3678437bd1373e9f6d405e1961" ON "base2_handled_transactions" ("externalId") `,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "base2_handled_transactions" ADD CONSTRAINT "FK_66724f03a2a5b85c2204ed2cc95" FOREIGN KEY ("walletId") REFERENCES "wallets"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "base2_handled_transactions" ADD CONSTRAINT "FK_25128681fadb6687265d7761be4" FOREIGN KEY ("assetId") REFERENCES "assets"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "base2_handled_transactions" DROP CONSTRAINT "FK_25128681fadb6687265d7761be4"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "base2_handled_transactions" DROP CONSTRAINT "FK_66724f03a2a5b85c2204ed2cc95"`,
      undefined,
    );
    await queryRunner.query(`DROP INDEX "IDX_3678437bd1373e9f6d405e1961"`, undefined);
    await queryRunner.query(`DROP TABLE "base2_handled_transactions"`, undefined);
    await queryRunner.query(`DROP TYPE "base2_handled_transactions_action_enum"`, undefined);
    await queryRunner.query(`DROP TYPE "base2_handled_transactions_type_enum"`, undefined);
  }
}
