import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixPostbacksSystemUrlsDefault1581830587069 implements MigrationInterface {
  name = 'FixPostbacksSystemUrlsDefault1581830587069';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "postbacks" ALTER COLUMN "urls" SET DEFAULT '[]'`, undefined);
    await queryRunner.query(`ALTER TABLE "postbacks" ALTER COLUMN "systemUrls" SET DEFAULT '[]'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "postbacks" ALTER COLUMN "systemUrls" DROP DEFAULT`, undefined);
    await queryRunner.query(`ALTER TABLE "postbacks" ALTER COLUMN "urls" DROP DEFAULT`, undefined);
  }
}
