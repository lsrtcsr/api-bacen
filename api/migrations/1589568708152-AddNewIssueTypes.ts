import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewIssueTypes1589568708152 implements MigrationInterface {
  name = 'AddNewIssueTypes1589568708152';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'custody_provider_kyc_failed', 'custody_provider_irrecoverable_error', 'user_sent_to_manual_verification', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'period_closing_date_expired', 'invoice_closing_date_expired', 'plan_subscription_error', 'plan_subscription_not_foud', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error', 'ccs_notification_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."issues_category_enum" RENAME TO "issues_category_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other', 'transaction_refund', 'transaction_reverse', 'cdt_provider', 'identity_data_provider', 'custody_provider', 'billing', 'invoice', 'invoice_period', 'invoice_state_transition', 'period_state_transition', 'billing_plan_subscription', 'ccs_message')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum" USING "category"::"text"::"issues_category_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum_old" AS ENUM('billing', 'billing_plan_subscription', 'boleto_payment', 'cdt_provider', 'celsius_api', 'celsius_provider', 'custody_provider', 'identity_data_provider', 'invoice', 'invoice_period', 'invoice_state_transition', 'kyc_flow', 'other', 'p2p_payment', 'period_state_transition', 'postback_delivery', 'provider_postback_processing', 'stellar_transaction', 'str_message', 'str_provider', 'str_proxy', 'transaction_refund', 'transaction_reverse', 'wallet_creation_flow', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum_old" USING "category"::"text"::"issues_category_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_category_enum_old" RENAME TO  "issues_category_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_irrecoverable_error', 'custody_provider_kyc_failed', 'custody_provider_registration_failed', 'database_access', 'entry_creation_error', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'invoice_balance_settlement_failed', 'invoice_closed', 'invoice_closing_date_expired', 'invoice_closing_error', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'period_closing_date_expired', 'period_closing_error', 'plan_subscription_error', 'plan_subscription_not_foud', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'service_charge_payment_error', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'user_sent_to_manual_verification', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`, undefined);
  }
}
