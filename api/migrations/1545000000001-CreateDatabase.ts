import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDatabase1545000000001 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "consumer_postbacks_status_enum" AS ENUM('pending', 'executed', 'failed')`);
    await queryRunner.query(
      `CREATE TABLE "consumer_postbacks" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "executed_at" TIMESTAMP, "status" "consumer_postbacks_status_enum" NOT NULL DEFAULT 'pending', "payload" json NOT NULL, "response" json, "sent_at" TIMESTAMP NOT NULL, "consumerId" uuid, CONSTRAINT "PK_8f9e3378ac418b6a4a1c631c660" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "user_limits_type_enum" AS ENUM('num_cards_active', 'num_boletos_per_day', 'sum_boletos_current_day', 'sum_boletos_last_24hours', 'transaction_sum_current_day', 'transaction_sum_current_month', 'mobile_credit_sum_current_day')`,
    );
    await queryRunner.query(
      `CREATE TABLE "user_limits" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "user_limits_type_enum" NOT NULL, "value" integer NOT NULL, "valid_until" TIMESTAMP WITH TIME ZONE, "current" boolean NOT NULL DEFAULT true, "userId" uuid NOT NULL, "created_by" uuid, CONSTRAINT "REL_8bc6532d6e8727943de6373a78" UNIQUE ("created_by"), CONSTRAINT "PK_6ccf7f1003a588cc764f969bd3f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown')`,
    );
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other')`,
    );
    await queryRunner.query(`CREATE TYPE "issues_severity_enum" AS ENUM('4', '3', '2', '1')`);
    await queryRunner.query(
      `CREATE TABLE "issues" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "issues_type_enum" NOT NULL, "category" "issues_category_enum" NOT NULL, "severity" "issues_severity_enum" NOT NULL DEFAULT '2', "details" jsonb NOT NULL, "description" character varying, "reason_code" character varying, "resource_name" character varying, "resource_id" character varying NOT NULL, "parentId" uuid, CONSTRAINT "PK_9d8ecbbeff46229c700f0449257" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown')`,
    );
    await queryRunner.query(
      `CREATE TABLE "alerts" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "issue_type" "alerts_issue_type_enum" NOT NULL, "settings" jsonb NOT NULL, "created_by" uuid, CONSTRAINT "REL_779c7c43268165afb5a947e056" UNIQUE ("created_by"), CONSTRAINT "PK_60f895662df096bfcdfab7f4b96" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee')`,
    );
    await queryRunner.query(
      `CREATE TABLE "payment" ("time" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "transactionHash" character varying NOT NULL, "asset" character varying NOT NULL, "source" character varying NOT NULL, "type" "payment_type_enum" NOT NULL, "domainId" character varying NOT NULL, "destination" character varying NOT NULL, "amount" character varying NOT NULL, CONSTRAINT "PK_28a93d6c6a63e44cd058155c7f2" PRIMARY KEY ("time"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "cards" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" character varying NOT NULL, "virtual" boolean NOT NULL, "walletId" uuid, CONSTRAINT "PK_5f3269634705fdff4a9935860fc" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "products" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" character varying NOT NULL, "domainId" uuid NOT NULL, CONSTRAINT "PK_0806c755e0aca124e67c0cf6d7d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "domains_role_enum" AS ENUM('root', 'common')`);
    await queryRunner.query(
      `CREATE TABLE "domains" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" text NOT NULL, "role" "domains_role_enum" NOT NULL DEFAULT 'common', "urls" text array, "postbackUrl" text, "test" boolean NOT NULL DEFAULT true, "settings" jsonb NOT NULL DEFAULT '{}', CONSTRAINT "PK_05a6b087662191c2ea7f7ddfc4d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "wallet_states_status_enum" AS ENUM('pending', 'registered', 'ready', 'failed')`,
    );
    await queryRunner.query(
      `CREATE TABLE "wallet_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "wallet_states_status_enum" NOT NULL DEFAULT 'pending', "additionalData" jsonb NOT NULL DEFAULT '{}', "walletId" uuid NOT NULL, CONSTRAINT "PK_571242bc9e5c7ede16c0da04d8d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("walletId", "status") `,
    );
    await queryRunner.query(
      `CREATE TABLE "wallets" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "stellar" jsonb NOT NULL, "userId" uuid NOT NULL, "productId" uuid, CONSTRAINT "PK_8402e5df5a30a229380e83e4f7e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "assets_provider_enum" AS ENUM('stellar', 'cdt-visa', 'celsius-network')`);
    await queryRunner.query(
      `CREATE TABLE "assets" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "name" character varying, "code" character varying NOT NULL, "provider" "assets_provider_enum", "issuerId" uuid NOT NULL, CONSTRAINT "UQ_bff60c1b89bff7edff592d85ea4" UNIQUE ("code"), CONSTRAINT "PK_da96729a8b113377cfb6a62439c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee')`,
    );
    await queryRunner.query(
      `CREATE TABLE "payments" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "payments_type_enum" NOT NULL DEFAULT 'transfer', "amount" character varying NOT NULL, "transactionId" uuid NOT NULL, "destinationId" uuid, "cardId" uuid, "assetId" uuid, CONSTRAINT "PK_197ab7af18c93fbb0c9b28b4a59" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "transaction_states_status_enum" AS ENUM('pending', 'AUTHORIZED', 'executed', 'notified', 'failed')`,
    );
    await queryRunner.query(
      `CREATE TABLE "transaction_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "transaction_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "transactionId" uuid NOT NULL, CONSTRAINT "PK_8dd7f4b7e9f2f5437479f35167e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "transaction_state_status_unique" ON "transaction_states" ("transactionId", "status") `,
    );
    await queryRunner.query(
      `CREATE TYPE "transactions_transaction_type_enum" AS ENUM('create_account', 'change_trust', 'payment')`,
    );
    await queryRunner.query(
      `CREATE TABLE "transactions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "transaction_type" "transactions_transaction_type_enum" NOT NULL, "additional_data" jsonb NOT NULL DEFAULT '{}', "sourceId" uuid NOT NULL, "created_by" uuid, CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "transaction_postbacks_status_enum" AS ENUM('pending', 'executed', 'failed')`);
    await queryRunner.query(
      `CREATE TABLE "transaction_postbacks" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "executed_at" TIMESTAMP, "status" "transaction_postbacks_status_enum" NOT NULL DEFAULT 'pending', "payload" json NOT NULL, "response" json, "sent_at" TIMESTAMP NOT NULL, "transactionId" uuid, CONSTRAINT "PK_01e759eb273bf35998e91d8628b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "bankings_type_enum" AS ENUM('checking', 'savings')`);
    await queryRunner.query(
      `CREATE TABLE "bankings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "bankings_type_enum" NOT NULL DEFAULT 'checking', "bank" integer NOT NULL, "agency" integer NOT NULL, "agencyDigit" character varying NOT NULL, "account" integer NOT NULL, "accountDigit" character varying NOT NULL, "consumerId" uuid, CONSTRAINT "PK_502e7d899cc9b56630d470e1d98" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'pending_documents', 'pending_selfie', 'pending_deletion', 'processing_documents', 'processing_wallets', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `CREATE TABLE "consumer_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "consumer_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "consumerId" uuid NOT NULL, CONSTRAINT "PK_6efd08d4af88463c245768d7bbf" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "document_states_status_enum" AS ENUM('verified', 'manually_verified', 'deleted_by_user', 'pending_information', 'processing', 'failed_verification', 'failed_manual_verification')`,
    );
    await queryRunner.query(
      `CREATE TABLE "document_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "document_states_status_enum" NOT NULL, "additionalData" jsonb NOT NULL DEFAULT '{}', "documentId" uuid NOT NULL, CONSTRAINT "PK_23f2bde2239a5514d1d6d53c92a" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('tax_id', 'brl_identity', 'brl_individual_reg', 'brl_drivers_license', 'brl_address_statement', 'passport', 'proof_of_address', 'other')`,
    );
    await queryRunner.query(
      `CREATE TABLE "documents" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "documents_type_enum" NOT NULL, "number" character varying, "front" character varying, "back" character varying, "selfie" character varying, "expiresAt" TIMESTAMP WITH TIME ZONE, "ticketId" character varying, "reason" character varying, "consumerId" uuid, CONSTRAINT "PK_ac51aa5181ee2036f5ca482857c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("consumerId", "type") WHERE deleted_at IS NULL`,
    );
    await queryRunner.query(`CREATE TYPE "phones_type_enum" AS ENUM('home', 'work', 'mobile')`);
    await queryRunner.query(
      `CREATE TABLE "phones" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "phones_type_enum" NOT NULL DEFAULT 'mobile', "code" character varying(2) NOT NULL, "number" text NOT NULL, "extension" text, "verifiedAt" TIMESTAMP WITH TIME ZONE, "consumerId" uuid, CONSTRAINT "PK_30d7fc09a458d7a4d9471bda554" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_c4384651f6a28da298612aaa74" ON "phones" ("code", "number") `);
    await queryRunner.query(`CREATE TYPE "consumers_type_enum" AS ENUM('corporate', 'personal')`);
    await queryRunner.query(
      `CREATE TABLE "consumers" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "birthday" TIMESTAMP, "motherName" character varying, "type" "consumers_type_enum" NOT NULL DEFAULT 'personal', "companyData" jsonb, "taxId" character varying NOT NULL, "userId" uuid, CONSTRAINT "UQ_fc884f17076bcdf35603d6e24f4" UNIQUE ("taxId"), CONSTRAINT "REL_56ddc48033fde7129a47218866" UNIQUE ("userId"), CONSTRAINT "PK_9355367764efa60a8c2c27856d0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "adressess_type_enum" AS ENUM('home', 'work')`);
    await queryRunner.query(
      `CREATE TABLE "adressess" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "type" "adressess_type_enum" NOT NULL DEFAULT 'home', "country" text NOT NULL, "state" text NOT NULL, "city" text NOT NULL, "code" text NOT NULL, "neighborhood" text NOT NULL, "street" text NOT NULL, "complement" text, "number" text, "reference" text, "consumerId" uuid, CONSTRAINT "PK_cf4c53ad35c0b76fbf29cd0b046" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "settings_field_enum" AS ENUM('initialized', 'root_domain', 'maintenance', 'test_hash')`,
    );
    await queryRunner.query(
      `CREATE TABLE "settings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "field" "settings_field_enum" NOT NULL, "value" character varying NOT NULL, "created_by" uuid, CONSTRAINT "UQ_48b41fd97ac34cadbdb1507c54d" UNIQUE ("field"), CONSTRAINT "REL_27b4d13068dc326981eafbd786" UNIQUE ("created_by"), CONSTRAINT "PK_0669fe20e252eb692bf4d344975" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "user_states_status_enum" AS ENUM('pending', 'processing', 'active', 'inactive', 'failed')`,
    );
    await queryRunner.query(
      `CREATE TABLE "user_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "user_states_status_enum" NOT NULL DEFAULT 'inactive', "additionalData" jsonb NOT NULL DEFAULT '{}', "userId" uuid NOT NULL, CONSTRAINT "PK_9e52504b0c593170a342d50ff07" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "users_role_enum" AS ENUM('admin', 'audit', 'mediator', 'operator', 'consumer', 'public')`,
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "role" "users_role_enum" NOT NULL, "passwordSalt" character varying, "passwordHash" character varying, "twoFactorSecret" character varying, "require2FA" boolean NOT NULL DEFAULT false, "deleteAt" TIMESTAMP WITH TIME ZONE, "domain_user" uuid, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_b383987bfa6e6a8745085621d0" ON "users" ("email") WHERE deleted_at IS NULL`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "user_role_admin_unique" ON "users" ("role") WHERE role = 'admin'`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "user_mediator_domain_unique" ON "users" ("domain_user") WHERE role = 'mediator'`,
    );
    await queryRunner.query(`CREATE TYPE "oauth_client_platform_enum" AS ENUM('root', 'api', 'web')`);
    await queryRunner.query(`CREATE TYPE "oauth_client_status_enum" AS ENUM('active', 'inactive')`);
    await queryRunner.query(
      `CREATE TABLE "oauth_client" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "clientId" character varying NOT NULL, "clientSecret" character varying NOT NULL, "platform" "oauth_client_platform_enum" NOT NULL DEFAULT 'web', "status" "oauth_client_status_enum" NOT NULL DEFAULT 'active', "domainId" uuid, CONSTRAINT "UQ_0c623b6d56742bcfaedc40302fb" UNIQUE ("clientId"), CONSTRAINT "PK_d6e58a7e0ec3ac17a67ba7f97cd" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "oauth_refresh_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "refresh_token" character varying NOT NULL, "expires" TIMESTAMP NOT NULL, "userId" uuid, "clientId" uuid NOT NULL, CONSTRAINT "UQ_1ab166256f4fdf8b5c53165e78f" UNIQUE ("refresh_token"), CONSTRAINT "PK_662a1052b3332c19fb979ff1825" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "oauth_access_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "access_token" character varying NOT NULL, "token_type" character varying NOT NULL, "expires" TIMESTAMP WITH TIME ZONE NOT NULL, "scope" json NOT NULL, "userAgent" json NOT NULL DEFAULT '{}', "clientId" uuid, "userId" uuid, CONSTRAINT "UQ_08533039827a87b843f50485057" UNIQUE ("access_token"), CONSTRAINT "PK_e9fb4dd4f613da7cd97b8abd7e1" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "oauth_secret_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "secret_token" character varying NOT NULL, "expires" TIMESTAMP WITH TIME ZONE NOT NULL, "scope" json NOT NULL DEFAULT '[]', "resources" jsonb NOT NULL DEFAULT '[]', "userId" uuid, CONSTRAINT "UQ_8a2ffbfc3de535a9e83b4792d73" UNIQUE ("secret_token"), CONSTRAINT "PK_c52627954df5f65358b41850047" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "assets_wallets_wallets" ("assetsId" uuid NOT NULL, "walletsId" uuid NOT NULL, CONSTRAINT "PK_2ace9f285b7a5e6ec5ea32ea16b" PRIMARY KEY ("assetsId", "walletsId"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_postbacks" ADD CONSTRAINT "FK_8a902939929c79eb2ec4fe9b798" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ADD CONSTRAINT "FK_2acde9307398c20a8a2219a9cd8" FOREIGN KEY ("userId") REFERENCES "users"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_limits" ADD CONSTRAINT "FK_8bc6532d6e8727943de6373a784" FOREIGN KEY ("created_by") REFERENCES "users"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ADD CONSTRAINT "FK_640663aa767685a78100f4647bb" FOREIGN KEY ("parentId") REFERENCES "issues"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ADD CONSTRAINT "FK_779c7c43268165afb5a947e0562" FOREIGN KEY ("created_by") REFERENCES "users"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "cards" ADD CONSTRAINT "FK_87978cc5f488cf1283cdf96e287" FOREIGN KEY ("walletId") REFERENCES "wallets"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "products" ADD CONSTRAINT "FK_ef0e1dec66cd7c0a3102c74569d" FOREIGN KEY ("domainId") REFERENCES "domains"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ADD CONSTRAINT "FK_6384dd32824b7f6b83d4eb67436" FOREIGN KEY ("walletId") REFERENCES "wallets"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ADD CONSTRAINT "FK_2ecdb33f23e9a6fc392025c0b97" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT`,
    );
    await queryRunner.query(
      `ALTER TABLE "wallets" ADD CONSTRAINT "FK_53ac7cc8bf8053c108a443203de" FOREIGN KEY ("productId") REFERENCES "products"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ADD CONSTRAINT "FK_36345af6b4b5584f817f947be20" FOREIGN KEY ("issuerId") REFERENCES "wallets"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ADD CONSTRAINT "FK_c39d78e8744809ece8ca95730e2" FOREIGN KEY ("transactionId") REFERENCES "transactions"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ADD CONSTRAINT "FK_4e23f9a12cc44828e1c78572855" FOREIGN KEY ("destinationId") REFERENCES "wallets"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ADD CONSTRAINT "FK_ffa0049cf3c2abff5d4aee33a9e" FOREIGN KEY ("cardId") REFERENCES "cards"("id") ON DELETE RESTRICT`,
    );
    await queryRunner.query(
      `ALTER TABLE "payments" ADD CONSTRAINT "FK_eae0ab7f66dae5e3ebbab43845c" FOREIGN KEY ("assetId") REFERENCES "assets"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ADD CONSTRAINT "FK_7d7dfbe1e1e0268e255a5bad438" FOREIGN KEY ("transactionId") REFERENCES "transactions"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "FK_387f8a712ecc551a9e072701399" FOREIGN KEY ("sourceId") REFERENCES "wallets"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "FK_77e84561125adeccf287547f66e" FOREIGN KEY ("created_by") REFERENCES "oauth_access_token"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_postbacks" ADD CONSTRAINT "FK_350c87c4e41d49bc0072bff555c" FOREIGN KEY ("transactionId") REFERENCES "transactions"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "bankings" ADD CONSTRAINT "FK_5db72e110d592e83aa436610a37" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ADD CONSTRAINT "FK_274d9c20b27b297699b3224f308" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "document_states" ADD CONSTRAINT "FK_d7aba3fd3b29db225873427f221" FOREIGN KEY ("documentId") REFERENCES "documents"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ADD CONSTRAINT "FK_0fe20ef77e9c40b2dc8e1dfcf34" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "phones" ADD CONSTRAINT "FK_c749803d113369ce11313543868" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ADD CONSTRAINT "FK_56ddc48033fde7129a472188663" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "adressess" ADD CONSTRAINT "FK_dbff60c4464a4c22d2735a25e41" FOREIGN KEY ("consumerId") REFERENCES "consumers"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "settings" ADD CONSTRAINT "FK_27b4d13068dc326981eafbd7869" FOREIGN KEY ("created_by") REFERENCES "users"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_states" ADD CONSTRAINT "FK_26948941f9ea6a2d0a7b4e1ebf0" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_7af5ed74a2763f455349a02de6e" FOREIGN KEY ("domain_user") REFERENCES "domains"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ADD CONSTRAINT "FK_19fbf862da12497be70d88f0104" FOREIGN KEY ("domainId") REFERENCES "domains"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ADD CONSTRAINT "FK_13b7f2098d0152626ff8cdacb8f" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_refresh_token" ADD CONSTRAINT "FK_465388af7892a2e21760375ba14" FOREIGN KEY ("clientId") REFERENCES "oauth_client"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ADD CONSTRAINT "FK_0fad21239e9444f6e7a5abe2e5e" FOREIGN KEY ("clientId") REFERENCES "oauth_client"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_access_token" ADD CONSTRAINT "FK_b6c9e30cfcd4495617d44c96252" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "oauth_secret_token" ADD CONSTRAINT "FK_bef616ced15cb40f7b129609e29" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE SET NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets_wallets_wallets" ADD CONSTRAINT "FK_31f5f95e4af83edea842ea7bdfb" FOREIGN KEY ("assetsId") REFERENCES "assets"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets_wallets_wallets" ADD CONSTRAINT "FK_5dbf1be2e028e2b4cd074e3a6d7" FOREIGN KEY ("walletsId") REFERENCES "wallets"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "assets_wallets_wallets" DROP CONSTRAINT "FK_5dbf1be2e028e2b4cd074e3a6d7"`);
    await queryRunner.query(`ALTER TABLE "assets_wallets_wallets" DROP CONSTRAINT "FK_31f5f95e4af83edea842ea7bdfb"`);
    await queryRunner.query(`ALTER TABLE "oauth_secret_token" DROP CONSTRAINT "FK_bef616ced15cb40f7b129609e29"`);
    await queryRunner.query(`ALTER TABLE "oauth_access_token" DROP CONSTRAINT "FK_b6c9e30cfcd4495617d44c96252"`);
    await queryRunner.query(`ALTER TABLE "oauth_access_token" DROP CONSTRAINT "FK_0fad21239e9444f6e7a5abe2e5e"`);
    await queryRunner.query(`ALTER TABLE "oauth_refresh_token" DROP CONSTRAINT "FK_465388af7892a2e21760375ba14"`);
    await queryRunner.query(`ALTER TABLE "oauth_refresh_token" DROP CONSTRAINT "FK_13b7f2098d0152626ff8cdacb8f"`);
    await queryRunner.query(`ALTER TABLE "oauth_client" DROP CONSTRAINT "FK_19fbf862da12497be70d88f0104"`);
    await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_7af5ed74a2763f455349a02de6e"`);
    await queryRunner.query(`ALTER TABLE "user_states" DROP CONSTRAINT "FK_26948941f9ea6a2d0a7b4e1ebf0"`);
    await queryRunner.query(`ALTER TABLE "settings" DROP CONSTRAINT "FK_27b4d13068dc326981eafbd7869"`);
    await queryRunner.query(`ALTER TABLE "adressess" DROP CONSTRAINT "FK_dbff60c4464a4c22d2735a25e41"`);
    await queryRunner.query(`ALTER TABLE "consumers" DROP CONSTRAINT "FK_56ddc48033fde7129a472188663"`);
    await queryRunner.query(`ALTER TABLE "phones" DROP CONSTRAINT "FK_c749803d113369ce11313543868"`);
    await queryRunner.query(`ALTER TABLE "documents" DROP CONSTRAINT "FK_0fe20ef77e9c40b2dc8e1dfcf34"`);
    await queryRunner.query(`ALTER TABLE "document_states" DROP CONSTRAINT "FK_d7aba3fd3b29db225873427f221"`);
    await queryRunner.query(`ALTER TABLE "consumer_states" DROP CONSTRAINT "FK_274d9c20b27b297699b3224f308"`);
    await queryRunner.query(`ALTER TABLE "bankings" DROP CONSTRAINT "FK_5db72e110d592e83aa436610a37"`);
    await queryRunner.query(`ALTER TABLE "transaction_postbacks" DROP CONSTRAINT "FK_350c87c4e41d49bc0072bff555c"`);
    await queryRunner.query(`ALTER TABLE "transactions" DROP CONSTRAINT "FK_77e84561125adeccf287547f66e"`);
    await queryRunner.query(`ALTER TABLE "transactions" DROP CONSTRAINT "FK_387f8a712ecc551a9e072701399"`);
    await queryRunner.query(`ALTER TABLE "transaction_states" DROP CONSTRAINT "FK_7d7dfbe1e1e0268e255a5bad438"`);
    await queryRunner.query(`ALTER TABLE "payments" DROP CONSTRAINT "FK_eae0ab7f66dae5e3ebbab43845c"`);
    await queryRunner.query(`ALTER TABLE "payments" DROP CONSTRAINT "FK_ffa0049cf3c2abff5d4aee33a9e"`);
    await queryRunner.query(`ALTER TABLE "payments" DROP CONSTRAINT "FK_4e23f9a12cc44828e1c78572855"`);
    await queryRunner.query(`ALTER TABLE "payments" DROP CONSTRAINT "FK_c39d78e8744809ece8ca95730e2"`);
    await queryRunner.query(`ALTER TABLE "assets" DROP CONSTRAINT "FK_36345af6b4b5584f817f947be20"`);
    await queryRunner.query(`ALTER TABLE "wallets" DROP CONSTRAINT "FK_53ac7cc8bf8053c108a443203de"`);
    await queryRunner.query(`ALTER TABLE "wallets" DROP CONSTRAINT "FK_2ecdb33f23e9a6fc392025c0b97"`);
    await queryRunner.query(`ALTER TABLE "wallet_states" DROP CONSTRAINT "FK_6384dd32824b7f6b83d4eb67436"`);
    await queryRunner.query(`ALTER TABLE "products" DROP CONSTRAINT "FK_ef0e1dec66cd7c0a3102c74569d"`);
    await queryRunner.query(`ALTER TABLE "cards" DROP CONSTRAINT "FK_87978cc5f488cf1283cdf96e287"`);
    await queryRunner.query(`ALTER TABLE "alerts" DROP CONSTRAINT "FK_779c7c43268165afb5a947e0562"`);
    await queryRunner.query(`ALTER TABLE "issues" DROP CONSTRAINT "FK_640663aa767685a78100f4647bb"`);
    await queryRunner.query(`ALTER TABLE "user_limits" DROP CONSTRAINT "FK_8bc6532d6e8727943de6373a784"`);
    await queryRunner.query(`ALTER TABLE "user_limits" DROP CONSTRAINT "FK_2acde9307398c20a8a2219a9cd8"`);
    await queryRunner.query(`ALTER TABLE "consumer_postbacks" DROP CONSTRAINT "FK_8a902939929c79eb2ec4fe9b798"`);
    await queryRunner.query(`DROP TABLE "assets_wallets_wallets"`);
    await queryRunner.query(`DROP TABLE "oauth_secret_token"`);
    await queryRunner.query(`DROP TABLE "oauth_access_token"`);
    await queryRunner.query(`DROP TABLE "oauth_refresh_token"`);
    await queryRunner.query(`DROP TABLE "oauth_client"`);
    await queryRunner.query(`DROP TYPE "oauth_client_status_enum"`);
    await queryRunner.query(`DROP TYPE "oauth_client_platform_enum"`);
    await queryRunner.query(`DROP INDEX "user_mediator_domain_unique"`);
    await queryRunner.query(`DROP INDEX "user_role_admin_unique"`);
    await queryRunner.query(`DROP INDEX "IDX_b383987bfa6e6a8745085621d0"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TYPE "users_role_enum"`);
    await queryRunner.query(`DROP TABLE "user_states"`);
    await queryRunner.query(`DROP TYPE "user_states_status_enum"`);
    await queryRunner.query(`DROP TABLE "settings"`);
    await queryRunner.query(`DROP TYPE "settings_field_enum"`);
    await queryRunner.query(`DROP TABLE "adressess"`);
    await queryRunner.query(`DROP TYPE "adressess_type_enum"`);
    await queryRunner.query(`DROP TABLE "consumers"`);
    await queryRunner.query(`DROP TYPE "consumers_type_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_c4384651f6a28da298612aaa74"`);
    await queryRunner.query(`DROP TABLE "phones"`);
    await queryRunner.query(`DROP TYPE "phones_type_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`);
    await queryRunner.query(`DROP TABLE "documents"`);
    await queryRunner.query(`DROP TYPE "documents_type_enum"`);
    await queryRunner.query(`DROP TABLE "document_states"`);
    await queryRunner.query(`DROP TYPE "document_states_status_enum"`);
    await queryRunner.query(`DROP TABLE "consumer_states"`);
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`);
    await queryRunner.query(`DROP TABLE "bankings"`);
    await queryRunner.query(`DROP TYPE "bankings_type_enum"`);
    await queryRunner.query(`DROP TABLE "transaction_postbacks"`);
    await queryRunner.query(`DROP TYPE "transaction_postbacks_status_enum"`);
    await queryRunner.query(`DROP TABLE "transactions"`);
    await queryRunner.query(`DROP TYPE "transactions_transaction_type_enum"`);
    await queryRunner.query(`DROP INDEX "transaction_state_status_unique"`);
    await queryRunner.query(`DROP TABLE "transaction_states"`);
    await queryRunner.query(`DROP TYPE "transaction_states_status_enum"`);
    await queryRunner.query(`DROP TABLE "payments"`);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`);
    await queryRunner.query(`DROP TABLE "assets"`);
    await queryRunner.query(`DROP TYPE "assets_provider_enum"`);
    await queryRunner.query(`DROP TABLE "wallets"`);
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`);
    await queryRunner.query(`DROP TABLE "wallet_states"`);
    await queryRunner.query(`DROP TYPE "wallet_states_status_enum"`);
    await queryRunner.query(`DROP TABLE "domains"`);
    await queryRunner.query(`DROP TYPE "domains_role_enum"`);
    await queryRunner.query(`DROP TABLE "products"`);
    await queryRunner.query(`DROP TABLE "cards"`);
    await queryRunner.query(`DROP TABLE "payment"`);
    await queryRunner.query(`DROP TYPE "payment_type_enum"`);
    await queryRunner.query(`DROP TABLE "alerts"`);
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum"`);
    await queryRunner.query(`DROP TABLE "issues"`);
    await queryRunner.query(`DROP TYPE "issues_severity_enum"`);
    await queryRunner.query(`DROP TYPE "issues_category_enum"`);
    await queryRunner.query(`DROP TYPE "issues_type_enum"`);
    await queryRunner.query(`DROP TABLE "user_limits"`);
    await queryRunner.query(`DROP TYPE "user_limits_type_enum"`);
    await queryRunner.query(`DROP TABLE "consumer_postbacks"`);
    await queryRunner.query(`DROP TYPE "consumer_postbacks_status_enum"`);
  }
}
