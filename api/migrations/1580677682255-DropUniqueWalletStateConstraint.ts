import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropUniqueWalletStateConstraint1580677682255 implements MigrationInterface {
  name = 'DropUniqueWalletStateConstraint1580677682255';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);
    await queryRunner.query(
      `CREATE INDEX "wallet_state_status_unique" ON "wallet_states" ("walletId", "status") `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("status", "walletId") `,
      undefined,
    );
  }
}
