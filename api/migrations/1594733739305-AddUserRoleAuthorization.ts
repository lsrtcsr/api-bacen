import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddUserRoleAuthorization1594733739305 implements MigrationInterface {
  name = 'AddUserRoleAuthorization1594733739305';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(`DROP INDEX "user_role_admin_unique"`);
    await queryRunner.query(`DROP INDEX "user_mediator_domain_unique"`);
    await queryRunner.query(
      `CREATE TYPE "users_role_enum_old" AS ENUM('admin', 'audit', 'consumer', 'mediator', 'operator', 'public', 'authorization')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "role" TYPE "users_role_enum_old" USING "role"::"text"::"users_role_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "users_role_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "users_role_enum_old" RENAME TO "users_role_enum"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
        WITH latest_states AS (
            SELECT
                DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
            FROM
                consumer_states
            ORDER BY
                "consumerId", created_at DESC
        )
        SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."taxId",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
        FROM consumers
        JOIN users on users.id = consumers."userId"
        JOIN latest_states on latest_states."consumerId" = consumers.id
        WHERE users.role = 'consumer' AND users.deleted_at IS NULL
        ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_consumer_with_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  users.id as "userId",\n            consumers.id as "consumerId",\n            latest_states.status,\n            users."firstName",\n            users."lastName",\n            users.email,\n            latest_states."additionalData",\n            consumers."taxId",\n            consumers."birthday",\n            consumers."companyData",\n            users.domain_user as "domainId"\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    WHERE users.role = \'consumer\' AND users.deleted_at IS NULL\n    ORDER BY latest_states.created_at DESC',
      ],
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "user_role_admin_unique" ON "users" ("role") WHERE role = 'admin'`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "user_mediator_domain_unique" ON "users" ("domain_user") WHERE role = 'mediator'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(`DROP INDEX "user_role_admin_unique"`);
    await queryRunner.query(`DROP INDEX "user_mediator_domain_unique"`);
    await queryRunner.query(
      `CREATE TYPE "users_role_enum_old" AS ENUM('admin', 'audit', 'consumer', 'mediator', 'operator', 'public')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "role" TYPE "users_role_enum_old" USING "role"::"text"::"users_role_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "users_role_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "users_role_enum_old" RENAME TO  "users_role_enum"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
        WITH latest_states AS (
            SELECT
                DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
            FROM
                consumer_states
            ORDER BY
                "consumerId", created_at DESC
        )
        SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."taxId",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
        FROM consumers
        JOIN users on users.id = consumers."userId"
        JOIN latest_states on latest_states."consumerId" = consumers.id
        WHERE users.role = 'consumer' AND users.deleted_at IS NULL
        ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_consumer_with_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"\n      FROM\n          consumer_states\n      ORDER BY\n          "consumerId", created_at DESC\n    )\n    SELECT  users.id as "userId",\n            consumers.id as "consumerId",\n            latest_states.status,\n            users."firstName",\n            users."lastName",\n            users.email,\n            latest_states."additionalData",\n            consumers."taxId",\n            consumers."birthday",\n            consumers."companyData",\n            users.domain_user as "domainId"\n    FROM consumers\n    JOIN users on users.id = consumers."userId"\n    JOIN latest_states on latest_states."consumerId" = consumers.id\n    WHERE users.role = \'consumer\' AND users.deleted_at IS NULL\n    ORDER BY latest_states.created_at DESC',
      ],
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "user_role_admin_unique" ON "users" ("role") WHERE role = 'admin'`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "user_mediator_domain_unique" ON "users" ("domain_user") WHERE role = 'mediator'`,
    );
  }
}
