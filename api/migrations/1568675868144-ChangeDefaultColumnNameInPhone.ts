import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeDefaultColumnNameInPhone1568675868144 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "phone_default_unique"`);
    await queryRunner.query(`ALTER TABLE "phones" RENAME COLUMN "default" TO "main_phone"`);
    await queryRunner.query(`CREATE UNIQUE INDEX "phone_default_unique" ON "phones" ("consumerId", "main_phone") `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "phone_default_unique"`);
    await queryRunner.query(`ALTER TABLE "phones" RENAME COLUMN "main_phone" TO "default"`);
    await queryRunner.query(`CREATE UNIQUE INDEX "phone_default_unique" ON "phones" ("consumerId", "default") `);
  }
}
