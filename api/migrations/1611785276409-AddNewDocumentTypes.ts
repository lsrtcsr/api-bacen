import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewDocumentTypes1611785276409 implements MigrationInterface {
  name = 'AddNewDocumentTypesAndSides1611785276409';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."documents_type_enum" RENAME TO "documents_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('brl_individual_reg', 'brl_drivers_license', 'brl_passport', 'ccmei', 'ei_registration_requirement', 'eireli_incorporation_statement', 'company_bylaws', 'articles_of_association', 'brl_address_statement', 'signature', 'proof_of_life', 'other')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('articles_of_association', 'brl_address_statement', 'brl_drivers_license', 'brl_individual_reg', 'ccmei', 'company_bylaws', 'ei_registration_requirement', 'eireli_incorporation_statement', 'other', 'signature')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO  "documents_type_enum"`, undefined);
  }
}
