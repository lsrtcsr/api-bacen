import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddReportIdToAssetRegistration1612230537246 implements MigrationInterface {
  name = 'AddReportIdToAssetRegistration1612230537246';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "asset_registrations" ADD "reportId" uuid`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."asset_registration_states_status_enum" RENAME TO "asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum" AS ENUM('pending_documents', 'processing', 'approved', 'blocked', 'pending', 'registered_in_provider', 'pending_provider_approval', 'ready', 'failed', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum" USING "status"::"text"::"asset_registration_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."documents_type_enum" RENAME TO "documents_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('brl_individual_reg', 'brl_drivers_license', 'brl_passport', 'ccmei', 'ei_registration_requirement', 'eireli_incorporation_statement', 'company_bylaws', 'articles_of_association', 'brl_address_statement', 'signature', 'proof_of_life', 'other')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('articles_of_association', 'brl_address_statement', 'brl_drivers_license', 'brl_individual_reg', 'ccmei', 'company_bylaws', 'ei_registration_requirement', 'eireli_incorporation_statement', 'other', 'signature')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO  "documents_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum_old" AS ENUM('failed', 'pending', 'pending_provider_approval', 'ready', 'registered_in_provider', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ALTER COLUMN "status" TYPE "asset_registration_states_status_enum_old" USING "status"::"text"::"asset_registration_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "asset_registration_states_status_enum_old" RENAME TO  "asset_registration_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "asset_registrations" DROP COLUMN "reportId"`, undefined);
  }
}
