import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddUsernameToUser1612297677180 implements MigrationInterface {
  name = 'AddUsernameToUser1612297677180';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "users" ADD "username" character varying`, undefined);
    await queryRunner.query(`UPDATE "users" SET "username"="email"`);
    await queryRunner.query(`ALTER TABLE "users" ALTER COLUMN "username" SET NOT NULL`);
    await queryRunner.query(`CREATE INDEX "IDX_fe0bb3f6520ee0469504521e71" ON "users" ("username") `, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_fe0bb3f6520ee0469504521e71"`, undefined);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "username"`, undefined);
  }
}
