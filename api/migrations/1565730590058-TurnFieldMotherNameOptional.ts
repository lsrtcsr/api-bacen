import { MigrationInterface, QueryRunner } from 'typeorm';

export class TurnFieldMotherNameOptional1565730590058 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "motherName" DROP NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "motherName" SET NOT NULL`);
  }
}
