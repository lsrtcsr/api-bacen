import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateEnums1557930776655 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "issues" ADD "component_id" character varying`);
    await queryRunner.query(`ALTER TYPE "issues_type_enum" RENAME TO "issues_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "issues_category_enum" RENAME TO "issues_category_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum" AS ENUM('postback_delivery', 'provider_postback_processing', 'str_provider', 'str_proxy', 'str_message', 'celsius_provider', 'celsius_api', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum" USING "category"::"text"::"issues_category_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum_old"`);
    await queryRunner.query(`ALTER TABLE "issues" ALTER COLUMN "details" SET DEFAULT '{}'`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_type_enum" RENAME TO "alerts_issue_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_type" TYPE "alerts_issue_type_enum" USING "issue_type"::"text"::"alerts_issue_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "assets_provider_enum" RENAME TO "assets_provider_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum" AS ENUM('stellar', 'cdt-visa', 'celsius-network', 'str')`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum" USING "provider"::"text"::"assets_provider_enum"`,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum_old"`);
    await queryRunner.query(`ALTER TYPE "bankings_type_enum" RENAME TO "bankings_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum" AS ENUM('checking', 'savings', 'salary', 'payment', 'deposit', 'investment', 'guaranteed')`,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum" USING "type"::"text"::"bankings_type_enum"`,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`);
    await queryRunner.query(`DROP TYPE "bankings_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "consumers_type_enum" RENAME TO "consumers_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "consumers_type_enum" AS ENUM('corporate', 'financial_institution', 'personal')`,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" DROP DEFAULT`);
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "type" TYPE "consumers_type_enum" USING "type"::"text"::"consumers_type_enum"`,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" SET DEFAULT 'personal'`);
    await queryRunner.query(`DROP TYPE "consumers_type_enum_old"`);
    await queryRunner.query(`CREATE INDEX "IDX_31f5f95e4af83edea842ea7bdf" ON "assets_wallets_wallets" ("assetsId") `);
    await queryRunner.query(`CREATE INDEX "IDX_5dbf1be2e028e2b4cd074e3a6d" ON "assets_wallets_wallets" ("walletsId") `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_5dbf1be2e028e2b4cd074e3a6d"`);
    await queryRunner.query(`DROP INDEX "IDX_31f5f95e4af83edea842ea7bdf"`);
    await queryRunner.query(`CREATE TYPE "consumers_type_enum_old" AS ENUM('corporate', 'personal')`);
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" DROP DEFAULT`);
    await queryRunner.query(
      `ALTER TABLE "consumers" ALTER COLUMN "type" TYPE "consumers_type_enum_old" USING "type"::"text"::"consumers_type_enum_old"`,
    );
    await queryRunner.query(`ALTER TABLE "consumers" ALTER COLUMN "type" SET DEFAULT 'personal'`);
    await queryRunner.query(`DROP TYPE "consumers_type_enum"`);
    await queryRunner.query(`ALTER TYPE "consumers_type_enum_old" RENAME TO "consumers_type_enum"`);
    await queryRunner.query(`CREATE TYPE "bankings_type_enum_old" AS ENUM('checking', 'savings')`);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum_old" USING "type"::"text"::"bankings_type_enum_old"`,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`);
    await queryRunner.query(`DROP TYPE "bankings_type_enum"`);
    await queryRunner.query(`ALTER TYPE "bankings_type_enum_old" RENAME TO "bankings_type_enum"`);
    await queryRunner.query(`CREATE TYPE "assets_provider_enum_old" AS ENUM('stellar', 'cdt-visa', 'celsius-network')`);
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum_old" USING "provider"::"text"::"assets_provider_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum"`);
    await queryRunner.query(`ALTER TYPE "assets_provider_enum_old" RENAME TO "assets_provider_enum"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_type" TYPE "alerts_issue_type_enum_old" USING "issue_type"::"text"::"alerts_issue_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_type_enum_old" RENAME TO "alerts_issue_type_enum"`);
    await queryRunner.query(`ALTER TABLE "issues" ALTER COLUMN "details" DROP DEFAULT`);
    await queryRunner.query(
      `CREATE TYPE "issues_category_enum_old" AS ENUM('postback_delivery', 'provider_postback_processing', 'p2p_payment', 'wallet_creation_flow', 'withdrawal', 'boleto_payment', 'kyc_flow', 'stellar_transaction', 'other')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "category" TYPE "issues_category_enum_old" USING "category"::"text"::"issues_category_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_category_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_category_enum_old" RENAME TO "issues_category_enum"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO "issues_type_enum"`);
    await queryRunner.query(`ALTER TABLE "issues" DROP COLUMN "component_id"`);
  }
}
