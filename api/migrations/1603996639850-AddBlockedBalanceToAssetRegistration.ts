import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddBlockedBalanceToAssetRegistration1603996639850 implements MigrationInterface {
  name = 'AddBlockedBalanceToAssetRegistration1603996639850';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" ADD "blocked_balance" numeric NOT NULL DEFAULT 0`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" ADD CONSTRAINT "blocked_balance_is_non_negative" CHECK (blocked_balance >= 0)`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" DROP CONSTRAINT "blocked_balance_is_non_negative"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "asset_registrations" DROP COLUMN "blocked_balance"`, undefined);
  }
}
