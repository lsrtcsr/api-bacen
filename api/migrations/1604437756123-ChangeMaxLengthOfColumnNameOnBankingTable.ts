import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeMaxLengthOfColumnNameOnBankingTable1604437756123 implements MigrationInterface {
  name = 'ChangeMaxLengthOfColumnNameOnBankingTable1604437756123';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."bankings_type_enum" RENAME TO "bankings_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum" AS ENUM('checking', 'savings', 'payment', 'deposit', 'investment', 'guaranteed', 'settlement', 'instant_payment', 'salary')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum" USING "type"::"text"::"bankings_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum_old" AS ENUM('checking', 'deposit', 'guaranteed', 'investment', 'payment', 'salary', 'savings', 'settlement')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum_old" USING "type"::"text"::"bankings_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "bankings_type_enum_old" RENAME TO  "bankings_type_enum"`, undefined);
  }
}
