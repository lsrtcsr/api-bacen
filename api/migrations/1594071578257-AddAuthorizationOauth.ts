import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddAuthorizationOauth1594071578257 implements MigrationInterface {
  name = 'AddAuthorizationOauth1594071578257';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."oauth_client_platform_enum" RENAME TO "oauth_client_platform_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "oauth_client_platform_enum" AS ENUM('root', 'api', 'web', 'external-authorizer')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "oauth_client" ALTER COLUMN "platform" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "platform" TYPE "oauth_client_platform_enum" USING "platform"::"text"::"oauth_client_platform_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "oauth_client" ALTER COLUMN "platform" SET DEFAULT 'web'`, undefined);
    await queryRunner.query(`DROP TYPE "oauth_client_platform_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "oauth_client_platform_enum_old" AS ENUM('api', 'root', 'web')`, undefined);
    await queryRunner.query(`ALTER TABLE "oauth_client" ALTER COLUMN "platform" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "oauth_client" ALTER COLUMN "platform" TYPE "oauth_client_platform_enum_old" USING "platform"::"text"::"oauth_client_platform_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "oauth_client" ALTER COLUMN "platform" SET DEFAULT 'web'`, undefined);
    await queryRunner.query(`DROP TYPE "oauth_client_platform_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "oauth_client_platform_enum_old" RENAME TO  "oauth_client_platform_enum"`,
      undefined,
    );
  }
}
