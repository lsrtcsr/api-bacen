import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddStatusFieldToPaymentModel1568904409000 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payments_status_enum" AS ENUM('authorized', 'confirmed', 'reversed', 'failed')`,
    );
    await queryRunner.query(`ALTER TABLE "payments" ADD "status" "payments_status_enum" NOT NULL DEFAULT 'authorized'`);
    await queryRunner.query(`DROP INDEX "transaction_state_status_unique"`);
    await queryRunner.query(
      `ALTER TYPE "transaction_states_status_enum" RENAME TO "transaction_states_status_enum_old"`,
    );
    await queryRunner.query(
      `CREATE TYPE "transaction_states_status_enum" AS ENUM('pending', 'AUTHORIZED', 'executed', 'notified', 'reversed', 'failed')`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "status" TYPE "transaction_states_status_enum" USING "status"::"text"::"transaction_states_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "transaction_states_status_enum_old"`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "transaction_state_status_unique" ON "transaction_states" ("transactionId", "status") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "transaction_state_status_unique"`);
    await queryRunner.query(
      `CREATE TYPE "transaction_states_status_enum_old" AS ENUM('AUTHORIZED', 'executed', 'failed', 'notified', 'pending')`,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "status" TYPE "transaction_states_status_enum_old" USING "status"::"text"::"transaction_states_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "transaction_states_status_enum"`);
    await queryRunner.query(
      `ALTER TYPE "transaction_states_status_enum_old" RENAME TO "transaction_states_status_enum"`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "transaction_state_status_unique" ON "transaction_states" ("status", "transactionId") `,
    );
    await queryRunner.query(`ALTER TABLE "payments" DROP COLUMN "status"`);
    await queryRunner.query(`DROP TYPE "payments_status_enum"`);
  }
}
