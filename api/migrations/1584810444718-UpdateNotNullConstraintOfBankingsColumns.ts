import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateNotNullConstraintOfBankingsColumns1584810444718 implements MigrationInterface {
  name = 'UpdateNotNullConstraintOfBankingsColumns1584810444718';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "taxId" DROP NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "agency" DROP NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "account" DROP NOT NULL`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "account" SET NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "agency" SET NOT NULL`, undefined);
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "taxId" SET NOT NULL`, undefined);
  }
}
