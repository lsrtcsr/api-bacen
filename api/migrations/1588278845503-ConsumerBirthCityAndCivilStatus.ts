import { MigrationInterface, QueryRunner } from 'typeorm';

export class ConsumerBirthCityAndCivilStatus1588278845503 implements MigrationInterface {
  name = 'ConsumerBirthCityAndCivilStatus1588278845503';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" ADD "birth_city" character varying`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" ADD "birth_state" character varying`, undefined);
    await queryRunner.query(
      `CREATE TYPE "consumers_civilstatus_enum" AS ENUM('single', 'married', 'divorced', 'widowed', 'other', 'unknown')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "consumers" ADD "civilStatus" "consumers_civilstatus_enum" NOT NULL DEFAULT 'unknown'`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "civilStatus"`, undefined);
    await queryRunner.query(`DROP TYPE "consumers_civilstatus_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "birth_state"`, undefined);
    await queryRunner.query(`ALTER TABLE "consumers" DROP COLUMN "birth_city"`, undefined);
  }
}
