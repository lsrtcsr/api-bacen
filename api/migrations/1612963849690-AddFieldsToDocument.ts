import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddFieldsToDocument1612963849690 implements MigrationInterface {
  name = 'AddFieldsToDocument1612963849690';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" ADD "issuer" character varying`, undefined);
    await queryRunner.query(`ALTER TABLE "documents" ADD "issuerState" character varying`, undefined);
    await queryRunner.query(`ALTER TABLE "documents" ADD "identityIds" jsonb NOT NULL DEFAULT '[]'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "identityIds"`, undefined);
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "issuerState"`, undefined);
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "issuer"`, undefined);
  }
}
