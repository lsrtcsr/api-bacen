import { MigrationInterface, QueryRunner } from 'typeorm';

export class ABC1554839792926 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "issues_type_enum" RENAME TO "issues_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_type_enum" RENAME TO "alerts_issue_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_type" TYPE "alerts_issue_type_enum" USING "issue_type"::"text"::"alerts_issue_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum_old"`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'pending_phone_verification', 'pending_documents', 'pending_selfie', 'pending_deletion', 'pending_kyc_recheck', 'processing_documents', 'processing_wallets', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`);
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`);
    await queryRunner.query(`ALTER TYPE "documents_type_enum" RENAME TO "documents_type_enum_old"`);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum" AS ENUM('tax_id', 'brl_identity', 'brl_individual_reg', 'brl_drivers_license', 'brl_address_statement', 'passport', 'other')`,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum" USING "type"::"text"::"documents_type_enum"`,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum_old"`);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("consumerId", "type") WHERE deleted_at IS NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_0d37706059876fc6f4423b0092"`);
    await queryRunner.query(
      `CREATE TYPE "documents_type_enum_old" AS ENUM('tax_id', 'brl_identity', 'brl_individual_reg', 'brl_drivers_license', 'brl_address_statement', 'passport', 'proof_of_address', 'other')`,
    );
    await queryRunner.query(
      `ALTER TABLE "documents" ALTER COLUMN "type" TYPE "documents_type_enum_old" USING "type"::"text"::"documents_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "documents_type_enum"`);
    await queryRunner.query(`ALTER TYPE "documents_type_enum_old" RENAME TO "documents_type_enum"`);
    await queryRunner.query(
      `CREATE INDEX "IDX_0d37706059876fc6f4423b0092" ON "documents" ("type", "consumerId") WHERE (deleted_at IS NULL)`,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('ready', 'rejected', 'pending_documents', 'pending_selfie', 'pending_deletion', 'pending_kyc_recheck', 'processing_documents', 'processing_wallets', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`);
    await queryRunner.query(`ALTER TYPE "consumer_states_status_enum_old" RENAME TO "consumer_states_status_enum"`);
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_type_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown')`,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_type" TYPE "alerts_issue_type_enum_old" USING "issue_type"::"text"::"alerts_issue_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_type_enum"`);
    await queryRunner.query(`ALTER TYPE "alerts_issue_type_enum_old" RENAME TO "alerts_issue_type_enum"`);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('external_service_call', 'provider_service_call', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown')`,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO "issues_type_enum"`);
  }
}
