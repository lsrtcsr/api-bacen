import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateAssetRegistrations1576588015878 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "asset_registration_states_status_enum" AS ENUM('pending', 'processing', 'registered', 'failed', 'rejected')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "asset_registration_states" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "status" "asset_registration_states_status_enum", "additionalData" jsonb NOT NULL DEFAULT '{}', "assetRegistrationId" uuid, CONSTRAINT "PK_d6815d2ed2e484ae61e381fab16" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "asset_registrations" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(), "deleted_at" TIMESTAMP WITH TIME ZONE, "assetId" uuid, "walletId" uuid, CONSTRAINT "PK_0b8284df19fd2f26d9a622dd181" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" ADD CONSTRAINT "FK_54693054283a3cecbb1315ad906" FOREIGN KEY ("assetRegistrationId") REFERENCES "asset_registrations"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" ADD CONSTRAINT "FK_801fce09522062495c83a0ba3a1" FOREIGN KEY ("assetId") REFERENCES "assets"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" ADD CONSTRAINT "FK_665770c6105c5b5a1e6753c1e64" FOREIGN KEY ("walletId") REFERENCES "wallets"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );

    const assetToWalletRelation: { assetsId: string; walletsId: string }[] = await queryRunner.query(
      `SELECT assets_wallets_wallets."assetsId", assets_wallets_wallets."walletsId" FROM assets_wallets_wallets`,
    );

    for (const assetToWalletRelationItem of assetToWalletRelation) {
      const assetRegistrationInsertQuery = relation =>
        `INSERT INTO asset_registrations ("id", "created_at", "updated_at", "deleted_at", "assetId", "walletId") VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, '${relation.assetsId}', '${relation.walletsId}') RETURNING id`;

      const assetRegistrationStateInsertQuery = id =>
        `INSERT INTO asset_registration_states ("id", "created_at", "updated_at", "deleted_at", "status", "additionalData", "assetRegistrationId") VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, NULL, DEFAULT, '${id}')`;

      const insertResult = await queryRunner.query(assetRegistrationInsertQuery(assetToWalletRelationItem));
      await queryRunner.query(assetRegistrationStateInsertQuery(insertResult[0].id));
    }

    await queryRunner.query(`DROP INDEX "IDX_5dbf1be2e028e2b4cd074e3a6d"`);
    await queryRunner.query(`DROP INDEX "IDX_31f5f95e4af83edea842ea7bdf"`);
    await queryRunner.query(`ALTER TABLE "assets_wallets_wallets" DROP CONSTRAINT "FK_5dbf1be2e028e2b4cd074e3a6d7"`);
    await queryRunner.query(`ALTER TABLE "assets_wallets_wallets" DROP CONSTRAINT "FK_31f5f95e4af83edea842ea7bdfb"`);
    await queryRunner.query(`DROP TABLE assets_wallets_wallets`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "assets_wallets_wallets" ("assetsId" uuid NOT NULL, "walletsId" uuid NOT NULL, CONSTRAINT "PK_2ace9f285b7a5e6ec5ea32ea16b" PRIMARY KEY ("assetsId", "walletsId"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets_wallets_wallets" ADD CONSTRAINT "FK_31f5f95e4af83edea842ea7bdfb" FOREIGN KEY ("assetsId") REFERENCES "assets"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "assets_wallets_wallets" ADD CONSTRAINT "FK_5dbf1be2e028e2b4cd074e3a6d7" FOREIGN KEY ("walletsId") REFERENCES "wallets"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_31f5f95e4af83edea842ea7bdf" ON "assets_wallets_wallets" ("assetsId") `);
    await queryRunner.query(`CREATE INDEX "IDX_5dbf1be2e028e2b4cd074e3a6d" ON "assets_wallets_wallets" ("walletsId") `);

    const assetRegistrations = await queryRunner.query(
      `SELECT asset_registrations."assetId", asset_registrations."walletId" FROM asset_registrations`,
    );

    for (const assetRegistrationItem of assetRegistrations) {
      await queryRunner.query(
        `INSERT INTO assets_wallets_wallets ("walletsId", "assetsId") VALUES ('${assetRegistrationItem.walletId}', '${assetRegistrationItem.assetId}')`,
      );
    }

    await queryRunner.query(
      `ALTER TABLE "asset_registrations" DROP CONSTRAINT "FK_665770c6105c5b5a1e6753c1e64"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registrations" DROP CONSTRAINT "FK_801fce09522062495c83a0ba3a1"`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "asset_registration_states" DROP CONSTRAINT "FK_54693054283a3cecbb1315ad906"`,
      undefined,
    );
    await queryRunner.query(`DROP TABLE "asset_registrations"`, undefined);
    await queryRunner.query(`DROP TABLE "asset_registration_states"`, undefined);
    await queryRunner.query(`DROP TYPE "asset_registration_states_status_enum"`, undefined);
  }
}
