import { MigrationInterface, QueryRunner } from 'typeorm';

export class TransactionMetricsView1587096169216 implements MigrationInterface {
  name = 'TransactionMetricsView1587096169216';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE VIEW "view_transaction_by_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"
      FROM
      transaction_states
      ORDER BY
          "transactionId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(transactions.id) as count
    FROM transactions
    JOIN latest_states on latest_states."transactionId" = transactions.id
    WHERE transactions.deleted_at is null
    GROUP BY latest_states.status
  `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_transaction_by_states"`, undefined);
  }
}
