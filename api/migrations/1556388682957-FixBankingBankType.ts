import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixBankingBankType1556388682957 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "bank" TYPE character varying`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "bank" TYPE integer`);
  }
}
