import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddDomainIdInConsumerView1586801785420 implements MigrationInterface {
  name = 'AddDomainIdInConsumerView1586801785420';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    WHERE users.role = 'consumer' AND users.deleted_at IS NULL
    ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP VIEW "view_consumer_with_states"`, undefined);
    await queryRunner.query(
      `CREATE VIEW "view_consumer_with_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as userId,
            consumers.id as consumerId,
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."birthday",
            consumers."companyData"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    ORDER BY latest_states.created_at DESC
    `,
      undefined,
    );
  }
}
