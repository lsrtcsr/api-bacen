import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixConsumerSatusEnum1579809393349 implements MigrationInterface {
  name = 'FixConsumerSatusEnum1579809393349';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TYPE "public"."consumer_states_status_enum" RENAME TO "consumer_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum" AS ENUM('ready', 'rejected', 'manually_approved', 'pending_phone_verification', 'pending_documents', 'pending_email_verification', 'pending_deletion', 'pending_legal_acceptance', 'processing_documents', 'processing_wallets', 'processing_provider_documents', 'kyc_rechecking', 'provider_failed', 'invalid_documents', 'manual_verification', 'suspended', 'blocked', 'deleted')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum" USING "status"::"text"::"consumer_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "consumer_states_status_enum_old" AS ENUM('blocked', 'deleted', 'invalid_documents', 'kyc_rechecking', 'manual_verification', 'manually_approved', 'pending_deletion', 'pending_documents', 'pending_email_verification', 'pending_phone_verification', 'processing_documents', 'processing_provider_documents', 'processing_wallets', 'provider_failed', 'ready', 'rejected', 'suspended')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "consumer_states" ALTER COLUMN "status" TYPE "consumer_states_status_enum_old" USING "status"::"text"::"consumer_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "consumer_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "consumer_states_status_enum_old" RENAME TO  "consumer_states_status_enum"`,
      undefined,
    );
  }
}
