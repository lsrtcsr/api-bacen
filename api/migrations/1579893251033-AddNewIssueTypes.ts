import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewIssueTypes1579893251033 implements MigrationInterface {
  name = 'AddNewIssueTypes1579893251033';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'user_sent_to_manual_verification', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'plan_subscription_error', 'plan_subscription_not_foud', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."alerts_issue_types_enum" RENAME TO "alerts_issue_types_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'user_sent_to_manual_verification', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'plan_subscription_error', 'plan_subscription_not_foud', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum"[] USING "issue_types"::"text"::"alerts_issue_types_enum"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "alerts_issue_types_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'user_sent_to_manual_verification', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "alerts" ALTER COLUMN "issue_types" TYPE "alerts_issue_types_enum_old"[] USING "issue_types"::"text"::"alerts_issue_types_enum_old"[]`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "alerts_issue_types_enum_old" RENAME TO  "alerts_issue_types_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'user_sent_to_manual_verification', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`, undefined);
  }
}
