import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixAddressPhoneDefaultIndex1584983740193 implements MigrationInterface {
  name = 'FixAddressPhoneDefaultIndex1584983740193';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "phone_default_unique"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_a3191cbe48d1a8cd291bd71b7d"`, undefined);
    await queryRunner.query(`DROP INDEX "address_default_unique"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_1462dc62f7704c994bd6a399b8" ON "phones" ("consumerId", "main_phone") WHERE deleted_at IS NULL AND main_phone = true`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_d8619dce6e396bdab85fde6783" ON "phones" ("countryCode", "code", "number", "consumerId") WHERE deleted_at IS NULL`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_145a6a9750aa4dd04a4aee61cf" ON "adressess" ("consumerId", "default") WHERE deleted_at IS NULL AND "default" = true`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "IDX_145a6a9750aa4dd04a4aee61cf"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_d8619dce6e396bdab85fde6783"`, undefined);
    await queryRunner.query(`DROP INDEX "IDX_1462dc62f7704c994bd6a399b8"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "address_default_unique" ON "adressess" ("consumerId", "default") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_a3191cbe48d1a8cd291bd71b7d" ON "phones" ("code", "number", "consumerId", "countryCode") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "phone_default_unique" ON "phones" ("consumerId", "main_phone") `,
      undefined,
    );
  }
}
