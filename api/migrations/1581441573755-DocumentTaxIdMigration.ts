import { MigrationInterface, QueryRunner } from 'typeorm';

export class DocumentTaxIdMigration1581441573755 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" ADD "isActive" boolean NOT NULL DEFAULT true`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "isActive"`);
  }
}
