import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropFieldsUsedForImageStorage1564067216650 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "front"`);
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "back"`);
    await queryRunner.query(`ALTER TABLE "documents" DROP COLUMN "selfie"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "documents" ADD "selfie" character varying`);
    await queryRunner.query(`ALTER TABLE "documents" ADD "back" character varying`);
    await queryRunner.query(`ALTER TABLE "documents" ADD "front" character varying`);
  }
}
