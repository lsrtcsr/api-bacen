import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddedPaymentIdToPaymentLog1555527376743 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ADD "paymentId" character varying NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" DROP COLUMN "paymentId"`);
  }
}
