import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddAuthorizedOnProviderToTransactionStatusEnum1604702156654 implements MigrationInterface {
  name = 'AddAuthorizedOnProviderToTransactionStatusEnum1604702156654';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'wallet_pending_balance_view',
    ]);
    await queryRunner.query(`DROP VIEW "wallet_pending_balance_view"`, undefined);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_transaction_by_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_transaction_by_states"`, undefined);
    await queryRunner.query(`DROP INDEX "transaction_state_status_unique"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "public"."transaction_states_status_enum" RENAME TO "transaction_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "transaction_states_status_enum" AS ENUM('pending', 'AUTHORIZED', 'accepted', 'executed', 'notified', 'reversed', 'failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "status" TYPE "transaction_states_status_enum" USING "status"::"text"::"transaction_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "transaction_states_status_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "transaction_state_status_unique" ON "transaction_states" ("transactionId", "status") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE VIEW "view_transaction_by_states" AS 
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"
      FROM
      transaction_states
      ORDER BY
          "transactionId", created_at DESC
    )
    SELECT latest_states.status,
            COUNT(transactions.id) as count
    FROM transactions
    JOIN latest_states on latest_states."transactionId" = transactions.id
    WHERE transactions.deleted_at is null
    GROUP BY latest_states.status
  `,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_transaction_by_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"\n      FROM\n      transaction_states\n      ORDER BY\n          "transactionId", created_at DESC\n    )\n    SELECT latest_states.status,\n            COUNT(transactions.id) as count\n    FROM transactions\n    JOIN latest_states on latest_states."transactionId" = transactions.id\n    WHERE transactions.deleted_at is null\n    GROUP BY latest_states.status',
      ],
    );
    await queryRunner.query(
      `CREATE VIEW "wallet_pending_balance_view" AS 
SELECT
  wallet.id AS "walletId",
  transactions.payment_asset AS asset,
  SUM(
    CASE
      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingDebtAmount",
  SUM (
    CASE
      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingCreditAmount"
FROM wallets wallet
LEFT OUTER JOIN (
  SELECT
    transactions.id AS transaction_id,
    transactions."sourceId" AS source_id,
    payments.status AS payment_status,
    payments."destinationId" AS destination_id,
    payments.amount AS payment_amount,
    payments."assetId" AS payment_asset,
    payments.id AS payment_id
  FROM transactions
  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id
) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id
LEFT OUTER JOIN (
  SELECT
    "transactionId",
    MAX(created_at) AS created_at
  FROM transaction_states
  GROUP BY 1
) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"
LEFT OUTER JOIN transaction_states ON (
  transaction_states."transactionId"=last_transaction_state."transactionId"
  AND transaction_states.created_at=last_transaction_state.created_at
)
WHERE transaction_states.status IN ('pending', 'AUTHORIZED', 'accepted')
AND transactions.payment_status IN ('authorized', 'settled')
GROUP BY 1, 2
`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'wallet_pending_balance_view',
        'SELECT\n  wallet.id AS "walletId",\n  transactions.payment_asset AS asset,\n  SUM(\n    CASE\n      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingDebtAmount",\n  SUM (\n    CASE\n      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingCreditAmount"\nFROM wallets wallet\nLEFT OUTER JOIN (\n  SELECT\n    transactions.id AS transaction_id,\n    transactions."sourceId" AS source_id,\n    payments.status AS payment_status,\n    payments."destinationId" AS destination_id,\n    payments.amount AS payment_amount,\n    payments."assetId" AS payment_asset,\n    payments.id AS payment_id\n  FROM transactions\n  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id\n) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id\nLEFT OUTER JOIN (\n  SELECT\n    "transactionId",\n    MAX(created_at) AS created_at\n  FROM transaction_states\n  GROUP BY 1\n) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"\nLEFT OUTER JOIN transaction_states ON (\n  transaction_states."transactionId"=last_transaction_state."transactionId"\n  AND transaction_states.created_at=last_transaction_state.created_at\n)\nWHERE transaction_states.status IN (\'pending\', \'AUTHORIZED\', \'accepted\')\nAND transactions.payment_status IN (\'authorized\', \'settled\')\nGROUP BY 1, 2',
      ],
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'wallet_pending_balance_view',
    ]);
    await queryRunner.query(`DROP VIEW "wallet_pending_balance_view"`, undefined);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'view_transaction_by_states',
    ]);
    await queryRunner.query(`DROP VIEW "view_transaction_by_states"`, undefined);
    await queryRunner.query(`DROP INDEX "transaction_state_status_unique"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "transaction_states_status_enum_old" AS ENUM('AUTHORIZED', 'executed', 'failed', 'notified', 'pending', 'reversed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "transaction_states" ALTER COLUMN "status" TYPE "transaction_states_status_enum_old" USING "status"::"text"::"transaction_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "transaction_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "transaction_states_status_enum_old" RENAME TO  "transaction_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "transaction_state_status_unique" ON "transaction_states" ("status", "transactionId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE VIEW "view_transaction_by_states" AS WITH latest_states AS (
      SELECT
          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"
      FROM
      transaction_states
      ORDER BY
          "transactionId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(transactions.id) as count
    FROM transactions
    JOIN latest_states on latest_states."transactionId" = transactions.id
    WHERE transactions.deleted_at is null
    GROUP BY latest_states.status`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'view_transaction_by_states',
        'WITH latest_states AS (\n      SELECT\n          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"\n      FROM\n      transaction_states\n      ORDER BY\n          "transactionId", created_at DESC\n    )\n    SELECT  latest_states.status,\n            COUNT(transactions.id) as count\n    FROM transactions\n    JOIN latest_states on latest_states."transactionId" = transactions.id\n    WHERE transactions.deleted_at is null\n    GROUP BY latest_states.status',
      ],
    );
    await queryRunner.query(
      `CREATE VIEW "wallet_pending_balance_view" AS SELECT
  wallet.id AS "walletId",
  transactions.payment_asset AS asset,
  SUM(
    CASE
      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingDebtAmount",
  SUM (
    CASE
      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingCreditAmount"
FROM wallets wallet
LEFT OUTER JOIN (
  SELECT
    transactions.id AS transaction_id,
    transactions."sourceId" AS source_id,
    payments.status AS payment_status,
    payments."destinationId" AS destination_id,
    payments.amount AS payment_amount,
    payments."assetId" AS payment_asset,
    payments.id AS payment_id
  FROM transactions
  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id
) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id
LEFT OUTER JOIN (
  SELECT
    "transactionId",
    MAX(created_at) AS created_at
  FROM transaction_states
  GROUP BY 1
) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"
LEFT OUTER JOIN transaction_states ON (
  transaction_states."transactionId"=last_transaction_state."transactionId"
  AND transaction_states.created_at=last_transaction_state.created_at
)
WHERE transaction_states.status IN ('pending', 'AUTHORIZED')
AND transactions.payment_status IN ('authorized', 'settled')
GROUP BY 1, 2`,
      undefined,
    );
    await queryRunner.query(
      `INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`,
      [
        'VIEW',
        'public',
        'wallet_pending_balance_view',
        'SELECT\n  wallet.id AS "walletId",\n  transactions.payment_asset AS asset,\n  SUM(\n    CASE\n      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingDebtAmount",\n  SUM (\n    CASE\n      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal\n      ELSE 0\n    END\n  ) AS "pendingCreditAmount"\nFROM wallets wallet\nLEFT OUTER JOIN (\n  SELECT\n    transactions.id AS transaction_id,\n    transactions."sourceId" AS source_id,\n    payments.status AS payment_status,\n    payments."destinationId" AS destination_id,\n    payments.amount AS payment_amount,\n    payments."assetId" AS payment_asset,\n    payments.id AS payment_id\n  FROM transactions\n  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id\n) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id\nLEFT OUTER JOIN (\n  SELECT\n    "transactionId",\n    MAX(created_at) AS created_at\n  FROM transaction_states\n  GROUP BY 1\n) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"\nLEFT OUTER JOIN transaction_states ON (\n  transaction_states."transactionId"=last_transaction_state."transactionId"\n  AND transaction_states.created_at=last_transaction_state.created_at\n)\nWHERE transaction_states.status IN (\'pending\', \'AUTHORIZED\')\nAND transactions.payment_status IN (\'authorized\', \'settled\')\nGROUP BY 1, 2',
      ],
    );
  }
}
