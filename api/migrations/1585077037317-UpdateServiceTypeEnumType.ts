import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateServiceTypeEnumType1585077037317 implements MigrationInterface {
  name = 'UpdateServiceTypeEnumType1585077037317';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."services_type_enum" RENAME TO "services_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "services_type_enum" AS ENUM('physical_card_issuing', 'card_reissue', 'virtual_card_issuing', 'active_card_maintenance', 'active_account_maintenance', 'account_opening', 'account_closing', 'withdrawal', 'deposit', 'transfer', 'card_withdrawal', 'card_purchase', 'boleto_payment_cash_in', 'boleto_payment', 'boleto_emission', 'boleto_validation', 'setup', 'subscription', 'hosting', 'phone_credits_purchase', 'mass_transport_credits_purchase', 'sms', 'email', 'consumer_data_query', 'cdq_address_dataset', 'cdq_phone_dataset', 'cdq_email_dataset', 'cdq_kyc_dataset', 'cdq_person_basic_dataset', 'cdq_company_basic_dataset', 'cdq_company_partners', 'ocr', 'facematch', 'compliance_check')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "services" ALTER COLUMN "type" TYPE "services_type_enum" USING "type"::"text"::"services_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "services_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "services_type_enum_old" AS ENUM('account_closing', 'account_opening', 'active_account_maintenance', 'active_card_maintenance', 'boleto_emission', 'boleto_payment', 'boleto_validation', 'card_purchase', 'card_reissue', 'card_withdrawal', 'cdq_address_dataset', 'cdq_company_basic_dataset', 'cdq_company_partners', 'cdq_email_dataset', 'cdq_kyc_dataset', 'cdq_person_basic_dataset', 'cdq_phone_dataset', 'compliance_check', 'consumer_data_query', 'deposit', 'email', 'facematch', 'hosting', 'mass_transport_credits_purchase', 'ocr', 'phone_credits_purchase', 'physical_card_issuing', 'setup', 'sms', 'subscription', 'transfer', 'virtual_card_issuing', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "services" ALTER COLUMN "type" TYPE "services_type_enum_old" USING "type"::"text"::"services_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "services_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "services_type_enum_old" RENAME TO  "services_type_enum"`, undefined);
  }
}
