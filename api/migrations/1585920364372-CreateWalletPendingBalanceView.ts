import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateWalletPendingBalanceView1585920364372 implements MigrationInterface {
  name = 'CreateWalletPendingBalanceView1585920364372';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE VIEW "wallet_pending_balance_view" AS 
SELECT
  wallet.id AS "walletId",
  transactions.payment_asset AS asset,
  SUM(
    CASE
      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingDebtAmount",
  SUM (
    CASE
      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingCreditAmount"
FROM wallets wallet
LEFT OUTER JOIN (
  SELECT
    transactions.id AS transaction_id,
    transactions."sourceId" AS source_id,
    payments.status AS payment_status,
    payments."destinationId" AS destination_id,
    payments.amount AS payment_amount,
    payments."assetId" AS payment_asset,
    payments.id AS payment_id
  FROM transactions
  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id
) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id
LEFT OUTER JOIN (
  SELECT
    "transactionId",
    MAX(created_at) AS created_at
  FROM transaction_states
  GROUP BY 1
) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"
LEFT OUTER JOIN transaction_states ON (
  transaction_states."transactionId"=last_transaction_state."transactionId"
  AND transaction_states.created_at=last_transaction_state.created_at
)
WHERE transaction_states.status IN ('pending', 'AUTHORIZED')
AND transactions.payment_status IN ('authorized', 'settled')
GROUP BY 1, 2
`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = 'VIEW' AND "schema" = $1 AND "name" = $2`, [
      'public',
      'wallet_pending_balance_view',
    ]);
    await queryRunner.query(`DROP VIEW "wallet_pending_balance_view"`, undefined);
  }
}
