import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixAssetRegistrationNullable1580662146606 implements MigrationInterface {
  name = 'FixAssetRegistrationNullable1580662146606';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."issues_type_enum" RENAME TO "issues_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum" AS ENUM('external_service_call', 'provider_service_call', 'response_processing_failed', 'provider_unavailable', 'transfer_refund', 'withdrawal_refund_failed', 'bad_request', 'not_found', 'account_holder_info_mismatch', 'message_scheduling_cancellation', 'judicial_lock', 'system_warning', 'database_access', 'max_num_retries_reached', 'invalid_request_payload', 'action_execution_failed', 'unknown', 'channel_account_balance', 'cdt_account_balance', 'cdt_unknown_error', 'transaction_not_authorized', 'original_transaction_not_found', 'insufficient_balance_in_stellar_wallet', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'stellar_network_registration_failed', 'custody_provider_registration_failed', 'custody_provider_irrecoverable_error', 'user_sent_to_manual_verification', 'rate_limit_exceeded', 'invoice_balance_settlement_failed', 'invoice_closing_error', 'invoice_closed', 'plan_subscription_error', 'plan_subscription_not_foud', 'entry_creation_error', 'period_closing_error', 'service_charge_payment_error')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum" USING "type"::"text"::"issues_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum_old"`, undefined);

    await queryRunner.query(
      `UPDATE "asset_registration_states" SET "status" = 'pending' WHERE status IS NULL`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "asset_registration_states" ALTER COLUMN "status" SET NOT NULL`, undefined);

    await queryRunner.query(
      `ALTER TYPE "public"."assets_provider_enum" RENAME TO "assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum" AS ENUM('stellar', 'cdt-visa', 'celsius-network', 'lecca-provider', 'parati-provider', 'creditas-provider', 'bs2')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum" USING "provider"::"text"::"assets_provider_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum_old"`, undefined);
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);

    await queryRunner.query(
      `ALTER TYPE "public"."wallet_states_status_enum" RENAME TO "wallet_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "wallet_states_status_enum" AS ENUM('pending', 'registered', 'registered_in_provider', 'pending_provider_approval', 'ready', 'failed', 'rejected')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "status" TYPE "wallet_states_status_enum" USING "status"::"text"::"wallet_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" SET DEFAULT 'pending'`, undefined);
    await queryRunner.query(`DROP TYPE "wallet_states_status_enum_old"`, undefined);

    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("walletId", "status") `,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP INDEX "wallet_state_status_unique"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "wallet_states_status_enum_old" AS ENUM('failed', 'pending', 'pending_provider_approval', 'ready', 'registered', 'registered_in_provider')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "wallet_states" ALTER COLUMN "status" TYPE "wallet_states_status_enum_old" USING "status"::"text"::"wallet_states_status_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "wallet_states" ALTER COLUMN "status" SET DEFAULT 'pending'`, undefined);
    await queryRunner.query(`DROP TYPE "wallet_states_status_enum"`, undefined);
    await queryRunner.query(
      `ALTER TYPE "wallet_states_status_enum_old" RENAME TO  "wallet_states_status_enum"`,
      undefined,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "wallet_state_status_unique" ON "wallet_states" ("status", "walletId") `,
      undefined,
    );
    await queryRunner.query(
      `CREATE TYPE "assets_provider_enum_old" AS ENUM('cdt-visa', 'celsius-network', 'creditas-provider', 'lecca-provider', 'parati-provider', 'stellar')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "assets" ALTER COLUMN "provider" TYPE "assets_provider_enum_old" USING "provider"::"text"::"assets_provider_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "assets_provider_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "assets_provider_enum_old" RENAME TO  "assets_provider_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "asset_registration_states" ALTER COLUMN "status" DROP NOT NULL`, undefined);
    await queryRunner.query(
      `CREATE TYPE "issues_type_enum_old" AS ENUM('account_holder_info_mismatch', 'action_execution_failed', 'bad_request', 'cdt_account_balance', 'cdt_unknown_error', 'channel_account_balance', 'custody_provider_registration_failed', 'database_access', 'entry_creation_error', 'external_service_call', 'identity_data_provider_failure', 'identity_data_provider_unavailable', 'insufficient_balance_in_stellar_wallet', 'invalid_request_payload', 'invoice_balance_settlement_failed', 'invoice_closed', 'invoice_closing_error', 'judicial_lock', 'max_num_retries_reached', 'message_scheduling_cancellation', 'not_found', 'original_transaction_not_found', 'period_closing_error', 'plan_subscription_error', 'plan_subscription_not_foud', 'provider_service_call', 'provider_unavailable', 'rate_limit_exceeded', 'response_processing_failed', 'service_charge_payment_error', 'stellar_network_registration_failed', 'system_warning', 'transaction_not_authorized', 'transfer_refund', 'unknown', 'user_sent_to_manual_verification', 'withdrawal_refund_failed')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "issues" ALTER COLUMN "type" TYPE "issues_type_enum_old" USING "type"::"text"::"issues_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "issues_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "issues_type_enum_old" RENAME TO  "issues_type_enum"`, undefined);
  }
}
