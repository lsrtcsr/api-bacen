import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewPaymentType1574558878442 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payments_type_enum" RENAME TO "payments_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payments_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);

    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum" USING "type"::"text"::"payments_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum_old"`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."bankings_type_enum" RENAME TO "bankings_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum" AS ENUM('checking', 'savings', 'payment', 'deposit', 'investment', 'guaranteed', 'settlement')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum" USING "type"::"text"::"bankings_type_enum"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "bankings_type_enum_old" AS ENUM('checking', 'deposit', 'guaranteed', 'investment', 'payment', 'savings')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" DROP DEFAULT`, undefined);
    await queryRunner.query(
      `ALTER TABLE "bankings" ALTER COLUMN "type" TYPE "bankings_type_enum_old" USING "type"::"text"::"bankings_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "bankings" ALTER COLUMN "type" SET DEFAULT 'checking'`, undefined);
    await queryRunner.query(`DROP TYPE "bankings_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "bankings_type_enum_old" RENAME TO  "bankings_type_enum"`, undefined);

    await queryRunner.query(
      `CREATE TYPE "payments_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" DROP DEFAULT`, undefined);

    await queryRunner.query(`UPDATE "payments" SET type='phone_credit' WHERE type='mobile_recharge'`);
    await queryRunner.query(
      `ALTER TABLE "payments" ALTER COLUMN "type" TYPE "payments_type_enum_old" USING "type"::"text"::"payments_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`ALTER TABLE "payments" ALTER COLUMN "type" SET DEFAULT 'transfer'`, undefined);
    await queryRunner.query(`DROP TYPE "payments_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payments_type_enum_old" RENAME TO  "payments_type_enum"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'service_fee', 'transaction_reversal', 'transfer', 'withdrawal')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
  }
}
