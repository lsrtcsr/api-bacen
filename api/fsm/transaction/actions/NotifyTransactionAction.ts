import { TransactionStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Transaction } from '../../../models';

export class NotifyTransactionAction extends Action<Transaction, TransactionStatus> {
  from = TransactionStatus.EXECUTED;

  to = TransactionStatus.NOTIFIED;
}
