import {
  TransactionStatus,
  IssueType,
  IssueCategory,
  SeverityLevel,
  IssueDetails,
  PaymentType,
  PaymentStatus,
  CustodyFeature,
} from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { UpdateResult } from 'typeorm';
import * as Package from 'pjson';
import { Payment, Transaction, Card } from '../../../models';
import { IssueHandler, ProviderManagerService } from '../../../services';
import { ProviderUtil } from '../../../utils';

export class ReverseTransactionAction extends Action<Transaction, TransactionStatus> {
  from = TransactionStatus.AUTHORIZED;

  to = TransactionStatus.REVERSED;

  async onTransition(instance: Transaction) {
    const transaction = instance.payments
      ? instance
      : await Transaction.findOne({ where: { id: instance.id }, relations: ['payments'] });

    if (!transaction) return false;

    try {
      const provider = ProviderManagerService.getInstance();
      let settledPayments: Payment[] = [];
      settledPayments = transaction.payments.filter(
        payment => payment.isInternalPayment() && payment.asset.provider && payment.status === PaymentStatus.SETTLED,
      );

      if (settledPayments && settledPayments.length > 0) {
        const asset = settledPayments && settledPayments[0].asset;
        const p2p = provider.from(asset.provider).feature(CustodyFeature.PAYMENT);
        await ProviderUtil.throwIfFeatureNotAvailable(p2p);
        const paymentsToBeReversed = settledPayments.map(payment =>
          Payment.create({
            ...payment,
            destination: transaction.source,
            transaction: {
              id: transaction.id,
              source: payment.destination,
              type: transaction.type,
            },
          }),
        );
        const response = await p2p.payment(paymentsToBeReversed);
        let paymentsReversed: string[] = [];
        paymentsReversed = response
          .filter(PaymentResponse => PaymentResponse.status === PaymentStatus.SETTLED)
          .map(paymentReversed => paymentReversed.id);

        if (paymentsReversed && paymentsReversed.length > 0)
          await Payment.update(paymentsReversed, { status: PaymentStatus.REVERSED });
      }
    } catch (error) {
      await IssueHandler.getInstance().handle({
        type: IssueType.TRANSFER_REFUND,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `Transaction's payments refund failed`,
        details: IssueDetails.from(transaction, error),
      });

      throw error;
    }

    if (transaction.additionalData && transaction.additionalData.card_id) {
      try {
        const card = await Card.safeFindOne({ where: { id: transaction.additionalData.card_id } });
        if (!card) throw new Error(`Card with id ${transaction.additionalData.card_id} not found!`);

        const paymentUpdatePromise: Promise<UpdateResult>[] = [];
        for (const payment of transaction.payments) {
          if (payment.type !== PaymentType.SERVICE_FEE) {
            paymentUpdatePromise.push(Payment.update(payment.id, { card: card.id as any }));
          }
        }
        await Promise.all(paymentUpdatePromise);
      } catch (error) {
        await IssueHandler.getInstance().handle({
          type: IssueType.UNKNOWN,
          category: IssueCategory.PAYMENT,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: `Error associating transaction with card ID ${transaction.additionalData.card_id}`,
          details: IssueDetails.from(transaction, error),
        });
      }
    }

    return true;
  }
}
