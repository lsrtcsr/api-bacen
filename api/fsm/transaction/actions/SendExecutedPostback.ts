import { TransactionStatus, IssueType, IssueCategory, IssueDetails, SeverityLevel } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { Transaction } from '../../../models';
import Config from '../../../../config';
import { PubSubUtil } from '../../../utils';
import { IssueHandler } from '../../../services';

export class SendExecutedPostback extends Action<Transaction, TransactionStatus> {
  from = '*';

  to = TransactionStatus.EXECUTED;

  async afterTransition(instance: Transaction): Promise<void> {
    try {
      await PubSubUtil.publish({
        projectId: Config.hooksproxy.projectId,
        topicId: Config.googlecloud.topics.transactionPostback,
        message: instance.toJSON(),
      });
    } catch (error) {
      await IssueHandler.getInstance().handle({
        type: IssueType.NOT_FOUND,
        category: IssueCategory.POSTBACK_DELIVERY,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `Error sending postback to queue ${Config.googlecloud.topics.transactionPostback}`,
        details: IssueDetails.from(instance, error),
      });
    }
  }
}
