import { TransactionStatus, PaymentStatus, PaymentType } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Transaction, Payment } from '../../../models';

export class AuthorizeOnProviderAction extends Action<Transaction, TransactionStatus> {
  from = TransactionStatus.AUTHORIZED;

  to = TransactionStatus.ACCEPTED;

  async onTransition(transaction: Transaction): Promise<boolean> {
    const payments =
      transaction.payments?.length > 0 ? transaction.payments : await Payment.find({ where: { transaction } });

    return payments
      .filter(payment => payment.type !== PaymentType.SERVICE_FEE)
      .every(payment => payment.status === PaymentStatus.SETTLED);
  }
}
