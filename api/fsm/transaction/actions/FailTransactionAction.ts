import {
  TransactionStatus,
  IssueType,
  IssueCategory,
  SeverityLevel,
  IssueDetails,
  PaymentType,
  PaymentStatus,
} from '@bacen/base-sdk';
import * as Package from 'pjson';
import { Action } from 'nano-fsm';
import { UpdateResult } from 'typeorm';
import { Card, Transaction, Payment, PaymentLog } from '../../../models';
import { IssueHandler } from '../../../services';

export class FailTransactionAction extends Action<Transaction, TransactionStatus> {
  from = '*';

  to = TransactionStatus.FAILED;

  async onTransition(instance: Transaction) {
    const transaction = instance.payments
      ? instance
      : await Transaction.findOne({ where: { id: instance.id }, relations: ['payments'] });

    if (!transaction) return false;

    try {
      const settledPayments = await Payment.safeFind({
        where: {
          transaction: { id: transaction.id },
          status: PaymentStatus.SETTLED,
        },
        relations: [
          'destination',
          'destination.user',
          'destination.user.domain',
          'transaction',
          'transaction.source',
          'asset',
        ],
      });

      const deletionPromises = settledPayments.map(payment => PaymentLog.delete({ paymentId: payment.id }));
      await Promise.all(deletionPromises);
    } catch (error) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        description: 'Error deleting records on the timeseries DB',
        details: IssueDetails.from(transaction, error),
      });
    }

    if (transaction.additionalData && transaction.additionalData.card_id) {
      try {
        const card = await Card.safeFindOne({ where: { id: transaction.additionalData.card_id } });
        if (!card) throw new Error(`Card with id ${transaction.additionalData.card_id} not found!`);

        const paymentUpdatePromise: Promise<UpdateResult>[] = [];
        for (const payment of transaction.payments) {
          if (payment.type !== PaymentType.SERVICE_FEE) {
            paymentUpdatePromise.push(Payment.update(payment.id, { card: card.id as any }));
          }
        }
        await Promise.all(paymentUpdatePromise);
      } catch (error) {
        await IssueHandler.getInstance().handle({
          type: IssueType.UNKNOWN,
          category: IssueCategory.PAYMENT,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: `Error associating transaction with card ID ${transaction.additionalData.card_id}`,
          details: IssueDetails.from(transaction, error),
        });
      }
    }

    return true;
  }
}
