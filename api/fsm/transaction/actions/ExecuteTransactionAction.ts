import {
  CustodyProvider,
  IssueCategory,
  IssueDetails,
  IssueType,
  PaymentStatus,
  PaymentType,
  SeverityLevel,
  TransactionStatus,
} from '@bacen/base-sdk';
import { StellarService, StellarPaymentOptions, PaymentRecipient } from '@bacen/stellar-service';
import { Logger } from 'nano-errors';
import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { getManager, UpdateResult } from 'typeorm';
import { Card, Entry, EntryStatus, Payment, Transaction, TransactionStateItem } from '../../../models';
import { IssueHandler, SignerService } from '../../../services';
import { PaymentLog } from '../../../timescale';

export class ExecuteTransactionAction extends Action<Transaction, TransactionStatus, { transactionId: string }> {
  from = [TransactionStatus.PENDING, TransactionStatus.AUTHORIZED, TransactionStatus.ACCEPTED];

  to = TransactionStatus.EXECUTED;

  async onTransition(instance: Transaction) {
    let transaction: Transaction;

    // Ensure this state does not exist in DB to prevent double spending
    const count = await TransactionStateItem.safeCount({
      where: { transaction: { id: instance.id }, status: TransactionStatus.EXECUTED },
    });

    if (count) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `Transaction tried moving to executed more than once.`,
        details: IssueDetails.from(transaction),
      });

      return false;
    }

    if (instance.source && instance.payments && instance.payments[0].asset && instance.payments[0].destination) {
      // If the instance has everything we need, just use it
      transaction = instance;
    } else {
      // Get everything we need from the db
      transaction = await Transaction.safeFindOne({
        where: { id: instance.id },
        relations: [
          'source',
          'source.assetRegistrations',
          'source.assetRegistrations.states',
          'source.user',
          'states',
          'payments',
          'payments.destination',
          'payments.destination.assetRegistrations',
          'payments.destination.assetRegistrations.states',
          'payments.destination.user',
          'payments.asset',
          'payments.asset.issuer',
        ],
      });
    }

    // Something went very wrong, fail the transition
    // TODO: this means the id which was posted to the queue doesn't actually reference anything in the DB, shouldn't this be an exception/ create an issue?
    if (!transaction) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `Transaction not found trying to be executed.`,
        details: { resourceId: instance?.id, resourceSnapshot: transaction, resourceType: 'transaction' },
      });

      return false;
    }

    const providerName = new Set(transaction.payments.map((p) => p.asset.provider)).values().next()
      .value as CustodyProvider;

    // Since we control the source-of-truth for this provider we try executing the provider-side authorization now
    const providerAuthorizedTransactions = await transaction.authorizeOnProvider({
      providerName,
      logger: this.logger,
    });

    // Update Authorized state additionalData to include transactions authorized on the provider
    const authorizedState = transaction.getStates()[0];
    const { authorized, ...rest } = authorizedState.additionalData;
    await TransactionStateItem.update(authorizedState.id, {
      additionalData: {
        authorized: [...(authorized || []), ...providerAuthorizedTransactions],
        ...rest,
      },
    });

    // Reload transaction
    transaction = await Transaction.safeFindOne({
      where: { id: instance.id },
      relations: [
        'source',
        'source.assetRegistrations',
        'source.assetRegistrations.states',
        'source.user',
        'states',
        'payments',
        'payments.destination',
        'payments.destination.assetRegistrations',
        'payments.destination.assetRegistrations.states',
        'payments.destination.user',
        'payments.asset',
        'payments.asset.issuer',
      ],
    });

    // Find pending payments that may halt the transaction execution
    const filteredPendingPayments = transaction.payments.filter((payment) => {
      return ![PaymentStatus.SETTLED, PaymentStatus.FAILED].includes(payment.status);
    });

    // Aborts if there are payments pending resolution by the custody provider
    if (filteredPendingPayments?.length) {
      Logger.getInstance().debug('Not all payments reached an end state, so nothing to be executed', {
        transaction: instance.id,
        pendingPayments: filteredPendingPayments?.map((payment) => payment?.id || payment),
      });
      return false;
    }

    let stellarTransactionRecord;
    try {
      // Get everything we need
      const sourceWallet = transaction.source;
      const paymentsSettled = transaction.payments.filter((payment) => payment.status === PaymentStatus.SETTLED);

      if (!paymentsSettled || paymentsSettled.length === 0) {
        Logger.getInstance().debug('No payments settled, so nothing to be executed');
        return false;
      }

      // const asset = paymentsSettled && paymentsSettled[0].asset;

      // Map the payments destinations to Stellar's recipients interface
      const recipients =
        paymentsSettled &&
        paymentsSettled.map((payment: Payment) => ({
          destination: payment.destination.stellar.publicKey,
          amount: payment.amount,
          asset: payment.asset,
        }));

      let extra;

      if (transaction.additionalData && transaction.additionalData.reversal) {
        extra = {
          originalTransaction: transaction.additionalData.originalTransaction,
          reversal: true,
        };
      } else if (transaction.additionalData && transaction.additionalData.externalTransaction) {
        extra = {
          externalTransactionId: transaction.additionalData.externalTransaction.id,
          reversal: false,
        };
      }

      /*
       * Sometimes we manually send an asset registration to ready, but this does not create the trustline between
       * the wallet and the asset. Therefore just before writing to stellar we need to ensure the trustline exists
       * for all wallets involved, both source and destination wallets.
       */
      await transaction.ensureWalletTrustlines();

      const channelAccount = await SignerService.getInstance().getNextChannelAccount();

      const operations: StellarPaymentOptions[] = recipients.map((payment) => {
        return {
          secretSeed: sourceWallet.stellar.secretKey,
          channel: channelAccount,
          recipients: [
            {
              destination: payment.destination,
              amount: payment.amount,
              asset: {
                code: payment.asset.code,
                issuer: payment.asset.issuer.stellar.publicKey,
              },
            },
          ],
          extra,
        };
      });

      // Send payment using Stellar network
      stellarTransactionRecord = await StellarService.getInstance().payment(operations);

      const additionalData = transaction.additionalData
        ? { ...transaction.additionalData, hash: stellarTransactionRecord.hash }
        : { hash: stellarTransactionRecord.hash };
      await Transaction.update(transaction.id, { additionalData });
    } catch (error) {
      let recordedOnTheStellar = false;
      let message = '';
      if (stellarTransactionRecord && stellarTransactionRecord.hash) {
        recordedOnTheStellar = true;
        message = `The transaction was recorded on the stellar network (hash: ${stellarTransactionRecord.hash})`;
      }

      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        description: `Error executing transaction. ${message}`,
        details: IssueDetails.from(transaction, error),
      });

      if (!recordedOnTheStellar) throw error;
    }
    return true;
  }

  async afterTransition(instance: Transaction) {
    const transaction = instance.payments
      ? instance
      : await Transaction.findOne({ where: { id: instance.id }, relations: ['payments'] });

    if (!transaction) return false;

    let entry: Entry;
    try {
      // update invoice entry status, once service fee was payed
      const serviceFee = transaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);
      if (serviceFee) {
        entry = await Entry.createQueryBuilder('entry')
          .select()
          .where("entry.additional_data->>'chargeback' = :chargeback", { chargeback: false })
          .andWhere("entry.additional_data->'paymentProof'->>'id' = :paymentId", { paymentId: serviceFee.id })
          .getOne();

        if (entry) await Entry.update(entry.id, { status: EntryStatus.SETTLED });
      }
    } catch (error) {
      await IssueHandler.getInstance().handle({
        type: IssueType.ENTRY_CREATION_ERROR, // TODO create event processing error type
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        description: `Error attempting to update entry status`,
        details: IssueDetails.from(entry || transaction, error),
      });
    }

    if (transaction.additionalData && transaction.additionalData.card_id) {
      const cardId = transaction.additionalData.card_id;

      const card = await Card.safeFindOne({ where: { id: cardId } });
      if (!card) {
        await IssueHandler.getInstance().handle({
          type: IssueType.NOT_FOUND,
          category: IssueCategory.PAYMENT,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: `Error associating transaction with card: card ${cardId} not found`,
          details: IssueDetails.from(transaction),
        });

        return true;
      }

      try {
        await getManager().transaction(async (manager) => {
          const promises: Promise<UpdateResult>[] = [];
          for (const payment of transaction.payments) {
            if ([PaymentType.CARD, PaymentType.TRANSACTION_REVERSAL, PaymentType.WITHDRAWAL].includes(payment.type)) {
              promises.push(manager.update(Payment, payment.id, { card: cardId as any }));
            }
          }
          await Promise.all(promises);
        });
      } catch (error) {
        await IssueHandler.getInstance().handle({
          type: IssueType.UNKNOWN,
          category: IssueCategory.PAYMENT,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: `Error associating transaction with card ${cardId}`,
          details: IssueDetails.from(transaction, error),
        });
      }
    }

    try {
      const results = await Payment.safeFind({
        where: {
          transaction: { id: transaction.id },
          status: PaymentStatus.SETTLED,
        },
        relations: [
          'destination',
          'destination.user',
          'destination.user.domain',
          'transaction',
          'transaction.source',
          'asset',
        ],
      });

      const promises = results.map((result) => PaymentLog.from(result));

      // Async send the payment results to the timeseries DB
      // TODO: Set log domain ID
      await Promise.all(promises);
    } catch (error) {
      await IssueHandler.getInstance().handle({
        type: IssueType.UNKNOWN,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: `Error attempting registering payments of transaction ${transaction.id} in timeseries DB`,
        details: IssueDetails.from(transaction, error),
      });
    }

    return true;
  }
}
