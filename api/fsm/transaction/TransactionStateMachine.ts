import FSM from 'nano-fsm';
import { TransactionStatus } from '@bacen/base-sdk';
import { Transaction, TransactionStateItem, Postback } from '../../models';
import {
  ExecuteTransactionAction,
  FailTransactionAction,
  NotifyTransactionAction,
  ReverseTransactionAction,
  SendExecutedPostback,
  AuthorizeOnProviderAction,
} from './actions';

export class TransactionStateMachine extends FSM<Transaction, TransactionStatus> {
  /* Sets the machine initial state */
  initialState: TransactionStatus = TransactionStatus.PENDING;

  states = [
    /* Success states */
    TransactionStatus.PENDING,
    TransactionStatus.EXECUTED,
    TransactionStatus.NOTIFIED,
    TransactionStatus.AUTHORIZED,
    TransactionStatus.ACCEPTED,

    /* Error states */
    TransactionStatus.REVERSED,
    TransactionStatus.FAILED,
  ];

  /* Sets the machine available actions */
  actions = [
    /* Transaction flow actions */
    new ExecuteTransactionAction(),
    new NotifyTransactionAction(),
    new ReverseTransactionAction(),
    new AuthorizeOnProviderAction(),
    new SendExecutedPostback(),

    /* Error actions */
    new FailTransactionAction(),
  ];

  constructor(instance) {
    // Get current transaction state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(from: TransactionStatus | (TransactionStatus | string)[], to: TransactionStatus, data: any) {
    // Save new state in database
    const additionalData = data || {};
    if (process.env.K8S_POD_NAME) additionalData.pod = process.env.K8S_POD_NAME;
    await TransactionStateItem.insert({ status: to, additionalData, transaction: this.instance });

    this.instance = await Transaction.safeFindOne({
      where: { id: this.instance.id },
      relations: [
        'source',
        'source.user',
        'source.user.domain',
        'payments',
        'payments.destination',
        'payments.destination.user',
        'payments.destination.user.domain',
        'payments.asset',
        'banking',
        'states',
      ],
    });

    // Prepare postback instance for job queue
    const postback = await Postback.fromTransaction(this.instance, this.instance.status);
    if (postback) await postback.send();
  }
}
