import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import * as moment from 'moment';
import { SeverityLevel, IssueType, IssueCategory, IssueDetails } from '@bacen/base-sdk';
import { DeepPartial } from 'typeorm';
import { Contract, ContractStatus } from '../../../../models';
import { IssueHandler, BillingService, ContractService } from '../../../../services';

export default class ReactivateContractAction extends Action<Contract, ContractStatus> {
  from = ContractStatus.EXPIRED;

  to = ContractStatus.ACTIVE;

  async onTransition(instance: Contract, data: any) {
    let contract: Contract;

    if (instance.plan && instance.contractor) {
      contract = instance;
    } else {
      contract = await Contract.safeFindOne({
        where: { id: instance.id },
        relations: ['plan', 'contractor'],
      });
    }

    // Something went very wrong, fail the transition
    if (!contract) return false;

    try {
      const currentSubscription = await ContractService.getInstance().getCurrentContract(contract.contractor.id);
      if (currentSubscription && contract.id !== currentSubscription.id) {
        this.logger.warn(
          `The subscription ${contract.id} cannot be `.concat('reactivated because it is not the current. Skipping...'),
        );
        return false;
      }

      const now = moment();
      if (contract.validUntil && contract.validUntil.isSameOrBefore(now) && !data?.force) {
        this.logger.warn(
          `The subscription ${contract.id} of user ${contract.contractor.id} cannot be  `.concat(
            `reactivated because its expiration date has been reached. Skipping...`,
          ),
        );
        return false;
      }

      const payload: DeepPartial<Contract> = { current: true };
      if (contract.validUntil && contract.validUntil.isSameOrBefore(now)) payload.validUntil = undefined;
      await Contract.update(contract.id, payload);

      await BillingService.getInstance().createInvoice({ contract, contractor: contract.contractor, from: now });
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(instance, exception),
        description: 'got error reactivating subscription',
      });

      return false;
    }

    return true;
  }
}
