import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import * as moment from 'moment';
import { SeverityLevel, IssueType, IssueCategory, IssueDetails } from '@bacen/base-sdk';
import { Contract, ContractStatus, Invoice, InvoiceStatus } from '../../../../models';
import { IssueHandler, BillingService } from '../../../../services';

export default class CancelContractAction extends Action<Contract, ContractStatus> {
  from = ContractStatus.ACTIVE;

  to = ContractStatus.CANCELLED;

  async onTransition(instance: Contract) {
    let contract: Contract;

    if (instance.plan && instance.contractor) {
      contract = instance;
    } else {
      contract = await Contract.safeFindOne({
        where: { id: instance.id },
        relations: ['plan', 'contractor'],
      });
    }

    // Something went very wrong, fail the transition
    if (!contract) return false;

    try {
      const currentInvoice = await Invoice.getCurrent(contract.contractor);
      if (currentInvoice && currentInvoice.status === InvoiceStatus.ACTIVE) {
        await BillingService.getInstance().closeCurrentInvoice(contract.contractor, true);

        this.logger.warn(
          `The subscription ${contract.id} of user ${contract.contractor.id} `.concat(
            `cannot be cancelled while the current invoice ${currentInvoice.id} is open. Skipping...`,
          ),
        );
        return false;
      }

      await Contract.update(contract.id, { current: false, validUntil: moment() });
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(instance, exception),
        description: 'got error cancelling subscription',
      });

      return false;
    }

    return true;
  }
}
