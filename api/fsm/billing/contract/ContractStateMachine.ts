import FSM from 'nano-fsm';
import { Contract, ContractState, ContractStatus } from '../../../models';
import { CancelContractAction, ExpireContractAction, FinishContractAction, ReactivateContractAction } from './actions';

export default class ContractStateMachine extends FSM<Contract, ContractStatus> {
  /* Sets the machine initial state */
  initialState: ContractStatus = ContractStatus.ACTIVE;

  states = [ContractStatus.ACTIVE, ContractStatus.CANCELLED, ContractStatus.EXPIRED, ContractStatus.FINISHED];

  /* Sets the machine available actions */
  actions = [
    new CancelContractAction(),
    new FinishContractAction(),
    new ExpireContractAction(),
    new ReactivateContractAction(),
  ];

  constructor(instance) {
    // Get current period state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(from: ContractStatus | (ContractStatus | string)[], to: ContractStatus, data: any) {
    // Save new state in database
    await ContractState.insert({ status: to, additionalData: data, contract: this.instance });
  }
}
