import { BaseError } from 'nano-errors';
import { Action } from 'nano-fsm';
import { getManager } from 'typeorm';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory } from '@bacen/base-sdk';
import { Invoice, InvoiceStatus, EventType, Event, EventStatus } from '../../../../models';
import {
  PeriodPipelinePublisher,
  BillingService,
  IssueHandler,
  PlanService,
  ClosingEntryHandler,
} from '../../../../services';

export default class ClosingInvoiceAction extends Action<Invoice, InvoiceStatus, { invoiceId: string }> {
  from = InvoiceStatus.ACTIVE;

  to = InvoiceStatus.CLOSING;

  async onTransition(instance: Invoice) {
    let invoice: Invoice;

    if (instance.periods && instance.contractor) {
      invoice = instance;
    } else {
      invoice = await Invoice.safeFindOne({
        where: { id: instance.id },
        relations: ['periods', 'contractor'],
      });
    }

    // Something went very wrong, fail the transition
    if (!invoice) return false;

    try {
      const currentPeriod = await invoice.getCurrentPeriod();
      if (!currentPeriod) {
        throw new BaseError('There is no open period, which is requerid to create the final entries');
      }

      const serviceTypes = await PlanService.getInstance().getServiceTypes(
        invoice.contractor.id,
        EventType.INVOICE_CLOSING,
      );

      const eventHandler = ClosingEntryHandler.getInstance();

      const entryDataPromises = serviceTypes.map(type => eventHandler.handle(invoice.contractor, type));
      const entriesData = await Promise.all(entryDataPromises);

      await getManager().transaction(async transaction => {
        for (const data of entriesData) {
          const event = await Event.insertAndFind({ data }, { manager: transaction });

          // trigger the creation of entries related to the event of invoice closing
          await BillingService.getInstance().createEntry(
            event,
            [EventType.INVOICE_CLOSING],
            currentPeriod,
            transaction,
          );

          await transaction.update(Event, event.id, { status: EventStatus.PROCESSED });
        }
      });
      await PeriodPipelinePublisher.getInstance().send(currentPeriod);
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        type: IssueType.INVOICE_CLOSING_ERROR,
        category: IssueCategory.INVOICE_STATE_TRANSITION,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: {
          exception,
          resourceId: invoice.id,
          resourceType: 'Invoice',
          resourceSnapshot: invoice,
        },
        description: 'error processing invoice status transition',
      });

      return false;
    }

    return true;
  }
}
