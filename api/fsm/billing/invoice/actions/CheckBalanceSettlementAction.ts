import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory, TransactionStatus } from '@bacen/base-sdk';
import * as currency from 'currency.js';
import { Asset, Invoice, InvoiceStatus, EntryStatus, TRANSACTION_SUCCESS_STATES } from '../../../../models';
import { IssueHandler } from '../../../../services';

export default class CheckBalanceSettlementAction extends Action<Invoice, InvoiceStatus> {
  from = [InvoiceStatus.CLOSED, InvoiceStatus.PENDING_SETTLEMENT];

  to = InvoiceStatus.SETTLED;

  async onTransition(instance: Invoice) {
    let invoice: Invoice;

    if (instance.contractor && instance.paymentProof) {
      invoice = instance;
    } else {
      invoice = await Invoice.safeFindOne({
        where: { id: instance.id },
        relations: ['contractor', 'paymentProof'],
      });
    }

    // Something went very wrong, fail the transition
    if (!invoice) return false;

    try {
      const balance = currency(await invoice.balance(EntryStatus.PENDING));

      // if there is no pending value
      if (balance.value === 0) return true;

      // TODO review this assumption
      const rootAsset = await Asset.getRootAsset();

      if (
        invoice.paymentProof &&
        TRANSACTION_SUCCESS_STATES.includes(invoice.paymentProof.status) &&
        Math.abs(invoice.paymentProof.totalAmount[rootAsset.code].valueOf()) === Math.abs(balance.value)
      ) {
        return true;
      }

      return false;
    } catch (exception) {
      IssueHandler.getInstance().handle({
        type: IssueType.INVOICE_BALANCE_SETTLEMENT_FAILED,
        category: IssueCategory.INVOICE_STATE_TRANSITION,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        details: {
          exception,
          resourceId: invoice.id,
          resourceType: 'Invoice',
          resourceSnapshot: invoice,
        },
        description: 'error attempting to settle invoice balance',
      });

      return false;
    }
  }
}
