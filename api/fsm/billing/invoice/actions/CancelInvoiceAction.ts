import { Action } from 'nano-fsm';
import { Invoice, InvoiceStatus } from '../../../../models';

export default class CancelInvoiceAction extends Action<Invoice, InvoiceStatus> {
  from = '*';

  to = InvoiceStatus.CANCELED;

  // should create a new invoice?
}
