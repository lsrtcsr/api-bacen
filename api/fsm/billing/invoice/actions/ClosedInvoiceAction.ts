import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory, IssueDetails, ContractStatus } from '@bacen/base-sdk';
import * as moment from 'moment';
import { Invoice, InvoiceStatus, Event } from '../../../../models';
import { BillingService, IssueHandler, ContractService } from '../../../../services';
import { ContractStateMachine } from '../../contract';

export default class ClosedInvoiceAction extends Action<Invoice, InvoiceStatus, { invoiceId: string }> {
  from = InvoiceStatus.CLOSING;

  to = InvoiceStatus.CLOSED;

  async onTransition(instance: Invoice) {
    let invoice: Invoice;

    if (instance.periods && instance.contractor) {
      invoice = instance;
    } else {
      invoice = await Invoice.safeFindOne({
        where: { id: instance.id },
        relations: ['periods', 'contractor'],
      });
    }

    // Something went very wrong, fail the transition
    if (!invoice) return false;

    try {
      const currentPeriod = await invoice.getCurrentPeriod();
      if (currentPeriod) return false;

      const closedAt = moment();
      await Invoice.update(invoice.id, { closedAt, current: false });
    } catch (exception) {
      // TODO create a specific type and category
      await IssueHandler.getInstance().handle({
        type: IssueType.INVOICE_CLOSING_ERROR,
        category: IssueCategory.INVOICE_STATE_TRANSITION,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        description: 'error closing invoice',
        details: IssueDetails.from(invoice, exception),
      });

      return false;
    }

    return true;
  }

  async afterTransition(instance: Invoice) {
    const invoice = await Invoice.safeFindOne({
      where: { id: instance.id },
      relations: ['states', 'periods', 'contractor'],
    });

    // Something went very wrong, fail the transition
    if (!invoice) return false;

    const now = moment();
    const currentSubscription = await ContractService.getInstance().getCurrentContract(invoice.contractor.id);
    const subscriptionState = currentSubscription.getStates()[0];

    if (currentSubscription.validUntil && currentSubscription.validUntil.isSameOrBefore(now)) {
      this.logger.info(
        `The current subscription ${currentSubscription.id} of user ${currentSubscription.contractor.id} `.concat(
          'has expired or is not in a valid state. Aborted the creation of a new invoice',
        ),
      );
      const subscriptionFSM = new ContractStateMachine(currentSubscription);
      subscriptionFSM.goTo(ContractStatus.EXPIRED);
      return true;
    }
    if (
      subscriptionState.additionalData?.stateTransitionRequest &&
      subscriptionState.additionalData.stateTransitionRequest?.to !== ContractStatus.ACTIVE
    ) {
      const destinationState = subscriptionState.additionalData.stateTransitionRequest.to as ContractStatus;
      this.logger.info(
        `The current subscription ${currentSubscription.id} of user ${currentSubscription.contractor.id} `.concat(
          `was ${destinationState}. Aborted the creation of a new invoice`,
        ),
      );
      const subscriptionFSM = new ContractStateMachine(currentSubscription);
      let data;
      if (subscriptionState.additionalData.stateTransitionRequest?.force) {
        data = { force: subscriptionState.additionalData.stateTransitionRequest.force };
      }
      subscriptionFSM.goTo(destinationState, data);
      return true;
    }

    const from = invoice.closureScheduledFor.clone().add(1, 'seconds');
    await BillingService.getInstance().createInvoice({ from, contractor: invoice.contractor });

    await IssueHandler.getInstance().handle({
      type: IssueType.INVOICE_CLOSED,
      category: IssueCategory.INVOICE,
      severity: SeverityLevel.NORMAL,
      componentId: Package.name,
      description: `the invoice ${invoice.id} was closed and its balance should be settled manually`,
      details: IssueDetails.from(invoice),
    });

    const pendingEvents = await Event.getPendingEvents(invoice.contractor);
    pendingEvents.forEach(async event => {
      try {
        await event.publishToPipeline();
      } catch (exception) {
        IssueHandler.getInstance().handle({
          type: IssueType.ENTRY_CREATION_ERROR,
          category: IssueCategory.BILLING,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          details: IssueDetails.from(event, exception),
          description: 'Error publishing service consumption data to pipeline',
        });
      }
    });

    return true;
  }
}
