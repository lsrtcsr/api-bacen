import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory } from '@bacen/base-sdk';
import { Invoice, InvoiceStatus } from '../../../../models';
import { IssueHandler } from '../../../../services';

export default class TriggerBalanceSettlementAction extends Action<Invoice, InvoiceStatus> {
  from = InvoiceStatus.CLOSED;

  to = InvoiceStatus.PENDING_SETTLEMENT;

  async onTransition(instance: Invoice) {
    let invoice: Invoice;

    if (instance.contractor) {
      invoice = instance;
    } else {
      invoice = await Invoice.safeFindOne({
        where: { id: instance.id },
        relations: ['contractor'],
      });
    }

    if (!invoice) return false;

    try {
      // TODO
      // execute transfer or call boleto emittion (depending on contract settings)
      // notify consumer via alert service informing final balance
    } catch (exception) {
      IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED, // TODO create new type and category
        category: IssueCategory.OTHER,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        details: {
          exception,
          resourceId: invoice.id,
          resourceType: 'Invoice',
          resourceSnapshot: invoice,
        },
        description: 'error attempting to trigger invoice balance settlement',
      });

      return false;
    }

    return true;
  }
}
