import FSM from 'nano-fsm';
import { Invoice, InvoiceState, InvoiceStatus } from '../../../models';
import {
  ClosedInvoiceAction,
  ClosingInvoiceAction,
  CancelInvoiceAction,
  TriggerBalanceSettlementAction,
  CheckBalanceSettlementAction,
} from './actions';

export default class InvoiceStateMachine extends FSM<Invoice, InvoiceStatus> {
  /* Sets the machine initial state */
  initialState: InvoiceStatus = InvoiceStatus.ACTIVE;

  states = [
    /* Success states */
    InvoiceStatus.ACTIVE,
    InvoiceStatus.CLOSING,
    InvoiceStatus.CLOSED,
    InvoiceStatus.PENDING_SETTLEMENT,
    InvoiceStatus.SETTLED,
    /* Error states */
    InvoiceStatus.CANCELED,
  ];

  /* Sets the machine available actions */
  actions = [
    /* Invoice flow actions */
    new ClosedInvoiceAction(),
    new ClosingInvoiceAction(),
    new TriggerBalanceSettlementAction(),
    new CheckBalanceSettlementAction(),

    /* Error actions */
    new CancelInvoiceAction(),
  ];

  constructor(instance) {
    // Get current invoice state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(from: InvoiceStatus | (InvoiceStatus | string)[], to: InvoiceStatus, data: any) {
    // Save new state in database
    await InvoiceState.insert({ status: to, additionalData: data, invoice: this.instance });
  }
}
