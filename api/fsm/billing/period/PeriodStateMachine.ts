import FSM from 'nano-fsm';
import { Period, PeriodState, PeriodStatus } from '../../../models';
import { ClosedPeriodAction, CancelPeriodAction } from './actions';

export default class PeriodStateMachine extends FSM<Period, PeriodStatus> {
  /* Sets the machine initial state */
  initialState: PeriodStatus = PeriodStatus.ACTIVE;

  states = [
    /* Success states */
    PeriodStatus.ACTIVE,
    PeriodStatus.CLOSED,
    /* Error states */
    PeriodStatus.CANCELED,
  ];

  /* Sets the machine available actions */
  actions = [
    /* Period flow actions */
    new ClosedPeriodAction(),

    /* Error actions */
    new CancelPeriodAction(),
  ];

  constructor(instance) {
    // Get current period state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(from: PeriodStatus | (PeriodStatus | string)[], to: PeriodStatus, data: any) {
    // Save new state in database
    await PeriodState.insert({ status: to, additionalData: data, period: this.instance });
  }
}
