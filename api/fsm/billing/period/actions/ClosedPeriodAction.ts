import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory } from '@bacen/base-sdk';
import { getManager } from 'typeorm';
import * as moment from 'moment';
import { BaseError } from 'nano-errors';
import {
  BillingService,
  InvoicePipelinePublisher,
  IssueHandler,
  ContractService,
  PlanService,
  ClosingEntryHandler,
} from '../../../../services';
import { Period, PeriodStatus, EventType, PeriodState, InvoiceStatus, Event, EventStatus } from '../../../../models';

export default class ClosingPeriodAction extends Action<Period, PeriodStatus, { periodId: string }> {
  from = PeriodStatus.ACTIVE;

  to = PeriodStatus.CLOSED;

  async onTransition(instance: Period) {
    if (!instance) return false;

    let period: Period;

    if (instance.invoice && instance.invoice.contractor && instance.invoice.states) {
      period = instance;
    } else {
      period = await Period.safeFindOne({
        where: { id: instance.id },
        relations: ['invoice', 'invoice.contractor', 'invoice.states'],
      });
    }

    const now = moment();
    if (period.closureScheduledFor.isAfter(now)) return false;

    if (period.invoice.closureScheduledFor.isBefore(now) && period.invoice.status === InvoiceStatus.ACTIVE) {
      await InvoicePipelinePublisher.getInstance().send(period.invoice);
      return false;
    }

    try {
      if (period.status !== PeriodStatus.ACTIVE) {
        throw new BaseError('The period is not open (active), which is requerid to create the final entries');
      }

      const serviceTypes = await PlanService.getInstance().getServiceTypes(
        period.invoice.contractor.id,
        EventType.PERIOD_CLOSING,
      );

      const eventHandler = ClosingEntryHandler.getInstance();

      const entryDataPromises = serviceTypes.map(type => eventHandler.handle(period.invoice.contractor, type));
      const entriesData = await Promise.all(entryDataPromises);

      await getManager().transaction(async manager => {
        for (const data of entriesData) {
          const event = await Event.insertAndFind({ data }, { manager });

          await BillingService.getInstance().createEntry(event, [EventType.PERIOD_CLOSING], period, manager);
          await manager.update(Event, event.id, { status: EventStatus.PROCESSED });
        }
        await manager.update(Period, period.id, { closedAt: now, current: false });

        if (period.invoice.closureScheduledFor.isSameOrBefore(now)) {
          await InvoicePipelinePublisher.getInstance().send(period.invoice);
        } else {
          const contract = await ContractService.getInstance().getCurrentContract(period.invoice.contractor.id);

          const startedAt = period.closureScheduledFor.clone().add(1, 'seconds');
          const closureScheduledFor = await contract.plan.getNextPeriodClosingDate(startedAt);
          const newPeriod = await Period.insertAndFind(
            {
              startedAt,
              closureScheduledFor,
              invoice: period.invoice.id as any,
            },
            { manager },
          );

          await manager.insert(
            PeriodState,
            PeriodState.create({
              status: PeriodStatus.ACTIVE,
              period: newPeriod.id as any,
            }),
          );
        }
      });
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        type: IssueType.PERIOD_CLOSING_ERROR,
        category: IssueCategory.PERIOD_STATE_TRANSITION,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: {
          exception,
          resourceId: period.id,
          resourceType: 'Period',
          resourceSnapshot: period,
        },
        description: 'error during period calculation and closing',
      });

      return false;
    }

    return true;
  }
}
