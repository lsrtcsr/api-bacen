import { Action } from 'nano-fsm';
import { Period, PeriodStatus } from '../../../../models';

export default class CancelPeriodAction extends Action<Period, PeriodStatus> {
  from = '*';

  to = PeriodStatus.CANCELED;
}
