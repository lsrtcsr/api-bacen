import { AssetRegistrationStatus, UserRole } from '@bacen/base-sdk';
import { ConsumerPipelinePublisher, MediatorPipelinePublisher, ProviderManagerService } from '../../../services';
import { AssetRegistration } from '../../../models';
import { BaseAction } from '../../base';

export class RegisterAssetAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  public from = AssetRegistrationStatus.APPROVED;

  public to = AssetRegistrationStatus.READY;

  public async onTransition(instance: AssetRegistration): Promise<boolean> {
    const { wallet, asset } = instance;

    if (asset.provider) {
      const provider = ProviderManagerService.getInstance().from(asset.provider);
      const { status } = await provider.isApproved(wallet);

      return status;
    }

    return true;
  }

  public async afterTransition(instance: AssetRegistration): Promise<void> {
    const manager = this.context?.manager;
    const { user } = instance.wallet!;

    await instance.wallet.ensureTrustline(instance.asset, manager);

    if (user.role === UserRole.MEDIATOR) {
      await MediatorPipelinePublisher.getInstance().send(user);
    } else {
      await ConsumerPipelinePublisher.getInstance().send(user);
    }
  }
}
