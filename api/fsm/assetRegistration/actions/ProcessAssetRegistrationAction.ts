import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { AssetRegistration } from '../../../models';
import { BaseAction } from '../../base';

export class ProcessAssetRegistrationAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  from = AssetRegistrationStatus.PENDING_DOCUMENTS;

  to = AssetRegistrationStatus.PROCESSING;
}
