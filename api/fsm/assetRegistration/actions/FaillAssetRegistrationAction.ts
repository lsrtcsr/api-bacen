import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { AssetRegistration } from '../../../models';
import { BaseAction } from '../../base';

export class FailAssetRegistrationAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  from = '*';

  to = AssetRegistrationStatus.FAILED;
}
