import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { AssetRegistration } from '../../../models';
import { AssetRegistrationRepository } from '../../../repositories';
import { BaseAction } from '../../base';

export class CreateAssetReportAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  from = AssetRegistrationStatus.PENDING_REGISTRATION;

  to = AssetRegistrationStatus.PENDING_DOCUMENTS;

  public async onTransition(instance: AssetRegistration, data: { reportId: string }): Promise<boolean> {
    const manager = this.context?.manager;
    const repository = new AssetRegistrationRepository(manager);

    await repository.updateReportId(instance, data.reportId);
    return true;
  }
}
