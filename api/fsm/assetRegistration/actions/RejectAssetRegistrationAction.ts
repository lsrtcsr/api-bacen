import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { AssetRegistration } from '../../../models';
import { BaseAction } from '../../base';

export class RejectAssetRegistrationAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  from = [AssetRegistrationStatus.PENDING_REGISTRATION, AssetRegistrationStatus.PROCESSING];

  to = AssetRegistrationStatus.REJECTED;
}
