import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { ProviderManagerService } from '../../../services';
import { AssetRegistrationApprovedEvent } from '../../../events';
import { AssetRegistration } from '../../../models';
import { BaseAction } from '../../base';

export class ApproveAssetRegistrationAction extends BaseAction<AssetRegistration, AssetRegistrationStatus> {
  from = [
    AssetRegistrationStatus.PENDING_REGISTRATION,
    AssetRegistrationStatus.PENDING_DOCUMENTS,
    AssetRegistrationStatus.PROCESSING,
  ];

  to = AssetRegistrationStatus.APPROVED;

  public async onTransition(instance: AssetRegistration): Promise<boolean> {
    const { wallet, asset } = instance;
    const manager = this.context?.manager;

    if (asset.provider) {
      const provider = ProviderManagerService.getInstance().from(asset.provider);
      await wallet.registerAssetProvider({ asset, provider, reportId: instance.reportId, manager });
    }

    return true;
  }

  public async afterTransition(instance: AssetRegistration): Promise<void> {
    const event = new AssetRegistrationApprovedEvent({ assetRegistrationId: instance.id });
    await event.publish();
  }
}
