import { AssetRegistrationStatus } from '@bacen/base-sdk';
import * as Actions from './actions';
import { AssetRegistration } from '../../models';
import { AssetRegistrationRepository } from '../../repositories';
import { BaseFSM } from '../base';

export class AssetRegistrationStateMachine extends BaseFSM<AssetRegistration, AssetRegistrationStatus> {
  initialState = AssetRegistrationStatus.PENDING_REGISTRATION;

  states = [
    AssetRegistrationStatus.PENDING_REGISTRATION,
    AssetRegistrationStatus.PENDING_DOCUMENTS,
    AssetRegistrationStatus.PROCESSING,
    AssetRegistrationStatus.APPROVED,
    AssetRegistrationStatus.READY,
    AssetRegistrationStatus.REJECTED,
    AssetRegistrationStatus.BLOCKED,
    AssetRegistrationStatus.FAILED,
  ];

  actions = [
    new Actions.FailAssetRegistrationAction(),
    new Actions.RejectAssetRegistrationAction(),
    new Actions.CreateAssetReportAction(),
    new Actions.RegisterAssetAction(),
    new Actions.FailAssetRegistrationAction(),
    new Actions.ApproveAssetRegistrationAction(),
    new Actions.ProcessAssetRegistrationAction(),
  ];

  constructor(instance: AssetRegistration, context?: any) {
    // Get current transaction state from model
    super(instance, { state: instance.status }, context);
  }

  async afterTransition(
    from: AssetRegistrationStatus | (AssetRegistrationStatus | string)[],
    to: AssetRegistrationStatus,
    data: any,
  ): Promise<void> {
    const manager = this.context?.manager;
    const assetRegistrationRepository = new AssetRegistrationRepository(manager);
    const additionalData = data?.additionalData || {};
    if (process.env.K8S_POD_NAME) additionalData.pod = process.env.K8S_POD_NAME;

    await assetRegistrationRepository.appendState(this.instance, { status: to, additionalData });
  }
}
