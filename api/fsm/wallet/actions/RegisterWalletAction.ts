import { WalletStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Wallet } from '../../../models';

export class RegisterWalletAction extends Action<Wallet, WalletStatus> {
  from = WalletStatus.PENDING;

  to = WalletStatus.REGISTERED_IN_STELLAR;

  async onTransition(instance: Wallet) {
    // Register pending wallet on Stellar
    await instance.register();
    return true;
  }
}
