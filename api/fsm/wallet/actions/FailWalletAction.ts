import { WalletStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Wallet } from '../../../models';

export class FailWalletAction extends Action<Wallet, WalletStatus> {
  from = WalletStatus.PENDING;

  to = WalletStatus.FAILED;
}
