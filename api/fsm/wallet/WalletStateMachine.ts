import {
  TransactionType,
  WalletStatus,
  SeverityLevel,
  IssueType,
  IssueCategory,
  IssueDetails,
  UserRole,
} from '@bacen/base-sdk';
import * as Package from 'pjson';
import FSM from 'nano-fsm';
import { Transaction, Wallet } from '../../models';
import * as Actions from './actions';
import Config from '../../../config';
import { PubSubUtil } from '../../utils/PubSubUtil';
import { IssueHandler } from '../../services';
import { WalletRepository } from '../../repositories';

export class WalletStateMachine extends FSM<Wallet, WalletStatus> {
  /* Sets the machine initial state */
  initialState: WalletStatus = WalletStatus.PENDING;

  states = [WalletStatus.PENDING, WalletStatus.REGISTERED_IN_STELLAR, WalletStatus.FAILED];

  /* Sets the machine available actions */
  actions = [new Actions.RegisterWalletAction(), new Actions.FailWalletAction()];

  constructor(instance: Wallet) {
    // Get current transaction state from model
    super(instance, { state: instance.persistentStatus });
  }

  async afterTransition(from: WalletStatus | (WalletStatus | string)[], to: WalletStatus, data: any): Promise<void> {
    const walletRepository = new WalletRepository();
    const additionalData: any = {};

    if (to === WalletStatus.REGISTERED_IN_STELLAR) {
      const transaction = await Transaction.createQueryBuilder('transaction')
        .select()
        .where("transaction.additional_data->>'wallet_id' = :walletId", { walletId: this.instance.id })
        .andWhere('transaction.type = :type', { type: TransactionType.CREATE_ACCOUNT })
        .getOne();

      if (transaction) {
        additionalData.transaction = transaction.id;
        additionalData.hash = transaction.additionalData?.hash;
      }
    }

    // Merge additionalData with data's additionalData field
    Object.assign(additionalData, data?.additionalData);
    await walletRepository.appendState(this.instance, { status: to, additionalData });

    // todo: status ready no longer exists, remove this? what even is this ?
    // if (this.instance.status === WalletStatus.READY) {
    //   const wallet =
    //     this.instance?.user?.consumer && this.instance?.states
    //       ? this.instance
    //       : await Wallet.findOne({
    //           where: { id: this.instance.id },
    //           relations: ['user', 'user.consumer', 'user.domain', 'states'],
    //         });

    //   if (![UserRole.CONSUMER, UserRole.MEDIATOR].includes(wallet.user.role)) return;

    //   try {
    //     if (Config.googlecloud.topics.walletPostback) {
    //       await PubSubUtil.publish({
    //         projectId: Config.hooksproxy.projectId,
    //         topicId: Config.googlecloud.topics.walletPostback,
    //         message: wallet.toJSON(),
    //       });
    //     } else {
    //       this.logger.warn(`Wallet postbacks pubsub not configured, message will not be published`, {
    //         wallet: wallet.id,
    //         project: Config.hooksproxy.projectId,
    //         topic: Config.googlecloud.topics.walletPostback,
    //       });
    //     }
    //   } catch (error) {
    //     await IssueHandler.getInstance().handle({
    //       type: IssueType.NOT_FOUND,
    //       category: IssueCategory.POSTBACK_DELIVERY,
    //       severity: SeverityLevel.NORMAL,
    //       componentId: Package.name,
    //       description: `Error sending postback to queue ${Config.googlecloud.topics.walletPostback}`,
    //       details: IssueDetails.from(this.instance, error),
    //     });
    //   }
    // }
  }
}
