import FSM, { Action, FSMOptions } from 'nano-fsm';

export abstract class BaseAction<Instance, State, Payload = any> extends Action<Instance, State, Payload> {
  protected context: any | undefined;

  public setContext(context: any): void {
    this.context = context;
  }
}

export abstract class BaseFSM<Instance, State, Payload = any> extends FSM<Instance, State, Payload> {
  protected readonly context: any;

  public abstract actions: BaseAction<Instance, State, Payload>[];

  constructor(instance: Instance, options?: FSMOptions<State>, context: any = {}) {
    super(instance, options);

    this.context = context;
  }

  public goTo(state: State, data?: Payload): Promise<boolean> {
    const actions = this.pathsTo(state);

    if (actions && this.context) {
      (actions as BaseAction<Instance, State, Payload>[]).forEach((action) => {
        action.setContext(this.context);
      });

      return super.goTo(state, data);
    }
  }
}
