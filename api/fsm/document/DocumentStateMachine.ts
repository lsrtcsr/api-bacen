/*
You can visualize the state machine on: https://state-machine-cat.js.org/

"Document" {
  pending_info => "processing";
  processing => "failed_verification";
  processing => "verified";
  failed_verification => "manually_verified";
  failed_verification => "failed_manual_verification";
  * => "deleted_by_user";
};

*/
import { DocumentStatus } from '@bacen/base-sdk';
import FSM from 'nano-fsm';
import { Document, DocumentState } from '../../models';
import * as Actions from './actions';

export default class DocumentStateMachine extends FSM<Document, DocumentStatus> {
  /* Sets the machine initial state */
  initialState: DocumentStatus = DocumentStatus.PENDING_INFORMATION;

  // String enums don't have reverse mapping, so this works
  states = Object.values(DocumentStatus) as DocumentStatus[];

  /* Sets the machine available actions */
  actions = [
    /* Success actions */
    new Actions.ManuallyVerifyDocumentAction(),
    new Actions.VerifyDocumentAction(),

    /* Transition actions */
    new Actions.SendDocumentToProcessingQueueAction(),

    /* Error actions */
    new Actions.FailManualVerificationDocumentAction(),
    new Actions.FailVerificationDocumentAction(),
  ];

  constructor(instance: Document) {
    // Get current transaction state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(
    from: DocumentStatus | (DocumentStatus | string)[],
    to: DocumentStatus,
    data: any,
  ): Promise<void> {
    // Save new state in database
    await DocumentState.insert({ status: to, additionalData: data, document: this.instance });
  }
}
