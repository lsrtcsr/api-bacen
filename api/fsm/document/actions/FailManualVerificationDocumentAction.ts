import { ConsumerStatus, DocumentStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { ConsumerStateMachine } from '../../consumer';
import { Document, User } from '../../../models';

export default class FailManualVerificationDocumentAction extends Action<Document, DocumentStatus> {
  from = DocumentStatus.FAILED_VERIFICATION;

  to = DocumentStatus.FAILED_MANUAL_VERIFICATION;

  async onTransition(instance: Document) {
    let user: User = instance.consumer && instance.consumer.user;

    if (!user) {
      // Yeah, this is fugly as hell
      // TODO: Make this with QueryBuilder to be more legible
      const documentWithUser = await Document.safeFindOne({
        where: { id: instance.id },
        relations: ['consumer', 'consumer.user', 'consumer.states'],
      });
      // "Invert" the relation so we have a user with a consumer
      user = documentWithUser.consumer.user;
      delete documentWithUser.consumer.user;
      user.consumer = documentWithUser.consumer;
    }

    return true;
  }
}
