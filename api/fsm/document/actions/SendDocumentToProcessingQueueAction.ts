import { DocumentStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Document } from '../../../models';

export default class SendDocumentToProcessingQueueAction extends Action<Document, DocumentStatus> {
  from = DocumentStatus.PENDING_INFORMATION;

  to = DocumentStatus.PROCESSING;
}
