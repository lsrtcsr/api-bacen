import { DocumentStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Document } from '../../../models';

export default class VerifyDocumentAction extends Action<Document, DocumentStatus> {
  from = DocumentStatus.PROCESSING;

  to = DocumentStatus.VERIFIED;
}
