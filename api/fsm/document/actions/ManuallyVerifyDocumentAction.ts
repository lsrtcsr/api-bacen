import { DocumentStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Document } from '../../../models';

export default class ManuallyVerifyDocumentAction extends Action<Document, DocumentStatus> {
  from = '*';

  to = DocumentStatus.MANUALLY_VERIFIED;
}
