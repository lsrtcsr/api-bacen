import { ConsumerStatus, DocumentStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Document, User } from '../../../models';
import { ConsumerStateMachine } from '../../consumer';

export default class FailVerificationDocumentAction extends Action<Document, DocumentStatus> {
  from = DocumentStatus.PROCESSING;

  to = DocumentStatus.FAILED_VERIFICATION;

  async onTransition(instance: Document, data = {} as any) {
    let user: User = instance.consumer && instance.consumer.user;

    if (!user) {
      // Yeah, this is fugly as hell
      // TODO: Make this with QueryBuilder to be more legible
      const documentWithUser = await Document.safeFindOne({
        where: { id: instance.id },
        relations: ['consumer', 'consumer.user', 'consumer.states'],
      });
      // "Invert" the relation so we have a user with a consumer
      user = documentWithUser.consumer.user;
      delete documentWithUser.consumer.user;
      user.consumer = documentWithUser.consumer;
    }

    return true;
  }
}
