import { AccountType, ConsumerStatus, DocumentStatus, UnleashFlags } from '@bacen/base-sdk';
import { BaseError } from 'nano-errors';
import { Action } from 'nano-fsm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { Document, User } from '../../../models';
import { CompanyIdentityService, PersonIdentityService } from '../../../services';
import { DocumentStateMachine } from '../../document';
import { ConsumerStateMachine } from '../ConsumerStateMachine';

export class ProcessConsumerDocumentsAction extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.PROCESSING_DOCUMENTS, ConsumerStatus.MANUALLY_APPROVED, ConsumerStatus.READY];

  to = ConsumerStatus.PENDING_LEGAL_ACCEPTANCE;

  async onTransition(instance: User, data: any = {}) {
    const consumerFSM = new ConsumerStateMachine(instance);

    // Prepare feature flag context for KYC checking
    const context = { userId: instance.id, properties: { domainId: instance.domain.id } };
    const kycSkipConsumer = UnleashUtil.isEnabled(UnleashFlags.KYC_SKIP_CONSUMER, context, false);
    const forceManualKYC = UnleashUtil.isEnabled(UnleashFlags.KYC_FORCE_MANUAL, context, false);
    const skipDocumentVerification = UnleashUtil.isEnabled(UnleashFlags.SKIP_DOCUMENT_VERIFICATION, context, false);
    const kycDocumentsFailedGoToManualVerification = UnleashUtil.isEnabled(
      UnleashFlags.KYC_DOCUMENTS_FAILED_MANUAL_VERIFICATION,
      context,
      false,
    );

    if (!instance.domain || !instance.consumer) {
      throw new BaseError("User is hasn't been populated with data for processing");
    }

    // Check user identity
    const identity =
      instance.consumer.type === AccountType.PERSONAL
        ? PersonIdentityService.getInstance()
        : CompanyIdentityService.getInstance();

    // Skip document processing if feature flag is enabled
    if (kycSkipConsumer) {
      this.logger.warn('Skipping KYC data matching and report generator for feature flag is active', {
        user: instance.id,
        domain: instance.domain.id,
        kycSkipConsumer,
      });

      return true;
    }

    const currentStatus = instance.consumer.status;
    const hasBeenManuallyApproved = currentStatus === ConsumerStatus.MANUALLY_APPROVED;

    // Check if user has been manually approved
    if (hasBeenManuallyApproved) {
      return true;
    }

    const hasDocumentsNeeded = await instance.consumer.requiredDocumentsProvided();

    if (!hasDocumentsNeeded) {
      // Fail silently so we can wait for documents uploads without failing the consumer
      return false;
    }

    // Get user documents for report checking
    let documents = await Document.safeFind({
      where: { consumer: instance.consumer, isActive: true },
      relations: ['states'],
    });

    for (const document of documents) {
      if (document.status !== DocumentStatus.PROCESSING) {
        // If the document is not in processing state, skip it
        continue;
      }

      const documentFSM = new DocumentStateMachine(document);

      if (skipDocumentVerification) {
        this.logger.warn('Skipping document image report generator for feature flag is active', {
          user: instance.id,
          domain: instance.domain.id,
          kycSkipConsumer,
        });

        await documentFSM.goTo(DocumentStatus.VERIFIED, { skipDocumentVerification: true });
        continue;
      }

      try {
        // Prepare document report for KYC checking
        const { accepted, sides } = await identity.documentReport(document);

        if (accepted) {
          await documentFSM.goTo(DocumentStatus.VERIFIED);
        } else {
          await documentFSM.goTo(DocumentStatus.FAILED_VERIFICATION, {
            accepted,
            sides,
          });
        }
      } catch (exception) {
        await documentFSM.goTo(DocumentStatus.FAILED_VERIFICATION, {
          accepted: false,
          exception: {
            static: exception.response ? exception.response.statusCode : exception.statusCode,
            message: exception.message,
          },
        });
      }
    }

    if (skipDocumentVerification) return true;

    // Get updated user documents for report checking
    documents = await Document.safeFind({
      where: { consumer: instance.consumer, isActive: true },
      relations: ['states'],
    });

    // Check if all docs has passed verification
    const acceptedDocs: boolean = documents.every(document =>
      [DocumentStatus.VERIFIED, DocumentStatus.MANUALLY_VERIFIED].includes(document.status),
    );

    const { accepted, kycFileName } = await identity.report(instance);
    const additionalData = { kycFileName };

    if (accepted && acceptedDocs && !forceManualKYC) {
      return true;
    }

    if (forceManualKYC) {
      // Some docs may have failed, send to manual verification
      await consumerFSM.goTo(ConsumerStatus.MANUAL_VERIFICATION, { forceManualKYC: true, ...additionalData });

      // Interrupt transition because we called FSM directly
      return false;
    }

    // Reject user for the KYC has not beeing successfully verified
    await consumerFSM.goTo(ConsumerStatus.INVALID_DOCUMENTS, {
      accepted: !!accepted,
      acceptedDocs: !!acceptedDocs,
      ...additionalData,
    });

    // If enabled, this flag send every failed document to manual verification instead of failing to the user
    if (!acceptedDocs && kycDocumentsFailedGoToManualVerification) {
      await consumerFSM.goTo(ConsumerStatus.MANUAL_VERIFICATION, {
        accepted: !!accepted,
        acceptedDocs: !!acceptedDocs,
        kycDocumentsFailedGoToManualVerification,
      });
    }

    return false;
  }
}
