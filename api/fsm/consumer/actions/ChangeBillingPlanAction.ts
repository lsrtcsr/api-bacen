import { Action } from 'nano-fsm';
import { ConsumerStatus, IssueType, IssueCategory, SeverityLevel, IssueDetails } from '@bacen/base-sdk';
import * as Package from 'pjson';

import { IssueHandler, ContractService } from '../../../services';
import { User, ContractState, ContractStatus } from '../../../models';
import { ContractStateMachine } from '../../billing';

export class ChangeBillingPlanAction extends Action<User, ConsumerStatus> {
  public from = ConsumerStatus.READY;

  public to = ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;

  public async onTransition(instance: User, data: any): Promise<boolean> {
    const user = await User.findOne({
      where: { id: instance.id },
      relations: ['domain', 'states', 'consumer', 'consumer.states'],
    });

    try {
      const currentContract = await ContractService.getInstance().getCurrentContract(user.id);
      if (currentContract) {
        const currentState = currentContract.getStates()[0];
        await ContractState.update(currentState.id, {
          additionalData: {
            planMigrationOptions: {
              ...data,
            },
            stateTransitionRequest: {
              to: ContractStatus.FINISHED,
              at: new Date(),
              by: 'ConsumerPipeline',
            },
          } as any,
        });
        const contractFSM = new ContractStateMachine(currentContract);
        await contractFSM.goTo(ContractStatus.FINISHED);
      }
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        type: IssueType.PLAN_SUBSCRIPTION_ERROR,
        category: IssueCategory.BILLING_PLAN_SUBSCRIPTION,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(user, exception),
        description: 'error subscribing plan',
      });

      return false;
    }

    return true;
  }
}
