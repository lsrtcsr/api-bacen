import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class SuspendConsumerAction extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.REJECTED;
}
