import { ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class SendToWalletCreationConsumerAction extends Action<User, ConsumerStatus> {
  // Consumer can be sent to processing_wallet from processing_documents, manually_approved plus
  // every state that comes after processing wallets, namely processing_provider_documents,
  // pending_billing_plan_subscriptions and ready. This is done so that the consumer's wallets can be reprocessed in
  // the followint events:
  //   - A new wallet is created
  //   - A new asset with custody has to be registerd in the wallet.
  from = [
    ConsumerStatus.PROCESSING_DOCUMENTS,
    ConsumerStatus.MANUALLY_APPROVED,
    ConsumerStatus.PROCESSING_PROVIDER_DOCUMENTS,
    ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION,
    ConsumerStatus.READY,
  ];

  to = ConsumerStatus.PROCESSING_WALLETS;

  async onTransition(instance: User) {
    // Ensure user major state is correct
    if (instance.status === UserStatus.PENDING) {
      const userFSM = new UserStateMachine(instance);
      await userFSM.goTo(UserStatus.PROCESSING);
    }

    return true;
  }
}
