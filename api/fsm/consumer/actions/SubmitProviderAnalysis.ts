import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class SubmitProviderAnalysis extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PROCESSING_WALLETS;

  to = ConsumerStatus.MANUAL_VERIFICATION;
}
