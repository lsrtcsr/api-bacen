import { ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class ConsumerProviderRejectedAction extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.PROCESSING_WALLETS, ConsumerStatus.PROCESSING_PROVIDER_DOCUMENTS];

  to = ConsumerStatus.PROVIDER_REJECTED;

  async onTransition(instance: User, data?: any) {
    // Ensure user major status is correct
    const userFSM = new UserStateMachine(instance);
    if (userFSM.state != UserStatus.FAILED) await userFSM.goTo(UserStatus.FAILED);
    return true;
  }
}
