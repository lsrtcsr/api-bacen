import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class ConsumerKYCManuallyDenied extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.INVALID_DOCUMENTS, ConsumerStatus.MANUAL_VERIFICATION];

  to = ConsumerStatus.REJECTED;
}
