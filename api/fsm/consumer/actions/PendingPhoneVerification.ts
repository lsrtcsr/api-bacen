import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class PendingPhoneVerification extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.PENDING_PHONE_VERIFICATION;
}
