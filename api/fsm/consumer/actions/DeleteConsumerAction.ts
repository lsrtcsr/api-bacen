import {
  ConsumerStatus,
  UserStatus,
  IssueType,
  SeverityLevel,
  IssueCategory,
  IssueDetails,
  UnleashFlags,
} from '@bacen/base-sdk';
import { UnleashContext, UnleashUtil } from '@bacen/shared-sdk';
import { CcsService } from '@bacen/ccs-sdk';
import { Action } from 'nano-fsm';
import { UpdateResult } from 'typeorm';
import { IssueHandler } from '../../../services';
import { User, Consumer, Wallet } from '../../../models';
import { UserStateMachine } from '../../user';

export class DeleteConsumerAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PENDING_DELETION;

  to = ConsumerStatus.DELETED;

  public async onTransition(instance: User): Promise<boolean> {
    // Ensure we have all necessary information
    const user = await User.findOne(instance.id, {
      relations: ['consumer', 'wallets'],
    });
    const updatePromises: Promise<UpdateResult>[] = [];

    if (!user.deletedAt) updatePromises.push(User.softDelete(user.id));
    if (!user.consumer.deletedAt) updatePromises.push(Consumer.softDelete(user.consumer.id));

    const walletUpdatePromises = user.wallets
      .filter(wallet => !wallet.deletedAt)
      .map(wallet => Wallet.softDelete(wallet.id));
    if (walletUpdatePromises.length > 0) updatePromises.push(...walletUpdatePromises);

    await Promise.all(updatePromises);

    // Ensure user status
    if (instance.status !== UserStatus.INACTIVE) {
      const fsm = new UserStateMachine(instance);
      await fsm.goTo(UserStatus.INACTIVE);
    }

    return true;
  }

  public async afterTransition(instance: User) {
    const context: UnleashContext = { userId: instance.id, properties: { domainId: instance.domainId } };
    const shouldNotify = UnleashUtil.isEnabled(UnleashFlags.ENABLE_CCS_NOTIFICATION, context, false);

    if (shouldNotify) {
      const ccsUserService = CcsService.getInstance().user;

      try {
        await instance.reload();
        await ccsUserService.notifyUser(instance);
        this.logger.debug('Notified CCS of user exclusion', {
          userId: instance.id,
        });
      } catch (err) {
        const readableError = err.toJSON ? err.toJSON() : err.message ? err.message : err;

        await IssueHandler.getInstance().handle({
          type: IssueType.CCS_NOTIFICATION_FAILED,
          severity: SeverityLevel.NORMAL,
          category: IssueCategory.CCS_MESSAGE,
          componentId: ' -api',
          details: IssueDetails.from(instance, err),
          description: readableError,
        });

        this.logger.error('Failed to notify CCS update', {
          user: instance.id,
          error: readableError,
        });
      }
    }
  }
}
