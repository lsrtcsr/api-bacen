import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class SendToDocumentProcessingAfterRecheckAction extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.KYC_RECHECKING, ConsumerStatus.BLOCKED];

  to = ConsumerStatus.PROCESSING_DOCUMENTS;
}
