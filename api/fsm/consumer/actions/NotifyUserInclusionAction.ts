import {
  ConsumerStatus,
  IssueType,
  SeverityLevel,
  IssueCategory,
  IssueDetails,
  UnleashFlags,
} from '@bacen/base-sdk';
import { UnleashContext, UnleashUtil } from '@bacen/shared-sdk';
import { Action } from 'nano-fsm';
import { CcsService } from '@bacen/ccs-sdk';
import { User } from '../../../models';
import { IssueHandler } from '../../../services';

export class NotifyUserInclusionAction extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.READY;

  public async afterTransition(instance: User) {
    const context: UnleashContext = { userId: instance.id, properties: { domainId: instance.domainId } };
    const shouldNotify = UnleashUtil.isEnabled(UnleashFlags.ENABLE_CCS_NOTIFICATION, context, false);

    if (shouldNotify) {
      const ccsUserWervice = CcsService.getInstance().user;

      try {
        await ccsUserWervice.notifyUser(instance);
        this.logger.debug('Notified CCS of user inclusion', {
          userId: instance.id,
        });
      } catch (err) {
        const readableError = err.toJSON ? err.toJSON() : err.message ? err.message : err;

        await IssueHandler.getInstance().handle({
          type: IssueType.CCS_NOTIFICATION_FAILED,
          severity: SeverityLevel.NORMAL,
          category: IssueCategory.CCS_MESSAGE,
          componentId: ' -api',
          details: IssueDetails.from(instance, err),
          description: readableError,
        });

        this.logger.error('Failed to notify CCS update', {
          user: instance.id,
          error: readableError,
        });
      }
    }
  }
}
