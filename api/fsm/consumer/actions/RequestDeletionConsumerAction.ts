import { Action, TransitionData } from 'nano-fsm';
import { ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { User, Wallet } from '../../../models';
import { UserStateMachine } from '../..';

export interface RequestConsumerDeletionPayload {
  reason: ConsumerDeleteReason;
  deleteBy?: Date;
}

export enum ConsumerDeleteReason {
  /** User requested account deletion */
  REQUESTED_BY_USER = 'requested_by_user',

  /**
   * If a consumer is in a pending state for more than 7 days it is considered abandoned
   * and marked for deletion.
   */
  ABANDONED_REGISTRATION = 'abandoned_registration',
}

export class RequestDeletionConsumerAction extends Action<User, ConsumerStatus, RequestConsumerDeletionPayload> {
  from = '*';

  to = ConsumerStatus.PENDING_DELETION;

  async onTransition(instance: User, data: TransitionData<ConsumerStatus, RequestConsumerDeletionPayload>) {
    // If deletion was requested by user we already checked that all balances are zero,
    // thus we do not need to check it again.
    if (data.reason !== ConsumerDeleteReason.REQUESTED_BY_USER) {
      const wallets = instance.wallets ? instance.wallets : await Wallet.safeFind({ where: { user: instance } });

      const promises = wallets.map(wallet => wallet.getBalances());
      const allBalances = await Promise.all(promises);

      // Transverse the two dimensional array, adding all balances
      // "native" are excluded, as they are Lumens and consumers can't touch that
      const totalBalance = allBalances.reduce(
        (totalSum, balances) =>
          totalSum +
          balances.reduce((sum, balance) => (balance.asset_type === 'native' ? 0 : sum + Number(balance.balance)), 0),
        0,
      );

      if (totalBalance !== 0) throw new Error('All wallets balances must be empty for account deletion');
    }

    // Ensure user status
    if (instance.status !== UserStatus.INACTIVE) {
      const fsm = new UserStateMachine(instance);
      await fsm.goTo(UserStatus.INACTIVE);
    }

    return true;
  }
}
