import { Action } from 'nano-fsm';
import { ConsumerStatus } from '@bacen/base-sdk';
import { User } from '../../../models';

export class SendToPendingLegalAcceptance extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.PENDING_LEGAL_ACCEPTANCE;

  // TODO: Maybe notify users/ mediator ?
}
