import { ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class ConsumerProviderFailedAction extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.PROVIDER_FAILED;

  async onTransition(instance: User, data?: any) {
    // Ensure user major status is correct
    const userFSM = new UserStateMachine(instance);

    if (instance.status !== UserStatus.FAILED) {
      await userFSM.goTo(UserStatus.FAILED);
    }

    return true;
  }
}
