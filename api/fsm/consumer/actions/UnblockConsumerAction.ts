import {
  AssetRegistrationStatus,
  ConsumerStatus,
  IssueCategory,
  IssueDetails,
  IssueType,
  SeverityLevel,
  UserRole,
  WalletStatus,
} from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { Not, IsNull, In } from 'typeorm';
import { Asset, User } from '../../../models';
import { IssueHandler, ProviderManagerService } from '../../../services';

export class UnblockConsumerAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.BLOCKED;

  to = '*';

  public async onTransition(instance: User): Promise<boolean> {
    const user = await User.findOne({
      where: { id: instance.id },
      relations: [
        'wallets',
        'wallets.states',
        'wallets.assetRegistrations',
        'wallets.assetRegistrations.states',
        'wallets.assetRegistrations.asset',
      ],
    });

    if (user.wallets && [UserRole.CONSUMER, UserRole.MEDIATOR].includes(user.role)) {
      const assetsIds = user.wallets
        .filter((w) => w.status === WalletStatus.READY)
        .flatMap((w) =>
          w.assetRegistrations.filter((reg) => reg.status === AssetRegistrationStatus.READY).map((reg) => reg.asset.id),
        );

      const assets = await Asset.find({ where: { provider: Not(IsNull()), id: In(assetsIds) }, relations: ['issuer'] });

      // Create set from providers to deduplicate entries, no need to cast to array, Set is iterable
      const providers = new Set(assets.map((a) => a.provider));

      try {
        for (const provider of providers) {
          this.logger.debug(`Unblocking user ${user.id} on provider ${provider}`);

          const providerService = ProviderManagerService.getInstance().from(provider);
          for (const wallet of user.wallets) {
            const extra = wallet.states.find((state) => state.status === WalletStatus.PENDING).additionalData;
            wallet.user = user;
            await providerService.unblock(wallet, extra);
          }
        }
      } catch (exception) {
        await IssueHandler.getInstance().handle({
          type: IssueType.FSM_ACTION_EXECUTION_FAILED,
          category: IssueCategory.OTHER,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: `Error unblocking user account at provider: user ${user.id}`,
          details: IssueDetails.from(user, exception),
        });

        return false;
      }
    }

    return true;
  }
}
