import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class MarkDocumentsAsInvalidConsumerAction extends Action<User, ConsumerStatus> {
  from = [
    ConsumerStatus.PENDING_DOCUMENTS,
    ConsumerStatus.PROCESSING_DOCUMENTS,
    ConsumerStatus.MANUAL_VERIFICATION,
    ConsumerStatus.KYC_RECHECKING,
    ConsumerStatus.REJECTED,
    ConsumerStatus.PROVIDER_FAILED,
  ];

  to = ConsumerStatus.INVALID_DOCUMENTS;
}
