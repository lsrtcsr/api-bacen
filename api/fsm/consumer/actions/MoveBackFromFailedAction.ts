import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class MoveConsumerBackFromFailedAction extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.PROVIDER_FAILED, ConsumerStatus.PROVIDER_REJECTED];

  to = '*';
}
