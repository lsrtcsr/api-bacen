import { ConsumerStatus, UserStatus, UnleashFlags } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class SendToVerificationConsumerAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PENDING_DOCUMENTS;

  to = ConsumerStatus.PROCESSING_DOCUMENTS;

  async onTransition(instance: User) {
    const user = await User.findOne({
      where: { id: instance.id },
      relations: ['domain', 'states', 'wallets', 'wallets.states', 'consumer', 'consumer.states'],
    });

    // Prepare feature flag context for KYC checking
    const context = { userId: instance.id, properties: { domainId: instance.domain.id } };
    const kycSkipConsumer = UnleashUtil.isEnabled(UnleashFlags.KYC_SKIP_CONSUMER, context, false);

    const hasDocumentsNeeded = await user.consumer.requiredDocumentsProvided();

    // Check user has needed documents for KYC processing
    // If flag KYC_SKIP_CONSUMER this will be skipped
    if (!hasDocumentsNeeded && !kycSkipConsumer) {
      this.logger.info(
        `Required documents have not been provided for user ${user.id}. Skipping transition: `
          .concat(ConsumerStatus.PENDING_DOCUMENTS)
          .concat(' => ')
          .concat(ConsumerStatus.PROCESSING_DOCUMENTS),
      );
      return false;
    }

    // Ensure user major status is correct
    if (user.status === UserStatus.PENDING) {
      const userFSM = new UserStateMachine(user);
      await userFSM.goTo(UserStatus.PROCESSING);
    }

    return true;
  }
}
