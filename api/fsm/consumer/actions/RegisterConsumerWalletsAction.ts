import { ConsumerStatus, UserStatus, WalletTransientStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { UserRepository } from '../../../repositories';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class RegisterConsumerWalletsAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PROCESSING_WALLETS;

  to = ConsumerStatus.READY;

  async onTransition(instance: User) {
    const userRepository = new UserRepository();
    const user = (await userRepository.getById(instance.id, [
      'states',
      'consumer',
      'consumer.states',
      'wallets',
      'wallets.states',
      'wallets.assetRegistrations',
      'wallets.assetRegistrations.states',
      'wallets.assetRegistrations.asset',
    ])) as User;

    const hasAtleastOneWalletReady = user.wallets.some((wallet) => wallet.status === WalletTransientStatus.READY);
    if (!hasAtleastOneWalletReady) {
      return false;
    }

    // Ensure user major status is correct
    if (user.status !== UserStatus.ACTIVE) {
      const userFSM = new UserStateMachine(user);
      await userFSM.goTo(UserStatus.ACTIVE);
    }

    return true;
  }
}
