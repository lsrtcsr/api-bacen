import { ConsumerStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { Logger } from 'nano-errors';
import { User } from '../../../models';

export class MarkConsumerForKYCRecheck extends Action<User, ConsumerStatus> {
  from = '*';

  to = ConsumerStatus.KYC_RECHECKING;

  async onTransition(instance: User) {
    // Skip transition if user is already marked for KYC recheck
    if (instance.consumer.status === ConsumerStatus.KYC_RECHECKING) {
      Logger.getInstance().warn(
        'Tried to mark a consumer for KYC recheck, but he was already marked as such',
        instance,
      );
      return false;
    }

    return true;
  }
}
