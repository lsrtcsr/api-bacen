import { Action } from 'nano-fsm';
import {
  ConsumerStatus,
  IssueType,
  IssueCategory,
  SeverityLevel,
  UserStatus,
  PlanStatus,
  UnleashFlags,
  IssueDetails,
  UserRole,
  ServiceType,
} from '@bacen/base-sdk';
import * as Package from 'pjson';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseError } from 'nano-errors';
import { PlanService, IssueHandler, EventHandlingGateway, ContractService } from '../../../services';
import { User, Plan } from '../../../models';
import { UserStateMachine } from '../../user';

export class SubscribeBillingPlanAction extends Action<User, ConsumerStatus> {
  public from = ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;

  public to = ConsumerStatus.READY;

  public async onTransition(instance: User): Promise<boolean> {
    const user = await User.findOne({
      where: { id: instance.id },
      relations: ['domain', 'states', 'consumer', 'consumer.states'],
    });

    const context = { userId: user.id, properties: { domainId: user.domain.id } };
    const skipBillingPlanSubscription = UnleashUtil.isEnabled(
      instance.role === UserRole.MEDIATOR
        ? UnleashFlags.SKIP_MEDIATOR_BILLING_PLAN_SUBSCRIPTION
        : UnleashFlags.SKIP_CONSUMER_BILLING_PLAN_SUBSCRIPTION,
      context,
      false,
    );

    if (!skipBillingPlanSubscription) {
      try {
        const currentContract = await ContractService.getInstance().getCurrentContract(user.id);
        if (currentContract) return true;

        const subscriptionOptions = { user } as any;

        let choice;
        const consumerCurrentState = user.consumer.getStates()[0];
        if (consumerCurrentState?.additionalData?.plan) {
          choice = consumerCurrentState.additionalData.plan;
        } else {
          const initialUserState = user.states.find((state) => state.status === UserStatus.PENDING);
          choice = initialUserState?.additionalData?.plan;
        }

        const planId = choice ? choice.planId || choice.id : undefined;
        const plan = planId
          ? await Plan.safeFindOne({ where: { id: planId, status: PlanStatus.AVAILABLE } })
          : await PlanService.getInstance().getDefaultPlan(user.domain);

        if (!plan) {
          const errorMessage = planId
            ? `There is no billing plan with the given ID ${planId}`
            : `Default billing plan of domain ${user?.domain?.id} not found!`;

          throw new BaseError(errorMessage);
        }

        subscriptionOptions.plan = plan.id;
        subscriptionOptions.prepaid = choice && choice.hasOwnProperty('prepaid') ? choice.prepaid : true;

        await PlanService.getInstance().subscribe(subscriptionOptions);
      } catch (exception) {
        await IssueHandler.getInstance().handle({
          type: IssueType.PLAN_SUBSCRIPTION_ERROR,
          category: IssueCategory.BILLING_PLAN_SUBSCRIPTION,
          severity: SeverityLevel.HIGH,
          componentId: Package.name,
          details: IssueDetails.from(user, exception),
          description: 'error subscribing plan',
        });

        return false;
      }

      try {
        await EventHandlingGateway.getInstance().process(ServiceType.ACCOUNT_OPENING, { user });
      } catch (exception) {
        // TODO should abort state transation?

        await IssueHandler.getInstance().handle({
          type: IssueType.ENTRY_CREATION_ERROR,
          category: IssueCategory.BILLING,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          details: {
            exception,
            resourceId: user.id,
            resourceType: 'User',
            resourceSnapshot: user,
          },
          description: 'error creating invoice entry related to the account opening',
        });
      }
    }

    if (user.status === UserStatus.PROCESSING) {
      const userFSM = new UserStateMachine(user);
      await userFSM.goTo(UserStatus.ACTIVE);
    }

    return true;
  }
}
