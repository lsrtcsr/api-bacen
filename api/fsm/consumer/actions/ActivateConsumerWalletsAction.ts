import { Action } from 'nano-fsm';
import { ConsumerStatus, WalletStatus } from '@bacen/base-sdk';
import { User } from '../../../models';
import { WalletStateMachine } from '../../wallet';

export class ActivateConsumerWalletsAction extends Action<User, ConsumerStatus> {
  public from = ConsumerStatus.PROCESSING_PROVIDER_DOCUMENTS;

  public to = ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;

  public async onTransition(instance: User): Promise<boolean> {
    try {
      const user = await User.findConsumerByIdAndPopulate(instance.id);

      const allWalletsReady = user.wallets.every(wallet => wallet.status === WalletStatus.READY);
      if (allWalletsReady) return true;

      const walletsPendingProviderApproval = user.wallets.filter(
        ({ status }) => status === WalletStatus.PENDING_PROVIDER_APPROVAL,
      );
      const walletsReady = user.wallets.filter(({ status }) => status === WalletStatus.READY);

      this.logger.debug(
        `ActivateConsumerWalletsAction: found ${walletsPendingProviderApproval.length} wallets in ${WalletStatus.PENDING_PROVIDER_APPROVAL}`,
      );
      for (const wallet of walletsPendingProviderApproval) {
        this.logger.debug(`ActivateConsumerWalletsAction: Sending wallet ${wallet.id} to ${WalletStatus.READY}`);

        const fsm = new WalletStateMachine(wallet);
        const success = await fsm.goTo(WalletStatus.READY);

        if (success) {
          this.logger.debug(
            `ActivateConsumerWalletsAction: Successfully transitioned Wallet ${wallet.id} to ${WalletStatus.READY}`,
          );
          walletsReady.push(wallet);
        }
      }

      // Fail silently if not all wallets are ready.
      return user.wallets.length === walletsReady.length;
    } catch (err) {
      // TODO: log issue.
      return false;
    }
  }
}
