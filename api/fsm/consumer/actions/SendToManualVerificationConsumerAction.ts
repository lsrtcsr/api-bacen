import { ConsumerStatus, IssueCategory, IssueDetails, IssueType, SeverityLevel } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import * as Package from 'pjson';
import { User } from '../../../models';
import { IssueHandler } from '../../../services';

export class SendToManualVerificationConsumerAction extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.INVALID_DOCUMENTS, ConsumerStatus.PROCESSING_DOCUMENTS, ConsumerStatus.KYC_RECHECKING];

  to = ConsumerStatus.MANUAL_VERIFICATION;

  async onTransition(instance: User, data: any = { forceManualKYC: false }) {
    if (!data.forceManualKYC) {
      IssueHandler.getInstance().handle({
        type: IssueType.USER_SENT_TO_MANUAL_VERIFICATION,
        category: IssueCategory.KYC_FLOW,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: 'Automatic verification failed: user registration sent to manunual verification',
        details: IssueDetails.from(instance),
      });
    }

    return true;
  }
}
