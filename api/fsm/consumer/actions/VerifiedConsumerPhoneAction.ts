import { ConsumerStatus, UnleashFlags } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { User } from '../../../models';

export class VerifiedConsumerPhoneAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PENDING_PHONE_VERIFICATION;

  to = ConsumerStatus.PENDING_LEGAL_ACCEPTANCE;

  async onTransition(instance: User, data: any = {}) {
    const context = { userId: instance.id, properties: { domainId: instance.domain.id } };
    const skipPhoneVerification = UnleashUtil.isEnabled(UnleashFlags.SKIP_PHONE_VERIFICATION, context, false);

    if (skipPhoneVerification) {
      this.logger.warn('Skipping phone verification for feature flag is active', {
        user: instance.id,
        domain: instance.domain.id,
        skipPhoneVerification,
      });

      return true;
    }

    const verifiedPhone = await instance.consumer.phoneVerified();

    if (!verifiedPhone) {
      // Fail silently so we can wait user verify the phone
      return false;
    }

    return true;
  }
}
