import { Action } from 'nano-fsm';
import { ConsumerStatus, IssueType, IssueCategory, SeverityLevel, UserRole, WalletStatus } from '@bacen/base-sdk';
import { Logger } from 'ts-framework-common';
import * as Package from 'pjson';
import { User } from '../../../models';
import { IssueHandler } from '../../../services/issue/IssueHandler';

export class SendMediatorToReadyAction extends Action<User, ConsumerStatus> {
  public from = ConsumerStatus.PENDING_DOCUMENTS;

  public to = ConsumerStatus.READY;

  public async onTransition(instance: User): Promise<boolean> {
    if (instance.role !== UserRole.MEDIATOR) return false;

    const user = await User.findOne({
      where: { id: instance.id },
      relations: ['consumer', 'wallets', 'wallets.states'],
    });

    if (user.wallets.some(wallet => wallet.status !== WalletStatus.READY)) {
      await IssueHandler.getInstance().handle({
        type: IssueType.FSM_ACTION_EXECUTION_FAILED,
        category: IssueCategory.OTHER,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: {
          resourceId: user.id,
          resourceType: 'User',
          resourceSnapshot: user,
        },
        description: 'Cannot set consumer as "ready" without all its wallets also as "ready"',
      });

      return false;
    }

    Logger.getInstance().warn(`Skipping document verification for mediator: ${user.id}`);

    return true;
  }
}
