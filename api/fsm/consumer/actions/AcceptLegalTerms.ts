import { Action } from 'nano-fsm';
import { ConsumerStatus, UserStatus, UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class AcceptLegalTerms extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PENDING_LEGAL_ACCEPTANCE;

  to = ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;

  async onTransition(instance: User) {
    const user = await User.findOne({
      where: { id: instance.id },
      relations: ['domain', 'states', 'wallets', 'wallets.states', 'consumer', 'consumer.states'],
    });

    // Prepare feature flag context
    const context = { userId: instance.id, properties: { domainId: instance.domain.id } };
    const skipLegalTermsAcceptance = UnleashUtil.isEnabled(UnleashFlags.SKIP_LEGAL_TERMS_ACCEPTANCE, context, false);
    const hasAcceptedTerms = await user.hasAcceptedAllLegalTerms();

    if (!skipLegalTermsAcceptance && !hasAcceptedTerms)
      // Fail silently so user may accept pending legal-terms.
      return false;

    // Ensure user major status is correct
    if (user.status === UserStatus.PENDING) {
      const userFSM = new UserStateMachine(user);
      await userFSM.goTo(UserStatus.PROCESSING);
    }

    return true;
  }
}
