import { ConsumerStatus, UnleashFlags } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { BaseError } from 'nano-errors';
import { UnleashUtil } from '@bacen/shared-sdk';
import { User } from '../../../models';

export class ConsumerKYCManuallyApproved extends Action<User, ConsumerStatus> {
  from = [ConsumerStatus.INVALID_DOCUMENTS, ConsumerStatus.MANUAL_VERIFICATION];

  to = ConsumerStatus.MANUALLY_APPROVED;

  async onTransition(instance: User, data: any) {
    const user =
      instance.consumer && instance.consumer.documents
        ? instance
        : await User.safeFindOne({ where: { id: instance.id }, relations: ['consumer', 'consumer.documents'] });

    if (UnleashUtil.isEnabled(UnleashFlags.KYC_SKIP_DOCUMENT_APPROVAL, {}, false)) {
      return true;
    }

    if (!(await user.consumer.requiredDocumentsProvided())) {
      throw new BaseError('One or more documents required for consumer approval have not been provided');
    }

    if (!(await user.consumer.documentsApproved())) {
      throw new BaseError('Consumer documents must be verified before registration approval');
    }

    return true;
  }
}
