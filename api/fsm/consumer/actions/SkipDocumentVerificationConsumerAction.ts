import { ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { BaseError, Logger } from 'ts-framework-common';
import { User } from '../../../models';
import { UserStateMachine } from '../../user';

export class SkipDocumentVerificationConsumerAction extends Action<User, ConsumerStatus> {
  from = ConsumerStatus.PENDING_DOCUMENTS;

  to = ConsumerStatus.PROCESSING_WALLETS;

  async onTransition(instance: User) {
    if (!instance.domain) {
      throw new BaseError('Cannot skip document verification without the user domain information');
    }

    // Ensure user major state is correct
    if (instance.status === UserStatus.PENDING) {
      const userFSM = new UserStateMachine(instance);
      await userFSM.goTo(UserStatus.PROCESSING);
    }

    Logger.getInstance().warn('Skipping document verification for user', instance);

    return true;
  }
}
