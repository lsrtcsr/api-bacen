import {
  ConsumerStatus,
  SeverityLevel,
  IssueType,
  IssueCategory,
  IssueDetails,
  UserStatus,
} from '@bacen/base-sdk';
import * as Package from 'pjson';
import FSM from 'nano-fsm';
import { Logger, SentryTransport } from 'ts-framework-common';
import { LoggerInstance } from 'nano-errors';
import { ConsumerState, Document, User, Postback } from '../../models';
import { IssueHandler } from '../../services';
import * as Actions from './actions';
import { UserStateMachine } from '../user';

const resultsContainingKYC = [
  ConsumerStatus.INVALID_DOCUMENTS,
  ConsumerStatus.MANUAL_VERIFICATION,
  ConsumerStatus.PROCESSING_WALLETS,
  ConsumerStatus.REJECTED,
];
const anormalResults = [
  ConsumerStatus.BLOCKED,
  ConsumerStatus.INVALID_DOCUMENTS,
  ConsumerStatus.MANUAL_VERIFICATION,
  ConsumerStatus.PROVIDER_FAILED,
  ConsumerStatus.REJECTED,
  ConsumerStatus.SUSPENDED,
  ConsumerStatus.PROVIDER_REJECTED,
];

export type ConsumerFSMPayload = Actions.RequestConsumerDeletionPayload | any;

export class ConsumerStateMachine extends FSM<User, ConsumerStatus, ConsumerFSMPayload> {
  /* Sets the machine initial state */
  initialState: ConsumerStatus = ConsumerStatus.PENDING_DOCUMENTS;

  // String enums don't have reverse mapping, so this works
  states = Object.values(ConsumerStatus);

  // Instantiating Sentry logger
  // TODO: Move this to logger config
  sentryLogger: LoggerInstance = Logger.initialize({
    transports: [...Logger.DEFAULT_TRANSPORTS, new SentryTransport({ dsn: process.env.SENTRY_KYC_DSN })],
  });

  /* Sets the machine available actions */
  actions = [
    new Actions.AcceptLegalTerms(),
    new Actions.PendingPhoneVerification(),
    new Actions.VerifiedConsumerPhoneAction(),
    new Actions.SendToPendingLegalAcceptance(),
    new Actions.SubscribeBillingPlanAction(),
    new Actions.ChangeBillingPlanAction(),
    new Actions.DeleteConsumerAction(),
    new Actions.RequestDeletionConsumerAction(),
    new Actions.SendMediatorToReadyAction(),
    new Actions.NotifyUserInclusionAction(),
  ];

  constructor(instance: User) {
    // Get current transaction state from model
    super(instance, { state: instance.consumer.status });
  }

  async afterTransition(
    from: ConsumerStatus | (ConsumerStatus | string)[],
    to: ConsumerStatus,
    data: any,
  ): Promise<void> {
    // Get updated user instance for FSM
    this.instance = await User.findOne(this.instance.id, {
      relations: ['states', 'consumer', 'consumer.states', 'consumer.documents', 'domain', 'wallets', 'wallets.states'],
    });

    const additionalData = data || {};

    if (process.env.K8S_POD_NAME) additionalData.pod = process.env.K8S_POD_NAME;

    await ConsumerState.insert({ additionalData, status: to, consumer: this.instance.consumer });

    if (to === ConsumerStatus.READY) {
      this.logger.debug(`Cacheing active consumer status for consumerId ${this.instance.consumer.id}`);
      await this.instance.cacheConsumerActiveStatus(true);
    } else {
      this.logger.debug(`Invalidating active consumer status cache for consumerId ${this.instance.consumer.id}`);
      await this.instance.invalidateConsumerActiveCache();
    }

    if (this.instance.domain && this.instance.domain.postbackUrl) {
      // Prepare postback instance for job queue
      const postback = await Postback.fromUser(this.instance, this.instance.consumer.status);
      await postback.send();
    }
  }
}
