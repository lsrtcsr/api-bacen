import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class ReprocessMediatorAction extends Action<User, UserStatus> {
  from = UserStatus.ACTIVE;

  to = UserStatus.PROCESSING;

  async onTransition(instance: User, data?: any): Promise<boolean> {
    return true;
  }
}
