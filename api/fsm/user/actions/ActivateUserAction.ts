import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class ActivateUserAction extends Action<User, UserStatus> {
  from = [UserStatus.PROCESSING, UserStatus.INACTIVE, UserStatus.PENDING];

  to = UserStatus.ACTIVE;
}
