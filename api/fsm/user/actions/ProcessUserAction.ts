import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class ProcessUserAction extends Action<User, UserStatus> {
  from = UserStatus.PENDING;

  to = UserStatus.PROCESSING;
}
