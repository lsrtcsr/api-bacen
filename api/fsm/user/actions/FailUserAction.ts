import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class FailUserAction extends Action<User, UserStatus> {
  from = '*';

  to = UserStatus.FAILED;
}
