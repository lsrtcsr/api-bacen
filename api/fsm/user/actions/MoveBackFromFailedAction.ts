import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class MoveUserBackFromFailedAction extends Action<User, UserStatus> {
  from = UserStatus.FAILED;

  to = '*';
}
