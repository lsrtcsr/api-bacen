import { UserStatus } from '@bacen/base-sdk';
import { Action } from 'nano-fsm';
import { User } from '../../../models';

export class DisableUserAction extends Action<User, UserStatus> {
  from = '*';

  to = UserStatus.INACTIVE;
}
