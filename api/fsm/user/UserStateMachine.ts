import { UserStatus } from '@bacen/base-sdk';
import FSM from 'nano-fsm';
import { User, UserState } from '../../models';
import * as Actions from './actions';

export class UserStateMachine extends FSM<User, UserStatus> {
  /* Sets the machine initial state */
  initialState: UserStatus = UserStatus.PENDING;

  states = Object.values(UserStatus);

  /* Sets the machine available actions */
  actions = [
    new Actions.ActivateUserAction(),
    new Actions.DisableUserAction(),
    new Actions.FailUserAction(),
    new Actions.MoveUserBackFromFailedAction(),
    new Actions.ProcessUserAction(),
    new Actions.ReprocessMediatorAction(),
  ];

  constructor(instance) {
    // Get current transaction state from model
    super(instance, { state: instance.status });
  }

  async afterTransition(from: UserStatus | (UserStatus | string)[], to: UserStatus, data: any): Promise<void> {
    const additionalData = data || {};
    if (process.env.K8S_POD_NAME) additionalData.pod = process.env.K8S_POD_NAME;

    const state = await UserState.insertAndFind(
      UserState.create({
        status: to,
        user: this.instance,
        additionalData,
      }),
    );

    if (to === UserStatus.ACTIVE) {
      this.logger.debug(`Cacheing userStatus as active for userId ${this.instance.id}`);
      await this.instance.cacheUserActiveStatus(true);
    } else {
      this.logger.debug(`Invalidating user_active cache for userId ${this.instance.id}`);
      await this.instance.invalidateUserActiveCache();
    }

    this.instance.states && this.instance.states.push(state);
  }
}
