import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateHypertableFunction1545000000000 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE OR REPLACE FUNCTION create_hypertable_on_create_table()
        RETURNS event_trigger LANGUAGE plpgsql AS $$
      DECLARE
        obj record;
      BEGIN 
        SELECT object_identity INTO obj FROM pg_event_trigger_ddl_commands();
        CASE WHEN obj.object_identity <> 'public.migrations' THEN
          RAISE NOTICE 'Creating hypertable for %', obj.object_identity;
          PERFORM create_hypertable(obj.object_identity, 'time', if_not_exists => TRUE);
        END CASE;
      END
      $$;	
      
      DROP EVENT TRIGGER IF EXISTS create_hypertable_trigger;
      CREATE EVENT TRIGGER create_hypertable_trigger
        ON ddl_command_end
        WHEN TAG IN ('SELECT INTO', 'CREATE TABLE', 'CREATE TABLE AS')
      EXECUTE PROCEDURE create_hypertable_on_create_table();`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP EVENT TRIGGER create_hypertable_trigger;`);
    await queryRunner.query(`DROP FUNCTION create_hypertable_on_create_table;`);
  }
}
