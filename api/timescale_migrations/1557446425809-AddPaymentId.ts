import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPaymentId1557446425809 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ADD "paymentId" character varying NOT NULL`);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "payment" DROP COLUMN "paymentId"`);
  }
}
