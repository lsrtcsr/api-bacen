import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNewPaymentType1578497782232 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'mobile_recharge')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`, undefined);
  }
}
