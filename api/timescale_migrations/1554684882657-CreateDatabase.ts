import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDatabase1554684882657 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee')`,
    );
    await queryRunner.query(
      `CREATE TABLE "payment" ("time" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "transactionHash" character varying NOT NULL, "asset" character varying NOT NULL, "source" character varying NOT NULL, "type" "payment_type_enum" NOT NULL, "domainId" character varying NOT NULL, "destination" character varying NOT NULL, "amount" character varying NOT NULL, CONSTRAINT "PK_28a93d6c6a63e44cd058155c7f2" PRIMARY KEY ("time"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "payment"`);
    await queryRunner.query(`DROP TYPE "payment_type_enum"`);
  }
}
