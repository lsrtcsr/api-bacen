import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdatePaymentTypeEnum1581541976783 implements MigrationInterface {
  name = 'UpdatePaymentTypeEnum1581541976783';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT CURRENT_TIMESTAMP`, undefined);
    await queryRunner.query(`ALTER TYPE "public"."payment_type_enum" RENAME TO "payment_type_enum_old"`, undefined);
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum" AS ENUM('balance_adjustment', 'boleto', 'card', 'authorized_card', 'authorized_card_reversal', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum" USING "type"::"text"::"payment_type_enum"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum_old"`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "payment_type_enum_old" AS ENUM('balance_adjustment', 'boleto', 'card', 'deposit', 'withdrawal', 'transfer', 'transaction_reversal', 'service_fee', 'manual_adjustment', 'phone_credit')`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "payment" ALTER COLUMN "type" TYPE "payment_type_enum_old" USING "type"::"text"::"payment_type_enum_old"`,
      undefined,
    );
    await queryRunner.query(`DROP TYPE "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TYPE "payment_type_enum_old" RENAME TO  "payment_type_enum"`, undefined);
    await queryRunner.query(`ALTER TABLE "payment" ALTER COLUMN "time" SET DEFAULT now()`, undefined);
  }
}
