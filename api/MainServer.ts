import IdentityService from '@bacen/identity-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { HeartbeatClient, RedisService } from '@bacen/service-heartbeat';
import { StellarService } from '@bacen/stellar-service';
import * as compression from 'compression';
import * as express from 'express';
import { RedisDiscoveryStorage, RedisObservable } from 'nano-discovery';
import Server, { ServerOptions } from 'ts-framework';
import { BaseError } from 'ts-framework-common';
import { TextGateway } from 'ts-framework-notification';
import { Versioning, VersioningOptions } from 'ts-framework-versioning';
import { HooksService } from '@bacen/hooks-sdk';
import { getConnection } from 'typeorm';
import { DictWebService } from '@bacen/dict-sdk';
import { AmqpService } from '@bacen/events';
import Config from '../config';
import { DatabaseGroup, PublishQueueGroup } from './groups';
import { ImpersonateGrantType, SecretTokenGrantType } from './helpers';
import * as Services from './services';
import {
  AuthorizerQueryService,
  CacheService,
  EmailTemplateService,
  MetricsService,
  SPBParticipantsService,
} from './services';
import { requestCache } from './utils';
import * as Events from './events';

const { logger } = Config.server;

// Initialize feature flag and metrics clients asap
UnleashUtil.initialize({ logger, ...Config.unleash });
MetricsService.initialize({ logger, ...Config.metrics });

// Prepare AssetService as soon as possible
Services.AssetService.initialize({
  logger,
  rootAssetConfigPath: Config.seed.rootAssetConfigPath,
  initialAssetConfigPath: Config.assetsInCustody.initialAssetsConfigPath,
});

// Prepare the database instance as soon as possible to prevent clashes in
// model registration. We can connect to the real database later.
DatabaseGroup.initialize({ logger });

// AMQP base service components group
PublishQueueGroup.initialize({ logger });

// Initialize Stellar service
const stellar = StellarService.initialize({ ...Config.stellar, logger });

Services.SignerService.initialize({
  logger,
  channelAccounts: Config.stellar.channelAccounts,
});

/* Provider services */
Services.StellarProviderService.initialize({ stellar, logger } as any);
Services.ProviderManagerService.initialize({
  providers: [
    Services.CDTProviderService.initialize({ ...Config.cards, logger }),
    Services.CreditasroviderService.initialize({ ...Config.creditasprovider, logger }),
    Services.BS2ProviderService.initialize({ ...Config.bs2, logger }),
    Services.ParatiProvider.initialize({ ...Config.paratiProvider, logger }),
    Services.CelcoinProviderService.initialize({ ...Config.celcoinProvider, logger }),
  ],
});

/* Identity Services */
IdentityService.initialize({ ...Config.identity });

export interface MainServerOptions extends ServerOptions {
  smtp?: {
    from: string;
    connectionUrl?: string;
  };
  sms?: {
    gateway: {
      from: string;
      gateway: TextGateway;
      gatewayOptions: {
        accountSid: string;
        authToken: string;
      };
    };
    tokenTTL: number;
    countryCode: string;
    secret: string;
  };
  versioning?: VersioningOptions;
}

export default class MainServer extends Server {
  public options: MainServerOptions;

  constructor(options?: MainServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, smtp, ...otherOptions } = { ...Config.server, ...options };

    // Add action logger middleware
    app.use(requestCache());
    app.use(compression());

    // Initialize basic services
    Services.CacheService.initialize(Config.redis);

    // Initialize the email service
    if (smtp && smtp.connectionUrl) {
      Services.EmailService.initialize({ ...smtp, template: { enabled: true }, logger });
    } else if (process.env.NODE_ENV !== 'production') {
      // Mock the email service sending to console
      Services.EmailService.initialize({ from: 'example@company.com', ...smtp, logger });
    } else {
      // Crash the API for safety or log text messages sendings in console
      throw new BaseError('Email service is not configured and is needed in production');
    }

    // Initialize identity services
    Services.PersonIdentityService.initialize({ logger });
    Services.CompanyIdentityService.initialize({ logger });

    // issue handling and notification
    Services.IssueHandler.initialize({ logger });
    Services.IssueAlertService.initialize({ logger });
    Services.JiraService.initialize({
      logger,
      name: 'JiraService',
      debug: process.env.NODE_ENV === 'test',
      baseUrl: Config.jira.baseUrl,
      issueAPIEndpoint: Config.jira.issueAPIEndpoint,
      auth: {
        username: Config.jira.auth.username,
        apiToken: Config.jira.auth.apiToken,
      },
    });

    // Initialize Limit Settings service
    Services.LimitSettingService.initialize({ logger });
    Services.UserMetricsService.initialize({ logger });

    // Initialize Billing related services
    Services.BillingService.initialize({ logger });
    Services.ContractService.initialize({ logger });
    Services.PlanService.initialize({ logger });
    Services.EventHandlingGateway.initialize({ logger });
    Services.MediatorBillingClient.initialize({ logger });
    Services.ClosingEntryHandler.initialize({ logger });
    Services.BoletoConsolidationService.initialize({ logger });

    new Services.TransactionHandler();
    new Services.ComplianceCheckServiceHandler();
    new Services.AccountServiceHandler();
    new Services.BoletoServiceHandler();
    new Services.CardIssuingServiceHandler();

    // Initialize Utility Service
    Services.DiscoveryService.initialize({
      logger,
      storage: new RedisDiscoveryStorage({ ...Config.redis }),
      observable: new RedisObservable({ clientOpts: { ...Config.redis } }),
    });

    Services.AuthorizationService.initialize({ logger });
    Services.StellarQueryService.initialize({ logger });
    Services.CDTAuthorizationService.initialize();
    Services.RequestLogService.initialize();

    HooksService.initialize({ baseURL: Config.hooksproxy.baseURL });
    RedisService.initialize({ clientOpts: Config.redis, logger });
    DictWebService.initialize({ ...Config.dict });
    SPBParticipantsService.initialize({ logger, spbParticipantsProvider: Config.spb.spbDefaultProvider });

    HeartbeatClient.initialize({ logger });
    EmailTemplateService.initialize({
      logger,
      emailConfigPath: Config.email.emailConfigPath,
      defaultParameters: Config.email.defaultParameters,
    });

    // Initialize entity services;
    Services.configure(logger, Services.AssetService.getInstance(), IdentityService.getInstance());

    const components = [
      Services.MetricsService.getInstance(),

      /* Databases */
      DatabaseGroup.getInstance(),

      /* Queue publishing services */
      PublishQueueGroup.getInstance(),

      /* Provider services */
      Services.ProviderManagerService.getInstance(),

      /* Other Services */
      Services.AssetService.getInstance(),
      Services.SignerService.getInstance(),
      Services.LegacyWalletService.getInstance(),
      Services.IssueHandler.getInstance(),
      Services.IssueAlertService.getInstance(),
      Services.JiraService.getInstance(),
      Services.LimitSettingService.getInstance(),
      Services.DiscoveryService.getInstance(),
      Services.BillingService.getInstance(),
      Services.ContractService.getInstance(),
      Services.PlanService.getInstance(),
      Services.EventHandlingGateway.getInstance(),
      Services.MediatorBillingClient.getInstance(),
      Services.PersonIdentityService.getInstance(),
      Services.CompanyIdentityService.getInstance(),
      Services.StellarQueryService.getInstance(),
      RedisService.getInstance(),
      HeartbeatClient.getInstance(),
      EmailTemplateService.getInstance(),
      SPBParticipantsService.getInstance(),
      AmqpService.initialize({
        logger,
        connectionUrl: Config.queue.amqp.host,
        prefetch: 1,
        events: Object.values(Events),
      }),
    ];

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...components, ...children],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('./controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('./models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              accessTokenLifetime: 7 * 24 * 60 * 60,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  public onMount() {
    /* Register external middlewares */
    this.app.use(Versioning.middleware({ ...Config.server.versioning, logger }));
    this.app.enable('trust proxy');

    // Register the email service
    this.component(Services.EmailService.getInstance());

    // Initialize the text service
    if (
      this.options.sms &&
      this.options.sms.gateway &&
      this.options.sms.gateway.gatewayOptions.accountSid &&
      this.options.sms.gateway.gatewayOptions.authToken
    ) {
      this.component(Services.TextService.getInstance({ ...this.options.sms.gateway, logger }));
    } else if (process.env.NODE_ENV !== 'production') {
      // Mock the text service sending to console
      this.component(Services.TextService.getInstance({ gateway: TextGateway.DEBUG }));
    } else {
      // Crash the API for safety or log text sendings in console
      throw new BaseError('SMS Text service is not configured and is needed in production');
    }

    /* Mount server options */
    super.onMount();
  }

  public async onInit() {
    await super.onInit();
    AuthorizerQueryService.initialize(getConnection());
  }

  public async onUnmount(): Promise<void> {
    let cacheServiceInstance: CacheService;
    try {
      cacheServiceInstance = CacheService.getInstance();
      await cacheServiceInstance.quit();
    } catch (error) {}
  }
}
