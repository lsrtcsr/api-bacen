import { EntityDatabase } from 'ts-framework-sql';
import Config from '../config';
import { PaymentLog, TimescaleEntity } from './timescale';

export default class TimescaleDatabase extends EntityDatabase {
  public static readonly ENTITIES = [TimescaleEntity, PaymentLog];

  protected static readonly instance: TimescaleDatabase = new TimescaleDatabase({
    connectionOpts: {
      ...Config.timescale,
      name: 'timescale',
      entities: TimescaleDatabase.ENTITIES,
    },
  } as any);

  /**
   * Gets the singleton database instance.
   */
  static getInstance(): any {
    return this.instance;
  }
}
