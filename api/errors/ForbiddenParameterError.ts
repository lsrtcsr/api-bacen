import { HttpError, HttpCode } from 'ts-framework';

export class ForbiddenParameterError extends HttpError {
  constructor(entity: string, params: string[], details?: any) {
    super(`One or more ${entity} parameters are forbidden for this request`, HttpCode.Client.FORBIDDEN, {
      params,
      entity,
      ...details,
    });
  }
}
