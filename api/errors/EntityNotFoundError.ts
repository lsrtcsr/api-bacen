import { HttpError, HttpCode } from 'ts-framework';

export class EntityNotFoundError extends HttpError {
  constructor(entityName: string, details?: any) {
    super(`${entityName} not found`, HttpCode.Client.NOT_FOUND, {
      entity: entityName,
      ...details,
    });
  }
}
