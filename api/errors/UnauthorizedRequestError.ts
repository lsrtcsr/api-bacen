import { HttpError, HttpCode } from 'ts-framework';

export class UnauthorizedRequestError extends HttpError {
  constructor(message?: string, details?: any) {
    super(message || 'Unauthorized', HttpCode.Client.UNAUTHORIZED, { ...details });
  }
}
