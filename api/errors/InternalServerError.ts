import { HttpError, HttpCode } from 'ts-framework';

export class InternalServerError extends HttpError {
  constructor(reason: string, params?: object) {
    super(reason, HttpCode.Server.INTERNAL_SERVER_ERROR, params);
  }
}
