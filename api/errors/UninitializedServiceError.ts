import { BaseError } from 'nano-errors';

export class UninitializedServiceError extends BaseError {
  constructor(service: string) {
    super('Service is not initialized', { service });
  }
}
