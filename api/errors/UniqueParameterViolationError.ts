import { HttpCode, HttpError } from 'ts-framework';

export class UniqueParameterViolationError extends HttpError {
  constructor(objectName: string, details?: any) {
    super(`${objectName} already exists, ensure all params are unique`, HttpCode.Client.BAD_REQUEST, { ...details });
  }
}
