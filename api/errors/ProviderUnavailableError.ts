import { HttpError, HttpCode } from 'ts-framework';
import { DiscoveryType } from '../services';

export class ProviderUnavailableError extends HttpError {
  constructor(service: DiscoveryType, details?: any) {
    super(`Service ${service} is unavailable`, HttpCode.Server.SERVICE_UNAVAILABLE, { ...details });
  }
}
