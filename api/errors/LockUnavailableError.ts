import { HttpError } from 'ts-framework';

export class LockUnavailableError extends HttpError {
  name = 'LockUnavailableError';

  constructor(details?: any) {
    // TODO: Create better error message.
    super('Unable to acquire advisory lock', 422, details);
  }
}
