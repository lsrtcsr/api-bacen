import { LoggerInstance, Logger } from 'nano-errors';
import { HttpCode, HttpError } from 'ts-framework';
import fclone from 'fclone';

export interface WrapErrorOptions {
  logger?: LoggerInstance;
  defaultMessage?: string;
  overrideMessage?: string;
  isOnboardingFlow?: boolean;
}

const getStatusFromDetails = (details: any) => {
  if (details.status) return details.status;
  if (details.response && details.response.status) return details.response.status;
  if (details.details) return getStatusFromDetails(details.details);
  if (details.data) return getStatusFromDetails(details.data);
  return HttpCode.Server.INTERNAL_SERVER_ERROR;
};

export function wrapError(error: any, options: WrapErrorOptions = { logger: Logger.getInstance() }): never {
  const details = error.details || error.response?.data || error.data || {};

  const message =
    options.overrideMessage ||
    (details.data && details.data.message) ||
    details.message ||
    error.message ||
    options.defaultMessage;

  const status = error.status || getStatusFromDetails(details);

  options?.logger?.error(message, {
    details,
    stackId: error.stackId,
    stack: error.stack,
  });

  throw new HttpError(message, status, fclone({ stackId: error.stackId, ...details }));
}
