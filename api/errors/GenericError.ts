import { BaseError } from 'nano-errors';
import { IssueType, IssueCategory } from '@bacen/base-sdk';

export class GenericError extends BaseError {
  type: IssueType;

  category: IssueCategory;

  constructor(options: { defaultMessage: string; type?: IssueType; category?: IssueCategory; details?: object }) {
    super(options.defaultMessage, { ...options.details });

    this.type = options.type || IssueType.UNKNOWN;
    this.category = options.category || IssueCategory.OTHER;
  }
}
