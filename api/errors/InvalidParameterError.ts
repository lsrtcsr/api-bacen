import { HttpError, HttpCode } from 'ts-framework';
import { ValidationError } from 'class-validator';

const flattenErrors = (errors: ValidationError[]): ValidationError[] => {
  let flat = [];
  for (let i = 0; i < errors.length; i += 1) {
    flat.push(errors[i]);
    if (errors[i].children) {
      const children = flattenErrors(errors[i].children);
      flat = flat.concat(children);
    }
  }
  return flat;
};

export class InvalidParameterError extends HttpError {
  constructor(invalidObjectName: string, errors?: ValidationError[], details?: object) {
    const message = `Invalid parameter or body field: "${invalidObjectName}"`;

    super(message, HttpCode.Client.BAD_REQUEST, {
      errors: Array.isArray(errors)
        ? flattenErrors(errors).map(error => ({
            value: error.value,
            constraints: error.constraints,
            property: error.property,
          }))
        : undefined,
      ...details,
    });
  }
}
