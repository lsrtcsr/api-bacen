import { HttpError, HttpCode } from 'ts-framework';

export class ForbiddenRequestError extends HttpError {
  constructor(msg?: string, details?: any) {
    super(msg || 'Forbidden, insufficient permission to perform this action', HttpCode.Client.FORBIDDEN, {
      ...details,
    });
  }
}
