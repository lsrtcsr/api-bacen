import { HttpError, HttpCode } from 'ts-framework';

export class InvalidRequestError extends HttpError {
  constructor(reason: string, details?: any) {
    super(reason, HttpCode.Client.BAD_REQUEST, { ...details });
  }
}
