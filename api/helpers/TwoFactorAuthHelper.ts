import * as Speakeasy from 'speakeasy';
import * as Package from '../../package.json';

export const DEFAULT_2FA_STEP = 30; // seconds
export const DEFAULT_2FA_ENCODING: Speakeasy.Encoding = 'base32'; // seconds

export class TwoFactorHelper {
  /**
   * Generate a secret for OTP generation.
   *
   * @param options The secret generation options
   */
  public static generateSecretSeed(options?: Speakeasy.GenerateSecretOptions): Speakeasy.GeneratedSecret {
    return Speakeasy.generateSecret({ issuer: Package.name, ...options });
  }

  /**
   * Generates a new token using the secret seed.
   *
   * @param secret The secret seed
   * @param encoding The encoding
   * @param digits
   */
  public static generateToken(
    secret: string,
    digits: number = 6,
    encoding: Speakeasy.Encoding = DEFAULT_2FA_ENCODING,
  ): string {
    // Generate a time-based token based on the base-32 key.
    return this.generateTokenFromDate(secret, undefined, digits, encoding);
  }

  /**
   * Generates a new token using the secret seed.
   *
   * @param secret The secret seed
   * @param encoding The encoding
   * @param digits
   */
  public static generateTokenFromDate(
    secret: string,
    date: Date,
    digits: number = 6,
    encoding: Speakeasy.Encoding = DEFAULT_2FA_ENCODING,
  ): string {
    // Generate a time-based token based on the base-32 key.
    return Speakeasy.totp({
      secret,
      encoding,
      digits,
      time: date ? date.getTime() : undefined,
    });
  }

  /**
   * Checks a token against a secret seed.
   *
   * @param token The input token to be verified
   * @param secret The secret seed to be verified against
   * @param window The window of the verification
   * @param encoding The secret seed encoding
   */
  public static verifyToken(
    token: string,
    secret: string,
    encoding: Speakeasy.Encoding = DEFAULT_2FA_ENCODING,
    window: number = 1,
    step: number = DEFAULT_2FA_STEP,
  ): boolean {
    return Speakeasy.totp.verify({ secret, encoding, window, step, token: token.replace(/ /g, '') });
  }
}
