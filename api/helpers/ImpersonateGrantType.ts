import { UserRole } from '@bacen/base-sdk';
import * as Bluebird from 'bluebird';
import * as InvalidArgumentError from 'oauth2-server/lib/errors/invalid-argument-error';
import * as InvalidRequestError from 'oauth2-server/lib/errors/invalid-request-error';
import * as AbstractGrantType from 'oauth2-server/lib/grant-types/abstract-grant-type';
import { MoreThan } from 'typeorm';
import { bacenRequest, OAuthAccessToken, User } from '../models';

export default class ImpersonateGrantType extends AbstractGrantType {
  public async handle(request: bacenRequest, oAuthClient) {
    if (!request) {
      throw new InvalidArgumentError('Missing parameter: `request`');
    }
    if (!oAuthClient) {
      throw new InvalidArgumentError('Missing parameter: `oAuthClient`');
    }
    if (!request.body.access_token) {
      throw new InvalidRequestError('Missing parameter: `access_token`');
    }
    if (!request.body.user_id) {
      throw new InvalidRequestError('Missing parameter: `user_id`');
    }

    // Gets user instance from database
    const userClient = await this.getUser(request.body.access_token, request.body.user_id);
    return this.saveToken(userClient, oAuthClient, undefined);
  }

  /**
   * Gets the user instance to be impersonated, validating the impersonator permissions.
   *
   * @param accessToken The access token of the impersonator
   * @param userId The user id to be impersonated
   */
  public async getUser(accessToken: string, userId: string) {
    // Tries to get the impersonator user from database
    const oauth = await OAuthAccessToken.findOne<OAuthAccessToken>({
      where: { accessToken, expires: MoreThan(new Date()) },
    });

    if (!oauth) {
      throw new InvalidRequestError('Unauthorized access token');
    }

    let user;
    const impersonator = await User.safeFindOne({ where: { id: oauth.user.id } });

    if (!impersonator) {
      throw new InvalidRequestError('Unauthorized access token: impersonator not found');
    }

    if (impersonator.role === UserRole.ADMIN) {
      // Impersonate any user using an Root access token
      user = await User.safeFindOne({ where: { id: userId } });
    } else {
      throw new InvalidRequestError(`Unauthorized user role: cannot use this Grant Type"`);
    }

    if (!user) {
      throw new InvalidRequestError(`Unauthorized user access: forbidden`);
    } else {
      return user;
    }
  }

  public async saveToken(user, oAuthClient, scope) {
    const fns = [
      (this as any).validateScope(user, oAuthClient, scope),
      (this as any).generateAccessToken(oAuthClient, user, scope),
      (this as any).generateRefreshToken(oAuthClient, user, scope),
      (this as any).getAccessTokenExpiresAt(),
      (this as any).getRefreshTokenExpiresAt(),
    ];

    return Bluebird.all(fns)
      .bind(this)
      .spread(function(scope, accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) {
        const token = {
          accessToken,
          accessTokenExpiresAt,
          refreshToken,
          refreshTokenExpiresAt,
          scope,
        };
        return this.model.saveToken(token, oAuthClient, user);
      });
  }
}
