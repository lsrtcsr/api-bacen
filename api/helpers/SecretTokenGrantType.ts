import * as Bluebird from 'bluebird';
import * as InvalidArgumentError from 'oauth2-server/lib/errors/invalid-argument-error';
import * as InvalidRequestError from 'oauth2-server/lib/errors/invalid-request-error';
import * as AbstractGrantType from 'oauth2-server/lib/grant-types/abstract-grant-type';
import { OAuthSecretToken, bacenRequest } from '../models';

export default class SecretTokenGrantType extends AbstractGrantType {
  public async handle(request: bacenRequest, oAuthClient) {
    if (!request) {
      throw new InvalidArgumentError('Missing parameter: `request`');
    }
    if (!oAuthClient) {
      throw new InvalidArgumentError('Missing parameter: `oAuthClient`');
    }
    if (!request.body.secret_token) {
      throw new InvalidRequestError('Missing parameter: `secret_token`');
    }

    // Gets secret token instance from database
    const secretToken = await this.getSecret(request.body.secret_token);
    return this.saveToken(secretToken.user, oAuthClient, undefined);
  }

  public async getSecret(secretToken: string) {
    const secret = await OAuthSecretToken.findBySecret(secretToken);

    if (!secret) {
      throw new InvalidRequestError('Unauthorized secret token');
    }

    return secret;
  }

  public async saveToken(user, oAuthClient, scope) {
    const fns = [
      (this as any).validateScope(user, oAuthClient, scope),
      (this as any).generateAccessToken(oAuthClient, user, scope),
      (this as any).generateRefreshToken(oAuthClient, user, scope),
      (this as any).getAccessTokenExpiresAt(),
      (this as any).getRefreshTokenExpiresAt(),
    ];

    return Bluebird.all(fns)
      .bind(this)
      .spread(function(scope, accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) {
        const token = {
          accessToken,
          accessTokenExpiresAt,
          refreshToken,
          refreshTokenExpiresAt,
          scope,
        };
        return this.model.saveToken(token, oAuthClient, user);
      });
  }
}
