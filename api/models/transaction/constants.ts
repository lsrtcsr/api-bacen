import { TransactionStatus } from '@bacen/base-sdk';

export const TRANSACTION_BLOCKED_BALANCE_STATES = [
  TransactionStatus.PENDING,
  TransactionStatus.AUTHORIZED,
  TransactionStatus.ACCEPTED,
];

export const TRANSACTION_REVERSIBLE_STATES = [TransactionStatus.AUTHORIZED, TransactionStatus.ACCEPTED];

export const TRANSACTION_REFUNDABLE_STATES = [TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED];

export const TRANSACTION_SUCCESS_STATES = [TransactionStatus.EXECUTED, TransactionStatus.NOTIFIED];

export const TRANSACTION_FINAL_STATES = [
  TransactionStatus.FAILED,
  TransactionStatus.NOTIFIED,
  TransactionStatus.REVERSED,
];

export const TRANSACTION_NON_CANCELABLE_STATES = [
  TransactionStatus.FAILED,
  TransactionStatus.EXECUTED,
  TransactionStatus.NOTIFIED,
];
