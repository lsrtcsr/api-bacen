import { Entity, ManyToOne, Column, getManager, Index } from 'typeorm';
import { TransactionTypeFriendly as Base2TransactionType, Base2Record } from '@bacen/base-sdk';
import { ExtendedEntity, FindByOptions } from '../base';
import { Wallet } from '../wallet/Wallet';
import { Asset } from '../asset';

export enum Base2Action {
  NONE = 'none',
  TRANSACTION_CREATED = 'transaction_created',
  TRANSACTION_REVERSED = 'transaction_reversed',
  TRANSACTION_EXECUTED = 'transaction_executed',
  REVERSAL_EXECUTED = 'reversal_executed',
  REVERSAL_UNDONE = 'reversal_undone',
  AMOUNT_ADJUSTED = 'amount_adjusted',
}

export type Base2RelatedTransactionsType = 'original' | 'created' | 'reversal' | 'confirmation' | 'adjustment';
export type Base2RelatedTransactions = Record<Base2RelatedTransactionsType, string>;
export interface Base2HandledTransactionAdditionalData {
  raw: Base2Record;
  transactions: Record<Base2RelatedTransactionsType, string>;
}

@Entity(Base2HandledTransaction.tableName)
export class Base2HandledTransaction extends ExtendedEntity {
  private static readonly tableName = 'base2_handled_transactions';

  @Column('enum', { nullable: false, enum: Base2TransactionType })
  public type: Base2TransactionType;

  @Column('enum', { nullable: true, enum: Base2Action })
  public action?: Base2Action;

  @ManyToOne(_ => Wallet, { nullable: true })
  public wallet?: Wallet;

  @ManyToOne(_ => Asset, { nullable: true })
  public asset?: Asset;

  @Index({ unique: true })
  @Column('uuid', { nullable: false })
  public externalId: string;

  @Column('decimal', { nullable: false })
  public originalAmount: string;

  @Column('decimal', { nullable: false })
  public confirmedAmount: string;

  @Column('jsonb', { nullable: false, default: {} })
  public additionalData: Base2HandledTransactionAdditionalData;

  public static findByExternalId(externalId: string, options: FindByOptions = {}): Promise<Base2HandledTransaction> {
    const { relations = [], manager = getManager() } = options;
    const baseQuery = manager
      .createQueryBuilder(Base2HandledTransaction, 'base2HandledTransaction')
      .where('"base2HandledTransaction"."externalId" = :externalId', { externalId });

    for (const relation of relations) {
      baseQuery.leftJoinAndSelect(`base2HandledTransaction.${relation}`, relation);
    }

    return baseQuery.getOne();
  }

  public static async existsByExternalId(externalId: string, manager = getManager()): Promise<boolean> {
    const count = await manager
      .createQueryBuilder(Base2HandledTransaction, 'base2HandledTransaction')
      .where('"base2HandledTransaction"."externalId" = :externalId', { externalId })
      .getCount();

    return count > 0;
  }

  // TODO: make Base2ResponseSchema shared by moving it to base-sdk
  public toInterchangeFormat(): object {
    return {
      externalId: this.externalId,
      action: this.action,
       Transactions: this.additionalData.transactions,
    };
  }

  public toSimpleJSON() {
    return {
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      type: this.type,
      action: this.action,
      originalAmount: this.originalAmount,
      confirmedAmount: this.confirmedAmount,
      additionalData: this.additionalData,
    };
  }

  public toJSON() {
    return {
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      type: this.type,
      action: this.action,
      originalAmount: this.originalAmount,
      confirmedAmount: this.confirmedAmount,
      wallet: this.wallet,
      additionalData: this.additionalData,
    };
  }
}
