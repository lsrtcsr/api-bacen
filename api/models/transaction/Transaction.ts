import {
  CustodyFeature,
  CustodyProvider,
  Pagination,
  PaymentStatus,
  PaymentType,
  TransactionAdditionalData,
  TransactionSchema,
  TransactionStatus,
  TransactionType,
} from '@bacen/base-sdk';
import { BaseError, Logger, LoggerInstance } from 'ts-framework-common';
import {
  Brackets,
  Column,
  DeepPartial,
  Entity,
  EntityManager,
  getManager,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Index,
} from 'typeorm';
import { ForbiddenRequestError } from '../../errors';
import { TransactionStateMachine } from '../../fsm';
import { ProviderManagerService } from '../../services';
import { ProviderUtil } from '../../utils';
import { Asset } from '../asset';
import { ExtendedEntity } from '../base';
import { OAuthAccessToken } from '../oauth2';
import { Payment } from '../payment';
import { Wallet } from '../wallet/Wallet';
import { TransactionStateItem } from './TransactionState';
import Config from '../../../config';
import { Banking } from '../consumer';
import { TRANSACTION_SUCCESS_STATES } from './constants';

export interface PrepareTransactionOptions {
  source: Wallet;
  recipients: {
    wallet: Wallet;
    amount: string;
    asset: Asset;
    type?: PaymentType;
    status?: PaymentStatus;
    scheduleFor?: Date;
  }[];
  type: PaymentType;
  createdBy?: OAuthAccessToken;
  additionalData?: Transaction['additionalData'];
  manager?: EntityManager;
  banking?: Banking;
  bypassReversalCheck?: boolean;
}

export interface AuthorizeOnProviderOptions {
  providerName: CustodyProvider;
  logger?: LoggerInstance;
  manager?: EntityManager;
}

@Entity(Transaction.tableName)
export class Transaction extends ExtendedEntity implements TransactionSchema {
  static readonly tableName = 'transactions';

  @Column({ name: 'transaction_type', nullable: false, type: 'enum', enum: TransactionType })
  type: TransactionType;

  @Index()
  @ManyToOne((type) => Wallet, (wallet) => wallet.transactions, { nullable: false })
  source: Wallet;

  @OneToMany((type) => Payment, (payment) => payment.transaction)
  payments?: Payment[];

  @Index()
  @ManyToOne((type) => Banking, (banking) => banking.transactions)
  banking?: Banking;

  @OneToMany((type) => TransactionStateItem, (transactionStateItem) => transactionStateItem.transaction, {
    cascade: ['insert', 'update'],
  })
  states?: TransactionStateItem[];

  @JoinColumn({ name: 'created_by' })
  @ManyToOne((type) => OAuthAccessToken, { nullable: true, eager: false })
  createdBy: OAuthAccessToken;

  @Column({ type: 'jsonb', default: {}, name: 'additional_data' })
  additionalData?: TransactionAdditionalData;

  constructor(schema: Partial<Transaction>) {
    super(schema);
    Object.assign(this, schema);
  }

  get status() {
    const states = this.getStates();
    if (states) {
      return states[0].status;
    }
  }

  public getStates(): TransactionStateItem[] | undefined {
    if (this.states && this.states.length) {
      return this.states.sort((a, b) => {
        if (b.createdAt && a.createdAt) {
          return b.createdAt.getTime() - a.createdAt.getTime();
        }
        return 0;
      });
    }
  }

  get totalAmount(): { [key: string]: number } {
    const amounts = {};
    const payments = (this.payments || []).filter((payment) => payment.status !== PaymentStatus.FAILED);
    for (const payment of payments) {
      let code: string;

      if (!payment.asset) {
        Logger.getInstance().warn('Getting totalAmount without asset info, marking as unknown');
        code = 'unknown';
      } else {
        code = payment.asset.code;
      }

      if (!amounts[code]) {
        amounts[code] = 0;
      }

      amounts[code] += payment.amount;
    }

    return amounts;
  }

  public static getMany(
    pagination: Pagination,
    options: {
      filters?: { walletId?: string; sourceWalletId?: string; destinationWalletId?: string; type?: TransactionType };
      orderBy?: { [key in keyof Transaction]?: 'ASC' | 'DESC' };
      relations?: string[]; // Nested relations should be in order: ['a', 'a.b', 'a.b.c']
      userFilters?: { [key: string]: string | number };
    },
  ): Promise<[Transaction[], number]> {
    let query = Transaction.createQueryBuilder('transaction');
    let hasWhere = false;
    let hasSort = false;

    const { filters, userFilters } = options;

    const addWhere = (queryString: string | Brackets, parameters?: any): void => {
      if (hasWhere) {
        query = query.andWhere(queryString, parameters);
      } else {
        query = query.where(queryString, parameters);
        hasWhere = true;
      }
    };

    const addOrderBy = (key: string, order: 'ASC' | 'DESC'): void => {
      if (hasSort) {
        query = query.addOrderBy(key, order);
      } else {
        query = query.orderBy(key, order);
        hasSort = true;
      }
    };

    if (userFilters && Object.keys(userFilters).length > 0) {
      addWhere(
        new Brackets((qb) =>
          qb
            .where('transaction.additionalData @> :additionalData', { additionalData: { userFilters } })
            .orWhere('transaction.additionalData @> :additionalData', { userDefined: { userDefined: userFilters } }),
        ),
      );
    }

    if (options.relations) {
      for (const relation of options.relations) {
        if (relation.includes('.')) {
          const parts = relation.split('.');
          const [parent, child] = [parts[parts.length - 2], parts[parts.length - 1]];
          query = query.innerJoinAndSelect(`${parent}.${child}`, relation);
        } else {
          query = query.leftJoinAndSelect(`transaction.${relation}`, relation);
        }
      }
    }

    if (filters) {
      const { walletId, destinationWalletId, sourceWalletId, type } = filters;
      if (walletId) {
        addWhere(
          new Brackets((qb) =>
            qb
              .where('transaction.source = :walletId', { walletId })
              .orWhere('payments.destination = :walletId', { walletId }),
          ),
        );
      } else {
        if (destinationWalletId) {
          addWhere('transaction.source = :destinationWalletId', {
            destinationWalletId,
          });
        }

        if (sourceWalletId) {
          if (!options.relations.includes('payments')) {
            query = query.leftJoinAndSelect('transaction.payments', 'payments');
          }
          addWhere('payments.destination = :sourceWalletId', {
            sourceWalletId,
          });
        }
      }

      if (type) {
        addWhere('transaction.type = :type', { type });
      }
    }

    if (options.orderBy) {
      for (const orderBy of Object.keys(options.orderBy)) {
        addOrderBy(`transaction.${orderBy}`, options.orderBy[orderBy]);
      }
    }

    const results = query.skip(pagination.skip).take(pagination.limit).getMany();
    const count = query.getCount();

    return Promise.all([results, count]);
  }

  // tslint:disable-next-line:max-line-length
  public static async prepare(options: PrepareTransactionOptions, authorizeProvider = true): Promise<Transaction> {
    const logger = Logger.getInstance();
    const { source, recipients, type } = options;

    const providers = new Set(options.recipients.map((recipient) => recipient.asset.provider));

    const manager = options.manager || getManager();

    // prevents it from being done in duplicate
    if (
      !options.bypassReversalCheck &&
      options.type === PaymentType.TRANSACTION_REVERSAL &&
      options.additionalData &&
      options.additionalData.originalTransaction
    ) {
      // Try to find already reversed transaction to prevent duplicate refunds
      const alreadyReversed = await manager
        .createQueryBuilder(Transaction, 'transaction')
        .where("transaction.additional_data->>'reversal' = :reversal", { reversal: true })
        .andWhere("transaction.additional_data->>'originalTransaction' = :originalTransaction", {
          originalTransaction: options.additionalData.originalTransaction,
        })
        .getCount();

      if (alreadyReversed > 0) {
        throw new ForbiddenRequestError('Transaction already reversed', {
          id: options.additionalData.originalTransaction,
        });
      }
    }

    const transaction = Transaction.create({
      source,
      createdBy: options.createdBy,
      type: TransactionType.PAYMENT,
      additionalData: options.additionalData,
      banking: options.banking,
    });
    await manager.insert(Transaction, transaction);

    // Save pending transaction in database
    const initialState = TransactionStateItem.create({
      transaction,
      status: TransactionStatus.PENDING,
    });

    await manager.insert(TransactionStateItem, initialState);
    transaction.states = transaction.states?.length ? transaction.states : [];
    transaction.states.unshift(initialState);

    // Save pending payment in database
    const paymentSchemas = recipients.map((recipient) =>
      Payment.create({
        transaction,
        type: recipient.type || type,
        amount: recipient.amount,
        asset: recipient.asset,
        destination: recipient.wallet,
        scheduleFor: recipient.scheduleFor,
        status: recipient.status || PaymentStatus.AUTHORIZED,
      }),
    );

    if (paymentSchemas.length) {
      await manager.insert(Payment, paymentSchemas);
      transaction.payments = transaction.payments?.length ? transaction.payments : [];
      transaction.payments.push(...paymentSchemas);
    }

    const authorizationResults = { authorized: [], skipped: [] };
    for (const payment of transaction.payments) {
      if (payment.asset.provider && !authorizeProvider) {
        logger.warn("Skipping provider authorization, this might be dangerous, be sure to know what you're doing");
        authorizationResults.skipped.push({
          provider: payment.asset.provider,
          asset: payment.asset.code,
          payment: payment.id,
        });
      } else if (!payment.asset.provider) {
        logger.debug(`No authorization needed for asset "${payment.asset.code}"`);
        payment.status = PaymentStatus.SETTLED;
        authorizationResults.authorized.push({
          // we assume that when there is no provider the   is the authorizer
          asset: payment.asset.code,
          payment: payment.id,
        });
      }
    }

    if (authorizationResults.authorized.length > 0) {
      // TODO: postpone this query to update all settled payments (including those authorized on provider) in a single query
      await manager.update(
        Payment,
        authorizationResults.authorized.map((auth) => auth.payment),
        { status: PaymentStatus.SETTLED },
      );
    }

    const providerName = providers.values().next().value as CustodyProvider;

    // We do not control the source-of-truth for this provider's balance so we execute the provider-side financial
    // flow this instant.
    if (!Config.asset.controlledBalanceProviders.includes(providerName)) {
      const providerAuthorizedTransactions = await transaction.authorizeOnProvider({ providerName, logger, manager });
      authorizationResults.authorized.push(...providerAuthorizedTransactions);
    }

    // Save pending transaction in database
    const authorizedState = TransactionStateItem.create({
      transaction,
      status: TransactionStatus.AUTHORIZED,
      additionalData: { ...authorizationResults },
    });
    await manager.insert(TransactionStateItem, authorizedState);
    transaction.states.unshift(authorizedState);

    return transaction;
  }

  public async authorizeOnProvider({
    providerName,
    logger = Logger.getInstance(),
    manager = getManager(),
  }: AuthorizeOnProviderOptions) {
    let paymentUpdatePromises = [];

    const internalTransfers = this.payments.filter(
      (payment) => payment.isInternalPayment() && payment.status === PaymentStatus.AUTHORIZED && payment.asset.provider,
    );
    const transfersWithNoProvider = this.payments.filter(
      (payment) => payment.status === PaymentStatus.AUTHORIZED && !payment.asset.provider,
    );

    const authorizationResults = [];

    if (internalTransfers.length > 0) {
      providerName = internalTransfers[0].asset?.provider;
      internalTransfers.forEach((payment) => {
        authorizationResults.push({
          provider: payment.asset.provider,
          asset: payment.asset.code,
          payment: payment.id,
        });
      });

      const provider = ProviderManagerService.getInstance();

      logger.debug(`Requesting payment in external provider "${providerName}"`);

      const p2p = provider.from(providerName).feature(CustodyFeature.PAYMENT);
      await ProviderUtil.throwIfFeatureNotAvailable(p2p);
      const response = await p2p.payment(
        internalTransfers.map((payment) =>
          Payment.create({
            ...payment,
            transaction: {
              source: this.source,
              id: this.id,
              type: TransactionType.PAYMENT,
            } as any,
          }),
        ),
      );

      const withExternalProvider = response
        .filter((paymentResponse) => paymentResponse.status === PaymentStatus.SETTLED)
        .map((settledPayment) => manager.update(Payment, settledPayment.id, { status: PaymentStatus.SETTLED }));

      paymentUpdatePromises = paymentUpdatePromises.concat(withExternalProvider);

      logger.info(`Payment successfully authorized in external provider "${providerName}"`, { ...response });
    }

    if (transfersWithNoProvider.length > 0) {
      const withoutExternalProvider = transfersWithNoProvider.map((settledPayment) =>
        manager.update(Payment, settledPayment.id, { status: PaymentStatus.SETTLED }),
      );

      paymentUpdatePromises = paymentUpdatePromises.concat(withoutExternalProvider);
    }

    await Promise.all(paymentUpdatePromises);

    return authorizationResults;
  }

  public static async recordFailedTransaction(options: PrepareTransactionOptions): Promise<Transaction> {
    const { source, recipients, type } = options;

    const providers = options.recipients
      .map((recipient) => recipient.asset.provider)
      .reduce((accumulator, current) => (current ? accumulator.add(current) : accumulator), new Set());

    if (providers.size > 1) {
      throw new BaseError('All assets in a transaction should be associated with a single provider', { providers });
    }

    const manager = options.manager || getManager();

    const transaction = await Transaction.insertAndFind(
      {
        source,
        type: TransactionType.PAYMENT,
        createdBy: options.createdBy,
        additionalData: options.additionalData,
      },
      { manager },
    );

    await TransactionStateItem.insertAndFind(
      {
        transaction: { id: transaction.id },
        status: TransactionStatus.FAILED,
      },
      { manager },
    );

    // Save pending payment in database
    const transientPayments = recipients.map((recipient) =>
      Payment.create({
        type,
        amount: recipient.amount,
        asset: recipient.asset,
        destination: recipient.wallet,
        status: PaymentStatus.FAILED,
        transaction: { id: transaction.id },
      }),
    );
    await manager.insert(Payment, transientPayments);

    return manager.findOne(Transaction, {
      where: { id: transaction.id },
      relations: ['source', 'states', 'payments', 'payments.asset', 'payments.destination'],
    });
  }

  public static async getIdsByStatus(status: TransactionStatus[]): Promise<string[]> {
    //   select "transactionId", status
    //   from transaction_states
    //       where created_at in  (select MAX(transaction_states.created_at)
    //    from transaction_states
    //    group by transaction_states."transactionId") and
    //  transaction_states.status = 'AUTHORIZED';
    const transactionsWithRequiredStatus = await TransactionStateItem.createQueryBuilder('transactionstate')
      .select('distinct transactionstate.transaction, transactionstate.status')
      .where((qb) => {
        const subQuery = qb
          .subQuery()
          .select('MAX(transaction_states.created_at)')
          .from(TransactionStateItem, 'transaction_states')
          .groupBy('transaction_states.transaction')
          .getQuery();
        return `transactionstate.created_at IN ${subQuery}`;
      })
      .andWhere('transactionstate.status IN (:...statuses)', { statuses: status.map((status) => status.valueOf()) })
      .getRawMany();

    const transactionIds: string[] = transactionsWithRequiredStatus.map((transactionStatus) => {
      return transactionStatus.transactionId;
    });

    if (typeof transactionIds === 'undefined' || transactionIds.length === 0) {
      return [];
    }

    return transactionIds;
  }

  public async manuallyFailTransaction(data?: any): Promise<Transaction> {
    const fsm = new TransactionStateMachine(this);
    await fsm.goTo(TransactionStatus.FAILED, data);

    return Transaction.findOne(this.id, {
      relations: [
        'states',
        'source',
        'source.user',
        'payments',
        'payments.asset',
        'payments.destination',
        'payments.destination.user',
      ],
    });
  }

  /**
   * Ensures the source wallet and all of the destination wallets have trustlines to all assets beign exchanged
   * in the transaction.
   * @param manager The entity manager to run the operation on
   */
  public async ensureWalletTrustlines(manager = getManager()): Promise<void> {
    const assets = new Set(this.payments.map((payment) => payment.asset));
    const wallets = new Set(this.payments.map((payment) => payment.destination));

    for (const asset of assets) {
      for (const destinationWallet of wallets) {
        await destinationWallet.ensureTrustline(asset, manager);
      }

      await this.source.ensureTrustline(asset, manager);
    }
  }

  public static async getByStellarTransactionHash(hash: string): Promise<Transaction> {
    const statuses = TRANSACTION_SUCCESS_STATES;
    return Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.destination', 'destination')
      .where('transactionstate.status IN (:...statuses)', { statuses })
      .andWhere("transaction.additional_data->>'hash' = :hash", { hash })
      .getOne();
  }

  public static findByExternalId(
    externalId: string,
    manager: EntityManager = getManager(),
  ): Promise<Transaction | undefined> {
    return manager
      .createQueryBuilder(Transaction, 'transaction')
      .innerJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.states', 'states')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.asset', 'asset')
      .leftJoinAndSelect('payments.destination', 'destination')
      .where(`transaction."additional_data"->>'externalId' = :externalId `, { externalId })
      .getOne();
  }

  public static findTransactionReversal(
    transaction: Transaction,
    manager: EntityManager = getManager(),
  ): Promise<Transaction | undefined> {
    return manager
      .createQueryBuilder(Transaction, 'transaction')
      .innerJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.states', 'states')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .where(`transaction."additional_data"->>'originalTransaction' = :hash`, { hash: transaction.additionalData.hash })
      .getOne();
  }

  public toJSON(): DeepPartial<Transaction> {
    return {
      id: this.id,
      type: this.type,
      status: this.status,
      additionalData: this.additionalData,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      states: this.states && this.states.map((state) => state.toJSON()),
      source: this.source && this.source.toJSON ? this.source.toJSON() : this.source,
      payments: this.payments && this.payments.map((payment) => payment.toJSON()),
      banking: this.banking && this.banking.toJSON ? this.banking.toJSON() : this.banking,
    };
  }
}
