import {
  ConsumerStatus,
  Pagination,
  PaginationData,
  TransactionAdditionalData,
  TransactionStatus,
  TransactionType,
} from '@bacen/base-sdk';
import { FindConditions, FindManyOptions, getManager, In, SelectQueryBuilder, ViewColumn, ViewEntity } from 'typeorm';
import { User } from '../../user';

@ViewEntity({
  expression: `
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"
      FROM
          transaction_states
      ORDER BY
          "transactionId", created_at DESC
    )
    SELECT  transactions.id as "id",
            transactions.transaction_type as "type",
            latest_states.status,
            latest_states."additionalData"
    FROM transactions
    JOIN latest_states on latest_states."transactionId" = transactions.id
    ORDER BY latest_states.created_at DESC
    `,
})
export class ViewTransactionWithStates {
  @ViewColumn()
  id: string;

  @ViewColumn()
  type: TransactionType;

  @ViewColumn()
  status: TransactionStatus;

  @ViewColumn()
  additionalData: TransactionAdditionalData;

  public static async find(options?: FindManyOptions<ViewTransactionWithStates>): Promise<ViewTransactionWithStates[]> {
    return getManager().find(ViewTransactionWithStates, options);
  }
}
