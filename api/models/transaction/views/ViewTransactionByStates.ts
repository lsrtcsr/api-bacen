import { ViewEntity, ViewColumn, getManager } from 'typeorm';
import { TransactionStatus } from '@bacen/base-sdk';

@ViewEntity({
  expression: `
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("transactionId") "transactionId", "status", created_at, "additionalData"
      FROM
      transaction_states
      ORDER BY
          "transactionId", created_at DESC
    )
    SELECT latest_states.status,
            COUNT(transactions.id) as count
    FROM transactions
    JOIN latest_states on latest_states."transactionId" = transactions.id
    WHERE transactions.deleted_at is null
    GROUP BY latest_states.status
  `,
})
export class ViewTransactionByStates {
  @ViewColumn()
  status: TransactionStatus;

  @ViewColumn()
  count: number;

  public static async find(): Promise<ViewTransactionByStates[] | undefined> {
    return getManager().find(ViewTransactionByStates);
  }
}
