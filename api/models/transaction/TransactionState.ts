import { TransactionStateSchema, TransactionStatus } from '@bacen/base-sdk';
import { Column, DeepPartial, Entity, Index, ManyToOne } from 'typeorm';
import { ExtendedEntity } from '../base';
import { Transaction } from './Transaction';

@Entity(TransactionStateItem.tableName)
@Index('transaction_state_status_unique', ['transaction', 'status'], { unique: true })
export class TransactionStateItem extends ExtendedEntity implements TransactionStateSchema {
  private static readonly tableName = 'transaction_states';

  @Column('enum', { nullable: false, enum: TransactionStatus })
  status: TransactionStatus;

  @Index()
  @ManyToOne(
    type => Transaction,
    transaction => transaction.states,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  transaction?: Transaction;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<TransactionStateItem> = {}) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<TransactionStateItem> {
    return {
      id: this.id,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      additionalData: this.additionalData,
    };
  }
}
