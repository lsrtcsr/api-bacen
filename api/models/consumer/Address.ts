import { AddressSchema, AddressStatus, AddressType, UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import * as CEP from 'cep-promise';
import { IsEnum, IsNotEmpty, IsNumberString, IsOptional, Length } from 'class-validator';
import { Logger } from 'ts-framework-common';
import { Column, DeepPartial, Entity, Index, ManyToOne } from 'typeorm';
import { InvalidParameterError, InvalidRequestError } from '../../errors';
import { ExtendedEntity } from '../base';
import { Consumer } from './Consumer';

export const MAX_ADDRESS_LENGTH = 30;

@Entity(Address.tableName)
@Index(['consumer', 'default'], { unique: true, where: 'deleted_at IS NULL AND "default" = true' })
export class Address extends ExtendedEntity implements AddressSchema {
  private static readonly tableName = 'adressess';

  @IsNotEmpty({ always: true })
  @Column({ nullable: false, default: AddressType.HOME, type: 'enum', enum: AddressType })
  type: AddressType = AddressType.HOME;

  @ManyToOne(
    type => Consumer,
    consumer => consumer.addresses,
    { onDelete: 'CASCADE' },
  )
  consumer: Consumer;

  @Length(0, MAX_ADDRESS_LENGTH, { always: true })
  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  country: string;

  @Length(2, 2, { always: true })
  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  state: string;

  @Length(0, MAX_ADDRESS_LENGTH, { always: true })
  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  city: string;

  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  code: string;

  @Length(0, MAX_ADDRESS_LENGTH, { always: true })
  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  neighborhood: string;

  @Length(0, 30, { always: true })
  @IsNotEmpty({ always: true })
  @Column('text', { nullable: false })
  street: string;

  @Length(0, MAX_ADDRESS_LENGTH, { always: true })
  @IsOptional({ always: true })
  @Column('text', { nullable: true })
  complement?: string;

  @IsNotEmpty({ always: true })
  @IsNumberString({ always: true })
  @Column('text', { nullable: true })
  number: string;

  @Length(0, MAX_ADDRESS_LENGTH, { always: true })
  @IsOptional({ always: true })
  @Column('text', { nullable: true })
  reference?: string;

  @IsOptional({ always: true })
  @Column('boolean', { nullable: true })
  default: boolean;

  @IsNotEmpty()
  @IsEnum(AddressStatus)
  @Column({ nullable: false, enum: AddressStatus, default: AddressStatus.OWN })
  status: AddressStatus = AddressStatus.OWN;

  constructor(data: Partial<Address> = {}) {
    super(data);
  }

  public static async fromCEP(
    cep: string,
    partial: DeepPartial<Address> = {},
    validate: boolean = true,
  ): Promise<Address> {
    // TODO: Use CDT cep service as a fallback
    if (cep.length !== 8) {
      throw new InvalidParameterError(
        `The supplied zip code ${cep} is invalid: it must contain exactly eight characters`,
      );
    }

    let addressFromCEP: any = {};

    if (!UnleashUtil.isEnabled(UnleashFlags.SKIP_ADDRESS_CODE_VERIFICATION)) {
      try {
        addressFromCEP = await CEP(cep);
      } catch (exception) {
        Logger.getInstance().error('Failed to get address from CEP', exception);
        throw new InvalidParameterError(`The supplied zip code ${cep} is invalid or it was not entered correctly`);
      }
    }

    const address = Address.create({
      ...partial,
      // Prefer basic reference from bureau
      state: addressFromCEP?.state?.slice(0, 2) || partial.state,
      city: addressFromCEP?.city?.slice(0, MAX_ADDRESS_LENGTH) || partial.city,
      // Prefer address specific references from input value
      street: partial.street || addressFromCEP?.street?.slice(0, MAX_ADDRESS_LENGTH),
      neighborhood: partial.neighborhood || addressFromCEP?.neighborhood?.slice(0, MAX_ADDRESS_LENGTH),
      country: addressFromCEP?.country?.slice(0, MAX_ADDRESS_LENGTH) || partial.country || 'BR',
    });

    if (validate) {
      const errors = await address.validate();
      if (errors.length) {
        throw new InvalidRequestError('The supplied address is not valid', { errors });
      }
    }

    return address;
  }

  public toSimpleJSON(): Partial<Address> {
    return {
      country: this.country,
      state: this.state,
      city: this.city,
      code: this.code,
      street: this.street,
      reference: this.reference,
      complement: this.complement,
      neighborhood: this.neighborhood,
      number: this.number,
      id: this.id,
      default: this.default,
      status: this.status,
    };
  }

  public toJSON(): Partial<Address> {
    return {
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      street: this.street,
      number: this.number,
      complement: this.complement,
      neighborhood: this.neighborhood,
      code: this.code,
      city: this.city,
      state: this.state,
      country: this.country,
      reference: this.reference,
      default: this.default,
      status: this.status,
      id: this.id,
    };
  }
}
