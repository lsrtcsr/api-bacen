import { ConsumerStateSchema, ConsumerStatus } from '@bacen/base-sdk';
import { Column, Entity, ManyToOne } from 'typeorm';
import { ExtendedStateEntity } from '../base';
import { Consumer } from './Consumer';

@Entity(ConsumerState.tableName)
export class ConsumerState extends ExtendedStateEntity<ConsumerStatus> implements ConsumerStateSchema {
  private static readonly tableName = 'consumer_states';

  @Column('enum', { nullable: false, enum: ConsumerStatus })
  status: ConsumerStatus;

  @ManyToOne(type => Consumer, consumer => consumer.states, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  consumer: Consumer;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<ConsumerState> = {}) {
    super(data);
    Object.assign(this, data);
  }
}
