import { LegalTermSchema, LegalTermType, CustodyProvider, LegalTermAcceptanceConsumerData } from '@bacen/base-sdk';
import { Column, Entity, OneToMany, AfterLoad, getManager } from 'typeorm';
import { BaseError } from 'nano-errors';
import { ExtendedEntity } from '../base';
import { LegalTermAcceptance } from './LegalTermAcceptance';
import { Consumer } from './Consumer';
import { User } from '../user';

@Entity(LegalTerm.tableName)
export class LegalTerm extends ExtendedEntity implements LegalTermSchema {
  private static readonly tableName = 'legal_terms';

  @Column('enum', { enum: LegalTermType })
  public type: LegalTermType;

  @Column()
  public name: string;

  @Column()
  public body: string;

  @Column({ nullable: true })
  public provider?: CustodyProvider;

  @OneToMany(
    () => LegalTermAcceptance,
    acceptance => acceptance.legalTerm,
  )
  public acceptances?: LegalTermAcceptance[];

  // Virtual property
  public active: boolean;

  @AfterLoad()
  protected async setActive() {
    const { createdAt: maxCreatedAt }: { createdAt: Date } = await LegalTerm.createQueryBuilder()
      .where('type = :type', { type: this.type })
      .select('MAX(created_at) as "createdAt"')
      .getRawOne();

    this.active = maxCreatedAt.getTime() === this.createdAt.getTime();
  }

  /**
   * Lists all the active terms. Active terms are the most recent terms of each type.
   */
  public static getActiveTerms(): Promise<LegalTerm[]> {
    return LegalTerm.createQueryBuilder('legal_term')
      .innerJoin(
        qb =>
          qb
            .from(LegalTerm, 'legal_term_by_type')
            .select('type, MAX(created_at) as created_at')
            .groupBy('1'),
        'latest_of_type',
        'latest_of_type.type=legal_term.type AND latest_of_type.created_at=legal_term.created_at',
      )
      .leftJoinAndSelect('legal_term.acceptances', 'acceptances')
      .leftJoinAndSelect('acceptances.consumer', 'accepting_consumers')
      .getMany();
  }

  /**
   * Gets the active term of a given type. The active term is the most recent term of each type.
   * @param type the type of the term.
   */
  public static getActiveTermByType(type?: LegalTermType): Promise<LegalTerm | undefined> {
    return LegalTerm.findOne({
      where: { type },
      order: { createdAt: 'DESC' },
    });
  }

  /**
   * Gets the list of all users which accepted the legalTerm
   */
  public getAcceptingConsumers(): Promise<User[]> {
    // we use innerJoins on consumer and acceptances bc we're not interested in users w/o a consumer or consumers w/o acceptances.
    return User.createQueryBuilder('user')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.states', 'user_states')
      .innerJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumer_states')
      .leftJoinAndSelect('user.wallets', 'wallets')
      .leftJoinAndSelect('wallets.states', 'wallet_states')
      .innerJoin('consumer.legalTermAcceptances', 'acceptances')
      .where(`acceptances."legalTermId" = :id`, { id: this.id })
      .getMany();
  }

  /**
   * Returns wheter or not a consumer has accepted the legalTerm
   * @param consumer The consumer instance.
   */
  public hasConsumerAcceptedSync(consumer: Consumer): boolean {
    if (!this.acceptances)
      throw new BaseError('hasConsumerAcceptedSync can only operate on terms where the acceptances field is populated');

    return this.acceptances.filter(acceptance => acceptance.consumer.id === consumer.id).length > 0;
  }

  /**
   * Accepts a LegalTerm for a given consumer.
   * @param consumer The consumer who is accepting the term
   * @param consumerInfo additional identifying info regarding the consumer
   */
  public async accept(
    consumer: Consumer,
    consumerInfo: LegalTermAcceptanceConsumerData,
    manager = getManager(),
  ): Promise<LegalTermAcceptance> {
    return LegalTermAcceptance.insertAndFind(
      {
        consumer,
        consumerInfo,
        legalTerm: this,
      },
      { manager },
    );
  }

  constructor(data: Partial<LegalTerm>) {
    super(data);
    Object.assign(this, data);
  }

  public toJSON() {
    return {
      id: this.id,
      type: this.type,
      active: this.active,
      name: this.name,
      body: this.body,
      provider: this.provider,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      deletedAt: this.deletedAt,
    };
  }
}
