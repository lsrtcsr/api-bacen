import { BaseError, Logger } from 'nano-errors';
import { DocumentSchema, DocumentSide, DocumentStatus, DocumentType, DocumentFileLink } from '@bacen/base-sdk';
import { TransferManager } from '@bacen/transport-sdk';
import {
  Column,
  DeepPartial,
  Entity,
  getManager,
  Index,
  IsNull,
  ManyToOne,
  OneToMany,
  AfterLoad,
  EntityManager,
} from 'typeorm';
import Config from '../../../config';
import { StorageService } from '../../services';
import { ExtendedStatefulEntity } from '../base';
import { Consumer } from './Consumer';
import { DocumentState } from './DocumentState';
import { User } from '../user';

export interface DocumentMetadata {
  [DocumentSide.FRONT]: any;
  [DocumentSide.BACK]: any;
  [DocumentSide.SELFIE]: any;
  [DocumentSide.BOTH_SIDES]: any;
}

@Entity(Document.tableName)
@Index(['consumer', 'type'], { where: 'deleted_at IS NULL' })
export class Document extends ExtendedStatefulEntity<DocumentStatus, DocumentState> implements DocumentSchema {
  private static readonly tableName = 'documents';

  @ManyToOne((type) => Consumer, (consumer) => consumer.documents, { cascade: ['insert', 'update'], nullable: false })
  consumer?: Consumer;

  @Column({ nullable: false, type: 'enum', enum: DocumentType })
  type!: DocumentType;

  @Column({ nullable: true })
  number?: string;

  @Column({ nullable: true })
  issuer?: string;

  @Column({ nullable: true })
  issuerState?: string;

  @Column('jsonb', { nullable: false, default: [] })
  identityIds!: string[];

  @Column('timestamp with time zone', { nullable: true })
  expiresAt?: Date;

  @OneToMany((type) => DocumentState, (documentState) => documentState.document, {
    cascade: ['insert', 'update'],
  })
  states?: DocumentState[];

  @Column({ nullable: true })
  ticketId?: string;

  @Column({ nullable: true })
  reason?: string;

  @Column({ nullable: false, default: true })
  isActive!: boolean;

  @Column('jsonb', { nullable: true })
  metadata?: DocumentMetadata;

  @Column('jsonb', { nullable: true })
  files?: DocumentFileLink[];

  constructor(data: Partial<Document> = {}) {
    super(data);
  }

  public static async findLastActive(user: User, number: string, type: DocumentType, manager = getManager()) {
    return manager.findOne(Document, {
      where: {
        type,
        consumer: user?.consumer?.id,
        deletedAt: IsNull(),
        isActive: true,
        number,
      },
    });
  }

  /**
   * Gets the generated name of the document side image.
   *
   * @param side The side of the document
   */
  public getImageFileName(side: string): string {
    if (!side) throw new BaseError('Document side is required');
    return `${this.id}${this.type}${side}${this.number || ''}`;
  }

  /**
   * Check if all required sides exists in the document storage service.
   */
  public async requiredSidesExist(): Promise<boolean> {
    const storageService = StorageService.getInstance();
    const requiredSides = Config.kyc.sidesByDocumentType.find((setting) => setting.type === this.type);

    let requiredSidesExist = false;
    for (const option of requiredSides.options) {
      const results: boolean[] = [];
      for (const side of option.sides) {
        results.push(await storageService.fileExists({ fileName: this.getImageFileName(side) }));
      }
      requiredSidesExist = results.reduce((result, current) => result && current, true);
      if (requiredSidesExist) break;
    }

    return requiredSidesExist;
  }

  /**
   * Returns the document sides already uploaded and persisted in the database.
   */
  public async existingSides(): Promise<DocumentSide[]> {
    const storageService = StorageService.getInstance();

    const results = [];
    for (const side of Object.values(DocumentSide)) {
      if (await storageService.fileExists({ fileName: this.getImageFileName(side) })) {
        results.push(side);
      }
    }

    return results;
  }

  public async generateDownloadableLinks(): Promise<void> {
    const sides = await this.existingSides();
    if (!sides.length) return;

    const transferManager = new TransferManager({ logger: Logger.getInstance() });

    const files: DocumentFileLink[] = [];
    for (const side of sides) {
      const url = await transferManager.getSignedUrl(
        {
          projectId: Config.googlecloud.projectId,
          file: this.getImageFileName(side),
          bucket: Config.googlecloud.documentPhotoStorage.bucketName,
        },
        Config.googlecloud.documentPhotoStorage.downloadableLinkExpirationTime,
      );

      files.push({ side, url });
    }
    await Document.update(this.id, { files });
  }

  /**
   * Updates the document metadata field.
   *
   * @param metadata The image metadata object
   * @param manager The typeorm entity manager
   */
  public async setMetadata(metadata: DocumentMetadata, manager: EntityManager = getManager()): Promise<Document> {
    return Document.updateAndFind(this.id, { metadata }, { manager });
  }

  public toSimpleJSON(): DeepPartial<Document> {
    return {
      id: this.id,
      type: this.type,
      number: this.number,
      status: this.status,
      isActive: this.isActive,
    };
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Document> {
    const states = this.getStates();

    return {
      updatedAt: states ? new Date(Math.max(this.updatedAt.getTime(), states[0].updatedAt.getTime())) : this.updatedAt,
      createdAt: this.createdAt,
      states: states ? states.map((state) => state.toJSON()) : states,
      number: this.number,
      status: this.status,
      type: this.type,
      id: this.id,
      isActive: this.isActive,
      files: this.files,
    };
  }
}
