import { PhoneSchema, PhoneType, UnleashFlags } from '@bacen/base-sdk';
import { IsNotEmpty, IsNumberString, IsOptional, Length } from 'class-validator';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import Config from '../../../config';
import { InvalidRequestError } from '../../errors';
import { TextService } from '../../services';
import { ExtendedEntity } from '../base';
import { OAuthAccessToken } from '../oauth2';
import { genPassword, verifyPassword } from '../user/helpers';
import { Consumer } from './Consumer';

import moment = require('moment');

export const DEFAULT_VERIFICATION_MAX_ATTEMPTS = 5;
export const DEFAULT_VERIFICATION_TTL_MS = 15 * 60 * 1000; // 15min

@Entity(Phone.tableName)
@Index(['consumer', 'default'], { unique: true, where: 'deleted_at IS NULL AND main_phone = true' })
export class Phone extends ExtendedEntity implements PhoneSchema {
  private static readonly tableName = 'phones';

  @Column({ nullable: false, default: PhoneType.MOBILE, type: 'enum', enum: PhoneType })
  type: PhoneType = PhoneType.MOBILE;

  @ManyToOne(
    type => Consumer,
    consumer => consumer.phones,
    { cascade: ['insert', 'update'] },
  )
  consumer: Consumer;

  @IsOptional({ always: true })
  @IsNumberString({ always: true })
  @Length(1, 3, { always: true })
  @Column({ nullable: false, default: '55' })
  countryCode: string;

  @IsNotEmpty({ always: false })
  @IsNumberString({ always: true })
  @Length(1, 3, { always: true })
  @Column({ length: 3, nullable: true })
  code: string;

  @IsNotEmpty({ always: true })
  @IsNumberString({ always: true })
  @Length(8, 9, { always: true })
  @Column('text', { nullable: false })
  number: string;

  @IsOptional({ always: true })
  @Column('text', { nullable: true })
  extension?: string;

  @IsOptional({ always: true })
  @Column('timestamp with time zone', { nullable: true })
  verifiedAt?: Date;

  @JoinColumn({ name: 'verified_by' })
  @IsOptional({ always: true })
  @ManyToOne(
    type => OAuthAccessToken,
    token => token.phoneVerifications,
  )
  verifiedBy?: OAuthAccessToken;

  @IsOptional()
  @Column('boolean', { name: 'main_phone', nullable: true })
  default: boolean;

  @IsOptional({ always: true })
  @Column({ nullable: true })
  verificationSalt: string;

  @IsOptional({ always: true })
  @Column({ nullable: true })
  verificationHash: string;

  @IsOptional({ always: true })
  @Column('timestamp with time zone', { nullable: true })
  generatedVerificationAt?: Date;

  @Column({ nullable: true, name: 'failed_verification_attempts', default: 0 })
  failedVerificationAttempts: number;

  public get fullNumber() {
    const hasCountryCode = this.countryCode && this.countryCode.length;
    if (hasCountryCode) {
      return `+${this.countryCode}${this.code || ''}${this.number}`;
    }

    return `${this.code || ''}${this.number}`;
  }

  /**
   * Finds default phone number based on user wallet id.
   *
   * @param walletId The wallet id from the phone number
   */
  public static async findDefaultByWalletId(walletId: string): Promise<Phone> {
    return Phone.createQueryBuilder('phone')
      .innerJoin('phone.consumer', 'consumer')
      .innerJoin('consumer.user', 'user')
      .innerJoin('user.wallets', 'wallets', 'wallets.id = :id', { id: walletId })
      .getOne();
  }

  /**
   * Get phone from string.
   *
   * @param fullnumber The full number of the phone number
   */
  public static getPhoneFromString(fullnumber: string): Phone {
    const phoneNumber = parsePhoneNumberFromString(fullnumber);

    return new Phone({
      countryCode: phoneNumber.countryCallingCode.toString(),
      number: phoneNumber.nationalNumber.toString(),
    });
  }

  constructor(data: Partial<Phone> = {}) {
    super(data);

    this.type = data.type;
    this.code = data.code;
    this.number = data.number;
    this.verifiedAt = data.verifiedAt;
    this.consumer = data.consumer;
    this.extension = data.extension;
  }

  /**
   * Validates if supplied verification matches the currently saved one.
   *
   * @param verification The verification code candidate that will be matched
   */
  public async validateVerificationHash(verification: string, verifiedBy?: OAuthAccessToken): Promise<boolean> {
    if (!verification || !this.verificationHash || !this.verificationSalt) {
      throw new InvalidRequestError('No phone verification hash available, you must generate and send a new one', {
        failedVerificationAttempts: this.failedVerificationAttempts,
      });
    }

    const shouldSKipMaxAttemps = UnleashUtil.isEnabled(UnleashFlags.SKIP_PHONE_VERIFICATION_MAX_ATTEMPTS);

    if (!shouldSKipMaxAttemps && this.failedVerificationAttempts >= +DEFAULT_VERIFICATION_MAX_ATTEMPTS) {
      throw new InvalidRequestError('Verification rate limit exceeded, current code was invalidated', {
        failedVerificationAttempts: this.failedVerificationAttempts,
      });
    }

    let result = true;
    const ttl = Config.otp.tokenTTL ? Config.otp.tokenTTL * 1000 : DEFAULT_VERIFICATION_TTL_MS;

    // Make sure token has not expired
    if (Date.now() > this.generatedVerificationAt.getTime() + ttl) {
      result = false;
    }

    // Validate token hash anyway, this is a security measure
    result = result && verifyPassword(verification, this.verificationHash);

    // Register the attempt, if failed
    if (result) {
      await Phone.createQueryBuilder()
        .update()
        .where({ id: this.id })
        .set({
          verifiedBy,
          verifiedAt: new Date(),
        })
        .execute();
    } else {
      await Phone.createQueryBuilder()
        .update()
        .where({ id: this.id })
        .set({ failedVerificationAttempts: () => 'failed_verification_attempts + 1' })
        .execute();

      this.failedVerificationAttempts++;

      throw new InvalidRequestError('Invalid or expired phone verification code', {
        failedVerificationAttempts: this.failedVerificationAttempts,
      });
    }

    return result;
  }

  /**
   * Send verification hash to user's phone.
   *
   */
  public async sendVerificationHash(): Promise<{ expiresIn: number; expiresAt: string; coolDownTime: number }> {
    // Make sure request limit was not exceeded
    if (this.verificationHash) {
      const minTimeToWait = Config.otp.coolDownTime * 1000;
      const timeSinceVerificationHash = Date.now() - this.generatedVerificationAt.getTime();

      if (timeSinceVerificationHash < minTimeToWait) {
        throw new InvalidRequestError('Verification code generation rate limit exceeded', {
          timeToWait: minTimeToWait - timeSinceVerificationHash,
        });
      }
    }

    const prefix = String.fromCharCode(Math.random() * (90 - 65) + 65);
    const code = Math.round(Math.random() * (999999 - 100000) + 100000);

    // Genrates code in A-123456 format
    const token = `${prefix}-${code}`;

    // Prepare verification hash
    const { salt, hash } = await genPassword(token);
    this.verificationSalt = salt;
    this.verificationHash = hash;

    // TODO: Put config in domain for custom prefix
    // Make sure SMS was sent before storing as verification
    await this.sms(`${Config.otp.smsBodyVerificationPrefix}${token}`);

    // Updates hash in phone instance
    await Phone.update(
      { id: this.id },
      {
        verificationHash: hash,
        verificationSalt: salt,
        failedVerificationAttempts: 0,
        generatedVerificationAt: new Date(),
      },
    );

    return {
      expiresIn: Config.otp.tokenTTL,
      expiresAt: moment()
        .add(+Config.otp.tokenTTL, 'seconds')
        .format(),
      coolDownTime: Config.otp.coolDownTime,
    };
  }

  /**
   * Sends an SMS message using Text Service.
   *
   * @param body The message body
   */
  public async sms(body: string) {
    return TextService.getInstance().send({
      from: Config.sms.gateway.from,
      to: this.fullNumber,
      text: body,
    });
  }

  /**
   * Exports simple phone JSON .
   */
  public toSimpleJSON(): Partial<Phone> {
    return {
      verifiedAt: this.verifiedAt,
      fullNumber: this.fullNumber,
      countryCode: this.countryCode,
      code: this.code,
      number: this.number,
      id: this.id,
    };
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): Partial<Phone> {
    return {
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      verifiedAt: this.verifiedAt,
      fullNumber: this.fullNumber,
      countryCode: this.countryCode,
      code: this.code,
      number: this.number,
      id: this.id,
      default: this.default,
    };
  }
}
