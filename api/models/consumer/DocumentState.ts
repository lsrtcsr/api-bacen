import { DocumentStateSchema, DocumentStatus } from '@bacen/base-sdk';
import { Column, Entity, ManyToOne } from 'typeorm';
import { ExtendedStateEntity } from '../base';
import { Document } from './Document';

@Entity(DocumentState.tableName)
export class DocumentState extends ExtendedStateEntity<DocumentStatus> implements DocumentStateSchema {
  private static readonly tableName = 'document_states';

  @Column('enum', { nullable: false, enum: DocumentStatus })
  status: DocumentStatus;

  @ManyToOne(type => Document, document => document.states, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  document: Document;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<DocumentState> = {}) {
    super(data);
    Object.assign(this, data);
  }
}
