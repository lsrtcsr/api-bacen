import {
  AccountType,
  CivilStatus,
  ConsumerSchema,
  ConsumerStatus,
  DocumentStatus,
  PhoneType,
  UserRole,
  UnleashFlags,
  WalletTransientStatus,
  ConsumerTransientStatus,
  WalletStatus,
} from '@bacen/base-sdk';
import { Equals, IsNotEmpty, IsOptional, ValidateNested, Length } from 'class-validator';
import { Readable } from 'stream';
import { Column, DeepPartial, Entity, In, IsNull, JoinColumn, Not, OneToMany, OneToOne, getManager } from 'typeorm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { DocumentClazz } from '../../../config/kyc.config';
import { default as Config } from '../../../config';
import { StorageService, CacheService } from '../../services';
import { IsCNPJ, IsCPF } from '../../utils/ValidationUtil';
import { ExtendedStatefulEntity } from '../base';
import { User } from '../user';
import { Address } from './Address';
import { Banking } from './Banking';
import { ConsumerState } from './ConsumerState';
import { Document } from './Document';
import { Phone } from './Phone';
import { LegalTermAcceptance } from './LegalTermAcceptance';
import { CompanyData } from './CompanyData';

export const DEFAULT_CONSUMER_LIMIT = 25;
export const DEFAULT_CONSUMER_PENDING_STATES = [
  ConsumerStatus.PENDING_DOCUMENTS,
  ConsumerStatus.PENDING_EMAIL_VERIFICATION,
  ConsumerStatus.PENDING_PHONE_VERIFICATION,
  // TODO: Re-enable after pending legal bugs are fixed
  // ConsumerStatus.PENDING_LEGAL_ACCEPTANCE,
];

export enum AnalysisResult {
  APPROVED = 'approved',
  DENIED = 'denied',
}

export interface AnalysisResultReason {
  result: AnalysisResult;
  at: Date;
  reason: string;
  by: User;
  customerData: {
    userId: string;
    name: string;
    taxId: string;
    motherName: string;
    address?: string;
    phone?: string;
    birthday?: Date;
  };
}

const WALLET_REGISTRATION_STATUS_PRECEDENCE_TABLE = {
  [WalletTransientStatus.READY]: -2,
  [WalletTransientStatus.PROCESSING]: -1,
  [WalletTransientStatus.PENDING_DOCUMENTS]: 0,
  [WalletTransientStatus.REJECTED]: 1,
  [WalletTransientStatus.FAILED]: 2,
};

const WALLET_REGISTRATION_TO_CONSUMER_TRANSIENT_STATUS_TABLE = {
  [WalletTransientStatus.READY]: ConsumerTransientStatus.READY,
  [WalletTransientStatus.PROCESSING]: ConsumerTransientStatus.PROCESSING,
  [WalletTransientStatus.PENDING_DOCUMENTS]: ConsumerTransientStatus.PENDING_DOCUMENTS,
  [WalletTransientStatus.REJECTED]: ConsumerTransientStatus.REJECTED,
  [WalletTransientStatus.FAILED]: ConsumerTransientStatus.FAILED,
};

@Entity(Consumer.tableName)
export class Consumer extends ExtendedStatefulEntity<ConsumerStatus, ConsumerState> implements ConsumerSchema {
  private static readonly tableName = 'consumers';

  @OneToMany((type) => ConsumerState, (consumerStateItem) => consumerStateItem.consumer, {
    cascade: ['insert', 'update'],
  })
  states?: ConsumerState[];

  @OneToOne((type) => User, (user) => user.consumer, {
    nullable: true,
    onDelete: 'SET NULL',
    cascade: ['insert', 'update'],
  })
  @JoinColumn()
  user?: User;

  @IsNotEmpty({ groups: [AccountType.PERSONAL] })
  @Column({ nullable: true })
  birthday: Date;

  @IsOptional({ always: true })
  @Column({ nullable: true, name: 'birth_city' })
  birthCity?: string;

  @Length(2, 2, { always: true })
  @IsOptional({ always: true })
  @Column({ nullable: true, name: 'birth_state' })
  birthState?: string;

  @Column({ type: 'enum', enum: CivilStatus, default: CivilStatus.UNKNOWN, nullable: false })
  civilStatus?: CivilStatus;

  @IsNotEmpty({ groups: [AccountType.PERSONAL] })
  @Column({ nullable: true })
  motherName?: string;

  @IsNotEmpty({ always: true })
  @Equals(AccountType.CORPORATE, { groups: [UserRole.MEDIATOR] })
  @Column({ type: 'enum', enum: AccountType, default: AccountType.PERSONAL, nullable: false })
  type: AccountType = AccountType.PERSONAL;

  @Column('jsonb', { nullable: true })
  @IsNotEmpty({ groups: [AccountType.CORPORATE] })
  @ValidateNested({ always: true })
  companyData?: CompanyData;

  @ValidateNested({ each: true, always: true })
  @OneToMany((type) => Address, (address) => address.consumer, { cascade: ['insert', 'update'], nullable: false })
  addresses?: Address[];

  @IsOptional({ always: true })
  @ValidateNested({ each: true, always: true })
  @OneToMany((type) => Banking, (banking) => banking.consumer, { cascade: ['insert', 'update'] })
  bankings?: Banking[];

  @IsOptional({ always: true })
  @ValidateNested({ each: true, always: true })
  @OneToMany((type) => Document, (document) => document.consumer, { cascade: ['insert', 'update'] })
  documents?: Document[];

  @ValidateNested({ each: true, always: true })
  @OneToMany((type) => Phone, (phone) => phone.consumer, { cascade: ['insert', 'update'], nullable: false })
  phones?: Phone[];

  @IsOptional({ always: true })
  @Column('uuid', { name: 'identity_id', nullable: true, unique: false })
  identityId?: string;

  @IsNotEmpty({ always: true })
  @IsCNPJ({ groups: [AccountType.CORPORATE] })
  @IsCPF({ groups: [AccountType.PERSONAL] })
  @Column({ nullable: false })
  taxId: string; // CPF ou CNPJ

  @IsNotEmpty()
  @Column('boolean', { default: false, nullable: false })
  pep = false;

  @OneToMany((type) => LegalTermAcceptance, (acceptance) => acceptance.consumer)
  legalTermAcceptances?: LegalTermAcceptance[];

  // TODO: Number anotation?
  @IsOptional({ always: true })
  @Column({ nullable: true, name: 'financialEquity' })
  financialEquity?: string;

  // TODO: Number anotation?
  @IsOptional({ always: true })
  @Column({ nullable: true, name: 'financialProfit' })
  financialProfit?: string;

  constructor(data: Partial<Consumer> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public get persistentStatus(): ConsumerStatus | undefined {
    return super.status;
  }

  public get status(): any | undefined {
    const consumerPersistedStatus = super.status;

    if (!consumerPersistedStatus) return undefined;

    // this.user.wallets

    switch (consumerPersistedStatus) {
      case ConsumerStatus.PENDING_PHONE_VERIFICATION:
        return ConsumerTransientStatus.PENDING_PHONE_VERIFICATION;
      case ConsumerStatus.PENDING_LEGAL_ACCEPTANCE:
        return ConsumerTransientStatus.PENDING_LEGAL_ACCEPTANCE;
      case ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION:
        return ConsumerTransientStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;
      case ConsumerStatus.READY:
        const leastAdvancedRequiredWalletRegistrationStatus = this.user?.wallets
          ?.map((wallet) => wallet.status)
          ?.sort(
            (left, right) =>
              WALLET_REGISTRATION_STATUS_PRECEDENCE_TABLE[left] - WALLET_REGISTRATION_STATUS_PRECEDENCE_TABLE[right],
          )?.[0];

        if (!leastAdvancedRequiredWalletRegistrationStatus) return undefined;
        return WALLET_REGISTRATION_TO_CONSUMER_TRANSIENT_STATUS_TABLE[leastAdvancedRequiredWalletRegistrationStatus];
      case ConsumerStatus.PENDING_DELETION:
        return ConsumerTransientStatus.PENDING_DELETION;
      case ConsumerStatus.DELETED:
        return ConsumerTransientStatus.DELETED;
    }
  }

  public async getLastKYCCheckReport(): Promise<Readable> {
    const statuses = await ConsumerState.find({
      where: {
        consumer: { id: this.id },
        additionalData: Not(IsNull()),
        status: In([
          ConsumerStatus.MANUAL_VERIFICATION,
          ConsumerStatus.INVALID_DOCUMENTS,
          ConsumerStatus.PROCESSING_WALLETS,
          ConsumerStatus.BLOCKED,
          ConsumerStatus.READY,
        ]),
      },
      order: { createdAt: 'DESC' },
    });

    let lastKYCCheck: ConsumerState;
    for (let index = 0; index < statuses.length; index++) {
      if (statuses[index].additionalData.hasOwnProperty('kycFileName')) {
        lastKYCCheck = statuses[index];
        break;
      }
    }

    if (!lastKYCCheck || !lastKYCCheck.additionalData || !lastKYCCheck.additionalData.kycFileName) {
      return undefined;
    }

    return StorageService.getInstance().getFileAsStream({
      bucketName: Config.googlecloud.storage.bucketName,
      fileName: lastKYCCheck.additionalData.kycFileName,
    });
  }

  public async requiredDocumentsProvided(): Promise<boolean> {
    if (UnleashUtil.instance.isEnabled(UnleashFlags.KYC_SKIP_CONSUMER, { userId: this.user?.id })) return true;

    const documents =
      this.documents || (await Document.safeFind({ where: { consumer: { id: this.id }, isActive: true } }));
    const requiredDocuments = Config.kyc.requiredDocuments.filter((required) => required.who.includes(this.type));

    const promises = requiredDocuments.map(async (requiredDocument) => {
      const options = Config.kyc.documentClassification.get(requiredDocument.clazz);
      const availableDocuments = documents.filter((document) => options.includes(document.type));
      if (!availableDocuments || availableDocuments.length === 0) return Promise.resolve(false);

      if (this.type === AccountType.CORPORATE && requiredDocument.clazz === DocumentClazz.COMPANY_IDENTITY_PROOF) {
        const companyIdentityProofType = Config.kyc.establishmentFormatToDocumentType.get(
          this.companyData.establishmentFormat,
        );
        const companyIdentityProof = availableDocuments.find((document) => document.type === companyIdentityProofType);
        return companyIdentityProof ? await companyIdentityProof.requiredSidesExist() : Promise.resolve(false);
      }
      if (this.type === AccountType.CORPORATE && requiredDocument.clazz === DocumentClazz.PERSON_IDENTITY_PROOF) {
        let requiredDocumentsForAllPartnersExists = true;
        for (const partner of this.companyData.partners) {
          const partnerIdentityProof = availableDocuments.find((document) =>
            [partner.taxId, partner.identityDoc].includes(document.number),
          );
          const partnerIdentityProofSidesExists = partnerIdentityProof
            ? await partnerIdentityProof.requiredSidesExist()
            : false;
          requiredDocumentsForAllPartnersExists =
            requiredDocumentsForAllPartnersExists && partnerIdentityProofSidesExists;
        }
        return Promise.resolve(requiredDocumentsForAllPartnersExists);
      }
      return availableDocuments[0].requiredSidesExist();
    });

    const results = await Promise.all(promises);

    return results.reduce((result, current) => result && current, true);
  }

  public async documentsApproved(): Promise<boolean> {
    const documents = await Document.safeFind({
      where: { consumer: { id: this.id }, isActive: true },
      relations: ['states'],
    });
    return documents.every((document) =>
      [DocumentStatus.VERIFIED, DocumentStatus.MANUALLY_VERIFIED].includes(document.status),
    );
  }

  public async phoneVerified(): Promise<boolean> {
    const phones = this.phones || (await Phone.find({ where: { consumer: this.id } }));
    if (!phones) return false;

    return phones.some((phone) => phone.type === PhoneType.MOBILE && !!phone.verifiedAt);
  }

  public async getDefaultAddress(): Promise<Address> {
    if (this.addresses) {
      return this.addresses.find((address) => address.default);
    }
    const address = await Address.safeFindOne({ where: { consumer: this, default: true } });
    if (address) return address;

    // Return first created
    return await Address.safeFindOne({ where: { consumer: this }, order: { createdAt: 'ASC' } });
  }

  public async getDefaultPhone(): Promise<Phone> {
    if (this.phones) {
      return this.phones.find((phone) => phone.default);
    }
    const phone = await Phone.safeFindOne({ where: { consumer: this, default: true } });
    if (phone) return phone;

    // Return first created
    return await Phone.safeFindOne({ where: { consumer: this }, order: { createdAt: 'ASC' } });
  }

  public async mapDocumentsForUpload(walletId: string, manager = getManager()) {
    const consumer = this.documents.length
      ? this
      : await manager.findOne(Consumer, this.id, { relations: ['documents'] });

    const filteredDocuments = consumer.documents.filter((document) => document.isActive);

    const documents = await Promise.all(
      filteredDocuments.flatMap(async (document) => {
        const sides = await document.existingSides();
        return Promise.all(
          sides.map(async (side) => {
            const fileName = document.getImageFileName(side);

            const file = await StorageService.getInstance().getFile({ fileName });
            return {
              document,
              file,
              side,
              walletId,
            };
          }),
        );
      }),
    );

    return documents.flat(1);
  }

  public async generateDownloadableLinksToActiveDocuments(): Promise<void> {
    const activeDocuments = this.documents.length
      ? this.documents.filter((document) => document.isActive)
      : await Document.find({ where: { consumer: { id: this.id }, isActive: true } });

    if (!activeDocuments.length) return;

    const promises = activeDocuments.map((document) => document.generateDownloadableLinks());
    await Promise.all(promises);
  }

  /**
   * Returns if the consumer instance is active
   */
  public async isActive(): Promise<boolean> {
    const cacheService = CacheService.getInstance();
    const key = `consumer_active_${this.id}`;

    const cachedResult = await cacheService.get(key);
    if (cachedResult) return cachedResult as boolean;

    let result: boolean;
    if (this.status) {
      result = this.status === ConsumerStatus.READY;
    } else {
      const lastState = await ConsumerState.createQueryBuilder('states')
        .where('"consumerId" = :consumerId', { consumerId: this.id })
        .orderBy('created_at', 'DESC')
        .getOne();

      result = lastState.status === ConsumerStatus.READY;
    }

    await cacheService.set(key, JSON.stringify(result), -1);
    return result;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Consumer> {
    const states = this.getStates();

    return {
      // Relations
      addresses: this.addresses && this.addresses.map((address) => address.toJSON()),
      documents: this.documents && this.documents.map((document) => document.toJSON()),
      phones: this.phones && this.phones.map((phone) => phone.toJSON()),
      legalTermAcceptances:
        this.legalTermAcceptances && this.legalTermAcceptances.map((acceptance) => acceptance.toJSON()),

      createdAt: this.createdAt,
      updatedAt: states ? new Date(Math.max(this.updatedAt.getTime(), states[0].updatedAt.getTime())) : this.updatedAt,
      states: states ? states.map((state) => state.toJSON()) : states,
      status: this.status,

      taxId: this.taxId,
      pep: this.pep,
      type: this.type,
      companyData: this.companyData ? this.companyData : undefined,
      financialEquity: this.financialEquity,
      financialProfit: this.financialProfit,
      civilStatus: this.civilStatus,
      motherName: this.motherName,
      birthday: this.birthday,
      birthState: this.birthState,
      birthCity: this.birthCity,
      id: this.id,
    };
  }

  /**
   * Converts the instance to a simple JSON to be returned from the public API in array.
   */
  public toSimpleJSON(): DeepPartial<Consumer> {
    const states = this.getStates();
    return {
      createdAt: this.createdAt,
      updatedAt: states ? new Date(Math.max(this.updatedAt.getTime(), states[0].updatedAt.getTime())) : this.updatedAt,
      status: this.status,
      taxId: this.taxId,
      pep: this.pep,
      type: this.type,
      companyData: this.companyData,
      id: this.id,
    };
  }
}
