import { LegalTermAcceptanceSchema, LegalTermAcceptanceConsumerData } from '@bacen/base-sdk';
import { Column, ManyToOne, Entity } from 'typeorm';
import { ExtendedEntity } from '../base';
import { LegalTerm } from './LegalTerm';
import { Consumer } from './Consumer';

@Entity(LegalTermAcceptance.tableName)
export class LegalTermAcceptance extends ExtendedEntity implements LegalTermAcceptanceSchema {
  private static readonly tableName = 'legal_term_acceptances';

  @ManyToOne(
    () => LegalTerm,
    term => term.acceptances,
    { nullable: false },
  )
  public legalTerm?: LegalTerm;

  @ManyToOne(
    () => Consumer,
    consumer => consumer.legalTermAcceptances,
    { nullable: false },
  )
  public consumer?: Consumer;

  @Column('jsonb', { nullable: false })
  public consumerInfo: LegalTermAcceptanceConsumerData;

  constructor(data: Partial<LegalTermAcceptance>) {
    super(data);
    Object.assign(this, data);
  }

  public toJSON() {
    return {
      id: this.id,
      legalTerm: this.legalTerm ? this.legalTerm.toJSON() : undefined,
      consumer: this.consumer ? this.consumer.toJSON() : undefined,
      consumerInfo: this.consumerInfo,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      deletedAt: this.deletedAt,
    };
  }
}
