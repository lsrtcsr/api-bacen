import {
  IsString,
  IsNumber,
  IsNotEmpty,
  IsBoolean,
  IsOptional,
  ValidateNested,
  IsEnum,
  IsDateString,
  ArrayNotEmpty,
  validate,
  ValidationError,
  Length,
} from 'class-validator';
import { AccountType, EconomicActivitySchema, CompanyPartnerSchema, CompanyDataSchema } from '@bacen/base-sdk';
import { Type, Transform } from 'class-transformer';
import { Address } from './Address';
import { Phone } from './Phone';
import { ExtendedEntity } from '../base';

export enum Gender {
  MALE = 'M',
  FEMALE = 'F',
  UNKNOWN = 'U',
}

export enum EstablishmentFormatEnum {
  MEI = 'MEI',
  EI = 'EI',
  EIRELI = 'EIRELI',
  LTDA = 'LTDA',
  SS = 'SS',
  SA = 'SA',
  ME = 'ME',
  EPP = 'EPP',
  EMGP = 'EMGP',
}

export enum IndividualPartnerProfile {
  OWNER = 'OWNER',
  ATTORNEY = 'ATTORNEY',
}

export enum GenderEnum {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER',
  NOT_SPECIFIED = 'NOT SPECIFIED',
}

export enum MaritalStatusEnum {
  SINGLE = 'SINGLE',
  MARIED = 'MARIED',
  DIVORCED = 'DIVORCED',
  WIDOWED = 'WIDOWED',
}

export class EconomicActivity implements EconomicActivitySchema {
  @IsNotEmpty({ always: true })
  isMain: boolean;

  @IsNotEmpty({ always: true })
  code: string;

  @IsNotEmpty({ always: true })
  description: string;
}

export class CompanyPartner implements CompanyPartnerSchema {
  @IsNotEmpty({ always: true })
  type: AccountType;

  @IsNotEmpty({ always: true })
  profile: IndividualPartnerProfile;

  @IsNotEmpty({ always: true })
  motherName: string;

  @IsNotEmpty({ always: true })
  @IsBoolean({ always: true })
  isPep: boolean;

  @IsOptional()
  @IsNumber()
  revenue?: number;

  @IsOptional()
  occupation?: string;

  @IsNotEmpty({ always: true })
  name: string;

  @IsOptional()
  gender?: Gender;

  @IsNotEmpty({ always: true })
  @IsDateString({ always: true })
  birthDate: Date;

  @IsNotEmpty({ always: true })
  birthCountry: string;

  @IsNotEmpty({ always: true })
  taxId: string;

  @IsNotEmpty({ always: true })
  email: string;

  @IsOptional()
  maritalStatus?: MaritalStatusEnum;

  @IsOptional()
  identityDoc?: string;

  @IsOptional()
  identityDocEmissionDate?: string;

  @IsOptional()
  identityDocEmissionEntity?: string;

  @IsOptional()
  identityDocState?: string;

  @Type(() => Phone)
  @IsNotEmpty({ always: true })
  @ArrayNotEmpty({ always: true })
  @ValidateNested({ each: true, always: true })
  phones: Phone[];

  @IsOptional()
  requiresAttention?: string[];

  @IsOptional()
  companyData?: CompanyDataSchema;

  @Type(() => Address)
  @IsNotEmpty({ always: true })
  @ArrayNotEmpty({ always: true })
  @ValidateNested({ each: true, always: true })
  addresses: Address[];

  public validate(): Promise<ValidationError[]> {
    return validate(this, { forbidUnknownValues: false });
  }
}

export class CompanyData extends ExtendedEntity implements CompanyDataSchema {
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  tradeName: string;

  @IsNotEmpty({ always: true })
  @IsDateString({ always: true })
  openingDate: Date;

  @Type(() => EconomicActivity)
  @ArrayNotEmpty({ always: true })
  @ValidateNested({ each: true, always: true })
  activities: EconomicActivity[];

  @Type(() => CompanyPartner)
  @ValidateNested({ each: true, always: true })
  partners?: CompanyPartner[];

  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  legalName: string;

  @Transform((value: string) => value.replace('-', ''))
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @Length(4, 4, { always: true })
  legalNature?: string;

  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  legalStatus: string;

  @IsNotEmpty({ always: true })
  @IsEnum(EstablishmentFormatEnum, { always: true })
  establishmentFormat: EstablishmentFormatEnum;

  @IsOptional()
  @IsString({ always: true })
  stateRegistration?: string;

  @IsNotEmpty({ always: true })
  @IsNumber()
  revenue: number;

  @IsNotEmpty({ always: true })
  @IsBoolean({ always: true })
  partnerChanged: boolean;
}
