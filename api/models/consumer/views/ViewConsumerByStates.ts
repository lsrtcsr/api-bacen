import { ViewEntity, ViewColumn, getManager } from 'typeorm';
import { ConsumerStatus } from '@bacen/base-sdk';

@ViewEntity({
  expression: `
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(users.id) as count
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    WHERE users.deleted_at is null
    GROUP BY latest_states.status
  `,
})
export class ViewConsumerByStates {
  @ViewColumn()
  status: ConsumerStatus;

  @ViewColumn()
  count: number;

  public static async find(): Promise<ViewConsumerByStates[] | undefined> {
    return getManager().find(ViewConsumerByStates);
  }
}
