import { ConsumerStatus, Pagination, PaginationData } from '@bacen/base-sdk';
import { FindConditions, FindManyOptions, getManager, In, SelectQueryBuilder, ViewColumn, ViewEntity } from 'typeorm';
import { User } from '../../user';
import { DEFAULT_CONSUMER_LIMIT, DEFAULT_CONSUMER_PENDING_STATES } from '../Consumer';

@ViewEntity({
  expression: `
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("consumerId") "consumerId", "status", created_at, "additionalData"
      FROM
          consumer_states
      ORDER BY
          "consumerId", created_at DESC
    )
    SELECT  users.id as "userId",
            consumers.id as "consumerId",
            latest_states.status,
            users."firstName",
            users."lastName",
            users.email,
            latest_states."additionalData",
            consumers."taxId",
            consumers."birthday",
            consumers."companyData",
            users.domain_user as "domainId"
    FROM consumers
    JOIN users on users.id = consumers."userId"
    JOIN latest_states on latest_states."consumerId" = consumers.id
    WHERE users.role = 'consumer' AND users.deleted_at IS NULL
    ORDER BY latest_states.created_at DESC
    `,
})
export class ViewConsumerWithStates {
  @ViewColumn()
  userId: string;

  @ViewColumn()
  consumerId: string;

  @ViewColumn()
  domainId: string;

  @ViewColumn()
  status: ConsumerStatus;

  @ViewColumn()
  firstName: string;

  @ViewColumn()
  lastName: string;

  @ViewColumn()
  email: string;

  @ViewColumn()
  additionalData: string;

  @ViewColumn()
  taxId: string;

  @ViewColumn()
  birthday: string;

  @ViewColumn()
  companyData: string;

  public static async find(options?: FindManyOptions<ViewConsumerWithStates>): Promise<ViewConsumerWithStates[]> {
    return getManager().find(ViewConsumerWithStates, options);
  }

  private static async getUserWhere(options?: FindManyOptions<ViewConsumerWithStates>): Promise<FindConditions<User>> {
    const { where, select } = options;

    const consumerViews = await ViewConsumerWithStates.find({ where, select });
    if (!consumerViews || consumerViews.length === 0) return null;
    const userIds = consumerViews.map(consumer => consumer.userId);

    return {
      id: In(userIds),
    };
  }

  /**
   * Find consumers based on view and populate with user information.
   *
   * @param options The find many options for the query
   */
  public static async findConsumers(options?: FindManyOptions<ViewConsumerWithStates>): Promise<User[]> {
    const { where, skip, take, select, ...rest } = options;
    const userWhere = await ViewConsumerWithStates.getUserWhere(options);
    if (!userWhere) return [];
    return User.safeFind({ ...rest, where: userWhere });
  }

  /**
   * Gets pending consumers from view.
   *
   * @param manager The entity manager
   */
  public static async findPendingConsumers(
    manager = getManager(),
    skip = 0,
    limit = DEFAULT_CONSUMER_LIMIT,
  ): Promise<
    {
      user_id: string;
      consumer_id: string;
      state_id: string;
      status: string;
      status_created_at: string;
    }[]
  > {
    return (await ViewConsumerWithStates.findPendingConsumersQuery(manager)).take(limit).getRawMany();
  }

  /**
   * Gets pending consumers count from view.
   *
   * @param manager The entity manager
   */
  public static async findPendingConsumersCount(manager = getManager()): Promise<number> {
    return (await (await ViewConsumerWithStates.findPendingConsumersQuery(manager)).getRawMany()).length;
  }

  /**
   * Gets pending consumers query builder from view.
   *
   * @param manager The entity manager
   */
  public static async findPendingConsumersQuery(manager = getManager()): Promise<SelectQueryBuilder<unknown>> {
    return getManager()
      .createQueryBuilder()
      .select(`"user_id", "consumer_id", "state_id", "status", "status_created_at"`)
      .from(
        subQuery =>
          subQuery
            .from(User, 'user')
            .leftJoin('user.consumer', 'consumer')
            .leftJoin('consumer.states', 'state')
            .select(
              `DISTINCT ON (consumer.id) user.id "user_id", consumer.id "consumer_id", state.id "state_id", state.status "status", state.created_at "status_created_at"`,
            )
            .where('"user"."deleted_at" IS NULL')
            .orderBy('consumer.id')
            .addOrderBy('state.created_at', 'DESC'),
        'pending_user',
      )
      .where('status IN (:...pendingStates)', { pendingStates: DEFAULT_CONSUMER_PENDING_STATES })
      .andWhere(`current_timestamp - status_created_at > interval '1 week' `);
  }

  public static async paginatedFindConsumers(
    options?: FindManyOptions<ViewConsumerWithStates> & { pagination: Pagination },
  ): Promise<[User[], PaginationData]> {
    const { where, skip, take, select, ...rest } = options;
    const userWhere = await ViewConsumerWithStates.getUserWhere(options);

    if (!userWhere)
      return [[], { dataLength: 0, dataLimit: options.pagination.limit, dataSkip: options.pagination.skip }];

    return User.safePaginatedFind({ ...rest, where: userWhere });
  }
}
