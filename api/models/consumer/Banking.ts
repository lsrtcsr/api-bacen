import { AccountType, BankingSchema, BankingType } from '@bacen/base-sdk';
import { IsNotEmpty, Length, IsDefined, IsOptional, IsNumberString } from 'class-validator';
import { Column, Entity, EntityManager, getManager, ManyToOne, OneToMany } from 'typeorm';
import { EntityNotFoundError } from '../../errors';
import { ExtendedEntity } from '../base';
import { Consumer } from './Consumer';
import { IsCPF, IsCNPJ } from '../../utils';
import { Transaction } from '../transaction';

@Entity(Banking.TABLE_NAME)
export class Banking extends ExtendedEntity implements BankingSchema {
  private static readonly TABLE_NAME = 'bankings';

  @ManyToOne((type) => Consumer, (consumer) => consumer.bankings)
  consumer: Consumer;

  @OneToMany((type) => Transaction, (transaction) => transaction.banking)
  transactions: Transaction[];

  @IsNotEmpty({ always: true })
  @Column({ nullable: false, type: 'enum', enum: BankingType, default: BankingType.CHECKING })
  type: BankingType = BankingType.CHECKING;

  @IsNotEmpty({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Column({ nullable: true })
  @IsCPF({ groups: [AccountType.PERSONAL] })
  @IsCNPJ({ groups: [AccountType.CORPORATE, AccountType.FINANCIAL_INSTITUTION] })
  taxId: string;

  @IsNotEmpty({ always: true })
  @Column({ type: 'enum', enum: AccountType, default: AccountType.PERSONAL, name: 'holder_type', nullable: false })
  holderType: AccountType = AccountType.PERSONAL;

  @IsOptional({ always: true })
  @Length(8, 77, {
    groups: ['pix'],
    message: 'property key has failed the following constraints: length (min 8 max 77)',
  })
  @Column({ nullable: true, length: 77 })
  key?: string;

  @IsOptional({ always: true })
  @Column('boolean', { name: 'has_same_source_fi', nullable: false, default: false })
  hasSameSourceFI: boolean;

  @IsNotEmpty({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Length(3, 100, {
    groups: [AccountType.PERSONAL, AccountType.CORPORATE],
    message: 'property name has failed the following constraints: length (min 3 max 100)',
  })
  @Column({ nullable: true, length: 100 })
  name?: string;

  @IsNotEmpty({ always: true })
  @Column({ nullable: false })
  bank: string;

  @IsNumberString({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Length(1, 4, { groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @IsNotEmpty({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Column({ nullable: true })
  agency: string;

  @IsDefined({ always: true })
  @Column({ nullable: true, type: 'character varying' })
  agencyDigit = '';

  @IsNumberString({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Length(1, 20, { groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @IsNotEmpty({ groups: [AccountType.PERSONAL, AccountType.CORPORATE] })
  @Column({ nullable: true })
  account?: string;

  @IsDefined({ always: true })
  @Column({ nullable: false, type: 'character varying' })
  accountDigit = '';

  constructor(data: Partial<Banking>) {
    super(data);

    Object.assign(this, data);
  }

  public toJSON(): Partial<Banking> {
    return {
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      name: this.name,
      taxId: this.taxId,
      holderType: this.holderType,
      type: this.type,
      bank: this.bank,
      agency: this.agency,
      agencyDigit: this.agencyDigit,
      account: this.account,
      accountDigit: this.accountDigit,
    };
  }

  public static async from(
    bankingSchema: BankingSchema,
    consumer: Consumer,
    manager: EntityManager = getManager(),
  ): Promise<Banking & { isNew?: boolean }> {
    const normalizedBanking = Banking.create(bankingSchema as any);

    const exists = await Banking.safeFindOne({
      where: {
        consumer: { id: consumer.id },
        taxId: normalizedBanking.taxId,
        bank: normalizedBanking.bank,
        agency: normalizedBanking.agency,
        agencyDigit: normalizedBanking.agencyDigit,
        account: normalizedBanking.account,
        accountDigit: normalizedBanking.accountDigit,
        holderType: normalizedBanking.holderType,
        type: normalizedBanking.type,
        hasSameSourceFI: normalizedBanking.hasSameSourceFI,
      },
    });

    if (exists) {
      return exists;
    }

    const banking = Banking.create({ ...(bankingSchema as any), consumer }) as Banking & { isNew: boolean };
    await manager.insert(Banking, banking);
    banking.isNew = true;

    return banking;
  }

  public static async findById(bankingId: string) {
    const destinationBank = await Banking.safeFindOne({ where: { id: bankingId } });
    if (!destinationBank) throw new EntityNotFoundError('Banking');
    return destinationBank;
  }
}
