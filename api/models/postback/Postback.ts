import { ConsumerStatus, TransactionStatus, PaymentType } from '@bacen/base-sdk';
import { BaseError, Logger, LoggerInstance } from 'nano-errors';
import { Column, Entity, OneToMany } from 'typeorm';
import { OAuthClient, Domain, User, Transaction } from '..';
import { PostbackDeliveryPublisher } from '../../services';
import { ExtendedEntity } from '../base';
import { PostbackDelivery, PostbackDeliveryType } from './PostbackDelivery';
import { PostbackStatus } from './PostbackStatus';

const loggerTitle = '[ PostbackDelivery]';

export interface BasePostback {
  type: string;
  status: string;
}

export interface ConsumerPostbackPayload extends BasePostback {
  id: string;
  type: 'consumer';
  status: ConsumerStatus;
  entity: 'user';
  user: Partial<User>;
  url: string;
}

export interface TransactionPostbackPayload extends BasePostback {
  id: string;
  type: 'transaction';
  status: TransactionStatus;
  entity: 'transaction';
  transaction: Partial<Transaction>;
  url: string;
}

export interface DictPostbackPayload extends BasePostback {
  entity: 'dictEntry' | 'dictClaim' | 'dictInfraction';
  id: string;
  status: string;
}

export interface CcsPostbackPayload extends BasePostback {
  type: 'ccs';
  urls?: string[];
}

@Entity(Postback.tableName)
export class Postback<T extends BasePostback = BasePostback> extends ExtendedEntity {
  protected static readonly tableName = 'postbacks';

  @Column({ name: 'executed_at', nullable: true })
  public executedAt: Date;

  @Column({ name: 'sent_at', nullable: true })
  public sentAt?: Date;

  @Column({ nullable: false, default: PostbackStatus.PENDING, type: 'enum', enum: PostbackStatus })
  public status: PostbackStatus;

  @Column({ type: 'jsonb' })
  public payload: T;

  @OneToMany(() => PostbackDelivery, (delivery) => delivery.postback)
  public deliveries: PostbackDelivery[];

  @Column({ type: 'character varying' })
  public entityType: T['type'];

  @Column()
  public entityId: string;

  @Column({ type: 'jsonb', nullable: true, default: [] })
  public urls?: string[];

  @Column({ type: 'jsonb', nullable: true, default: [] })
  public systemUrls?: string[];

  public constructor(data: Partial<Postback<T>> = {}) {
    super(data);

    Object.assign(this, data);
  }

  public getRelatedEntity(relations: string[] = []): Promise<User | Transaction> {
    if (this.entityType === 'consumer') {
      return User.safeFindOne({ where: { id: this.entityId }, relations });
    }
    if (this.entityType === 'transaction') {
      return Transaction.safeFindOne({ where: { id: this.entityId }, relations });
    }
  }

  public static getResourceURL(entity: any) {
    if (entity.constructor.name === 'Consumer') {
      const baseURL = (entity as User).domain?.urls?.length && (entity as User).domain?.urls[0];

      if (baseURL?.length) {
        return baseURL?.concat(`/consumers/${entity.id}`);
      }
    }
    if (entity.constructor.name === 'Transaction') {
      const baseURL =
        (entity as Transaction).source.user.domain?.urls?.length && (entity as Transaction).source.user.domain?.urls[0];

      if (baseURL?.length) {
        return baseURL?.concat(`/transactions/${entity.id}`);
      }
    }
  }

  public async getAllDeliveries(): Promise<PostbackDelivery[]> {
    const mappedUrls = [
      ...(this.urls || []).map((url) => ({ url, type: PostbackDeliveryType.MEDIATOR })),
      ...(this.systemUrls || []).map((url) => ({ url, type: PostbackDeliveryType.SYSTEM })),
    ];
    const allDeliveries = await Promise.all(
      mappedUrls.map((mappedUrl) => PostbackDelivery.getOrBuild({ ...mappedUrl, postback: this })),
    );
    return allDeliveries;
  }

  public async getPendingDeliveries(): Promise<PostbackDelivery[]> {
    const allDeliveries = await this.getAllDeliveries();
    const pendingDeliveries = allDeliveries.filter((d) => d.status === PostbackStatus.PENDING);
    return pendingDeliveries;
  }

  public async getStatusFromDeliveries(): Promise<PostbackStatus> {
    const allDeliveries = await this.getAllDeliveries();

    if (allDeliveries.length > this.urls.length + this.systemUrls.length) return PostbackStatus.PENDING;

    const mediatorDeliveries = allDeliveries.filter((delivery) => delivery.type === PostbackDeliveryType.MEDIATOR);
    const systemDeliveries = allDeliveries.filter((delivery) => delivery.type === PostbackDeliveryType.SYSTEM);

    if (
      mediatorDeliveries.every((delivery) => delivery.status === PostbackStatus.FAILED) ||
      systemDeliveries.some((delivery) => delivery.status === PostbackStatus.FAILED)
    ) {
      return PostbackStatus.FAILED;
    }
    if (
      mediatorDeliveries.some((delivery) => delivery.status === PostbackStatus.EXECUTED) &&
      systemDeliveries.every((delivery) => delivery.status === PostbackStatus.EXECUTED)
    ) {
      return PostbackStatus.EXECUTED;
    }

    return PostbackStatus.PENDING;
  }

  public async updateStatusFromDeliveries(): Promise<void> {
    const status = await this.getStatusFromDeliveries();

    if (this.status !== status) await Postback.safeUpdate(this.id, { status });
  }

  public async getClient(
    payload: ConsumerPostbackPayload | TransactionPostbackPayload | DictPostbackPayload,
  ): Promise<OAuthClient | undefined> {
    let logger: LoggerInstance;
    let client: OAuthClient;

    if (payload.entity === 'transaction') {
      try {
        const { transaction } = payload;
        const { payments } = transaction;
        const paymentTypes: PaymentType[] = payments.map((item) => {
          if (item.type !== PaymentType.SERVICE_FEE) {
            return item.type;
          }
        });

        const cashInTypes = [PaymentType.BOLETO, PaymentType.DEPOSIT, PaymentType.AUTHORIZED_CARD_REVERSAL];

        const isCashIn: boolean = paymentTypes.some((item) => cashInTypes.includes(item));
        const sourceDomainId = transaction.source.user.id;
        const destinationDomainId = payments.map((item) => item.destination.user.id)[0];

        const userId = !isCashIn ? sourceDomainId : destinationDomainId;
        const user: User = await User.safeFindOne({ where: { id: userId }, relations: ['domain'] });

        if (!user) {
          throw new BaseError(`Postback User not found`, { userId });
        }

        const domain: Domain = await Domain.findOneOrFail(user.domain.id);

        if (!domain) {
          throw new BaseError(`Postback User Domain not found`, { userId });
        }

        client = await OAuthClient.safeFindOne({ where: { domain }, order: { createdAt: 'ASC' } });

        if (client) {
          logger?.info(`${loggerTitle} Transaction postback was signed by clientId: ${client.id}`, {
            postback: this.id,
            client: client.id,
          });

          return client;
        }
        logger?.info(`${loggerTitle} Transaction postback wasn't signed`, {
          postback: this.id,
        });
      } catch (exception) {
        logger?.warn(`${loggerTitle} can't found an OAuthClient for the postback: ${exception.message}`, {
          message: exception.message,
          details: {
            stack: exception.stack,
            data: exception?.toJSON() || exception.response?.data || exception.data || exception,
          },
        });
        return undefined;
      }
    }
    if (payload.entity === 'user') {
      try {
        // TODO: get consumer domain by delivery payload
        const user: User = await User.safeFindOne({ where: { id: payload.user.id }, relations: ['domain'] });

        // Find oldest client by domain, used by the mediator
        // TODO: Discuss about better mechanisms
        if (!user) {
          throw new BaseError(`Postback User not found`, { userId: payload.user.id });
        }

        const domain: Domain = await Domain.findOneOrFail(user.domain.id);

        if (!domain) {
          throw new BaseError(`Postback User Domain not found`, { userId: payload.user.id });
        }

        client = await OAuthClient.safeFindOne({ where: { domain }, order: { createdAt: 'ASC' } });

        if (client) {
          logger?.info(`${loggerTitle} Consumer postback was signed by clientId: ${client.id}`, {
            postback: this.id,
            client: client.clientId,
          });
          return client;
        }
      } catch (exception) {
        logger?.warn(`${loggerTitle} can't found an OAuthClient for the postback: ${exception.message}`, {
          message: exception.message,
          details: {
            stack: exception.stack,
            data: exception?.toJSON() || exception.response?.data || exception.data || exception,
          },
        });
        return undefined;
      }
    }
    if (payload.entity === 'dictClaim' || payload.entity === 'dictEntry' || payload.entity === 'dictInfraction') {
      try {
        const domain: Domain = await Domain.findOneOrFail(payload[payload.entity].domainId);

        if (!domain) {
          throw new BaseError(`Postback Domain not found`, { domainId: payload[payload.entity].domainId });
        }

        client = await OAuthClient.safeFindOne({ where: { domain }, order: { createdAt: 'ASC' } });

        if (client) {
          logger?.info(`${loggerTitle} Postback was signed by clientId: ${client.id}`, {
            postback: this.id,
            client: client.clientId,
          });
          return client;
        }
      } catch (exception) {
        logger?.warn(`${loggerTitle} can't found an OAuthClient for the postback: ${exception.message}`, {
          message: exception.message,
          details: {
            stack: exception.stack,
            data: exception?.toJSON() || exception.response?.data || exception.data || exception,
          },
        });
        return undefined;
      }
    }
    // TODO: return null or a default/fallback from domain settings?
    logger?.warn(`${loggerTitle} Postback can't be signed`, { postback: this.id });
    return undefined;
  }

  public async send(): Promise<boolean[]> {
    const logger = Logger.getInstance();

    if ((!this.urls || !this.urls?.length) && (!this.systemUrls || !this.systemUrls?.length)) {
      logger?.warn(`${loggerTitle} Tried to send a postback with no urls configured`, this);
      return [];
    }

    if (this.sentAt) {
      logger?.warn(`${loggerTitle} Sending postback already sent`, this);
    }

    await Postback.update(this.id, { sentAt: new Date() });

    const pendingDeliveries = await this.getPendingDeliveries();
    return Promise.all(pendingDeliveries.map((delivery) => PostbackDeliveryPublisher.getInstance().send(delivery)));
  }

  public static async fromUser(entity: User, status: ConsumerStatus): Promise<Postback<ConsumerPostbackPayload>> {
    const postback = (await Postback.insertAndFind({
      entityId: entity.id,
      payload: {
        id: entity.id,
        status,
        entity: 'user',
        user: entity,
        url: this.getResourceURL(entity),
      } as ConsumerPostbackPayload,
      entityType: 'consumer',
      urls: entity.domain.postbackUrls,
      systemUrls: entity.domain.systemPostbackUrls,
      executedAt: entity.updatedAt,
    })) as Postback<ConsumerPostbackPayload>;

    return postback;
  }

  public static async fromTransaction(
    entity: Transaction,
    status: TransactionStatus,
  ): Promise<Postback<TransactionPostbackPayload>> {
    const mediatorUrls = new Set<string>();
    if (entity.source?.user?.domain?.postbackUrl) {
      entity.source.user.domain.postbackUrls.forEach((url) => mediatorUrls.add(url));
    }

    entity?.payments.forEach((payment) => {
      if (payment?.destination?.user?.domain?.postbackUrl) {
        payment.destination.user.domain.postbackUrls.forEach((url) => mediatorUrls.add(url));
      }
    });

    const systemUrls = new Set<string>();
    if (entity.source?.user?.domain?.systemPostbackUrls?.length) {
      entity.source.user.domain.systemPostbackUrls.forEach((url) => systemUrls.add(url));
    }

    entity?.payments.forEach((payment) => {
      if (payment?.destination?.user?.domain?.systemPostbackUrls?.length) {
        payment.destination.user.domain.systemPostbackUrls.forEach((url) => systemUrls.add(url));
      }
    });

    if (!mediatorUrls.size && !systemUrls.size) return undefined;

    const postback = (await Postback.insertAndFind({
      entityId: entity.id,
      payload: {
        id: entity.id,
        status,
        entity: 'transaction',
        transaction: entity,
        url: this.getResourceURL(entity),
      } as TransactionPostbackPayload,
      entityType: 'transaction',
      urls: Array.from(mediatorUrls),
      systemUrls: Array.from(systemUrls),
    })) as Postback<TransactionPostbackPayload>;

    return postback;
  }

  public toJSON(): Partial<Postback<T>> {
    return {
      id: this.id,
      payload: this.payload,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      executedAt: this.executedAt,
    };
  }
}
