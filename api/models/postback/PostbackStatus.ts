export enum PostbackStatus {
  PENDING = 'pending',
  EXECUTED = 'executed',
  FAILED = 'failed',
}
