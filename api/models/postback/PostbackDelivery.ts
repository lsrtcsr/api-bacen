import { Entity, Column, ManyToOne } from 'typeorm';
import { IssueCategory, SeverityLevel, IssueType } from '@bacen/base-sdk';
import { ExtendedEntity } from '../base';
import { Postback } from './Postback';
import { PostbackStatus } from './PostbackStatus';
import { IssueHandler } from '../../services';

export enum PostbackDeliveryType {
  MEDIATOR = 'mediator',
  SYSTEM = 'system',
}

export interface PostbackDeliveryResponse {
  status: number;
  headers: any;
  body: any;
  exception: any;
}

@Entity(PostbackDelivery.tableName)
export class PostbackDelivery extends ExtendedEntity {
  protected static readonly tableName = 'postback_deliveries';

  @Column({ enum: PostbackDeliveryType, default: PostbackDeliveryType.MEDIATOR })
  public type: PostbackDeliveryType;

  @Column({ default: 0 })
  public retries: number;

  @Column({ default: 3 })
  public maxRetries: number;

  @Column({ type: 'jsonb', nullable: true })
  public lastResponse: PostbackDeliveryResponse;

  @Column({ enum: PostbackStatus, default: PostbackStatus.PENDING })
  public status: PostbackStatus;

  @Column()
  public url: string;

  @ManyToOne(
    () => Postback,
    postback => postback.deliveries,
  )
  public postback: Postback;

  public static async getOrBuild(options: {
    postback: Postback;
    url: string;
    type?: PostbackDeliveryType;
    maxRetries?: number;
  }): Promise<PostbackDelivery> {
    const { postback, url, type = PostbackDeliveryType.MEDIATOR, maxRetries } = options;

    const count = await PostbackDelivery.safeCount({ where: { postback, url, type, maxRetries } });

    if (count) return PostbackDelivery.safeFindOne({ where: { postback, url, type, maxRetries } });

    return PostbackDelivery.insertAndFind({ postback, url, type, maxRetries });
  }

  public async recordFailedAttempt(response: any): Promise<boolean> {
    this.retries += 1;
    this.lastResponse = response;

    if (this.retries >= this.maxRetries) {
      this.status = PostbackStatus.FAILED;
    }

    try {
      await PostbackDelivery.safeUpdate(this.id, {
        retries: this.retries,
        lastResponse: this.lastResponse,
        status: this.status,
      });
    } catch (exception) {
      await IssueHandler.getInstance().handle({
        category: IssueCategory.POSTBACK_DELIVERY,
        componentId: 'postback_delivery',
        severity: SeverityLevel.HIGH,
        details: exception,
        type: IssueType.UNKNOWN,
      });
    }

    return this.status !== PostbackStatus.FAILED;
  }

  public recordSuccessAttempt(response: any): Promise<PostbackDelivery> {
    this.lastResponse = response;

    this.status = PostbackStatus.EXECUTED;

    return PostbackDelivery.safeUpdate(this.id, {
      lastResponse: this.lastResponse,
      status: this.status,
    });
  }
}
