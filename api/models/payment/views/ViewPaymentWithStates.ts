import {
  ConsumerStatus,
  Pagination,
  PaginationData,
  TransactionAdditionalData,
  TransactionType,
  PaymentStatus,
  PaymentType,
  TransactionStatus,
} from '@bacen/base-sdk';
import {
  FindConditions,
  FindManyOptions,
  getManager,
  In,
  IsNull,
  Not,
  SelectQueryBuilder,
  ViewColumn,
  ViewEntity,
} from 'typeorm';
import { User } from '../../user';

@ViewEntity({
  expression: `
  with latest_states as (
    select distinct on (transaction_states."transactionId")
             transaction_states."transactionId"
           , transaction_states.status
           , transaction_states.created_at
           , transaction_states."additionalData"
        from transaction_states
    order by transaction_states."transactionId", transaction_states.created_at desc
   )
    select   p.id as id
           , t.id as transaction_id
           , ts.status as transaction_status
           , p.type as type
           , p.status as status
           , p.amount
           , p."destinationId" as destination
           , ud.id as destination_user
           , ud."firstName" as destination_user_first_name
           , ud."lastName" as destination_user_last_name
           , ud.role as destination_user_role
           , t."sourceId" as source
           , us.id as source_user
           , us."firstName" as source_user_first_name
           , us."lastName" as source_user_last_name
           , us.role as source_user_role
           , ts."additionalData" as transaction_additional_data
           , p.deleted_at
           , t.updated_at
        from payments p
        join transactions t on t.id = p."transactionId"
        join latest_states ts on ts."transactionId" = t.id
        join wallets wd on wd.id = p."destinationId"
        join users ud on ud.id = wd."userId"
        join wallets ws on ws.id = t."sourceId"
        join users us on us.id = ws."userId"
    order by ts.created_at desc;
    `,
})
export class ViewPaymentWithStates {
  @ViewColumn()
  id: string;

  @ViewColumn()
  type: string;

  @ViewColumn()
  status: PaymentStatus;

  @ViewColumn()
  amount: string;

  @ViewColumn()
  updated_at: string;

  @ViewColumn()
  deleted_at: string;

  @ViewColumn()
  transaction_id: string;

  @ViewColumn()
  transaction_status: TransactionStatus;

  @ViewColumn()
  transaction_additional_data: string;

  @ViewColumn()
  destination: string;

  @ViewColumn()
  destination_user: string;

  @ViewColumn()
  destination_user_first_name: string;

  @ViewColumn()
  destination_user_last_name: string;

  @ViewColumn()
  destination_user_role: string;

  @ViewColumn()
  source: string;

  @ViewColumn()
  source_user: string;

  @ViewColumn()
  source_user_last_name: string;

  @ViewColumn()
  source_user_first_name: string;

  @ViewColumn()
  source_user_role: string;

  public get source_user_name(): string {
    return `${this.source_user_first_name} ${this.source_user_last_name}`;
  }

  public get destination_user_name(): string {
    return `${this.destination_user_first_name} ${this.destination_user_last_name}`;
  }

  public toJSON() {
    return { ...this };
  }

  public toObject() {
    return { ...this };
  }

  public static async find(options?: FindManyOptions<ViewPaymentWithStates>): Promise<ViewPaymentWithStates[]> {
    return getManager().find(ViewPaymentWithStates, options);
  }

  public static async findByFilters(
    filters: { source?: string; destination?: string } & Pagination = {},
  ): Promise<[ViewPaymentWithStates[], PaginationData]> {
    const manager = getManager();

    const qb = manager
      .createQueryBuilder(ViewPaymentWithStates, 'view_payment_with_states')
      .select()
      .where('deleted_at is null');

    const countQb = manager
      .createQueryBuilder(ViewPaymentWithStates, 'view_payment_with_states')
      .select('count(distinct(id))', 'count')
      .where('deleted_at is null');

    if (filters.source) {
      qb.andWhere('source = :source', { source: filters.source });
    }

    if (filters.destination) {
      qb.andWhere('destination = :destination', { destination: filters.destination });
    }

    if (filters.skip) {
      qb.skip(+filters.skip);
    }

    if (filters.limit) {
      qb.take(+filters.limit);
    }

    const [results, { count }] = await Promise.all([qb.getMany(), countQb.getRawOne()]);

    const paginationData: PaginationData = {
      dataLength: count,
      dataLimit: filters.limit,
      dataSkip: filters.skip,
    };

    return [results, paginationData];
  }

  public static async sumByFilters(filters: { source?: string; destination?: string }): Promise<{ amount: string }> {
    const manager = getManager();

    const qb = manager
      .createQueryBuilder(ViewPaymentWithStates, 'view_payment_with_states')
      .select('sum(amount)', 'sum_amount')
      .where('deleted_at is null');

    if (filters.source) {
      qb.andWhere('source = :source', { source: filters.source });
    }

    if (filters.destination) {
      qb.andWhere('destination = :destination', { destination: filters.destination });
    }

    const { sum_amount: amount } = await qb.getRawOne();
    return { amount: amount?.toFixed(6) || '0.00' };
  }

  public static async WalletSource(filters: { wallet: string }): Promise<{ wallet: string; amount: string }> {
    const manager = getManager();

    const qb = manager
      .createQueryBuilder(ViewPaymentWithStates, 'view_payment_with_states')
      .select(['source', 'type', 'amount'])
      .where('deleted_at is null');

    const { source, amount } = await qb.getRawOne();
    return { wallet: source, amount };
  }
}
