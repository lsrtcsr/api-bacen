import {
  ConsumerStatus,
  Pagination,
  PaginationData,
  TransactionAdditionalData,
  TransactionType,
  PaymentStatus,
  PaymentType,
  TransactionStatus,
} from '@bacen/base-sdk';
import {
  FindConditions,
  FindManyOptions,
  getManager,
  In,
  IsNull,
  Not,
  SelectQueryBuilder,
  ViewColumn,
  ViewEntity,
} from 'typeorm';
import { User } from '../../user';

@ViewEntity({
  expression: `
  with latest_states as (
    select distinct on (transaction_states."transactionId")
         transaction_states."transactionId"
       , transaction_states.status
       , transaction_states.created_at
       , transaction_states."additionalData"
    from transaction_states
order by transaction_states."transactionId", transaction_states.created_at desc
)
, payment_with_states as (
    select   p.id as id
           , t.id as transaction_id
           , ts.status as transaction_status
           , p.type as type
           , p.status as status
           , p."assetId" as asset
           , p.amount
           , p."destinationId" as destination
           , ud.id as destination_user
           , ud."firstName" as destination_user_first_name
           , ud."lastName" as destination_user_last_name
           , ud.role as destination_user_role
           , t."sourceId" as source
           , us.id as source_user
           , us."firstName" as source_user_first_name
           , us."lastName" as source_user_last_name
           , us.role as source_user_role
           , ts."additionalData" as transaction_additional_data
           , p.deleted_at
           , t.updated_at
    from payments p
    join transactions t on t.id = p."transactionId"
    join latest_states ts on ts."transactionId" = t.id
    join wallets wd on wd.id = p."destinationId"
    join users ud on ud.id = wd."userId"
    join wallets ws on ws.id = t."sourceId"
    join users us on us.id = ws."userId"
order by ts.created_at desc
)
    select account
         , transaction_id
         , id
         , status
         , updated_at
         , deleted_at
         , wallet
         , user_first_name
         , user_last_name
         , user_role
         , user_type
         , asset
         , case
               when user_type in ('boleto', 'withdrawal', 'card', 'authorized_card', 'phone_credit', 'service_fee')
                   or (user_type = 'transfer' and account = 'source') then amount::numeric(16,6) * -1
               else amount::numeric(16,6)
        end as amount
    from (
             select 'source' as account                    
                  , transaction_id
                  , id
                  , status
                  , asset
                  , updated_at
                  , deleted_at
                  , source  as wallet
                  , source_user_first_name as user_first_name
                  , source_user_last_name as user_last_name
                  , source_user_role as user_role
                  , type user_type
                  , amount::numeric(16,6)
             from payment_with_states
            union all
             select 'destination'
                  , transaction_id
                  , id
                  , status
                  , asset                  
                  , updated_at
                  , deleted_at                      
                  , destination
                  , destination_user_first_name
                  , destination_user_last_name
                  , destination_user_role
                  , type
                  , amount::numeric(16,6)
             from payment_with_states
         ) t
      `,
})
export class ViewPaymentAccounting {
  @ViewColumn()
  account: string;

  @ViewColumn()
  transaction_id: string;

  @ViewColumn()
  id: string;

  @ViewColumn()
  status: PaymentStatus;

  @ViewColumn()
  asset: string;

  @ViewColumn()
  updated_at: string;

  @ViewColumn()
  deleted_at: string;

  @ViewColumn()
  wallet: string;

  @ViewColumn()
  user_first_name: string;

  @ViewColumn()
  user_last_name: string;

  @ViewColumn()
  user_role: string;

  @ViewColumn()
  user_type: string;

  @ViewColumn()
  amount: string;

  public get source_user_name(): string {
    return `${this.user_first_name} ${this.user_last_name}`;
  }

  public toJSON() {
    return { ...this };
  }

  public toObject() {
    return { ...this };
  }

  public static async find(options?: FindManyOptions<ViewPaymentAccounting>): Promise<ViewPaymentAccounting[]> {
    return getManager().find(ViewPaymentAccounting, options);
  }

  public static async getAccountWallet(filters: {
    wallet: string;
    asset: string;
  }): Promise<{ wallet: string; asset: string; amount: string }> {
    const manager = getManager();

    const qb = manager
      .createQueryBuilder(ViewPaymentAccounting, 'view_payment_accounting')
      .select('sum(amount)', 'amount')
      .where('deleted_at is null')
      .andWhere("status = 'settled'")
      .andWhere('asset = :asset', { asset: filters.asset })
      .andWhere('wallet = :wallet', { wallet: filters.wallet });

    const { amount } = await qb.getRawOne();

    return { wallet: filters.wallet, asset: filters.asset, amount };
  }

  public static async getAccountCustody(filters: { asset: string }): Promise<{ asset: string; amount: string }> {
    const manager = getManager();

    const qb = manager
      .createQueryBuilder(ViewPaymentAccounting, 'view_payment_accounting')
      .select('sum(amount)', 'amount')
      .where('deleted_at is null')
      .andWhere("status = 'settled'")
      .andWhere('asset = :asset', { asset: filters.asset })
      .andWhere("user_role = 'consumer'");

    const { amount } = await qb.getRawOne();

    return { asset: filters.asset, amount };
  }
}
