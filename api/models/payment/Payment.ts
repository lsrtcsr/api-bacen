import {
  OAuthClientPlatform,
  PaymentSchema,
  PaymentStatus,
  PaymentType,
  TransactionStatus,
} from '@bacen/base-sdk';
import { Column, DeepPartial, Entity, ManyToOne, Index } from 'typeorm';
import { PaymentLog } from '../../timescale';
import { Asset } from '../asset';
import { ExtendedEntity } from '../base';
import { Card } from '../card';
import { Transaction, TransactionStateItem } from '../transaction';
import { Wallet } from '../wallet/Wallet';

@Entity(Payment.tableName)
// eslint-disable-next-line import/prefer-default-export
export class Payment extends ExtendedEntity implements PaymentSchema {
  static readonly tableName = 'payments';

  @Column('enum', { default: PaymentType.TRANSFER, enum: PaymentType })
  type: PaymentType;

  @Index()
  @ManyToOne(
    type => Transaction,
    transaction => transaction.payments,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  transaction?: Transaction;

  @Column({ nullable: false })
  amount: string;

  @Index()
  @ManyToOne(
    type => Wallet,
    wallet => wallet.received,
    { nullable: false },
  )
  destination?: Wallet;

  @ManyToOne(
    type => Card,
    card => card.payments,
    {
      nullable: true,
      onDelete: 'RESTRICT',
    },
  )
  card?: Card;

  @Index()
  @ManyToOne(
    type => Asset,
    asset => asset.payments,
  )
  asset?: Asset;

  @Column({ type: 'date', name: 'schedule_for', nullable: true })
  scheduleFor?: Date;

  @Column({ type: 'enum', enum: PaymentStatus, default: PaymentStatus.AUTHORIZED, nullable: false })
  status: PaymentStatus = PaymentStatus.AUTHORIZED;

  constructor(data: Partial<Payment> = {}) {
    super(data);

    Object.assign(this, data);
  }

  /**
   * Checks if payment is an internal payment which must pass through the feature p2p flow.
   */
  public isInternalPayment(): boolean {
    const knownInternalPaymentTypes = [
      PaymentType.SERVICE_FEE,
      PaymentType.TRANSFER,
      PaymentType.CARD,
      PaymentType.AUTHORIZED_CARD,
      PaymentType.TRANSACTION_REVERSAL,
      PaymentType.AUTHORIZED_CARD_REVERSAL,
    ];

    // TODO: Check if came from the external authorization interface
    // if (this.transaction.createdBy.client.clientId === 'cdt-visa') {
    //   return false;
    // }

    return knownInternalPaymentTypes.includes(this.type);
  }

  public static async findBySourceWalletAndTransactionStatus(
    status: TransactionStatus[],
    walletId: string,
    assetId?: string,
  ): Promise<Payment[]> {
    const transactionIds = await Transaction.getIdsByStatus(status);

    // Early exit if there are no transactions matching the desired status
    if (!transactionIds.length) {
      return [];
    }

    // select * from payment p
    // left join transaction t where p.transactionId = t.id
    // where p.walletId in () and t.id in () and p.asset = asset
    const query = Payment.createQueryBuilder('payment')
      .leftJoinAndSelect('payment.transaction', 'transaction')
      .leftJoinAndSelect('payment.asset', 'asset')
      .where('transaction.source = :wallet', { wallet: walletId })
      .cache(false)
      .andWhere('payment.status in (:...statuses)', {
        statuses: [PaymentStatus.AUTHORIZED, PaymentStatus.SETTLED].map(status => status.valueOf()),
      });

    if (transactionIds.length > 0) {
      query.andWhere('transaction.id in (:...ids)', { ids: transactionIds });
    }

    // Add optional asset filtering
    if (assetId) {
      query.andWhere('payment.asset = :asset', { asset: assetId });
    }

    return query.getMany();
  }

  public static async findSumBySourceWalletAndTransactionStatus(
    status: TransactionStatus[],
    walletId: string,
    assetId?: string,
  ): Promise<number> {
    let query = Payment.createQueryBuilder('payment')
      .leftJoinAndSelect('payment.transaction', 'transaction')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('payment.asset', 'asset')
      .where('transaction.source = :wallet', { wallet: walletId })
      .andWhere('payment.status in (:...pstatuses)', {
        pstatuses: [PaymentStatus.AUTHORIZED, PaymentStatus.SETTLED],
      })
      .andWhere(qb => {
        const subQuery = qb
          .subQuery()
          .select('MAX(transaction_states.created_at)')
          .from(TransactionStateItem, 'transaction_states')
          .groupBy('transaction_states.transaction')
          .getQuery();
        return `transactionstate.created_at IN ${subQuery}`;
      })
      .andWhere('transactionstate.status IN (:...statuses)', { statuses: status.map(status => status.valueOf()) });

    // Add optional asset filtering
    if (assetId) {
      query = query.andWhere('payment.asset = :asset', { asset: assetId });
    }

    const result = await query
      .select('SUM(payment.amount::decimal)', 'totalAmount')
      .cache(false)
      .getRawOne();

    return result?.totalAmount ? parseFloat(result.totalAmount) : 0;
  }

  public static async findSumByDestinationWalletAndTransactionStatus(
    status: TransactionStatus[],
    walletId: string,
    assetId?: string,
  ): Promise<number> {
    let query = Payment.createQueryBuilder('payment')
      .leftJoinAndSelect('payment.transaction', 'transaction')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('payment.asset', 'asset')
      .where('payment.destination = :wallet', { wallet: walletId })
      .andWhere('payment.status in (:...pstatuses)', {
        pstatuses: [PaymentStatus.AUTHORIZED, PaymentStatus.SETTLED],
      })
      .andWhere(qb => {
        const subQuery = qb
          .subQuery()
          .select('MAX(transaction_states.created_at)')
          .from(TransactionStateItem, 'transaction_states')
          .groupBy('transaction_states.transaction')
          .getQuery();
        return `transactionstate.created_at IN ${subQuery}`;
      })
      .andWhere('transactionstate.status IN (:...statuses)', { statuses: status.map(status => status.valueOf()) });

    // Add optional asset filtering
    if (assetId) {
      query = query.andWhere('payment.asset = :asset', { asset: assetId });
    }

    const result = await query
      .select('SUM(payment.amount::decimal)', 'totalAmount')
      .cache(false)
      .getRawOne();

    return result?.totalAmount ? parseFloat(result.totalAmount) : 0;
  }

  public static async findByDestinationWalletAndTransactionStatus(
    status: TransactionStatus[],
    walletId: string,
    assetId?: string,
  ): Promise<Payment[]> {
    const transactionIds = await Transaction.getIdsByStatus(status);
    // select * from payment p
    // left join transaction t where p.transactionId = t.id
    // where p.walletId in () and t.id in () and p.asset = asset

    // Early exit if there are no transactions matching the desired status
    if (!transactionIds.length) {
      return [];
    }

    const query = Payment.createQueryBuilder('payment')
      .leftJoinAndSelect('payment.transaction', 'transaction')
      .leftJoinAndSelect('payment.asset', 'asset')
      .where('payment.destination = :wallet', { wallet: walletId })
      .cache(false)
      .andWhere('payment.status in (:...statuses)', {
        statuses: [PaymentStatus.AUTHORIZED, PaymentStatus.SETTLED].map(status => status.valueOf()),
      });

    if (transactionIds.length > 0) {
      query.andWhere('transaction.id in (:...ids)', { ids: transactionIds });
    }

    // Add optional asset filtering
    if (assetId) {
      query.andWhere('payment.asset = :asset', { asset: assetId });
    }

    return query.getMany();
  }

  public toJSON(): DeepPartial<Payment> {
    return {
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      scheduleFor: this.scheduleFor,
      asset: this.asset && this.asset.toJSON ? this.asset.toJSON() : this.asset,
      amount: this.amount,
      destination: this.destination && this.destination.toJSON ? this.destination.toJSON() : this.destination,
      transaction: this.transaction && this.transaction.toJSON ? this.transaction.toJSON() : this.transaction,
      type: this.type,
      status: this.status,
    };
  }

  public toLog(): PaymentLog {
    return new PaymentLog({
      source: this.transaction.source.id,
      asset: this.asset.code,
      amount: this.amount,
      destination: this.destination.id,
      domainId: this.destination.user.domain.id,
      type: this.type,
      transactionHash: this.transaction.additionalData.hash,
    });
  }
}
