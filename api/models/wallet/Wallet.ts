import {
  Arrangement,
  AssetRegistrationStatus,
  BaseCustody,
  CustodyFeature,
  DomainRole,
  LegalTermSchema,
  PaymentStatus,
  StellarWalletData,
  TransactionStatus,
  TransactionType,
  TransitoryAccountType,
  UserRole,
  WalletSchema,
  WalletStatus,
  WalletTransientStatus,
} from '@bacen/base-sdk';
import { StellarService } from '@bacen/stellar-service';
import * as currency from 'currency.js';
import * as Stellar from 'stellar-sdk';
import { BaseError, Logger } from 'ts-framework-common';
import {
  AfterLoad,
  Column,
  DeepPartial,
  Entity,
  EntityManager,
  getManager,
  Index,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import Config from '../../../config';
import { CacheService, ProviderManagerService, StellarProviderService, LegacyWalletService } from '../../services';
import { PaymentLog } from '../../timescale';
import { Asset } from '../asset';
import { AssetRegistration } from '../asset/AssetRegistration';
import { ExtendedStatefulEntity } from '../base';
import { Card } from '../card';
import { Domain } from '../domain';
import { OAuthAccessToken } from '../oauth2';
import { Payment } from '../payment';
import { Transaction, TransactionStateItem } from '../transaction';
import { User } from '../user';
import { WalletPendingBalanceView } from './views';
import { WalletAdditionalData } from './WalletAdditionalData';
import { WalletState } from './WalletState';

const ASSET_REGISTRATION_STATUS_PRECEDENCE_TABLE = {
  [AssetRegistrationStatus.FAILED]: -3,
  [AssetRegistrationStatus.BLOCKED]: -2,
  [AssetRegistrationStatus.REJECTED]: -1,
  [AssetRegistrationStatus.PENDING_REGISTRATION]: 0,
  [AssetRegistrationStatus.PENDING_DOCUMENTS]: 1,
  [AssetRegistrationStatus.PROCESSING]: 2,
  [AssetRegistrationStatus.APPROVED]: 3,
  [AssetRegistrationStatus.READY]: 4,
};

const ASSET_REGISTRATION_TO_WALLET_TRANSIENT_STATUS_TABLE = {
  [AssetRegistrationStatus.FAILED]: WalletTransientStatus.FAILED,
  [AssetRegistrationStatus.BLOCKED]: WalletTransientStatus.BLOCKED,
  [AssetRegistrationStatus.REJECTED]: WalletTransientStatus.REJECTED,
  [AssetRegistrationStatus.PENDING_REGISTRATION]: WalletTransientStatus.PENDING_REGISTRATION,
  [AssetRegistrationStatus.PENDING_DOCUMENTS]: WalletTransientStatus.PENDING_DOCUMENTS,
  [AssetRegistrationStatus.PROCESSING]: WalletTransientStatus.PROCESSING,
  [AssetRegistrationStatus.APPROVED]: WalletTransientStatus.PROCESSING,
  [AssetRegistrationStatus.READY]: WalletTransientStatus.READY,
};

export interface RegisterAssetOnProviderOptions {
  asset: Asset;

  provider: BaseCustody;

  reportId?: string;

  manager?: EntityManager;
}

@Entity(Wallet.tableName)
export class Wallet extends ExtendedStatefulEntity<WalletStatus, WalletState> implements WalletSchema {
  private static readonly tableName = 'wallets';

  @OneToMany((type) => WalletState, (walletState) => walletState.wallet, {
    cascade: ['insert', 'update'],
  })
  states?: WalletState[];

  @Column('jsonb', { nullable: false })
  stellar: StellarWalletData;

  @Index()
  @ManyToOne((type) => User, (user) => user.wallets, {
    cascade: ['insert', 'update'],
    nullable: false,
    onDelete: 'RESTRICT',
  })
  user?: User;

  @OneToMany((type) => Card, (card) => card.wallet, {
    onDelete: 'RESTRICT',
    cascade: ['insert', 'update'],
  })
  cards: Card[];

  @OneToMany((type) => Asset, (asset) => asset.issuer, {
    onDelete: 'RESTRICT',
    cascade: ['insert', 'update'],
  })
  issuedAssets?: Asset[];

  @OneToMany(() => AssetRegistration, (assetRegistration) => assetRegistration.wallet)
  assetRegistrations?: AssetRegistration[];

  @OneToMany((type) => Transaction, (transaction) => transaction.source, { onDelete: 'SET NULL' })
  transactions?: Transaction[];

  @OneToMany((type) => Payment, (payment) => payment.destination, { eager: false })
  received?: Payment[]; // TODO: Think of a better name

  @Column({ type: 'jsonb', default: {} })
  additionalData?: WalletAdditionalData;

  // Virtual property
  public assets?: Asset[];

  @AfterLoad()
  protected populateAssets() {
    this.assets =
      this.assetRegistrations && this.assetRegistrations.every((reg) => reg.asset)
        ? this.assetRegistrations.map((reg) => reg.asset)
        : undefined;
  }

  constructor(data: Partial<Wallet> = {}) {
    super(data);

    Object.assign(this, data);

    if (!(this.stellar && this.stellar.publicKey)) {
      const random = Stellar.Keypair.random();

      this.stellar = {
        publicKey: random.publicKey(),
        secretKey: random.secret(),
      };
    }
  }

  public get persistentStatus(): WalletStatus | undefined {
    return super.status;
  }

  public get status(): any | undefined {
    const walletPersistedStatus = super.status;
    if (!walletPersistedStatus) return undefined;

    switch (walletPersistedStatus) {
      case WalletStatus.PENDING:
        return WalletTransientStatus.PENDING;

      case WalletStatus.FAILED:
        return WalletTransientStatus.FAILED;

      case WalletStatus.REGISTERED_IN_STELLAR: {
        const leastAdvandecRequiredAssetRegistrationStatus = this.assetRegistrations
          ?.filter((ar) => ar.asset?.required && ar.status)
          ?.map((ar) => ar.status)
          ?.sort(
            (left, right) =>
              ASSET_REGISTRATION_STATUS_PRECEDENCE_TABLE[left] - ASSET_REGISTRATION_STATUS_PRECEDENCE_TABLE[right],
          )?.[0];

        if (!leastAdvandecRequiredAssetRegistrationStatus) return undefined;
        return ASSET_REGISTRATION_TO_WALLET_TRANSIENT_STATUS_TABLE[leastAdvandecRequiredAssetRegistrationStatus];
      }
    }
  }

  /**
   * Gets the root wallet in the database.
   */
  public static async getRootWallet(manager = getManager()): Promise<Wallet> {
    const keyPair = Stellar.Keypair.fromSecret(Config.stellar.rootWallet.secretKey);

    return manager
      .createQueryBuilder(Wallet, 'wallet')
      .select()
      .where("wallet.stellar->>'publicKey' = :publicKey", { publicKey: keyPair.publicKey() })
      .getOne();
  }

  public static async isIdFromRootWallet(walletId: string): Promise<boolean> {
    const wallet = await this.getRootWallet();

    return wallet.id === walletId;
  }

  /**
   * Returns the root transitory account wallet of the given type if it exists, otherwise returns undefined.
   *
   * A root transitory account wallet is a wallet associated with the root mediator user which receives assets associated
   * with a given transactionType (eg. CardPayments, boletos or ServiceFee)
   *
   * @param type The type of the TransitoryAccountWallet
   * @param manager The ORM manager
   */
  public static async getRootTransitoryAccountWallet(
    type: TransitoryAccountType,
    manager = getManager(),
  ): Promise<Wallet | undefined> {
    return manager
      .createQueryBuilder(Wallet, 'wallet')
      .innerJoin('wallet.user', 'user')
      .innerJoin('user.domain', 'domain')
      .where('user.role = :userRole', { userRole: UserRole.MEDIATOR })
      .andWhere('domain.role = :domainRole', { domainRole: DomainRole.ROOT })
      .andWhere(`"additionalData"->>'isTransitoryAccount' = 'true'`)
      .andWhere(`"additionalData"->>'transitoryAccountType' = :type`, { type })
      .getOne();
  }

  /**
   * Gets a transitory account wallet by it's type and holder or undefined if not exists.
   *
   * @param type    The transitory account type
   * @param holder  The wallet's holder
   * @param manager The ORM manager
   */
  public static async getTransitoryAccountByType(options: {
    type: TransitoryAccountType;
    holder?: User;
    manager?: EntityManager;
  }): Promise<Wallet | undefined> {
    const manager = options.manager || getManager();
    const mediator = options.holder || (await User.getRootMediator());

    return manager
      .createQueryBuilder(Wallet, 'wallet')
      .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
      .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
      .leftJoinAndSelect('assetRegistrations.asset', '.asset')
      .where('"userId" = :userId', { userId: mediator.id })
      .andWhere(`wallet."additionalData"->>'isTransitoryAccount' = 'true'`)
      .andWhere(`wallet."additionalData"->>'transitoryAccountType' = :type`, { type: options.type })
      .cache(true)
      .getOne();
  }

  public get root(): boolean {
    const keyPair = Stellar.Keypair.fromSecret(Config.stellar.rootWallet.secretKey);
    return keyPair.publicKey() === this.stellar.publicKey;
  }

  /**
   * Gets domain from the wallet owner.
   */
  public async getDomain(): Promise<Domain> {
    const user = this.user && this.user.domain ? this.user : await User.findOne(this.user);
    return user.domain && user.domain.name ? user.domain : await Domain.findOne(user.domain);
  }

  /**
   * Gets balances from stellar service.
   */
  public async getBalances(stellar: StellarService = StellarService.getInstance()) {
    if (!this.stellar) {
      throw new BaseError('Wallet has no stellar mapping information');
    }

    try {
      const info = await stellar.loadAccount(this.stellar.publicKey);
      return info.balances;
    } catch (exception) {
      throw exception;
    }
  }

  /**
   * Gets balance of the given asset from stellar service.
   */
  public async getAssetBalance(assetCode: string, stellar: StellarService = StellarService.getInstance()) {
    if (!this.stellar) {
      throw new BaseError('Wallet has no stellar mapping information');
    }

    try {
      const info = await stellar.loadAccount(this.stellar.publicKey);
      const assetBalance = info.balances.find((b) => (b as any).asset_code === assetCode);
      return assetBalance ? assetBalance.balance : undefined;
    } catch (exception) {
      throw exception;
    }
  }

  public async getAuthorizableBalance(asset: Asset): Promise<number> {
    const blockchainBalance = await this.getAssetBalance(asset.code);
    const pendingDebts = await WalletPendingBalanceView.getWalletPendingDebts(this.id);

    return Number(blockchainBalance) - (pendingDebts[asset.id] ?? 0);
  }

  /**
   * Gets available balance of the given asset
   */
  public async getAvailableBalanceByAsset(
    asset: Asset,
    stellar: StellarService = StellarService.getInstance(),
  ): Promise<number> {
    if (!this.stellar) throw new BaseError('Wallet has no stellar mapping information');

    try {
      const info = await stellar.loadAccount(this.stellar.publicKey);

      const assetBalance = info.balances.find((b) => (b as any).asset_code === asset.code);
      if (!assetBalance) return undefined;

      Logger.getInstance().debug(`Current balance in ${asset.code} of wallet ${this.id}: ${assetBalance.balance}`);

      const pendingBalance = await LegacyWalletService.getInstance().getPendingPaymentsSum(this.id, asset);
      Logger.getInstance().debug(`Pending balance in ${asset.code} of wallet ${this.id}: ${pendingBalance}`);

      const availableBalance = currency(assetBalance.balance, { precision: 7 }).add(pendingBalance).value;
      Logger.getInstance().debug(`Available balance in ${asset.code} of wallet ${this.id}: ${availableBalance}`);

      return availableBalance;
    } catch (exception) {
      throw exception;
    }
  }

  public static async getBlockedBalanceByAsset(options: {
    asset: Asset | string;
    arrangement?: Arrangement;
    wallets?: string[];
  }): Promise<number> {
    const asset = options.asset instanceof Asset ? options.asset.id : options.asset;

    let query = Payment.createQueryBuilder('payment')
      .leftJoinAndSelect('payment.transaction', 'transaction')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.states', 'transactionstate')
      .leftJoinAndSelect('payment.asset', 'asset')
      .where('payment.asset = :asset', { asset })
      .andWhere('payment.status = :paymentStatus', { paymentStatus: PaymentStatus.AUTHORIZED })
      .andWhere((qb) => {
        const subQuery = qb
          .subQuery()
          .select('MAX(transaction_states.created_at)')
          .from(TransactionStateItem, 'transaction_states')
          .groupBy('transaction_states.transaction')
          .getQuery();
        return `transactionstate.created_at IN ${subQuery}`;
      })
      .andWhere('transactionstate.status = :transactionStatus', { transactionStatus: TransactionStatus.AUTHORIZED });

    // Add optional arrangement filtering
    if (options.arrangement) {
      query = query
        .andWhere("transaction.additional_data ? 'arrangement'")
        .andWhere("transaction.additional_data->>'arrangement' = :arrangement", { arrangement: options.arrangement });
    }

    // Add optional source filtering
    if (options.wallets && options.wallets.length > 0) {
      query = query.andWhere('source.id IN (:...sources)', { sources: options.wallets });
    }

    const result = await query.select('SUM(payment.amount::decimal)', 'totalAmount').cache(false).getRawOne();

    return result?.totalAmount ? parseFloat(result.totalAmount) : 0;
  }

  /**
   * Gets the mediator wallet in the database.
   */
  public static async getMediatorWallet(domain: Domain): Promise<Wallet> {
    const query = Wallet.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
      .leftJoinAndSelect('wallet.user', 'user')
      .innerJoinAndSelect('user.domain', 'domain')
      .where('domain.id = :domainId', { domainId: domain.id })
      .andWhere('user.role = :role', { role: UserRole.MEDIATOR });

    return query.getOne();
  }

  public static async getWalletOwner(walletId: string): Promise<User> {
    return User.createQueryBuilder('user')
      .innerJoin('user.wallets', 'wallet')
      .leftJoinAndSelect('user.domain', 'domain')
      .where('wallet.id = :walletId', { walletId })
      .andWhere('wallet.deletedAt IS NULL')
      .andWhere('user.deletedAt IS NULL')
      .getOne();
  }

  /**
   * Registers the wallet in Stellar network.
   */
  public async register(
    createdBy?: OAuthAccessToken,
    stellar: StellarProviderService = StellarProviderService.getInstance(),
  ): Promise<Wallet> {
    // TODO: Do this in FSM
    Logger.getInstance().debug(`Registering wallet in Stellar network`, {
      wallet: this.id,
      publicKey: this.stellar.publicKey,
    });

    await stellar.register(createdBy, this);
    return this;
  }

  /**
   * Ensures the asset is registered in the external provider, when available, creating trustlines when needed.
   *
   * @param asset The asset instance to be checked
   */
  public async registerAssetProvider(options: RegisterAssetOnProviderOptions): Promise<void> {
    const { asset, provider, reportId, manager = getManager() } = options;

    if (provider.type !== asset.provider) {
      throw new BaseError(`Provider supplied is not valid for asset code "${asset.code}"`);
    }

    if (!this.user) {
      throw new BaseError(`Cannot register asset "${asset.code}", wallet user is unavailable`);
    }

    const user = await manager.findOne(User, {
      where: { id: this.user.id },
      relations: [
        'domain',
        'consumer',
        'consumer.states',
        'consumer.addresses',
        'consumer.phones',
        'consumer.documents',
      ],
    });

    const states =
      this.states && this.states.length > 0
        ? this.states
        : await manager.find(WalletState, { where: { wallet: this } });

    const { additionalData } = states.find((state) => state.status === WalletStatus.PENDING);

    Logger.getInstance().debug(`Registering wallet in external provider`, {
      asset: {
        provider: asset.provider,
        code: asset.code,
      },
      walletId: this.id,
    });

    // TODO: Fix typings
    await provider.register(
      {
        ...user.toJSON(),
        domain: user.domain.toJSON(),
        consumer: user.consumer ? user.consumer.toJSON() : undefined,
      } as any,
      this.toJSON() as WalletSchema,
      { ...additionalData, reportId },
    );
  }

  /**
   * Ensures the asset is registered in the wallet, creating trustlines when needed.
   *
   * @param asset The asset instance to be checked
   */
  public async registerAsset(
    asset: Asset,
    options: { manager?: EntityManager; reportId?: string } = {},
  ): Promise<void> {
    if (!asset.issuer) {
      throw new BaseError(`Cannot register asset "${asset.code}", issuer wallet is unavailable`, {
        asset: { code: asset.code },
      });
    }

    if (asset.issuer.id === this.id) {
      Logger.getInstance().warn(`Trying to register asset in its issuer wallet, this is not going to work`, {
        asset: { code: asset.code },
      });
      return;
    }

    if (asset.provider) {
      // Register in the external provider whenever before creating the trustline
      const provider = ProviderManagerService.getInstance().from(asset.provider);
      await this.registerAssetProvider({ asset, provider, reportId: options.reportId, manager: options.manager });
    }

    // Ensure there is a trustline in stellar
    await this.ensureTrustline(asset, options.manager);
  }

  /**
   * Ensures the asset is trusted by the wallet, or create trustline when needed.
   *
   * @param asset The asset instance to be checked
   */
  public async ensureTrustline(asset: Asset, manager: EntityManager = getManager()): Promise<void> {
    // issuer does not need a trustline.
    if (asset.issuer.id === this.id) return;

    const stellar = StellarService.getInstance();

    const stellarAcct = await stellar.loadAccount(this.stellar.publicKey);

    const trustlineExists = stellarAcct.balances.some(
      (balance: Stellar.Horizon.BalanceLineAsset<any>) =>
        balance.asset_code === asset.code && balance.asset_issuer === asset.issuer.stellar.publicKey,
    );

    if (trustlineExists) {
      return;
    }

    const response = await stellar.changeTrust({
      secretSeed: this.stellar.secretKey,
      asset: {
        code: asset.code,
        issuer: asset.issuer.stellar.publicKey,
      },
    });

    const transaction = await Transaction.insertAndFind(
      {
        source: this,
        type: TransactionType.CHANGE_TRUST,
        additionalData: {
          hash: response.hash,
          asset_id: asset.id,
          asset_code: asset.code,
        },
      },
      { manager },
    );

    const now = Date.now();

    // Save stellar transaction states
    await manager.insert(TransactionStateItem, [
      {
        transaction,
        status: TransactionStatus.PENDING,
        createdAt: new Date(now - 100),
        updatedAt: new Date(now - 100),
      },
      {
        transaction,
        status: TransactionStatus.AUTHORIZED,
        createdAt: new Date(now - 50),
        updatedAt: new Date(now - 50),
      },
      {
        transaction,
        status: TransactionStatus.EXECUTED,
        createdAt: new Date(now),
        updatedAt: new Date(now),
      },
    ]);
  }

  public async getProviderTerms(accepted?: 'true' | 'false'): Promise<LegalTermSchema[]> {
    const assets =
      this.assets ||
      (
        await AssetRegistration.find({
          where: { wallet: this },
          relations: ['asset'],
        })
      ).map((ar) => ar.asset);

    const promises = assets
      .filter((asset) => asset.provider)
      .filter((asset) => {
        const provider = ProviderManagerService.getInstance().from(asset.provider);
        return provider.implements(CustodyFeature.LEGAL);
      })
      .map((asset) => {
        const provider = ProviderManagerService.getInstance().from(asset.provider);
        const legalFeature = provider.feature(CustodyFeature.LEGAL);

        return legalFeature.findAll(this.id, accepted);
      });

    const pendingTerms = await Promise.all(promises);
    return pendingTerms.flat(1);
  }

  public async getPendingProviderTerms(): Promise<LegalTermSchema[]> {
    const assets =
      this.assets ||
      (
        await AssetRegistration.find({
          where: { wallet: this },
          relations: ['asset'],
        })
      ).map((ar) => ar.asset);

    const promises = assets
      .filter((asset) => asset.provider)
      .map((asset) => ProviderManagerService.getInstance().from(asset.provider))
      .filter((provider) => provider.implements(CustodyFeature.LEGAL))
      .map((provider) => {
        const legalFeature = provider.feature(CustodyFeature.LEGAL);
        return legalFeature.pending(this.id);
      });

    const pendingTerms = await Promise.all(promises);
    return pendingTerms.flat(1);
  }

  /**
   * Returns if the wallet instance is active
   */
  public async isActive(): Promise<boolean> {
    const cacheService = CacheService.getInstance();
    const key = `wallet_active_${this.id}`;

    const cachedResult = await cacheService.get(key);
    if (cachedResult) return cachedResult as boolean;

    let result: boolean;
    if (this.status) {
      result = this.status === WalletStatus.READY;
    } else {
      const lastState = await WalletState.createQueryBuilder('states')
        .where('"walletId" = :walletId', { walletId: this.id })
        .orderBy('created_at', 'DESC')
        .getOne();

      result = lastState.status === WalletStatus.READY;
    }

    await cacheService.set(key, JSON.stringify(result), -1);
    return result;
  }

  public async cacheActiveStatus(active: boolean) {
    const cacheService = CacheService.getInstance();
    const key = `wallet_active_${this.id}`;

    await cacheService.set(key, JSON.stringify(active), -1);
  }

  public async invalidateWalletActiveCache() {
    const cacheService = CacheService.getInstance();
    const key = `wallet_active_${this.id}`;

    await cacheService.del(key);
  }

  public async checkBalanceWithTimescale(): Promise<void> {
    const tolerance = 0.1;

    const timescaleBalances = await PaymentLog.getBalance(this.id);
    const stellarBalances = await this.getBalances();

    for (const asset of Object.keys(timescaleBalances)) {
      const timescaleBalance = timescaleBalances[asset];
      const stellarBalance = stellarBalances.find((balance) => (balance as any).asset_code === asset).balance;

      if (Math.abs(timescaleBalance - Number(stellarBalance)) > tolerance) {
        Logger.getInstance().warn(`Timescale and Stellar are unbalanced!`, {
          asset,
          timescaleBalance,
          stellarBalance,
          walletId: this.id,
        });
      }
    }
  }

  public static async getByPublicKey(publicKey: string): Promise<Wallet> {
    return Wallet.createQueryBuilder('wallet')
      .where("wallet.stellar->>'publicKey' = :publicKey", { publicKey })
      .getOne();
  }

  public toSimpleJSON(): DeepPartial<Wallet> {
    const obj: any = {
      user: this.user && this.user.toSimpleJSON ? this.user.toSimpleJSON() : this.user,
      assets: this.assets ? this.assets.map((asset) => asset.toSimpleJSON()) : this.assets,
      stellar: this.stellar,
      root: this.root ? true : undefined,
      status: this.status,
      id: this.id,
    };

    // TODO: Secret key should never be sent, NEVER!
    // We need cryptography, and a safe store for this
    if (obj.stellar && obj.stellar.secretKey) {
      delete obj.stellar.secretKey;
    }

    if (obj.data && obj.data.secretKey) {
      delete obj.data.secretKey;
    }

    return obj;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(setUser = true, assetRegistrations = false) {
    const states = this.getStates();

    const obj = {
      additionalData: this.additionalData,
      issuedAssets:
        this.issuedAssets && this.issuedAssets.length
          ? this.issuedAssets.map((asset) => asset.toSimpleJSON())
          : undefined,
      assets: this.assets ? this.assets.map((asset) => asset.toSimpleJSON()) : this.assets,
      user: setUser ? (this.user && this.user.toJSON ? this.user.toJSON(false) : this.user) : undefined,
      assetRegistrations:
        this.assetRegistrations && assetRegistrations
          ? this.assetRegistrations.map((ar) => ar.toJSON(false))
          : undefined,
      deletedAt: this.deletedAt,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      stellar: { ...this.stellar },
      states: states ? states.map((state) => state.toJSON()) : states,
      root: this.root ? true : undefined,
      status: this.status,
      id: this.id,
    };

    // TODO: Secret key should never be sent, NEVER!
    // We need cryptography, and a safe store for this
    if (obj.stellar && obj.stellar.secretKey) {
      delete obj.stellar.secretKey;
    }

    // TODO: Strip legacy data object
    if ((obj as any).data && (obj as any).data.secretKey) {
      delete (obj as any).data.secretKey;
    }

    return obj;
  }
}
