import { TransitoryAccountType } from '@bacen/base-sdk';

export interface WalletNotificationItem {
  type: 'slack';
  to: string;
  additionalData?: any;
}

export interface WalletAdditionalData {
  notifications?: WalletNotificationItem[];
  isTransitoryAccount?: boolean;
  transitoryAccountType?: TransitoryAccountType;
}
