import { getManager, ViewColumn, ViewEntity } from 'typeorm';
import { TRANSACTION_BLOCKED_BALANCE_STATES } from '../../transaction/constants';

@ViewEntity({
  expression: `
SELECT
  wallet.id AS "walletId",
  transactions.payment_asset AS asset,
  SUM(
    CASE
      WHEN transactions.source_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingDebtAmount",
  SUM (
    CASE
      WHEN transactions.destination_id=wallet.id THEN transactions.payment_amount::decimal
      ELSE 0
    END
  ) AS "pendingCreditAmount"
FROM wallets wallet
LEFT OUTER JOIN (
  SELECT
    transactions.id AS transaction_id,
    transactions."sourceId" AS source_id,
    payments.status AS payment_status,
    payments."destinationId" AS destination_id,
    payments.amount AS payment_amount,
    payments."assetId" AS payment_asset,
    payments.id AS payment_id
  FROM transactions
  LEFT OUTER JOIN payments ON payments."transactionId"=transactions.id
) transactions ON transactions.source_id=wallet.id OR transactions.destination_id=wallet.id
LEFT OUTER JOIN (
  SELECT
    "transactionId",
    MAX(created_at) AS created_at
  FROM transaction_states
  GROUP BY 1
) last_transaction_state ON transactions.transaction_id=last_transaction_state."transactionId"
LEFT OUTER JOIN transaction_states ON (
  transaction_states."transactionId"=last_transaction_state."transactionId"
  AND transaction_states.created_at=last_transaction_state.created_at
)
WHERE transaction_states.status IN (${TRANSACTION_BLOCKED_BALANCE_STATES.map((status) => `'${status}'`).join(', ')})
AND transactions.payment_status IN ('authorized', 'settled')
GROUP BY 1, 2
`,
})
export class WalletPendingBalanceView {
  @ViewColumn()
  walletId: string;

  @ViewColumn()
  asset: string;

  @ViewColumn()
  pendingDebtAmount: number;

  @ViewColumn()
  pendingCreditAmount: number;

  public static findByWalletAndAssetId(
    walletId: string,
    assetId: string,
    manager = getManager(),
  ): Promise<WalletPendingBalanceView | undefined> {
    return manager.findOne(WalletPendingBalanceView, {
      where: {
        walletId,
        asset: assetId,
      },
    });
  }

  public static async getWalletPendingDebts(walletId: string, manager = getManager()): Promise<Record<string, number>> {
    const pendingBalanceByAsset: Pick<WalletPendingBalanceView, 'asset' | 'pendingDebtAmount'>[] = await manager.find(
      WalletPendingBalanceView,
      {
        where: { walletId },
        select: ['asset', 'pendingDebtAmount'],
      },
    );

    const balances: Record<string, number> = {};
    for (const assetBalance of pendingBalanceByAsset) {
      balances[assetBalance.asset] = assetBalance.pendingDebtAmount;
    }

    return balances;
  }

  public static getWalletPendingBalance(walletId: string, manager = getManager()): Promise<WalletPendingBalanceView[]> {
    return manager.createQueryBuilder(WalletPendingBalanceView, 'pendingBalance').where({ walletId }).getMany();
  }
}
