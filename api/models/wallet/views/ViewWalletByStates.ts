import { ViewEntity, ViewColumn, getManager } from 'typeorm';
import { WalletStatus } from '@bacen/base-sdk';

@ViewEntity({
  expression: `
    WITH latest_states AS (
      SELECT
          DISTINCT ON ("walletId") "walletId", "status", created_at, "additionalData"
      FROM
      wallet_states
      ORDER BY
          "walletId", created_at DESC
    )
    SELECT  latest_states.status,
            COUNT(wallets.id) as count
    FROM wallets
    JOIN latest_states on latest_states."walletId" = wallets.id
    WHERE wallets.deleted_at is null
    GROUP BY latest_states.status
  `,
})
export class ViewWalletByStates {
  @ViewColumn()
  status: WalletStatus;

  @ViewColumn()
  count: number;

  public static async find(): Promise<ViewWalletByStates[] | undefined> {
    return getManager().find(ViewWalletByStates);
  }
}
