import { WalletStateSchema, WalletStatus } from '@bacen/base-sdk';
import { Column, Entity, Index, ManyToOne } from 'typeorm';
import { ExtendedStateEntity } from '../base';
import { Wallet } from './Wallet';

@Entity(WalletState.tableName)
@Index('wallet_state_status_unique', ['wallet', 'status'], { unique: false })
export class WalletState extends ExtendedStateEntity<WalletStatus> implements WalletStateSchema {
  private static readonly tableName = 'wallet_states';

  @Column('enum', { enum: WalletStatus, default: WalletStatus.PENDING, nullable: false })
  status: WalletStatus;

  @Index()
  @ManyToOne(
    type => Wallet,
    wallet => wallet.states,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  wallet: Wallet;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<WalletState> = {}) {
    super(data);
    Object.assign(this, data);
  }
}
