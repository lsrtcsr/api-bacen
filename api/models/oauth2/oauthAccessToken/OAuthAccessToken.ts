import * as moment from 'moment';
import {
  Column,
  DeepPartial,
  Entity,
  ManyToOne,
  MoreThan,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  LessThan,
  Index,
} from 'typeorm';
import { ExtendedEntity } from '../../base';
import { User } from '../../user';
import { OAuthClient } from '../oauthClient';
import { OAuthRefreshToken } from '../oauthRefreshToken';
import { OAuthAccessTokenUserAgent } from './OAuthAccessTokenUserAgent';
import { Transaction } from '../../transaction';
import { Phone } from '../../consumer';

@Entity(OAuthAccessToken.tableName)
export class OAuthAccessToken extends ExtendedEntity {
  private static readonly tableName = 'oauth_access_token';

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, unique: true, name: 'access_token' })
  accessToken: string;

  @Column({ nullable: false, name: 'token_type' })
  tokenType: string;

  @Column({ nullable: false, type: 'timestamp with time zone', name: 'expires' })
  expires: Date;

  @ManyToOne(
    type => OAuthClient,
    client => client.accessTokens,
    { eager: true },
  )
  client: OAuthClient;

  @Column('json', { nullable: false })
  scope: string[];

  @Column('json', { nullable: false, default: {} })
  userAgent: OAuthAccessTokenUserAgent = {};

  @OneToOne(
    type => OAuthAccessToken,
    token => token.accessToken,
    { eager: false, nullable: true },
  )
  refreshToken: OAuthRefreshToken;

  @OneToMany(
    type => Transaction,
    transaction => transaction.createdBy,
  )
  transactionsCreated: Transaction;

  @ManyToOne(
    type => User,
    user => user.accessTokens,
    {
      eager: true,
      nullable: true,
      onDelete: 'SET NULL',
    },
  )
  user: User;

  @OneToMany(
    type => Phone,
    phone => phone.verifiedBy,
    { onDelete: 'SET NULL' },
  )
  phoneVerifications: Phone[];

  constructor(data: Partial<OAuthAccessToken>) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Updates an access token setting its user agent stuff.
   *
   * @param accessToken The acess token to be updated
   * @param ip The client ip
   * @param userAgent The user agent information
   */
  public static async updateUserAgent(accessToken: string, ip: string, userAgent: any) {
    const ua = {
      ip,
      browser: userAgent.browser,
      version: userAgent.version,
      os: userAgent.os,
      platform: userAgent.browser.platform,
      source: userAgent.source,
    };

    await this.update({ accessToken }, { userAgent: { ...ua } });
    return ua;
  }

  /**
   * Revokes access tokens based on specified conditions.
   *
   * @param conditions
   */
  public static async revoke(conditions: Partial<OAuthAccessToken>) {
    const now = new Date();
    const condition = { ...conditions, expires: MoreThan(now) as DeepPartial<Date> };
    if (condition.accessToken) {
      delete condition.expires;
    }

    const rawTokens: OAuthAccessToken[] = await this.find({
      where: condition,
      select: ['accessToken'],
    });
    const keys: string[] = rawTokens.map(t => t.accessToken);
    const result: any = await this.update(condition, { expires: now });
    keys.push(conditions.user.id);

    return { keys, result };
  }

  /**
   * Saves a new access token in the database according to the oauth 2.0 middleware.
   *
   * @param token The token instance
   * @param client The client instance
   * @param user The user instance
   */
  static async saveAccessToken(token, client, user) {
    // Prepare the new access token instance
    const accessToken = await this.insertAndFind({
      client,
      user: user.virtual ? undefined : user,
      tokenType: 'Bearer',
      scope: (token.scope || '').split(','),
      expires: token.accessTokenExpiresAt,
      accessToken: token.accessToken,
    });

    let refreshToken;

    // Prepare the new refresh token instance
    if (token.refreshToken) {
      refreshToken = await OAuthRefreshToken.insertAndFind({
        client,
        accessToken,
        user: user.virtual ? undefined : user,
        expires: token.refreshTokenExpiresAt,
        refreshToken: token.refreshToken,
      });
    }

    // Return the middleware expected output.
    const result = {
      user: user.id,
      client: client.id,
      user_id: user.id,
      scope: accessToken.scope,
      accessToken: accessToken.accessToken,
      refreshToken: refreshToken ? refreshToken.refreshToken : undefined,
      expires_in: moment(token.accessTokenExpiresAt).diff(moment(), 'seconds') + 1,
    } as any;

    if (user.virtual) {
      result.virtual = true;
    }

    return result;
  }

  static async countActive(): Promise<number> {
    return this.safeCount({ where: { expires: MoreThan(new Date()) } });
  }
}
