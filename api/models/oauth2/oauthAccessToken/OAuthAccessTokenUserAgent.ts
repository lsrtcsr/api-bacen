export interface OAuthAccessTokenUserAgent {
  ip?: string;
  browser?: string;
  version?: string;
  os?: string;
  platform?: string;
  source?: string;
}
