import { UserRole, CustodyProvider } from '@bacen/base-sdk';
import { Client } from 'oauth2-server';
import { Logger } from 'ts-framework-common';
import Config from '../../../config';
import { DiscoveryService, CacheService } from '../../services';
import { Domain } from '../domain';
import { User } from '../user';
import { OAuthAccessToken } from './oauthAccessToken';
import { OAuthClient } from './oauthClient';
import { OAuthRefreshToken } from './oauthRefreshToken';

export default class OAuth2Middleware {
  /**
   * Gets an OAuth 2.0 Client instance from database.
   *
   * @param clientId The client id
   * @param clientSecret The client secret
   */
  public static async getClient(clientId: string, clientSecret: string): Promise<Client> {
    const client = await OAuthClient.safeFindOne({
      where: { clientId },
      cache: {
        id: `${clientId}`,
        milliseconds: Config.database.cacheTimeout,
      },
    });

    // Ensure that what come to here has the same secret
    if (client && client.clientSecret === clientSecret) {
      return {
        redirectUris: [],
        grants: Config.oauth.grantTypes,
        ...(client.toJSON() as OAuthClient),
      };
    }

    return null;
  }

  /**
   * Gets an user based on email and password credentials.
   *
   * @param email The user username or email
   * @param password The user password
   */
  static async getUser(email: string, password: string): Promise<User> {
    const users: User[] = await User.safeFind({
      where: { email: email?.toLowerCase().trim() },
      take: 1,
      order: {
        createdAt: 'ASC',
      },
      cache: {
        id: email?.toLowerCase().trim(),
        milliseconds: Config.database.cacheTimeout,
      },
    });

    if (users && users.length && users[0].validatePassword(password)) {
      return users[0];
    }

    return null;
  }

  /**
   * Gets a virtual user for a client credentials authentication.
   *
   * @param client The oauth client
   */
  static async getUserFromClient(client: OAuthClient): Promise<any> {
    let domain;
    if (client.domain) {
      domain = await Domain.safeFindOne({
        where: { id: client.domain.id || client.domain.id },
        cache: {
          id: client.domain.id,
          milliseconds: Config.database.cacheTimeout,
        },
      });
    }

    const clientRole = OAuthClient.getUserRoleByPlatform(client.platform);

    return {
      domain,
      id: client.id,
      virtual: true,
      clientId: client,
      name: client.clientId,
      role: clientRole,
    };
  }

  /**
   * Saves a new access token on the database.
   *
   * @param token The token instance
   * @param client The client instance
   * @param user The user instance
   */
  static async saveToken(token, client, user): Promise<boolean> {
    //! TODO: Unhardcode this
    if (client.clientId === 'cdt-cards') {
      await DiscoveryService.getInstance().cdtUp();
      Logger.getInstance().debug('Discovery service got a new UP event: "cdt-provider-api"');
    } else if (client.clientId === CustodyProvider.CREDITAS_PROVIDER) {
      await DiscoveryService.getInstance().creditasProviderUp();
      Logger.getInstance().debug('Discovery service got a new UP event: "str-provider-api" (Creditas)');
    } else if (client.clientId === CustodyProvider.LECCA_PROVIDER) {
      await DiscoveryService.getInstance().leccaProviderUp();
      Logger.getInstance().debug('Discovery service got a new UP event: "str-provider-api" (LECCA)');
    } else if (client.clientId === CustodyProvider.PARATI_PROVIDER) {
      await DiscoveryService.getInstance().paratiProviderUp();
      Logger.getInstance().debug('Discovery service got a new UP event: "str-provider-api" (PARATI)');
    }

    // Legacy client id for backwards compatibility
    if (
      client.clientId === CustodyProvider.BS2 ||
      client.clientId === 'bs2-provider' ||
      client.clientId === 'bs2-provider-api'
    ) {
      await DiscoveryService.getInstance().bs2Up();
      Logger.getInstance().debug('Discovery service got a new UP event: "bs2-provider-api"');
    }

    if (client.clientId === CustodyProvider.CELCOIN) {
      await DiscoveryService.getInstance().celcoinUp();
      Logger.getInstance().debug('Discovery service got a new UP event: "celcoin-provider-api"');
    }

    return OAuthAccessToken.saveAccessToken(token, client, user);
  }

  /**
   * Gets an OAuth 2.0 Access Token from the database.
   *
   * @param accessToken The access token
   */
  static async getAccessToken(accessToken: string): Promise<OAuthAccessToken | null> {
    const cacheService = CacheService.getInstance();
    const cacheKey = `oauth_access_token_${accessToken}`;
    const cachedToken: OAuthAccessToken = await cacheService.get(cacheKey);
    let token: OAuthAccessToken | undefined;

    if (cachedToken) {
      token = OAuthAccessToken.create(cachedToken);
    } else {
      token = await OAuthAccessToken.createQueryBuilder('oauthAccessToken')
        .leftJoinAndSelect('oauthAccessToken.client', 'client')
        .leftJoinAndSelect('oauthAccessToken.user', 'user')
        .where('oauthAccessToken.accessToken = :accessToken', { accessToken })
        .getOne();

      // Check if token exists and has a valid client.
      if (typeof token?.client?.dangerousToJSON === 'function') {
        const tokenJSON = {
          ...token,
          client: token.client.dangerousToJSON(),
        };

        cacheService.set(cacheKey, JSON.stringify(tokenJSON), Config.oauth.msTokenCacheTimeout / 1000);
      }
    }

    // Ensures it is not expired
    if (!token || new Date(token.expires) < new Date()) {
      return null;
    }

    // If no user get virtual user from client credentials
    token.user = token.user ? token.user : await this.getUserFromClient(token.client);

    // Prepare token expiration
    (token as any).accessTokenExpiresAt = new Date(token.expires);

    return token;
  }

  /**
   * Gets an OAuth 2.0 Refresh Token from the database.
   *
   * @param refreshToken The refresh token
   */
  static async getRefreshToken(refreshToken: string): Promise<OAuthAccessToken> {
    // Gets instance from database ensuring its expiration
    const token: any = await OAuthRefreshToken.safeFindOne({
      where: { refreshToken },
      relations: ['client', 'user'],
      cache: {
        id: refreshToken,
        milliseconds: Config.database.cacheTimeout,
      },
    });

    if (!token || new Date(token.expires) < new Date() || !token.user) {
      return null;
    }

    // Prepare token expiration
    token.accessTokenExpiresAt = token.expires;

    return token;
  }

  /**
   * Validates the scopes requested by the client.
   *
   * @param user The user instance
   * @param client The OAuth Client instance
   * @param scope The scopes
   */
  static async validateScope(user, client, scope) {
    if (scope && scope.length) {
      const scopeGroup = Config.oauth.scopes[user.role];

      // Parse requested scope group from request
      const requestedScopes = scope.split(',').filter(s => {
        return scopeGroup.indexOf(s) >= 0;
      });

      // Returns a flatten scope list
      return requestedScopes.join(',');
    }

    // If user role is known and 'allowEmptyScopes' is true, we will return all available scopes for this token
    if (user.role && Object.values(UserRole).includes(user.role) && Config.oauth.allowEmptyScopes) {
      // Return all available scopes for current role using configuration, this can be dangerous...
      const scopes = Config.oauth.scopes[user.role];
      return this.validateScope(user, client, scopes ? scopes.join(',') : undefined);
    }
  }

  /**
   * Verifies the scopes required in the access token.
   * Multiple scopes can be defined using the delimiter ','
   * In case of multiple scopes, it will return true if the user has any of them.
   *
   * @param token The token instance
   * @param scope The scopes required, joined by commas
   */
  static async verifyScope(token, scopes: string = '') {
    // Parse scopes into array
    const parsed = scopes.split(',');
    const scopeGroup = Config.oauth.scopes[token.user.role];

    // Check parsed array for availability in token instance
    const result = parsed.map(s => {
      return scopeGroup.indexOf(s) >= 0;
    });

    // Return aggregated results from scope verification
    return result.reduce((aggr, next) => aggr && next, true);
  }

  /**
   * Revokes a token.
   *
   * @param token The refresh token to be revoked
   */
  static async revokeToken(token: { refreshToken: string }) {
    await OAuthRefreshToken.update({ refreshToken: token.refreshToken }, { expires: new Date() });
    return true;
  }
}
