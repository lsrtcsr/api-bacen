import { Column, DeepPartial, Entity, ManyToOne, MoreThan, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ExtendedEntity } from '../../base';
import { User } from '../../user';
import { OAuthAccessToken } from '../oauthAccessToken';
import { OAuthClient } from '../oauthClient';

@Entity(OAuthRefreshToken.tableName)
export class OAuthRefreshToken extends ExtendedEntity {
  private static readonly tableName = 'oauth_refresh_token';

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, unique: true, name: 'refresh_token' })
  refreshToken: string;

  @Column({ nullable: false })
  expires: Date;

  @ManyToOne(
    type => User,
    user => user.accessTokens,
    {
      eager: false,
      nullable: true,
      onDelete: 'SET NULL',
    },
  )
  user: User;

  @ManyToOne(
    type => OAuthClient,
    client => client.accessTokens,
    {
      eager: false,
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  client: OAuthClient;

  @OneToOne(type => OAuthAccessToken, {
    eager: false,
    nullable: false,
    onDelete: 'CASCADE',
  })
  accessToken: OAuthAccessToken;

  /**
   * Revokes refresh tokens based on specified conditions.
   *
   * @param conditions
   * @param options
   */
  public static async revoke(conditions: Partial<OAuthRefreshToken>) {
    const now = new Date();
    return this.update({ ...conditions, expires: MoreThan(now) as DeepPartial<Date> }, { expires: now });
  }

  static async countActive(): Promise<number> {
    return this.safeCount({ where: { expires: MoreThan(new Date()) } });
  }
}
