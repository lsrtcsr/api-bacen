import { OAuthClientPlatform, OAuthClientSchema, OAuthClientStatus, UserRole } from '@bacen/base-sdk';
import { IsEnum, MinLength } from 'class-validator';
import { Column, DeepPartial, Entity, ManyToOne, OneToMany } from 'typeorm';
import { ExtendedEntity } from '../../base';
import { Domain } from '../../domain';
import { OAuthAccessToken } from '../oauthAccessToken';

@Entity(OAuthClient.tableName)
export class OAuthClient extends ExtendedEntity implements OAuthClientSchema {
  private static readonly tableName = 'oauth_client';

  @MinLength(3)
  @Column({ nullable: false, unique: true })
  clientId: string;

  @MinLength(8)
  @Column({ nullable: false })
  clientSecret: string;

  @IsEnum(OAuthClientPlatform)
  @Column({ nullable: false, type: 'enum', enum: OAuthClientPlatform, default: OAuthClientPlatform.WEB })
  platform: OAuthClientPlatform = undefined;

  @IsEnum(OAuthClientStatus)
  @Column({ nullable: false, type: 'enum', enum: OAuthClientStatus, default: OAuthClientStatus.ACTIVE })
  status: OAuthClientStatus = OAuthClientStatus.ACTIVE;

  @ManyToOne(
    type => Domain,
    domain => domain.clients,
    {
      nullable: true,
    },
  )
  domain?: Domain;

  @OneToMany(
    type => OAuthAccessToken,
    token => token.client,
    {
      cascade: ['insert', 'update'],
      onDelete: 'SET NULL',
    },
  )
  accessTokens: OAuthAccessToken[];

  constructor(data: Partial<OAuthClient> = {}) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Get User Role by platform.
   */
  public static getUserRoleByPlatform(platform: OAuthClientPlatform): UserRole {
    switch (platform) {
      case OAuthClientPlatform.ROOT:
        return UserRole.ADMIN;
      case OAuthClientPlatform.API:
      case OAuthClientPlatform.WEB:
        return UserRole.PUBLIC;
      case OAuthClientPlatform.EXTERNAL_AUTHORIZER:
        return UserRole.AUTHORIZATION;
    }
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<OAuthClient> {
    return {
      domain: this.domain,
      platform: this.platform,
      status: this.status,
      clientId: this.clientId,
      id: this.id,
    };
  }

  /**
   * Converts the instance to a JSON without omitting sensitive fields, therefore not suitable to be returned from the
   * public API.
   */
  public dangerousToJSON(): any {
    return {
      domain: this.domain,
      platform: this.platform,
      status: this.status,
      clientId: this.clientId,
      clientSecret: this.clientSecret,
      id: this.id,
    };
  }
}
