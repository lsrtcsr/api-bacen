export interface OAuthSecretTokenResource {
  entity: 'wallet';
  id: string;
}
