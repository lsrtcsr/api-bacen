import { Column, DeepPartial, Entity, ManyToOne, MoreThan, PrimaryGeneratedColumn } from 'typeorm';
import * as uuid from 'uuid';
import { UserStatus } from '@bacen/base-sdk';
import Config from '../../../../config';
import { ExtendedEntity } from '../../base';
import { User } from '../../user';
import { OAuthSecretTokenResource } from './OAuthSecretTokenResource';

interface OAuthAdditinalData {
  email?: string;
}
@Entity(OAuthSecretToken.tableName)
export class OAuthSecretToken extends ExtendedEntity {
  private static readonly tableName = 'oauth_secret_token';

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false, unique: true, name: 'secret_token' })
  secretToken: string;

  @Column({ nullable: false, type: 'timestamp with time zone', name: 'expires' })
  expires: Date;

  @ManyToOne(
    type => User,
    user => user.secretTokens,
    {
      eager: true,
      nullable: true,
      onDelete: 'SET NULL',
    },
  )
  user: User;

  @Column('json', { nullable: false, default: [] })
  scope: string[] = [];

  @Column('jsonb', { nullable: false, default: [] })
  resources: OAuthSecretTokenResource[] = [];

  @Column('jsonb', { nullable: true })
  additionalData?: OAuthAdditinalData;

  constructor(data: Partial<OAuthSecretToken> = {}) {
    super(data);
  }

  /**
   * Finds token based on its secret, if available and still valid.
   *
   * @param secret The user secret token
   */
  public static async findBySecret(secretToken: string): Promise<OAuthSecretToken | undefined> {
    return this.safeFindOne({
      where: { secretToken, expires: MoreThan(new Date()) },
    });
  }

  /**
   * Generates a new recovery token for the User account, revoking all existing ones.
   *
   * @param user The user to generate the secret token
   * @param scopes The list of scopes to be granted
   */
  public static async generateRecoveryToken(
    user: User,
    scopes?: string[],
    additionalData?: OAuthAdditinalData,
    resources: OAuthSecretTokenResource[] = [],
  ) {
    // Starts by revoking all currently active tokens for user
    await this.revoke({ user });

    // Generate a new secret token for recovery
    // TODO: Create a specific scope "user:password" only available using Secret token
    return this.generateToken(user, scopes, resources, additionalData);
  }

  /**
   * Generates a new secret token for the User account.
   *
   * @param user The user to generate the secret token
   * @param scopes The list of scopes to be granted
   */
  public static async generateToken(
    user: User,
    scope: string[] = [],
    resources: OAuthSecretTokenResource[] = [],
    additionalData?: OAuthAdditinalData,
  ) {
    // Ensure user is valid
    if (!user || user.status === UserStatus.INACTIVE) {
      throw new Error('User is not active or defined');
    }

    // Generate a new secret token
    const secret = await OAuthSecretToken.insertAndFind({
      user,
      scope,
      resources,
      secretToken: uuid.v4(),
      expires: new Date(Date.now() + Config.user.resetPassword.expiration || 24 * 60 * 60000), // 1 day
      additionalData,
    });

    // Return the new token
    return secret;
  }

  /**
   * Revokes secret tokens based on specified conditions.
   *
   * @param conditions The conditions for the token revoke
   */
  public static async revoke(conditions: Partial<OAuthSecretToken>) {
    const now = new Date();
    return this.update(
      { ...conditions, expires: MoreThan(now) as DeepPartial<Date> },
      {
        deletedAt: now,
        expires: now,
      },
    );
  }

  public toJSON(): DeepPartial<OAuthSecretToken> & { user: string } {
    // Don't return the secret token
    return {
      id: this.id,
      scope: this.scope,
      resources: this.resources,
      expires: this.expires,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      user: (this.user && this.user.id ? this.user.id : this.user) as any,
    };
  }
}
