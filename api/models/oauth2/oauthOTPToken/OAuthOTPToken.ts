import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { ForbiddenRequestError, InvalidRequestError } from '../../../errors';
import { TwoFactorHelper } from '../../../helpers';
import { ExtendedEntity } from '../../base';
import { User } from '../../user';

// TODO: Move to config
export const DEFAULT_MAX_FAILED_ATTEMPTS = 5;

@Entity(OAuthOTPToken.tableName)
export class OAuthOTPToken extends ExtendedEntity {
  public static readonly tableName = 'oauth_otp_tokens';

  @Column({ nullable: false, name: 'secret_seed' })
  secret: string;

  @Column({ nullable: false, name: 'secret_encoding', default: 'base32' })
  encoding: 'base32' | 'ascii' = 'base32';

  @Column({ nullable: true, name: 'qrcode_url' })
  qrcodeUrl: string;

  @Column({ nullable: true, type: 'timestamp with time zone', name: 'expires_at' })
  expires: Date;

  @Column({ nullable: true, name: 'failed_attempts', default: 0 })
  failedAttempts: number;

  @Column({ nullable: true, name: 'last_failed_attempt' })
  lastFailedAttempt: Date;

  @Column({ nullable: true, name: 'last_failed_attempt_token' })
  lastFailedAttemptToken: string;

  @Column({ nullable: true, name: 'last_successful_attempt_token' })
  lastSuccessfulAttemptToken: string;

  @JoinColumn()
  @ManyToOne(type => User, user => user.twoFactorSecret, { nullable: false, onDelete: 'SET NULL' })
  user: User;

  constructor(data: Partial<OAuthOTPToken> = {}) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Generates a new OAuth OTP secret and associates with user.
   *
   * @param user The user to associate the OTP secret to
   */
  public static async generate(user: User, requiresAfterConfigured: boolean = true) {
    const secret = TwoFactorHelper.generateSecretSeed();

    const otp = await OAuthOTPToken.insertAndFind({
      qrcodeUrl: secret.otpauth_url,
      secret: secret.base32,
      encoding: 'base32',
      user,
    });

    await User.update({ id: user.id }, { twoFactorRequired: requiresAfterConfigured });
    return otp;
  }

  /**
   * Disables two factor for specified user.
   *
   * @param user The user instance, will throw error if 2fa not enabled
   */
  public static async disable(user: User): Promise<void> {
    if (!user || !user.id || !user.twoFactorSecret || !user.twoFactorRequired) {
      throw new InvalidRequestError('Two factor is not configured for user account');
    }

    // Soft deletes the OAuth OTP Token
    await OAuthOTPToken.softDelete(user.twoFactorSecret.id);

    // Disable two factor in user authentication requests
    await User.update({ id: user.id }, { twoFactorRequired: false, twoFactorSecret: null });
  }

  /**
   * Verifies a token based on current secret.
   *
   * @param token The token to verify the secret against
   * @param window The window of checks
   */
  public async verify(token: string, window: number = 0, step?: number) {
    const verification = TwoFactorHelper.verifyToken(token, this.secret, this.encoding, window, step);

    // Check if is a failed token or a new one
    const sameToken = this.lastFailedAttemptToken === token;
    const reachedMaximumAttempts = this.failedAttempts >= DEFAULT_MAX_FAILED_ATTEMPTS;

    if (!verification && sameToken && reachedMaximumAttempts) {
      throw new ForbiddenRequestError('Reached maximum retries for current token, try again later');
    } else if (!verification) {
      await this.registerFailedAttempt(token);
    }

    return verification;
  }

  /**
   * Generates a new token based on current secret.
   *
   * @param token The token to verify the secret against
   * @param window The window of checks
   */
  public async token(timestamp: Date = undefined, digits: number = 6) {
    return TwoFactorHelper.generateTokenFromDate(this.secret, timestamp, digits, this.encoding);
  }

  private async registerFailedAttempt(token: string): Promise<void> {
    const qb = OAuthOTPToken.createQueryBuilder().update();

    if (this.lastSuccessfulAttemptToken === token) {
      qb.set({
        lastFailedAttemptToken: token,
        lastFailedAttempt: new Date(),
        failedAttempts: () => 'failed_attempts + 1',
      });
    } else {
      qb.set({
        lastFailedAttemptToken: token,
        lastFailedAttempt: new Date(),
        failedAttempts: 1,
      });
    }

    await qb.execute();
  }
}
