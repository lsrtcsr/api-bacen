import { IsNotEmpty } from 'class-validator';
import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { NotificationSchema, NotificationChannel } from '@bacen/base-sdk';
import { ExtendedEntity } from '../base';
import { Alert } from './Alert';

@Entity(Notification.tableName)
export class Notification extends ExtendedEntity implements NotificationSchema {
  private static readonly tableName = 'notifications';

  @IsNotEmpty()
  @ManyToOne(parent => Alert, alert => alert.notifications, {
    cascade: ['insert', 'update'],
    onDelete: 'SET NULL',
    nullable: true,
  })
  source: Alert;

  @IsNotEmpty()
  @Column({ type: 'enum', enum: NotificationChannel, nullable: false })
  channel: NotificationChannel;

  @IsNotEmpty()
  @Column({ nullable: false })
  destination: string;

  @IsNotEmpty()
  @Column({ default: false, nullable: false })
  confirmed: boolean = false;

  @IsNotEmpty()
  @Column('timestamp with time zone', { nullable: false })
  sentAt: Date;

  constructor(data: Partial<Notification> = {}) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Notification> {
    return {
      id: this.id,
      source: this.source.toJSON(),
      channel: this.channel,
      destination: this.destination,
      confirmed: this.confirmed,
      sentAt: this.sentAt,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
