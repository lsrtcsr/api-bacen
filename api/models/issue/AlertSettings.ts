import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { SeverityLevel, AlertSettingsSchema, Subscription } from '@bacen/base-sdk';

export class AlertSettings implements AlertSettingsSchema {
  @IsNotEmpty()
  limitType?: 'upperLimit' | 'lowerLimit' = 'upperLimit';

  @IsNotEmpty()
  threshold: SeverityLevel;

  // in minutes
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  timeWindow: number = 0;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  value: number = 0;

  subscriptions: Subscription[];

  constructor(data?: Partial<AlertSettings>) {
    if (!data) return;
    Object.assign(this, data);
  }
}
