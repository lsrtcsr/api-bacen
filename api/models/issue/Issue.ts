import { IsNotEmpty, IsOptional } from 'class-validator';
import { Column, Entity, OneToMany, ManyToOne, AfterInsert } from 'typeorm';
import { IssueType, IssueCategory, SeverityLevel, IssueSchema, IssueDetails } from '@bacen/base-sdk';
import { Logger } from 'nano-errors';
import { ExtendedEntity } from '../base';
import { IssueAlertService } from '../../services';

@Entity(Issue.tableName)
export class Issue extends ExtendedEntity implements IssueSchema {
  static readonly tableName = 'issues';

  @Column('enum', { nullable: false, enum: IssueType })
  type: IssueType;

  @Column('enum', { nullable: false, enum: IssueCategory })
  category: IssueCategory;

  @Column('enum', { nullable: false, default: SeverityLevel.NORMAL, enum: SeverityLevel })
  severity: SeverityLevel;

  @IsNotEmpty()
  @Column({ name: 'component_id', nullable: true })
  componentId: string;

  @IsNotEmpty()
  @Column({ type: 'jsonb', default: {}, nullable: false })
  details: IssueDetails;

  @IsOptional()
  @Column({ nullable: true })
  description?: string;

  @IsOptional()
  @Column({ name: 'reason_code', nullable: true })
  reasonCode?: string;

  @IsOptional()
  @ManyToOne(
    parent => Issue,
    parent => parent.id,
  )
  parent?: Issue;

  @IsOptional()
  @OneToMany(
    child => Issue,
    child => child.id,
  )
  child?: Issue;

  @IsOptional()
  @Column({ nullable: true })
  resource_name?: string;

  @IsOptional()
  @Column({ nullable: true })
  resource_id?: string;

  constructor(data: Partial<Issue> = {}) {
    super(data);
    Object.assign(this, data);
  }

  @AfterInsert()
  async notify() {
    try {
      await IssueAlertService.getInstance().notify(this);
    } catch (error) {
      Logger.getInstance().error(error);
    }
  }

  public toJSON(): Partial<Issue> {
    return {
      id: this.id,
      details: this.details,
      type: this.type,
      severity: this.severity,
      category: this.category,
      description: this.description,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
