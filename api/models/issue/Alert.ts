import { IsOptional } from 'class-validator';
import { Column, DeepPartial, Entity, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { AlertSchema, IssueType, IssueCategory } from '@bacen/base-sdk';
import { ExtendedEntity } from '../base';
import { User } from '../user';
import { AlertSettings } from './AlertSettings';
import { Notification } from './Notification';

@Entity(Alert.tableName)
export class Alert extends ExtendedEntity implements AlertSchema {
  private static readonly tableName = 'alerts';

  @IsOptional()
  @Column({ name: 'issue_types', type: 'enum', enum: IssueType, array: true, nullable: true })
  issueTypes?: IssueType[];

  @IsOptional()
  @Column({ name: 'issue_categories', type: 'enum', enum: IssueCategory, array: true, nullable: true })
  issueCategories?: IssueCategory[];

  @Column('jsonb', { nullable: false })
  settings: AlertSettings = new AlertSettings();

  @ManyToOne(type => User, user => user.alerts, { cascade: ['insert', 'update'], nullable: true })
  @JoinColumn({ name: 'created_by' })
  createdBy?: User;

  @OneToMany(type => Notification, notification => notification.source, {
    cascade: ['insert', 'update'],
    onDelete: 'SET NULL',
  })
  notifications?: Notification[];

  constructor(data: Partial<Alert> = {}) {
    super(data);
    Object.assign(this, data);
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Alert> {
    return {
      id: this.id,
      issueTypes: this.issueTypes && this.issueTypes.length ? this.issueTypes : undefined,
      issueCategories: this.issueCategories && this.issueCategories.length ? this.issueCategories : undefined,
      settings: this.settings,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      createdBy: this.createdBy,
    };
  }
}
