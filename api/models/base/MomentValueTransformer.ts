import * as moment from 'moment';
import { ValueTransformer } from 'typeorm';

export class MomentValueTransformer implements ValueTransformer {
  to(value: moment.Moment): Date | undefined {
    return value ? value.toDate() : undefined;
  }

  from(value: Date): moment.Moment | undefined {
    return value ? moment(value) : undefined;
  }
}
