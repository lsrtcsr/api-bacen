import { validate, ValidationError, ValidatorOptions } from 'class-validator';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeepPartial,
  EntityManager,
  FindManyOptions,
  FindOneOptions,
  InsertResult,
  IsNull,
  ObjectID,
  ObjectType,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  UpdateResult,
} from 'typeorm';
import { Pagination, PaginationData } from '@bacen/base-sdk';
import { FindConditions } from './FindConditions';

export interface ExtendedEntitySchema {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}

export interface InsertAndFindOptions<T extends ExtendedEntity> {
  manager?: EntityManager;
  findOptions?: FindOneOptions<T>;
}

export interface UpdateAndFindOptions<T extends ExtendedEntity> {
  manager?: EntityManager;
  findOptions?: FindOneOptions<T>;
}

export type SafeFindOneOptions<T> = Omit<FindOneOptions<T>, 'where'> & { where?: FindConditions<T> };
export type SafeFindManyOptions<T> = Omit<FindManyOptions<T>, 'where'> & { where?: FindConditions<T> };

export interface FindByOptions {
  manager?: EntityManager;
  relations?: string[];
}

const findOptionsWithoutSoftDeleted = <T extends ExtendedEntity>(
  options: SafeFindOneOptions<T> | SafeFindManyOptions<T> = {},
): SafeFindOneOptions<T> | SafeFindManyOptions<T> => {
  // TODO: Fix typing
  options.where = { ...options.where, deletedAt: IsNull() };
  return options;
};

export abstract class ExtendedEntity extends BaseEntity implements ExtendedEntitySchema {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ type: 'timestamp with time zone', name: 'created_at', default: () => 'clock_timestamp()' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', name: 'updated_at', default: () => 'clock_timestamp()' })
  updatedAt: Date;

  @Column({ type: 'timestamp with time zone', name: 'deleted_at', nullable: true })
  deletedAt?: Date;

  constructor(data: Partial<ExtendedEntity> = {}) {
    super();

    Object.assign(this, data);
  }

  /**
   * Find all non soft deleted entities
   * Accepts all TypeORM options except string wheres
   */
  public static safeFind<T extends ExtendedEntity>(
    this: ObjectType<T>,
    options: SafeFindManyOptions<T> = {},
  ): Promise<T[]> {
    return BaseEntity.find.call(this, findOptionsWithoutSoftDeleted(options));
  }

  /**
   * Find all non soft deleted entities
   * Accepts all TypeORM options except string wheres
   */
  public static async safePaginatedFind<T extends ExtendedEntity>(
    this: ObjectType<T>,
    options: SafeFindManyOptions<T> & { pagination: Pagination },
  ): Promise<[T[], PaginationData]> {
    const { skip, limit } = options.pagination;

    const paginatedOptions = { ...options, skip, take: limit, where: options.where };

    const queries: [Promise<T[]>, Promise<number>] = [
      ExtendedEntity.safeFind.call(this, paginatedOptions),
      ExtendedEntity.safeCount.call(this, options),
    ];

    const [results, count] = await Promise.all(queries);

    const paginationData: PaginationData = {
      dataLength: count,
      dataLimit: limit,
      dataSkip: skip,
    };

    return [results, paginationData];
  }

  /**
   * Find one non soft deleted entity
   * Accepts all TypeORM options except string wheres
   */
  public static safeFindOne<T extends ExtendedEntity>(
    this: ObjectType<T>,
    options: SafeFindOneOptions<T> = {},
  ): Promise<T> {
    return BaseEntity.findOne.call(this, findOptionsWithoutSoftDeleted(options));
  }

  /**
   * Count all non soft deleted entities
   * Accepts all TypeORM options except string wheres
   */
  public static safeCount<T extends ExtendedEntity>(
    this: ObjectType<T>,
    options?: SafeFindManyOptions<T>,
  ): Promise<number> {
    return BaseEntity.count.call(this, findOptionsWithoutSoftDeleted(options));
  }

  /**
   * Update a entity without changing the default updatedAt and createdAt values
   * You should use this method if you need to pass a full entity as the payload for an update
   */
  public static async safeUpdate<T extends ExtendedEntity>(
    this: ObjectType<T>,
    criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<T>,
    payload: DeepPartial<T>,
    manager?: EntityManager,
  ): Promise<T> {
    // Don't try and override the default values
    delete payload.createdAt;
    delete payload.updatedAt;

    const schema = BaseEntity.create.call(this, payload);
    delete schema.id;

    // Remove undefineds
    // https://github.com/typeorm/typeorm/issues/2331
    Object.keys(schema).forEach(key => schema[key] === undefined && delete schema[key]);

    let updateResult: UpdateResult;
    if (manager) {
      updateResult = await manager.update(this, criteria, schema);
    } else {
      updateResult = await BaseEntity.update.call(this, criteria, schema);
    }

    return BaseEntity.create.call(this, updateResult.generatedMaps[0]);
  }

  /**
   * Insert an entity to the database and find it back to ensure it's the most updated version possible
   */
  public static async insertAndFind<T extends ExtendedEntity>(
    this: ObjectType<T>,
    payload: DeepPartial<T>,
    options: InsertAndFindOptions<T> = {},
  ): Promise<T> {
    const schema = BaseEntity.create.call(this, payload);

    let result: InsertResult;
    if (options.manager) {
      result = await options.manager.insert(this, schema);
    } else {
      result = await BaseEntity.insert.call(this, schema);
    }

    const { id } = result.identifiers[0];

    if (options.manager) {
      return options.manager.findOne(this, id, options.findOptions);
    }

    return ExtendedEntity.safeFindOne.call(this, { where: { id } });
  }

  /**
   * Safely update an entity to the database and find it back to ensure it's the most updated version possible
   */
  public static async updateAndFind<T extends ExtendedEntity>(
    this: ObjectType<T>,
    id: string,
    payload: DeepPartial<T>,
    options: InsertAndFindOptions<T> = {},
  ): Promise<T> {
    await ExtendedEntity.safeUpdate.call(this, id, payload);

    if (options.manager) {
      return options.manager.findOne(this, id, options.findOptions);
    }

    return ExtendedEntity.safeFindOne.call(this, { where: { id } });
  }

  /**
   * Soft delete a entity by it's uuid
   *
   * @todo Cascade soft delete to relations
   */
  public static softDelete<T extends ExtendedEntity>(id: string): Promise<UpdateResult> {
    // We must use any as DeepPartial breaks things for reasons unkown
    return BaseEntity.update.call(this, id, { deletedAt: new Date() } as any);
  }

  public validate(validatorOptions?: ValidatorOptions): Promise<ValidationError[]> {
    // TODO: Find out why forbidUnkownValues always fails with nested validation
    return validate(this, { forbidUnknownValues: false, ...validatorOptions });
  }

  /**
   * Typeorm's save method is way too clever for it's own good, and keeps changing data in ways you don't expect.
   * After so many different critical bugs caused by saving over data we didn't want to change,
   * we decided to just stop using it.
   *
   * @deprecated
   */
  public async save() {
    throw new Error("Don't use the save method.");
    return this;
  }
}
