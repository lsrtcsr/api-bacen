import { ExtendedEntity } from './ExtendedEntity';

export abstract class ExtendedStateEntity<Status> extends ExtendedEntity {
  abstract status: Status;

  abstract additionalData?: any;

  public toSimpleJSON(): Partial<ExtendedStateEntity<Status>> {
    return {
      additionalData: this.additionalData,
      status: this.status,
      id: this.id,
    };
  }

  public toJSON(): Partial<ExtendedStateEntity<Status>> {
    return {
      additionalData: this.additionalData,
      updatedAt: this.updatedAt,
      createdAt: this.createdAt,
      status: this.status,
      id: this.id,
    };
  }
}

export abstract class ExtendedStatefulEntity<Status, State extends ExtendedStateEntity<Status>> extends ExtendedEntity {
  abstract states?: State[];

  /**
   * The status of the entity based on latest state, if available.
   */
  get status() {
    const states = this.getStates();
    if (states) {
      return states[0].status;
    }
  }

  /**
   * Get entity states ordered by creation date.
   */
  public getStates(): State[] | undefined {
    if (this.states && this.states.length) {
      return this.states.sort((a, b) => {
        if (b.createdAt && a.createdAt) {
          return b.createdAt.getTime() - a.createdAt.getTime();
        }
        return 0;
      });
    }
  }
}
