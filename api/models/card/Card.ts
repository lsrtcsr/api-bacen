import { CardSchema, CardStatus, IssueDetails, IssueType, IssueCategory, SeverityLevel } from '@bacen/base-sdk';
import * as Package from 'pjson';
import { Column, DeepPartial, Entity, ManyToOne, OneToMany } from 'typeorm';
import { ExtendedEntity } from '../base';
import { Payment } from '../payment';
import { Transaction } from '../transaction/Transaction';
import { Wallet } from '../wallet/Wallet';
import { IssueHandler } from '../../services';

@Entity(Card.TABLE_NAME)
export class Card extends ExtendedEntity implements CardSchema {
  private static readonly TABLE_NAME = 'cards';

  @ManyToOne(
    type => Wallet,
    wallet => wallet.cards,
    { onDelete: 'CASCADE' },
  )
  wallet: Wallet;

  @OneToMany(
    type => Payment,
    payment => payment.card,
    { onDelete: 'SET NULL' },
  )
  payments: Payment[];

  @Column({ nullable: false, default: 'root' })
  asset: string;

  @Column('enum', { nullable: false, enum: CardStatus })
  status: CardStatus;

  @Column({ nullable: false })
  virtual: boolean;

  @Column({ name: 'activated_at', nullable: true })
  activatedAt?: Date;

  @Column({ name: 'cancelled_at', nullable: true })
  cancelledAt?: Date;

  constructor(data: Partial<Card> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public static async associatePayment(payment: Payment): Promise<void> {
    if (!payment || !payment.transaction) return;

    const transaction = await Transaction.findOne({ where: { id: payment.transaction.id } });
    if (!transaction.additionalData || !transaction.additionalData.card_id) return;

    const card = await Card.safeFindOne({ where: { id: transaction.additionalData.card_id } });
    if (!card) {
      await IssueHandler.getInstance().handle({
        type: IssueType.UNKNOWN,
        category: IssueCategory.PAYMENT,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        description: 'Error associating payment to card: '.concat(
          `card with id ${transaction.additionalData.card_id} not found!`,
        ),
        details: IssueDetails.from(payment),
      });

      return;
    }

    await Payment.update(payment.id, { card: card.id as any });
  }

  public toJSON(): DeepPartial<Card> {
    return {
      wallet: this.wallet && this.wallet.toJSON ? this.wallet.toJSON() : this.wallet,
      asset: this.asset,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      status: this.status,
      virtual: this.virtual,
      id: this.id,
    };
  }
}
