import { Column, Entity, ManyToOne } from 'typeorm';
import { ExtendedEntity, User } from '..';
import { OAuthAccessToken } from '../oauth2';

export enum LogType {
  AUTHORIZATION = 'authorization',
}

@Entity(RequestLog.tableName)
export class RequestLog extends ExtendedEntity {
  static readonly tableName = 'request_logs';

  @ManyToOne(_ => OAuthAccessToken, { nullable: false })
  token: OAuthAccessToken;

  @Column({ type: 'enum', enum: LogType, nullable: false })
  type: LogType;

  @Column('jsonb', { nullable: true })
  request: any;

  @Column('jsonb', { nullable: true })
  response: any;
}
