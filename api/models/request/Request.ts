import { Entity, Column, Index } from 'typeorm';
import { ExtendedEntity } from '../base';

@Entity(Request.tableName)
export class Request extends ExtendedEntity {
  protected static tableName = 'requests';

  @Index({ unique: true, where: 'deleted_at IS NULL' })
  @Column('text', { nullable: false })
  idempotenceKey: string;

  @Column('int', { nullable: false })
  status: number;

  @Column('text', { nullable: false, default: 'plain/text' })
  contentType: string;

  @Column('jsonb', { nullable: false, default: {} })
  body: object;

  constructor(data: Partial<Request>) {
    super(data);
    Object.assign(this, data);
  }
}
