import { BaseRequest } from 'ts-framework';
import { Domain } from '../domain';
import { OAuthAccessToken } from '../oauth2';
import { User } from '../user';
import { Payment } from '../payment';
import { Transaction } from '../transaction';

export interface bacenRequest extends BaseRequest {
  user?: User;

  accessToken?: OAuthAccessToken;

  serviceFee?: Partial<Payment>;

  transaction?: Transaction;

  instanceId?: string;

  ispb?: string;

   ?: {
    domain?: Domain;
  };

  // Request cache api
  cache: {
    get<T>(key: string): T | undefined;
    set<T>(key: string, obj: T): void;
  };

  file: Express.Multer.File;
}
