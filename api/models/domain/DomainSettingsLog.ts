import { DomainRole } from '@bacen/base-sdk';
import { IsNotEmpty } from 'class-validator';
import { Column, Entity } from 'typeorm';
import { ExtendedEntity } from '../base';
import { DomainSettingType } from './DomainSettings';

export const settingsToSave = [DomainSettingType.CCS_NUMBER_DAYS_INTERVAL];

@Entity(DomainSettingsLog.tableName)
export class DomainSettingsLog extends ExtendedEntity {
  private static readonly tableName = 'domain_settings_logs';

  @IsNotEmpty()
  @Column({ nullable: false })
  setting: string;

  @Column({ nullable: false })
  userId: string;

  @Column()
  oldValue: number;

  @Column()
  newValue: number;

  constructor(data: Partial<DomainSettingsLog> = {}) {
    super(data);

    Object.assign(this, data);
  }
}
