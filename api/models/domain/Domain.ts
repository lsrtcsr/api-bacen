import { DomainRole, DomainSchema } from '@bacen/base-sdk';
import { IsArray, IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';
import { Column, DeepPartial, Entity, OneToMany, EntityManager, Index } from 'typeorm';
import { ExtendedEntity } from '../base';
import { OAuthClient } from '../oauth2';
import { User } from '../user';
import { DomainSettings } from './DomainSettings';
import { Contract, Plan } from '../billing';
import { CacheService } from '../../services';
import Config from '../../../config';

@Entity(Domain.tableName)
export class Domain extends ExtendedEntity implements DomainSchema {
  private static readonly tableName = 'domains';

  @IsNotEmpty()
  @Column('text', { nullable: false })
  name: string;

  // TODO
  // @IsNotEmpty()
  // @IsIn(Object.values(DomainRole), { message: "$value should be of type Domain Role" })
  @Index('domain_role_root_unique', { unique: true, where: `role = '${DomainRole.ROOT}'` })
  @Index('domain_role_default_unique', { unique: true, where: `role = '${DomainRole.DEFAULT}'` })
  @Column({ nullable: false, default: DomainRole.COMMON, type: 'enum', enum: DomainRole })
  role: DomainRole;

  @IsArray()
  @IsOptional()
  @IsNotEmpty({ each: true })
  @Column('text', { array: true, nullable: true })
  urls?: string[];

  @IsArray()
  @IsOptional()
  @IsNotEmpty({ each: true })
  @Column('jsonb', { name: 'postback_urls', nullable: true, default: [] })
  postbackUrls?: string[];

  @IsArray()
  @IsOptional()
  @IsNotEmpty({ each: true })
  @Column('jsonb', { name: 'system_postback_urls', nullable: true, default: [] })
  systemPostbackUrls?: string[];

  get postbackUrl(): string {
    return this.postbackUrls && this.postbackUrls[0];
  }

  @OneToMany(
    type => User,
    user => user.domain,
    {
      cascade: ['insert', 'update'],
      onDelete: 'SET NULL',
    },
  )
  users?: User[];

  @OneToMany(
    type => Contract,
    contract => contract.supplier,
  )
  contracts: Contract[];

  @OneToMany(
    type => Plan,
    plan => plan.supplier,
  )
  plans: Plan[];

  @OneToMany(
    type => OAuthClient,
    client => client.domain,
    {
      cascade: ['insert', 'update'],
      onDelete: 'SET NULL',
    },
  )
  clients?: OAuthClient[];

  @ValidateNested()
  @Column('jsonb', { default: {} })
  settings: DomainSettings;

  constructor(data: Partial<Domain> = {}) {
    super(data);

    Object.assign(this, data);

    if (data.settings) {
      this.settings =
        data.settings && data.settings instanceof DomainSettings ? data.settings : new DomainSettings(this.settings);
    }
  }

  /**
   * Gets the root domain in the database.
   */
  public static async getRootDomain(options: { manager?: EntityManager } = {}): Promise<Domain> {
    const query = options.manager
      ? options.manager.createQueryBuilder(Domain, 'domain')
      : Domain.createQueryBuilder('domain');

    return query
      .select()
      .where({ role: DomainRole.ROOT })
      .getOne();
  }

  /**
   * Gets the default domain in the database.
   */
  public static async getDefaultDomain(options: { manager?: EntityManager } = {}): Promise<Domain> {
    const cacheService = CacheService.getInstance();
    const cacheKey = 'default_domain';

    const cachedResult = await cacheService.get(cacheKey);
    let defaultDomain: Domain;

    if (cachedResult) {
      defaultDomain = Domain.create(cachedResult);
    } else {
      const query = options.manager
        ? options.manager.createQueryBuilder(Domain, 'domain')
        : Domain.createQueryBuilder('domain');

      defaultDomain = await query
        .select()
        .where({ role: DomainRole.DEFAULT })
        .getOne();

      cacheService.set(cacheKey, JSON.stringify(defaultDomain), Config.database.cacheTimeout / 1000);
    }

    return defaultDomain;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Domain> {
    return {
      // Relations
      users: this.users && this.users.map(user => user.toJSON()),
      settings: this.settings,
      postbackUrls: this.postbackUrls,
      updatedAt: this.updatedAt,
      createdAt: this.createdAt,
      urls: this.urls,
      role: this.role,
      name: this.name,
      id: this.id,
    };
  }

  public toSimpleJSON(): DeepPartial<Domain> {
    return {
      role: this.role,
      name: this.name,
      id: this.id,
    };
  }
}
