import { DomainSettingsLocksSchema } from '@bacen/base-sdk';
import { IsNotEmpty, IsNumber, Min, ValidateNested } from 'class-validator';
import dictConfig from '../../../config/dict.config';

export enum DomainSettingType {
  CARD_EXPIRATION_TIME = 'card_expiration_time',
  TOTAL_ACTIVE_CARDS = 'total_active_cards',
  TOTAL_SINGLE_TRANSACTION_VALUE = 'total_single_transaction_value',
  TOTAL_RECENT_TRANSACTIONS_VALUE_MONTHLY = 'total_recent_transactions_value_monthly',
  TOTAL_RECENT_TRANSACTIONS_VALUE_WEEKLY = 'total_recent_transactions_value_weekly',
  TOTAL_RECENT_TRANSACTIONS_VALUE_DAILY = 'total_recent_transactions_value_daily',
  TOTAL_SENT_TRANSACTIONS_VALUE_MONTHLY = 'total_sent_transactions_value_monthly',
  TOTAL_SENT_TRANSACTIONS_VALUE_WEEKLY = 'total_sent_transactions_value_weekly',
  TOTAL_SENT_TRANSACTIONS_VALUE_DAILY = 'total_sent_transactions_value_daily',
  TOTAL_RECEIVED_TRANSACTIONS_VALUE_MONTHLY = 'total_received_transactions_value_monthly',
  TOTAL_RECEIVED_TRANSACTIONS_VALUE_WEEKLY = 'total_received_transactions_value_weekly',
  TOTAL_RECEIVED_TRANSACTIONS_VALUE_DAILY = 'total_received_transactions_value_daily',
  TOTAL_MOBILE_CREDITS_DAILY = 'total_mobile_credits_daily',
  CCS_NUMBER_DAYS_INTERVAL = 'ccs_number_days_interval',
}

const DEFAULT_CARD_EXPIRATION_TIME = 24; // in months
const DEFAULT_TOTAL_ACTIVE_CARDS = 3;
const DEFAULT_TOTAL_SINGLE_TRANSACTION_VALUE = 0;
const DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_MONTHLY = 0;
const DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_WEEKLY = 0;
const DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_DAILY = 0;
const DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_MONTHLY = 0;
const DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_WEEKLY = 0;
const DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_DAILY = 0;
const DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_MONTHLY = 0;
const DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_WEEKLY = 0;
const DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_DAILY = 0;
const DEFAULT_TOTAL_MOBILE_CREDITS_DAILY = 10;
const DEFAULT_CCS_NUMBER_DAYS_INTERVAL = 1;

export class DomainSettingsLocks implements DomainSettingsLocksSchema {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  card_expiration_time = DEFAULT_CARD_EXPIRATION_TIME;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_single_transaction_value = DEFAULT_TOTAL_SINGLE_TRANSACTION_VALUE;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_mobile_credits_daily = DEFAULT_TOTAL_MOBILE_CREDITS_DAILY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_recent_transactions_value_monthly = DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_MONTHLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_recent_transactions_value_weekly = DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_WEEKLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_recent_transactions_value_daily = DEFAULT_TOTAL_RECENT_TRANSACTIONS_VALUE_DAILY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_sent_transactions_value_monthly = DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_MONTHLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_sent_transactions_value_weekly = DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_WEEKLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_sent_transactions_value_daily = DEFAULT_TOTAL_SENT_TRANSACTIONS_VALUE_DAILY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_received_transactions_value_monthly = DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_MONTHLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_received_transactions_value_weekly = DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_WEEKLY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_received_transactions_value_daily = DEFAULT_TOTAL_RECEIVED_TRANSACTIONS_VALUE_DAILY;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  total_active_cards = DEFAULT_TOTAL_ACTIVE_CARDS;

  @IsNotEmpty()
  ccs_number_days_interval = DEFAULT_CCS_NUMBER_DAYS_INTERVAL;

  constructor(data: Partial<DomainSettingsLocks> = {}) {
    Object.assign(this, data);
  }

  public update(type: DomainSettingType, value: number) {
    const success = (this[type] = value);
    if (!success) throw new Error('Setting update failed');
  }
}

export class DomainSettingsFees {
  [key: string]: {
    fixed: string;
    percentage: number;
  };

  constructor(data: Partial<DomainSettingsFees> = {}) {
    Object.assign(this, data);
  }
}

export class DomainSettings {
  logo?: string;

  primaryColor?: string;

  tintColor?: string;

  fees: DomainSettingsFees = new DomainSettingsFees();

  @ValidateNested()
  locks: DomainSettingsLocks = new DomainSettingsLocks();

  ispb: string;

  constructor(data: Partial<DomainSettings> = {}) {
    // As this is a ever changing schema, assigning each property individually
    // would create lots of "forgot to add to constructor" situations
    // So we assign all own props at once with this trick
    Object.assign(this, data);
    this.fees = new DomainSettingsFees(data.fees);
    this.locks = new DomainSettingsLocks(data.locks);
    this.ispb = data.ispb || dictConfig.ispb;
  }
}
