import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { Entity, Column, ManyToOne, Index } from 'typeorm';
import { ExtendedStateEntity } from '../base';
import { AssetRegistration } from './AssetRegistration';

@Entity(AssetRegistrationState.tableName)
export class AssetRegistrationState extends ExtendedStateEntity<AssetRegistrationStatus> {
  public static readonly tableName = 'asset_registration_states';

  @Column('enum', { enum: AssetRegistrationStatus, nullable: false })
  public status: AssetRegistrationStatus;

  @Column('jsonb', { default: {}, nullable: false })
  additionalData: any;

  @Index()
  @ManyToOne(
    () => AssetRegistration,
    assetRegistration => assetRegistration.states,
    { onDelete: 'CASCADE' },
  )
  assetRegistration: AssetRegistration;
}
