import { SlackMessage } from 'ts-framework-notification';
import { logger } from '../../../config/logger.config';
import slackConfig from '../../../config/slack.config';
import { SlackService } from '../../services';
import { WalletNotificationItem } from '../wallet';

// tslint:disable-next-line:max-line-length
export const notifyAssetInSlack = (
  operation: 'emitted' | 'destroyed',
  item: WalletNotificationItem,
  asset,
  wallet,
  options,
) =>
  SlackService.getInstance()
    .send(
      new SlackMessage({
        to: item.to,
        text: '',
        webhookUrl: slackConfig.defaultChannel,
        username: '@bacen',
        attachments: [
          {
            fallback: `Asset ${operation} to your wallet: ${options.amount} ${asset.code}`,
            pretext: `Asset ${operation} to your wallet`,
            color: 'info',
            fields: [
              {
                title: 'Amount',
                value: `${operation === 'emitted' ? '' : '-'}${options.amount} ${asset.code}`,
              },
              {
                title: 'Wallet ID',
                value: `\`${wallet.id}\``,
              },
              {
                title: 'Additional Data',
                value: `
\`\`\`
${JSON.stringify(options.additionalData || {}, null, 2)}
\`\`\`
            `,
              },
            ],
          },
        ],
      }),
    )
    .catch(logger.error);
