import { AssetRegistrationStatus, IsApprovedResponse } from '@bacen/base-sdk';
import {
  Check,
  Column,
  DeepPartial,
  Entity,
  EntityManager,
  getManager,
  In,
  Index,
  ManyToOne,
  OneToMany,
  RelationId,
  Unique,
} from 'typeorm';
import { AssetRegistrationStateMachine } from '../../fsm/assetRegistration';
import { ProviderManagerService } from '../../services';
import { MaybeArray } from '../../utils';
import { ExtendedStatefulEntity } from '../base';
import { Wallet } from '../wallet/Wallet';
import { Asset } from './Asset';
import { AssetRegistrationState } from './AssetRegistrationState';

@Entity(AssetRegistration.tableName)
@Unique('asset_registration_asset_wallet_unique', ['asset', 'wallet'])
@Check('blocked_balance_is_non_negative', 'blocked_balance >= 0')
export class AssetRegistration extends ExtendedStatefulEntity<AssetRegistrationStatus, AssetRegistrationState> {
  public static readonly tableName = 'asset_registrations';

  @OneToMany(() => AssetRegistrationState, (state) => state.assetRegistration)
  states: AssetRegistrationState[];

  @Index()
  @ManyToOne(() => Asset, (asset) => asset.registrations)
  asset: Asset;

  @RelationId((registration: AssetRegistration) => registration.asset)
  assetId: string;

  @Index()
  @ManyToOne(() => Wallet, (wallet) => wallet.assetRegistrations)
  wallet?: Wallet;

  @RelationId((registration: AssetRegistration) => registration.wallet)
  walletId: string;

  @Column('decimal', { name: 'blocked_balance', default: 0, nullable: false })
  blockedBalance: string;

  @Column('uuid', { nullable: true })
  reportId?: string;

  constructor(data: DeepPartial<AssetRegistration> = {}) {
    super(data as Partial<AssetRegistration>);
    Object.assign(this, data);
  }

  public static from(wallet: Wallet, asset: Asset, manager?: EntityManager): Promise<AssetRegistration>;

  public static from(wallet: Wallet, assets: Asset[], manager?: EntityManager): Promise<AssetRegistration[]>;

  public static async from(
    wallet: Wallet,
    assets: MaybeArray<Asset>,
    manager = getManager(),
  ): Promise<AssetRegistration | AssetRegistration[]> {
    const isArray = Array.isArray(assets);

    const registrations = isArray
      ? (assets as Asset[]).map((asset) => AssetRegistration.create({ wallet, asset }))
      : [AssetRegistration.create({ wallet, asset: assets as Asset })];

    const insertResult = await manager.insert(AssetRegistration, registrations);
    const ids: string[] = insertResult.identifiers.map((identifier) => identifier.id);

    await manager.insert(
      AssetRegistrationState,
      ids.map((id) => ({
        assetRegistration: { id },
        status: AssetRegistrationStatus.PENDING,
      })),
    );

    if (isArray) {
      return manager.find(AssetRegistration, {
        where: { id: In(ids) },
        relations: ['states', 'asset', 'asset.issuer'],
      });
    }

    return manager.findOne(AssetRegistration, ids[0], { relations: ['states', 'asset', 'asset.issuer'] });
  }

  /**
   * Checks wheter there is an active AssetRegistration between an asset-wallet pair
   * @param wallet The wallet which we want to check if a given asset is registered.
   * @param asset The asset we want to check is registered on a given wallet.
   */
  public static async isRegistered(wallet: Wallet, asset: Asset): Promise<boolean> {
    const registration = await AssetRegistration.safeFindOne({
      where: { wallet, asset },
      relations: ['states'],
    });

    return registration?.status === AssetRegistrationStatus.READY;
  }

  /**
   * Tries registering a pending assetRegistration.
   */
  public async registerPending(): Promise<boolean> {
    const fsm = new AssetRegistrationStateMachine(this);

    const didProcess = await fsm.goTo(AssetRegistrationStatus.REGISTERED_IN_PROVIDER);
    if (!didProcess) return false;

    const docsSubmitted = await fsm.goTo(AssetRegistrationStatus.PENDING_PROVIDER_APPROVAL);
    if (!docsSubmitted) return false;

    const didRegister = await fsm.goTo(AssetRegistrationStatus.READY);
    if (!didRegister) return false;

    return true;
  }

  public async isApproved(): Promise<IsApprovedResponse> {
    const asset = this.asset || (await Asset.findOne(this.assetId));

    let wallet: Wallet;
    if (!this.wallet || !this.wallet.user) {
      wallet = await Wallet.findOne({ where: { id: this.walletId }, relations: ['user'] });
    } else {
      wallet = this.wallet;
    }

    if (asset.provider) {
      const provider = ProviderManagerService.getInstance().from(asset.provider);
      return provider.isApproved(wallet);
    }

    return { status: true };
  }

  public toJSON(setWallet = true) {
    // call status before states so that states array may be sorted most-recent first.
    const { status } = this;

    return {
      states: this.states ? this.states.map((state) => state.toJSON()) : undefined,
      deletedAt: this.deletedAt,
      updatedAt: this.updatedAt,
      createdAt: this.createdAt,
      reportId: this.reportId,
      asset: this.asset ? this.asset.toSimpleJSON() : undefined,
      wallet: this.wallet && setWallet ? this.wallet.toJSON() : undefined,
      status,
      id: this.id,
    };
  }
}
