import {
  AssetSchema,
  CustodyProvider,
  PaymentType,
  WalletStatus,
  PaymentStatus,
  AssetRegistrationStatus,
} from '@bacen/base-sdk';
import { IsUppercase, Length } from 'class-validator';
import { Column, DeepPartial, Entity, EntityManager, getManager, ManyToOne, OneToMany, AfterLoad, In } from 'typeorm';
import { BaseError } from 'nano-errors';
import { ExtendedEntity, FindByOptions } from '../base';
import { OAuthAccessToken } from '../oauth2';
import { Payment } from '../payment';
import { Transaction } from '../transaction';
import { Wallet } from '../wallet/Wallet';
import { notifyAssetInSlack } from './notificationUtil';
import { AssetRegistration } from './AssetRegistration';
import { AssetService } from '../../services';
import { isUUID } from '../../utils';
import { Banking } from '../consumer';

export interface EmitOrDestroyOptions {
  amount?: string;
  scheduleFor?: Date;
  paymentType?: PaymentType;
  createdBy?: OAuthAccessToken;
  targetWallet?: Wallet;
  status?: PaymentStatus;
  serviceFee?: Partial<Payment>;
  additionalData?: Transaction['additionalData'];
  banking?: Banking;
  manager?: EntityManager;
  bypassReversalCheck?: boolean;
}

@Entity(Asset.tableName)
export class Asset extends ExtendedEntity implements AssetSchema {
  static readonly tableName = 'assets';

  @Column({ nullable: true })
  name?: string;

  @Length(3, 4)
  @IsUppercase()
  @Column({ nullable: false, unique: true })
  code: string;

  @ManyToOne((type) => Wallet, (wallet) => wallet.issuedAssets, { nullable: false })
  issuer: Wallet;

  @Column({ nullable: true, type: 'enum', enum: CustodyProvider })
  provider: CustodyProvider;

  @OneToMany(() => AssetRegistration, (registration) => registration.asset)
  registrations: AssetRegistration[];

  @OneToMany((type) => Payment, (payment) => payment.asset)
  payments: Payment[];

  // virtual property
  public wallets?: Wallet[];

  @AfterLoad()
  private populateWallets() {
    this.wallets =
      this.registrations && this.registrations.every((reg) => reg.wallet)
        ? this.registrations.map((reg) => reg.wallet!)
        : undefined;
  }

  constructor(data: Partial<Asset> = {}) {
    super(data);

    Object.assign(this, data);
  }

  /**
   * Gets the default assets.
   * Default assets are those that are registered to a wallet upon the wallet's creation
   */
  public static async getDefaultAssets(manager = getManager()): Promise<Asset[]> {
    const assetService = AssetService.getInstance();
    return manager.find(Asset, {
      where: { code: In(assetService.requiredAssets.map((asset) => asset.code)) },
      relations: ['issuer'],
    });
  }

  /**
   * Gets the root asset.
   */
  public static getRootAsset(options: FindByOptions = {}): Promise<Asset> {
    const { manager = getManager(), relations: relationsArg = [] } = options;
    const assetService = AssetService.getInstance();
    const relations = Array.from(new Set(['issuer', ...relationsArg]));

    const baseQuery = manager
      .createQueryBuilder(Asset, 'asset')
      .where('asset.code = :code', { code: assetService.rootAsset.code });

    for (const relation of relations) {
      baseQuery.leftJoinAndSelect(`asset.${relation}`, relation);
    }

    return baseQuery.getOne();
  }

  public static getAuthorizableAssets(options: FindByOptions = {}): Promise<Asset[] | undefined> {
    const { manager = getManager(), relations = [] } = options;
    const assetService = AssetService.getInstance();

    if (!assetService.authorizableAssets) return undefined;

    return manager.find(Asset, {
      where: { code: In(assetService.authorizableAssets.map((asset) => asset.code)) },
      relations: Array.from(new Set(['issuer', ...relations])),
      cache: true,
    });
  }

  /**
   * Returns the provider of the root asset from the configs
   */
  public static getRootAssetProvider(): CustodyProvider {
    const assetService = AssetService.getInstance();
    return assetService.rootAsset.provider;
  }

  /**
   * Returns the code of the root asset from the configs
   */
  public static getRootAssetCode(): string {
    const assetService = AssetService.getInstance();
    return assetService.rootAsset.code;
  }

  /**
   * Flag for checking if asset is root.
   */
  public get root(): boolean {
    const assetService = AssetService.getInstance();
    return assetService.rootAsset.code === this.code;
  }

  public static getByCode(code: string, options: FindByOptions = {}): Promise<Asset | undefined> {
    const { manager = getManager(), relations: relationsArg = [] } = options;

    if (code === 'root') {
      return Asset.getRootAsset(options);
    }

    const relations = Array.from(new Set(['issuer', ...relationsArg]));
    const baseQuery = manager.createQueryBuilder(Asset, 'asset').where('asset.code = :code', { code });

    for (const relation of relations) {
      baseQuery.leftJoinAndSelect(`asset.${relation}`, relation);
    }

    return baseQuery.getOne();
  }

  public static getByIdOrCode(idOrCode: string, options: FindByOptions = {}): Promise<Asset | undefined> {
    const { manager = getManager(), relations: relationsArg = [] } = options;

    if (isUUID(idOrCode)) {
      const relations = Array.from(new Set(['issuer', ...relationsArg]));
      const baseQuery = manager.createQueryBuilder(Asset, 'asset').where('asset.id = :id', { id: idOrCode });

      for (const relation of relations) {
        baseQuery.leftJoinAndSelect(`asset.${relation}`, relation);
      }

      return baseQuery.getOne();
    }

    return this.getByCode(idOrCode, options);
  }

  public static getByProvider(provider: CustodyProvider | 'root', manager = getManager()): Promise<Asset | undefined> {
    const desiredProvider = provider === 'root' ? this.getRootAssetProvider() : provider;
    return manager.findOne(Asset, { where: { provider: desiredProvider }, relations: ['issuer'] });
  }

  /**
   * Flag for checking if asset is emittable.
   */
  public get emittable(): boolean {
    return this.provider === null || this.provider === undefined;
  }

  /**
   * Flag for checking if an asset is required during onboarding.
   */
  public get required(): boolean {
    return this.root || !!AssetService.getInstance().requiredAssets?.find((a) => a.required && a.code === this.code);
  }

  /**
   * Creates a new emission transaction.
   */
  // tslint:disable-next-line:max-line-length
  public async emit(options: EmitOrDestroyOptions): Promise<Transaction> {
    if (!options.targetWallet) {
      throw new Error('Wallet supplied was not found or is invalid');
    }

    if (options.targetWallet.status === WalletStatus.PENDING || options.targetWallet.status === WalletStatus.FAILED) {
      throw new Error('Wallet is not ready for operation, check its status or contact the support');
    }

    const thisAssetRegistration = options.targetWallet.assetRegistrations?.find(
      (AssetRegistration) => AssetRegistration.asset.code === this.code,
    );

    if (!thisAssetRegistration || thisAssetRegistration.status !== AssetRegistrationStatus.READY) {
      throw new BaseError('Asset is not registered in wallet or is not currently available', {
        asset: this,
        wallet: options.targetWallet,
      });
    }

    const transaction = await Transaction.prepare(
      {
        createdBy: options.createdBy,
        banking: options.banking,
        additionalData: options.additionalData,
        source: this.issuer,
        recipients: [
          {
            amount: options.amount,
            asset: this,
            wallet: options.targetWallet,
            status: options.status,
          },
        ],
        type: options.paymentType || PaymentType.DEPOSIT,
        bypassReversalCheck: options.bypassReversalCheck,
        manager: options.manager,
      },
      false,
    );

    // TODO: Move this to service
    // Try to notify wallet if available
    if (options.targetWallet.additionalData) {
      const { notifications = [] } = options.targetWallet.additionalData;

      notifications.map((item) => {
        if (item.type === 'slack') {
          notifyAssetInSlack('emitted', item, this, options.targetWallet, options);
        }
      });
    }

    return transaction;
  }

  /**
   * Creates a new destruction transaction.
   *
   * @param amount The amount of the asset to be destroyed
   * @param sourceWallet The source wallet for the destruction
   * @param additionalData Any additional data for this operation
   */
  // tslint:disable-next-line:max-line-length
  public async destroy(options: EmitOrDestroyOptions): Promise<Transaction> {
    if (!options.targetWallet) {
      throw new Error('Wallet supplied was not found or is invalid');
    }

    // TODO: Should we allow wallets which are not ready (eg. registered_in_stellar, registered_in_provider, pending_provider_approval) to operate?
    if (options.targetWallet.status === WalletStatus.PENDING || options.targetWallet.status === WalletStatus.FAILED) {
      throw new Error('Wallet is not ready for operation, check its status or contact the support');
    }

    const thisAssetRegistration = options.targetWallet.assetRegistrations?.find(
      (AssetRegistration) => AssetRegistration.asset.code === this.code,
    );

    if (!thisAssetRegistration || thisAssetRegistration.status !== AssetRegistrationStatus.READY) {
      throw new BaseError('Asset is not registered in wallet or is not currently available', {
        asset: this,
        wallet: options.targetWallet,
      });
    }

    const recipients: any[] = options.amount
      ? [
          {
            amount: options.amount,
            asset: this,
            wallet: this.issuer,
            scheduleFor: options.scheduleFor,
            status: options.status,
          },
        ]
      : [];

    if (options.serviceFee) {
      recipients.push({ wallet: options.serviceFee.destination, ...options.serviceFee });
    }

    const transaction = await Transaction.prepare(
      {
        recipients,
        manager: options.manager,
        createdBy: options.createdBy,
        additionalData: options.additionalData,
        source: options.targetWallet,
        type: options.paymentType || PaymentType.WITHDRAWAL,
        banking: options.banking,
        bypassReversalCheck: options.bypassReversalCheck,
      },
      false,
    );

    // TODO: Move this to service
    // Try to notify wallet if available
    if (options.targetWallet.additionalData) {
      const { notifications = [] } = options.targetWallet.additionalData;

      notifications.map((item) => {
        if (item.type === 'slack') {
          notifyAssetInSlack('destroyed', item, this, options.targetWallet, options);
        }
      });
    }

    return transaction;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON() {
    return {
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      wallets: this.wallets ? this.wallets.map((wallet) => wallet.toSimpleJSON()) : undefined,
      issuer: this.issuer ? this.issuer.toJSON() : undefined,
      required: this.required,
      provider: this.provider,
      root: this.root,
      code: this.code,
      name: this.name,
      id: this.id,
    };
  }

  /**
   * Converts the instance to a small JSON to be returned from the public API.
   */
  public toSimpleJSON(): DeepPartial<Asset> {
    return {
      provider: this.provider,
      required: this.required,
      root: this.root,
      code: this.code,
      name: this.name,
      id: this.id,
    };
  }
}
