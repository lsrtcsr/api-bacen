import { IsNotEmpty, IsNumber, IsOptional, IsString, Min } from 'class-validator';
import { Column, DeepPartial, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { ExtendedEntity } from '../base';
import { User } from '../user';

export enum LimitType {
  NUM_OF_ACTIVE_CARDS = 'num_cards_active',
  NUM_OF_BOLETOS_ISSUED_PER_DAY = 'num_boletos_per_day',
  SUM_OF_BOLETOS_ISSUED_CURRENT_DAY = 'sum_boletos_current_day',
  SUM_OF_BOLETOS_ISSUED_LAST_24HOURS = 'sum_boletos_last_24hours',
  TRANSACTION_SUM_CURRENT_DAY = 'transaction_sum_current_day',
  TRANSACTION_SUM_CURRENT_MONTH = 'transaction_sum_current_month',
  TRANSACTION_SUM_CURRENT_WEEK = 'transaction_sum_current_week',
  RECEIVED_CURRENT_DAY = 'received_current_day',
  RECEIVED_CURRENT_MONTH = 'received_current_month',
  RECEIVED_CURRENT_WEEK = 'received_current_week',
  PER_TRANSACTION = 'per_transaction',
  MOBILE_CREDIT_SUM_CURRENT_DAY = 'mobile_credit_sum_current_day',
}

export interface UserLimitSchema {
  type: LimitType;
  user: User;
  asset: string;
  value: number;
  createdBy?: User;
}

@Entity(UserLimit.tableName)
export class UserLimit extends ExtendedEntity implements UserLimitSchema {
  private static readonly tableName = 'user_limits';

  @IsNotEmpty()
  @Column({ type: 'enum', enum: LimitType, nullable: false })
  type: LimitType;

  @ManyToOne((type) => User, (user) => user.limits, {
    cascade: ['insert', 'update'],
    nullable: false,
  })
  user: User;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Column({ nullable: false })
  value: number;

  @IsString()
  @IsNotEmpty()
  @Column({ nullable: true })
  asset: string;

  @ManyToOne((type) => User, (user) => user.liability, { cascade: ['insert', 'update'], nullable: true })
  @JoinColumn({ name: 'created_by' })
  createdBy?: User;

  @IsOptional()
  @Column('timestamp with time zone', { name: 'valid_until', nullable: true })
  validUntil?: Date;

  @Column('boolean', { default: true, nullable: false })
  current = true;

  constructor(data: Partial<UserLimit> = {}) {
    super(data);

    Object.assign(this, data);
  }

  public toSimpleJSON(): DeepPartial<UserLimit> {
    return {
      type: this.type,
      value: this.value,
      asset: this.asset,
    };
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<UserLimit> {
    return {
      user: this.user && this.user.toJSON ? this.user.toJSON() : this.user,
      createdBy: this.createdBy && this.createdBy.toJSON ? this.createdBy.toJSON() : this.createdBy,

      id: this.id,
      type: this.type,
      value: this.value,
      asset: this.asset,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
