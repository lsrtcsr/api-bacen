import { Column, DeepPartial, Entity, OneToOne, JoinColumn } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { ExtendedEntity } from '../base';
import { User } from '../user';

export enum SettingsFields {
  INIT = 'initialized',
  ROOT_DOMAIN = 'root_domain',
  DEFAULT_DOMAIN = 'default_domain',
  MAINTENANCE = 'maintenance',
  // For unit testing
  TEST_HASH = 'test_hash',
}

@Entity(Settings.tableName)
export class Settings extends ExtendedEntity {
  private static readonly tableName = 'settings';

  @IsNotEmpty()
  @Column({ nullable: false, unique: true, type: 'enum', enum: SettingsFields })
  field: SettingsFields;

  @IsNotEmpty()
  @Column({ nullable: false })
  value: string;

  @JoinColumn({ name: 'created_by' })
  @OneToOne(() => User, { nullable: true })
  createdBy?: User;

  constructor(data: Partial<Settings> = {}) {
    super(data);
    this.field = data.field;
    this.value = data.value;
    this.createdBy = data.createdBy;
  }

  public static async setField(field: SettingsFields, value: string, user?: User) {
    let settings = await Settings.findOne({ where: { field } });
    if (!settings) {
      settings = Settings.create({ value, field, createdBy: user });
    }
    settings.value = value;
    if (settings.id) {
      return Settings.updateAndFind(settings.id, settings);
    }
    return Settings.insertAndFind(settings);
  }

  public static async getField(field: SettingsFields, defaultValue?: any): Promise<any> {
    const setting = await Settings.findOne({ where: { field } });

    if (!setting) return defaultValue;

    return setting.value;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<Settings> {
    return {
      id: this.id,
      field: this.field,
      value: this.value,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      createdBy: this.createdBy,
    };
  }
}
