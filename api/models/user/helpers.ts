import * as bcrypt from 'bcrypt';
import user from '../../../config/user.config';

export function genHash(password: string, salt: string): Promise<string> {
  return bcrypt.hash(password, salt);
}

export function verifyPassword(password: string, hash: string): boolean {
  return bcrypt.compareSync(password, hash);
}

export async function genPassword(password: string): Promise<{ salt: string; hash: string }> {
  const salt = await bcrypt.genSalt(user.saltRounds);
  const hash = await genHash(password, salt);

  return { salt, hash };
}
