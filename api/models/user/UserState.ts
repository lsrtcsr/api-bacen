import { UserStateSchema, UserStatus } from '@bacen/base-sdk';
import { Column, Entity, ManyToOne } from 'typeorm';
import { ExtendedStateEntity } from '../base';
import { User } from './User';

@Entity(UserState.tableName)
export class UserState extends ExtendedStateEntity<UserStatus> implements UserStateSchema {
  private static readonly tableName = 'user_states';

  @Column('enum', { enum: UserStatus, default: UserStatus.INACTIVE, nullable: false })
  status: UserStatus;

  @ManyToOne(type => User, user => user.states, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  user: User;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<UserState> = {}) {
    super(data);
    Object.assign(this, data);
  }
}
