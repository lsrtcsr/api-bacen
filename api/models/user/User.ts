import {
  ConsumerStatus,
  DomainRole,
  UserRole,
  UserSchema,
  UserStatus,
  Pagination,
  PaginationData,
  CompanyData,
} from '@bacen/base-sdk';
import { IsNotEmpty, IsOptional, ValidateNested, IsEmail } from 'class-validator';
import {
  Column,
  DeepPartial,
  Entity,
  getManager,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  In,
  EntityManager,
  RelationId,
} from 'typeorm';
import { mergeDeep } from '@bacen/shared-sdk';
import { BaseError } from 'nano-errors';
import Config from '../../../config';
import { ExtendedStatefulEntity, ExtendedEntity } from '../base';
import { Consumer, LegalTerm, LegalTermAcceptance } from '../consumer';
import { Domain } from '../domain';
import { Invoice, Contract } from '../billing';
import { Alert } from '../issue';
import { OAuthAccessToken, OAuthClient, OAuthOTPToken, OAuthSecretToken } from '../oauth2';
import { UserLimit } from '../settings';
import { Wallet } from '../wallet/index';
import { genPassword, verifyPassword } from './helpers';
import { UserState } from './UserState';
import { CacheService } from '../../services';
import { ConsumerStateMachine, UserStateMachine } from '../../fsm';

@Entity(User.tableName)
export class User extends ExtendedStatefulEntity<UserStatus, UserState> implements UserSchema {
  private static readonly tableName = 'users';

  // True if the user is an API
  // Should not be saved to database, as APIs aren't real users
  virtual: boolean;

  client?: OAuthClient;

  @IsNotEmpty({ always: true })
  @Column({ nullable: false })
  firstName: string;

  @IsNotEmpty({ always: true })
  @Column({ nullable: false })
  lastName: string;

  @IsEmail()
  @IsNotEmpty({ always: true })
  @Column({ nullable: false })
  email: string;

  @OneToMany((type) => UserState, (userState) => userState.user, {
    cascade: ['insert', 'update'],
  })
  states?: UserState[];

  @IsNotEmpty({ always: true })
  @Column({ nullable: false, type: 'enum', enum: UserRole })
  @Index('user_role_admin_unique', { unique: true, where: `role = '${UserRole.ADMIN}'` })
  role: UserRole = undefined;

  @IsOptional({ always: true })
  @Column({ nullable: true })
  passwordSalt: string;

  @IsOptional({ always: true })
  @Column({ nullable: true })
  passwordHash: string;

  @OneToMany((type) => OAuthAccessToken, (token) => token.user)
  accessTokens: OAuthAccessToken;

  @OneToMany((type) => OAuthSecretToken, (token) => token.user)
  secretTokens: OAuthSecretToken;

  @OneToOne((type) => OAuthOTPToken, (token) => token.user, { nullable: true, onDelete: 'SET NULL' })
  twoFactorSecret: OAuthOTPToken;

  @IsOptional({ always: true })
  @Column({ name: 'two_factor_required', nullable: false, default: false })
  twoFactorRequired: boolean;

  @ManyToOne((type) => Domain, (domain) => domain.users, {
    nullable: true,
    onDelete: 'SET NULL',
    cascade: ['insert', 'update'],
  })
  @JoinColumn({ name: 'domain_user' })
  @Index('user_mediator_domain_unique', { unique: true, where: `role = '${UserRole.MEDIATOR}'` })
  domain?: Domain;

  @RelationId((user: User) => user.domain)
  domainId: string;

  @OneToMany((type) => Contract, (contract) => contract.contractor)
  contracts: Contract[];

  @OneToMany((type) => Invoice, (invoice) => invoice.contractor)
  invoices: Invoice[];

  @ValidateNested({ groups: [UserRole.MEDIATOR, UserRole.CONSUMER] })
  @OneToOne((type) => Consumer, (consumer) => consumer.user, {
    cascade: ['insert', 'update'],
    onDelete: 'SET NULL',
  })
  consumer?: Consumer;

  @OneToMany((type) => Wallet, (wallet) => wallet.user, { cascade: ['insert', 'update'] })
  wallets?: Wallet[];

  @OneToMany((type) => UserLimit, (limit) => limit.user, { cascade: ['insert', 'update'] })
  limits: UserLimit[];

  @OneToMany((type) => UserLimit, (limit) => limit.createdBy, { cascade: ['insert', 'update'] })
  liability: UserLimit[];

  @OneToMany((type) => Alert, (alert) => alert.createdBy, { cascade: ['insert', 'update'] })
  alerts: Alert[];

  @Index()
  @Column({ nullable: false })
  username!: string;

  constructor(data: Partial<User> = {}) {
    super(data);
    Object.assign(this, data);
    this.role = data.role || UserRole.CONSUMER;

    // Ensure password has not been set in constructor
    if ((this as any).password) {
      (this as any).password = undefined;
    }
  }

  /**
   * Gets root user, if any.
   */
  public static async getRootUser(): Promise<User | undefined> {
    return this.findOne({
      where: { role: UserRole.ADMIN },
    });
  }

  public static async findPaginatedByConsumerRelation(
    userId: string,
    relation: typeof ExtendedEntity,
    pagination: Pagination,
    stateful = false,
  ): Promise<[ExtendedEntity[], PaginationData]> {
    let baseQuery = relation
      .createQueryBuilder('relation')
      .leftJoin('relation.consumer', 'consumer')
      .leftJoin('consumer.user', 'user')
      .where('relation.deletedAt IS NULL');

    if (stateful) {
      baseQuery = baseQuery.leftJoinAndSelect('relation.states', 'states');
    }

    baseQuery = baseQuery.andWhere('user.id = :userId', { userId });
    const countQuery = baseQuery.getCount();

    let resultsQuery = baseQuery;
    if (pagination.skip) resultsQuery = resultsQuery.skip(pagination.skip);
    if (pagination.limit) resultsQuery = resultsQuery.take(pagination.limit);

    const [count, results] = await Promise.all([countQuery, resultsQuery.getMany()]);

    const paginationData: PaginationData = {
      dataLength: count,
      dataLimit: pagination.limit,
      dataSkip: pagination.skip,
    };

    return [results, paginationData];
  }

  public static async findUserAndPopulate(userId: string): Promise<User | undefined> {
    return User.createQueryBuilder('user')
      .leftJoinAndSelect('user.states', 'user.states')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('user.contracts', 'contracts')
      .leftJoinAndSelect('contracts.plan', 'plan')
      .leftJoinAndSelect('consumer.states', 'consumer.states')
      .leftJoinAndSelect('consumer.addresses', 'consumer.addresses', 'consumer.addresses.deletedAt IS NULL')
      .leftJoinAndSelect('consumer.phones', 'consumer.phones', 'consumer.phones.deletedAt IS NULL')
      .leftJoinAndSelect('user.wallets', 'wallets', 'wallets.deletedAt IS NULL')
      .leftJoinAndSelect('wallets.states', 'wallets.states')
      .where('user.id = :userId', { userId })
      .getOne();
  }

  public static async findUserByIdAndPopulate(userId: string): Promise<User | undefined> {
    const user = await User.createQueryBuilder('user')
      .leftJoinAndSelect('user.states', 'user.states')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('user.contracts', 'contracts')
      .leftJoinAndSelect('contracts.plan', 'plan')
      .leftJoinAndSelect('consumer.states', 'consumer.states')
      .leftJoinAndSelect('consumer.addresses', 'consumer.addresses', 'consumer.addresses.deletedAt IS NULL')
      .leftJoinAndSelect('consumer.phones', 'consumer.phones', 'consumer.phones.deletedAt IS NULL')
      .leftJoinAndSelect('user.wallets', 'wallets', 'wallets.deletedAt IS NULL')
      .leftJoinAndSelect('wallets.states', 'wallets.states')
      .leftJoinAndSelect('wallets.assetRegistrations', 'assetRegistrations')
      .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
      .leftJoinAndSelect('assetRegistrations.asset', 'asset')
      .where('user.id = :userId', { userId })
      .getOne();

    if (user.consumer) {
      user.consumer.user = user;
    }

    return user;
  }

  public static async findConsumerByIdAndPopulate(userId: string): Promise<User | undefined> {
    const user = await User.createQueryBuilder('user')
      .innerJoinAndSelect('user.states', 'user.states')
      .innerJoinAndSelect('user.domain', 'domain')
      .innerJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('user.contracts', 'contracts')
      .leftJoinAndSelect('contracts.plan', 'plan')
      .leftJoinAndSelect('consumer.states', 'consumer.states')
      .leftJoinAndSelect('consumer.addresses', 'consumer.addresses', 'consumer.addresses.deletedAt IS NULL')
      .leftJoinAndSelect('consumer.phones', 'consumer.phones', 'consumer.phones.deletedAt IS NULL')
      .leftJoinAndSelect('user.wallets', 'wallets', 'wallets.deletedAt IS NULL')
      .innerJoinAndSelect('wallets.states', 'wallets.states')
      .leftJoinAndSelect('wallets.assetRegistrations', 'assetRegistrations')
      .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
      .leftJoinAndSelect('assetRegistrations.asset', 'asset')
      .where('user.id = :userId', { userId })
      .getOne();

    if (user.consumer) {
      user.consumer.user = user;
    }

    return user;
  }

  /**
   * Finds an active user based on his email address.
   *
   * @param secret The user email address
   */
  public static async findByEmail(email: string): Promise<User | undefined> {
    return this.createQueryBuilder().select().where({ email, status: UserStatus.ACTIVE }).getOne();
  }

  public get name(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  /**
   * Returns the user's current contract
   */
  public get contract(): Contract | undefined {
    if (this.contracts && this.contracts.length > 0) return this.contracts.find((c) => c.current);
    return undefined;
  }

  /**
   * Validates if supplied password matches the currently saved one.
   *
   * @param password The password candidate that will be matched
   */
  public validatePassword(password: string): boolean {
    if (!password || !this.passwordHash || !this.passwordSalt) {
      return false;
    }

    return verifyPassword(password, this.passwordHash);
  }

  /**
   * Hashes and sets the user password.
   *
   * @param password The new password
   */
  public async setPassword(password: string) {
    const { salt, hash } = await genPassword(password);
    this.passwordSalt = salt;
    this.passwordHash = hash;
  }

  public async isRootMediator(): Promise<boolean> {
    if (this.role !== UserRole.MEDIATOR) return false;

    if (this.domain) {
      return this.domain.role === DomainRole.ROOT;
    }

    // Check if the domain of this user is root
    const count = await User.createQueryBuilder('user')
      .leftJoin('user.domain', 'domain')
      .where('domain.role = :role', { role: DomainRole.ROOT })
      .andWhere('user.id = :userId', { userId: this.id })
      .getCount();

    return !!count;
  }

  public static loadRequestUser(id: string): Promise<User> {
    return User.createQueryBuilder('user')
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.states', 'states')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumerStates')
      .leftJoinAndSelect('user.twoFactorSecret', 'twoFactorSecret')
      .where('user.id = :id', { id })
      .andWhere('user.deletedAt IS NULL')
      .cache(id, Config.database.cacheTimeout)
      .getOne();
  }

  /**
   * Gets the first wallet of the consumer's user
   */
  public async getPrimaryWallet({
    relations,
    manager = getManager(),
  }: { relations?: string[]; manager?: EntityManager } = {}): Promise<Wallet> {
    return manager.findOne(Wallet, {
      where: { user: this },
      order: {
        createdAt: 'ASC',
      },
      relations,
    });
  }

  /**
   * Returns if a given consumer has accepted all applicable terms-of-use
   *
   * WARNING: does not yet support consumers with multiple wallets, only checks provider terms for the primary wallet.
   */
  public async hasAcceptedAllLegalTerms(): Promise<boolean> {
    const consumer = this.consumer || Consumer.findOne({ where: { user: this } });

    const [activeTerms, primaryWallet] = await Promise.all([
      LegalTerm.getActiveTerms(),
      this.getPrimaryWallet({ relations: ['assetRegistrations'] }),
    ]);

    const [count, providerPendingTerms] = await Promise.all([
      LegalTermAcceptance.count({
        where: {
          consumer,
          legalTerm: In(activeTerms.map((term) => term.id)),
        },
      }),
      primaryWallet.getPendingProviderTerms(),
    ]);

    return count === activeTerms.length && providerPendingTerms.length === 0;
  }

  /**
   * Returns if the user instance is active
   */
  public async isActive(): Promise<boolean> {
    const cacheService = CacheService.getInstance();
    const key = `user_active_${this.id}`;

    const cachedResult = await cacheService.get(key);
    if (cachedResult) return cachedResult as boolean;

    let result: boolean;
    if (this.status) {
      result = this.status === UserStatus.ACTIVE;
    } else {
      const lastState = await UserState.createQueryBuilder('states')
        .where('"userId" = :userId', { userId: this.id })
        .orderBy('created_at', 'DESC')
        .getOne();

      result = lastState.status === UserStatus.ACTIVE;
    }

    await cacheService.set(key, JSON.stringify(result), -1);
    return result;
  }

  public async cacheUserActiveStatus(active: boolean) {
    const cacheService = CacheService.getInstance();
    const key = `user_active_${this.id}`;

    await cacheService.set(key, JSON.stringify(active), -1);
  }

  public async invalidateUserActiveCache() {
    const cacheService = CacheService.getInstance();
    const key = `user_active_${this.id}`;

    await cacheService.del(key);
  }

  public async cacheConsumerActiveStatus(active: boolean) {
    const cacheService = CacheService.getInstance();
    const key = `consumer_active_${this.consumer.id}`;

    await cacheService.set(key, JSON.stringify(active), -1);
  }

  public async invalidateConsumerActiveCache() {
    const cacheService = CacheService.getInstance();
    const key = `consumer_active_${this.consumer.id}`;

    await cacheService.del(key);
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(deep = true): DeepPartial<User> {
    const states = this.getStates();

    return {
      // Relations
      domain: this.domain && this.domain.toSimpleJSON(),
      consumer: this.consumer && this.consumer.toJSON(),
      wallets: this.wallets && deep ? this.wallets.map((w) => (w.toJSON ? w.toJSON(false, true) : w)) : undefined,
      states: states && deep ? states.map((s) => (s.toJSON ? s.toJSON() : s)) : undefined,
      contract: this.contract ? this.contract.toJSON() : undefined,

      // Base entity fields
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,

      // Basic user info
      twoFactorRequired: this.twoFactorRequired,
      role: this.role,
      email: this.email,
      username: this.username,
      lastName: this.lastName,
      firstName: this.firstName,
      name: this.name,
      status: this.status,
      id: this.id,
    };
  }

  public toSimpleJSON(): DeepPartial<User> {
    return {
      domain: this.domain && this.domain.toSimpleJSON(),

      // Basic user info
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      role: this.role,
      lastName: this.lastName,
      firstName: this.firstName,
      email: this.email,
      name: this.name,
      status: this.status,
      id: this.id,
    };
  }

  public static async findByLatestStatusAndDomain(status: ConsumerStatus, domainId?: string): Promise<User[]> {
    const qb = User.createQueryBuilder('u')
      .innerJoinAndSelect('u.consumer', 'c')
      .leftJoinAndSelect('u.domain', 'domain')
      .innerJoin(
        (innerQb) =>
          innerQb
            .subQuery()
            .select(`DISTINCT ON("consumerId") "consumerId", "created_at", "status"`)
            .from('consumer_states', 'cs1')
            .orderBy(`"consumerId", "created_at"`, 'DESC'),
        'consumer_states" ON "consumer_states"."consumerId" = "c"."id',
      )
      .where('consumer_states.status = :status', { status })
      .andWhere('u.deleted_at IS NULL');

    if (domainId && domainId.length) {
      qb.andWhere('domain.id = :domainId', { domainId });
    }

    return qb.orderBy('u.created_at', 'ASC').getMany();
  }

  /**
   * Gets the domain's mediator
   */
  public static async getMediator(domain: string | Domain): Promise<User> {
    const domainId = typeof domain === 'string' ? domain : domain.id;
    return this.findOne({
      where: { role: UserRole.MEDIATOR, domain: { id: domainId } },
    });
  }

  /**
   * Returns the root mediator user if it exists, returns undefined otherwise.
   * @param manager The ORM manager.
   */
  public static async getRootMediator(manager = getManager()): Promise<User | undefined> {
    return manager
      .createQueryBuilder(User, 'user')
      .leftJoin('user.domain', 'domain')
      .where('user.role = :userRole', { userRole: UserRole.MEDIATOR })
      .andWhere('domain.role = :domainRole', { domainRole: DomainRole.ROOT })
      .getOne();
  }

  // Yeah, it's ugly
  // But TypeORM's repository by design does not update relations
  public static async updateWithRelation(userId: string, payload: DeepPartial<User>): Promise<User> {
    const current = await User.safeFindOne({
      where: { id: userId },
      relations: [
        'domain',
        'states',
        'consumer',
        'consumer.addresses',
        'consumer.documents',
        'consumer.phones',
        'consumer.states',
        'wallets',
        'wallets.assetRegistrations',
        'wallets.assetRegistrations.states',
        'wallets.assetRegistrations.asset',
      ],
    });

    if (!current) {
      throw new BaseError('User not found', { id: userId });
    }

    // Prevents user role update from the API
    const { consumer: consumerData, role, ...restUserData } = payload;
    const user = await User.updateAndFind(userId, { ...restUserData, role: current.role });
    Object.assign(current, user);

    if (consumerData) {
      const { companyData, ...restConsumerData } = consumerData;

      if (companyData) {
        // TODO remove fields that not allowed to be updated
        const currentCompanyData = current?.consumer?.companyData ?? {};
        (restConsumerData as any).companyData = Object.keys(currentCompanyData).length
          ? (mergeDeep(currentCompanyData, companyData) as CompanyData)
          : { ...companyData };
      }

      const consumer = await Consumer.updateAndFind(current.consumer.id, {
        ...restConsumerData,
        type: current.consumer.type,
      });
      Object.assign(current.consumer, consumer);
    }

    await current.reload();
    return current;
  }

  public async removeFailedState(): Promise<void> {
    const consumerFailureStatus = [ConsumerStatus.PROVIDER_FAILED, ConsumerStatus.PROVIDER_REJECTED];

    if (this?.consumer?.states?.length > 0 && consumerFailureStatus.includes(this.consumer.status)) {
      // Sometimes failure states can be duplicated
      const lastStateBeforeFail = this.consumer
        .getStates()
        .find((state) => !consumerFailureStatus.includes(state.status));

      const consumerFSM = new ConsumerStateMachine(this);
      await consumerFSM.goTo(lastStateBeforeFail.status);
    }

    if (this?.states?.length > 0 && this?.status === UserStatus.FAILED) {
      const lastStateBeforeFail = this.getStates().find((state) => state.status !== UserStatus.FAILED);

      const userFSM = new UserStateMachine(this);
      await userFSM.goTo(lastStateBeforeFail.status);
    }
  }

  public static deleteAccount(userId: string, hard = false): Promise<boolean> {
    if (hard) {
      // Deletes the row from the table
      // May cascade and break things
      return User.delete(userId).then((_) => true);
    }
    return User.softDelete(userId).then((_) => true);
  }
}
