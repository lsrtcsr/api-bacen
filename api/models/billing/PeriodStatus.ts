export enum PeriodStatus {
  ACTIVE = 'active',
  CLOSED = 'closed',
  CANCELED = 'canceled',
}
