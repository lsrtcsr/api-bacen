export enum ServiceCategory {
  EXTERNAL_PROVIDER_SERVICE = 'external_provider_service',
  PLATFORM_SERVICE = 'platform_service',
  INFRA_SERVICE = 'infra_service',
}
