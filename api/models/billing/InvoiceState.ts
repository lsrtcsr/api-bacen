import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { ExtendedEntity } from '../base';
import { Invoice } from './Invoice';
import { InvoiceStatus } from './InvoiceStatus';

@Entity(InvoiceState.tableName)
export class InvoiceState extends ExtendedEntity {
  private static readonly tableName = 'invoice_states';

  @Column('enum', { nullable: false, enum: InvoiceStatus })
  status: InvoiceStatus;

  @ManyToOne(
    type => Invoice,
    invoice => invoice.states,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  invoice: Invoice;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<InvoiceState> = {}) {
    super(data);
    this.status = data.status;
    this.additionalData = data.additionalData;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<InvoiceState> {
    return {
      id: this.id,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
