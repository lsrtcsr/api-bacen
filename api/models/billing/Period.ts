import { Column, Entity, ManyToOne, DeepPartial, OneToMany } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ServiceType } from '@bacen/base-sdk';
import * as moment from 'moment';
import * as currency from 'currency.js';
import { ExtendedEntity } from '../base';
import { PeriodState } from './PeriodState';
import { Invoice } from './Invoice';
import { Entry, OperationType, EntryStatus } from './Entry';
import { EntryType } from './EntryType';
import { MomentValueTransformer } from '../base/MomentValueTransformer';

@Entity(Period.TABLE_NAME)
export class Period extends ExtendedEntity {
  private static readonly TABLE_NAME = 'periods';

  @OneToMany(
    type => Entry,
    entry => entry.period,
    { cascade: ['insert', 'update'] },
  )
  entries: Entry[];

  @IsNotEmpty()
  @ManyToOne(
    type => Invoice,
    invoice => invoice.periods,
    {
      cascade: ['insert', 'update'],
      onDelete: 'CASCADE',
      nullable: false,
    },
  )
  invoice: Invoice;

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'start_at',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  startedAt: moment.Moment;

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'closure_scheduled_for',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  closureScheduledFor: moment.Moment;

  @IsOptional()
  @Column('timestamp with time zone', {
    name: 'closed_at',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  closedAt?: moment.Moment;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  current: boolean = true;

  @OneToMany(
    type => PeriodState,
    periodState => periodState.period,
    {
      eager: true,
      cascade: ['insert', 'update'],
    },
  )
  states?: PeriodState[];

  get status() {
    const states = this.getStates();
    if (states) {
      return states[0].status;
    }
  }

  public async balance(entryStatus?: EntryStatus): Promise<string> {
    return currency(await this.totalCredits(entryStatus))
      .subtract(await this.totalDebits(entryStatus))
      .format(false);
  }

  public async totalDebits(entryStatus?: EntryStatus): Promise<string> {
    const entries = this.entries || (await Entry.safeFind({ where: { period: { id: this.id } } }));

    const requiredStatus = entryStatus ? [entryStatus] : [EntryStatus.PENDING, EntryStatus.SETTLED];

    return entries
      .filter(entry => entry.operationType === OperationType.DEBIT && requiredStatus.includes(entry.status))
      .map(entry => entry.amount)
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public totalDebitsSync(entryStatus?: EntryStatus): string {
    if (!this.entries) return undefined;

    const requiredStatus = entryStatus ? [entryStatus] : [EntryStatus.PENDING, EntryStatus.SETTLED];

    return this.entries
      .filter(entry => entry.operationType === OperationType.DEBIT && requiredStatus.includes(entry.status))
      .map(entry => entry.amount)
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public async totalCredits(entryStatus?: EntryStatus): Promise<string> {
    const entries = this.entries || (await Entry.safeFind({ where: { period: { id: this.id } } }));

    const requiredStatus = entryStatus ? [entryStatus] : [EntryStatus.PENDING, EntryStatus.SETTLED];

    return entries
      .filter(entry => entry.operationType === OperationType.CREDIT && requiredStatus.includes(entry.status))
      .map(entry => entry.amount)
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public totalCreditsSync(entryStatus?: EntryStatus): string {
    if (!this.entries) return undefined;

    const requiredStatus = entryStatus ? [entryStatus] : [EntryStatus.PENDING, EntryStatus.SETTLED];

    return this.entries
      .filter(entry => entry.operationType === OperationType.CREDIT && requiredStatus.includes(entry.status))
      .map(entry => entry.amount)
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public async findEntryByService(
    serviceType: ServiceType,
    free: boolean = false,
    from?: moment.Moment,
  ): Promise<Entry[]> {
    const entries = await Entry.find({
      where: { period: { id: this.id } },
      relations: ['period', 'type', 'type.service'],
    });

    return entries.filter(
      entry =>
        entry.type.service.type === serviceType &&
        entry.status !== EntryStatus.CANCELED &&
        entry.additionalData.chargeback === free &&
        (!from || from.isSameOrAfter(moment(entry.createdAt))),
    );
  }

  public async getServiceConsumption(
    serviceType: ServiceType,
    free: boolean = false,
    from?: moment.Moment,
  ): Promise<number> {
    const entries = await this.findEntryByService(serviceType, free, from);

    return entries.length;
  }

  public async findEntryByType(type: EntryType, free: boolean = false): Promise<Entry[]> {
    const entries = this.entries
      ? this.entries
      : await Entry.find({
          where: { period: { id: this.id } },
          relations: ['period', 'type'],
        });

    return entries.filter(
      entry =>
        entry.type.id === type.id && entry.status !== EntryStatus.CANCELED && entry.additionalData.chargeback === free,
    );
  }

  constructor(data: Partial<Period> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public getStates(): PeriodState[] | undefined {
    if (this.states && this.states.length) {
      return this.states.sort((a, b) => {
        if (b.createdAt && a.createdAt) {
          return b.createdAt.getTime() - a.createdAt.getTime();
        }
        return 0;
      });
    }
  }

  public toJSON(): DeepPartial<Period> & any {
    return {
      entries:
        this.entries &&
        this.entries
          .sort((a, b) => {
            if (b.createdAt && a.createdAt) {
              return b.createdAt.getTime() - a.createdAt.getTime();
            }
            return 0;
          })
          .map(entry => entry.toSimpleJSON()),

      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      startedAt: this.startedAt ? this.startedAt.toDate() : this.startedAt,
      closureScheduledFor: this.closureScheduledFor ? this.closureScheduledFor.toDate() : this.closureScheduledFor,
      closedAt: this.closedAt ? this.closedAt.toDate() : this.closedAt,
      status: this.status,
      current: this.current,
      totalPendingCredits: this.totalCreditsSync(EntryStatus.PENDING),
      totalSettledCredits: this.totalCreditsSync(EntryStatus.SETTLED),
      totalPendingDebits: this.totalDebitsSync(EntryStatus.PENDING),
      totalSettledDebits: this.totalDebitsSync(EntryStatus.SETTLED),
      pendingEntriesBalance: currency(this.totalCreditsSync(EntryStatus.PENDING), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.PENDING))
        .format(false),
      settledEntriesBalance: currency(this.totalCreditsSync(EntryStatus.SETTLED), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.SETTLED))
        .format(false),
    };
  }
}
