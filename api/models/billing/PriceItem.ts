import { Column, Entity, ManyToOne, DeepPartial, OneToMany } from 'typeorm';
import * as moment from 'moment';
import { IsNotEmpty } from 'class-validator';
import { ExtendedEntity } from '../base';
import { Service } from './Service';
import { Entry } from './Entry';
import { Plan } from './Plan';
import { MomentValueTransformer } from '../base/MomentValueTransformer';

@Entity(PriceItem.TABLE_NAME)
export class PriceItem extends ExtendedEntity {
  private static readonly TABLE_NAME = 'price_items';

  @IsNotEmpty()
  @ManyToOne(
    type => Service,
    service => service.priceItems,
    {
      cascade: ['insert', 'update'],
      onDelete: 'RESTRICT',
      eager: true,
      nullable: false,
    },
  )
  service: Service;

  @IsNotEmpty()
  @ManyToOne(
    type => Plan,
    plan => plan.priceList,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  plan: Plan;

  @OneToMany(
    type => Entry,
    entry => entry.priceItem,
    {
      cascade: ['insert', 'update'],
      onDelete: 'RESTRICT',
    },
  )
  entries: Entry[];

  @IsNotEmpty()
  @Column({ nullable: false })
  amount: string;

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'valid_from',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  validFrom: moment.Moment;

  @Column('timestamp with time zone', {
    name: 'valid_until',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  validUntil?: moment.Moment;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  current: boolean = true;

  constructor(data: Partial<PriceItem> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public toJSON(): DeepPartial<PriceItem> {
    return {
      service: this.service && this.service.toJSON ? this.service.toJSON() : this.service,
      current: this.current,
      validFrom: this.validFrom ? this.validFrom.toDate() : this.validFrom,
      validUntil: this.validUntil ? this.validUntil.toDate() : this.validUntil,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      amount: this.amount,
      id: this.id,
    };
  }
}
