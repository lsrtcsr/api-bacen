import { Column, Entity, ManyToOne, DeepPartial, Not, Equal, MoreThanOrEqual } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import * as moment from 'moment';
import { ExtendedEntity } from '../base';
import { Period } from './Period';
import { PriceItem } from './PriceItem';
import { EntryType, EntryClazz } from './EntryType';
import { Payment } from '../payment';
import { Event } from '.';

export enum EntryStatus {
  PENDING = 'pending',
  SETTLED = 'settled',
  CANCELED = 'canceled',
}

export enum OperationType {
  CREDIT = 'credit',
  DEBIT = 'debit',
}

export interface EntryData {
  sourceId: string;
  sourceTransaction: any;
  paymentProof?: Payment;
  chargeback: boolean;
}

@Entity(Entry.TABLE_NAME)
export class Entry extends ExtendedEntity {
  private static readonly TABLE_NAME = 'entries';

  @IsNotEmpty()
  @ManyToOne(
    type => EntryType,
    type => type.entries,
    {
      onDelete: 'RESTRICT',
      nullable: false,
    },
  )
  type: EntryType;

  @IsOptional()
  @ManyToOne(
    type => Event,
    event => event.entries,
    { nullable: true, onDelete: 'CASCADE' },
  )
  event: Event;

  @IsOptional()
  @Column('jsonb', { name: 'additional_data', nullable: true })
  additionalData?: EntryData;

  @ManyToOne(
    type => Period,
    period => period.entries,
    { onDelete: 'CASCADE' },
  )
  period: Period;

  @IsNotEmpty()
  @ManyToOne(
    type => PriceItem,
    priceItem => priceItem.entries,
    { onDelete: 'SET NULL' },
  )
  priceItem: PriceItem;

  @IsNotEmpty()
  @Column({ nullable: false })
  amount: string;

  @IsNotEmpty()
  @Column({ type: 'enum', enum: EntryStatus, default: EntryStatus.PENDING, nullable: false })
  status: EntryStatus = EntryStatus.PENDING;

  @IsNotEmpty()
  @Column({ type: 'enum', enum: OperationType, default: OperationType.DEBIT, nullable: false })
  operationType: OperationType = OperationType.DEBIT;

  constructor(data: Partial<Entry> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public static async findWithFilters(
    userId: string,
    type: EntryType,
    clazz: EntryClazz,
    from: moment.Moment = moment(),
  ): Promise<Entry[]> {
    return await Entry.safeFind({
      where: {
        period: {
          invoice: { contractor: { id: userId } },
        },
        type: {
          plan: { id: type.plan.id },
          settings: { clazz },
        },
        status: Not(Equal(EntryStatus.CANCELED)),
        createdAt: MoreThanOrEqual(from.toDate()),
      },
      relations: ['type', 'type.plan', 'period', 'period.invoice', 'period.invoice.contractor'],
      order: { createdAt: 'DESC' },
    });
  }

  public static async getEntriesByTypeAndContractor(type: EntryType, contractorId: string): Promise<Entry[]> {
    return await Entry.safeFind({
      where: {
        type,
        period: {
          invoice: { contractor: { id: contractorId } },
        },
        status: Not(Equal(EntryStatus.CANCELED)),
      },
      relations: ['type', 'period', 'period.invoice', 'period.invoice.contractor'],
      order: { createdAt: 'DESC' },
    });
  }

  public toSimpleJSON(): DeepPartial<Entry> {
    return {
      type: this.type && this.type.toSimpleJSON ? this.type.toSimpleJSON() : this.type,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      operationType: this.operationType,
      amount: this.amount,
      additionalData: this.additionalData,
      status: this.status,
      id: this.id,
    };
  }

  public toJSON(): DeepPartial<Entry> {
    return {
      type: this.type && this.type.toJSON ? this.type.toJSON() : this.type,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      operationType: this.operationType,
      amount: this.amount,
      period: this.period && this.period.toJSON ? this.period.toJSON() : this.period,
      additionalData: this.additionalData,
      status: this.status,
      id: this.id,
    };
  }
}
