import { Column, Entity, DeepPartial, OneToMany, ManyToOne, OneToOne, JoinColumn, FindConditions } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ServiceType, Pagination, PaginationData } from '@bacen/base-sdk';
import * as currency from 'currency.js';
import * as moment from 'moment';
import { ExtendedEntity } from '../base';
import { Period } from './Period';
import { InvoiceState } from './InvoiceState';
import { EntryStatus, Entry } from './Entry';
import { User } from '../user';
import { EntryType } from './EntryType';
import { MomentValueTransformer } from '../base/MomentValueTransformer';
import { PeriodStatus } from './PeriodStatus';
import { Transaction } from '../transaction';
import { isUUID } from '../../utils';

@Entity(Invoice.TABLE_NAME)
export class Invoice extends ExtendedEntity {
  private static readonly TABLE_NAME = 'invoices';

  @IsNotEmpty()
  @ManyToOne(
    type => User,
    user => user.invoices,
    {
      nullable: false,
      onDelete: 'RESTRICT',
    },
  )
  contractor: User;

  @IsNotEmpty()
  @OneToMany(
    type => Period,
    period => period.invoice,
    {
      cascade: ['insert', 'update'],
      nullable: false,
      onDelete: 'RESTRICT',
    },
  )
  periods: Period[];

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'start_at',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  startedAt: moment.Moment;

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'closure_scheduled_for',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  closureScheduledFor: moment.Moment;

  @IsOptional()
  @Column('timestamp with time zone', {
    name: 'closed_at',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  closedAt?: moment.Moment;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  current: boolean = true;

  @IsOptional()
  @JoinColumn({ name: 'payment_proof' })
  @OneToOne(type => Transaction, { nullable: true })
  paymentProof: Transaction;

  @OneToMany(
    type => InvoiceState,
    invoiceState => invoiceState.invoice,
    {
      eager: true,
      cascade: ['insert', 'update'],
    },
  )
  states?: InvoiceState[];

  get status() {
    const states = this.getStates();
    if (states) {
      return states[0].status;
    }
  }

  constructor(data: Partial<Invoice> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public getStates(): InvoiceState[] | undefined {
    if (this.states && this.states.length) {
      return this.states.sort((a, b) => {
        if (b.createdAt && a.createdAt) {
          return b.createdAt.getTime() - a.createdAt.getTime();
        }
        return 0;
      });
    }
  }

  public static async getCurrent(contractor: User): Promise<Invoice> {
    return Invoice.safeFindOne({
      where: { contractor: { id: contractor.id }, current: true },
      relations: ['contractor', 'periods'],
    });
  }

  public async getServiceConsumption(
    service: ServiceType,
    free: boolean = false,
    from?: moment.Moment,
  ): Promise<number> {
    const promises = this.periods.map(period => period.getServiceConsumption(service, free, from));

    const consumptionByPeriod = await Promise.all(promises);
    return consumptionByPeriod.reduce((sum, current) => sum + current, 0);
  }

  public async findEntryByService(service: ServiceType, free: boolean = false, from?: moment.Moment): Promise<Entry[]> {
    const entriesPromises = this.periods.map(period => period.findEntryByService(service, free, from));
    return [].concat.apply([], await Promise.all(entriesPromises));
  }

  public async findEntryByType(type: EntryType, free: boolean = false): Promise<Entry[]> {
    const entriesPromises = this.periods.map(period => period.findEntryByType(type, free));
    return [].concat.apply([], await Promise.all(entriesPromises));
  }

  public async getCurrentPeriod(): Promise<Period> {
    const currentPeriod = await Period.safeFindOne({
      where: {
        invoice: { id: this.id },
        current: true,
      },
      relations: ['invoice', 'states'],
    });

    if (!currentPeriod || currentPeriod.status !== PeriodStatus.ACTIVE) return undefined;

    return currentPeriod;
  }

  public async balance(entryStatus?: EntryStatus): Promise<string> {
    return currency(await this.totalCredits(entryStatus), { precision: 7 })
      .subtract(await this.totalDebits(entryStatus))
      .format(false);
  }

  public async totalDebits(entryStatus?: EntryStatus): Promise<string> {
    const periods = this.periods || (await Period.safeFind({ where: { invoice: { id: this.id } } }));

    const totalDebitsPerPeriod = await Promise.all(periods.map(async period => await period.totalDebits(entryStatus)));
    return totalDebitsPerPeriod
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public totalDebitsSync(entryStatus?: EntryStatus): string {
    if (!this.periods) return undefined;

    const totalDebitsPerPeriod = this.periods.map(period => period.totalDebitsSync(entryStatus));
    return totalDebitsPerPeriod
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public async totalCredits(entryStatus?: EntryStatus): Promise<string> {
    const periods = this.periods || (await Period.safeFind({ where: { invoice: { id: this.id } } }));

    const totalCreditsPerPeriod = await Promise.all(
      periods.map(async period => await period.totalCredits(entryStatus)),
    );
    return totalCreditsPerPeriod
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public totalCreditsSync(entryStatus?: EntryStatus): string {
    if (!this.periods) return undefined;

    const totalCreditsPerPeriod = this.periods.map(period => period.totalCreditsSync(entryStatus));
    return totalCreditsPerPeriod
      .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }))
      .format(false);
  }

  public static async findAllByUser(userId: string, pagination: Pagination): Promise<[Invoice[], PaginationData]> {
    return Invoice.safePaginatedFind({
      pagination,
      where: {
        contractor: { id: userId },
      },
      relations: ['contractor', 'periods', 'periods.entries'],
      order: { startedAt: 'DESC' },
    });
  }

  public static async findOneByUser(userId: string, id?: string) {
    let where: FindConditions<Invoice> = id && isUUID(id) ? { id } : { current: true };
    where = { contractor: { id: userId }, ...where };

    return Invoice.safeFindOne({
      where,
      relations: [
        'contractor',
        'contractor.contracts',
        'contractor.contracts.plan',
        'periods',
        'periods.entries',
        'periods.entries.type',
        'periods.entries.type.service',
      ],
    });
  }

  public toSimpleJSON(): DeepPartial<Invoice> & any {
    return {
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      startedAt: this.startedAt ? this.startedAt.toDate() : this.startedAt,
      closureScheduledFor: this.closureScheduledFor ? this.closureScheduledFor.toDate() : this.closureScheduledFor,
      closedAt: this.closedAt ? this.closedAt.toDate() : this.closedAt,
      status: this.status,
      current: this.current,
      paymentProof: this.paymentProof,
      totalPendingCredits: this.totalCreditsSync(EntryStatus.PENDING),
      totalSettledCredits: this.totalCreditsSync(EntryStatus.SETTLED),
      totalPendingDebits: this.totalDebitsSync(EntryStatus.PENDING),
      totalSettledDebits: this.totalDebitsSync(EntryStatus.SETTLED),
      pendingEntriesBalance: currency(this.totalCreditsSync(EntryStatus.PENDING), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.PENDING))
        .format(false),
      settledEntriesBalance: currency(this.totalCreditsSync(EntryStatus.SETTLED), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.SETTLED))
        .format(false),
    };
  }

  public toJSON(): DeepPartial<Invoice> & any {
    return {
      periods:
        this.periods &&
        this.periods
          .sort((a, b) => {
            if (b.createdAt && a.createdAt) {
              return b.createdAt.getTime() - a.createdAt.getTime();
            }
            return 0;
          })
          .map(period => period.toJSON()),
      contractor: this.contractor && this.contractor.toJSON ? this.contractor.toJSON() : this.contractor,

      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      startedAt: this.startedAt ? this.startedAt.toDate() : this.startedAt,
      closureScheduledFor: this.closureScheduledFor ? this.closureScheduledFor.toDate() : this.closureScheduledFor,
      closedAt: this.closedAt ? this.closedAt.toDate() : this.closedAt,
      status: this.status,
      current: this.current,
      paymentProof: this.paymentProof,
      totalPendingCredits: this.totalCreditsSync(EntryStatus.PENDING),
      totalSettledCredits: this.totalCreditsSync(EntryStatus.SETTLED),
      totalPendingDebits: this.totalDebitsSync(EntryStatus.PENDING),
      totalSettledDebits: this.totalDebitsSync(EntryStatus.SETTLED),
      pendingEntriesBalance: currency(this.totalCreditsSync(EntryStatus.PENDING), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.PENDING))
        .format(false),
      settledEntriesBalance: currency(this.totalCreditsSync(EntryStatus.SETTLED), { precision: 7 })
        .subtract(this.totalDebitsSync(EntryStatus.SETTLED))
        .format(false),
    };
  }
}
