import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { ExtendedEntity } from '../base';
import { Contract } from './Contract';
import { ContractStatus } from './ContractStatus';

@Entity(ContractState.tableName)
export class ContractState extends ExtendedEntity {
  private static readonly tableName = 'contract_states';

  @Column('enum', { nullable: false, enum: ContractStatus })
  status: ContractStatus;

  @ManyToOne(
    type => Contract,
    contract => contract.states,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  contract: Contract;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<ContractState> = {}) {
    super(data);
    this.status = data.status;
    this.additionalData = data.additionalData;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<ContractState> {
    return {
      id: this.id,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
