import { Column, Entity, ManyToOne, DeepPartial, OneToMany } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import * as moment from 'moment';
import { ExtendedEntity } from '../base';
import { User } from '../index';
import { ContractState } from './ContractState';
import { Plan } from './Plan';
import { Domain } from '../domain';
import { MomentValueTransformer } from '../base/MomentValueTransformer';

export enum PaymentMethod {
  BOLETO = 'boleto',
  TRANSFER = 'transfer',
}

@Entity(Contract.TABLE_NAME)
export class Contract extends ExtendedEntity {
  private static readonly TABLE_NAME = 'contracts';

  @IsNotEmpty()
  @ManyToOne(
    type => Domain,
    domain => domain.contracts,
    {
      nullable: false,
      onDelete: 'RESTRICT',
    },
  )
  supplier: Domain;

  @IsNotEmpty()
  @ManyToOne(
    type => User,
    user => user.contracts,
    {
      nullable: false,
      onDelete: 'RESTRICT',
    },
  )
  contractor: User;

  @IsNotEmpty()
  @ManyToOne(
    type => Plan,
    plan => plan.contracts,
    {
      nullable: false,
      onDelete: 'SET NULL',
    },
  )
  plan: Plan;

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'valid_from',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  validFrom: moment.Moment;

  @IsOptional()
  @Column('timestamp with time zone', {
    name: 'valid_until',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  validUntil?: moment.Moment;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  current: boolean = true;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  prepaid: boolean = true;

  @IsNotEmpty()
  @Column({
    type: 'enum',
    name: 'payment_method',
    enum: PaymentMethod,
    default: PaymentMethod.TRANSFER,
    nullable: false,
  })
  paymentMethod: PaymentMethod = PaymentMethod.TRANSFER;

  @OneToMany(
    type => ContractState,
    contractState => contractState.contract,
    {
      eager: true,
      cascade: ['insert', 'update'],
    },
  )
  states?: ContractState[];

  get status() {
    const states = this.getStates();
    if (states) {
      return states[0].status;
    }
  }

  constructor(data: Partial<Contract> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public getStates(): ContractState[] | undefined {
    if (this.states && this.states.length) {
      return this.states.sort((a, b) => {
        if (b.createdAt && a.createdAt) {
          return b.createdAt.getTime() - a.createdAt.getTime();
        }
        return 0;
      });
    }
  }

  public toJSON(): DeepPartial<Contract> {
    return {
      contractor: this.contractor && this.contractor.toJSON ? this.contractor.toJSON() : this.contractor,
      supplier: this.supplier && this.supplier.toJSON ? this.supplier.toJSON() : this.supplier,
      plan: this.plan && this.plan.toJSON ? this.plan.toJSON() : this.plan,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      validFrom: this.validFrom ? this.validFrom.format() : this.validFrom,
      validUntil: this.validUntil ? this.validUntil.format() : this.validUntil,
      prepaid: this.prepaid,
      status: this.status,
      current: this.current,
      id: this.id,
    };
  }
}
