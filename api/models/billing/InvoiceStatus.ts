export enum InvoiceStatus {
  ACTIVE = 'active',
  CLOSING = 'closing',
  CLOSED = 'closed',
  PENDING_SETTLEMENT = 'pending_settlement',
  SETTLED = 'settled',
  CANCELED = 'canceled',
}
