import { Column, Entity, ManyToOne, DeepPartial, OneToMany, getManager, EntityManager } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import * as moment from 'moment';
import { ExtendedEntity } from '../base';
import { Service } from './Service';
import { Plan } from './Plan';
import { Entry } from './Entry';
import { MomentValueTransformer } from '../base/MomentValueTransformer';

export enum EventType {
  TRANSACTION = 'transaction',
  PERIOD_CLOSING = 'period_closing',
  INVOICE_CLOSING = 'invoice_closing',
}

export enum EntryClazz {
  SERVICE_CHARGE = 'service_charge', // the default one
  COMMISSION = 'commission',
  PROGRESSIVE_DISCOUNT = 'progressive_discount',
  INCENTIVE = 'incentive',
  PENALTY = 'penalty',
  TAX = 'tax',
}

export interface EntrySettings {
  clazz: EntryClazz;
  billingTrigger: EventType;
  freeUntil?: number; // applicable only to service charge clazz

  entryCalculationMode?: 'absolute' | 'price_percentage' | 'transaction_percentage';
  value: number;
  liability?: 'bacen' | 'provider';
  reverseOnFail?: boolean;

  // used for incentive and penalties
  rule?: {
    threshold: number;
    thresholdCheckCalculationMode: 'transaction_count' | 'transaction_amount';
    excludeServiceFee?: boolean; // service charge should be excluded from the entry calculation basis?
    maxNumberOfEntries?: number; // per user
  };
}

@Entity(EntryType.TABLE_NAME)
export class EntryType extends ExtendedEntity {
  private static readonly TABLE_NAME = 'entry_types';

  @IsNotEmpty()
  @Column({ nullable: false })
  name: string;

  @IsNotEmpty()
  @Column('jsonb', { default: {}, nullable: false })
  settings: EntrySettings;

  @IsNotEmpty()
  @ManyToOne(
    type => Service,
    service => service.entryTypes,
    { eager: true, nullable: false },
  )
  service: Service;

  @IsNotEmpty()
  @ManyToOne(
    type => Plan,
    plan => plan.items,
    { nullable: false },
  )
  plan: Plan;

  @OneToMany(
    type => Entry,
    entry => entry.type,
  )
  entries: Entry[];

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'valid_from',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  validFrom: moment.Moment;

  @Column('timestamp with time zone', {
    name: 'valid_until',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  validUntil?: moment.Moment;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  valid: boolean = true;

  constructor(data: Partial<EntryType> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public static async invalidate(id: string, transaction: EntityManager = getManager()): Promise<any> {
    const updateResult = await transaction.update(EntryType, id, {
      validUntil: moment(),
      valid: false,
    });

    return updateResult.raw;
  }

  public async invalidate(transaction: EntityManager = getManager()): Promise<any> {
    const updateResult = await transaction.update(EntryType, this.id, {
      validUntil: moment(),
      valid: false,
    });

    return updateResult.raw;
  }

  public toSimpleJSON(): DeepPartial<EntryType> {
    return {
      name: this.name,
      service: this.service && this.service.toSimpleJSON ? this.service.toSimpleJSON() : this.service,
      id: this.id,
    };
  }

  public toJSON(): DeepPartial<EntryType> {
    return {
      name: this.name,
      service: this.service && this.service.toJSON ? this.service.toJSON() : this.service,
      settings: this.settings,
      valid: this.valid,
      validFrom: this.validFrom ? this.validFrom.toDate() : this.validFrom,
      validUntil: this.validUntil ? this.validUntil.toDate() : this.validUntil,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      id: this.id,
    };
  }
}
