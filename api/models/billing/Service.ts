import { Column, Entity, DeepPartial, OneToMany } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ServiceType } from '@bacen/base-sdk';
import { ExtendedEntity } from '../base';
import { EntryType } from './EntryType';
import { PriceItem } from './PriceItem';

@Entity(Service.TABLE_NAME)
export class Service extends ExtendedEntity {
  private static readonly TABLE_NAME = 'services';

  @IsNotEmpty()
  @Column({ nullable: false })
  name: string;

  @IsNotEmpty()
  @Column({ nullable: false })
  description: string;

  @IsNotEmpty()
  @Column({ type: 'enum', enum: ServiceType, nullable: false })
  type: ServiceType;

  @IsOptional()
  @Column({ nullable: true })
  provider?: string;

  @OneToMany(
    type => EntryType,
    entryType => entryType.service,
    { onDelete: 'SET NULL' },
  )
  entryTypes: EntryType[];

  @OneToMany(
    type => PriceItem,
    priceItem => priceItem.service,
    { onDelete: 'SET NULL' },
  )
  priceItems: PriceItem[];

  constructor(data: Partial<Service> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public toSimpleJSON(): DeepPartial<Service> {
    return {
      type: this.type,
      provider: this.provider,
      name: this.name,
      description: this.description,
      id: this.id,
    };
  }

  public toJSON(): DeepPartial<Service> {
    return {
      priceItems: this.priceItems && this.priceItems.map(priceItem => priceItem.toJSON()),
      entryTypes: this.entryTypes && this.entryTypes.map(entryType => entryType.toJSON()),
      type: this.type,
      provider: this.provider,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      name: this.name,
      description: this.description,
      id: this.id,
    };
  }
}
