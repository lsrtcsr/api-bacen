import { IsOptional, IsNotEmpty } from 'class-validator';
import { Periodicity } from './Periodicity';

export interface ClosingSettings {
  periodicity?: Periodicity;
  dayOfWeek?: number; // between 0 (Sunday) and 6 (Saturday)
  dayOfMonth?: number;
  lastDayOfMonth?: boolean;
}

export interface BillingSettingsSchema {
  periodCalculation?: ClosingSettings;
  invoiceClosing: ClosingSettings;
  settlementTrigger: 'periodClosing' | 'invoiceClosing';
}

export class BillingSettings implements BillingSettingsSchema {
  @IsOptional()
  periodCalculation?: ClosingSettings = undefined;

  @IsNotEmpty()
  invoiceClosing: ClosingSettings = undefined;

  @IsNotEmpty()
  settlementTrigger: 'periodClosing' | 'invoiceClosing' = 'invoiceClosing';

  constructor(data: Partial<BillingSettingsSchema> = {}) {
    Object.assign(this, data);
  }
}
