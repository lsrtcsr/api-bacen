import { Column, Entity, DeepPartial, OneToMany } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ExtendedEntity } from '../base';
import { ServiceConsumptionData } from '../../schemas';
import { Entry } from './Entry';
import { User } from '../user';
import { EventPipelinePublisher } from '../../services';

export enum EventStatus {
  PENDING = 'pending',
  PROCESSED = 'processed',
}

@Entity(Event.TABLE_NAME)
export class Event extends ExtendedEntity {
  private static readonly TABLE_NAME = 'events';

  @IsOptional()
  @Column('jsonb', { nullable: false })
  data: ServiceConsumptionData;

  @IsNotEmpty()
  @Column('integer', { default: 0, nullable: false })
  retries: number = 0;

  @OneToMany(
    type => Entry,
    entry => entry.event,
    { cascade: ['insert', 'update'] },
  )
  entries: Entry[];

  @IsNotEmpty()
  @Column({ type: 'enum', enum: EventStatus, default: EventStatus.PENDING, nullable: false })
  status: EventStatus = EventStatus.PENDING;

  public static async getPendingEvents(user: User): Promise<Event[]> {
    return Event.createQueryBuilder('event')
      .where('status = :status', { status: EventStatus.PENDING })
      .andWhere("data->>'userId' = :userId", { userId: user.id })
      .getMany();
  }

  public async publishToPipeline(): Promise<boolean> {
    return EventPipelinePublisher.getInstance().send(this);
  }

  constructor(data: Partial<Event> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public toSimpleJSON(): DeepPartial<Event> {
    return {
      status: this.status,
      id: this.id,
    };
  }

  public toJSON(): DeepPartial<Event> {
    return {
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      data: this.data,
      retries: this.retries,
      status: this.status,
      id: this.id,
    };
  }
}
