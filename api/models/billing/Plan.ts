import { Column, Entity, ManyToOne, DeepPartial, OneToMany, getManager, EntityManager, Brackets } from 'typeorm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ServiceType } from '@bacen/base-sdk';
import * as moment from 'moment';
import { ExtendedEntity } from '../base';
import { Contract } from './Contract';
import { EntryType, EventType, EntryClazz } from './EntryType';
import { PriceItem } from './PriceItem';
import { Domain } from '../domain';
import { BillingSettings, ClosingSettings } from './BillingSettings';
import { Periodicity } from './Periodicity';
import { MomentValueTransformer } from '../base/MomentValueTransformer';

export enum PlanStatus {
  AVAILABLE = 'available',
  UNAVAILABLE = 'unavailable',
}

@Entity(Plan.TABLE_NAME)
export class Plan extends ExtendedEntity {
  private static readonly TABLE_NAME = 'plans';

  @IsNotEmpty()
  @ManyToOne(
    type => Domain,
    domain => domain.plans,
    { nullable: false },
  )
  supplier: Domain;

  @IsNotEmpty()
  @Column({ nullable: false })
  name: string;

  @OneToMany(
    type => EntryType,
    entryType => entryType.plan,
    {
      cascade: ['insert', 'update'],
      onDelete: 'SET NULL',
    },
  )
  items: EntryType[];

  @OneToMany(
    type => Contract,
    contract => contract.plan,
    { onDelete: 'RESTRICT' },
  )
  contracts: Contract[];

  @OneToMany(
    type => PriceItem,
    item => item.plan,
    { cascade: ['insert', 'update'] },
  )
  priceList: PriceItem[];

  @IsNotEmpty()
  @Column('timestamp with time zone', {
    name: 'valid_from',
    transformer: new MomentValueTransformer(),
    nullable: false,
  })
  validFrom: moment.Moment;

  @IsOptional()
  @Column('timestamp with time zone', {
    name: 'valid_until',
    transformer: new MomentValueTransformer(),
    nullable: true,
  })
  validUntil?: moment.Moment;

  @IsNotEmpty()
  @Column({ type: 'enum', enum: PlanStatus, default: PlanStatus.AVAILABLE, nullable: false })
  status: PlanStatus = PlanStatus.AVAILABLE;

  @IsNotEmpty()
  @Column({ default: true, nullable: false })
  default: boolean;

  @IsNotEmpty()
  @Column('jsonb', { name: 'billing_settings', nullable: false })
  billingSettings: BillingSettings = new BillingSettings();

  constructor(data: Partial<Plan> = {}) {
    super(data);
    Object.assign(this, data);
  }

  public async getCurrentPrice(
    serviceType: ServiceType,
    provider?: string,
    transaction: EntityManager = getManager(),
  ): Promise<PriceItem> {
    let priceItem;
    if (this.priceList && this.priceList.length) {
      priceItem = this.priceList.find(
        priceItem =>
          priceItem.current &&
          priceItem.service.type === serviceType &&
          (provider ? priceItem.service.provider === provider : true),
      );
    } else {
      let qb = PriceItem.createQueryBuilder('priceItem')
        .innerJoinAndSelect('priceItem.service', 'service')
        .leftJoinAndSelect('priceItem.plan', 'plan')
        .where('plan.id = :id', { id: this.id })
        .andWhere('current = true')
        .andWhere('service.type = :serviceType', { serviceType });

      if (provider) {
        qb = qb.andWhere('service.provider = :provider', { provider });
      }

      priceItem = await qb.getOne();
    }

    return priceItem;
  }

  public async setPriceItem(item: PriceItem, transaction: EntityManager = getManager()): Promise<PriceItem> {
    const updatedAt = moment();
    const current = await this.getCurrentPrice(item.service.type, item.service.provider);
    if (current) {
      if (item.amount && item.amount === current.amount) return current;

      await transaction.update(PriceItem, current.id, {
        current: false,
        validUntil: item.validFrom || updatedAt,
      });
    }

    if (!item.amount) return undefined;

    const insertResult = await transaction.insert(PriceItem, {
      plan: this.id as any,
      validFrom: item.validFrom || updatedAt,
      ...item,
    });
    const itemId = insertResult.identifiers[0].id;

    return transaction.findOne(PriceItem, { where: { id: itemId } });
  }

  public async getEntryTypes(options: {
    eventType: EventType;
    serviceType?: ServiceType;
    provider?: string;
    referenceDate?: moment.Moment;
  }): Promise<EntryType[]> {
    const referenceDate = options.referenceDate || moment();

    let entryTypes: EntryType[] = [];
    if (this?.items.length) {
      entryTypes = this.items.filter(
        item =>
          item.valid &&
          item.settings.billingTrigger === options.eventType &&
          (!item.validUntil || item.validUntil.isAfter(referenceDate)) &&
          (options.serviceType ? item.service.type === options.serviceType : true) &&
          (options.provider ? item.service.provider === options.provider : true),
      );
    } else {
      let qb = EntryType.createQueryBuilder('entryType')
        .innerJoinAndSelect('entryType.service', 'service')
        .leftJoinAndSelect('entryType.plan', 'plan')
        .where('plan.id = :id', { id: this.id })
        .andWhere('settings.billingTrigger = :trigger', { trigger: options.eventType })
        .andWhere('valid = true');

      if (options.serviceType) {
        qb = qb.andWhere('service.type = :serviceType', { serviceType: options.serviceType });
      }

      if (options.provider) {
        qb = qb.andWhere('service.provider = :provider', { provider: options.provider });
      }

      qb = qb.andWhere(
        new Brackets(sqb => {
          sqb.where('validUntil IS NULL');
          sqb.orWhere('validUntil > :referenceDate', { referenceDate: referenceDate.toDate() });
        }),
      );

      entryTypes = await qb.getMany();
    }

    return entryTypes;
  }

  public async getServiceTypes(eventType: EventType, referenceDate: moment.Moment = moment()): Promise<ServiceType[]> {
    let serviceTypes: ServiceType[] = [];

    let { items } = this;
    if (!items) {
      items = await EntryType.safeFind({
        where: {
          plan: { id: this.id },
          settings: { billingTrigger: eventType },
          valid: true,
        },
      });
    }

    serviceTypes = items
      .filter(
        item =>
          item.settings.billingTrigger === eventType &&
          item.valid &&
          (!item.validUntil || item.validUntil.isAfter(referenceDate)),
      )
      .map(item => item.service.type);

    return serviceTypes;
  }

  public async getChargeSettingsByService(serviceType: ServiceType, provider?: string): Promise<EntryType> {
    const planItems =
      this.items && this.items.length ? this.items : await EntryType.safeFind({ where: { plan: { id: this.id } } });

    const preSelectedItems = planItems.filter(
      item => item.service.type === serviceType && item.settings.clazz === EntryClazz.SERVICE_CHARGE && item.valid,
    );

    return provider ? preSelectedItems.find(item => item.service.provider === provider) : preSelectedItems[0];
  }

  public async setEntryType(item: EntryType, manager: EntityManager = getManager()): Promise<EntryType> {
    const current =
      item.settings.clazz === EntryClazz.SERVICE_CHARGE
        ? await this.getChargeSettingsByService(item.service.type, item.service.provider)
        : undefined;

    const updatedAt = moment();
    // if its a service charge setting item thats has already been set
    if (current) {
      await manager.update(EntryType, current.id, {
        validUntil: updatedAt,
        valid: false,
      });
    }

    const insertResult = await manager.insert(EntryType, {
      plan: this.id as any,
      validFrom: item.validFrom || updatedAt,
      ...item,
    });
    const entryTypeId = insertResult.identifiers[0].id;

    return manager.findOne(EntryType, { where: { id: entryTypeId } });
  }

  /**
   * TODO Address timezone problem
   *
   * @param settings
   * @param from
   */
  public async getNextClosingDate(settings: ClosingSettings, from?: moment.Moment): Promise<moment.Moment> {
    const startDate = from ? from.clone() : moment();

    let closingDate;
    switch (settings.periodicity) {
      case Periodicity.DAILY:
        closingDate = moment()
          .date(startDate.date() + 1)
          .endOf('date');
        break;

      case Periodicity.WEEKLY:
        const dayOfWeek =
          startDate.day() > settings.dayOfWeek
            ? settings.dayOfWeek + 7 // next week
            : settings.dayOfWeek; // current week

        closingDate = moment()
          .day(dayOfWeek)
          .endOf('date');
        break;

      case Periodicity.MONTHLY:
        if (settings.lastDayOfMonth) {
          closingDate = startDate.endOf('month');
        } else {
          const month = settings.dayOfMonth > startDate.date() ? startDate.month() : startDate.month() + 1;

          closingDate = moment()
            .month(month)
            .date(settings.dayOfMonth)
            .endOf('date');
        }
        break;
    }

    return closingDate;
  }

  public async getNextInvoiceClosingDate(from?: moment.Moment): Promise<moment.Moment> {
    return this.getNextClosingDate(this.billingSettings.invoiceClosing, from);
  }

  public async getNextPeriodClosingDate(from?: moment.Moment): Promise<moment.Moment> {
    const nextInvoiceClosingDate = await this.getNextClosingDate(this.billingSettings.invoiceClosing, from);

    if (!this.billingSettings.periodCalculation) return nextInvoiceClosingDate;

    const nextPeriodClosingDate = await this.getNextClosingDate(this.billingSettings.periodCalculation, from);

    return nextPeriodClosingDate.isAfter(nextInvoiceClosingDate) ? nextInvoiceClosingDate : nextPeriodClosingDate;
  }

  public toJSON(): DeepPartial<Plan> {
    return {
      id: this.id,
      name: this.name,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      validFrom: this.validFrom ? this.validFrom.format() : this.validFrom,
      validUntil: this.validUntil ? this.validUntil.format() : this.validUntil,
      status: this.status,
      default: this.default,
      // Relations
      supplier: this.supplier && this.supplier.toSimpleJSON(),
      items: this.items && this.items.filter(item => item.valid).map(item => item.toJSON()),
      priceList: this.priceList && this.priceList.filter(item => item.current).map(item => item.toJSON()),
      billingSettings: this.billingSettings,
    };
  }
}
