import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { ExtendedEntity } from '../base';
import { Period } from './Period';
import { PeriodStatus } from './PeriodStatus';

@Entity(PeriodState.tableName)
export class PeriodState extends ExtendedEntity {
  private static readonly tableName = 'period_states';

  @Column('enum', { nullable: false, enum: PeriodStatus })
  status: PeriodStatus;

  @ManyToOne(
    type => Period,
    period => period.states,
    {
      nullable: false,
      onDelete: 'CASCADE',
    },
  )
  period: Period;

  @Column({ type: 'jsonb', default: {} })
  additionalData?: any;

  constructor(data: Partial<PeriodState> = {}) {
    super(data);
    this.status = data.status;
    this.additionalData = data.additionalData;
  }

  /**
   * Converts the instance to a JSON to be returned from the public API.
   */
  public toJSON(): DeepPartial<PeriodState> {
    return {
      id: this.id,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
