import * as express from 'express';
import { ServerOptions } from 'ts-framework';
import { RedisService, HeartbeatService } from '@bacen/service-heartbeat';
import { HeartbeatType } from '@bacen/base-sdk';
import Config from '../../config';
import { PostbackPipelineJobManager, PublishQueueGroup, PostbackSubscribeQueueGroup } from '../groups';
import { ImpersonateGrantType, SecretTokenGrantType } from '../helpers';
import MainServer, { MainServerOptions } from '../MainServer';
import { AssetService } from '../services';

const { logger } = Config.server;

// AMQP base service components group
PostbackSubscribeQueueGroup.initialize({ logger });

// Initialize job manager groups
PostbackPipelineJobManager.initialize({ logger });

export interface PostbackWorkersServerOptions extends MainServerOptions {}

export class PostbackWorkersServer extends MainServer {
  public options: PostbackWorkersServerOptions;

  constructor(options?: PostbackWorkersServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, ...otherOptions } = { ...Config.server, ...options };

    const components = [
      /* Heartbeat should be initialized first */
      new HeartbeatService({ type: HeartbeatType. _POSTBACK_WORKERS, logger }),

      /* Pipeline Services */
      PublishQueueGroup.getInstance(),
      PostbackSubscribeQueueGroup.getInstance(),

      /* Job manager */
      PostbackPipelineJobManager.getInstance(),

      /* Other Services */
      AssetService.getInstance(),
      RedisService.initialize({ clientOpts: Config.redis, logger }),
    ];

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...children, ...components],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('../controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('../models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  async listen() {
    await this.onInit();
    return undefined;
  }

  public async onInit(): Promise<void> {
    await super.onInit();
    await super.onReady();
    this.logger.info('Successfully started server in Workers mode, will not listen to any requests');
  }
}
