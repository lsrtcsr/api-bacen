import * as express from 'express';
import { ServerOptions } from 'ts-framework';
import { RedisService, HeartbeatService } from '@bacen/service-heartbeat';
import { HeartbeatType } from '@bacen/base-sdk';
import Config from '../../config';
import { BillingPipelineJobManager, PublishQueueGroup, BillingSubscribeQueueGroup } from '../groups';
import { ImpersonateGrantType, SecretTokenGrantType } from '../helpers';
import MainServer, { MainServerOptions } from '../MainServer';
import { AssetService, CacheService } from '../services';

const { logger } = Config.server;

// AMQP base service components group
BillingSubscribeQueueGroup.initialize({ logger });

// Initialize job manager groups
BillingPipelineJobManager.initialize({ logger });

export interface BillingWorkersServerOptions extends MainServerOptions {}

export class BillingWorkersServer extends MainServer {
  public options: BillingWorkersServerOptions;

  constructor(options?: BillingWorkersServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, ...otherOptions } = { ...Config.server, ...options };

    // Initialize cache service
    CacheService.initialize(Config.redis);

    const components = [
      /* Heartbeat should be initialized first */
      new HeartbeatService({ type: HeartbeatType. _BILLING_WORKERS, logger }),

      /* Pipeline Services */
      PublishQueueGroup.getInstance(),
      BillingSubscribeQueueGroup.getInstance(),

      /* Job manager */
      BillingPipelineJobManager.getInstance(),

      /* Other Services */
      AssetService.getInstance(),
      RedisService.initialize({ clientOpts: Config.redis, logger }),
    ];

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...children, ...components],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('../controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('../models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  async listen() {
    await this.onInit();
    return undefined;
  }

  public async onInit(): Promise<void> {
    await super.onInit();
    await super.onReady();
    this.logger.info('Successfully started server in Workers mode, will not listen to any requests');
  }

  public async onUnmount(): Promise<void> {
    let cacheServiceInstance: CacheService;
    try {
      cacheServiceInstance = CacheService.getInstance();
      await cacheServiceInstance.quit();
    } catch (error) {}
  }
}
