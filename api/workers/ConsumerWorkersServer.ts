import * as express from 'express';
import { ServerOptions } from 'ts-framework';
import { RedisService, HeartbeatService } from '@bacen/service-heartbeat';
import { HeartbeatType, ConsumerStatus } from '@bacen/base-sdk';
import { CcsService } from '@bacen/ccs-sdk';
import { AmqpService } from '@bacen/events';
import { PubSub } from '@google-cloud/pubsub';
import Config from '../../config';
import { ConsumerPipelineJobManager, PublishQueueGroup, ConsumerSubscribeQueueGroup } from '../groups';
import { ImpersonateGrantType, SecretTokenGrantType } from '../helpers';
import MainServer, { MainServerOptions } from '../MainServer';
import {
  OtherAssetsHeldInCustodyJob,
  DeletePendingConsumerAfterTimeoutJob,
  RemoveConsumersMarkedForDeletionJob,
  InactivateUsersPendingDeletionJob,
  DeleteWalletsWithDisabledUsersJob,
} from '../jobs';
import { AssetService, CacheService } from '../services';
import * as Consumers from '../consumers';
import * as Events from '../events';

const { logger } = Config.server;

// AMQP base service components group
ConsumerSubscribeQueueGroup.initialize({ logger });

// Initialize job manager groups
ConsumerPipelineJobManager.initialize({ logger });

export interface ConsumerWorkerServerOptions extends MainServerOptions {}

export class ConsumerWorkersServer extends MainServer {
  public options: ConsumerWorkerServerOptions;

  private ccsService: CcsService;

  constructor(options?: ConsumerWorkerServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, ...otherOptions } = { ...Config.server, ...options };

    const identityPubSub = new PubSub({
      projectId: Config.identity.gcloudProjectId,
      keyFile: Config.identity.gcloudCredentials,
    });

    // Initialize cache service
    CacheService.initialize(Config.redis);

    const components = [
      new HeartbeatService({ type: HeartbeatType. _CONSUMER_WORKERS, logger }),

      /* Pipeline Services */
      PublishQueueGroup.getInstance(),
      ConsumerSubscribeQueueGroup.getInstance(),

      /* Job manager */
      ConsumerPipelineJobManager.getInstance(),

      /* Other Services */
      AssetService.getInstance(),
      RedisService.initialize({ clientOpts: Config.redis, logger }),

      /** Startup jobs */
      new OtherAssetsHeldInCustodyJob({ logger }),

      /** Management Jobs */
      // new RegisterMissingInitialAssetsJob({ logger }),
      new DeletePendingConsumerAfterTimeoutJob({ logger }),
      new RemoveConsumersMarkedForDeletionJob({ logger, markedForDeletionStates: [ConsumerStatus.PENDING_DELETION] }),
      new InactivateUsersPendingDeletionJob({ logger }),
      new DeleteWalletsWithDisabledUsersJob({ logger }),

      AmqpService.initialize({
        logger,
        connectionUrl: Config.queue.amqp.host,
        prefetch: 1,
        consumers: [
          Consumers.RegisterWalletOnStellarConsumer,
          Consumers.RegisterRootMediatorAssetsConsumer,
          Consumers.RegisterAssetConsumer,
          Consumers.ApproveAssetRegistrationConsumer,
        ],
        events: Object.values(Events),
      }),
      new Consumers.ProcessReportStatusChangedPubSubConsumer(logger, identityPubSub as any, {
        topic: Config.identity.reportEventsTopic,
      }),
    ];

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...children, ...components],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('../controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('../models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  async listen() {
    await this.onInit();
    return undefined;
  }

  public async onInit(): Promise<void> {
    this.ccsService = CcsService.initialize({
      baseURL: Config.ccs.baseURL,
      projectId: Config.ccs.projectId,
      topicId: Config.ccs.userNotificationTopic,
    });

    await super.onInit();
    await super.onReady();
    this.logger.info('Successfully started server in Workers mode, will not listen to any requests');
  }

  public async onUnmount(): Promise<void> {
    let cacheServiceInstance: CacheService;
    try {
      cacheServiceInstance = CacheService.getInstance();
      await cacheServiceInstance.quit();
    } catch (error) {}
  }
}
