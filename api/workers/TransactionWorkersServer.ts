import * as express from 'express';
import { ServerOptions } from 'ts-framework';
import { RedisService, HeartbeatService } from '@bacen/service-heartbeat';
import { HeartbeatType } from '@bacen/base-sdk';
import { Component } from 'ts-framework-common';
import Config from '../../config';
import { TransactionPipelineJobManager, PublishQueueGroup, TransactionSubscribeQueueGroup } from '../groups';
import { ImpersonateGrantType, SecretTokenGrantType } from '../helpers';
import MainServer, { MainServerOptions } from '../MainServer';
import { AssetService, CacheService, Base2ConciliationService } from '../services';
import { RegisterMissingInitialAssetsJob, OtherAssetsHeldInCustodyJob, WalletManagementJob } from '../jobs';

const { logger } = Config.server;

// AMQP base service components group
TransactionSubscribeQueueGroup.initialize({ logger });

// Initialize job manager groups
TransactionPipelineJobManager.initialize({ logger });

export interface TransactionWorkersServerOptions extends MainServerOptions {}

export class TransactionWorkersServer extends MainServer {
  public options: TransactionWorkersServerOptions;

  constructor(options?: TransactionWorkersServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, ...otherOptions } = { ...Config.server, ...options };

    // Initialize cache service
    CacheService.initialize(Config.redis);

    const components: Component[] = [
      /* Heartbeat should be initialized first */
      new HeartbeatService({ type: HeartbeatType. _TRANSACTION_WORKERS, logger }),

      /* Pipeline Services */
      PublishQueueGroup.getInstance(),
      TransactionSubscribeQueueGroup.getInstance(),

      /* Job manager */
      TransactionPipelineJobManager.getInstance(),

      /* Other Services */
      AssetService.getInstance(),

      RedisService.initialize({ clientOpts: Config.redis, logger }),
      new HeartbeatService({ type: HeartbeatType. _TRANSACTION_WORKERS, logger }),

      /** Startup jobs */
      new OtherAssetsHeldInCustodyJob({ logger }),
      // TODO: RegisterMissingInitialAssetsJob is generating a huge load and crashing the worker on startup, needs major refactor.
      new RegisterMissingInitialAssetsJob({ logger }),

      RedisService.initialize({ clientOpts: Config.redis, logger }),

      /* Stellar management jobs */
      new WalletManagementJob({
        logger,
        channelAccounts: Config.stellar.channelAccounts,
      }),
    ];

    /* Conciliation Services */
    if (Config.pubSubConfig.base2ResponseTopic) {
      components.push(
        Base2ConciliationService.initialize({ responseTopic: Config.pubSubConfig.base2ResponseTopic, logger }),
      );
    }

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...children, ...components],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('../controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('../models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  async listen() {
    await this.onInit();
    return undefined;
  }

  public async onInit(): Promise<void> {
    await super.onInit();
    await super.onReady();
    this.logger.info('Successfully started server in Workers mode, will not listen to any requests');
  }

  public async onUnmount(): Promise<void> {
    let cacheServiceInstance: CacheService;
    try {
      cacheServiceInstance = CacheService.getInstance();
      await cacheServiceInstance.quit();
    } catch (error) {}
  }
}
