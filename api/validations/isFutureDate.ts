// tslint:disable:function-name
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsFutureDate', async: false })
class IsFutureDateConstraint implements ValidatorConstraintInterface {
  validate(date: Date, validationArguments: ValidationArguments) {
    const now = Date.now();

    return date instanceof Date && date.getTime() > now;
  }

  defaultMessage(args: ValidationArguments) {
    return '($value) must be a future date.';
  }
}

export function IsFutureDate(validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      propertyName,
      target: object.constructor,
      options: validationOptions,
      constraints: [],
      validator: IsFutureDateConstraint,
    });
  };
}
