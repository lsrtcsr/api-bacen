import { BaseEvent, DomainEvent } from '@bacen/events';
import { WALLET_EVENTS_EXC } from './exchanges';

export interface RootMediatorWalletCreatedEventSchema {
  walletId: string;
}

@DomainEvent(WALLET_EVENTS_EXC)
export class RootMediatorWalletCreatedEvent extends BaseEvent implements RootMediatorWalletCreatedEventSchema {
  constructor(public readonly walletId: string) {
    super();
  }
}
