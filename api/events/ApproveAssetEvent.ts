import { BaseEvent, DomainEvent } from '@bacen/events';
import { WALLET_EVENTS_EXC } from './exchanges';

export interface ApproveAssetEventSchema {
  readonly assetRegistrationId: string;
}

@DomainEvent(WALLET_EVENTS_EXC)
export class ApproveAssetEvent extends BaseEvent implements ApproveAssetEventSchema {
  constructor(public readonly assetRegistrationId: string) {
    super();
  }
}
