import { BaseEvent, DomainEvent } from '@bacen/events';
import { WALLET_EVENTS_EXC } from './exchanges';

export interface WalletCreatedEventSchema {
  walletId: string;
}

@DomainEvent(WALLET_EVENTS_EXC)
export class WalletCreatedEvent extends BaseEvent implements WalletCreatedEventSchema {
  constructor(public readonly walletId: string) {
    super();
  }
}
