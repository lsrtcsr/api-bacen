import * as fastJson from 'fast-json-stringify';
import { Transaction } from '../models';

// TODO: extract schemas
export const transactionSerializer: (transaction: Transaction) => string = fastJson({
  title: 'Wallet withdraw success response schema',
  definitions: {
    additionalData: {
      type: 'object',
      additionalProperties: true,
    },
    stellar: {
      type: 'object',
      properties: {
        publicKey: { type: 'string' },
      },
    },
  },
  type: 'object',
  properties: {
    id: { type: 'string' },
    type: { type: 'string' },
    status: { type: 'string' },
    createdAt: { type: 'string' },
    updatedAt: { type: 'string' },
    banking: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        bank: { type: 'string' },
        agency: { type: 'string' },
        agencyDigit: { type: 'string' },
        account: { type: 'string' },
        accountDigit: { type: 'string' },
        type: { type: 'string' },
        holderType: { type: 'string' },
        name: { type: 'string' },
        taxId: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      },
    },
    payments: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          type: { type: 'string' },
          status: { type: 'string' },
          createdAt: { type: 'string' },
          updatedAt: { type: 'string' },
          scheduleFor: { type: 'string', nullable: true },
          amount: { type: 'string' },
          destination: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              root: { type: 'boolean' },
              stellar: { $ref: '#/definitions/stellar' },
              updatedAt: { type: 'string' },
              createdAt: { type: 'string' },
              deletedAt: { type: 'string', nullable: true },
              additionalData: { $ref: '#/definitions/additionalData' },
            },
          },
          asset: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              name: { type: 'string' },
              code: { type: 'string' },
              root: { type: 'boolean' },
              provider: { type: 'string' },
              required: { type: 'boolean' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          },
        },
      },
    },
    source: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        root: { type: 'boolean' },
        stellar: { $ref: '#/definitions/stellar' },
        updatedAt: { type: 'string' },
        createdAt: { type: 'string' },
        deletedAt: { type: 'string', nullable: true },
        user: {
          type: 'object',
          properties: {
            id: { type: 'string' },
            name: { type: 'string' },
            firstName: { type: 'string' },
            lastName: { type: 'string' },
            email: { type: 'string' },
            role: { type: 'string' },
            twoFactorRequired: { type: 'string', nullable: true },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
            domain: {
              type: 'object',
              properties: {
                id: { type: 'string' },
                name: { type: 'string' },
                role: { type: 'string' },
              },
            },
          },
        },
        additionalData: { $ref: '#/definitions/additionalData' },
      },
    },
    states: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string' },
          status: { type: 'string' },
          createdAt: { type: 'string' },
          updatedAt: { type: 'string' },
          additionalData: {
            type: 'object',
            additionalProperties: true,
          },
        },
      },
    },
    additionalData: { $ref: '#/definitions/additionalData' },
  },
});
