import { EntityManager, getManager } from 'typeorm';
import { AssetRegistrationStatus } from '@bacen/base-sdk';
import * as faker from 'faker';
import MainDatabase from '../MainDatabase';
import { AssetRegistrationRepository } from './AssetRegistrationRepository';
import { User, Wallet, Asset, AssetRegistration, AssetRegistrationState } from '../models';
import { UserTestUtil } from '../../tests/util';

describe('api.repositories.AssetRegistrationRepository', () => {
  let database: MainDatabase;
  let manager: EntityManager;
  let repository: AssetRegistrationRepository;
  let user: User;
  let wallet: Wallet;
  let issuerWallet: Wallet;
  let asset: Asset;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    manager = getManager();
    repository = new AssetRegistrationRepository(manager);
  });

  beforeEach(async () => {
    await manager.query('START TRANSACTION');
    user = await UserTestUtil.generateUser({ manager });

    wallet = manager.create(Wallet, { user });
    issuerWallet = manager.create(Wallet, { user });
    await manager.insert(Wallet, [wallet, issuerWallet]);

    asset = manager.create(Asset, { issuer: issuerWallet, code: faker.finance.currencyCode() });
    await manager.insert(Asset, asset);
  });

  afterEach(async () => {
    await manager.query('ROLLBACK');
  });

  describe('create', () => {
    it('inserts a new assetRegistration to the DB', async () => {
      const assetRegistration = await repository.create(asset, wallet);

      expect(assetRegistration).toBeDefined();
      expect(assetRegistration.id).toBeDefined();
      expect(assetRegistration.wallet!.id).toBe(wallet.id);
      expect(assetRegistration.asset!.id).toBe(asset.id);
    });

    it('populates the reportId column', async () => {
      const reportId = faker.random.uuid();
      const assetRegistration = await repository.create(asset, wallet, reportId);

      expect(assetRegistration).toBeDefined();
      expect(assetRegistration.id).toBeDefined();
      expect(assetRegistration.reportId).toBe(reportId);
    });
  });

  describe('appendState', () => {
    let assetRegistration: AssetRegistration;

    beforeEach(async () => {
      assetRegistration = manager.create(AssetRegistration, { asset, wallet });
      await manager.insert(AssetRegistration, assetRegistration);
    });

    it('appends a new state for a assetRegistration which has no previous states', async () => {
      await repository.appendState(assetRegistration, { status: AssetRegistrationStatus.PENDING });

      expect(assetRegistration.states.length).toBe(1);
      expect(assetRegistration.states[0].id).toBeDefined();
      expect(assetRegistration.status).toBe(AssetRegistrationStatus.PENDING);
    });

    it('appends a new state for a assetRegistration which has previous states', async () => {
      const previousState = manager.create(AssetRegistrationState, {
        assetRegistration,
        status: AssetRegistrationStatus.PENDING,
      });
      await manager.insert(AssetRegistrationState, previousState);
      assetRegistration.states = [previousState];

      await repository.appendState(assetRegistration, { status: AssetRegistrationStatus.READY });

      expect(assetRegistration.states.length).toBe(2);
      expect(assetRegistration.states.map(state => state.status)).toEqual([
        AssetRegistrationStatus.READY,
        AssetRegistrationStatus.PENDING,
      ]);
      expect(assetRegistration.states[0].id).toBeDefined();
      expect(assetRegistration.status).toBe(AssetRegistrationStatus.READY);
    });
  });
});
