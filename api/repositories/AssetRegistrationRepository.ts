import { EntityManager, getManager } from 'typeorm';
import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { leftJoinAndSelectRelations } from '@bacen/shared-sdk';
import { Asset, Wallet, AssetRegistration, AssetRegistrationState } from '../models';
import { StatePartial } from '../utils';

export class AssetRegistrationRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public async getById(assetRegistrationId: string, relations: string[] = []): Promise<AssetRegistration | undefined> {
    const baseQb = this.manager
      .createQueryBuilder(AssetRegistration, 'assetRegistration')
      .where('assetRegistration.id = :assetRegistrationId', { assetRegistrationId });

    leftJoinAndSelectRelations(baseQb, relations, 'assetRegistration');
    return baseQb.getOne();
  }

  public async getByWalletAndAssetId(
    assetId: string,
    walletId: string,
    relations: string[] = [],
  ): Promise<AssetRegistration | undefined> {
    const baseQb = this.manager
      .createQueryBuilder(AssetRegistration, 'assetRegistration')
      .where('assetRegistration.wallet = :walletId', { walletId })
      .andWhere('assetRegistration.asset = :assetId', { assetId });

    leftJoinAndSelectRelations(baseQb, relations, 'assetRegistration');
    return baseQb.getOne();
  }

  public async create(asset: Asset, wallet: Wallet, reportId?: string): Promise<AssetRegistration> {
    const assetRegistration = this.manager.create(AssetRegistration, { asset, wallet, reportId });
    await this.manager.insert(AssetRegistration, assetRegistration);

    return assetRegistration;
  }

  public async appendState(
    assetRegistration: AssetRegistration,
    statePartial: StatePartial<AssetRegistrationStatus>,
  ): Promise<void> {
    const { status, additionalData } = statePartial;

    const state = this.manager.create(AssetRegistrationState, { assetRegistration, status, additionalData });
    await this.manager.insert(AssetRegistrationState, state);

    assetRegistration.states = assetRegistration.states ?? [];
    assetRegistration.states.unshift(state);
  }

  public async findByReportId(reportId: string, relations: string[] = []): Promise<AssetRegistration[]> {
    const baseQb = this.manager
      .createQueryBuilder(AssetRegistration, 'assetRegistration')
      .where('assetRegistration.reportId = :reportId', { reportId });

    leftJoinAndSelectRelations(baseQb, relations, 'assetRegistration');

    return baseQb.getMany();
  }

  public async updateReportId(assetRegistration: AssetRegistration, reportId: string): Promise<void> {
    assetRegistration.reportId = reportId;

    await this.manager
      .createQueryBuilder(AssetRegistration, 'assetRegistration')
      .update()
      .set({ reportId })
      .where({ id: assetRegistration.id })
      .execute();
  }
}
