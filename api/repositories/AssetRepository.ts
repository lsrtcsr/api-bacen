import { EntityManager, getManager } from 'typeorm';
import { leftJoinAndSelectRelations } from '@bacen/shared-sdk';
import { CustodyProvider } from ' /base-sdk/dist';
import { Asset } from '../models';

export class AssetRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public async getAssetByProvider(provider: CustodyProvider, relations: string[] = []): Promise<Asset | undefined> {
    const normalizedRelations = this.dedupeRelations(relations, ['issuer']);
    const baseQb = this.manager.createQueryBuilder(Asset, 'asset').where('asset.provider = :provider', { provider });
    leftJoinAndSelectRelations(baseQb, normalizedRelations, 'asset');

    return baseQb.getOne();
  }

  public async getAssetsByCode(codes: string[], relations: string[] = []): Promise<Asset[]> {
    const normalizedRelations = this.dedupeRelations(relations, ['issuer']);
    const baseQb = this.manager.createQueryBuilder(Asset, 'asset').where('asset.code IN (:...codes)', { codes });
    leftJoinAndSelectRelations(baseQb, normalizedRelations, 'asset');

    return baseQb.getMany();
  }

  private dedupeRelations(relations: string[], defaultRelations: string[]): string[] {
    return Array.from(new Set([...defaultRelations, ...relations]));
  }
}
