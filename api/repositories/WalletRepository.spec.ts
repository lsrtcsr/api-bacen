import { EntityManager, getManager } from 'typeorm';
import { WalletStatus, TransitoryAccountType } from '@bacen/base-sdk';
import * as Stellar from 'stellar-sdk';
import { User, Wallet, WalletState } from '../models';
import { WalletRepository } from './WalletRepository';
import MainDatabase from '../MainDatabase';
import { UserTestUtil } from '../../tests/util';

describe('api.repositories.WalletRepository', () => {
  let database: MainDatabase;
  let manager: EntityManager;
  let repository: WalletRepository;
  let user: User;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    manager = getManager();
    repository = new WalletRepository(manager);
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await manager.query('START TRANSACTION');
    user = await UserTestUtil.generateUser({ manager });
  });

  afterEach(async () => {
    await manager.query('ROLLBACK');
  });

  describe('create', () => {
    it('inserts a new wallet to the DB', async () => {
      const wallet = await repository.create({ user });
      expect(wallet.id).toBeDefined();

      const walletFromDb = await manager.findOne(Wallet, wallet.id, { relations: ['user'] });
      expect(walletFromDb).toBeDefined();
      expect(walletFromDb.user.id).toBe(user.id);
    });

    it('inserts a new wallet to the DB with the provided stellar keys', async () => {
      const keypair = Stellar.Keypair.random();

      const stellar = {
        publicKey: keypair.publicKey(),
        secretKey: keypair.secret(),
      };

      const wallet = await repository.create({ user, stellar });
      expect(wallet.id).toBeDefined();
      expect(wallet.stellar).toEqual(stellar);
    });

    it('inserts a new wallet to the DB with the provided additionalData', async () => {
      const additionalData = {
        isTransitoryAccount: true,
        transitoryAccountType: TransitoryAccountType.BOLETO_PAYMENT,
      };

      const wallet = await repository.create({ user, additionalData });
      expect(wallet.id).toBeDefined();
      expect(wallet.additionalData).toEqual(additionalData);
    });
  });

  describe('appendState', () => {
    let wallet: Wallet;
    beforeEach(async () => {
      wallet = manager.create(Wallet, { user });
      await manager.insert(Wallet, wallet);
    });

    it('appends a new state for a wallet which has no previous states', async () => {
      await repository.appendState(wallet, { status: WalletStatus.PENDING });

      expect(wallet.states.length).toBe(1);
      expect(wallet.states[0].id).toBeDefined();
      expect(wallet.persistentStatus).toBe(WalletStatus.PENDING);
    });

    it('appends a new state for a wallet which has previous states', async () => {
      const previousState = manager.create(WalletState, { wallet, status: WalletStatus.PENDING });
      await manager.insert(WalletState, previousState);
      wallet.states = [previousState];

      await repository.appendState(wallet, { status: WalletStatus.READY });

      expect(wallet.states.length).toBe(2);
      expect(wallet.states.map(state => state.status)).toEqual([WalletStatus.READY, WalletStatus.PENDING]);
      expect(wallet.states[0].id).toBeDefined();
      expect(wallet.persistentStatus).toBe(WalletStatus.READY);
    });
  });
});
