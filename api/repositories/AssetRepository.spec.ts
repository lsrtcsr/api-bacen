import { EntityManager, getManager } from 'typeorm';
import * as faker from 'faker';
import MainDatabase from '../MainDatabase';
import { User, Wallet, Asset } from '../models';
import { AssetRepository } from './AssetRepository';
import { UserTestUtil } from '../../tests/util';

describe('api.repositories.AssetRepository', () => {
  let database: MainDatabase;
  let manager: EntityManager;
  let repository: AssetRepository;
  let user: User;
  let issuerWallet: Wallet;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    manager = getManager();
    repository = new AssetRepository(manager);
  });

  beforeEach(async () => {
    await manager.query('START TRANSACTION');
    user = await UserTestUtil.generateUser({ manager });

    issuerWallet = manager.create(Wallet, { user });
    await manager.insert(Wallet, issuerWallet);
  });

  afterEach(async () => {
    await manager.query('ROLLBACK');
  });

  describe('getAssetsByCode', () => {
    let assets: Asset[];
    let assetsCount: number;

    beforeEach(async () => {
      assetsCount = faker.random.number({ min: 2, max: 10 });
      assets = Array.from({ length: assetsCount }, _ =>
        manager.create(Asset, { issuer: issuerWallet, code: faker.random.word() }),
      );
      await manager.insert(Asset, assets);
    });

    it('returs a list of assets with codes matchint the codes in the given list', async () => {
      const codesCount = faker.random.number({ min: 1, max: assetsCount });
      const codes = assets.map(asset => asset.code).slice(0, codesCount);

      const returnedAssets = await repository.getAssetsByCode(codes);

      expect(returnedAssets.length).toBe(codesCount);
      codes.forEach(code => {
        expect(returnedAssets.some(asset => asset.code === code)).toBe(true);
      });
      expect(returnedAssets.every(asset => asset.issuer));
    });
  });
});
