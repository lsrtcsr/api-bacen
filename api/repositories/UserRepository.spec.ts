import { EntityManager, getManager } from 'typeorm';
import * as faker from 'faker';
import { UserRole, UserStatus } from '@bacen/base-sdk';
import { User, Domain, UserState } from '../models';
import MainDatabase from '../MainDatabase';
import { UserRepository } from './UserRepository';
import { DomainTestUtil, UserTestUtil } from '../../tests/util';

describe('api.repositories.UserRepository', () => {
  let database: MainDatabase;
  let manager: EntityManager;
  let repository: UserRepository;
  let domain: Domain;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    manager = getManager();
    repository = new UserRepository(manager);
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await manager.query('START TRANSACTION');
    domain = await DomainTestUtil.generateDomain({ manager });
  });

  afterEach(async () => {
    await manager.query('ROLLBACK');
  });

  describe('create', () => {
    it('inserts a new user to the DB', async () => {
      const user = await repository.create({
        domain,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        role: UserRole.CONSUMER,
        username: faker.internet.userName(),
      });

      expect(user.id).toBeDefined();
      expect(user.passwordHash).toBeFalsy();
      expect(user.passwordSalt).toBeFalsy();

      const userFromDb = await manager.findOne(User, user.id, { relations: ['domain'] });
      expect(userFromDb).toBeDefined();
      expect(userFromDb.domain.id).toBe(domain.id);
    });

    it('inserts a new user to the DB with the provided password', async () => {
      const password = faker.random.word();
      const user = await repository.create({
        domain,
        password,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        role: UserRole.CONSUMER,
        username: faker.internet.userName(),
      });

      expect(user.id).toBeDefined();
      expect(user.passwordHash).toBeTruthy();
      expect(user.passwordSalt).toBeTruthy();

      const userFromDb = await manager.findOne(User, user.id, { relations: ['domain'] });
      expect(userFromDb).toBeDefined();
      expect(userFromDb.domain.id).toBe(domain.id);
    });
  });

  describe('appendState', () => {
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ userDomain: domain, manager });
    });

    it('appends a new state for a user which has no previous states', async () => {
      await repository.appendState(user, { status: UserStatus.PENDING });

      expect(user.states.length).toBe(1);
      expect(user.states[0].id).toBeDefined();
      expect(user.status).toBe(UserStatus.PENDING);
    });

    it('appends a new state for a user which has previous states', async () => {
      const previousState = manager.create(UserState, { user, status: UserStatus.PENDING });
      await manager.insert(UserState, previousState);
      user.states = [previousState];

      await repository.appendState(user, { status: UserStatus.ACTIVE });

      expect(user.states.length).toBe(2);
      expect(user.states.map(state => state.status)).toEqual([UserStatus.ACTIVE, UserStatus.PENDING]);
      expect(user.states[0].id).toBeDefined();
      expect(user.status).toBe(UserStatus.ACTIVE);
    });
  });

  describe('findByUsername', () => {
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ userDomain: domain, manager });
    });

    it('find user by username', async () => {
      user = await repository.findByUsername(user.username);

      expect(user).toBeDefined();
      expect(user.id).toBeDefined();
    });
  });
});
