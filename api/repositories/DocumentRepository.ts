import { DocumentType } from '@bacen/base-sdk';
import { removeUndefinedValues, computePartialDiff } from '@bacen/shared-sdk';
import { EntityManager, getManager } from 'typeorm';
import { Consumer, Document } from '../models';

export interface DocumentDto {
  type: DocumentType;

  number?: string;

  issuer?: string;

  issuerState?: string;

  expirationDate?: string;
}

export class DocumentRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public findDocumentByConsumerAndType(consumer: Consumer, type: DocumentType): Promise<Document | undefined> {
    return this.manager
      .createQueryBuilder(Document, 'document')
      .where('document.consumer = :consumerId', { consumerId: consumer.id })
      .andWhere('document.type = :type', { type })
      .andWhere('document.deletedAt IS NULL')
      .getOne();
  }

  public async upsertDocument(consumer: Consumer, dto: DocumentDto): Promise<Document> {
    const { type, number, issuer, issuerState } = dto;
    const expirationDate = dto.expirationDate ? new Date(dto.expirationDate) : undefined;

    let document = await this.findDocumentByConsumerAndType(consumer, type);

    if (document) {
      const diff = computePartialDiff(document, removeUndefinedValues({ number, issuer, issuerState, expirationDate }));

      // do not execute query if there is no diff.
      if (Object.keys(diff).length > 0) {
        Object.assign(document, diff);
        await this.manager.createQueryBuilder(Document, 'document').update().set(diff).execute();
      }
    } else {
      document = this.manager.create(Document, { consumer, ...dto });
      await this.manager.insert(Document, document);
    }

    return document;
  }

  public async setDocumentIdentityIds(document: Document, identityIds: string[]): Promise<void> {
    document.identityIds = identityIds;
    await this.manager
      .createQueryBuilder(Document, 'document')
      .update()
      .set({ identityIds })
      .where({ id: document.id })
      .execute();
  }
}
