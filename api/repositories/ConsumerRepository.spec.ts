import { EntityManager, getManager } from 'typeorm';
import * as faker from 'faker';
import {
  ConsumerStatus,
  AccountType,
  DocumentSchema,
  DocumentType,
  BankingSchema,
  BankingType,
  PhoneSchema,
  AddressSchema,
  AddressStatus,
} from '@bacen/base-sdk';
import * as cpf from 'cpf';
import * as sinon from 'sinon';
import { User, Consumer, ConsumerState, Address } from '../models';
import { ConsumerRepository } from './ConsumerRepository';
import MainDatabase from '../MainDatabase';
import { ConsumerTestUtil, UserTestUtil } from '../../tests/util';

jest.setTimeout(1000000);

describe('api.repositories.ConsumerRepository', () => {
  let database: MainDatabase;
  let manager: EntityManager;
  let repository: ConsumerRepository;

  beforeAll(async () => {
    database = MainDatabase.getInstance();
    await database.connect();

    manager = getManager();
    repository = new ConsumerRepository(manager);
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await manager.query('START TRANSACTION');
  });

  afterEach(async () => {
    await manager.query('ROLLBACK');
  });

  describe('create', () => {
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });
    });

    it('inserts a new consumer to the DB', async () => {
      const consumer = await repository.create({
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      expect(consumer.id).toBeDefined();

      const consumerFromDb = await manager.findOne(Consumer, consumer.id, { relations: ['user'] });
      expect(consumerFromDb).toBeDefined();
      expect(consumerFromDb.user.id).toBe(user.id);
    });
  });

  describe('appendState', () => {
    let consumer: Consumer;
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });

      consumer = manager.create(Consumer, {
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      await manager.insert(Consumer, consumer);
    });

    it('appends a new state for a consumer which has no previous states', async () => {
      await repository.appendState(consumer, { status: ConsumerStatus.PENDING_DOCUMENTS });

      expect(consumer.states.length).toBe(1);
      expect(consumer.states[0].id).toBeDefined();
      expect(consumer.persistentStatus).toBe(ConsumerStatus.PENDING_DOCUMENTS);
    });

    it('appends a new state for a consumer which has previous states', async () => {
      const previousState = manager.create(ConsumerState, { consumer, status: ConsumerStatus.PENDING_DOCUMENTS });
      await manager.insert(ConsumerState, previousState);
      consumer.states = [previousState];

      await repository.appendState(consumer, { status: ConsumerStatus.READY });

      expect(consumer.states.length).toBe(2);
      expect(consumer.states.map(state => state.status)).toEqual([
        ConsumerStatus.READY,
        ConsumerStatus.PENDING_DOCUMENTS,
      ]);
      expect(consumer.states[0].id).toBeDefined();
      expect(consumer.persistentStatus).toBe(ConsumerStatus.READY);
    });
  });

  describe('createConsumerDocuments', () => {
    let consumer: Consumer;
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });
      
      consumer = manager.create(Consumer, {
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      await manager.insert(Consumer, consumer);
    });

    it('inserts a single document to the DB', async () => {
      const documentSchemas: DocumentSchema[] = [
        {
          consumer,
          type: DocumentType.BRL_INDIVIDUAL_REG,
          number: faker.finance.account(6),
        },
      ];

      await repository.createConsumerDocuments(consumer, documentSchemas);

      expect(consumer.documents.length).toBe(1);
      expect(consumer.documents[0].id).toBeDefined();
    });

    it('inserts multiple documents to the DB', async () => {
      const docsCount = faker.random.number({ min: 2, max: 5 });
      const documentSchemas: DocumentSchema[] = Array.from({ length: docsCount }, _ => ({
        consumer,
        type: faker.random.arrayElement(Object.values(DocumentType)),
        number: faker.finance.account(6),
      }));

      await repository.createConsumerDocuments(consumer, documentSchemas);

      expect(consumer.documents.length).toBe(docsCount);
      for (const document of consumer.documents) {
        expect(document.id).toBeDefined();
      }
    });
  });

  describe('createConsumerBankings', () => {
    let consumer: Consumer;
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });

      consumer = manager.create(Consumer, {
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      await manager.insert(Consumer, consumer);
    });

    it('inserts a single banking to the DB', async () => {
      const bankingSchemas: BankingSchema[] = [
        {
          consumer,
          bank: '001',
          agency: faker.finance.account(4),
          agencyDigit: '0',
          account: faker.finance.account(8),
          accountDigit: '0',
          type: BankingType.CHECKING,
          holderType: AccountType.PERSONAL,
          taxId: cpf.generate(),
        },
      ];

      await repository.createConsumerBankings(consumer, bankingSchemas);

      expect(consumer.bankings.length).toBe(1);
      expect(consumer.bankings[0].id).toBeDefined();
    });

    it('inserts multiple bankings to the DB', async () => {
      const bankingsCount = faker.random.number({ min: 2, max: 5 });
      const bankingSchemas: BankingSchema[] = Array.from({ length: bankingsCount }, _ => ({
        consumer,
        bank: '001',
        agency: faker.finance.account(4),
        agencyDigit: '0',
        account: faker.finance.account(8),
        accountDigit: '0',
        type: BankingType.CHECKING,
        holderType: AccountType.PERSONAL,
        taxId: cpf.generate(),
      }));

      await repository.createConsumerBankings(consumer, bankingSchemas);

      expect(consumer.bankings.length).toBe(bankingsCount);
      for (const banking of consumer.bankings) {
        expect(banking.id).toBeDefined();
      }
    });
  });

  describe('createConsumerPhones', () => {
    let consumer: Consumer;
    let user: User;

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });

      consumer = manager.create(Consumer, {
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      await manager.insert(Consumer, consumer);
    });

    it('inserts a single phone to the DB', async () => {
      const phoneSchemas: PhoneSchema[] = [
        {
          consumer,
          code: faker.finance.account(2),
          number: faker.finance.account(9),
        },
      ];

      await repository.createConsumerPhones(consumer, phoneSchemas);

      expect(consumer.phones.length).toBe(1);
      expect(consumer.phones[0].id).toBeDefined();
      expect(consumer.phones[0].default).toBe(true);
    });

    it('inserts multiple phones to the DB without specifying which is default', async () => {
      const phonesCount = faker.random.number({ min: 2, max: 5 });
      const phoneSchemas: PhoneSchema[] = Array.from({ length: phonesCount }, _ => ({
        consumer,
        code: faker.finance.account(2),
        number: faker.finance.account(9),
      }));

      await repository.createConsumerPhones(consumer, phoneSchemas);

      expect(consumer.phones.length).toBe(phonesCount);
      consumer.phones.forEach((phone, idx) => {
        expect(phone.id).toBeDefined();
        expect(Boolean(phone.default)).toBe(idx === 0);
      });
    });

    it('inserts multiple phones to the DB specifying which is default', async () => {
      const phonesCount = faker.random.number({ min: 2, max: 5 });
      const defaultIdx = faker.random.number({ min: 0, max: phonesCount - 1 });
      const phoneSchemas: PhoneSchema[] = Array.from({ length: phonesCount }, _ => ({
        consumer,
        code: faker.finance.account(2),
        number: faker.finance.account(9),
      }));
      phoneSchemas[defaultIdx].default = true;

      await repository.createConsumerPhones(consumer, phoneSchemas);

      expect(consumer.phones.length).toBe(phonesCount);
      consumer.phones.forEach((phone, idx) => {
        expect(phone.id).toBeDefined();
        expect(Boolean(phone.default)).toBe(idx === defaultIdx);
      });
    });
  });

  describe('createConsumerAddresses', () => {
    let consumer: Consumer;
    let user: User;
    let fromCepStub: sinon.SinonStub;

    beforeAll(() => {
      fromCepStub = sinon
        .stub(Address, 'fromCEP')
        .callsFake((_, partial) => Promise.resolve(manager.create(Address, partial)));
    });

    afterAll(() => {
      fromCepStub.restore();
    });

    beforeEach(async () => {
      user = await UserTestUtil.generateUser({ manager });

      consumer = manager.create(Consumer, {
        user,
        taxId: cpf.generate(false),
        type: AccountType.PERSONAL,
        motherName: faker.name.findName(),
        birthday: faker.date.past(),
        pep: false,
      });
      await manager.insert(Consumer, consumer);
    });

    it('inserts a single phone to the DB', async () => {
      const addressSchemas: AddressSchema[] = [
        {
          consumer,
          country: 'Brasil',
          state: 'SP',
          city: 'São Paulo',
          neighborhood: 'Bela Vista',
          street: 'Av. Paulista',
          number: '1000',
          status: AddressStatus.OWN,
          code: '0100000',
        },
      ];

      await repository.createConsumerAddresses(consumer, addressSchemas);

      expect(consumer.addresses.length).toBe(1);
      expect(consumer.addresses[0].id).toBeDefined();
      expect(consumer.addresses[0].default).toBe(true);
    });

    it('inserts multiple phones to the DB without specifying which is default', async () => {
      const addressesCount = faker.random.number({ min: 2, max: 5 });
      const addressSchemas: AddressSchema[] = Array.from({ length: addressesCount }, _ => ({
        consumer,
        country: 'Brasil',
        state: 'SP',
        city: 'São Paulo',
        neighborhood: 'Bela Vista',
        street: 'Av. Paulista',
        number: '1000',
        status: AddressStatus.OWN,
        code: '0100000',
      }));

      await repository.createConsumerAddresses(consumer, addressSchemas);

      expect(consumer.addresses.length).toBe(addressesCount);
      consumer.addresses.forEach((address, idx) => {
        expect(address.id).toBeDefined();
        expect(Boolean(address.default)).toBe(idx === 0);
      });
    });

    it('inserts multiple phones to the DB specifying which is default', async () => {
      const addressesCount = faker.random.number({ min: 2, max: 5 });
      const defaultIdx = faker.random.number({ min: 0, max: addressesCount - 1 });
      const addressSchemas: AddressSchema[] = Array.from({ length: addressesCount }, _ => ({
        consumer,
        country: 'Brasil',
        state: 'SP',
        city: 'São Paulo',
        neighborhood: 'Bela Vista',
        street: 'Av. Paulista',
        number: '1000',
        status: AddressStatus.OWN,
        code: '0100000',
      }));
      addressSchemas[defaultIdx].default = true;

      await repository.createConsumerAddresses(consumer, addressSchemas);

      expect(consumer.addresses.length).toBe(addressesCount);
      consumer.addresses.forEach((address, idx) => {
        expect(address.id).toBeDefined();
        expect(Boolean(address.default)).toBe(idx === defaultIdx);
      });
    });
  });

  describe.skip('findConsumerByStatusDate', () => {
    const status = ConsumerStatus.READY;

    it('returns nothing if the DB is empty', async () => {
      const [users, count] = await repository.findConsumerByStatusDate({ status });

      expect(users.length).toBe(0);
      expect(count).toBe(0);
    });

    it('returns nothing if no consumers have passed through the desired state', async () => {
      await ConsumerTestUtil.generateConsumer({ consumerState: ConsumerStatus.DELETED, manager });

      const [users, count] = await repository.findConsumerByStatusDate({ status });

      expect(users).toEqual([]);
      expect(count).toBe(0);
    });

    it('returns nothing if no consumers have passed through the desired state in the specified time range', async () => {
      await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });

      const [users, count] = await repository.findConsumerByStatusDate({
        status,
        range: {
          start: new Date(Date.now() - 2 * 24 * 3600 * 1000),
          end: new Date(Date.now() - 24 * 3600 * 1000),
        },
      });

      expect(users).toEqual([]);
      expect(count).toBe(0);
    });

    it('returns users that have passed through the desired state in the specified time range', async () => {
      const consumerInDesiredState = await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });

      const [users, count] = await repository.findConsumerByStatusDate({
        status,
        range: {
          start: new Date(Date.now() - 24 * 3600 * 1000),
          end: new Date(Date.now() + 24 * 3600 * 1000),
        },
      });

      expect(users.map(user => user.id)).toEqual([consumerInDesiredState.id]);
      expect(count).toBe(1);
    });

    it('returns only users that have passed through the desired state in the specified time range', async () => {
      const consumerInDesiredState = await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });
      const consumerInDesiredStateOutsideOfRange = await ConsumerTestUtil.generateConsumer({ manager });
      await manager.insert(ConsumerState, {
        consumer: consumerInDesiredStateOutsideOfRange.consumer,
        status,
        createdAt: new Date(Date.now() - 2 * 24 * 3600 * 1000),
      });

      const [users, count] = await repository.findConsumerByStatusDate({
        status,
        range: {
          start: new Date(Date.now() - 24 * 3600 * 1000),
          end: new Date(Date.now() + 24 * 3600 * 1000),
        },
      });

      expect(users.map(user => user.id)).toEqual([consumerInDesiredState.id]);
      expect(count).toBe(1);
    });

    it('does not return duplicated results if the consumer has passed through the state more than once in the time range', async () => {
      const consumerInDesiredState = await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });
      await manager.insert(ConsumerState, {
        consumer: consumerInDesiredState.consumer,
        status,
        createdAt: new Date(Date.now() - 3600 * 1000),
      });

      const [users, count] = await repository.findConsumerByStatusDate({
        status,
        range: {
          start: new Date(Date.now() - 24 * 3600 * 1000),
          end: new Date(Date.now() + 24 * 3600 * 1000),
        },
      });

      expect(users.map(user => user.id)).toEqual([consumerInDesiredState.id]);
      expect(count).toBe(1);
    });

    it('works in a complex scenario', async () => {
      const consumerInDesiredStateOnce = await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });
      const consumerInDesiredStateTwice = await ConsumerTestUtil.generateConsumer({ consumerState: status, manager });
      await manager.insert(ConsumerState, {
        consumer: consumerInDesiredStateTwice.consumer,
        status,
        createdAt: new Date(Date.now() - 3600 * 1000),
      });

      const consumerInDesiredStateOutsideOfRange = await ConsumerTestUtil.generateConsumer({ manager });
      await manager.insert(ConsumerState, {
        consumer: consumerInDesiredStateOutsideOfRange.consumer,
        status,
        createdAt: new Date(Date.now() - 2 * 24 * 3600 * 1000),
      });

      const consumerInOtherStateInTimeRange = await ConsumerTestUtil.generateConsumer({
        consumerState: ConsumerStatus.DELETED,
        manager
      });

      const [users, count] = await repository.findConsumerByStatusDate({
        status,
        range: {
          start: new Date(Date.now() - 24 * 3600 * 1000),
          end: new Date(Date.now() + 24 * 3600 * 1000),
        },
      });

      const returnedUserIds = users.map(user => user.id).sort();
      expect(returnedUserIds).toEqual([consumerInDesiredStateOnce.id, consumerInDesiredStateTwice.id].sort());
      expect(returnedUserIds).not.toContain(consumerInOtherStateInTimeRange.id);
      expect(returnedUserIds).not.toContain(consumerInDesiredStateOutsideOfRange.id);
      expect(count).toBe(2);
    });
  });
});
