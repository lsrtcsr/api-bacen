import { EntityManager, getManager } from 'typeorm';
import { UserStatus, UserRole } from '@bacen/base-sdk';
import { arraify, leftJoinAndSelectRelations, MaybeArray } from '@bacen/shared-sdk';
import { User, UserState, Domain } from '../models';
import { StatePartial } from '../utils';

export interface CreateUserOptions {
  firstName: string;

  lastName: string;

  email: string;

  role: UserRole;

  domain: Domain;

  twoFactorRequired?: boolean;

  password?: string;

  username: string;
}

export class UserRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public async getById(userId: string, relations: string[] = []): Promise<User | undefined> {
    const baseQb = this.manager.createQueryBuilder(User, 'user').where('user.id = :userId', { userId });

    leftJoinAndSelectRelations(baseQb, relations, 'user');

    return baseQb.getOne();
  }

  public async create(options: CreateUserOptions): Promise<User> {
    const { password, ...userSchema } = options;

    const user = this.manager.create(User, userSchema);
    if (password) {
      await user.setPassword(password);
    }

    await this.manager.insert(User, user);
    return user;
  }

  public async findByUsername(username: string, relations: string[] = []): Promise<User | undefined> {
    const baseQb = this.manager.createQueryBuilder(User, 'user').where('user.username = :username', { username });

    leftJoinAndSelectRelations(baseQb, relations, 'user');

    return baseQb.getOne();
  }

  public async appendState(user: User, statePartial: StatePartial<UserStatus>): Promise<void>;
  public async appendState(user: User, statePartials: StatePartial<UserStatus>[]): Promise<void>;
  public async appendState(user: User, statePartialMaybeArray: MaybeArray<StatePartial<UserStatus>>): Promise<void> {
    const statePartials = arraify(statePartialMaybeArray);

    const userState = this.manager.create(
      UserState,
      statePartials.map((partial) => ({ user, ...partial })),
    );
    await this.manager.insert(UserState, userState);

    // sort inserted userStates most-recent first
    userState.sort((left, right) => right.createdAt.getTime() - left.createdAt.getTime());

    user.states = user.states ?? [];
    user.states.unshift(...userState);
  }
}
