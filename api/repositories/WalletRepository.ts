import { getManager, EntityManager } from 'typeorm';
import { StellarWalletData, WalletStatus } from '@bacen/base-sdk';
import { leftJoinAndSelectRelations } from '@bacen/shared-sdk';
import { User, Wallet, WalletState, WalletAdditionalData } from '../models';
import { StatePartial } from '../utils';

export interface CreateWalletOptions {
  user: User;

  stellar?: StellarWalletData;

  additionalData?: WalletAdditionalData;
}

export class WalletRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public async create(options: CreateWalletOptions): Promise<Wallet> {
    const { user, stellar, additionalData } = options;

    const wallet = this.manager.create(Wallet, { user, stellar, additionalData });
    await this.manager.insert(Wallet, wallet);
    return wallet;
  }

  public async getById(walletId: string, relations: string[] = []): Promise<Wallet | undefined> {
    const baseQb = this.manager.createQueryBuilder(Wallet, 'wallet').where('wallet.id = :walletId', { walletId });

    leftJoinAndSelectRelations(baseQb, relations, 'wallet');

    return baseQb.getOne();
  }

  public async appendState(wallet: Wallet, statePartial: StatePartial<WalletStatus>): Promise<void> {
    const { status, additionalData } = statePartial;

    const state = this.manager.create(WalletState, { wallet, status, additionalData });
    await this.manager.insert(WalletState, state);

    wallet.states = wallet.states ?? [];
    wallet.states.unshift(state);
  }
}
