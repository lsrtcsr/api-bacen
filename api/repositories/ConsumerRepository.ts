import {
  ConsumerStatus,
  PaginationOptions,
  DEFAULT_LIMIT,
  DocumentSchema,
  BankingSchema,
  PhoneSchema,
  AddressSchema,
  AccountType,
  CivilStatus,
  CompanyData,
  CompanyDataSchema,
} from '@bacen/base-sdk';
import { getManager, EntityManager } from 'typeorm';
import { arraify } from '@bacen/shared-sdk';
import { DateRange, MaybeArray, StatePartial } from '../utils';
import { User, Document, Banking, Phone, Address, ConsumerState, Consumer } from '../models';

export interface FindConsumerByStatusDateOptions {
  status: ConsumerStatus;
  range?: Partial<DateRange>;
  pagination?: PaginationOptions;
  manager?: EntityManager;
}

export interface CreateConsumerOptions {
  user: User;

  taxId: string;

  type: AccountType;

  birthday?: string | Date;

  motherName?: string;

  birthCity?: string;

  birthState?: string;

  civilStatus?: CivilStatus;

  pep?: boolean;

  financialEquity?: string;

  financialProfit?: string;

  companyData?: CompanyDataSchema;
}

export class ConsumerRepository {
  constructor(private readonly manager: EntityManager = getManager()) {}

  public async create(consumerPartial: CreateConsumerOptions): Promise<Consumer> {
    const consumer = this.manager.create(Consumer, consumerPartial as any);
    await this.manager.insert(Consumer, consumer);

    return consumer;
  }

  public async createConsumerDocuments(consumer: Consumer, documentSchemas: DocumentSchema[]): Promise<void> {
    const documents = this.manager.create(
      Document,
      documentSchemas.map((schema) => ({ ...schema, consumer } as Document)),
    );
    await this.manager.insert(Document, documents);

    consumer.documents = documents;
  }

  public async createConsumerBankings(consumer: Consumer, bankingSchemas: BankingSchema[]): Promise<void> {
    const bankings = this.manager.create(
      Banking,
      bankingSchemas.map((schema) => ({ ...schema, consumer } as Banking)),
    );
    await this.manager.insert(Banking, bankings);

    consumer.bankings = bankings;
  }

  public async createConsumerPhones(consumer: Consumer, phoneSchemas: PhoneSchema[]): Promise<void> {
    const phones = this.manager.create(
      Phone,
      phoneSchemas.map((schema) => ({ ...schema, consumer } as Phone)),
    );

    // set first phone as default if none are marked as so
    if (!phones.some((phone) => phone.default)) {
      phones[0].default = true;
    }

    await this.manager.insert(Phone, phones);
    consumer.phones = phones;
  }

  public async createConsumerAddresses(consumer: Consumer, addressSchemas: AddressSchema[]): Promise<void> {
    const addresses = await Promise.all(
      addressSchemas.map((address) => Address.fromCEP(address.code, { ...address, consumer } as Address)),
    );

    // set first phone as default if none are marked as so
    if (!addresses.some((address) => address.default)) {
      addresses[0].default = true;
    }

    await this.manager.insert(Address, addresses);
    consumer.addresses = addresses;
  }

  public async appendState(consumer: Consumer, statePartial: StatePartial<ConsumerStatus>): Promise<void>;
  public async appendState(consumer: Consumer, statePartials: StatePartial<ConsumerStatus>[]): Promise<void>;
  public async appendState(
    consumer: Consumer,
    statePartialMaybeArray: MaybeArray<StatePartial<ConsumerStatus>>,
  ): Promise<void> {
    const statePartials = arraify(statePartialMaybeArray);

    const consumerState = this.manager.create(
      ConsumerState,
      statePartials.map((partial) => ({ consumer, ...partial })),
    );
    await this.manager.insert(ConsumerState, consumerState);

    // sort states most-recent first
    consumerState.sort((left, right) => right.createdAt.getTime() - left.createdAt.getTime());

    consumer.states = consumer.states ?? [];
    consumer.states.unshift(...consumerState);
  }

  /**
   * Lists all consumers which have entered the given state within the specified time range
   * @param status The consumer status
   * @param range The date range
   */
  public async findConsumerByStatusDate(options: FindConsumerByStatusDateOptions): Promise<[User[], number]> {
    const { status, range = {}, pagination: { skip = 0, limit = DEFAULT_LIMIT } = {} } = options;
    const { start, end } = range;

    const baseQuery = this.manager
      .createQueryBuilder(User, 'user')
      .innerJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.states', 'user_states')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumer_states')
      .innerJoin(
        'consumer.states',
        'target_state',
        'target_state."consumerId" = consumer.id AND target_state.status = :status',
        { status },
      );

    if (start && end) {
      baseQuery.where('target_state.created_at BETWEEN :start AND :end', { start, end });
    } else if (start && !end) {
      baseQuery.where('target_state.created_at >= :start', { start });
    } else if (!start && end) {
      baseQuery.where('target_state.created_at <= :end', { end });
    }

    return baseQuery.skip(skip).take(limit).getManyAndCount();
  }
}
