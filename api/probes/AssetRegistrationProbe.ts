import { AssetRegistration, AssetRegistrationStatus } from '@bacen/base-sdk';
import { AnalysisProvider } from '@bacen/identity-sdk';
import { BaseProbe } from './base';

export class AssetRegistrationProbe extends BaseProbe {
  public logAssetRegistrationNotFound(identifier: object): void {
    this.logger.info(`[${this.serviceName}] Asset registration not found`, identifier);
  }

  public logCustomAssetRegistered(assetRegistration: AssetRegistration): void {
    this.logger.info(
      `[${this.serviceName}] Asset does not have a registered identity-provider and will skip directly to approved`,
      {
        asset: assetRegistration.asset?.code,
        walletId: assetRegistration.wallet?.id,
      },
    );
  }

  public logRegistrationSkippedDueToWalletNotRegistered(assetRegistration: AssetRegistration): void {
    this.logger.warn(`[${this.serviceName}] Wallet is not registered yet, skipping`, {
      asset: assetRegistration.asset.code,
      walletId: assetRegistration.wallet.id,
    });
  }

  public logTransitionFailedSilently(
    assetRegistrationId: string,
    currentState: AssetRegistrationStatus,
    nextState: AssetRegistrationStatus,
  ): void {
    this.logger.warn(`[${this.serviceName}] AssetRegistrationStateMachine transition failed silently`, {
      assetRegistrationId,
      currentState,
      nextState,
    });
  }

  public logSuccessfullStateTransition(
    assetRegistrationId: string,
    previousState: AssetRegistrationStatus,
    currentState: AssetRegistrationStatus,
  ): void {
    this.logger.info(`[${this.serviceName}] AssetRegistrationStateMachine successfully transitioned`, {
      assetRegistrationId,
      previousState,
      currentState,
    });
  }

  public logTransitionFailed(assetRegistrationId: string, exception: any) {
    this.logger.error(`[${this.serviceName}] AssetRegistrationStateMachine transition failed`, {
      assetRegistrationId,
      exception,
    });
  }

  public logReportCreation(
    assetRegistration: AssetRegistration,
    analysisProvider: AnalysisProvider,
    reportId: string,
  ): void {
    this.logger.info(`[${this.serviceName}] Successfully created report for asset registration`, {
      asset: assetRegistration.asset.code,
      analysisProvider,
      reportId,
    });
  }

  public logReportAlreadyExists(
    assetRegistration: AssetRegistration,
    analysisProvider: AnalysisProvider,
    reportId: string,
  ): void {
    this.logger.info(
      `[${this.serviceName}] Found a report with matching taxId, analysis-type and domainId for this provider`,
      {
        asset: assetRegistration.asset.code,
        analysisProvider,
        reportId,
      },
    );
  }

  public logReportRejected(
    assetRegistration: AssetRegistration,
    analysisProvider: AnalysisProvider,
    rejectionReason: any,
  ): void {
    this.logger.debug(`[${this.serviceName}] Report was rejected by provider`, {
      asset: assetRegistration.asset.code,
      analysisProvider,
      rejectionReason,
    });
  }

  public logReportCreationFailed(
    assetRegistration: AssetRegistration,
    analysisProvider: AnalysisProvider,
    exception: any,
  ): void {
    this.logger.error(`[${this.serviceName}] Report creation failed`, {
      asset: assetRegistration.asset.code,
      analysisProvider,
      exception,
    });
  }

  public logEventIgnored(additionalInfo: object): void {
    this.logger.info(`[${this.serviceName}] No action available for event`, additionalInfo);
  }

  public logNewEvent(event: any): void {
    this.logger.silly(`[${this.serviceName}] Got new event:`, event);
  }
}
