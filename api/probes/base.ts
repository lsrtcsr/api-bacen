import { LoggerInstance } from 'nano-errors';

export abstract class BaseProbe {
  constructor(protected readonly serviceName: string, protected readonly logger: LoggerInstance) {}
}
