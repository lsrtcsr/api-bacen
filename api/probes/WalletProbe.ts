import { AssetRegistration } from '@bacen/base-sdk';
import { BaseProbe } from './base';

export class WalletProbe extends BaseProbe {
  public logWalletNotFound(walletId: string): void {
    this.logger.error(`[${this.serviceName}] Wallet not found`, {
      walletId,
    });
  }

  public logRegistrationInStellarFailed(walletId: string): void {
    this.logger.error(`[${this.serviceName}] Registration in stellar failed, transitioning wallet to failed!`, {
      walletId,
    });
  }

  public logTransitionToFailedException(walletId: string, error: any): void {
    this.logger.error(`[${this.serviceName}] An error occurred when trying to move the wallet to failed, nacking`, {
      walletId,
      error,
    });
  }

  public logSuccessfullyRegisteredWalletOnStellar(walletId: string, publicKey: string): void {
    this.logger.info(`[${this.serviceName}] Successfully registered wallet on stellar`, {
      walletId,
      publicKey,
    });
  }

  public logApprovedAssetRegistrationsPublished(assetRegistrations: AssetRegistration[]): void {
    this.logger.info(
      `[${this.serviceName}] Wallet has pending approved assetRegistrations, a new event will be published for each.`,
      assetRegistrations.map((ar) => ({
        asset: ar.asset.code,
      })),
    );
  }

  public logSuccessfullyRegisteredRootMediatorAssets(assetRegistrations: AssetRegistration[]): void {
    this.logger.info(
      `[${this.serviceName}] Successfully registered rootMediator assets.`,
      assetRegistrations.map((ar) => ({
        asset: ar.asset.code,
      })),
    );
  }

  public logConsumerError(error: any) {
    this.logger.error(`[${this.serviceName}] An error occurred while processing an event. Message will be nacked`, {
      error,
    });
  }
}
