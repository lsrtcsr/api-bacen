import { AccountType, DocumentSide, DocumentStatus, UnleashFlags, UserRole } from '@bacen/base-sdk';
import { ImageService } from '@bacen/image-tools';
import { UnleashUtil } from '@bacen/shared-sdk';
import * as FileType from 'file-type';
import { Readable } from 'stream';
import { Service, ServiceOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import {
  CORPORATE_DOCUMENT_CONTENT_TYPE_WHITELIST,
  PERSONAL_DOCUMENT_CONTENT_TYPE_WHITELIST,
} from '../../config/kyc.config';
import { logger } from '../../config/logger.config';
import { DocumentStateMachine } from '../fsm';
import { Document, DocumentMetadata, User } from '../models';
import { ConsumerPipelinePublisher, MediatorPipelinePublisher } from './pipeline';
import StorageService from './storageservice';

export interface DocumentServiceOptions extends ServiceOptions {}

export interface DocumentUploadRequest {
  buffer: Buffer;
  type: DocumentType | string;
  side: DocumentSide | string;
  taxId: string;
  metadata: DocumentMetadata;
  user: User;
  reason: string;
}

export class DocumentService extends Service {
  public options: DocumentServiceOptions;

  public domainSettingToLimitTypeMap;

  protected static instance: DocumentService;

  constructor(options: DocumentServiceOptions = {}) {
    super(options);
  }

  static initialize(options: DocumentServiceOptions) {
    this.instance = new DocumentService(options);
  }

  static getInstance() {
    return this.instance;
  }

  public static cleanBase64ImageHeader(image: string): string {
    return image
      .split('dataimage/pngbase64')
      .join()
      .split('data:image/jpeg;base64,')
      .join()
      .split('data:image/png;base64,')
      .join()
      .replace(/={1,2}$/, '');
  }

  public static cleanBase64ImageFooter(image: string): string {
    const lastThreeDigits = image.slice(image.length - 3);
    const lastDigitsWithoutBar = lastThreeDigits.replace(/\/{1,2}/, '');
    return image.substring(0, image.length - 3).concat(lastDigitsWithoutBar);
  }

  /**
   * Returns the analysis from the file type.
   *
   * @param fileBuffer The file buffer to check
   * @param accountType The account type to check
   * @param context The unleash context flags
   */
  public static async isValidContentType(
    fileBuffer: Buffer,
    accountType: AccountType,
    context?: any,
  ): Promise<{ isValid: boolean; mime?: FileType.MimeType; allowedContentTypes?: FileType.MimeType[] }> {
    // Gets content type from file buffer
    const contentType = await DocumentService.getContentType(fileBuffer);
    const allowedContentTypes = this.allowedContentTypes(accountType, context);

    if (!contentType || !contentType.mime) {
      return { isValid: false, allowedContentTypes };
    }

    if (!allowedContentTypes.includes(contentType.mime)) {
      return { isValid: false, mime: contentType.mime, allowedContentTypes };
    }

    return { isValid: true, mime: contentType.mime, allowedContentTypes };
  }

  /**
   * Returns allowed content type for this document based on the account type and unleash context.
   *
   * @param accountType The account type
   * @param context The unleash context for feature flags
   */
  public static allowedContentTypes(accountType: AccountType, context?: any): FileType.MimeType[] {
    let allowedContentTypes: FileType.MimeType[];

    if (accountType === AccountType.PERSONAL) {
      allowedContentTypes = UnleashUtil.isEnabled(UnleashFlags.DISABLE_PERSONAL_PDF_UPLOAD, context, false)
        ? PERSONAL_DOCUMENT_CONTENT_TYPE_WHITELIST
        : [...PERSONAL_DOCUMENT_CONTENT_TYPE_WHITELIST, 'application/pdf'];
    } else {
      allowedContentTypes = CORPORATE_DOCUMENT_CONTENT_TYPE_WHITELIST;
    }

    return allowedContentTypes;
  }

  /**
   * Fetches document file metadata.
   *
   * @param file The image file
   */
  public static async fetchMetadata(file: Buffer | Readable): Promise<DocumentMetadata[DocumentSide]> {
    let metadata: any;
    const allowMetadataFailure = UnleashUtil.isEnabled(UnleashFlags.ALLOW_DOCUMENT_IMAGE_METADATA_FAILURE, {}, false);

    try {
      const service = await (file instanceof Buffer ? ImageService.fromBuffer(file) : ImageService.fromStream(file));
      metadata = await service.identify();
      // TODO: Save image report into database
      console.log(metadata);
    } catch (exception) {
      if (allowMetadataFailure) {
        logger.debug('Could not check document format using ImageMagick', exception);
        metadata = exception?.response?.data || exception?.data;
      } else {
        throw exception;
      }
    }

    return metadata;
  }

  public static async upload({ reason, ...data }: DocumentUploadRequest, manager = getManager()) {
    const type = ((data.type as any) || '').toLowerCase();

    // TODO: Re-enable jpeg conversion
    // TODO: Check image content-type before converting
    // manipuledPicture = await DocumentService.toJPEG(picture);
    const document = await manager.transaction(async (manager) => {
      let document: Document;
      const lastActiveDocument = await Document.findLastActive(data.user, data.taxId, type, manager);

      // if there is no other document with same taxId
      if (!lastActiveDocument) {
        document = await Document.insertAndFind(
          {
            number: data.taxId,
            type,
            reason,
            consumer: data.user?.consumer,
            isActive: true,
            metadata: { [data.side]: data.metadata },
          },
          { manager },
        );
      } else {
        // same taxId and diferente types
        if (lastActiveDocument.type !== data.type) {
          document = await Document.insertAndFind(
            {
              number: data.taxId,
              type,
              reason,
              consumer: data.user?.consumer,
              isActive: true,
              metadata: { [data.side]: data.metadata },
            },
            { manager },
          );
          // same taxId and same type
        } else {
          const documentSides = await lastActiveDocument.existingSides();

          // if there is a side inserted, create a new document
          // otherwise do nothing
          if (documentSides.includes(data.side as DocumentSide)) {
            document = await Document.insertAndFind(
              {
                number: data.taxId,
                type,
                reason,
                consumer: data.user?.consumer,
                isActive: true,
                metadata: { [data.side]: data.metadata },
              },
              { manager },
            );
            await Document.updateAndFind(lastActiveDocument.id, { isActive: false }, { manager });
          } else {
            document = lastActiveDocument;

            // Updates document metadata
            await document.setMetadata({ ...lastActiveDocument.metadata, [data.side]: data.metadata }, manager);
          }
        }
      }

      const storageService = StorageService.getInstance();
      await storageService.uploadFile({
        fileName: document.getImageFileName(data.side),
        data: data.buffer.toString('base64'),
        encoding: 'base64',
      });

      return document;
    });

    if (await document.requiredSidesExist()) {
      // Mark the document as ready to be verified
      const fsm = new DocumentStateMachine(document);
      await fsm.goTo(DocumentStatus.PROCESSING);
    }

    const isReadyToAdvance = await data?.user?.consumer?.requiredDocumentsProvided();

    if (isReadyToAdvance && data?.user?.role === UserRole.MEDIATOR) {
      // Send consumer to remote async pipeline for verification
      await MediatorPipelinePublisher.getInstance().send(data.user);
    } else if (isReadyToAdvance) {
      // Send consumer to remote async pipeline for verification
      await ConsumerPipelinePublisher.getInstance().send(data.user);
    }

    return document;
  }

  public static getContentType(buffer: Buffer): Promise<FileType.FileTypeResult | undefined>;

  public static getContentType(string: string, encoding?: string): Promise<FileType.FileTypeResult | undefined>;

  /**
   * Gets content type from file.
   *
   * @param bufferOrString The buffer or base64 file string
   * @param encoding The string encoding, defaults to base64.
   */
  public static async getContentType(
    bufferOrString: Buffer | string,
    encoding: BufferEncoding = 'base64',
  ): Promise<FileType.FileTypeResult | undefined> {
    const buffer = Buffer.isBuffer(bufferOrString) ? bufferOrString : Buffer.from(bufferOrString, encoding);
    return FileType.fromBuffer(buffer);
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
