import { CardStatus } from '@bacen/base-sdk';
import { BaseError, Service, ServiceOptions } from 'ts-framework-common';
import { getManager, In } from 'typeorm';
import { Card, DomainSettingType, User, Asset, Domain, Wallet } from '../models';
import { LimitType, UserLimit, UserLimitSchema } from '../models/settings/UserLimit';
import { UserMetricsService } from './metrics';
import { Trace } from '../utils';

export interface GetValueConsumedOptions {
  type: LimitType;
  wallet?: Wallet;
  user?: User;
  asset?: Asset;
}

export interface LimitSettingServiceOptions extends ServiceOptions {}

export class LimitSettingService extends Service {
  public options: LimitSettingServiceOptions;

  public domainSettingToLimitTypeMap;

  protected static instance: LimitSettingService;

  constructor(options: LimitSettingServiceOptions = {}) {
    super(options);

    this.domainSettingToLimitTypeMap = new Map<DomainSettingType, LimitType>();
    this.domainSettingToLimitTypeMap.set(DomainSettingType.TOTAL_ACTIVE_CARDS, LimitType.NUM_OF_ACTIVE_CARDS);
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_MOBILE_CREDITS_DAILY,
      LimitType.MOBILE_CREDIT_SUM_CURRENT_DAY,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_SENT_TRANSACTIONS_VALUE_DAILY,
      LimitType.TRANSACTION_SUM_CURRENT_DAY,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_SENT_TRANSACTIONS_VALUE_WEEKLY,
      LimitType.TRANSACTION_SUM_CURRENT_WEEK,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_SENT_TRANSACTIONS_VALUE_MONTHLY,
      LimitType.TRANSACTION_SUM_CURRENT_MONTH,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_RECEIVED_TRANSACTIONS_VALUE_DAILY,
      LimitType.RECEIVED_CURRENT_DAY,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_RECEIVED_TRANSACTIONS_VALUE_WEEKLY,
      LimitType.RECEIVED_CURRENT_WEEK,
    );
    this.domainSettingToLimitTypeMap.set(
      DomainSettingType.TOTAL_RECEIVED_TRANSACTIONS_VALUE_MONTHLY,
      LimitType.RECEIVED_CURRENT_MONTH,
    );
  }

  static initialize(options: LimitSettingServiceOptions) {
    this.instance = new LimitSettingService(options);
  }

  static getInstance() {
    return this.instance;
  }

  @Trace()
  public async getLimits(type: LimitType, user: User, asset?: string, domain?: Domain): Promise<number> {
    /*
     * Fetching both the userLimit and the domainDefaultLimit is an optimization relying on the assumption that most
     * users will not have limits associated with them, so we'll end up fetching the domainDefaultLimit anyway. This
     * may change in the future, so feel free to challenge it.
     */
    const [userLimit, domainDefaultLimit] = await Promise.all([
      UserLimit.safeFindOne({
        where: { type, user: { id: user.id }, asset, current: true },
      }),
      // Fast-tracks getDomainDefaultLimits if we already have it available by avoiding querying for the user's domain.
      this.getDomainDefaultLimit(type, user.id, domain),
    ]);

    return userLimit ? userLimit.value : domainDefaultLimit;
  }

  public async hasLimit(type: LimitType, user: User): Promise<boolean> {
    const limit = await this.getLimits(type, user);

    const consumed = await this.getValueConsumed({ type, user });

    return consumed < limit;
  }

  public async getDomainDefaultLimit(type: LimitType, userId: string, _domain?: Domain): Promise<number> {
    const domain =
      _domain ||
      (await Domain.createQueryBuilder('domain')
        .innerJoin('domain.users', 'user')
        .where('user.id = :userId', { userId })
        .andWhere('user.deletedAt IS NULL')
        .andWhere('domain.deletedAt IS NULL')
        .getOne());

    /* think in a better way to do this */
    let domainSettingType;
    switch (type) {
      case LimitType.NUM_OF_ACTIVE_CARDS:
        domainSettingType = DomainSettingType.TOTAL_ACTIVE_CARDS;
        break;
      case LimitType.TRANSACTION_SUM_CURRENT_DAY:
        domainSettingType = DomainSettingType.TOTAL_SENT_TRANSACTIONS_VALUE_DAILY;
        break;
      case LimitType.TRANSACTION_SUM_CURRENT_MONTH:
        domainSettingType = DomainSettingType.TOTAL_SENT_TRANSACTIONS_VALUE_MONTHLY;
        break;
      case LimitType.MOBILE_CREDIT_SUM_CURRENT_DAY:
        domainSettingType = DomainSettingType.TOTAL_MOBILE_CREDITS_DAILY;
        break;
      default:
        throw new BaseError('Setting type not supported yet.');
    }

    // ? WHY???????????????/
    return Reflect.get(domain.settings.locks, domainSettingType);
  }

  /**
   * Fetches from the DB the amount consumed from a given limit by a user
   * @param type the limit type
   * @param user the user instance (invariant: must have wallets relation populated)
   * @param asset the asset instance
   */
  @Trace()
  public async getValueConsumed(opts: GetValueConsumedOptions): Promise<number> {
    const { type, wallet, asset, user } = opts;

    if (!(wallet || user)) {
      throw new BaseError('Either user or wallet must be passed to getValueConsumed', { args: opts });
    }

    const operationAsset = asset || (await Asset.getRootAsset());

    switch (type) {
      case LimitType.NUM_OF_ACTIVE_CARDS: {
        const wallets = user.wallets
          ? user.wallets
          : await Wallet.safeFind({
              where: { user },
              select: ['id'],
            });

        return Card.safeCount({
          where: {
            status: In([CardStatus.AVAILABLE, CardStatus.BLOCKED]),
            wallet: { id: In(wallets.map((wallet) => wallet.id)) },
          },
        });
      }
      case LimitType.TRANSACTION_SUM_CURRENT_DAY:
        // TODO: Shouldn't the first arg be the walletId ?
        return UserMetricsService.getInstance().getPaymentAmountForUserAndAssetToday(wallet.id, operationAsset);
      case LimitType.TRANSACTION_SUM_CURRENT_MONTH:
        // TODO: Shouldn't the first arg be the walletId ?
        return UserMetricsService.getInstance().getPaymentAmountForUserAndAssetThisMonth(wallet.id, operationAsset);
      case LimitType.MOBILE_CREDIT_SUM_CURRENT_DAY:
        // TODO: Shouldn't the first arg be the walletId ?
        return UserMetricsService.getInstance().getMobileCreditOrderAmountForWalletToday(wallet.id, operationAsset);
      case LimitType.NUM_OF_BOLETOS_ISSUED_PER_DAY:
      case LimitType.SUM_OF_BOLETOS_ISSUED_CURRENT_DAY:
      case LimitType.SUM_OF_BOLETOS_ISSUED_LAST_24HOURS:
        throw new BaseError(`${type} limit control not implemented!`);
    }
  }

  public async update(newLimit: UserLimitSchema): Promise<UserLimit> {
    let newLimitId: string;
    await getManager().transaction(async (transaction) => {
      const current = await transaction.findOne(UserLimit, {
        where: {
          type: newLimit.type,
          user: { id: newLimit.user.id },
          current: true,
        },
      });

      if (current) {
        await transaction.update<UserLimit>(UserLimit, current.id, {
          current: false,
          validUntil: new Date(),
        });
      }

      const insertResult = await transaction.insert(UserLimit, UserLimit.create(newLimit));
      newLimitId = insertResult.identifiers[0].id;
    });

    return UserLimit.findOne({ where: { id: newLimitId } });
  }

  public async create(newLimit: UserLimitSchema): Promise<UserLimit> {
    let newLimitId: string;
    await getManager().transaction(async (transaction) => {
      const insertResult = await transaction.insert(UserLimit, UserLimit.create(newLimit));
      newLimitId = insertResult.identifiers[0].id;
    });

    return UserLimit.findOne({ where: { id: newLimitId } });
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
