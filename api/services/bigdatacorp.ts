import { BigBoostService, BigIdService } from '@bacen/bigdatacorp-service';

export { DocumentType as BigDocumentType, TaxIdStatus, ResultCodes } from '@bacen/bigdatacorp-service';

export const BigBoost = BigBoostService;
export const BigId = BigIdService;
