import * as sinon from 'sinon';
import * as faker from 'faker';
import * as cpf from 'cpf';
import { AnalysisProvider } from '@bacen/identity-sdk';
import { UserRole, AccountType, AssetRegistrationStatus } from '@bacen/base-sdk';
import { AssetRegistrationService } from './AssetRegistrationService';
import { AssetRegistrationRepository } from '../repositories';

describe('api.services.AssetRegistrationService', () => {
  describe('create', () => {
    let appendStateStub: sinon.SinonStub;
    let insertStub: sinon.SinonStub;

    let service: AssetRegistrationService;

    const wallet = {
      id: faker.random.uuid(),
      user: {
        id: faker.random.uuid(),
        email: faker.internet.email(),
        role: UserRole.CONSUMER,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        consumer: {
          taxId: cpf.generate(),
          type: AccountType.PERSONAL,
        },
        domain: {
          id: faker.random.uuid(),
        }
      },
      assetRegistrations: [],
    };
    const asset = {
      id: faker.random.uuid(),
      code: faker.finance.currencyCode(),
    };

    beforeAll(() => {
      insertStub = sinon.stub(AssetRegistrationRepository.prototype, 'create');
      appendStateStub = sinon.stub(AssetRegistrationRepository.prototype, 'appendState');

      service = new AssetRegistrationService(
        {} as any,
        {} as any,
      );
    });

    afterEach(() => {
      insertStub.resetBehavior();

      insertStub.resetHistory();
      appendStateStub.resetHistory();
    });

    afterAll(() => {
      insertStub.restore();
      appendStateStub.restore();
    });

    it('creates a report on state pending_registration', async () => {
      const id = faker.random.uuid();
      insertStub.callsFake((asset, wallet) => ({ id, asset, wallet }));

      const assetRegistration = await service.create(wallet as any, asset as any, {} as any);

      expect(assetRegistration.id).toBe(id);
      sinon.assert.calledOnce(insertStub);
      sinon.assert.calledWithMatch(appendStateStub, sinon.match.any, sinon.match({ status: AssetRegistrationStatus.PENDING_REGISTRATION }));
    });
  });

  describe('chooseAssetIdentity', () => {
    let listProvidersStub: sinon.SinonStub;
    let service: AssetRegistrationService;

    beforeAll(() => {
      listProvidersStub = sinon.stub();
      service = new AssetRegistrationService({} as any, {
        providers: () => ({ list: listProvidersStub })
      } as any);
    });

    afterEach(() => {
      listProvidersStub.resetBehavior();
      listProvidersStub.resetHistory();
    });

    it('returns undefined if the assetIdentities list is empty', async () => {
      const asset = { code: faker.random.word() };
      const domainId = faker.random.uuid();
      listProvidersStub.resolves([]);

      const assetIdentity = await service.chooseAssetIdentity(asset as any, domainId);

      expect(assetIdentity).toBeUndefined();
    });

    it('returns undefined if there are no assetIdentities matching the provided domainId and there is no assetIdentity with null domainId', async () => {
      const asset = { code: faker.finance.currencyCode() };
      const domainId = faker.random.uuid();

      const assetIdentityCount = faker.random.number({ min: 2, max: 10 });
      const assetIdentities = Array.from({ length: assetIdentityCount}, _ => ({
        asset: asset.code,
        domainId: faker.random.uuid(),
        analysisProvider: faker.random.arrayElement(Object.values(AnalysisProvider))
      }));
      listProvidersStub.resolves(assetIdentities)

      const assetIdentity = await service.chooseAssetIdentity(asset as any, domainId);

      expect(assetIdentity).toBeUndefined();
    });

    it('returns the assetIdentity with matching asset and domainId', async () => {
      const asset = { code: faker.finance.currencyCode() };

      const assetIdentityCount = faker.random.number({ min: 2, max: 10 });
      const assetIdentities = Array.from({ length: assetIdentityCount}, _ => ({
        asset: asset.code,
        domainId: faker.random.uuid(),
        analysisProvider: faker.random.arrayElement(Object.values(AnalysisProvider))
      }));
      listProvidersStub.resolves(assetIdentities)

      const domainId = faker.random.arrayElement(assetIdentities).domainId;

      const assetIdentity = await service.chooseAssetIdentity(asset as any, domainId);

      expect(assetIdentity).toBeDefined();
      expect(assetIdentity).toMatchObject({ asset: asset.code, domainId });
    });

    it('returns the assetIdentity with matching asset and null domainId if there are no assetIdentities matching the domainId', async () => {
      const asset = { code: faker.finance.currencyCode() };
      const domainId = faker.random.uuid();

      const assetIdentityCount = faker.random.number({ min: 2, max: 10 });
      const assetIdentities = Array.from({ length: assetIdentityCount}, _ => ({
        asset: asset.code,
        domainId: faker.random.uuid(),
        analysisProvider: faker.random.arrayElement(Object.values(AnalysisProvider))
      }));
      listProvidersStub.resolves(assetIdentities)

      const nullDomainIdx = faker.random.number(assetIdentityCount - 1);
      assetIdentities[nullDomainIdx].domainId = null;


      const assetIdentity = await service.chooseAssetIdentity(asset as any, domainId);

      expect(assetIdentity).toBeDefined();
      expect(assetIdentity).toMatchObject({ asset: asset.code, domainId: null });
    });
  });
});
