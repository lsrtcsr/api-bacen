import { PaymentType, CustodyFeature, TransactionStatus, PaymentStatus, TransactionType } from '@bacen/base-sdk';
import { BaseError, LoggerInstance, Logger } from 'nano-errors';
import { getConnection, getManager, EntityManager } from 'typeorm';
import { Horizon } from 'stellar-sdk';
import { HttpError, HttpCode } from 'ts-framework';
import { StellarService } from '@bacen/stellar-service';
import { TransactionPipelinePublisher } from '../pipeline';
import { Asset, Wallet, PaymentLog, Transaction, TransactionStateItem, Payment } from '../../models';
import { asyncPipe, lift2, pipe, ProviderUtil } from '../../utils';
import { ProviderManagerService, AuthorizerQueryService } from '..';

export interface BoletoConsolidationServiceOptions {}

export interface BalanceMap<T extends number | string = number> {
  [id: string]: T;
}

export interface PipelineData {
  wallet: Wallet;
  sourceWallet: Wallet;
  sourceAsset: Asset;
  targetAsset: Asset;
  balance: BalanceMap<string>;
  stellarBalance?: string;
  additionalData?: object;
  transaction?: Transaction;
}

export interface ChargeBoletoPaymentOptions {
  paymentSchema: Partial<Payment>;
  transactionSchema: Partial<Transaction>;
  manager?: EntityManager;
}

// This class is written in a style which is quite exotic compared to the rest of the codebase.
// It started out as a simple service class with two complex methods which were very painfull to test due to:
//
// - many external dependencies (StellarService, EntityManager, Repositories, TransactionPipelinePublisher, LoggerInstance)
// - plenty of side-effects (reading from stellar, reading from DB, writing to DB, publishing to queue, logging, ...)
// - a lot of assumptions about the system (there is a rootWallet, there are wallets with positive balance of a given asset, ...)
//
// So it was rewritten to a more functional style in hopes of making it more testable by dividing
// the two methods in many smaller functions which could be more easily unit-tested (this was quite successfull).
//
// There are many small methods which try their best to do one thing and one thing only, some of these functions take their dependencies as
// parameters and return a unary function which operates solely on data. There are also other functions (eg. getWalletsWithExecutedBalance,
// consolidateWalletsBallance and liquidateBalanceFromWallet) which don't have much logic, they merely compose other functions into a pipeline
// if we assume that our pipeline function/ composition is well tested or trustable we can skip on testing those.
export class BoletoConsolidationService {
  private static instance?: BoletoConsolidationService;

  constructor(public options: BoletoConsolidationServiceOptions = {}) {}

  public static initialize(opts?: BoletoConsolidationServiceOptions): BoletoConsolidationService {
    return (this.instance = new BoletoConsolidationService(opts));
  }

  public static getInstance(): BoletoConsolidationService {
    if (!this.instance) throw new BaseError('BoletoConsolidationService is not initialized yet.');

    return this.instance;
  }

  public async getWalletsWithExecutedBalance(assetCode: string, sourceWalletId?: string): Promise<BalanceMap> {
    const { id: issuerWalletId } = await this.findSourceWallet(sourceWalletId);

    return asyncPipe(
      this.findAssetByCode,
      this.getAssetPaymentLog,
      lift2(
        this.buildWalletBalancesFromCashInsAndCashOuts,
        this.getCashInsFromPaymentLog(issuerWalletId),
        this.getCashOutsFromPaymentLog(issuerWalletId),
      ),
      this.removeZeroBalances,
    )(assetCode);
  }

  public async findSourceWallet(sourceWalletId?: string): Promise<Wallet> {
    const issuerWallet = sourceWalletId ? await Wallet.findOne(sourceWalletId) : await Wallet.getRootWallet();

    if (!issuerWallet)
      throw new HttpError(`Source wallet with id ${sourceWalletId} not found`, HttpCode.Client.NOT_FOUND);

    return issuerWallet;
  }

  public async findAssetByCode(assetCode: string): Promise<Asset> {
    const asset = await Asset.getByCode(assetCode);
    if (!asset) throw new HttpError(`Asset with code ${assetCode} not found`, HttpCode.Client.NOT_FOUND);

    return asset;
  }

  public getAssetPaymentLog(asset: Asset): Promise<PaymentLog[]> {
    return getConnection('timescale')
      .getRepository(PaymentLog)
      .find({ where: { asset: asset.code } });
  }

  public getCashInsFromPaymentLog(
    issuerWalletId: string,
  ): (paymentLog: PaymentLog[]) => { id: string; amount: number }[] {
    return (paymentLog: PaymentLog[]) =>
      paymentLog
        .filter(payment => payment.source === issuerWalletId)
        .map(payment => ({
          id: payment.destination,
          amount: parseFloat(payment.amount),
        }));
  }

  public getCashOutsFromPaymentLog(
    issuerWalletId: string,
  ): (paymentLog: PaymentLog[]) => { id: string; amount: number }[] {
    return (paymentLog: PaymentLog[]) =>
      paymentLog
        .filter(payment => payment.destination === issuerWalletId)
        .map(payment => ({
          id: payment.destination,
          amount: -parseFloat(payment.amount),
        }));
  }

  public buildWalletBalancesFromCashInsAndCashOuts(
    cashIns: { id: string; amount: number }[],
    cashOuts: { id: string; amount: number }[],
  ): BalanceMap {
    const walletBalances: BalanceMap = {};

    cashIns.concat(cashOuts).forEach(payment => {
      walletBalances[payment.id] === undefined
        ? (walletBalances[payment.id] = payment.amount)
        : (walletBalances[payment.id] += payment.amount);
    });

    return walletBalances;
  }

  public removeZeroBalances(balances: BalanceMap): BalanceMap {
    return Object.fromEntries(Object.entries(balances).filter(([_, balance]) => balance !== 0));
  }

  public async consolidateWalletsBallance(
    balances: BalanceMap,
    sourceAssetCode: string,
    targetAssetCode: string = 'root',
    sourceWalletId?: string,
    manager = getManager(),
    stellarService = StellarService.getInstance(),
    publisher = TransactionPipelinePublisher.getInstance(),
    logger = Logger.getInstance(),
  ): Promise<BalanceMap<string>> {
    const [sourceAsset, targetAsset, wallets, sourceWallet] = await Promise.all([
      this.findAssetByCode(sourceAssetCode),
      this.findAssetByCode(targetAssetCode),
      this.findWalletsFromBalances(balances),
      this.findSourceWallet(sourceWalletId),
    ]);

    const internalBalances: BalanceMap<string> = Object.fromEntries(
      Object.entries(balances).map(([id, balance]) => [id, balance.toFixed(7)]),
    );
    const pipelineData = { balance: internalBalances, sourceAsset, targetAsset, sourceWallet };

    let returnedBalances: BalanceMap<string> = {};
    for (const wallet of wallets) {
      ({ balance: returnedBalances } = await this.liquidateBalanceFromWallet(
        stellarService,
        manager,
        publisher,
        logger,
      )({ ...pipelineData, wallet }));
    }

    return returnedBalances;
  }

  public async findWalletsFromBalances(balances: BalanceMap) {
    const wallets = (await pipe<BalanceMap, {}, Promise<Wallet[]>>(
      Object.keys,
      Wallet.findByIds,
    )(balances)) as Wallet[];

    if (!wallets.length) throw new HttpError('No wallets were found with the given ids', HttpCode.Client.NOT_FOUND);
    return wallets;
  }

  public liquidateBalanceFromWallet(
    stellarService: StellarService,
    manager: EntityManager,
    publisher: TransactionPipelinePublisher,
    logger: LoggerInstance,
  ) {
    return (data: PipelineData) =>
      asyncPipe<PipelineData>(
        this.loadAccounBalanceFromStellar(stellarService),
        this.assertBalanceIntegrity(logger),
        this.prepareTransaction(manager),
        this.destroyAsset(manager),
        this.publishTransaction(publisher),
      )(data);
  }

  public loadAccounBalanceFromStellar(stellarService: StellarService) {
    return async ({ sourceAsset, wallet, ...rest }: PipelineData) => {
      const accountInfo = await stellarService.loadAccount(wallet.stellar.publicKey);
      const { balance } = accountInfo.balances.find(
        (balance: Horizon.BalanceLineAsset) => balance.asset_code === sourceAsset.code,
      );

      return { ...rest, sourceAsset, wallet, stellarBalance: balance };
    };
  }

  public assertBalanceIntegrity(logger: LoggerInstance) {
    return ({ balance, stellarBalance, wallet, sourceAsset, ...rest }: PipelineData): PipelineData => {
      let newBalance: BalanceMap<string>;

      if (parseFloat(balance[wallet.id]) !== parseFloat(stellarBalance)) {
        logger.warn(`
balance mismatch between DB and stellar for wallet ${wallet.id} and asset ${sourceAsset.code}.\n
\n
Our balance: ${balance[wallet.id]}\n
stellarBalance: ${stellarBalance}\n
`);
        newBalance = { ...balance, [wallet.id]: stellarBalance };
      } else {
        newBalance = { ...balance };
      }

      return { balance: newBalance, stellarBalance, wallet, sourceAsset, ...rest };
    };
  }

  public prepareTransaction(manager: EntityManager) {
    return async ({ wallet, targetAsset, additionalData, stellarBalance, sourceWallet, ...rest }: PipelineData) => {
      const transaction = await Transaction.prepare({
        manager,
        additionalData,
        recipients: [
          {
            wallet,
            asset: targetAsset,
            amount: stellarBalance,
          },
        ],
        source: sourceWallet,
        type: PaymentType.TRANSFER,
      });

      return { ...rest, transaction, wallet, targetAsset, additionalData, stellarBalance, sourceWallet };
    };
  }

  public destroyAsset(manager: EntityManager) {
    return async ({
      sourceAsset,
      wallet,
      stellarBalance,
      additionalData,
      ...rest
    }: PipelineData): Promise<PipelineData> => {
      await sourceAsset.destroy({
        manager,
        additionalData,
        targetWallet: wallet,
        amount: stellarBalance,
      });

      return { ...rest, sourceAsset, wallet, stellarBalance, additionalData };
    };
  }

  public publishTransaction(publisher: TransactionPipelinePublisher) {
    return async ({ transaction, ...rest }: PipelineData): Promise<PipelineData> => {
      await publisher.send(transaction);

      return { ...rest, transaction };
    };
  }

  public async chargeBoletoPayment(options: ChargeBoletoPaymentOptions): Promise<Transaction> {
    const { paymentSchema, transactionSchema, manager } = options;

    const authorizerQueryService = AuthorizerQueryService.getInstance();
    const provider = ProviderManagerService.getInstance();
    const p2p = provider.from(paymentSchema.asset.provider).feature(CustodyFeature.PAYMENT);
    await ProviderUtil.throwIfFeatureNotAvailable(p2p);

    const transactionInsertResult = await manager.insert(Transaction, transactionSchema);
    const transactionId = transactionInsertResult.identifiers[0].id;

    const initialState = TransactionStateItem.create({
      transaction: { id: transactionId },
      status: TransactionStatus.AUTHORIZED,
    });

    await manager.insert(TransactionStateItem, initialState);

    const payment = await Payment.insertAndFind(
      {
        ...paymentSchema,
        status: PaymentStatus.AUTHORIZED,
        transaction: { id: transactionId },
      },
      { manager, findOptions: { relations: ['asset', 'destination'] } },
    );

    const transaction = await manager.findOne(Transaction, transactionId, {
      relations: ['states', 'source', 'payments', 'payments.asset', 'payments.destination'],
    });

    const authorizedState = transaction.getStates().find(state => state.status === TransactionStatus.AUTHORIZED);
    const { additionalData } = authorizedState;
    const authorized = additionalData.hasOwnProperty('authorized') ? (additionalData.authorized as any[]) : [];
    authorized.push({
      provider: payment.asset.provider,
      asset: payment.asset.code,
      payment: payment.id,
    });

    await manager.update(TransactionStateItem, authorizedState.id, {
      additionalData: { ...additionalData, authorized },
    });

    return transaction;
  }
}
