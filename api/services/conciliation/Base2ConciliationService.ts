import {
  TransactionStatus,
  TransitoryAccountType,
  PaymentType,
  TransactionType,
  ResolvedBase2Record,
  TransactionTypeFriendly,
  IssueType,
  IssueCategory,
  SeverityLevel,
  IssueDetails,
  PaymentStatus,
} from '@bacen/base-sdk';
import { BaseServer, ServiceOptions, Service, BaseError } from 'ts-framework-common';
import { getManager } from 'typeorm';
import { Message, PubSub, Topic } from '@google-cloud/pubsub';
import * as Package from 'pjson';
import Config from '../../../config';
import {
  Transaction,
  TransactionStateItem as TransactionState,
  Wallet,
  Asset,
  Payment,
  Base2HandledTransaction,
  Base2Action,
  TRANSACTION_REFUNDABLE_STATES,
  TRANSACTION_SUCCESS_STATES,
} from '../../models';
import { TransactionStateMachine } from '../../fsm';
import { TransactionPipelinePublisher } from '../pipeline';
import { IssueHandler } from '..';
import { pubSub } from '../../../config/pubsub.config';

export interface Base2ConciliationServiceOptions extends ServiceOptions {
  responseTopic: string;
}

export interface AssertRequiredInfoOptions {
  wallet: Wallet;
  transitoryWallet: Wallet;
  asset: Asset;
}

export interface GetTransactionAmountOptions {
  amount: string;
  asset: Asset;
  wallet: Wallet;
}

export class Base2ConciliationService extends Service {
  protected static instance: Base2ConciliationService;

  protected _isSubscribed: boolean = false;

  protected responseTopic?: Topic;

  constructor(options: Base2ConciliationServiceOptions) {
    super({ ...options, name: 'Base2ConciliationService' });

    this.responseTopic = pubSub.topic(options.responseTopic);

    this.messageHandler = this.messageHandler.bind(this);
  }

  public static initialize(options: Base2ConciliationServiceOptions): Base2ConciliationService {
    return (this.instance = new Base2ConciliationService(options));
  }

  public static getInstance(): Base2ConciliationService {
    if (!this.instance) {
      throw new BaseError('Service not initialized', {
        service: 'Base2ConciliationService',
      });
    }

    return this.instance;
  }

  public async publishResponse(message: object): Promise<void> {
    await this.responseTopic.publishJSON(message);
  }

  public async handleReversion(reversalRecord: ResolvedBase2Record, manager = getManager()): Promise<void> {
    const { externalId, type, amount, walletId, raw } = reversalRecord;
    this.logger?.silly(
      `${this.options?.name} handling reversion for transaction with externalId: ${reversalRecord.externalId}`,
    );
    const transaction = await Transaction.findByExternalId(externalId, manager);

    const wallet = await Wallet.safeFindOne({ where: { id: walletId } });

    const asset =
      transaction?.payments?.[0]?.asset || (await Asset.getByIdOrCode(Config.asset.defaultAuthorizableAsset));

    const transitoryWallet = await Wallet.getTransitoryAccountByType({
      type: TransitoryAccountType.CARD_TRANSACTION,
    });

    const hasAllRequiredInfo = await this.assertRequiredInfo({ wallet, transitoryWallet, asset });
    if (!hasAllRequiredInfo) {
      this.logger?.warn(
        `${this.options?.name} Required infos weren't satisfied transaction with externalId ${reversalRecord.externalId} won't be processed`,
      );
      return;
    }

    const base2Transaction = Base2HandledTransaction.create({
      type,
      wallet,
      asset,
      externalId,
      originalAmount: amount,
      confirmedAmount: amount,
    });

    if (!transaction) {
      const persistedTransaction = await Transaction.prepare({
        manager,
        source: wallet,
        recipients: [
          {
            wallet: transitoryWallet,
            amount,
            asset,
          },
        ],
        type: PaymentType.AUTHORIZED_CARD,
        additionalData: {
          externalId,
          raw,
        } as any,
      });

      const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
        ...base2Transaction,
        action: Base2Action.TRANSACTION_CREATED,
        additionalData: {
          raw,
        },
      });

      await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
      const fsm = new TransactionStateMachine(persistedTransaction);
      await fsm.goTo(TransactionStatus.REVERSED);
      this.logger?.silly(`succsessfully created reversed transaction`, { externalId, type, amount });
    } else if (transaction.status === TransactionStatus.REVERSED) {
      const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
        ...base2Transaction,
        action: Base2Action.NONE,
        additionalData: {
          raw,
          transactions: {
            original: transaction.id,
          },
        },
      });

      await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
      this.logger?.silly(`Transaction ok`, { externalId, type, amount });
    } else if (transaction.status === TransactionStatus.AUTHORIZED) {
      const fsm = new TransactionStateMachine(transaction);

      try {
        const success = await fsm.goTo(TransactionStatus.REVERSED);

        if (!success) {
          this.logger?.error(`Error reversing authorized transaction from base2`, { externalId, type, amount });
        } else {
          this.logger?.silly(`Successfully reversed authorized transaction from base2`, { externalId, type, amount });
        }

        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
          ...base2Transaction,
          action: Base2Action.TRANSACTION_REVERSED,
          additionalData: {
            raw,
            transactions: {
              original: transaction.id,
            },
          },
        });

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
      } catch (err) {
        this.logger?.error(`Error reversing authorized transaction from base2`, {
          externalId,
          type,
          amount,
          error: err,
        });

        const issueHandler = IssueHandler.getInstance();
        await issueHandler.handle({
          type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
          category: IssueCategory.EXTERNAL_AUTHORIZER,
          severity: SeverityLevel.CRITICAL,
          componentId: Package.name,
          details: IssueDetails.from(err),
          description: err,
        });
      }
    } else if (TRANSACTION_REFUNDABLE_STATES.includes(transaction.status)) {
      const reversal = await Transaction.findTransactionReversal(transaction, manager);

      if (!reversal) {
        const transitoryAccount = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);
        const wallet = transaction.source;

        const reversalTransaction = await Transaction.prepare({
          source: transitoryAccount,
          recipients: transaction.payments.map(payment => ({
            wallet,
            amount: payment.amount,
            asset,
          })),
          type: PaymentType.AUTHORIZED_CARD_REVERSAL,
          additionalData: {
            externalId,
            originalTransaction: transaction.additionalData.hash,
            raw,
          } as any,
        });

        const publisher = TransactionPipelinePublisher.getInstance();
        await publisher.send(reversalTransaction);

        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
          ...base2Transaction,
          action: Base2Action.TRANSACTION_REVERSED,
          additionalData: {
            raw,
            transactions: {
              original: transaction.id,
              reversal: reversalTransaction.id,
            },
          },
        });

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        this.logger?.silly(`Sent newly created base2 transaction to pipeline`, { externalId, type, amount });
      } else if (TRANSACTION_SUCCESS_STATES.includes(reversal.status)) {
        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
          ...base2Transaction,
          action: Base2Action.NONE,
          additionalData: {
            raw,
            transactions: {
              original: transaction.id,
              reversal: reversal.id,
            },
          },
        });

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        this.logger?.silly(`Transaction ok`, { externalId, type, amount });
      } else if (reversal.status === TransactionStatus.AUTHORIZED) {
        const publisher = TransactionPipelinePublisher.getInstance();

        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind({
          ...base2Transaction,
          action: Base2Action.REVERSAL_EXECUTED,
          additionalData: {
            raw,
            transactions: {
              original: transaction.id,
              reversal: reversal.id,
            },
          },
        });

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        await publisher.send(reversal);
        this.logger?.silly(`Sent authorized base2 transaction to pipeline`, { externalId, type, amount });
      }
    }
  }

  public async handleConfirmation(record: ResolvedBase2Record, manager = getManager()): Promise<void> {
    const { externalId, type, amount, walletId, raw } = record;
    this.logger?.silly(
      `${this.options?.name} handling confirmation for transaction with externalId: ${record.externalId}`,
    );

    const publisher = TransactionPipelinePublisher.getInstance();
    const base2Amount = Number(amount).toFixed(2);

    const wallet = await Wallet.findOne({ where: { id: walletId } });
    const transitoryWallet = await Wallet.getRootTransitoryAccountWallet(TransitoryAccountType.CARD_TRANSACTION);

    const transaction = await Transaction.findByExternalId(externalId, manager);

    const asset = transaction?.payments?.[0]?.asset || (await Asset.getByCode(Config.asset.defaultAuthorizableAsset));

    const hasAllRequiredInfo = await this.assertRequiredInfo({ wallet, transitoryWallet, asset });
    if (!hasAllRequiredInfo) {
      this.logger?.warn(
        `${this.options?.name} Required infos weren't satisfied transaction with externalId ${record.externalId} won't be processed`,
      );
      return;
    }

    const base2Transaction = Base2HandledTransaction.create({
      type,
      asset,
      wallet,
      externalId,
      confirmedAmount: amount,
    });

    if (!transaction) {
      const transactionAmount = await this.getTransactionAmount({ amount, asset, wallet });

      if (parseFloat(transactionAmount) > 0) {
        const persistedTransaction = await Transaction.prepare({
          manager,
          source: wallet,
          recipients: [
            {
              wallet: transitoryWallet,
              amount: transactionAmount,
              asset,
            },
          ],
          type: PaymentType.AUTHORIZED_CARD,
          additionalData: {
            externalId,
            raw,
          } as any,
        });

        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
          {
            ...base2Transaction,
            action: Base2Action.TRANSACTION_CREATED,
            originalAmount: amount,
            additionalData: {
              raw,
              transactions: {
                created: persistedTransaction.id,
              },
            },
          },
          { manager },
        );

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        await publisher.send(persistedTransaction);
        this.logger?.silly(`succsessfully created transaction`, { externalId, type, amount });
      } else {
        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
          {
            ...base2Transaction,
            action: Base2Action.NONE,
            additionalData: {
              raw,
            },
          },
          { manager },
        );

        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        this.logger?.silly('Cannot create transaction, user has no balance available', {
          type,
          amount,
          externalId,
        });
      }
    } else {
      const transactionStatus = transaction.status;
      const originalAmount = transaction
        .payments!.reduce((total, payment) => total + Number(payment.amount), 0)
        .toFixed(2);

      if (transactionStatus === TransactionStatus.REVERSED) {
        const transactionAmount = await this.getTransactionAmount({ amount, asset, wallet });

        if (parseFloat(transactionAmount) > 0) {
          const persistedTransaction = await Transaction.prepare({
            manager,
            source: wallet,
            recipients: [
              {
                wallet: transitoryWallet,
                amount: transactionAmount,
                asset,
              },
            ],
            type: PaymentType.AUTHORIZED_CARD,
            additionalData: {
              externalId,
              raw,
              originalTransaction: transaction.id,
            } as any,
          });

          const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
            {
              ...base2Transaction,
              originalAmount,
              action: Base2Action.REVERSAL_UNDONE,
              additionalData: {
                raw,
                transactions: {
                  original: transaction.id,
                  confirmation: persistedTransaction.id,
                },
              },
            },
            { manager },
          );

          await publisher.send(persistedTransaction);
          await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
          this.logger?.silly(`succsessfully undid transaction reversal`, { externalId, type, amount });
        } else {
          const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
            {
              ...base2Transaction,
              originalAmount,
              action: Base2Action.NONE,
              additionalData: {
                raw,
                transactions: {
                  original: transaction.id,
                },
              },
            },
            { manager },
          );
          await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
          this.logger?.silly('Cannot adjusted authorized transaction amount, user has no balance available', {
            type,
            amount,
            externalId,
          });
        }
      } else if (transactionStatus === TransactionStatus.AUTHORIZED) {
        if (originalAmount !== base2Amount) {
          const transactionAmount = await this.getTransactionAmount({ amount, asset, wallet });

          const fsm = new TransactionStateMachine(transaction);
          await fsm.goTo(TransactionStatus.REVERSED);

          if (parseFloat(transactionAmount) > 0) {
            const adjustedTransaction = await Transaction.prepare({
              manager,
              source: wallet,
              recipients: [
                {
                  wallet: transitoryWallet,
                  amount: transactionAmount,
                  asset,
                },
              ],
              type: PaymentType.AUTHORIZED_CARD,
              additionalData: {
                externalId,
                raw,
              } as any,
            });

            const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
              {
                ...base2Transaction,
                originalAmount,
                action: Base2Action.AMOUNT_ADJUSTED,
                additionalData: {
                  raw,
                  transactions: {
                    original: transaction.id,
                    adjustment: adjustedTransaction.id,
                  },
                },
              },
              { manager },
            );

            await publisher.send(adjustedTransaction);
            await this.publishResponse(persistedBase2Transaction);
            this.logger?.silly('Adjusted authorized transaction value', { type, amount, externalId });
          } else {
            const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
              {
                ...base2Transaction,
                originalAmount,
                action: Base2Action.NONE,
                additionalData: {
                  raw,
                  transactions: {
                    original: transaction.id,
                  },
                },
              },
              { manager },
            );

            await this.publishResponse(persistedBase2Transaction);
            this.logger?.silly('Cannot adjusted authorized transaction amount, user has no balance available', {
              type,
              amount,
              externalId,
            });
          }
        } else {
          const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
            {
              ...base2Transaction,
              originalAmount,
              action: Base2Action.TRANSACTION_EXECUTED,
              additionalData: {
                raw,
                transactions: {
                  original: transaction.id,
                },
              },
            },
            { manager },
          );

          await publisher.send(transaction);
          await this.publishResponse(persistedBase2Transaction);
          this.logger?.silly(`Successfully processed transaction from base2`, { externalId, type, amount });
        }
      } else if (originalAmount !== amount) {
        const transactionAmount = await this.getTransactionAmount({ amount, asset, wallet });

        if (parseFloat(transactionAmount) > 0) {
          const transactionReversal = await Transaction.prepare({
            manager,
            source: transitoryWallet,
            type: PaymentType.AUTHORIZED_CARD_REVERSAL,
            recipients: transaction.payments.map(pmt => ({
              wallet,
              asset,
              amount: pmt.amount,
            })),
            additionalData: {
              raw,
              externalId,
              reversal: true,
              originalTransaction: transaction.additionalData.hash,
            } as any,
          });

          const adjustedTransaction = await Transaction.prepare({
            manager,
            source: wallet,
            recipients: [
              {
                wallet: transitoryWallet,
                amount: transactionAmount,
                asset,
              },
            ],
            type: PaymentType.AUTHORIZED_CARD,
            additionalData: {
              externalId,
              raw,
            } as any,
          });

          const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
            {
              ...base2Transaction,
              originalAmount,
              action: Base2Action.AMOUNT_ADJUSTED,
              additionalData: {
                raw,
                transactions: {
                  original: transaction.id,
                  reversal: transactionReversal.id,
                  adjustment: adjustedTransaction.id,
                },
              },
            },
            { manager },
          );

          await Promise.all([transaction, transactionReversal, adjustedTransaction].map(t => publisher.send(t)));
          await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
          this.logger?.silly('Adjusted authorized transaction value', { type, amount, externalId });
        } else {
          const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
            {
              ...base2Transaction,
              originalAmount,
              action: Base2Action.NONE,
              additionalData: {
                raw,
                transactions: {
                  original: transaction.id,
                },
              },
            },
            { manager },
          );

          await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
          this.logger?.silly('Cannot adjusted authorized transaction amount, user has no balance available', {
            type,
            amount,
            externalId,
          });
        }
      } else {
        const persistedBase2Transaction = await Base2HandledTransaction.insertAndFind(
          {
            ...base2Transaction,
            originalAmount,
            action: Base2Action.NONE,
            additionalData: {
              raw,
              transactions: {
                original: transaction.id,
              },
            },
          },
          { manager },
        );

        await publisher.send(transaction);
        await this.publishResponse(persistedBase2Transaction.toInterchangeFormat());
        this.logger?.silly(`Successfully processed transaction from base2`, { externalId, type, amount });
      }
    }
  }

  public async handleCreditVoucher(record: ResolvedBase2Record, manager = getManager()): Promise<void> {
    const { externalId, type, amount, walletId, raw } = record;

    this.logger?.silly(
      `${this.options?.name} handling credit voucher for transaction with externalId: ${record.externalId}`,
    );

    const transitoryWallet = await Wallet.getTransitoryAccountByType({
      type: TransitoryAccountType.CARD_TRANSACTION,
    });

    const wallet = await Wallet.findOne({
      where: { id: walletId },
      relations: ['assetRegistrations', 'assetRegistrations.states', 'assetRegistrations.asset'],
    });

    const asset = await Asset.getByIdOrCode(Config.asset.defaultAuthorizableAsset);

    const hasAllRequiredInfo = await this.assertRequiredInfo({ wallet, transitoryWallet, asset });
    if (!hasAllRequiredInfo) {
      this.logger?.warn(
        `${this.options?.name} Required infos weren't satisfied transaction with externalId ${record.externalId} won't be processed`,
      );
      return;
    }

    const { transaction, base2Transaction } = await manager.transaction(async tx => {
      const transaction = await Transaction.prepare({
        source: transitoryWallet,
        recipients: [{ wallet, amount, asset }],
        type: PaymentType.TRANSFER,
        additionalData: {
          externalTransaction: { id: externalId },
        },
        manager: tx,
      });

      const base2Transaction = await Base2HandledTransaction.insertAndFind(
        {
          type,
          asset,
          wallet,
          externalId,
          originalAmount: amount,
          confirmedAmount: amount,
          action: Base2Action.TRANSACTION_CREATED,
          additionalData: {
            raw,
            transactions: {
              original: transaction.id,
            },
          },
        },
        { manager: tx },
      );

      return { transaction, base2Transaction };
    });

    await this.publishResponse(base2Transaction.toInterchangeFormat());
    await TransactionPipelinePublisher.getInstance().send(transaction);

    this.logger?.silly(`Succefully gave credit to user`, { externalId, type, amount });
  }

  public async messageHandler(message: Message): Promise<void> {
    try {
      const payload: ResolvedBase2Record = JSON.parse(message.data.toString('utf8'));
      this.logger?.debug(`${this.options?.name} Got a new message to handle: ${message.id}. `, { ...payload });

      // PubSub guarantees at-least-once delivery, so we must handle redeliveries
      const alreadyExists = await Base2HandledTransaction.existsByExternalId(payload.externalId);
      if (alreadyExists) {
        this.logger?.info(
          `${this.options?.name} transaction with externalId ${payload.externalId} already exists: ${message.id}. Message acked.`,
        );
        message.ack();
        return;
      }

      switch (payload.type) {
        case TransactionTypeFriendly.CREDIT_VOUCHER_REVERSAL:
        case TransactionTypeFriendly.PURCHASE_REVERSAL:
        case TransactionTypeFriendly.WITHDRAW_REVERSAL:
          await this.handleReversion(payload);
          break;

        case TransactionTypeFriendly.PURCHASE:
        case TransactionTypeFriendly.WITHDRAW:
          await this.handleConfirmation(payload);
          break;

        case TransactionTypeFriendly.CREDIT_VOUCHER:
          await this.handleCreditVoucher(payload);
          break;

        default:
          this.logger?.warn(`Received unhandled base2 record. Skipping`, { ...payload });
          const issueHandler = IssueHandler.getInstance();
          await issueHandler.handle({
            type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
            category: IssueCategory.EXTERNAL_AUTHORIZER,
            severity: SeverityLevel.CRITICAL,
            componentId: Package.name,
            details: IssueDetails.from(payload),
            description: 'Received unhandled base2 record',
          });
      }
    } catch (err) {
      this.logger?.error(`${this.options?.name} Error handling message ${message.id}`, { ...message });

      const readableError = err.toJSON ? err.toJSON() : err.message ? err.message : err;

      const issueHandler = IssueHandler.getInstance();
      await issueHandler.handle({
        type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
        category: IssueCategory.EXTERNAL_AUTHORIZER,
        severity: SeverityLevel.CRITICAL,
        componentId: Package.name,
        details: IssueDetails.from(message),
        description: readableError,
      });
    } finally {
      this.logger?.silly(`${this.options?.name} Message ${message.id} acked.`);
      message.ack();
    }
  }

  public onMount(server: BaseServer): void {}

  public async onReady(server: BaseServer): Promise<void> {}

  public async onInit(server: BaseServer): Promise<void> {
    if (pubSub) {
      pubSub.subscription(Config.pubSubConfig.base2Subscription).on('message', this.messageHandler);
      this._isSubscribed = true;
    }
  }

  public onUnmount(server: BaseServer): void {
    if (this._isSubscribed) {
      pubSub.subscription(Config.pubSubConfig.base2Subscription).removeListener('message', this.messageHandler);
      this._isSubscribed = false;
    }
  }

  private async assertRequiredInfo({ wallet, transitoryWallet, asset }: AssertRequiredInfoOptions): Promise<boolean> {
    const issueHandler = IssueHandler.getInstance();
    let hasAllInfo = true;

    if (!transitoryWallet) {
      hasAllInfo = false;
      this.logger?.info(`${this.options?.name} transitory wallet not found`);
      await issueHandler.handle({
        type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
        category: IssueCategory.EXTERNAL_AUTHORIZER,
        severity: SeverityLevel.CRITICAL,
        componentId: Package.name,
        details: IssueDetails.from(transitoryWallet),
        description: 'Cannot find wallet',
      });
    }

    if (!wallet) {
      hasAllInfo = false;
      this.logger?.info(`${this.options?.name} wallet not found`);
      await issueHandler.handle({
        type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
        category: IssueCategory.EXTERNAL_AUTHORIZER,
        severity: SeverityLevel.CRITICAL,
        componentId: Package.name,
        details: IssueDetails.from(wallet),
        description: 'Cannot find transitory wallet',
      });
    }

    if (!asset) {
      hasAllInfo = false;
      this.logger?.info(`${this.options?.name} asset not found`);
      await issueHandler.handle({
        type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
        category: IssueCategory.EXTERNAL_AUTHORIZER,
        severity: SeverityLevel.CRITICAL,
        componentId: Package.name,
        details: IssueDetails.from(asset),
        description: 'Cannot find authorizable asset',
      });
    }

    return hasAllInfo;
  }

  private async getTransactionAmount({ amount, asset, wallet }: GetTransactionAmountOptions): Promise<string> {
    let transactionAmount = amount;
    const availableBalance = await wallet.getAuthorizableBalance(asset);

    if (availableBalance < +amount) {
      transactionAmount = availableBalance.toString();

      if (availableBalance > 0) {
        const issueHandler = IssueHandler.getInstance();
        await issueHandler.handle({
          type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
          category: IssueCategory.EXTERNAL_AUTHORIZER,
          severity: SeverityLevel.CRITICAL,
          componentId: Package.name,
          details: IssueDetails.from(wallet),
          description: "User doesn't have enough balance to create transaction with full amount",
        });
      } else {
        const issueHandler = IssueHandler.getInstance();
        await issueHandler.handle({
          type: IssueType.EXTERNAL_AUTHORIZER_TRANSACTION_FAILED,
          category: IssueCategory.EXTERNAL_AUTHORIZER,
          severity: SeverityLevel.CRITICAL,
          componentId: Package.name,
          details: IssueDetails.from(wallet),
          description: 'Transaction not created user has 0 balance',
        });
      }
    }

    return transactionAmount;
  }
}
