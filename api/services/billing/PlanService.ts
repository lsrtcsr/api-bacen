import { Service, ServiceOptions, BaseError } from 'ts-framework-common';
import { getManager, EntityManager } from 'typeorm';
import * as moment from 'moment';
import { ServiceType, UserRole } from '@bacen/base-sdk';
import { HttpError, HttpCode } from 'ts-framework';
import { ContractService } from './ContractService';
import { PlanRequestSchema, PlanServiceSchema, SubscriptionRequestSchema } from '../../schemas';
import {
  EntryType,
  EventType,
  Contract,
  Plan,
  PlanStatus,
  PriceItem,
  Service as ServiceModel,
  User,
  Domain,
  EntryClazz,
} from '../../models';

export interface PlanServiceOptions extends ServiceOptions {}

export class PlanService extends Service {
  public options: PlanServiceOptions;

  protected static instance: PlanService;

  constructor(options: PlanServiceOptions = {}) {
    super(options);
  }

  static initialize(options: PlanServiceOptions) {
    this.instance = new PlanService(options);
  }

  static getInstance(options?: PlanServiceOptions) {
    if (!this.instance) this.initialize(options || {});
    return this.instance;
  }

  public async getEntryTypes(options: {
    userId: string;
    eventType: EventType;
    serviceType?: ServiceType;
    provider?: string;
  }): Promise<EntryType[]> {
    const contract = await ContractService.getInstance().getCurrentContract(options.userId);
    return contract?.plan.getEntryTypes(options);
  }

  public async getServiceTypes(userId: string, eventType: EventType): Promise<ServiceType[]> {
    const contract = await ContractService.getInstance().getCurrentContract(userId);
    let serviceTypes = await contract.plan.getServiceTypes(eventType);
    if (!serviceTypes.length && !contract.plan.default) {
      const defaultPlan = await this.getDefaultPlan(contract.supplier);
      if (!defaultPlan) return [];

      serviceTypes = await defaultPlan.getServiceTypes(eventType);
    }

    return serviceTypes;
  }

  public async getDefaultPlan(supplier: Domain): Promise<Plan> {
    return await Plan.safeFindOne({
      where: { supplier, default: true, status: PlanStatus.AVAILABLE },
      relations: ['supplier', 'priceList', 'items'],
    });
  }

  public async createPlan(options: {
    planData: PlanRequestSchema;
    supplier?: Domain;
    createdBy?: User;
    validUntil?: moment.Moment;
  }): Promise<Plan> {
    let supplier: Domain;
    if (options.supplier) {
      supplier = options.supplier;
    } else {
      const user = options.createdBy.domain
        ? options.createdBy
        : await User.safeFindOne({ where: { id: options.createdBy.id }, relations: ['domain'] });

      supplier = user.domain;
    }

    const currentDefaultPlan = await Plan.safeFindOne({ where: { supplier: { id: supplier.id }, default: true } });

    const validFrom = moment(options.planData.validFrom) || moment();
    const validUntil = options.planData.default ? moment() : moment(options.validUntil) || moment();

    let planId;
    await getManager().transaction(async transaction => {
      const now = moment();

      const isDefault = options.planData.hasOwnProperty('default') ? options.planData.default : !currentDefaultPlan;
      if (isDefault && currentDefaultPlan) {
        await transaction.update<Plan>(Plan, currentDefaultPlan.id, { default: false });
      }

      const planInsertResult = await transaction.insert(
        Plan,
        Plan.create({
          supplier,
          validFrom,
          name: options.planData.name,
          billingSettings: options.planData.billingSettings,
          status: validFrom && validFrom.isAfter(now) ? PlanStatus.UNAVAILABLE : PlanStatus.AVAILABLE,
          default: isDefault,
        }),
      );
      planId = planInsertResult.identifiers[0].id;

      const planItems = options.planData.items;
      for (const item of planItems) {
        const service = await transaction.findOne(ServiceModel, { where: { id: item.serviceId } });

        const entryType = await this.createEntryType(item, transaction);

        await transaction.insert(EntryType, {
          plan: planId as any,
          ...entryType,
        });

        if (!item.price) continue;

        await transaction.insert(
          PriceItem,
          PriceItem.create({
            service,
            plan: planId as any,
            validFrom: item.validFrom || moment(),
            amount: item.price.toFixed(2),
          }),
        );
      }
    });

    return Plan.findOne({ where: { id: planId } });
  }

  public async updateBasicInfo(options: {
    id: string;
    name?: string;
    status?: PlanStatus;
    validUntil?: moment.Moment;
  }): Promise<Plan> {
    const currentPlan = await Plan.findOne(options.id);
    const now = moment();

    if (currentPlan.status === options.status)
      throw new HttpError(`Plan is already in status ${currentPlan.status}`, HttpCode.Client.BAD_REQUEST);

    if (options.status === PlanStatus.AVAILABLE) {
      if (!options.validUntil) currentPlan.validUntil = null;

      currentPlan.validFrom = now;
    }

    if (options.status === PlanStatus.AVAILABLE && !options.validUntil) currentPlan.validUntil = null;

    if (options.status === PlanStatus.UNAVAILABLE) options.validUntil = now;

    return await Plan.updateAndFind(options.id, {
      name: options.name || currentPlan.name,
      status: options.status || currentPlan.status,
      validUntil: options.validUntil || currentPlan.validUntil,
      validFrom: currentPlan.validFrom,
    });
  }

  public async subscribe(subscriptionRequestData: SubscriptionRequestSchema): Promise<Contract> {
    let planId;
    if (subscriptionRequestData.plan) {
      planId =
        subscriptionRequestData.plan instanceof Plan ? subscriptionRequestData.plan.id : subscriptionRequestData.plan;
    }

    let supplier;
    if (subscriptionRequestData.user.role === UserRole.MEDIATOR) {
      supplier = await Domain.getRootDomain();
    } else {
      const contractor = subscriptionRequestData.user.domain
        ? subscriptionRequestData.user
        : await User.safeFindOne({ where: { id: subscriptionRequestData.user.id }, relations: ['domain'] });

      supplier = contractor.domain;
    }

    const plan = planId
      ? await Plan.safeFindOne({ where: { supplier, id: planId, status: PlanStatus.AVAILABLE } })
      : await Plan.safeFindOne({ where: { supplier, default: true, status: PlanStatus.AVAILABLE } });

    if (!plan) {
      throw new BaseError('There is no plan with the given id or it is not available anymore');
    }

    // avoid subscribing the same plan
    const currentSubscription = await Contract.safeFindOne({
      where: { contractor: { id: subscriptionRequestData.user.id }, current: true },
      relations: ['contractor', 'plan'],
    });
    if (currentSubscription && currentSubscription.plan.id === plan.id) {
      this.logger.warn(
        `The user ${subscriptionRequestData.user.id} already has an active subscription `.concat(
          `of the plan provided (${planId}). Skipping...`,
        ),
      );
      return currentSubscription;
    }

    return getManager().transaction(async transaction => {
      return ContractService.getInstance().createContract({
        plan,
        transaction,
        ...subscriptionRequestData,
      });
    });
  }

  public async updatePlan(planId: string, items: PlanServiceSchema[]): Promise<Plan> {
    await getManager().transaction(async transaction => {
      const plan = await transaction.findOne(Plan, planId);

      for (const item of items) {
        await this.setServiceSettings(plan, item, transaction);
        await this.setServicePrice(plan, item, transaction);
      }
    });

    return Plan.findOne({ where: { id: planId } });
  }

  public async setServiceSettings(
    plan: Plan,
    entryData: PlanServiceSchema,
    manager: EntityManager = getManager(),
  ): Promise<EntryType> {
    const entryType = await this.createEntryType(entryData, manager);

    // otherwise, save sent settings
    return plan.setEntryType(entryType, manager);
  }

  public async setServicePrice(
    plan: Plan,
    entryData: PlanServiceSchema,
    manager: EntityManager = getManager(),
  ): Promise<PriceItem> {
    const service = await manager.findOne(ServiceModel, { where: { id: entryData.serviceId } });

    return plan.setPriceItem(
      PriceItem.create({
        service,
        validFrom: entryData.validFrom || moment(),
        amount: (entryData.price && entryData.price.toFixed(2)) || undefined,
      }),
      manager,
    );
  }

  public async createEntryType(
    entryData: PlanServiceSchema,
    manager: EntityManager = getManager(),
  ): Promise<EntryType> {
    if (
      entryData.entryClazz &&
      entryData.entryClazz !== EntryClazz.SERVICE_CHARGE &&
      (!entryData.value || !entryData.entryCalculationMode)
    ) {
      throw new Error('The fields entryCalculationMode and value are required');
    }

    const service = await manager.findOne(ServiceModel, { where: { id: entryData.serviceId } });

    return EntryType.create({
      service,
      name: entryData.name || service.name,
      validFrom: entryData.validFrom || moment(),
      validUntil: entryData.validUntil,
      settings: {
        rule: entryData.rule ? { ...entryData.rule } : undefined,
        entryCalculationMode: entryData.entryCalculationMode || 'price_percentage',
        clazz: entryData.entryClazz || EntryClazz.SERVICE_CHARGE,
        billingTrigger: entryData.billingTrigger || EventType.TRANSACTION,
        freeUntil: entryData.freeUntil || 0,
        value: entryData.value || 100,
        liability: entryData.liability || 'bacen',
        reverseOnFail: entryData.reverseOnFail || false,
      },
    });
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
