import { Service, ServiceOptions, BaseError } from 'ts-framework-common';
import { getManager, EntityManager } from 'typeorm';
import * as currency from 'currency.js';
import * as moment from 'moment';
import {
  PaymentType,
  UserRole,
  TransactionStatus,
  SeverityLevel,
  IssueType,
  IssueCategory,
  IssueDetails,
  UnleashFlags,
  TransitoryAccountType,
  CustodyFeature,
  PaymentStatus,
  ServiceType,
} from '@bacen/base-sdk';
import * as Package from 'pjson';
import { UnleashUtil } from '@bacen/shared-sdk';
import { PlanService } from './PlanService';
import {
  Entry,
  Period,
  EntryType,
  EventType,
  EntryStatus,
  Invoice,
  User,
  InvoiceState,
  InvoiceStatus,
  PeriodState,
  PeriodStatus,
  Payment,
  OperationType,
  EntryClazz,
  Transaction,
  PriceItem,
  Wallet,
  Asset,
  Contract,
  Event,
  TransactionStateItem,
  TRANSACTION_SUCCESS_STATES,
} from '../../models';
import { InvoicePipelinePublisher, PeriodPipelinePublisher } from '../pipeline';
import { getOperationTypeFromEntryClazz, ProviderUtil } from '../../utils';
import { ContractService } from './ContractService';
import { IssueHandler } from '../issue/IssueHandler';
import { ProviderManagerService } from '../provider';
import { GenericError } from '../../errors';
import Config from '../../../config';

export interface BillingServiceOptions extends ServiceOptions {}

export class BillingService extends Service {
  public options: BillingServiceOptions;

  protected static instance: BillingService;

  constructor(options: BillingServiceOptions = {}) {
    super(options);
  }

  static initialize(options: BillingServiceOptions) {
    this.instance = new BillingService(options);
  }

  static getInstance(options?: BillingServiceOptions) {
    if (!this.instance) this.initialize(options || {});
    return this.instance;
  }

  public async createEntry(
    event: Event,
    eventTypes: EventType[] = [EventType.TRANSACTION],
    target?: Period,
    entityManager: EntityManager = getManager(),
  ): Promise<Entry[]> {
    const accountable = await User.safeFindOne({ where: { id: event.data.userId }, relations: ['domain'] });

    const currentContract = await ContractService.getInstance().getCurrentContract(event.data.userId);

    // Prepare feature flag context
    let forceBilling = false;
    const context = { userId: accountable.id, properties: { domainId: accountable.domain.id } };
    if (accountable.role === UserRole.CONSUMER) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_CONSUMER_BILLING, context, false);
    } else if (accountable.role === UserRole.MEDIATOR) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false);
    }

    if (!forceBilling && !currentContract) return [];

    if (!currentContract && forceBilling) {
      await IssueHandler.getInstance().handle({
        type: IssueType.PLAN_SUBSCRIPTION_NOT_FOUND,
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(event),
        description: 'Error attempting to creating invoice entry: plan subscription not found!',
      });

      return [];
    }

    if (eventTypes[0] === EventType.TRANSACTION && currentContract.prepaid && event.data.payment) {
      const payment = await Payment.safeFindOne({
        where: { id: event.data.payment.id },
        relations: ['transaction', 'transaction.states'],
      });

      if (!TRANSACTION_SUCCESS_STATES.includes(payment.transaction.status)) {
        event.data.settled = false;
      }
    }

    try {
      const period = target
        ? await Period.safeFindOne({ where: { id: target.id }, relations: ['invoice', 'states'] })
        : await (await Invoice.getCurrent(currentContract.contractor)).getCurrentPeriod();

      await this.openForNewEntries(period, eventTypes, event.data.eventDate);

      const planService = PlanService.getInstance();

      const entryTypesPromises = eventTypes.map((eventType) =>
        planService.getEntryTypes({
          eventType,
          userId: event.data.userId,
          serviceType: event.data.type,
          provider: event.data?.extra?.provider,
        }),
      );
      const entryTypes = [].concat.apply([], await Promise.all(entryTypesPromises));

      this.logger.debug('Entry types', { ...entryTypes });

      if (!entryTypes || !entryTypes.length) return [];

      const entriesPromises = entryTypes.map((type) => this.calculateEntry(type, event, period.invoice));
      const entries = [].concat.apply([], await Promise.all(entriesPromises));

      if (!entries || !entries.length) return [];

      const insertResult = await entityManager.insert(Entry, entries);
      const entryIdList = insertResult.identifiers.map((entry) => entry.id);

      await entityManager.createQueryBuilder().relation(Period, 'entries').of(period).add(entryIdList);

      return await entityManager.findByIds(Entry, entryIdList);
    } catch (error) {
      this.logger.error('Error attempting to create invoice entries ', { target, data: event.data, ...error });
      throw error;
    }
  }

  /**
   * Calculates an invoice entry based on the entry settings,
   * its price and data related to the service consumption
   *
   * @param type      the entry settings
   * @param event     the data related to the service consumption
   * @param invoice   the invoice in witch the entry should be created
   */
  public async calculateEntry(entryType: EntryType, event: Event, invoice: Invoice): Promise<Entry[]> {
    const entries: Entry[] = [];
    const serviceData = event.data;

    const fullInvoice = invoice.periods
      ? invoice
      : await Invoice.findOne({ where: { id: invoice.id }, relations: ['periods'] });

    const type =
      entryType.plan && entryType.entries && entryType.service
        ? entryType
        : await EntryType.findOne({ where: { id: entryType.id }, relations: ['plan', 'entries', 'service'] });

    const priceItem = await this.getCurrentPrice({
      service: serviceData.type,
      userId: serviceData.userId,
      provider: (serviceData.extra && serviceData.extra.provider) || undefined,
    });

    if (priceItem && Number(priceItem.amount) === 0) return entries;

    // TODO review this assumption
    const rootAsset = await Asset.getRootAsset();

    // TODO
    // check if current priceItem was created before event

    // otherwise, find the applicable priceItem (that was active in the event creation moment)

    let status;
    let amount;
    if (type.settings.clazz === EntryClazz.SERVICE_CHARGE) {
      let baseAmount;
      if (type.settings.entryCalculationMode === 'transaction_percentage') {
        const calculationBasis = (serviceData.event as Transaction).payments
          .filter((payment) => payment.type !== PaymentType.SERVICE_FEE)
          .reduce((total, payment) => total + Number(payment.amount), 0);

        baseAmount = currency(calculationBasis, { precision: 7 }).multiply(type.settings.value).divide(100);
      } else if (type.settings.entryCalculationMode === 'price_percentage') {
        baseAmount = currency(priceItem.amount, { precision: 7 }).multiply(type.settings.value).divide(100);
      } else if (type.settings.entryCalculationMode === 'absolute') {
        baseAmount = currency(priceItem.amount, { precision: 7 });
      }
      amount = serviceData.calculationRule ? serviceData.calculationRule(baseAmount) : baseAmount;
      status = serviceData.settled ? EntryStatus.SETTLED : EntryStatus.PENDING;
    } else if (
      [EntryClazz.PROGRESSIVE_DISCOUNT, EntryClazz.COMMISSION, EntryClazz.INCENTIVE, EntryClazz.PENALTY].includes(
        type.settings.clazz,
      )
    ) {
      const entriesRelatedToRule = await fullInvoice.findEntryByType(type, false);

      if (
        type.settings.rule.maxNumberOfEntries &&
        type.settings.rule.maxNumberOfEntries <= entriesRelatedToRule.length
      ) {
        return [];
      }

      const serviceConsumptionRecords: any[] = [serviceData.event];
      if (type.settings.rule.threshold > 1) {
        // sort desc by creation date
        entriesRelatedToRule.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());

        // filter by date was disabled because is not working as expeted
        // TODO it is critical and should be reviwed asap
        const charges = await fullInvoice.findEntryByService(
          serviceData.type,
          false,
          // entriesRelatedToRule.length ? entriesRelatedToRule[0].createdAt : type.validFrom
        );

        charges
          .map((charge) => charge.additionalData.sourceTransaction)
          .forEach((event) => serviceConsumptionRecords.push(event));
      }

      let elegible = false;
      if (
        type.settings.rule.thresholdCheckCalculationMode === 'transaction_amount' &&
        [ServiceType.WITHDRAWAL, ServiceType.DEPOSIT, ServiceType.TRANSFER].includes(type.service.type)
      ) {
        const sum = serviceConsumptionRecords
          .map((record) => currency((record as Transaction).totalAmount[rootAsset.code].valueOf()))
          .reduce((accumulator, current) => accumulator.add(current), currency(0, { precision: 7 }));

        elegible = sum >= currency(type.settings.rule.threshold);
      } else if (type.settings.rule.thresholdCheckCalculationMode === 'transaction_count') {
        elegible = serviceConsumptionRecords.length > type.settings.rule.threshold;
      } else {
        throw new Error('Not supported');
      }

      if (!elegible) return [];

      /**
       * the percentage mode is applicable only for events originated
       * from payments (deposits, withdrawals, transfers)
       */

      if (
        type.settings.entryCalculationMode === 'transaction_percentage' &&
        [ServiceType.WITHDRAWAL, ServiceType.DEPOSIT, ServiceType.TRANSFER].includes(type.service.type)
      ) {
        /**
         * for incentives and penalties settings we assume that the calculation
         * basis of entry's amount should be the transaction amount
         */
        const calculationBasis = type.settings.rule.excludeServiceFee
          ? (serviceData.event as Transaction).payments
              .filter((payment) => payment.type !== PaymentType.SERVICE_FEE)
              .reduce((total, payment) => total + Number(payment.amount), 0)
          : (serviceData.event as Transaction).totalAmount[rootAsset.code].valueOf();

        amount = currency(calculationBasis, { precision: 7 }).multiply(type.settings.value).divide(100);
      } else if (type.settings.entryCalculationMode === 'price_percentage') {
        amount = currency(priceItem.amount, { precision: 7 }).multiply(type.settings.value).divide(100);
      } else if (type.settings.entryCalculationMode === 'absolute') {
        amount = currency(priceItem.amount, { precision: 7 });
      } else {
        throw new Error('The entryCalculationMode is not supported');
      }
      status = EntryStatus.PENDING;
    }

    if (!amount) {
      await IssueHandler.getInstance().handle({
        type: IssueType.SERVICE_CHARGE_PAYMENT_FAILED, // create a specific type asap
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(event),
        description: 'Service fee could not be calculated: price or settings of '.concat(
          `service ${entryType.service.type} not found or inconsistent!`,
        ),
      });

      throw new BaseError('Service fee could not be calculated');
    }

    let freeOfCharge = false;
    // if there is a setting for free use of the service and the limit has
    // not yet been reached, create an entry that reverse the charge
    if (type.settings.clazz === EntryClazz.SERVICE_CHARGE && type.settings.freeUntil) {
      const freeConsumption = await fullInvoice.getServiceConsumption(serviceData.type, true);

      if (type.settings.freeUntil > freeConsumption) {
        entries.push(
          Entry.create({
            type,
            event,
            priceItem,
            amount: amount.format(false),
            operationType: OperationType.CREDIT,
            status: EntryStatus.SETTLED,
            additionalData: {
              sourceId: serviceData.event && serviceData.event.id,
              sourceTransaction: serviceData.event,
              chargeback: true,
            },
          }),
        );

        freeOfCharge = true;
      }
    }

    entries.push(
      Entry.create({
        type,
        event,
        status: freeOfCharge ? EntryStatus.SETTLED : status,
        priceItem:
          type.settings.clazz === EntryClazz.SERVICE_CHARGE || type.settings.entryCalculationMode === 'price_percentage'
            ? priceItem
            : undefined,
        amount: amount.format(false),
        operationType: getOperationTypeFromEntryClazz(type.settings.clazz),
        additionalData: {
          paymentProof: type.settings.clazz === EntryClazz.SERVICE_CHARGE ? serviceData.payment : undefined,
          sourceId: serviceData.event && serviceData.event.id,
          sourceTransaction: serviceData.event,
          chargeback: false,
        },
      }),
    );

    return entries;
  }

  public async getServiceFee(options: {
    serviceType: ServiceType;
    user: User;
    contract?: Contract;
    provider?: string;
    asset?: Asset;
    transactionAmount?: currency.Any;
  }): Promise<Partial<Payment>> {
    const contractor: User = options.user.domain
      ? options.user
      : await User.safeFindOne({ where: { id: options.user.id }, relations: ['domain'] });

    const currentContract = options.contract || (await ContractService.getInstance().getCurrentContract(contractor.id));
    if (!currentContract) return undefined;

    const holder =
      contractor.role === UserRole.CONSUMER ? await User.getMediator(contractor.domain) : await User.getRootMediator();

    const destination = await Wallet.getTransitoryAccountByType({ holder, type: TransitoryAccountType.SERVICE_FEE });

    if (!destination) {
      await IssueHandler.getInstance().handle({
        type: IssueType.SERVICE_CHARGE_PAYMENT_FAILED, // create a specific type asap
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(currentContract),
        description: 'Transitory account not found',
      });

      throw new BaseError('Transitory account not found');
    }

    const priceItem = await this.getCurrentPrice({
      service: options.serviceType,
      contract: currentContract,
      provider: options.provider,
    });

    const entryType = await currentContract.plan.getChargeSettingsByService(options.serviceType, options.provider);

    if (
      !entryType ||
      (entryType.settings?.entryCalculationMode === 'transaction_percentage' && !options.transactionAmount) ||
      ((!priceItem || Number(priceItem.amount) === 0) &&
        ['price_percentage', 'absolute'].includes(entryType.settings?.entryCalculationMode))
    )
      return undefined;

    const freeUsageLimit = entryType?.settings?.freeUntil || 0;

    this.logger.debug(`Free usage limit: ${freeUsageLimit}`);

    const invoice = await Invoice.getCurrent(contractor);
    const freeConsumption = await invoice.getServiceConsumption(options.serviceType, true);

    this.logger.debug(`Free consumption: ${freeConsumption}`);

    let feeAmount: currency;
    if (priceItem) {
      if (entryType.settings.entryCalculationMode === 'price_percentage') {
        feeAmount = currency(priceItem.amount, { precision: 7 }).multiply(entryType.settings.value).divide(100);
      } else if (entryType.settings.entryCalculationMode === 'absolute') {
        feeAmount = currency(priceItem.amount, { precision: 7 });
      }
    } else if (
      options.transactionAmount &&
      entryType?.settings?.entryCalculationMode === 'transaction_percentage' &&
      entryType?.settings?.value
    ) {
      feeAmount = currency(options.transactionAmount, { precision: 7 }).multiply(entryType.settings.value).divide(100);
    }

    if (!feeAmount) {
      await IssueHandler.getInstance().handle({
        type: IssueType.SERVICE_CHARGE_PAYMENT_FAILED, // create a specific type asap
        category: IssueCategory.BILLING,
        severity: SeverityLevel.HIGH,
        componentId: Package.name,
        details: IssueDetails.from(currentContract),
        description: 'Service fee could not be calculated: price or settings of '.concat(
          `service ${options.serviceType} not found or inconsistent!`,
        ),
      });

      throw new BaseError('Service fee could not be calculated');
    }

    const asset = options.asset || (await Asset.getRootAsset());

    const serviceFee: Partial<Payment> = {
      asset,
      destination,
      type: PaymentType.SERVICE_FEE,
      amount: freeUsageLimit > freeConsumption ? '0.00' : feeAmount.format(false),
    };

    return serviceFee;
  }

  public async getCurrentPrice(options: {
    service: ServiceType;
    userId?: string;
    provider?: string;
    contract?: Contract;
  }): Promise<PriceItem | undefined> {
    const contract = options.contract || (await ContractService.getInstance().getCurrentContract(options.userId));
    return contract.plan.getCurrentPrice(options.service, options.provider);
  }

  public async chargeServiceFee(
    fee: Partial<Payment>,
    transaction: Transaction,
    manager: EntityManager = getManager(),
  ): Promise<Transaction> {
    const provider = ProviderManagerService.getInstance();
    const p2p = provider.from(fee.asset.provider).feature(CustodyFeature.PAYMENT);
    await ProviderUtil.throwIfFeatureNotAvailable(p2p);

    this.logger.debug(`Requesting payment to external provider "${fee.asset.provider}"`);
    const payment = Payment.create({
      ...fee,
      status: PaymentStatus.AUTHORIZED,
      transaction: { id: transaction.id },
    });
    await manager.insert(Payment, payment);

    const authorizedState = transaction.getStates().find((state) => state.status === TransactionStatus.AUTHORIZED);
    const { additionalData } = authorizedState;
    const authorized = additionalData.hasOwnProperty('authorized') ? (additionalData.authorized as any[]) : [];
    authorized.push({
      provider: payment.asset.provider,
      asset: payment.asset.code,
      payment: payment.id,
    });

    await manager.update(TransactionStateItem, authorizedState.id, {
      additionalData: { ...additionalData, authorized },
    });
    authorizedState.additionalData = { ...additionalData, authorized };

    // If we have no control of the balances (such as cdt-provider) we pre-authorize using P2P
    if (!Config.asset.controlledBalanceProviders.includes(payment.asset.provider)) {
      const response = await p2p.payment([payment]);
      const feePaymentResult = response.find((paymentResult) => paymentResult.id === payment.id);

      // Make sure payment status is updated
      if (feePaymentResult.status !== payment.status) {
        await manager.update(Payment, payment.id, { status: feePaymentResult.status });
        payment.status = feePaymentResult.status;
      }

      this.logger.info(`Payment successfully authorized in external provider "${fee.asset.provider}"`, { ...response });
    }

    /*
     * We were reloading the transaction from the DB to fetch it with the serviceFee payment populated and with changes
     * to the authorizedState additional data applied, but we're changing those inline on the transaction obj to avoid
     * a new query.
     */
    transaction.payments = transaction.payments ?? [];
    transaction.payments.push(payment);
    return transaction;
  }

  public async openForNewEntries(
    period: Period,
    eventTypes: EventType[],
    eventDate: moment.Moment = moment(),
  ): Promise<boolean> {
    if (period.closedAt || period.status === PeriodStatus.CLOSED) return false;

    if (period.closureScheduledFor.isBefore(eventDate) && eventTypes.includes(EventType.TRANSACTION)) {
      let type = IssueType.PERIOD_CLOSING_DATE_EXPIRED;
      if (period.invoice.closureScheduledFor.isBefore(eventDate)) {
        await InvoicePipelinePublisher.getInstance().send(period.invoice);
        type = IssueType.INVOICE_CLOSING_DATE_EXPIRED;
      } else {
        await PeriodPipelinePublisher.getInstance().send(period);
      }

      const message = (type === IssueType.PERIOD_CLOSING_DATE_EXPIRED
        ? `Period ${period.id} `
        : `Invoice ${period.invoice.id} `
      ).concat('closing date was expired. ');
      throw new GenericError({
        type,
        category: IssueCategory.BILLING,
        defaultMessage: message.concat('Thrown exception to retry after open a new one'),
      });
    }

    return true;
  }

  public async createInvoice(options: {
    contractor: User;
    contract?: Contract;
    from?: moment.Moment;
    transaction?: EntityManager;
  }): Promise<void> {
    const transaction = options.transaction || getManager();
    const from = options.from || moment();

    const alreadyExists = await Invoice.getCurrent(options.contractor);
    if (alreadyExists) {
      throw new BaseError('There cannot be more than one open invoice at a time');
    }

    const contract =
      options.contract || (await ContractService.getInstance().getCurrentContract(options.contractor.id, transaction));

    const invoiceClosingDate = await contract.plan.getNextInvoiceClosingDate(from);
    const invoiceInsertResult = await transaction.insert(
      Invoice,
      Invoice.create({
        contractor: options.contractor,
        startedAt: from,
        closureScheduledFor: invoiceClosingDate,
      }),
    );
    const newInvoiceId = invoiceInsertResult.identifiers[0].id;

    await transaction.insert(
      InvoiceState,
      InvoiceState.create({
        status: InvoiceStatus.ACTIVE,
        invoice: newInvoiceId as any,
      }),
    );

    const periodClosingDate = await contract.plan.getNextPeriodClosingDate(from);
    const periodInsertResult = await transaction.insert(
      Period,
      Period.create({
        invoice: newInvoiceId as any,
        startedAt: from || moment(),
        closureScheduledFor: periodClosingDate,
      }),
    );
    const newPeriodId = periodInsertResult.identifiers[0].id;

    await transaction.insert(
      PeriodState,
      PeriodState.create({
        status: PeriodStatus.ACTIVE,
        period: newPeriodId as any,
      }),
    );
  }

  public async closeCurrentInvoice(contractor: User, force = false): Promise<boolean> {
    const currentInvoice = await Invoice.safeFindOne({
      where: { contractor, current: true },
      relations: ['contractor', 'periods'],
    });

    if (!currentInvoice) {
      throw new BaseError('There is no open invoices');
    }

    const now = moment();
    if (moment(currentInvoice.closureScheduledFor).isAfter(now) && !force) {
      throw new BaseError(`The invoice closure is scheduled for ${currentInvoice.closureScheduledFor}`);
    }

    await getManager().transaction(async (transaction) => {
      const currentPeriod = await currentInvoice.getCurrentPeriod();

      await transaction.update(Period, currentPeriod.id, { closureScheduledFor: now });
      await transaction.update(Invoice, currentInvoice.id, { closureScheduledFor: now });

      await InvoicePipelinePublisher.getInstance().send(currentInvoice);
    });

    return true;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
