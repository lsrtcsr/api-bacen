import { getManager, EntityManager } from 'typeorm';
import { Service, ServiceOptions, BaseError } from 'ts-framework-common';
import { UserRole } from '@bacen/base-sdk';
import * as moment from 'moment';
import { User, Contract, ContractStatus, Domain, Plan, ContractState } from '../../models';
import { BillingService } from './BillingService';
import { SubscriptionRequestSchema } from '../../schemas';

export interface ContractServiceOptions extends ServiceOptions {}

export class ContractService extends Service {
  public options: ContractServiceOptions;

  protected static instance: ContractService;

  constructor(options: ContractServiceOptions = {}) {
    super(options);
  }

  static initialize(options: ContractServiceOptions) {
    this.instance = new ContractService(options);
  }

  static getInstance(options?: ContractServiceOptions) {
    if (!this.instance) this.initialize(options || {});
    return this.instance;
  }

  public async createContract(subscriptionRequestData: SubscriptionRequestSchema): Promise<Contract> {
    if (![UserRole.MEDIATOR, UserRole.CONSUMER].includes(subscriptionRequestData.user.role)) {
      throw new BaseError('Forbiden: contracts can only be created for consumers and mediators');
    }

    if (!subscriptionRequestData.plan) {
      throw new BaseError('Plan id or instance is required');
    }

    const planId =
      subscriptionRequestData.plan instanceof Plan ? subscriptionRequestData.plan.id : subscriptionRequestData.plan;
    const plan = await Plan.safeFindOne({ where: { id: planId }, relations: ['supplier'] });
    const { supplier } = plan;

    // for now we will not support the prepaid option for mediators
    let prepaid = false;
    if (subscriptionRequestData.user.role === UserRole.CONSUMER) {
      prepaid = subscriptionRequestData.hasOwnProperty('prepaid') ? subscriptionRequestData.prepaid : true;
    }

    const validFrom = subscriptionRequestData.validFrom || moment();
    const transaction: EntityManager = subscriptionRequestData.transaction || getManager();

    const contractor = subscriptionRequestData.user.domain
      ? subscriptionRequestData.user
      : await transaction.findOne(User, { where: { id: subscriptionRequestData.user.id }, relations: ['domain'] });

    const currentContract = await this.getCurrentContract(contractor.id, transaction);
    if (currentContract) {
      const validUntil = moment();
      await transaction.update(Contract, currentContract.id, { validUntil, current: false });
      await transaction.insert(
        ContractState,
        ContractState.create({
          status: ContractStatus.FINISHED,
          contract: currentContract.id as any,
        }),
      );
    }

    const newContractInsertResult = await transaction.insert(
      Contract,
      Contract.create({
        supplier,
        contractor,
        plan,
        validFrom,
        prepaid,
      }),
    );
    const newContractId = newContractInsertResult.identifiers[0].id;

    this.logger.debug('newContractId:', newContractId);

    await transaction.insert(
      ContractState,
      ContractState.create({
        status: ContractStatus.ACTIVE,
        contract: newContractId as any,
      }),
    );

    const newContract = await transaction.findOne(Contract, {
      where: { id: newContractId },
      relations: ['supplier', 'contractor', 'plan'],
    });

    await BillingService.getInstance().createInvoice({
      contractor,
      transaction,
      from: validFrom,
      contract: newContract,
    });

    return newContract;
  }

  public async getCurrentContract(userId: string, manager: EntityManager = getManager()): Promise<Contract> {
    const user = await manager
      .createQueryBuilder(User, 'user')
      .leftJoinAndSelect('user.domain', 'domain')
      .where('user.id = :userId', { userId })
      .getOne();

    if (!user) throw new BaseError(`User ${userId} not found`);

    return manager
      .createQueryBuilder(Contract, 'contract')
      .leftJoinAndSelect('contract.supplier', 'supplier')
      .leftJoinAndSelect('contract.contractor', 'contractor')
      .leftJoinAndSelect('contract.states', 'states')
      .leftJoinAndSelect('contract.plan', 'plan')
      .leftJoinAndSelect('plan.priceList', 'priceList')
      .leftJoinAndSelect('plan.items', 'items')
      .leftJoinAndSelect('items.service', 'service')
      .leftJoinAndSelect('priceList.service', 'priceListService')
      .where('contractor.id = :userId', { userId })
      .andWhere('contract.current = :current', { current: true })
      .getOne();
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
