import { PaymentType, PaymentStatus, ServiceType, User, UserRole } from '@bacen/base-sdk';
import { BaseError } from 'nano-errors';
import { ServiceConsumptionData } from '../../../schemas';
import { EventHandler } from './EventHandler';
import { Transaction } from '../../../models';
import { fromDateToMoment, getPaymentTypeFromServiceType } from '../../../utils';

export class TransactionHandler extends EventHandler<Transaction> {
  public async handle(type: ServiceType, event: Transaction, extra?: any): Promise<ServiceConsumptionData> {
    // TODO: review if this is safe.
    const transaction = event;

    const paymentMatchesTheTypeOfServiceTypeHandled = transaction.payments.some(
      (payment) => payment.type === getPaymentTypeFromServiceType(type),
    );
    if (!paymentMatchesTheTypeOfServiceTypeHandled) {
      throw new BaseError('Transaction does not match the type of service handled', transaction);
    }

    let feePaymentTransaction = ((extra && extra.feePaymentProof) || transaction) as Transaction;
    feePaymentTransaction = await Transaction.createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('source.user', 'user')
      .leftJoinAndSelect('transaction.payments', 'payments')
      .leftJoinAndSelect('payments.asset', 'assets')
      .leftJoinAndSelect('payments.destination', 'destinations')
      .where('transaction.id = :transactionId', { transactionId: feePaymentTransaction.id })
      .getOne();

    const paymentProof = feePaymentTransaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);

    let accountable: User;
    if (feePaymentTransaction.source.user.role === UserRole.ADMIN || extra?.accountable) {
      // deposit or an user defined target wallet
      accountable = extra?.accountable;
    } else if ([UserRole.CONSUMER, UserRole.MEDIATOR].includes(feePaymentTransaction.source.user.role)) {
      // cash out and default wallet
      accountable = feePaymentTransaction.source.user;
    }

    if (!accountable) {
      throw new BaseError('User responsible for paying the fee not provided', transaction);
    }

    const serviceData: ServiceConsumptionData = {
      type,
      extra,
      event: transaction,
      payment: paymentProof,
      userId: accountable.id,
      eventDate: fromDateToMoment(transaction.createdAt),
      settled: paymentProof && paymentProof.status === PaymentStatus.SETTLED,
    };

    return serviceData;
  }

  public isHandled(type: ServiceType): boolean {
    return [
      ServiceType.BOLETO_PAYMENT,
      ServiceType.CARD_PURCHASE,
      ServiceType.CARD_WITHDRAWAL,
      ServiceType.WITHDRAWAL,
      ServiceType.TRANSFER,
      ServiceType.PHONE_CREDITS_PURCHASE,
      ServiceType.DEPOSIT,
      ServiceType.BOLETO_PAYMENT_CASH_IN,
    ].includes(type);
  }
}
