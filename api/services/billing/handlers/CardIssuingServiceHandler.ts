import { PaymentType, ServiceType } from '@bacen/base-sdk';
import * as moment from 'moment';
import { ServiceConsumptionData } from '../../../schemas';
import { EventHandler } from './EventHandler';
import { Transaction, Payment, User, TRANSACTION_SUCCESS_STATES } from '../../../models';

export interface CardIssuingServiceEvent {
  user: User;
  operationDate?: moment.Moment;
  serviceData: any;
  transaction?: Transaction;
}

export class CardIssuingServiceHandler extends EventHandler<CardIssuingServiceEvent> {
  public async handle(type: ServiceType, event: CardIssuingServiceEvent, extra?: any): Promise<ServiceConsumptionData> {
    const eventDate = event.operationDate || moment();
    let paymentProof: Payment;
    let settled = false;
    if (event.transaction) {
      const transaction = await Transaction.safeFindOne({
        where: { id: event.transaction.id },
        relations: ['payments', 'source', 'source.user'],
      });

      paymentProof =
        event.transaction && event.transaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);

      settled = paymentProof && TRANSACTION_SUCCESS_STATES.includes(transaction.status);
    }

    return {
      type,
      event,
      eventDate,
      extra,
      settled,
      payment: paymentProof,
      userId: event.user.id,
    };
  }

  public isHandled(type: ServiceType): boolean {
    return [ServiceType.PHYSICAL_CARD_ISSUING, ServiceType.VIRTUAL_CARD_ISSUING].includes(type);
  }
}
