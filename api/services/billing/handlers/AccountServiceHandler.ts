import * as moment from 'moment';
import { PaymentType, ServiceType } from '@bacen/base-sdk';
import { ServiceConsumptionData } from '../../../schemas';
import { EventHandler } from './EventHandler';
import { User, Transaction, Payment, TRANSACTION_SUCCESS_STATES } from '../../../models';

export interface AccountServiceEvent {
  user: User;
  operationDate?: moment.Moment;
  serviceData?: any;
  transaction?: Transaction;
}

export class AccountServiceHandler extends EventHandler<AccountServiceEvent> {
  public async handle(type: ServiceType, event: AccountServiceEvent, extra?: any): Promise<ServiceConsumptionData> {
    const eventDate = event.operationDate || moment();
    let settled = false;
    let paymentProof: Payment;
    if (event.transaction) {
      const transaction = await Transaction.safeFindOne({
        where: { id: event.transaction.id },
        relations: ['payments', 'source', 'source.user'],
      });

      paymentProof =
        event.transaction && event.transaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);

      settled = paymentProof && TRANSACTION_SUCCESS_STATES.includes(transaction.status);
    }

    return {
      type,
      event,
      eventDate,
      extra,
      settled,
      payment: paymentProof,
      userId: event.user.id,
    };
  }

  public isHandled(type: ServiceType): boolean {
    return [ServiceType.ACCOUNT_OPENING, ServiceType.ACCOUNT_CLOSING].includes(type);
  }
}
