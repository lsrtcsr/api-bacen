import { CardStatus, UserRole, UserStatus, ServiceType } from '@bacen/base-sdk';
import * as moment from 'moment';
import { LoggerInstance } from 'ts-framework-common';
import { In } from 'typeorm';
import { Card, User } from '../../../models';
import { ServiceConsumptionData } from '../../../schemas/request/billing';

export interface ClosingEntryHandlerOptions {
  logger?: LoggerInstance;
}

export class ClosingEntryHandler {
  private logger: LoggerInstance;

  public options: ClosingEntryHandlerOptions;

  protected static instance: ClosingEntryHandler;

  constructor(options: ClosingEntryHandlerOptions = {}) {
    this.logger = options && options.logger;
    this.options = options;
  }

  static initialize(options: ClosingEntryHandlerOptions) {
    this.instance = new ClosingEntryHandler(options);
  }

  static getInstance() {
    return this.instance;
  }

  public async handle(user: User, type: ServiceType): Promise<ServiceConsumptionData> {
    const serviceData: ServiceConsumptionData = {
      type,
      userId: user.id,
      eventDate: moment(),
      liability: user.role === UserRole.CONSUMER ? 'consumer' : 'mediator',
      settled: false,
    };

    switch (type) {
      case ServiceType.ACTIVE_ACCOUNT_MAINTENANCE:
        serviceData.calculationRule = await this.activeAccounts(user);
        break;

      case ServiceType.ACTIVE_CARD_MAINTENANCE:
        serviceData.calculationRule = await this.activeCards(user);
        break;

      default:
        serviceData.calculationRule = undefined;
        break;
    }

    return serviceData;
  }

  private async activeCards(user: User): Promise<Function> {
    const numberOfActiveCards = await Card.safeCount({
      where: {
        // virtual: false,
        status: CardStatus.AVAILABLE,
      },
    });
    const rule = (fee: currency) => fee.multiply(numberOfActiveCards);

    return rule;
  }

  private async activeAccounts(user: User): Promise<Function> {
    const contractor = user.domain ? user : await User.safeFindOne({ where: { id: user.id } });

    const activeAccounts = await User.safeFind({
      where: {
        domain: { id: contractor.domain.id },
        role: In([UserRole.CONSUMER, UserRole.MEDIATOR]),
      },
      relations: ['states'],
    });
    const numberOfActiveAccounts = activeAccounts.filter(account => account.status === UserStatus.ACTIVE).length;

    const rule = (fee: currency) => fee.multiply(numberOfActiveAccounts);

    return rule;
  }
}
