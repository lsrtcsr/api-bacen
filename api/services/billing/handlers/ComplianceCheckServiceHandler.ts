import { DatasetType } from '@bacen/bigdatacorp-service';
import * as moment from 'moment';
import { PaymentType, TransactionStatus, ServiceType } from '@bacen/base-sdk';
import { ServiceConsumptionData } from '../../../schemas';
import { EventHandler } from './EventHandler';
import { User, Transaction, Payment } from '../../../models';
import Config from '../../../../config';

export interface ConsumerDataQueryEvent {
  user: User;
  datasets?: DatasetType[];
  serviceData?: any;
  operationDate: moment.Moment;
  transaction?: Transaction;
}

export class ComplianceCheckServiceHandler extends EventHandler<ConsumerDataQueryEvent> {
  public async handle(
    type: ServiceType,
    event: ConsumerDataQueryEvent,
    extra?: any,
  ): Promise<ServiceConsumptionData[]> {
    const user = event.user.domain
      ? event.user
      : await User.safeFindOne({ where: { id: event.user.id }, relations: ['domain'] });

    let paymentProof: Payment;
    let settled = false;
    if (event.transaction) {
      const transaction = await Transaction.safeFindOne({
        where: { id: event.transaction.id },
        relations: ['payments', 'source', 'source.user'],
      });

      paymentProof =
        event.transaction && event.transaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);

      settled = paymentProof && [TransactionStatus.AUTHORIZED].includes(transaction.status);
    }

    const datasets = event.datasets || Config.kyc.defaultDatasets;

    return datasets.map((dataset) => {
      return {
        event,
        extra,
        settled,
        payment: paymentProof,
        type: this.getServiceType(dataset),
        userId: user.id,
        eventDate: event.operationDate,
      } as ServiceConsumptionData;
    });
  }

  public getServiceType(dataset: DatasetType): ServiceType {
    switch (dataset) {
      case DatasetType.BASIC_DATA:
        return ServiceType.CDQ_PERSON_BASIC_DATASET;
      case DatasetType.ADDRESS:
        return ServiceType.CDQ_ADDRESS_DATASET;
      case DatasetType.EMAIL:
        return ServiceType.CDQ_EMAIL_DATASET;
      case DatasetType.PHONE:
        return ServiceType.CDQ_PHONE_DATASET;
      case DatasetType.KYC:
        return ServiceType.CDQ_KYC_DATASET;
      case DatasetType.RELATIONSHIPS:
        return ServiceType.CDQ_COMPANY_PARTNERS_DATASET;
    }
  }

  public isHandled(type: ServiceType): boolean {
    return [ServiceType.CONSUMER_DATA_QUERY, ServiceType.COMPLIANCE_CHECK].includes(type);
  }
}
