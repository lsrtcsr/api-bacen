import { Service, ServiceOptions, BaseError } from 'ts-framework-common';
import * as Package from 'pjson';
import { UserRole, SeverityLevel, IssueType, IssueCategory, UnleashFlags, ServiceType } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { ServiceConsumptionData } from '../../../schemas/request/billing';
import { EventHandler } from './EventHandler';
import { IssueHandler } from '../../issue/IssueHandler';
import { EventPipelinePublisher } from '../../pipeline/billing';
import { User, Event } from '../../../models';

export interface EventHandlingGatewayOptions extends ServiceOptions {}

export class EventHandlingGateway extends Service {
  private static instance: EventHandlingGateway;

  private handlers: EventHandler<any>[] = [];

  constructor(options: EventHandlingGatewayOptions = {}) {
    super(options);
  }

  public static initialize(options?: EventHandlingGatewayOptions) {
    this.instance = new EventHandlingGateway(options);
  }

  public static getInstance(): EventHandlingGateway {
    if (!this.instance) this.initialize();
    return this.instance;
  }

  public register(handler: EventHandler<any>) {
    this.handlers.push(handler);
  }

  public async process(eventType: ServiceType, event: any, extra?: any): Promise<void> {
    const eventHandlers = this.handlers.filter((handler) => handler.isHandled(eventType));
    for (const handler of eventHandlers) {
      try {
        const result = await handler.handle(eventType, event, extra);
        const entryDataList = result instanceof Array ? result : [result];
        for (const entryData of entryDataList) await this.publish(entryData);
      } catch (exception) {
        // TODO create a specific type and category
        await IssueHandler.getInstance().handle({
          type: IssueType.ENTRY_CREATION_ERROR,
          category: IssueCategory.BILLING,
          severity: SeverityLevel.HIGH,
          componentId: Package.name,
          details: {
            exception,
            resourceType: eventType,
            resourceId: (event.serviceData && event.serviceData.id) || event.id,
            resourceSnapshot: event,
          },
          description: `Error publishing service consumption data to pipeline: ${exception.message || undefined}`,
        });
      }
    }
  }

  public async publish(data: ServiceConsumptionData) {
    // publish event that will be consumer by the consumer billing
    const event = Event.create({ data });
    await Event.insert(event);

    const user = await User.createQueryBuilder('user')
      .leftJoinAndSelect('user.domain', 'domain')
      .where('user.id = :userId', { userId: data.userId })
      .getOne();

    if (!user) {
      throw new BaseError('Could not push event to queue, invalid information', { event });
    }

    await EventPipelinePublisher.getInstance().send(event);

    if (user.role !== UserRole.MEDIATOR) await this.publishMediatorBillingEvent(user, data);
  }

  public async publishMediatorBillingEvent(user: User, data: ServiceConsumptionData) {
    const mediator = await User.safeFindOne({
      where: {
        domain: { id: user.domain.id },
        role: UserRole.MEDIATOR,
      },
    });
    const context = { userId: mediator.id, properties: { domainId: user.domain.id } };

    if (UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false)) {
      const mediatorBillingEvent = Event.create({
        data: {
          ...data,
          userId: mediator.id,
          payment: undefined,
          settled: false,
          liability: 'mediator',
        } as ServiceConsumptionData,
      });
      await Event.insert(mediatorBillingEvent);
      await EventPipelinePublisher.getInstance().send(mediatorBillingEvent);
    }
  }

  async onInit(server) {}

  async onMount(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
