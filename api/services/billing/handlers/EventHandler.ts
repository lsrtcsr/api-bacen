import { ServiceType } from '@bacen/base-sdk';
import { ServiceConsumptionData } from '../../../schemas/request/billing';
import { EventHandlingGateway } from './EventHandlingGateway';

export abstract class EventHandler<T> {
  constructor() {
    EventHandlingGateway.getInstance().register(this);
  }

  abstract isHandled(type: ServiceType): boolean;

  abstract handle(type: ServiceType, event: T, extra?: any): Promise<ServiceConsumptionData | ServiceConsumptionData[]>;
}
