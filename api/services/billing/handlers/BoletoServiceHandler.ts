import * as moment from 'moment';
import { PaymentType, ServiceType } from '@bacen/base-sdk';
import { ServiceConsumptionData } from '../../../schemas/request/billing';
import { EventHandler } from './EventHandler';
import { User, Transaction, Payment, TRANSACTION_SUCCESS_STATES } from '../../../models';

export interface BoletoServiceEvent {
  user: User;
  operationDate: moment.Moment;
  serviceData: any;
  transaction?: Transaction;
}

export class BoletoServiceHandler extends EventHandler<BoletoServiceEvent> {
  public async handle(type: ServiceType, event: BoletoServiceEvent, extra?: any): Promise<ServiceConsumptionData> {
    const user = event.user.domain
      ? event.user
      : await User.safeFindOne({ where: { id: event.user.id }, relations: ['domain'] });

    let paymentProof: Payment;
    let settled = false;
    if (event.transaction) {
      const transaction = await Transaction.safeFindOne({
        where: { id: event.transaction.id },
        relations: ['payments', 'source', 'source.user'],
      });

      paymentProof =
        event.transaction && event.transaction.payments.find((payment) => payment.type === PaymentType.SERVICE_FEE);

      settled = paymentProof && TRANSACTION_SUCCESS_STATES.includes(transaction.status);
    }

    return {
      type,
      event,
      extra,
      settled,
      payment: paymentProof,
      userId: user.id,
      eventDate: event.operationDate,
    };
  }

  public isHandled(type: ServiceType): boolean {
    return [ServiceType.BOLETO_EMISSION, ServiceType.BOLETO_VALIDATION].includes(type);
  }
}
