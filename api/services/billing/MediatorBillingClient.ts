import { Service, ServiceOptions } from 'ts-framework-common';
import { Http } from '@bacen/base-sdk';
import { ServiceConsumptionData } from '../../schemas/request/billing';
import Config from '../../../config';

export interface MediatorBillingClientOptions extends ServiceOptions {}

export class MediatorBillingClient extends Service {
  public options: MediatorBillingClientOptions;

  protected static instance: MediatorBillingClient;

  constructor(options: MediatorBillingClientOptions = {}) {
    super(options);
  }

  static initialize(options: MediatorBillingClientOptions) {
    this.instance = new MediatorBillingClient(options);
  }

  static getInstance() {
    return this.instance;
  }

  public async createEntry(serviceData: ServiceConsumptionData): Promise<boolean> {
    const http: Http = new Http({ baseURL: Config.billing.mediatorBillingURL });

    const response = await http.post('', serviceData, {
      headers: {
        apiKey: Config.billing.billingAPIKey,
      },
    });

    this.logger.debug(`Evento enviado com sucesso para billing mediator: ${serviceData}`);

    if (!response || response.status !== 200) throw response;

    this.logger.debug(`Evento processado com sucesso do billing mediator: ${response}`);

    return true;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
