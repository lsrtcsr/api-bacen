import { MoreThanOrEqual, LessThanOrEqual, getManager } from 'typeorm';
import { Invoice, Period, InvoiceStatus, EntryType } from '../../models';
import { InvoicePipelinePublisher, PeriodPipelinePublisher } from '../pipeline';

export class TaskSchedule {
  /**
   * Review:
   * - now timezone for date comparing
   * - status check
   */
  public static async closeInvoices(): Promise<void> {
    const now = new Date();
    const invoices: Invoice[] = await Invoice.safeFind({
      where: {
        closureScheduledFor: MoreThanOrEqual(now),
        current: true,
      },
      relations: ['states'],
    });

    const invoicePipelinePublisher = InvoicePipelinePublisher.getInstance();
    invoices.forEach(async invoice => await invoicePipelinePublisher.send(invoice));
  }

  public static async closePeriods(): Promise<void> {
    const now = new Date();
    const periods: Period[] = await Period.safeFind({
      where: {
        closureScheduledFor: MoreThanOrEqual(now),
        current: true,
      },
      relations: ['states'],
    });

    const periodPipelinePublisher = PeriodPipelinePublisher.getInstance();
    periods.forEach(async period => await periodPipelinePublisher.send(period));
  }

  public static async invalidateExpiredEntryTypes(): Promise<void> {
    const now = new Date();
    await getManager().transaction(async transaction => {
      const expiredItems = await transaction.find(EntryType, {
        where: {
          validUntil: LessThanOrEqual(now),
          valid: true,
        },
      });

      expiredItems.forEach(async expiredItem => await expiredItem.invalidate(transaction));
    });
  }
}
