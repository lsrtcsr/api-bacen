import {
  DiscoveryService as BaseDiscoveryService,
  DiscoveryServiceOptions as BaseDiscoveryServiceOptions,
  DiscoveryStatus,
  Observer,
} from 'nano-discovery';
import { DiscoveryType } from './DiscoveryType';

export type DiscoveryListener = (type: DiscoveryType, status: DiscoveryStatus) => void;

export interface DiscoveryModulesMap {
  [name: string]: DiscoveryStatus;
}

export interface DiscoveryListenersMap {
  [clientId: string]: DiscoveryListener[];
}

export interface DiscoveryServiceOptions extends BaseDiscoveryServiceOptions {
  types?: DiscoveryType[];
  initialMap?: DiscoveryModulesMap;
  initialListeners?: DiscoveryListenersMap;
}

export class DiscoveryService extends BaseDiscoveryService {
  protected static instance: DiscoveryService;

  static initialize(options: DiscoveryServiceOptions): DiscoveryService {
    this.instance = new DiscoveryService(options);
    return this.instance;
  }

  static getInstance() {
    return this.instance;
  }

  public subscribe(type: DiscoveryType, listener: Observer): Promise<void> {
    return super.subscribe(type, listener);
  }

  public unsubscribe(type: DiscoveryType, listener: Observer): Promise<void> {
    return super.unsubscribe(type, listener);
  }

  public up(type: DiscoveryType): Promise<void> {
    this.logger.debug(`Discovery got update from "${type}", now it is UP`);
    return super.up(type);
  }

  public cdtUp(): Promise<void[]> {
    return Promise.all([
      super.up(DiscoveryType.CDT_PROVIDER_AUDIT),
      super.up(DiscoveryType.CDT_PROVIDER_BASE),
      super.up(DiscoveryType.CDT_PROVIDER_BOLETO_PAYMENT),
      super.up(DiscoveryType.CDT_PROVIDER_BOLETO_EMISSION),
      super.up(DiscoveryType.CDT_PROVIDER_CARD),
      super.up(DiscoveryType.CDT_PROVIDER_DEPOSIT),
      super.up(DiscoveryType.CDT_PROVIDER_PAYMENT),
      super.up(DiscoveryType.CDT_PROVIDER_PHONECREDIT),
      super.up(DiscoveryType.CDT_PROVIDER_POSTBACK),
      super.up(DiscoveryType.CDT_PROVIDER_WITHDRAW),
    ]);
  }

  public async creditasProviderUp(): Promise<void[]> {
    return Promise.all([
      super.up(DiscoveryType.CREDITAS_PROVIDER_BASE),
      super.up(DiscoveryType.CREDITAS_PROVIDER_BRANCH),
      super.up(DiscoveryType.CREDITAS_PROVIDER_DEPOSIT),
      super.up(DiscoveryType.CREDITAS_PROVIDER_PAYMENT),
      super.up(DiscoveryType.CREDITAS_PROVIDER_WITHDRAW),
      super.up(DiscoveryType.CREDITAS_PROVIDER_POSTBACK),
      super.up(DiscoveryType.CREDITAS_PROVIDER_DOCUMENT),
      super.up(DiscoveryType.CREDITAS_PROVIDER_PARTICIPATING_INSTITUTION),
    ]);
  }

  public async leccaProviderUp(): Promise<void[]> {
    return Promise.all([
      super.up(DiscoveryType.LECCA_PROVIDER_BASE),
      super.up(DiscoveryType.LECCA_PROVIDER_BRANCH),
      super.up(DiscoveryType.LECCA_PROVIDER_DEPOSIT),
      super.up(DiscoveryType.LECCA_PROVIDER_PAYMENT),
      super.up(DiscoveryType.LECCA_PROVIDER_WITHDRAW),
      super.up(DiscoveryType.LECCA_PROVIDER_PARTICIPATING_INSTITUTION),
    ]);
  }

  public async paratiProviderUp(): Promise<void[]> {
    return Promise.all([
      super.up(DiscoveryType.PARATI_PROVIDER_BASE),
      super.up(DiscoveryType.PARATI_PROVIDER_BRANCH),
      super.up(DiscoveryType.PARATI_PROVIDER_DEPOSIT),
      super.up(DiscoveryType.PARATI_PROVIDER_PAYMENT),
      super.up(DiscoveryType.PARATI_PROVIDER_WITHDRAW),
      super.up(DiscoveryType.PARATI_PROVIDER_POSTBACK),
      super.up(DiscoveryType.PARATI_PROVIDER_DOCUMENT),
      super.up(DiscoveryType.PARATI_PROVIDER_PARTICIPATING_INSTITUTION),
    ]);
  }

  public bs2Up(): Promise<void[]> {
    return Promise.all([
      super.up(DiscoveryType.BS2_PROVIDER_AUDIT),
      super.up(DiscoveryType.BS2_PROVIDER_BOLETO_EMISSION),
      super.up(DiscoveryType.BS2_PROVIDER_PAYMENT),
      super.up(DiscoveryType.BS2_PROVIDER_POSTBACK),
      super.up(DiscoveryType.BS2_PROVIDER_WITHDRAW),
    ]);
  }

  public celcoinUp(): Promise<void[]> {
    return Promise.all([super.up(DiscoveryType.CELCOIN_PROVIDER_BOLETO_PAYMENT)]);
  }

  public down(type: DiscoveryType): Promise<void> {
    return super.down(type);
  }

  public status(type: DiscoveryType): Promise<DiscoveryStatus> {
    return super.status(type);
  }
}
