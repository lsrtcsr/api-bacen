export enum DiscoveryType {
  CDT_PROVIDER_AUDIT = 'cdt-visa_provider_audit',
  CDT_PROVIDER_BASE = 'cdt-visa_provider_base',
  CDT_PROVIDER_BOLETO_PAYMENT = 'cdt-visa_provider_boleto_payment',
  CDT_PROVIDER_BOLETO_EMISSION = 'cdt-visa_provider_boleto_emission',
  CDT_PROVIDER_CARD = 'cdt-visa_provider_card',
  CDT_PROVIDER_DEPOSIT = 'cdt-visa_provider_deposit',
  CDT_PROVIDER_PAYMENT = 'cdt-visa_provider_payment',
  CDT_PROVIDER_PHONECREDIT = 'cdt-visa_provider_phone_credits',
  CDT_PROVIDER_POSTBACK = 'cdt-visa_provider_postback',
  CDT_PROVIDER_WITHDRAW = 'cdt-visa_provider_withdraw',

  BS2_PROVIDER_BOLETO_EMISSION = 'bs2_provider_boleto_emission',
  BS2_PROVIDER_AUDIT = 'bs2_provider_audit',
  BS2_PROVIDER_WITHDRAW = 'bs2_provider_withdraw',
  BS2_PROVIDER_PAYMENT = 'bs2_provider_payment',
  BS2_PROVIDER_POSTBACK = 'bs2_provider_postback',

  CELSIUS_PROVIDER = 'celsius_provider',

  CELCOIN_PROVIDER_BOLETO_PAYMENT = 'celcoin-provider_provider_boleto_payment',

  STR_PROVIDER_ = 'str_provider',

  CREDITAS_PROVIDER_BASE = 'creditas-provider_provider_base',
  CREDITAS_PROVIDER_DEPOSIT = 'creditas-provider_provider_deposit',
  CREDITAS_PROVIDER_PAYMENT = 'creditas-provider_provider_payment',
  CREDITAS_PROVIDER_WITHDRAW = 'creditas-provider_provider_withdraw',
  CREDITAS_PROVIDER_BRANCH = 'creditas-provider_provider_branch',
  CREDITAS_PROVIDER_POSTBACK = 'creditas-provider_provider_postback',
  CREDITAS_PROVIDER_DOCUMENT = 'creditas-provider_provider_document',
  CREDITAS_PROVIDER_PARTICIPATING_INSTITUTION = 'creditas-provider_provider_participating_institution',

  LECCA_PROVIDER_BASE = 'lecca-provider_provider_base',
  LECCA_PROVIDER_DEPOSIT = 'lecca-provider_provider_deposit',
  LECCA_PROVIDER_PAYMENT = 'lecca-provider_provider_payment',
  LECCA_PROVIDER_WITHDRAW = 'lecca-provider_provider_withdraw',
  LECCA_PROVIDER_BRANCH = 'lecca-provider_provider_branch',
  LECCA_PROVIDER_PARTICIPATING_INSTITUTION = 'lecca-provider_provider_participating_institution',

  PARATI_PROVIDER_BASE = 'parati-provider_provider_base',
  PARATI_PROVIDER_DEPOSIT = 'parati-provider_provider_deposit',
  PARATI_PROVIDER_PAYMENT = 'parati-provider_provider_payment',
  PARATI_PROVIDER_WITHDRAW = 'parati-provider_provider_withdraw',
  PARATI_PROVIDER_BRANCH = 'parati-provider_provider_branch',
  PARATI_PROVIDER_POSTBACK = 'parati-provider_provider_postback',
  PARATI_PROVIDER_DOCUMENT = 'parati-provider_provider_document',
  PARATI_PROVIDER_PARTICIPATING_INSTITUTION = 'parati-provider_provider_participating_institution',
}
