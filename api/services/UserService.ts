import { LoggerInstance, BaseError } from 'nano-errors';
import {
  UserRole,
  UserSchema,
  UnleashFlags,
  PlanStatus,
  UserStatus,
  AccountType,
  ConsumerStatus,
  TransitoryAccountType,
} from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { HooksService } from '@bacen/hooks-sdk';
import { EntityManager, getManager } from 'typeorm';
import { BaseEvent } from '@bacen/events';
import { Domain, Consumer, User, Phone, Asset, Plan } from '../models';
import { UniqueParameterViolationError, UninitializedServiceError } from '../errors';
import { ProviderManagerService } from './provider';
import { UserRepository, ConsumerRepository, CreateUserOptions } from '../repositories';
import { WalletService } from './WalletService';

export interface PrepareUserOptions {
  role: UserRole;

  extra: any;

  domain: Domain;

  payload: UserSchema;

  consumer: Consumer;

  password?: string;

  additionalData?: any;

  acceptProviderLegalTerms?: boolean;

  fingerprint?: string;
}

export class UserService {
  private static instance: UserService | undefined;

  constructor(private readonly logger: LoggerInstance, private readonly walletService: WalletService) {}

  public static initialize(logger: LoggerInstance, walletService: WalletService): UserService {
    return (this.instance = new UserService(logger, walletService));
  }

  public static getInstance(): UserService {
    if (!this.instance) {
      throw new UninitializedServiceError(this.name);
    }

    return this.instance;
  }

  public async createUserWithoutConsumer(
    data: CreateUserOptions,
    manager: EntityManager = getManager(),
  ): Promise<User> {
    const userRepository = new UserRepository(manager);
    // If username is not specified default to email as username.
    data.username = data.username || data.email;

    const user = await userRepository.create(data);
    await userRepository.appendState(user, { status: UserStatus.ACTIVE });

    return user;
  }

  public async prepare(options: PrepareUserOptions) {
    const {
      domain,
      payload,
      consumer: consumerSchema,
      password,
      role,
      additionalData,
      acceptProviderLegalTerms,
      fingerprint,
      extra,
    } = options;

    const email = payload.email.trim().toLowerCase();

    if (UnleashUtil.isEnabled(UnleashFlags.ENABLE_UNIQUE_USER_EMAIL)) {
      const userCount = await User.createQueryBuilder('user')
        .where('user.email = :email', { email })
        .andWhere('user.deletedAt IS NULL')
        .getCount();

      if (userCount > 0) {
        throw new UniqueParameterViolationError('User', { email });
      }
    }

    if (UnleashUtil.isEnabled(UnleashFlags.ENABLE_UNIQUE_USER_PHONE)) {
      for (const phone of consumerSchema.phones || []) {
        const phoneCount = await Phone.createQueryBuilder('phone')
          .leftJoin('phone.consumer', 'consumer')
          .leftJoin('consumer.user', 'user')
          .where('phone.code = :code', { code: phone.code })
          .andWhere('phone.number = :number', { number: phone.number })
          .andWhere('user.deletedAt IS NULL')
          .getCount();

        if (phoneCount > 0) {
          throw new UniqueParameterViolationError('PhoneCount', { phone });
        }
      }
    }

    // TaxId should not be unique unless explicitly required
    const shouldTaxIdBeUnique = UnleashUtil.isEnabled(UnleashFlags.FORCE_UNIQUE_TAX_ID, {}, false);

    if (shouldTaxIdBeUnique) {
      const consumerCount = await Consumer.createQueryBuilder('consumer')
        .leftJoin('consumer.user', 'user')
        .where('consumer.taxId = :taxId', { taxId: consumerSchema.taxId })
        .andWhere('user.deletedAt IS NULL')
        .getCount();

      if (consumerCount > 0) {
        throw new UniqueParameterViolationError('Consumer', { taxId: consumerSchema.taxId });
      }
    }

    if (extra && (extra.number || extra.bankingType)) {
      let available: boolean;
      // TODO: This may be done for assets other than the root-asset
      const asset = await Asset.getRootAsset();
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_HOOKS)) {
        const provider = ProviderManagerService.getInstance().from(asset.provider);
        available = await provider.isAccountNumberAvailable({
          accountNumber: extra.number,
          branch: extra.branch,
          bankingType: extra.bankingType,
        });
      } else {
        available = await HooksService.getInstance().wallets().isAccountNumberAvailable({
          asset: asset.code,
          branch: extra.branch,
          accountNumber: extra.number,
          bankingType: extra.bankingType,
        });
      }

      if (!available) {
        throw new UniqueParameterViolationError('Account with same number and/or type', {
          branch: extra.branch,
          number: extra.number,
          bankingType: extra.bankingType,
        });
      }
    }

    if (extra && extra?.plan?.id) {
      const plan = await Plan.safeFindOne({
        where: {
          id: extra.plan.id,
          supplier: domain,
          status: PlanStatus.AVAILABLE,
        },
      });
      if (!plan) throw new BaseError(`There is no billing plan with the given ID: ${extra.plan.id}`);
    }

    return getManager().transaction(async (manager) => {
      const userRepository = new UserRepository(manager);
      const consumerRepository = new ConsumerRepository(manager);

      const username = payload.username || payload.email;
      const user = await userRepository.create({ ...payload, password, role, domain, username });
      await userRepository.appendState(user, { status: UserStatus.PENDING, additionalData });

      // Prepare consumer details
      const {
        addresses: addressSchemas = [],
        bankings: bankingSchemas = [],
        documents: documentSchemas = [],
        phones: phoneSchemas = [],
        ...consumerData
      } = consumerSchema;

      if (consumerData.type === AccountType.CORPORATE) {
        consumerData.birthday = consumerData.birthday ? consumerData.birthday : consumerData.companyData.openingDate;
      }

      // Validate consumer stuff
      const consumer = Consumer.create({ ...consumerData, user });
      user.consumer = consumer;

      await manager.insert(Consumer, consumer);

      const skipPhoneVerification = UnleashUtil.isEnabled(
        UnleashFlags.SKIP_PHONE_VERIFICATION,
        { properties: { domainId: domain.id } },
        false,
      );

      let consumerStatus = ConsumerStatus.PENDING_LEGAL_ACCEPTANCE;

      if (user.role === UserRole.MEDIATOR) {
        consumerStatus = ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION;
      } else if (consumer.type === AccountType.PERSONAL && !skipPhoneVerification) {
        consumerStatus = ConsumerStatus.PENDING_PHONE_VERIFICATION;
      }

      await consumerRepository.appendState(consumer, { status: consumerStatus });

      if (addressSchemas.length) {
        await consumerRepository.createConsumerAddresses(consumer, addressSchemas);
      }

      if (bankingSchemas.length) {
        await consumerRepository.createConsumerBankings(consumer, bankingSchemas);
      }

      if (documentSchemas.length) {
        await consumerRepository.createConsumerDocuments(consumer, documentSchemas);
      }

      if (phoneSchemas.length) {
        await consumerRepository.createConsumerPhones(consumer, phoneSchemas);
      }

      const events: BaseEvent[] = [];

      const [wallet, walletEvents] = await this.walletService.createInitialWallet({
        user,
        manager,
        walletAdditionalData: { acceptProviderLegalTerms, fingerprint },
      });

      user.wallets = [wallet];
      events.push(...walletEvents);

      if (role === UserRole.MEDIATOR) {
        const [transitoryAccount, transitoryAcctEvents] = await this.walletService.createInitialWallet({
          user,
          manager,
          walletAdditionalData: {
            isTransitoryAccount: true,
            transitoryAccountType: TransitoryAccountType.SERVICE_FEE,
          },
          stateAdditionalData: { fingerprint, acceptProviderLegalTerms },
        });

        user.wallets.push(transitoryAccount);
        events.push(...transitoryAcctEvents);
      }

      // don't await it.
      events.map((event) => event.publish());
      return user;
    });
  }
}
