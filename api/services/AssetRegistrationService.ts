import { LoggerInstance } from 'nano-errors';
import IdentityService, {
  AnalysisType,
  AssetIdentitySchema,
  BaseUserData,
  ReportSchema,
  ReportStatus,
} from '@bacen/identity-sdk';
import { AssetRegistrationStatus, UnleashFlags, UserRole } from '@bacen/base-sdk';
import { EntityManager } from 'typeorm';
import { HttpCode, HttpError } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseEvent } from '@bacen/events';
import { UninitializedServiceError } from '../errors';
import { Wallet, AssetRegistration, Asset, User } from '../models';
import { AssetConfig } from './AssetService';
import { AssetRegistrationRepository, AssetRepository } from '../repositories';
import { AssetRegistrationStateMachine } from '../fsm/assetRegistration';
import { AssetRegistrationProbe } from '../probes';
import { ApproveAssetEvent } from '../events';
import { WalletService } from './WalletService';
import * as moment from 'moment';
import { cloneDeep } from 'lodash'

export interface IAssetRegistrationService {
  registerAssets(wallet: Wallet, assetConfigs: AssetConfig[], manager: EntityManager): Promise<BaseEvent[]>;

  registerAssetsForRootMediator(wallet: Wallet, assetConfigs: AssetConfig[], manager: EntityManager): Promise<void>;
}

export class AssetRegistrationService implements IAssetRegistrationService {
  private static instance: AssetRegistrationService | undefined;

  private readonly probe: AssetRegistrationProbe;

  constructor(logger: LoggerInstance, private readonly identityService: IdentityService) {
    this.probe = new AssetRegistrationProbe(this.constructor.name, logger);
  }

  public static initialize(logger: LoggerInstance, identityService: IdentityService) {
    return (this.instance = new AssetRegistrationService(logger, identityService));
  }

  public static getInstance(): AssetRegistrationService {
    if (!this.instance) {
      throw new UninitializedServiceError(this.name);
    }

    return this.instance;
  }

  public async create(wallet: Wallet, asset: Asset, manager: EntityManager): Promise<AssetRegistration> {
    const assetRegistrationRepository = new AssetRegistrationRepository(manager);

    const assetRegistration = await assetRegistrationRepository.create(asset, wallet);
    await assetRegistrationRepository.appendState(assetRegistration, {
      status: AssetRegistrationStatus.PENDING_REGISTRATION,
    });

    wallet.assetRegistrations.push(assetRegistration);
    return assetRegistration;
  }

  public async shouldSkipRootMediatorApproval(user: User, wallet: Wallet): Promise<boolean> {
    const context = UnleashUtil.getContext({ user, wallet });
    const skipRootMediatorApproval = UnleashUtil.isEnabled(
      UnleashFlags.SKIP_ROOT_MEDIATOR_REPORT_APPROVAL,
      context,
      false,
    );

    return skipRootMediatorApproval && (await user.isRootMediator());
  }

  public async createReport(assetRegistration: AssetRegistration, manager: EntityManager): Promise<BaseEvent[]> {
    const { wallet } = assetRegistration;
    const { user } = wallet;
    const events: BaseEvent[] = [];

    const reportService = this.identityService.reports();
    const skipRootMediatorApproval = await this.shouldSkipRootMediatorApproval(user, wallet);

    const reportCreationMethod = skipRootMediatorApproval
      ? reportService.createForRootMediator.bind(reportService)
      : reportService.create.bind(reportService);

    const assetIdentity = await this.chooseAssetIdentity(assetRegistration.asset, user.domain.id);
    const fsm = new AssetRegistrationStateMachine(assetRegistration, { manager });

    if (assetIdentity) {
      let report: ReportSchema;

      try {
        // Should we also check if the report already exists for consumers ?
        let reports: ReportSchema[] = [];
        if (user.role === UserRole.MEDIATOR) {
          reports = await this.identityService.reports().findAll({
            filters: {
              analysisType: user.role === UserRole.MEDIATOR ? AnalysisType.KYP : AnalysisType.KYC,
              domainId: user.domain.id,
              taxId: user.consumer.taxId,
              provider: assetIdentity.analysisProvider,
            },
          });
        }

        if (reports.length) {
          report = reports[0];
          this.probe.logReportAlreadyExists(assetRegistration, assetIdentity.analysisProvider, report.id);
        } else {
          report = await reportCreationMethod({
            domainId: user.domain.id,
            analysisType: user.role === UserRole.MEDIATOR ? AnalysisType.KYP : AnalysisType.KYC,
            provider: assetIdentity.analysisProvider,
            data: this.formatUser(user),
            userId: user.id,
          });

          this.probe.logReportCreation(assetRegistration, assetIdentity.analysisProvider, report.id);
        }
      } catch (err) {
        if (err.details?.status === 400) {
          this.probe.logReportRejected(assetRegistration, assetIdentity.analysisProvider, err);
          await fsm.goTo(AssetRegistrationStatus.REJECTED, { additionalData: { rejectionReason: err.details } });
        } else {
          this.probe.logReportCreationFailed(assetRegistration, assetIdentity.analysisProvider, err);
          await fsm.goTo(AssetRegistrationStatus.FAILED, { additionalData: { exception: err.details || err } });
        }
      }

      if (report) {
        await fsm.goTo(AssetRegistrationStatus.PENDING_DOCUMENTS, { reportId: report.id });

        if (report.status === ReportStatus.ACCEPTED) {
          events.push(new ApproveAssetEvent(assetRegistration.id));
        }
      }
    } else {
      this.probe.logCustomAssetRegistered(assetRegistration);
      events.push(new ApproveAssetEvent(assetRegistration.id));
    }

    return events;
  }

  public async registerAssets(
    wallet: Wallet,
    assetConfigs: AssetConfig[],
    manager: EntityManager,
  ): Promise<BaseEvent[]> {
    const assetRepository = new AssetRepository(manager);
    const assets = await assetRepository.getAssetsByCode(assetConfigs.map(config => config.code));
    const events: BaseEvent[] = [];

    const exceptions: any[] = [];
    wallet.assetRegistrations = wallet.assetRegistrations ?? [];

    for (const asset of assets) {
      const assetRegistration = await this.create(wallet, asset, manager);

      const assetEvents = await this.createReport(assetRegistration, manager);
      events.push(...assetEvents);

      const assetRegistrationStatus = assetRegistration.status;

      if (asset.required) {
        const lastStateAdditionalData = assetRegistration.getStates()[0].additionalData;

        if (assetRegistrationStatus === AssetRegistrationStatus.REJECTED) {
          exceptions.push(lastStateAdditionalData.rejectionReason);
        } else if (assetRegistrationStatus === AssetRegistrationStatus.FAILED) {
          exceptions.push(lastStateAdditionalData.exception);
        }
      }
    }

    if (exceptions.length) {
      const errors = exceptions.flatMap(err => err?.details?.errors || err.toJSON?.() || err);
      throw new HttpError('An error occurred during user creation', HttpCode.Client.BAD_REQUEST, { errors });
    }

    return events;
  }

  public async registerAssetsForRootMediator(
    wallet: Wallet,
    assetConfigs: AssetConfig[],
    manager: EntityManager,
  ): Promise<void> {
    const assetRepository = new AssetRepository(manager);
    const assets = await assetRepository.getAssetsByCode(assetConfigs.map(config => config.code));

    wallet.assetRegistrations = wallet.assetRegistrations ?? [];

    for (const asset of assets) {
      await this.create(wallet, asset, manager);
    }
  }

  /**
   * Chooses an analysisProvider for a report based on a list of candidate analysisProviders and the user's domainId.
   *
   * It is possible to query the identity-api for a list of associations between assets and analysis-providers, so
   * that we can choose a valid analysis-provider for a given asset registration. The business rules for choosing an
   * analysis-provider is as follows:
   *  - Query identity-api for all analysis-providers related to a given asset code.
   *  - Search the list for an asset-provider associated with the user's domainId
   *  - If there is no provider associated with the user's domain, default to the provider with null domainId
   *  - If there is no provider with null domainId then there is no applicable analysis provider.
   *
   * It was decided to keep the identity-api endpoint for querying the analysis-providers as a simple REST list action
   * in order to make it more flexible and not couple it with this business-rule which is specific to the  -api,
   * therefore this method implements this logic.
   *
   * @param candidates The list of candidate analysis-providers
   * @param domainId The user's domainId
   */
  public async chooseAssetIdentity(asset: Asset, domainId: string): Promise<AssetIdentitySchema | undefined> {
    const candidates = await this.identityService.providers().list({ asset: asset.code });

    const candidateWithMatchingDomainId = candidates.find(candidate => candidate.domainId === domainId);
    const candidateWithNullDomainId = candidates.find(candidate => candidate.domainId == null);

    return candidateWithMatchingDomainId || candidateWithNullDomainId;
  }

  private formatUser(user: User): BaseUserData {
    const clonedUser = cloneDeep(user);

    // identity expects birthday on YYYY-MM-DD format
    if (clonedUser.consumer?.birthday) {
      const birthday = moment(clonedUser.consumer.birthday).format('YYYY-MM-DD');
      clonedUser.consumer.birthday = birthday as any;
    }

    return clonedUser as BaseUserData;
  }
}

