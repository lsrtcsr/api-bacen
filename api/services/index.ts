import IdentityService from '@bacen/identity-sdk';
import { LoggerInstance } from 'nano-errors';
import { AssetRegistrationService } from './AssetRegistrationService';
import { AssetService } from './AssetService';
import * as BigDataCorp from './bigdatacorp';
import { UserService } from './UserService';
import { WalletService } from './WalletService';

/**
 * initializes the following services:
 *  - AssetRegistrationService
 *  - WalletService,
 *  - UserService
 *
 * @param logger the logger instance
 */
export function configure(logger: LoggerInstance, assetService: AssetService, identityService: IdentityService): void {
  AssetRegistrationService.initialize(logger, identityService);
  WalletService.initialize(logger, assetService, AssetRegistrationService.getInstance());
  UserService.initialize(logger, WalletService.getInstance());
}

export * from './LimitSettingService';
export { AMQPServiceOptions, default as AMQPService } from './amqp';
export * from './billing';
export * from './identity';
export * from './cache';
export * from './discovery';
export * from './notifications';
export * from './pipeline';
export * from './postback';
export * from './provider';
export * from './pipeline';
export * from './authorization';
export * from './wallet';
export * from './issue';
export * from './metrics';
export * from './conciliation';
export * from './AssetService';
export * from './SignerService';
export * from './DocumentService';
export * from './locks';
export * from './RequestLogService';
export * from './EmailTemplateService';
export * from './idempotence';
export * from './SPBParticipantsService';
export * from './AssetRegistrationService';
export * from './WalletService';
export * from './UserService';

export { default as StorageService } from './storageservice';
export { BigDataCorp };
