import { LoggerInstance } from 'nano-errors';
import { StellarWalletData, WalletStatus } from '@bacen/base-sdk';
import { getManager, EntityManager } from 'typeorm';
import { BaseEvent } from '@bacen/events';
import { User, Wallet } from '../models';
import { WalletRepository } from '../repositories/WalletRepository';
import { AssetService } from './AssetService';
import { IAssetRegistrationService } from './AssetRegistrationService';
import { UninitializedServiceError } from '../errors';
import { RootMediatorWalletCreatedEvent, WalletCreatedEvent } from '../events';

export interface CreateInitialWalletOptions {
  user: User;

  stellar?: StellarWalletData;

  walletAdditionalData?: any;

  stateAdditionalData?: any;

  manager?: EntityManager;
}

export class WalletService {
  private static instance: WalletService | undefined;

  constructor(
    private readonly logger: LoggerInstance,
    private readonly assetService: AssetService,
    private readonly assetRegistrationService: IAssetRegistrationService,
  ) {}

  public static initialize(
    logger: LoggerInstance,
    assetService: AssetService,
    assetRegistrationService: IAssetRegistrationService,
  ): WalletService {
    return (this.instance = new WalletService(logger, assetService, assetRegistrationService));
  }

  public static getInstance(): WalletService {
    if (!this.instance) {
      throw new UninitializedServiceError(this.name);
    }

    return this.instance;
  }

  public getFingerprintFromWalletState(wallet: Wallet): string {
    const pendingState = wallet.states.find(state => state.status === WalletStatus.PENDING);

    return pendingState.additionalData.fingerprint;
  }

  public async createInitialWallet(options: CreateInitialWalletOptions): Promise<[Wallet, BaseEvent[]]> {
    const { user, walletAdditionalData, stateAdditionalData = {}, stellar, manager = getManager() } = options;

    const walletRepo = new WalletRepository(manager);
    const wallet = await walletRepo.create({ user, stellar, additionalData: walletAdditionalData });

    if (!stateAdditionalData?.fingerprint) {
      Object.assign(stateAdditionalData, {
        fingerprint: ' -api#127.0.0.1', // TODO Revisar, talvez criar uma Config pra isso
      });
    }

    await walletRepo.appendState(wallet, {
      status: WalletStatus.PENDING,
      additionalData: stateAdditionalData,
    });

    const initialAssets = this.assetService.allInitialAssets;
    const assetRegistrationEvents = await this.assetRegistrationService.registerAssets(wallet, initialAssets, manager);

    const walletEvent = new WalletCreatedEvent(wallet.id);
    return [wallet, [walletEvent, ...assetRegistrationEvents]];
  }

  public async createRootMediatorInitialWallet(options: CreateInitialWalletOptions): Promise<Wallet> {
    const { user, walletAdditionalData, stateAdditionalData, stellar, manager = getManager() } = options;

    const walletRepo = new WalletRepository(manager);
    const wallet = await walletRepo.create({ user, stellar, additionalData: walletAdditionalData });

    const initialAssets = this.assetService.allInitialAssets;
    await this.assetRegistrationService.registerAssetsForRootMediator(wallet, initialAssets, manager);

    await walletRepo.appendState(wallet, {
      status: WalletStatus.PENDING,
      additionalData: stateAdditionalData,
    });

    const rootMediatorEvent = new RootMediatorWalletCreatedEvent(wallet.id);
    await rootMediatorEvent.publish();

    const walletCreatedEvent = new WalletCreatedEvent(wallet.id);
    await walletCreatedEvent.publish();

    return wallet;
  }
}
