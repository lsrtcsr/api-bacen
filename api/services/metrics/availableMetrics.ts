export enum AvailableMetrics {
  /* ----- Database ----- */
  PG_TOTAL_CONNECTIONS = 'pg_pool_total_connections',
  PG_IDLE_CONNECTIONS = 'pg_pool_idle_connections',
  PG_WAITING_CONNECTIONS = 'pg_pool_waiting_connections',
}
