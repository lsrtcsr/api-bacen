import { Service, ServiceOptions } from 'ts-framework-common';
import { getConnection } from 'typeorm';
import { CustodyFeature } from '@bacen/base-sdk';
import { PaymentLog, Asset } from '../../models';
import { InternalServerError } from '../../errors';
import { ProviderUtil } from '../../utils';
import { ProviderManagerService } from '../provider';

export interface UserMetricsServiceOptions extends ServiceOptions {}

export default class UserMetricsService extends Service implements UserMetricsServiceOptions {
  protected static instance: UserMetricsService;

  public async getMobileCreditOrderAmountForWalletToday(walletId: string, asset: Asset) {
    try {
      const provider = ProviderManagerService.getInstance().from(asset.provider);
      const phoneChargeFeature = provider.feature(CustodyFeature.PHONE_CREDITS);
      await ProviderUtil.throwIfFeatureNotAvailable(phoneChargeFeature);
      const response = await phoneChargeFeature.getOrderAmountForWalletToday(walletId);
      return response.value;
    } catch (err) {
      throw new InternalServerError('Failed getting mobile credit order amount');
    }
  }

  public async getPaymentAmountForUserAndAssetToday(walletId: string, asset: Asset) {
    return this.getPaymentAmountForInterval({ walletId, asset, interval: 'day' });
  }

  public async getPaymentAmountForUserAndAssetThisMonth(walletId: string, asset: Asset) {
    return this.getPaymentAmountForInterval({ walletId, asset, interval: 'month' });
  }

  public async getPaymentAmountForInterval(options: {
    interval: 'day' | 'month';
    walletId: string;
    asset: Asset;
  }): Promise<number> {
    const { interval, walletId, asset } = options;
    const queryRunner = getConnection('timescale').createQueryRunner();
    let result: { total_amount: string };
    try {
      if (interval === 'day') {
        result = await queryRunner.query(
          `
  SELECT SUM(amount::NUMERIC) AS total_amount
  FROM payment
  WHERE "source" = $1
    AND "asset" = $2
    AND time BETWEEN TIMESTAMP 'today' AND TIMESTAMP 'tomorrow'
  `,
          [walletId, asset.code],
        );
      } else if (interval === 'month') {
        result = await queryRunner.query(
          `
  SELECT SUM(amount::NUMERIC) AS total_amount
  FROM payment
  WHERE "source" = $1
    AND "asset" = $2
    AND time BETWEEN TIMESTAMP 'today' - INTERVAL '1 month' AND TIMESTAMP 'tomorrow'
  `,
          [walletId, asset.code],
        );
      }
      return Number(result[0].total_amount) ?? 0;
    } finally {
      await queryRunner.release();
    }
  }

  constructor(options: UserMetricsServiceOptions = {}) {
    super(options);
  }

  static initialize(options: UserMetricsServiceOptions = {}) {
    this.instance = new UserMetricsService(options);
  }

  static getInstance() {
    return this.instance;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
