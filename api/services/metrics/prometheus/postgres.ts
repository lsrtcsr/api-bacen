import { Pool } from 'pg';
import { Gauge, Registry } from 'prom-client';
import { getConnection } from 'typeorm';
import { PostgresDriver } from 'typeorm/driver/postgres/PostgresDriver';
import Config from '../../../../config';
import { AvailableMetrics } from '../availableMetrics';

function getConnectionPool(): Pool[] {
  const driver = getConnection().driver as PostgresDriver;
  return [driver.master, ...(driver.slaves ?? [])];
}

export const postgresPoolTotalConnectionCount = (registry: Registry) =>
  registry.registerMetric(
    new Gauge({
      name: `${Config.metrics.prefix}${AvailableMetrics.PG_TOTAL_CONNECTIONS}`,
      help: 'The total number of connections on the postgres connection pool',
      registers: [],
      collect() {
        const pools = getConnectionPool();
        const totalConnectionCount = pools.reduce((acc, pool) => acc + pool.totalCount, 0);
        this.set(totalConnectionCount);
      },
    }),
  );

export const postgresPoolIdleConnectionCount = (registry: Registry) =>
  registry.registerMetric(
    new Gauge({
      name: `${Config.metrics.prefix}${AvailableMetrics.PG_IDLE_CONNECTIONS}`,
      help: 'The number of idle connections on the postgres connection pool',
      registers: [],
      collect() {
        const pools = getConnectionPool();
        const idleConnectionCount = pools.reduce((acc, pool) => acc + pool.idleCount, 0);
        this.set(idleConnectionCount);
      },
    }),
  );

export const postgresPoolWaitingConnectionCount = (registry: Registry) =>
  registry.registerMetric(
    new Gauge({
      name: `${Config.metrics.prefix}${AvailableMetrics.PG_WAITING_CONNECTIONS}`,
      help: 'The number of waiting connections on the postgres connection pool',
      registers: [],
      collect() {
        const pools = getConnectionPool();
        const waitingConnectionCount = pools.reduce((acc, pool) => acc + pool.waitingCount, 0);
        this.set(waitingConnectionCount);
      },
    }),
  );
