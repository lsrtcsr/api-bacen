import { BaseMetricsService, BaseMetricsServiceOptions } from '@bacen/monitoring-tools';
import { BaseError } from 'nano-errors';
import { AvailableMetrics } from './availableMetrics';

export interface MetricsServiceOptions extends BaseMetricsServiceOptions {}

export default class MetricsService extends BaseMetricsService<AvailableMetrics> {
  protected static instance: MetricsService;

  constructor(options: MetricsServiceOptions = {}) {
    super(options);
  }

  static initialize(options: MetricsServiceOptions = {}): MetricsService {
    this.instance = new MetricsService(options);
    return this.instance;
  }

  static getInstance(): MetricsService {
    if (!this.instance) throw new BaseError('Metrics service was not initialized yet');
    return this.instance;
  }

  async onReady(server) {
    await super.onReady(server);

    // Will use push gateway for workers without a listening port
    if (!server['server'] && this.gateway) {
      // TODO
      // https://github.com/siimon/prom-client/issues/390
      // await promisify(this.gateway.push.bind(this.gateway))({ jobName: this.jobName });
      this.logger?.warn(
        'Pushing metrics to gateway is disabled because of a prom-client bug, check official issue #390',
      );
    } else if (this.register) {
      // Register main server metrics asynchronously
      const prometheus = await import('./prometheus');
      Object.values(prometheus).map(metric => metric(this.register));
    }
  }
}
