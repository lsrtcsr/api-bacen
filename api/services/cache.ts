import * as Redis from 'redis';
import { BaseError, Logger, Service, LoggerInstance, ServiceOptions } from 'ts-framework-common';

export interface CacheServiceOptions extends Redis.ClientOpts, ServiceOptions {
  verbose?: boolean;
}

export class CacheService extends Service {
  private static instance: CacheService;

  private client: Redis.RedisClient;

  private isReady: boolean = false;

  onMount(server): void {}

  onUnmount(server): void {
    this.client && this.client.quit();
  }

  async onInit(server): Promise<void> {}

  async onReady(server): Promise<void> {}

  /* Default cache expiration in seconds */
  public static DEFAULT_EXPIRES = 15 * 60; // 15min

  /**
   * Initializes the Cache service.
   */
  constructor(public readonly options: CacheServiceOptions) {
    super({});
    const { verbose = false, ...clientOpts } = options;
    this.logger = Logger.getInstance();
    this.logger.info('Initializing Redis cache service...', clientOpts);
    this.client = Redis.createClient(clientOpts);

    // Bind client events
    this.client.on('error', this.handleError.bind(this));
    this.client.on('ready', this.handleReady.bind(this));
  }

  /**
   * Stores a value in the cache.
   *
   * @param key The identifier of the cache
   * @param value The value that will be stored on the cache
   * @param expiresIn The number of seconds the value will be stored before expiring, set to -1 if you want the cache entry to not expire
   */
  public async set(key: Readonly<string>, value: any, expiresIn?: number): Promise<void> {
    // Ensure value is a string
    const v: Readonly<string> = typeof value !== typeof 'a' ? JSON.stringify(value) : value;

    // Store in cache
    if (expiresIn === -1) {
      return new Promise<void>((resolve, reject) =>
        this.client.set(key, v, (err, reply) => {
          if (err) reject(err);

          resolve();
        }),
      );
    }

    return new Promise<void>((resolve, reject) =>
      this.client.set(key, v, 'EX', expiresIn || CacheService.DEFAULT_EXPIRES, (err, reply) => {
        if (err) reject(err);

        resolve();
      }),
    );
  }

  /**
   * Stores a value of a list in the cache.
   *
   * @param key The identifier of the cache
   * @param value The value that will be stored on the cache
   * @param expiresIn The number of seconds the value will be stored before expiring, set to -1 if you want the cache entry to not expire
   */
  public async push(key: Readonly<string>, value: any, expiresIn?: number): Promise<void> {
    // Ensure value is a string
    const v: Readonly<string> = typeof value !== typeof 'a' ? JSON.stringify(value) : value;

    // Store in cache
    return new Promise<void>((resolve, reject) =>
      this.client.rpush(key, v, (err, reply) => {
        if (err) reject(err);

        resolve();
      }),
    );
  }

  /**
   * Retrieves a value of a list from the cache.
   *
   * @param key The identifier of the cache
   */
  public async lpop(key: Readonly<string>): Promise<any> {
    return new Promise<string>((resolve, reject) =>
      this.client.lpop(key, (err, result: string) => {
        if (err) return reject(err);
        try {
          if (!result && this.options.verbose) this.logger.debug(`CacheService: GET ${key} Cache miss`);

          const parsedResult = JSON.parse(result);
          if (this.options.verbose) {
            this.logger.debug(`CacheService: POP ${key} hit`, parsedResult);
          }
          resolve(parsedResult);
        } catch (e) {
          // TODO: Handle properly
          resolve(result);
        }
      }),
    );
  }

  /**
   * Perform a list of argumens.
   *
   * @param args Redis arguments
   */
  public async multi(args: any[]): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.client.multi(args).exec((err, result: [any]) => {
        if (err) {
          return reject(false);
        }
        resolve(result);
      });
    });
  }

  /**
   * Round a list of values.
   *
   * @param key The identifier of the cache
   */
  public async round(key: Readonly<string>): Promise<any> {
    return new Promise<string>((resolve, reject) =>
      this.client.rpoplpush(key, key, (err, result: string) => {
        if (err) return reject(err);
        try {
          if (!result && this.options.verbose) this.logger.debug(`CacheService: GET ${key} Cache miss`);

          const parsedResult = JSON.parse(result);
          if (this.options.verbose) {
            this.logger.debug(`CacheService: ROUND ${key} hit`, parsedResult);
          }
          resolve(parsedResult);
        } catch (e) {
          // TODO: Handle properly
          resolve(result);
        }
      }),
    );
  }

  /**
   * Retrieves a value from the cache.
   *
   * @param key The identifier of the cache
   */
  public async get(key: Readonly<string>): Promise<any> {
    return new Promise<string>((resolve, reject) =>
      this.client.get(key, (err, result: string) => {
        if (err) return reject(err);
        try {
          if (!result && this.options.verbose) this.logger.debug(`CacheService: GET ${key} Cache miss`);

          const parsedResult = JSON.parse(result);
          if (this.options.verbose) {
            this.logger.debug(`CacheService: GET ${key} hit`, parsedResult);
          }
          resolve(parsedResult);
        } catch (e) {
          // TODO: Handle properly
          resolve(result);
        }
      }),
    );
  }

  /**
   * Deletes a value from the cache.
   *
   * @param key The identifier of the cache
   */
  public async del(key: Readonly<string>): Promise<number> {
    return new Promise<number>((resolve, reject) =>
      this.client.del(key, (err, result: number) => {
        if (err) return reject(err);
        resolve(result);
      }),
    );
  }

  /**
   * @description Deletes data fro redis cache
   * @param keys array of strings as Redis server keys
   */
  public async removeCache(keys: string[]): Promise<number> {
    try {
      if (!keys || !keys.length) {
        throw new BaseError('Trying to remove invalid cache key.');
      }

      return await Promise.all(keys.filter(key => typeof key === 'string').map(key => this.del(key)))
        .then(result => result as any)
        .catch(e => {
          throw new BaseError(e.message, e);
        });
    } catch (e) {
      throw new BaseError(e.message, e);
    }
  }

  /**
   * The event handler when the Redis client has an error
   * @param err The error object
   */
  protected handleError(err: any): void {
    this.isReady = false;
    Logger.getInstance().error(`Cache error: `, err);
  }

  /**
   * The event handler when the Redis client is ready
   */
  protected handleReady(): void {
    this.isReady = true;
    Logger.getInstance().info(`Successfully connected to Redis cache database`);
  }

  public static initialize(clientOpts: Redis.ClientOpts): CacheService {
    this.instance = new CacheService(clientOpts);
    return this.instance;
  }

  /**
   * Gets the singleton cache service.
   */
  public static getInstance(): CacheService {
    if (!CacheService.instance) {
      throw new Error('Cache service has not been initialized yet');
    }
    return CacheService.instance;
  }

  public async quit(): Promise<void> {
    return new Promise((ok, fail) => {
      this.client.quit((err, res) => {
        if (err) fail(err);

        ok();
      });
    });
  }
}
