import AMQP, { AMQPServiceOptions } from 'ts-framework-queue';
import { BaseError } from 'ts-framework-common';

export { AMQPServiceOptions };

export default class AMQPService extends AMQP<any> {
  protected static instance: AMQPService;

  public static initialize(options: AMQPServiceOptions): AMQPService {
    this.instance = new AMQPService({
      ...options,
    });
    return this.instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }
}
