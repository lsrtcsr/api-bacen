import { BaseError } from 'ts-framework-common';
import { User } from '../../../models';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../subscriber';
import {
  ConsumerPipelinePayload,
  ConsumerPipelineRoutes,
  ConsumerPipelineSubscription,
  CONSUMER_PIPELINE_EXCHANGE,
  CONSUMER_PIPELINE_QUEUE,
} from './defaults';

export interface ConsumerPipelineSubscriberOptions extends BasePipelineSubscriberOptions<ConsumerPipelinePayload> {}

export class ConsumerPipelineSubscriber extends BasePipelineSubscriber<
  User,
  ConsumerPipelinePayload,
  ConsumerPipelineSubscription
> {
  protected static instance: ConsumerPipelineSubscriber;

  public options: ConsumerPipelineSubscriberOptions;

  public name = CONSUMER_PIPELINE_EXCHANGE;

  public queue = {
    name: CONSUMER_PIPELINE_QUEUE,
    routes: [ConsumerPipelineRoutes.DEFAULT],
  };

  constructor(options: ConsumerPipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: ConsumerPipelineSubscriberOptions): ConsumerPipelineSubscriber {
    const instance = new ConsumerPipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Consumer pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: User): Promise<ConsumerPipelinePayload> {
    if (data) {
      return { user: data.id, status: data.consumer.status };
    }
    this.logger.warn('Consumer pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: ConsumerPipelinePayload): Promise<User> {
    return User.findOne({
      where: { id: payload.user },
      relations: ['domain', 'states', 'consumer', 'consumer.states', 'wallets', 'wallets.states'],
    });
  }
}
