import { AMQPMessage } from 'ts-framework-queue';
import { User } from '../../../models';
import { PipelineSubscriberActions } from '../subscriber';

export const CONSUMER_PIPELINE_QUEUE = 'consumer_pipeline';
export const CONSUMER_PIPELINE_EXCHANGE = 'consumer_pipeline_exc';

export interface ConsumerPipelinePayload {
  user: string;
  status: string;
}

export type ConsumerPipelineSubscription = (
  user: User,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<User, ConsumerPipelinePayload>,
) => Promise<void>;

export enum ConsumerPipelineRoutes {
  DEFAULT = 'default',
}
