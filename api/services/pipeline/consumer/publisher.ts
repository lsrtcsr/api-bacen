import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { UserRole } from '@bacen/base-sdk';
import { User } from '../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../publisher';
import {
  ConsumerPipelinePayload,
  ConsumerPipelineRoutes,
  CONSUMER_PIPELINE_QUEUE,
  CONSUMER_PIPELINE_EXCHANGE,
} from './defaults';

export interface ConsumerPipelinePublisherOptions extends BasePipelinePublisherOptions<ConsumerPipelinePayload> {}

export class ConsumerPipelinePublisher extends BasePipelinePublisher<User, ConsumerPipelinePayload> {
  protected static instance: ConsumerPipelinePublisher;

  public options: ConsumerPipelinePublisherOptions;

  public name = CONSUMER_PIPELINE_EXCHANGE;

  public queue = {
    name: CONSUMER_PIPELINE_QUEUE,
    routes: [ConsumerPipelineRoutes.DEFAULT],
  };

  constructor(options: ConsumerPipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: ConsumerPipelinePublisherOptions): ConsumerPipelinePublisher {
    const instance = new ConsumerPipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Consumer pipeline publisher service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: User): Promise<ConsumerPipelinePayload> {
    return { user: data.id, status: data.consumer?.status };
  }

  /**
   * Push new consumer to the queue.
   * The domain is required.
   */
  async send(user: User, options?: AMQPOptions.Publish): Promise<boolean> {
    if (user.role !== UserRole.CONSUMER) {
      throw new BaseError('Could not push consumer to queue, invalid information', { user });
    }

    // Push consumer with specific domain test information
    return super.publish(ConsumerPipelineRoutes.DEFAULT, user, { ...options });
  }
}
