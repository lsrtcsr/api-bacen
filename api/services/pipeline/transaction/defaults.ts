import { AMQPMessage } from 'ts-framework-queue';
import { Transaction } from '../../../models';
import { PipelineSubscriberActions } from '../subscriber';

export const TRANSACTION_PIPELINE_QUEUE = 'transaction_pipeline';
export const TRANSACTION_PIPELINE_EXCHANGE = 'transaction_pipeline_exc';

export interface TransactionPipelinePayload {
  transaction: string;
  status: string;
}

export type TransactionPipelineSubscription = (
  transaction: Transaction,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<Transaction, TransactionPipelinePayload>,
) => Promise<void>;

export enum TransactionPipelineRoutes {
  DEFAULT = 'default',
}
