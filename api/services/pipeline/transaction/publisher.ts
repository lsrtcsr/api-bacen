import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { Transaction } from '../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../publisher';
import {
  TransactionPipelinePayload,
  TransactionPipelineRoutes,
  TRANSACTION_PIPELINE_QUEUE,
  TRANSACTION_PIPELINE_EXCHANGE,
} from './defaults';

export interface TransactionPipelinePublisherOptions extends BasePipelinePublisherOptions<TransactionPipelinePayload> {}
export class TransactionPipelinePublisher extends BasePipelinePublisher<Transaction, TransactionPipelinePayload> {
  protected static instance: TransactionPipelinePublisher;

  public options: TransactionPipelinePublisherOptions;

  public name = TRANSACTION_PIPELINE_EXCHANGE;

  public queue = {
    name: TRANSACTION_PIPELINE_QUEUE,
    routes: [TransactionPipelineRoutes.DEFAULT],
  };

  constructor(options: TransactionPipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: TransactionPipelinePublisherOptions): TransactionPipelinePublisher {
    const instance = new TransactionPipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Transaction pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Transaction): Promise<TransactionPipelinePayload> {
    return { transaction: data.id, status: data.status };
  }

  /**
   * Push new transaction to the queue.
   */
  async send(data: Transaction, options?: AMQPOptions.Publish): Promise<boolean> {
    return super.publish(TransactionPipelineRoutes.DEFAULT, data, { ...options });
  }
}
