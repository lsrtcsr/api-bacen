import { BaseError } from 'ts-framework-common';
import { Transaction } from '../../../models';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../subscriber';
import {
  TransactionPipelinePayload,
  TransactionPipelineRoutes,
  TransactionPipelineSubscription,
  TRANSACTION_PIPELINE_EXCHANGE,
  TRANSACTION_PIPELINE_QUEUE,
} from './defaults';

export interface TransactionPipelineSubscriberOptions
  extends BasePipelineSubscriberOptions<TransactionPipelinePayload> {}

export class TransactionPipelineSubscriber extends BasePipelineSubscriber<
  Transaction,
  TransactionPipelinePayload,
  TransactionPipelineSubscription
> {
  protected static instance: TransactionPipelineSubscriber;

  protected;

  public options: TransactionPipelineSubscriberOptions;

  public name = TRANSACTION_PIPELINE_EXCHANGE;

  public queue = {
    name: TRANSACTION_PIPELINE_QUEUE,
    routes: [TransactionPipelineRoutes.DEFAULT],
  };

  constructor(options: TransactionPipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: TransactionPipelineSubscriberOptions): TransactionPipelineSubscriber {
    const instance = new TransactionPipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Transaction pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Transaction): Promise<TransactionPipelinePayload> {
    if (data) {
      return { transaction: data.id, status: data.status };
    }
    this.logger.warn('Transaction pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: TransactionPipelinePayload): Promise<Transaction> {
    return Transaction.findOne({
      where: { id: payload.transaction },
      relations: ['source', 'source.user', 'source.user.domain', 'payments', 'states'],
    });
  }
}
