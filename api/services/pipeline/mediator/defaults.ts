import { AMQPMessage } from 'ts-framework-queue';
import { User } from '../../../models';
import { PipelineSubscriberActions } from '../subscriber';

export const MEDIATOR_PIPELINE_QUEUE = 'mediator_pipeline';
export const MEDIATOR_PIPELINE_EXCHANGE = 'mediator_pipeline_exc';

export interface MediatorPipelinePayload {
  user: string;
  status: string;
}

export type MediatorPipelineSubscription = (
  user: User,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<User, MediatorPipelinePayload>,
) => Promise<void>;

export enum MediatorPipelineRoutes {
  DEFAULT = 'default',
}
