import { BaseError } from 'ts-framework-common';
import { User } from '../../../models';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../subscriber';
import {
  MediatorPipelinePayload,
  MediatorPipelineRoutes,
  MediatorPipelineSubscription,
  MEDIATOR_PIPELINE_EXCHANGE,
  MEDIATOR_PIPELINE_QUEUE,
} from './defaults';

export interface MediatorPipelineSubscriberOptions extends BasePipelineSubscriberOptions<MediatorPipelinePayload> {}

export class MediatorPipelineSubscriber extends BasePipelineSubscriber<
  User,
  MediatorPipelinePayload,
  MediatorPipelineSubscription
> {
  protected static instance: MediatorPipelineSubscriber;

  public options: MediatorPipelineSubscriberOptions;

  public name = MEDIATOR_PIPELINE_EXCHANGE;

  public queue = {
    name: MEDIATOR_PIPELINE_QUEUE,
    routes: [MediatorPipelineRoutes.DEFAULT],
  };

  constructor(options: MediatorPipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: MediatorPipelineSubscriberOptions): MediatorPipelineSubscriber {
    const instance = new MediatorPipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Mediator pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: User): Promise<MediatorPipelinePayload> {
    if (data) {
      return { user: data.id, status: data.status };
    }
    this.logger.warn('Mediator pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: MediatorPipelinePayload): Promise<User> {
    return User.findOne({
      where: { id: payload.user },
      relations: ['domain', 'states', 'consumer', 'consumer.states', 'wallets', 'wallets.states'],
    });
  }
}
