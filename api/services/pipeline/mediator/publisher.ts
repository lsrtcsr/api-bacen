import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { UserRole } from '@bacen/base-sdk';
import { User } from '../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../publisher';
import {
  MediatorPipelinePayload,
  MediatorPipelineRoutes,
  MEDIATOR_PIPELINE_QUEUE,
  MEDIATOR_PIPELINE_EXCHANGE,
} from './defaults';

export interface MediatorPipelinePublisherOptions extends BasePipelinePublisherOptions<MediatorPipelinePayload> {}

export class MediatorPipelinePublisher extends BasePipelinePublisher<User, MediatorPipelinePayload> {
  protected static instance: MediatorPipelinePublisher;

  public options: MediatorPipelinePublisherOptions;

  public name = MEDIATOR_PIPELINE_EXCHANGE;

  public queue = {
    name: MEDIATOR_PIPELINE_QUEUE,
    routes: [MediatorPipelineRoutes.DEFAULT],
  };

  constructor(options: MediatorPipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: MediatorPipelinePublisherOptions): MediatorPipelinePublisher {
    const instance = new MediatorPipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Mediator pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: User): Promise<MediatorPipelinePayload> {
    return { user: data.id, status: data.status };
  }

  /**
   * Push new Mediator to the queue.
   */
  async send(user: User, options?: AMQPOptions.Publish): Promise<boolean> {
    if (!user || user.role !== UserRole.MEDIATOR) {
      throw new BaseError('Could not push mediator to queue, invalid information', { user });
    }

    // Push mediator with specific domain test information
    return super.publish(MediatorPipelineRoutes.DEFAULT, user, { ...options });
  }
}
