import { BaseError } from 'ts-framework-common';
import { PostbackDelivery } from '../../../models';
import BasePostbackSubscriber, { BasePipelineSubscriberOptions } from '../subscriber';
import {
  PostbackDeliveryRoutes,
  PostbackDeliverySubscription,
  POSTBACK_DELIVERY_EXCHANGE,
  POSTBACK_DELIVERY_QUEUE,
} from './defaults';

export class PostbackDeliverySubscriber extends BasePostbackSubscriber<
  PostbackDelivery,
  { id: string },
  PostbackDeliverySubscription
> {
  protected static instance: PostbackDeliverySubscriber;

  public options: BasePipelineSubscriberOptions<{ id: string }>;

  public name = POSTBACK_DELIVERY_EXCHANGE;

  public queue = {
    name: POSTBACK_DELIVERY_QUEUE,
    routes: [PostbackDeliveryRoutes.POSTBACK],
  };

  constructor(options: BasePipelineSubscriberOptions<{ id: string }>) {
    super({ ...options });
  }

  public static initialize(options: BasePipelineSubscriberOptions<{ id: string }>): PostbackDeliverySubscriber {
    const instance = new PostbackDeliverySubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Transaction postback subscriber is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: PostbackDelivery): Promise<{ id: string }> {
    return { id: data.id };
  }

  protected async unserialize(payload: { id: string }): Promise<PostbackDelivery> {
    // Get transaction to be handled
    return PostbackDelivery.findOne({
      where: { id: payload.id },
      relations: ['postback'],
    });
  }
}
