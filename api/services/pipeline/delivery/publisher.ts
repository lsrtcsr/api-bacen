import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { PostbackDelivery } from '../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../publisher';
import { PostbackDeliveryRoutes, POSTBACK_DELIVERY_EXCHANGE, POSTBACK_DELIVERY_QUEUE } from './defaults';

export class PostbackDeliveryPublisher extends BasePipelinePublisher<PostbackDelivery, { id: string }> {
  protected static instance: PostbackDeliveryPublisher;

  public options: BasePipelinePublisherOptions<{ id: string }>;

  public name = POSTBACK_DELIVERY_EXCHANGE;

  public queue = {
    name: POSTBACK_DELIVERY_QUEUE,
    routes: [PostbackDeliveryRoutes.POSTBACK],
  };

  constructor(options: BasePipelinePublisherOptions<{ id: string }>) {
    super({ ...options });
  }

  public static initialize(options: BasePipelinePublisherOptions<{ id: string }>): PostbackDeliveryPublisher {
    const instance = new PostbackDeliveryPublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Transaction postback publisher is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: PostbackDelivery): Promise<{ id: string }> {
    return { id: data.id };
  }

  /**
   * Push new transaction to the postback queue.
   */
  async send(data: PostbackDelivery, options?: AMQPOptions.Publish): Promise<boolean> {
    return super.publish(PostbackDeliveryRoutes.POSTBACK, data, { ...options });
  }
}
