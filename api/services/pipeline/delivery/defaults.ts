import { AMQPMessage } from 'ts-framework-queue';
import { PostbackDeliveryJob } from '../../../jobs';
import { PostbackDelivery } from '../../../models';
import { PipelineSubscriberActions } from '../subscriber';

export const POSTBACK_DELIVERY_QUEUE = 'transaction_postback';
export const POSTBACK_DELIVERY_EXCHANGE = 'transaction_postback_exc';

export type PostbackDeliverySubscription = (
  postback: PostbackDelivery,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<PostbackDelivery, PostbackDeliveryJob>,
) => Promise<void>;

export enum PostbackDeliveryRoutes {
  POSTBACK = 'postback',
  DEFAULT = 'DEFAULT',
}
