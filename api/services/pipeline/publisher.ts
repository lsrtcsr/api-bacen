import { Service, ServiceOptions } from 'ts-framework-common';
import AMQP, { AMQPOptions, Channel, Exchange, QueueInformation } from 'ts-framework-queue';
import AMQPService from '../amqp';

export interface BasePipelinePublisherOptions<Data> extends ServiceOptions {
  amqp: AMQP<Data>;
}

export default abstract class BasePipelinePublisher<Data, Payload> extends Service {
  public abstract name: string;

  public abstract queue: QueueInformation;

  public readonly amqp: AMQP<Payload>;

  public options: BasePipelinePublisherOptions<Payload>;

  public _channel?: Channel<Payload>;

  public _exchange?: Exchange<Payload>;

  constructor(options: BasePipelinePublisherOptions<Payload>) {
    super(options);
    this.amqp = options.amqp || AMQPService.getInstance();
  }

  public async onMount() {}

  public async onInit() {
    this._channel = await this.amqp.channel(this.name, {
      onError: err => {
        this.logger.error('Received error from amqp channel', err);
        process.exit(-1);
      },
    });
    this._exchange = await this._channel.exchange(this.name, {
      prefetch: 1,
      bind: [this.queue],
      exchangeOptions: { durable: true },
    });
  }

  public async onReady() {}

  protected abstract serialize(data: Data): Promise<Payload>;

  protected async publish(route: string, data: Data, options: AMQPOptions.Publish): Promise<boolean> {
    const payload = await this.serialize(data);
    return this._exchange.publish(route, payload, options);
  }

  public async onUnmount() {}
}
