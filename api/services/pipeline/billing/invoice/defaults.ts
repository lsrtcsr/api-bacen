import { AMQPMessage } from 'ts-framework-queue';
import { Invoice } from '../../../../models';
import { PipelineSubscriberActions } from '../../subscriber';

export const INVOICE_PIPELINE_QUEUE = 'invoice_pipeline';
export const INVOICE_PIPELINE_EXCHANGE = 'invoice_pipeline_exc';

export interface InvoicePipelinePayload {
  invoice: string;
  status: string;
}

export type InvoicePipelineSubscription = (
  invoice: Invoice,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<Invoice, InvoicePipelinePayload>,
) => Promise<void>;

export enum InvoicePipelineRoutes {
  DEFAULT = 'default',
}
