import { BaseError } from 'ts-framework-common';
import { Invoice } from '../../../../models';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../../subscriber';
import {
  InvoicePipelinePayload,
  InvoicePipelineRoutes,
  InvoicePipelineSubscription,
  INVOICE_PIPELINE_EXCHANGE,
  INVOICE_PIPELINE_QUEUE,
} from './defaults';

// tslint:disable-next-line:max-line-length
export interface InvoicePipelineSubscriberOptions extends BasePipelineSubscriberOptions<InvoicePipelinePayload> {}

// tslint:disable-next-line:max-line-length
export default class InvoicePipelineSubscriber extends BasePipelineSubscriber<
  Invoice,
  InvoicePipelinePayload,
  InvoicePipelineSubscription
> {
  protected static instance: InvoicePipelineSubscriber;

  protected;

  public options: InvoicePipelineSubscriberOptions;

  public name = INVOICE_PIPELINE_EXCHANGE;

  public queue = {
    name: INVOICE_PIPELINE_QUEUE,
    routes: [InvoicePipelineRoutes.DEFAULT],
  };

  constructor(options: InvoicePipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: InvoicePipelineSubscriberOptions): InvoicePipelineSubscriber {
    const instance = new InvoicePipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Invoice pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Invoice): Promise<InvoicePipelinePayload> {
    if (data) {
      return { invoice: data.id, status: data.status };
    }
    this.logger.warn('Invoice pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: InvoicePipelinePayload): Promise<Invoice> {
    return Invoice.findOne({
      where: { id: payload.invoice },
      relations: ['periods', 'contractor', 'contractor.domain'],
    });
  }
}
