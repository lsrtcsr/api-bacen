import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { Invoice } from '../../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../../publisher';
import {
  InvoicePipelinePayload,
  InvoicePipelineRoutes,
  INVOICE_PIPELINE_QUEUE,
  INVOICE_PIPELINE_EXCHANGE,
} from './defaults';

export interface InvoicePipelinePublisherOptions extends BasePipelinePublisherOptions<InvoicePipelinePayload> {}

export default class InvoicePipelinePublisher extends BasePipelinePublisher<Invoice, InvoicePipelinePayload> {
  protected static instance: InvoicePipelinePublisher;

  public options: InvoicePipelinePublisherOptions;

  public name = INVOICE_PIPELINE_EXCHANGE;

  public queue = {
    name: INVOICE_PIPELINE_QUEUE,
    routes: [InvoicePipelineRoutes.DEFAULT],
  };

  constructor(options: InvoicePipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: InvoicePipelinePublisherOptions): InvoicePipelinePublisher {
    const instance = new InvoicePipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Invoice pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Invoice): Promise<InvoicePipelinePayload> {
    return { invoice: data.id, status: data.status };
  }

  /**
   * Push new consumer to the queue.
   */
  async send(data: Invoice, options?: AMQPOptions.Publish): Promise<boolean> {
    let invoice: Invoice;
    if (data.periods && data.contractor && data.contractor.domain) {
      // If the instance has everything we need, just use it
      invoice = data;
    } else {
      // Get everything we need from the db
      invoice = await Invoice.safeFindOne({
        where: { id: data.id },
        relations: ['periods', 'contractor', 'contractor.domain'],
      });
    }

    if (!invoice || !invoice.periods || !invoice.contractor || !invoice.contractor.domain) {
      throw new BaseError('Could not push invoice to queue, invalid information', { period: data });
    }

    return super.publish(InvoicePipelineRoutes.DEFAULT, invoice, { ...options });
  }
}
