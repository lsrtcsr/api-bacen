import { AMQPMessage } from 'ts-framework-queue';
import { Period } from '../../../../models';
import { PipelineSubscriberActions } from '../../subscriber';

export const PERIOD_PIPELINE_QUEUE = 'period_pipeline';
export const PERIOD_PIPELINE_EXCHANGE = 'period_pipeline_exc';

export interface PeriodPipelinePayload {
  period: string;
  status: string;
}

export type PeriodPipelineSubscription = (
  period: Period,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<Period, PeriodPipelinePayload>,
) => Promise<void>;

export enum PeriodPipelineRoutes {
  DEFAULT = 'default',
}
