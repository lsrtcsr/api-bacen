import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { Period } from '../../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../../publisher';
import {
  PeriodPipelinePayload,
  PeriodPipelineRoutes,
  PERIOD_PIPELINE_QUEUE,
  PERIOD_PIPELINE_EXCHANGE,
} from './defaults';

export interface PeriodPipelinePublisherOptions extends BasePipelinePublisherOptions<PeriodPipelinePayload> {}

export default class PeriodPipelinePublisher extends BasePipelinePublisher<Period, PeriodPipelinePayload> {
  protected static instance: PeriodPipelinePublisher;

  public options: PeriodPipelinePublisherOptions;

  public name = PERIOD_PIPELINE_EXCHANGE;

  public queue = {
    name: PERIOD_PIPELINE_QUEUE,
    routes: [PeriodPipelineRoutes.DEFAULT],
  };

  constructor(options: PeriodPipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: PeriodPipelinePublisherOptions): PeriodPipelinePublisher {
    const instance = new PeriodPipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Period pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Period): Promise<PeriodPipelinePayload> {
    return { period: data.id, status: data.status };
  }

  /**
   * Push new period to the queue.
   */
  async send(data: Period, options?: AMQPOptions.Publish): Promise<boolean> {
    let period: Period;
    if (data.entries && data.invoice && data.invoice.contractor && data.invoice.contractor.domain) {
      // If the instance has everything we need, just use it
      period = data;
    } else {
      // Get everything we need from the db
      period = await Period.safeFindOne({
        where: { id: data.id },
        relations: ['entries', 'invoice', 'invoice.contractor', 'invoice.contractor.domain'],
      });
    }

    if (!period || !period.entries || !period.invoice) {
      throw new BaseError('Could not push period to queue, invalid information', { period: data });
    }

    return super.publish(PeriodPipelineRoutes.DEFAULT, period, { ...options });
  }
}
