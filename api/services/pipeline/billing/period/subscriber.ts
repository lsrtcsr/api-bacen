import { BaseError } from 'ts-framework-common';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../../subscriber';
import {
  PeriodPipelinePayload,
  PeriodPipelineRoutes,
  PeriodPipelineSubscription,
  PERIOD_PIPELINE_EXCHANGE,
  PERIOD_PIPELINE_QUEUE,
} from './defaults';
import { Period } from '../../../../models';

// tslint:disable-next-line:max-line-length
export interface PeriodPipelineSubscriberOptions extends BasePipelineSubscriberOptions<PeriodPipelinePayload> {}

// tslint:disable-next-line:max-line-length
export default class PeriodPipelineSubscriber extends BasePipelineSubscriber<
  Period,
  PeriodPipelinePayload,
  PeriodPipelineSubscription
> {
  protected static instance: PeriodPipelineSubscriber;

  protected;

  public options: PeriodPipelineSubscriberOptions;

  public name = PERIOD_PIPELINE_EXCHANGE;

  public queue = {
    name: PERIOD_PIPELINE_QUEUE,
    routes: [PeriodPipelineRoutes.DEFAULT],
  };

  constructor(options: PeriodPipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: PeriodPipelineSubscriberOptions): PeriodPipelineSubscriber {
    const instance = new PeriodPipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Period pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Period): Promise<PeriodPipelinePayload> {
    if (data) {
      return { period: data.id, status: data.status };
    }
    this.logger.warn('Period pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: PeriodPipelinePayload): Promise<Period> {
    return Period.findOne({
      where: { id: payload.period },
      relations: ['entries', 'invoice', 'invoice.contractor', 'invoice.contractor.domain'],
    });
  }
}
