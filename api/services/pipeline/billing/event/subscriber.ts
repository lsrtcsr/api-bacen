import { BaseError } from 'ts-framework-common';
import BasePipelineSubscriber, { BasePipelineSubscriberOptions } from '../../subscriber';
import {
  EventPipelinePayload,
  EventPipelineRoutes,
  EventPipelineSubscription,
  EVENT_PIPELINE_EXCHANGE,
  EVENT_PIPELINE_QUEUE,
} from './defaults';
import { Event } from '../../../../models';

// tslint:disable-next-line:max-line-length
export interface EventPipelineSubscriberOptions extends BasePipelineSubscriberOptions<EventPipelinePayload> {}

// tslint:disable-next-line:max-line-length
export default class EventPipelineSubscriber extends BasePipelineSubscriber<
  Event,
  EventPipelinePayload,
  EventPipelineSubscription
> {
  protected static instance: EventPipelineSubscriber;

  public options: EventPipelineSubscriberOptions;

  public name = EVENT_PIPELINE_EXCHANGE;

  public queue = {
    name: EVENT_PIPELINE_QUEUE,
    routes: [EventPipelineRoutes.DEFAULT],
  };

  constructor(options: EventPipelineSubscriberOptions) {
    super(options);
  }

  public static initialize(options: EventPipelineSubscriberOptions): EventPipelineSubscriber {
    const instance = new EventPipelineSubscriber(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Event pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Event): Promise<EventPipelinePayload> {
    if (data) return { data };

    this.logger.warn('Event pipeline service got undefined data in payload serialization');
  }

  protected async unserialize(payload: EventPipelinePayload): Promise<Event> {
    return payload.data;
  }
}
