import { BaseError } from 'ts-framework-common';
import { AMQPOptions } from 'ts-framework-queue';
import { User, Event } from '../../../../models';
import BasePipelinePublisher, { BasePipelinePublisherOptions } from '../../publisher';
import { EventPipelinePayload, EventPipelineRoutes, EVENT_PIPELINE_QUEUE, EVENT_PIPELINE_EXCHANGE } from './defaults';

export interface EventPipelinePublisherOptions extends BasePipelinePublisherOptions<EventPipelinePayload> {}

export default class EventPipelinePublisher extends BasePipelinePublisher<Event, EventPipelinePayload> {
  protected static instance: EventPipelinePublisher;

  public options: EventPipelinePublisherOptions;

  public name = EVENT_PIPELINE_EXCHANGE;

  public queue = {
    name: EVENT_PIPELINE_QUEUE,
    routes: [EventPipelineRoutes.DEFAULT],
  };

  constructor(options: EventPipelinePublisherOptions) {
    super(options);
  }

  public static initialize(options: EventPipelinePublisherOptions): EventPipelinePublisher {
    const instance = new EventPipelinePublisher(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Event pipeline service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  protected async serialize(data: Event): Promise<EventPipelinePayload> {
    return { data };
  }

  /**
   * Push new service consumption data to the queue
   */
  async send(event: Event, options?: AMQPOptions.Publish): Promise<boolean> {
    if (!event) {
      throw new BaseError('Could not push event to queue, invalid information', { event });
    }

    return await super.publish(EventPipelineRoutes.DEFAULT, event, { ...options });
  }
}
