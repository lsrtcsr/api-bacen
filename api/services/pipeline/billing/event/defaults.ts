import { AMQPMessage } from 'ts-framework-queue';
import { PipelineSubscriberActions } from '../../subscriber';
import { Event } from '../../../../models';

export const EVENT_PIPELINE_QUEUE = 'event_pipeline';
export const EVENT_PIPELINE_EXCHANGE = 'event_pipeline_exc';

export interface EventPipelinePayload {
  data: Event;
}

export type EventPipelineSubscription = (
  data: Event,
  message: AMQPMessage,
  actions: PipelineSubscriberActions<Event, EventPipelinePayload>,
) => Promise<void>;

export enum EventPipelineRoutes {
  DEFAULT = 'default',
}
