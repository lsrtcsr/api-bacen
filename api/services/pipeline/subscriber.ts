import { BaseError, Logger, Service, ServiceOptions } from 'ts-framework-common';
import AMQP, {
  AMQPMessage,
  AMQPOptions,
  Channel,
  Exchange,
  ExchangeActions,
  QueueInformation,
} from 'ts-framework-queue';
import AMQPService from '../amqp';

export interface BasePipelineSubscriberOptions<Payload> extends ServiceOptions {
  amqp: AMQP<Payload>;
  routes?: string[];
}

export class PipelineSubscriberActions<Data, Payload> {
  constructor(public actions: ExchangeActions<Payload>, public serialize: Function) {
    this.ack = this.ack.bind(this);
    this.nack = this.nack.bind(this);
    this.publish = this.publish.bind(this);
  }

  public async ack(allUpTo?: boolean) {
    return this.actions.ack(allUpTo);
  }

  public async nack(allUpTo?: boolean, requeue?: boolean) {
    return this.actions.nack(allUpTo, requeue);
  }

  async publish(route, data: Data, options?: AMQPOptions.Publish) {
    const payload = await this.serialize(data);
    await this.actions.publish(route, payload, options);
    return true;
  }
}

export default abstract class BasePipelineSubscriber<Data, Payload, Listener extends Function> extends Service {
  public abstract name: string;

  public abstract queue: QueueInformation;

  public readonly amqp: AMQP<Payload>;

  public options: BasePipelineSubscriberOptions<Payload>;

  public _channel?: Channel<Payload>;

  public _exchange?: Exchange<Payload>;

  constructor(options: BasePipelineSubscriberOptions<Payload>) {
    super(options);
    this.amqp = options.amqp || AMQPService.getInstance();
  }

  public get routes() {
    if (this.options.routes && this.options.routes.length) {
      return this.options.routes;
    }
    return this.queue.routes;
  }

  public async onMount() {
    // Ensure requested routes are registered in queue
    if (this.options.routes && this.options.routes.length) {
      const validation = this.options.routes
        .map(route => {
          return this.queue.routes.indexOf(route) >= 0;
        })
        .reduce((aggr, next) => aggr && next, true);

      if (!validation) {
        throw new BaseError(`Cannot subscribe to unknown route`, {
          routes: this.options.routes,
          queue: this.queue,
        });
      }
    }
  }

  public async onInit() {
    this._channel = await this.amqp.channel(this.name, {
      onError: err => {
        this.logger.error('Received error from amqp channel', err);
        process.exit(-1);
      },
    });
    this._exchange = await this._channel.exchange(this.name, {
      prefetch: 1,
      bind: [
        {
          name: this.queue.name,
          routes: this.routes,
        },
      ],
      exchangeOptions: { durable: true },
    });
  }

  public async onReady() {}

  protected abstract serialize(data: Data): Promise<Payload>;

  protected abstract unserialize(payload: Payload): Promise<Data>;

  public async subscribe(listener: Listener, options: AMQPOptions.Consume): Promise<void> {
    // Wrap callback for unserializing model using abstract methods
    const wrapper = async (payload: Payload, message: AMQPMessage, actions: ExchangeActions<Payload>) => {
      let data: Data;

      try {
        data = await this.unserialize(payload);
      } catch (exception) {
        this.logger?.error('Could not unserialize amqp message, message wont be reenqueued', {
          exception,
          payload,
        });
        actions.nack(undefined, false);
        return;
      }

      await listener(data, message, new PipelineSubscriberActions(actions, this.serialize.bind(this)));
    };

    // Subscribe to exchange messages
    return this._exchange.subscribe(this.queue.name, wrapper, options);
  }

  public async onUnmount() {}
}
