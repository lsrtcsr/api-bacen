import { BaseError } from 'ts-framework-common';

export interface DocumentServiceOptions {}

export class DocumentService {
  private static instance: DocumentService;

  constructor(options: DocumentServiceOptions = {}) {}

  /**
   * Gets the singleton Document service.
   *
   * @param options The service options
   */
  public static initialize(options: DocumentServiceOptions): DocumentService {
    const instance = new DocumentService(options);
    if (!DocumentService.instance) {
      DocumentService.instance = instance;
    }
    return instance;
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("CDT Cards service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static cleanBase64String(encodedContent: string): string {
    let cleanEncodedContent = encodedContent
      .split('dataimage/pngbase64')
      .join()
      .split('data:image/jpeg;base64,')
      .join()
      .split('data:image/png;base64,')
      .join()
      .replace(/={1,2}$/, '');

    cleanEncodedContent = DocumentService.removeBarFromLastDigits(cleanEncodedContent);
    return cleanEncodedContent;
  }

  public static removeBarFromLastDigits(encodedContent: string): string {
    const lastThreeDigits = encodedContent.slice(encodedContent.length - 3);
    const lastDigitsWithoutBar = lastThreeDigits.replace(/\/{1,2}/, '');

    return encodedContent.substring(0, encodedContent.length - 3).concat(lastDigitsWithoutBar);
  }

  public static isStringBase64Valid(encodedContent: string): boolean {
    if (encodedContent === '') return false;

    return true;
  }
}
