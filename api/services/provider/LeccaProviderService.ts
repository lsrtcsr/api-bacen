import {
  BaseCustodyFeature,
  CustodyDepositWebService,
  CustodyPaymentWebService,
  CustodyProvider,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyWithdrawWebService,
  CustodyBranchWebService,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface LeccaProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class LeccaProviderService extends CustodyProviderWebService {
  protected static instance: LeccaProviderService;

  public readonly type = CustodyProvider.LECCA_PROVIDER;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: LeccaProviderServiceOptions): LeccaProviderService {
    this.instance = new LeccaProviderService(options);
    return this.instance;
  }

  static getInstance(): LeccaProviderService {
    return this.instance;
  }

  public constructor(public options: LeccaProviderServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing STR Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in STR provider
    this.features.push(
      ...[
        new CustodyDepositWebService(options),
        new CustodyBranchWebService(options),
        new CustodyWithdrawWebService(options),
        new CustodyPaymentWebService(options),
      ],
    );
  }
}
