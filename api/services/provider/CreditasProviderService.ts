import {
  BaseCustodyFeature,
  CustodyDepositWebService,
  CustodyPaymentWebService,
  CustodyProvider,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyWithdrawWebService,
  CustodyBranchWebService,
  CustodyParticipatingInstitutionWebService,
  CustodyAccountInfoWebService,
  CustodyDocumentWebService,
  CustodyDocumentFeatureMethod,
  CustodyPostbackWebService,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface CreditasProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class CreditasProviderService extends CustodyProviderWebService {
  protected static instance: CreditasProviderService;

  public readonly type = CustodyProvider.CREDITAS_PROVIDER;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: CreditasProviderServiceOptions): CreditasProviderService {
    this.instance = new CreditasProviderService(options);
    return this.instance;
  }

  static getInstance(): CreditasProviderService {
    return this.instance;
  }

  public constructor(public options: CreditasProviderServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing STR Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in STR provider
    this.features.push(
      ...[
        new CustodyDepositWebService(options),
        new CustodyBranchWebService(options),
        new CustodyWithdrawWebService(options),
        new CustodyPaymentWebService(options),
        new CustodyParticipatingInstitutionWebService(options),
        new CustodyAccountInfoWebService(options),
        new CustodyPostbackWebService(options),
        new CustodyDocumentWebService({
          ...options,
          documentFeatureMethod: CustodyDocumentFeatureMethod.DOWNLOAD,
        }),
      ],
    );
  }
}
