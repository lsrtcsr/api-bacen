import {
  BaseCustodyFeature,
  CustodyAuditWebService,
  CustodyBoletoEmissionWebService,
  CustodyPaymentWebService,
  CustodyProvider,
  CustodyPostbackWebService,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyWithdrawWebService,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface BS2ProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class BS2ProviderService extends CustodyProviderWebService {
  protected static instance: BS2ProviderService;

  public readonly type = CustodyProvider.BS2;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: BS2ProviderServiceOptions): BS2ProviderService {
    this.instance = new BS2ProviderService(options);
    return this.instance;
  }

  static getInstance(): BS2ProviderService {
    return this.instance;
  }

  public constructor(public options: BS2ProviderServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing BS2 Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in BS2 provider
    this.features.push(
      new CustodyBoletoEmissionWebService(options),
      new CustodyWithdrawWebService(options),
      new CustodyAuditWebService(options),
      new CustodyPaymentWebService(options),
      new CustodyPostbackWebService(options),
    );
  }
}
