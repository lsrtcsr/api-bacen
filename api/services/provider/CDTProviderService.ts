import {
  BaseCustodyFeature,
  CustodyAuditWebService,
  CustodyBoletoPaymentWebService,
  CustodyCardWebService,
  CustodyDepositWebService,
  CustodyPaymentWebService,
  CustodyPhoneCreditWebService,
  CustodyPostbackWebService,
  CustodyProvider,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyWithdrawWebService,
  CustodyLegalWebService,
  CustodyBoletoEmissionWebService,
  CustodyDocumentWebService,
  CustodyDocumentFeatureMethod,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface CDTProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class CDTProviderService extends CustodyProviderWebService {
  protected static instance: CDTProviderService;

  public readonly type = CustodyProvider.CDT_VISA;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: CDTProviderServiceOptions): CDTProviderService {
    this.instance = new CDTProviderService(options);
    return this.instance;
  }

  static getInstance(): CDTProviderService {
    return this.instance;
  }

  public constructor(public options: CDTProviderServiceOptions) {
    super(options);
    this.options.documentFeatureMethod = CustodyDocumentFeatureMethod.UPLOAD;
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing CDT Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in CDT provider
    this.features.push(
      ...[
        new CustodyAuditWebService(options),
        new CustodyDepositWebService(options),
        new CustodyBoletoEmissionWebService(options),
        new CustodyBoletoPaymentWebService(options),
        new CustodyCardWebService(options),
        new CustodyDepositWebService(options),
        new CustodyDocumentWebService(options),
        new CustodyPaymentWebService(options),
        new CustodyPhoneCreditWebService(options),
        new CustodyPostbackWebService(options),
        new CustodyWithdrawWebService(options),
        new CustodyLegalWebService(options),
      ],
    );
  }
}
