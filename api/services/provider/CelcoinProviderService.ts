import {
  BaseCustodyFeature,
  CustodyProvider,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyBoletoPaymentWebService,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface CelcoinProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class CelcoinProviderService extends CustodyProviderWebService {
  protected static instance: CelcoinProviderService;

  public readonly type = CustodyProvider.CELCOIN;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: CelcoinProviderServiceOptions): CelcoinProviderService {
    this.instance = new CelcoinProviderService(options);
    return this.instance;
  }

  static getInstance(): CelcoinProviderService {
    return this.instance;
  }

  public constructor(public options: CelcoinProviderServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing Celcoin Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in STR provider
    this.features.push(...[new CustodyBoletoPaymentWebService(options)]);
  }
}
