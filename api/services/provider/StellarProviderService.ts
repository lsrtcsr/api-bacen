import {
  TransactionStatus,
  TransactionType,
  UnregisterReason,
  UserSchema,
  WalletSchema,
  WalletStatus,
} from '@bacen/base-sdk';
import { StellarService } from '@bacen/stellar-service';
import * as Stellar from 'stellar-sdk';
import { BaseError } from 'ts-framework-common';
import { EntityManager, getManager } from 'typeorm';
import Config from '../../../config';
import { Asset, OAuthAccessToken, Transaction, TransactionStateItem, Wallet, WalletState } from '../../models';

export interface StellarProviderServiceOptions {
  stellar: StellarService;
}

export default class StellarProviderService {
  public readonly stellar: StellarService;

  protected static instance: StellarProviderService;

  public readonly features = [];

  constructor(protected options: StellarProviderServiceOptions) {
    this.stellar = options.stellar || StellarService.getInstance();
  }

  public static initialize(options: StellarProviderServiceOptions): StellarProviderService {
    const instance = new StellarProviderService(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return this.instance;
  }

  public static getInstance(): StellarProviderService {
    return this.instance;
  }

  public async getBalance(walletId: string): Promise<number> {
    const wallet = await Wallet.safeFindOne({ where: { id: walletId } });
    const account = await this.stellar.loadAccount(wallet.stellar.publicKey);
    return Number(account.balances.find(balance => balance.asset_type === 'native').balance);
  }

  public async initializeWallet(
    userId: string,
    input: Partial<Wallet> = {},
    manager: EntityManager = getManager(),
  ): Promise<Wallet> {
    const random = Stellar.Keypair.random();

    const insertResult = await manager.insert(
      Wallet,
      Wallet.create({
        user: { id: userId },
        status: WalletStatus.PENDING,
        stellar: {
          publicKey: random.publicKey(),
          secretKey: random.secret(),
        },
        ...input,
      }),
    );

    await manager.insert(
      WalletState,
      WalletState.create({
        status: WalletStatus.PENDING,
        wallet: { id: insertResult.identifiers[0].id },
      }),
    );

    return manager.findOne(Wallet, insertResult.identifiers[0].id);
  }

  public async register(createdBy: OAuthAccessToken, wallet: WalletSchema): Promise<{ externalId: string }> {
    let response: any;
    let account: Stellar.AccountResponse;

    const manager = getManager();

    const rootWallet = await Wallet.getRootWallet(manager);
    const rootAsset = await Asset.getRootAsset({ manager });

    if (!rootWallet) {
      throw new Error('No Root Wallet. Something went very wrong.');
    }

    if (!rootAsset) {
      throw new Error('No Root Asset. Something went very wrong.');
    }

    if (!wallet.stellar) {
      throw new Error('Wallet has no Stellar information to be registered');
    }

    try {
      // Try to load existing account, to avoid unecessary failures
      account = await this.stellar.loadAccount(wallet.stellar.publicKey);

      if (!account || !account.account_id) {
        throw new BaseError('Stellar account not found');
      }
    } catch (ignoredException) {
      try {
        // Create account in Stellar network
        response = await this.stellar.createAccount(
          Config.stellar.rootWallet.secretKey,
          Stellar.Keypair.fromSecret(wallet.stellar.secretKey),
        );
      } catch (exception) {
        if (exception.response && exception.response.data) {
          throw new BaseError('Could not create account in the Stellar network', {
            ...exception.response.data,
          });
        } else {
          throw new BaseError('Could not create account in the Stellar network', exception);
        }
      }
    }

    // Save stellar transaction for audit
    const insertResults = await manager.insert(Transaction, {
      createdBy,
      source: rootWallet,
      type: TransactionType.CREATE_ACCOUNT,
      additionalData: {
        wallet_id: wallet?.id,
        hash: response?.hash?.toString(),
      },
    });

    const now = Date.now();

    // Save stellar transaction states
    await manager.insert(TransactionStateItem, [
      {
        transaction: insertResults.identifiers[0],
        status: TransactionStatus.PENDING,
        createdAt: new Date(now - 100),
        updatedAt: new Date(now - 100),
      },
      {
        transaction: insertResults.identifiers[0],
        status: TransactionStatus.AUTHORIZED,
        createdAt: new Date(now - 50),
        updatedAt: new Date(now - 50),
      },
      {
        transaction: insertResults.identifiers[0],
        status: TransactionStatus.EXECUTED,
        createdAt: new Date(now),
        updatedAt: new Date(now),
      },
    ]);

    await manager.findOne(Wallet, wallet.id);
    return { externalId: wallet.stellar.publicKey };
  }

  public async balance(wallet: WalletSchema): Promise<[{ balance: string }]> {
    const account = await this.stellar.loadAccount(wallet.stellar.publicKey);
    return account.balances as any;
  }

  public async update(user: UserSchema, wallet: WalletSchema): Promise<any> {
    throw new Error('Method not implemented.');
  }

  public async history(wallet: WalletSchema): Promise<any[]> {
    throw new Error('Method not implemented.');
  }

  public async unregister(user: UserSchema, wallet: WalletSchema, reason: UnregisterReason): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
