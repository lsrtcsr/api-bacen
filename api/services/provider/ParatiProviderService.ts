import {
  BaseCustodyFeature,
  CustodyDepositWebService,
  CustodyPaymentWebService,
  CustodyProvider,
  CustodyProviderWebService,
  CustodyProviderWebServiceOptions,
  CustodyWithdrawWebService,
  CustodyBranchWebService,
  CustodyParticipatingInstitutionWebService,
  CustodyDocumentWebService,
  CustodyDocumentFeatureMethod,
  CustodyAccountInfoWebService,
  CustodyPostbackWebService,
} from '@bacen/base-sdk';
import { Logger, LoggerInstance } from 'ts-framework-common';

export interface ParatiProviderServiceOptions extends CustodyProviderWebServiceOptions {
  logger?: LoggerInstance;
}

export default class ParatiProviderService extends CustodyProviderWebService {
  protected static instance: ParatiProviderService;

  public readonly type = CustodyProvider.PARATI_PROVIDER;

  public readonly logger: LoggerInstance;

  public readonly features: BaseCustodyFeature[] = [];

  static initialize(options: ParatiProviderServiceOptions): ParatiProviderService {
    this.instance = new ParatiProviderService(options);
    return this.instance;
  }

  static getInstance(): ParatiProviderService {
    return this.instance;
  }

  public constructor(public options: ParatiProviderServiceOptions) {
    super(options);
    this.options.documentFeatureMethod = CustodyDocumentFeatureMethod.DOWNLOAD;
    this.logger = options.logger || Logger.getInstance();
    this.logger.info('Initializing STR Provider service', {
      baseURL: options.baseURL,
    });

    // Add available features in STR provider
    this.features.push(
      ...[
        new CustodyDepositWebService(options),
        new CustodyBranchWebService(options),
        new CustodyWithdrawWebService(options),
        new CustodyPaymentWebService(options),
        new CustodyParticipatingInstitutionWebService(options),
        new CustodyDocumentWebService(options),
        new CustodyAccountInfoWebService(options),
        new CustodyPostbackWebService(options),
      ],
    );
  }
}
