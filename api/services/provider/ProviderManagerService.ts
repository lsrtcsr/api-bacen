import { BaseCustody, CustodyProvider, CustodyProviderWebService } from '@bacen/base-sdk';
import { Service, ServiceOptions, BaseError } from 'ts-framework-common';

export interface ProviderManagerServiceOptions extends ServiceOptions {
  providers: CustodyProviderWebService[];
}

export default class ProviderManagerService extends Service {
  public readonly providers: CustodyProviderWebService[];

  protected static instance: ProviderManagerService;

  public readonly options: ProviderManagerServiceOptions;

  constructor(options: ProviderManagerServiceOptions) {
    super(options);
    this.providers = options.providers;
  }

  static initialize(options: ProviderManagerServiceOptions): ProviderManagerService {
    this.instance = new ProviderManagerService(options);
    return this.instance;
  }

  static getInstance(): ProviderManagerService {
    return this.instance;
  }

  public from(type: CustodyProvider): BaseCustody | undefined {
    const provider = this.providers.find(provider => provider.type === type);

    if (!provider) {
      throw new BaseError(`No provider instance available for type "${type}" in the manager service`);
    }

    return provider;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
