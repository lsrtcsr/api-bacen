import { Service, BaseServer, ServiceOptions, BaseError } from 'ts-framework-common';
import { EmailTemplateLocals } from './notifications';
import { loadFile } from '../utils';
import { UninitializedServiceError } from '../errors';

export enum EmailTemplateType {
  PASSWORD_RESET = 'password_reset',
  CONFIRMATION_DICT = 'confirmation',
}

export interface EmailTemplateConfig {
  type: EmailTemplateType;
  locals: EmailTemplateLocals;
}

export interface EmailTemplateServiceOptions extends ServiceOptions {
  emailConfigPath?: string;
  defaultParameters: EmailTemplateLocals;
}

export interface GetConfigOptions {
  type: EmailTemplateType;

  /** TODO: Not supported yet, but there is definetely a use case for domain-specific tempaltes */
  domain?: string;
}

export class EmailTemplateService extends Service {
  private static instance?: EmailTemplateService;

  private readonly emailConfigPath?: string;

  private readonly defaultParameters: EmailTemplateLocals;

  private readonly templateParameters: Map<EmailTemplateType, EmailTemplateLocals> = new Map();

  constructor(options: EmailTemplateServiceOptions) {
    super({ ...options, name: 'EmailTemplateService' });

    this.emailConfigPath = options.emailConfigPath;
    this.defaultParameters = options.defaultParameters;
  }

  public static initialize(options: EmailTemplateServiceOptions): EmailTemplateService {
    return (this.instance = new EmailTemplateService(options));
  }

  public static getInstance(): EmailTemplateService {
    if (!this.instance) {
      throw new UninitializedServiceError('EmailTemplateService');
    }

    return this.instance;
  }

  /**
   * Fetches an emailParameters object based on a type and optionally a domain
   * @param options The getConfig options.
   */
  public getConfig(options: GetConfigOptions): EmailTemplateLocals {
    const { type } = options;

    return this.templateParameters.get(type) ?? this.defaultParameters;
  }

  public onMount(server: BaseServer): void {}

  public async onUnmount(server: BaseServer): Promise<void> {}

  public async onInit(server: BaseServer): Promise<void> {
    if (this.emailConfigPath) {
      try {
        const rawTemplates = await loadFile(this.emailConfigPath);
        const templates: EmailTemplateConfig[] =
          typeof rawTemplates === 'string' ? JSON.parse(rawTemplates) : rawTemplates;
        for (const entry of templates) {
          const { type, locals } = entry;

          if (!(type && locals)) {
            throw new BaseError('Invalid email template parameter format.', {
              type,
              locals,
            });
          }

          if (!Object.values(EmailTemplateType).includes(type)) {
            throw new BaseError('Email template has an invalid type', {
              type,
              validType: Object.values(EmailTemplateType),
            });
          }

          this.templateParameters.set(type, locals);
        }
      } catch (err) {
        if (err instanceof BaseError) {
          throw err;
        }

        throw new BaseError(err);
      }
    }
  }

  public async onReady(server: BaseServer): Promise<void> {}
}
