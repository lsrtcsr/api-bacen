import * as moment from 'moment';
import { BaseError, Service, ServiceOptions } from 'ts-framework-common';
import { MoreThanOrEqual, getManager, Brackets } from 'typeorm';
import { SeverityLevel, IssueType, NotificationChannel, DataType } from '@bacen/base-sdk';
import { Alert, Issue, Notification } from '../../models';
import Config from '../../../config';
import { TextService, SlackService, EmailService, JiraService } from '../notifications';

export interface IssueAlertServiceOptions extends ServiceOptions {}

export class IssueAlertService extends Service {
  public options: IssueAlertServiceOptions;

  protected static instance: IssueAlertService;

  private slackService: SlackService;

  constructor(options: IssueAlertServiceOptions = {}) {
    super(options);
  }

  static initialize(options?: IssueAlertServiceOptions) {
    const instance = new IssueAlertService(options);

    instance.slackService = SlackService.getInstance({
      webhookUrl: Config.slack.issuesChannel,
    });

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  static getInstance() {
    if (!this.instance) {
      throw new BaseError("Notification service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public async notify(issue: Issue) {
    const alerts = await Alert.createQueryBuilder('alert')
      .where('alert.deletedAt IS NULL')
      .andWhere(
        new Brackets(qb => {
          qb.where('alert.issueTypes IS NULL').orWhere(':issueType = ANY (alert.issueTypes)', {
            issueType: issue.type,
          });
        }),
      )
      .andWhere(
        new Brackets(qb => {
          qb.where('alert.issueCategories IS NULL').orWhere(':issueCategory = ANY (alert.issueCategories)', {
            issueCategory: issue.category,
          });
        }),
      )
      .getMany();

    alerts.forEach(async alert => {
      if (issue.severity >= alert.settings.threshold) {
        const startPoint = alert.settings.timeWindow
          ? moment(issue.createdAt)
              .subtract(alert.settings.timeWindow, 'minutes')
              .toDate()
          : issue.createdAt;
        let count;
        if (alert.issueTypes && alert.issueTypes.length && alert.settings.value > 1) {
          count = await Issue.safeCount({
            where: {
              type: issue.type,
              createdAt: MoreThanOrEqual(startPoint),
            },
          });
        }

        if (!count || count >= alert.settings.value) {
          const url = this.buildEndpointUrl(issue.type, alert.settings.threshold, startPoint);

          alert.settings.subscriptions.forEach(async subscription => {
            const recipients = subscription.subscribers.map(subscriber => subscriber.identifier);

            switch (subscription.channel) {
              case NotificationChannel.SLACK:
                let text;
                let title;
                let webhookUrl;
                if (subscription.extraData) {
                  text = subscription.extraData.find(datum => datum.type === DataType.TEXT);
                  title = subscription.extraData.find(datum => datum.type === DataType.TITLE);
                  webhookUrl = subscription.extraData.find(datum => datum.type === DataType.WEBHOOK_URL);
                }

                await this.slackService.send({
                  webhookUrl: (webhookUrl && webhookUrl.value) || undefined,
                  username: (recipients && recipients.join(',')) || undefined,
                  text: (text && text.value) || Config.alerts.notification.slack.text,
                  attachments: [
                    {
                      fallback: (title && title.value) || Config.alerts.notification.slack.title,
                      title: (title && title.value) || Config.alerts.notification.slack.title,
                      titleLink: url,
                    },
                  ],
                });
                break;

              case NotificationChannel.EMAIL:
                let body;
                let subject;
                if (subscription.extraData) {
                  body = subscription.extraData.find(datum => datum.type === DataType.BODY);
                  subject = subscription.extraData.find(datum => datum.type === DataType.SUBJECT);
                }

                await EmailService.getInstance().send({
                  to: recipients,
                  subject: (subject && subject.value) || Config.alerts.notification.email.subject,
                  locals: {
                    body: (body && body.value) || Config.alerts.notification.email.body,
                    button: {
                      url,
                      label: Config.alerts.notification.email.button.label,
                    },
                  },
                });
                break;

              case NotificationChannel.SMS:
                const message =
                  subscription.extraData && subscription.extraData.find(datum => datum.type === DataType.TEXT);
                await TextService.getInstance().send({
                  to: recipients,
                  text: message.value,
                });
                break;

              case NotificationChannel.JIRA:
                let epicId;
                let projectId;
                let issueTypeId;
                let description;
                let summary;
                let labels;
                if (subscription.extraData) {
                  epicId = subscription.extraData.find(datum => datum.type === DataType.EPIC_ID);
                  projectId = subscription.extraData.find(datum => datum.type === DataType.PROJECT_ID);
                  issueTypeId = subscription.extraData.find(datum => datum.type === DataType.ISSUE_TYPE_ID);
                  description = subscription.extraData.find(datum => datum.type === DataType.DESCRIPTION);
                  summary = subscription.extraData.find(datum => datum.type === DataType.TITLE);
                  labels = subscription.extraData
                    .filter(datum => datum.type === DataType.LABEL)
                    .map(label => label.value);
                }

                let descriptionText;
                if (description && description.value) {
                  descriptionText = description.value.concat(`
                  * ID ${issue.details.resourceId}
                  `);
                }

                await JiraService.getInstance().createIssue({
                  labels,
                  epicId: (epicId && epicId.value) || undefined,
                  projectId: (projectId && projectId.value) || undefined,
                  issueTypeId: (issueTypeId && issueTypeId.value) || undefined,
                  description: descriptionText,
                  summary: (summary && summary.value) || undefined,
                });
                break;
            }
            await this.trackNotification(alert, subscription.channel, recipients);
          });
        }
      }
    });
  }

  public async trackNotification(source: Alert, channel: NotificationChannel, recipients: string[]): Promise<void> {
    await getManager().transaction(async transaction => {
      for (const recipient of recipients) {
        await transaction.insert(
          Notification,
          Notification.create({
            source,
            channel,
            destination: recipient,
            sentAt: new Date(),
          }),
        );
      }
    });
  }

  private buildEndpointUrl(issueType: IssueType, severity: SeverityLevel, after: Date): string {
    return `${Config.alerts.issuesAPIUrl}?type=${issueType}&severiry=${severity}&after=${after}`;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
