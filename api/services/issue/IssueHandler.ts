import { BaseError, Service, ServiceOptions } from 'ts-framework-common';
import { Between } from 'typeorm';
import { IssueSchema, IssueDetails } from '@bacen/base-sdk';
import { Issue } from '../../models/issue/Issue';
import issuesConfig from '../../../config/issues.config';

export interface IssueHandlerOptions extends ServiceOptions {}

export class IssueHandler extends Service {
  public options: IssueHandlerOptions;

  protected static instance: IssueHandler;

  constructor(options: IssueHandlerOptions = {}) {
    super(options);
  }

  static initialize(options: IssueHandlerOptions) {
    const instance = new IssueHandler(options);
    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  static getInstance() {
    if (!this.instance) {
      throw new BaseError("Issue handler is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public async handle(issueData: IssueSchema): Promise<Issue> {
    this.logger.debug('Issue data', { ...issueData });

    // Checking if we should group this anomaly with a parent
    const nTimeAgo =
      new Date().getTime() - (issuesConfig.groupingWindows[issueData.category] || issuesConfig.defaultTimeInterval);
    const parent = await Issue.findOne({
      where: {
        resource_id: issueData.details.resourceId,
        resource_name: issueData.details.resourceType,
        reasonCode: issueData.type,
        createdAt: Between(new Date(nTimeAgo).toISOString(), new Date().toISOString()),
      },
      order: {
        createdAt: 'ASC',
      },
    });

    const issue = await Issue.insertAndFind(
      Issue.create({
        resource_id: issueData.details.resourceId,
        resource_name: issueData.details.resourceType,
        parent: parent || null,
        ...issueData,
      }),
    );

    if (parent) {
      this.logger.error('Issue registered in anomaly service', { issue: issue.toJSON() });
    } else {
      this.logger.error('New issue generated in anomaly service', { issue: issue.toJSON() });
    }

    return issue;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
