import Axios, { AxiosInstance, AxiosResponse } from 'axios';
import { HttpCode } from 'ts-framework';
import { BaseError, Service, ServiceOptions } from 'ts-framework-common';
import { HttpOptions } from '@bacen/base-sdk';
import Config from '../../../config';

export interface PostbackRequestSchema {
  type: string;
  status: string;
  [key: string]: any;
}
export interface PostbackDeliveryServiceOptions extends ServiceOptions {
  client?: AxiosInstance;
}

export class PostbackDeliveryService extends Service {
  protected static client: AxiosInstance;

  public options: PostbackDeliveryServiceOptions;

  protected static instance: PostbackDeliveryService;

  constructor(options: PostbackDeliveryServiceOptions = {}) {
    super(options);

    // Prepare HTTP client for requests
    if (Config.postbackDelivery.isActive) {
      options.client = Axios.create({
        httpsAgent: Config.postbackDelivery.httpsAgent,
      });
    } else {
      options.client = Axios.create();
    }

    PostbackDeliveryService.client = options.client;
  }

  static initialize(options: PostbackDeliveryServiceOptions) {
    const instance = new PostbackDeliveryService(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }

  static getInstance() {
    if (!this.instance) {
      throw new BaseError("Postback delivery service is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public async send(url: string, payload: PostbackRequestSchema, options?: HttpOptions): Promise<AxiosResponse<any>> {
    return PostbackDeliveryService.client.post(url, payload, options);
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
