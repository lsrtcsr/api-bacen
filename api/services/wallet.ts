import { Service, ServiceOptions } from 'ts-framework-common';
import * as currency from 'currency.js';
import { Horizon } from 'stellar-sdk';
import MainServer from '../MainServer';
import { Asset, Payment, Wallet, TRANSACTION_BLOCKED_BALANCE_STATES } from '../models';
import { Trace } from '../utils';
import { StellarQueryService } from './authorization';

export interface WalletServiceOptions extends ServiceOptions {}

export class LegacyWalletService extends Service {
  protected static instance?: LegacyWalletService;

  public async getBalancesFromStellar(wallet: Wallet, assets: Asset[]): Promise<{ code: string; balance: number }[]> {
    const stellerQueryService = StellarQueryService.getInstance();

    if (stellerQueryService.isReady) {
      return stellerQueryService.getBalanceForMultipleAssets(wallet, assets);
    }

    this.logger.warn('stellar-  DB not available, querying balances from horizon');
    const assetCodes = assets.map((asset) => asset.code);

    const walletBalances = await wallet.getBalances();
    return walletBalances
      .filter((balance) => balance.asset_type !== 'native' && assetCodes.includes(balance.asset_code))
      .map((balance: Horizon.BalanceLineAsset<'credit_alphanum4'>) => ({
        code: balance.asset_code,
        balance: Number(balance.balance) ?? 0,
      }));
  }

  @Trace()
  public async getPendingPaymentsSum(walletId: string, asset: Asset | string): Promise<number> {
    const paidIn = typeof asset === 'string' ? await Asset.getByIdOrCode(asset.toUpperCase()) : asset;

    const sumOfPendingExpenses = await Payment.findSumBySourceWalletAndTransactionStatus(
      TRANSACTION_BLOCKED_BALANCE_STATES,
      walletId,
      paidIn.id,
    );
    this.logger.debug(`Sum of pending expenses in ${paidIn.code} from wallet ${walletId}: ${sumOfPendingExpenses}`);

    const sumOfPendingReceipts = await Payment.findSumByDestinationWalletAndTransactionStatus(
      TRANSACTION_BLOCKED_BALANCE_STATES,
      walletId,
      paidIn.id,
    );
    this.logger.debug(`Sum of pending receipts in ${paidIn.code} to wallet ${walletId}: ${sumOfPendingReceipts}`);

    return currency(sumOfPendingReceipts, { precision: 7 }).subtract(sumOfPendingExpenses).value;
  }

  public static getInstance(options: ServiceOptions = {}): LegacyWalletService {
    if (!LegacyWalletService.instance) {
      LegacyWalletService.instance = new LegacyWalletService(options);
    }

    return LegacyWalletService.instance;
  }

  public onMount(server: MainServer): void {}

  public onUnmount(server: MainServer): void {}

  public async onInit(server: MainServer): Promise<void> {}

  public async onReady(server: MainServer): Promise<void> {}
}
