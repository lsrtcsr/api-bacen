import * as crc from 'crc64-ecma182';
import { QueryRunner } from 'typeorm';
import { BaseError, LoggerInstance, Logger } from 'nano-errors';
import { LockUnavailableError } from '../../errors';

export interface PgLockServiceOptions {
  /** The Service's logger */
  logger?: LoggerInstance;

  /** Time to wait when trying to syncrhonously acquire a lock. Defaults to 10 seconts */
  timeout?: number;

  /** Wheter or not to throw when trying to acquire a lock fails. Defaults to true */
  throwOnFail?: boolean;
}

const DEFAULT_PG_LOCK_SERVICE_OPTIONS: PgLockServiceOptions = {
  logger: Logger.getInstance(),
  timeout: 10_000,
  throwOnFail: true,
};

export class PgLockService {
  private readonly queryRunner: QueryRunner;

  public readonly logger: LoggerInstance;

  public readonly options: Omit<PgLockServiceOptions, 'logger'>;

  constructor(queryRunner: QueryRunner, optionsParam: PgLockServiceOptions = {}) {
    // Merge passed options with defaults
    const options = { ...DEFAULT_PG_LOCK_SERVICE_OPTIONS, ...optionsParam };
    const { logger, ...serviceOptions } = options;

    this.queryRunner = queryRunner;
    this.options = serviceOptions;
    this.logger = logger;
  }

  public static generateKeysFromId(id: string): [number, number] {
    // Postgres only accepts either one int64 or two int32 numbers as the id of an advisory lock. UUIDs contain 128
    // bytes so it is impossible to losslessly convert them into either of these values, thus we must resort to some
    // form of hash function in order to do this conversion while avoiding colisions as possible.
    //
    // As recommended by https://stackoverflow.com/a/53959211/10162858 we use crc64-ecma182 as our hash function. The
    // author of the response claims that by taking 5 bytes out of the crc64 they tested it for "several million UUIDs"
    // successfully. We will be taking all of the 8 bytes of the response and our constraint is not that there can never
    // be a colission but rather that two *active* ids must not collide. This places an upper bound not on the number of
    // walletIds (which we will hash to generate lock keys) but on the maximum number of *concurrent transactions* which
    // can occur at any given time, and as the author claims this limit is in the millions.
    //
    // Furthermore each client shall have a separate database so this bound applies per client. Also in case of an
    // eventual collision it should not result in data corruption but rather two wallets that are unable to perform
    // transactions in parallel. Therefore I believe it should be quite safe to use this method into the foreseable
    // future.

    const hash = crc.crc64(id) as Buffer;
    const firstInt = hash.readInt32LE(0);
    const secondInt = hash.readInt32LE(4);

    return [firstInt, secondInt];
  }

  /**
   * Creates a transaction-level advisory lock.
   * If the lock is unavailable the service may wait until it times-out and throws.
   *
   * @param id The id of the entity which we want to lock.
   */
  public async acquireTransactionLock(id: string): Promise<void> {
    if (this.queryRunner.isReleased) {
      throw new BaseError(
        'Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.',
      );
    }

    if (!this.queryRunner.isTransactionActive) {
      throw new BaseError('Tried to create a lock without a transaction. Locks may only be used within a transaction.');
    }

    const [key1, key2] = PgLockService.generateKeysFromId(id);

    this.logger.debug(`PgLockService: Acquiring transaction-level lock with id: ${id}`);

    const queryPromise = this.queryRunner.query(`SELECT pg_advisory_xact_lock($1, $2)`, [key1, key2]);
    const timeoutPromise = new Promise<void>((_, fail) => setTimeout(fail, this.options.timeout));

    try {
      await Promise.race([queryPromise, timeoutPromise]);
    } catch (err) {
      this.logger.error(`PgLockService: Failed to acquire lock with id: ${id}`);
      throw new LockUnavailableError({ id, key1, key2 });
    }
    this.logger.info(`PgLockService: Successfully acquired lock for id: ${id}`);
  }

  /**
   * Creates a transaction-level advisory lock.
   * If the lock is unavailable the service may or may not throw.
   * The default is to throw but this may be changed on the service options.
   *
   * @param id The id of the entity which we want to lock.
   */
  public async tryAcquireTransactionLock(id: string): Promise<boolean> {
    if (this.queryRunner.isReleased) {
      throw new BaseError(
        'Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.',
      );
    }

    if (!this.queryRunner.isTransactionActive) {
      throw new BaseError('Tried to create a lock without a transaction. Locks may only be used within a transaction.');
    }

    const [key1, key2] = PgLockService.generateKeysFromId(id);

    this.logger.debug(`PgLockService: Trying to acquire transaction-level lock for id: ${id}`);

    const result = await this.queryRunner.query(`SELECT pg_try_advisory_xact_lock($1, $2)`, [key1, key2]);
    const success = result[0]['pg_try_advisory_xact_lock'] as boolean;

    if (!success) {
      if (this.options.throwOnFail) {
        this.logger.error(`PgLockService: Failed to acquire lock with id: ${id}`);
        throw new LockUnavailableError({ id, key1, key2 });
      } else {
        this.logger.warn(`PgLockService: Failed to acquire lock with id: ${id}`);
      }
    } else {
      this.logger.info(`PgLockService: Successfully acquired lock for id: ${id}`);
    }

    return success;
  }

  /**
   * Creates a session-level advisory lock.
   * If the lock is unavailable the service may wait until it times-out and throws.
   *
   * @param id The id of the entity which we want to lock.
   */
  public async acquireLock(id: string): Promise<void> {
    if (this.queryRunner.isReleased) {
      throw new BaseError(
        'Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.',
      );
    }

    const [key1, key2] = PgLockService.generateKeysFromId(id);
    this.logger.debug(`PgLockService: Acquiring session-level lock for id: ${id}`);

    const queryPromise = this.queryRunner.query(`SELECT pg_advisory_lock($1, $2)`, [key1, key2]);
    const timeoutPromise = new Promise<void>((_, fail) => setTimeout(fail, this.options.timeout));

    try {
      await Promise.race([queryPromise, timeoutPromise]);
    } catch (err) {
      this.logger.error(`PgLockService: Failed to acquire lock with id: ${id}`);
      throw new LockUnavailableError({ id, key1, key2 });
    }

    this.logger.info(`PgLockService: Successfully acquired lock for id: ${id}`);
  }

  /**
   * Creates a session-level advisory lock.
   * If the lock is unavailable the service may or may not throw.
   * The default is to throw but this may be changed on the service options.
   *
   * @param id The id of the entity which we want to lock.
   */
  public async tryAcquireLock(id: string): Promise<boolean> {
    if (this.queryRunner.isReleased) {
      throw new BaseError(
        'Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.',
      );
    }

    const [key1, key2] = PgLockService.generateKeysFromId(id);

    this.logger.debug(`PgLockService: Trying to acquire session-level lock for id: ${id}`);

    const result = await this.queryRunner.query(`SELECT pg_try_advisory_lock($1, $2)`, [key1, key2]);
    const success = result[0]['pg_try_advisory_lock'] as boolean;

    if (!success) {
      if (this.options.throwOnFail) {
        this.logger.error(`PgLockService: Failed to acquire lock with id: ${id}`);
        throw new LockUnavailableError({ id, key1, key2 });
      } else {
        this.logger.warn(`PgLockService: Failed to acquire lock with id: ${id}`);
      }
    } else {
      this.logger.info(`PgLockService: Successfully acquired lock for id: ${id}`);
    }

    return success;
  }

  /**
   * Releases a session-level lock.
   * May throw if the operation fails depending on the options.
   * @param id The id of the entity which we want to unlock
   */
  public async releaseLock(id: string): Promise<boolean> {
    if (this.queryRunner.isReleased) {
      throw new BaseError(
        'Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.',
      );
    }

    const [key1, key2] = PgLockService.generateKeysFromId(id);
    this.logger.debug(`PgLockService: Releasing session-level lock for id: ${id}`);

    const result = await this.queryRunner.query(`SELECT pg_advisory_unlock($1, $2)`, [key1, key2]);
    const success = result[0]['pg_advisory_unlock'] as boolean;

    if (!success) {
      if (this.options.throwOnFail) {
        this.logger.error(`PgLockService: Failed to release lock with id: ${id}`);
        throw new LockUnavailableError({ id, key1, key2 });
      } else {
        this.logger.warn(`PgLockService: Failed to release lock with id: ${id}`);
      }
    } else {
      this.logger.info(`PgLockService: Successfully released lock for id: ${id}`);
    }

    return success;
  }
}
