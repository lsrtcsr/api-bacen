import { Service, ServiceOptions, BaseServer } from 'ts-framework-common';
import * as Redis from 'redis';
import * as Redlock from 'redlock';
import { MaybePromise } from '../../utils';

export interface LockServiceOptions extends ServiceOptions {
  redisConfig: Redis.ClientOpts[];
  retry: {
    driftFactor?: number;
    retryCount?: number;
    retryDelay?: number;
    retryJitter?: number;
  };
}

export interface AcquireLockOptions {
  key: string | string[];
  ttl?: number;
}

export class LockService extends Service {
  private readonly redisClients: Redis.RedisClient[];

  private readonly redlock: Redlock;

  public readonly options: LockServiceOptions;

  constructor(options: LockServiceOptions) {
    const { redisConfig, retry, ...serviceOptions } = options;
    super({ ...serviceOptions, name: 'LockService' });

    this.options = options;
    this.redisClients = [];
    for (const redisInstanceConfig of redisConfig) {
      this.redisClients.push(Redis.createClient(redisInstanceConfig));
    }

    this.redlock = new Redlock(this.redisClients, retry);
    this.redlock.on('clientError', (err: any) => this.logger.error(`Redlock: a redis error has ocurred`, err));
  }

  public async withLock<T>(
    lockOptions: AcquireLockOptions,
    callback: (lock?: Redlock.Lock) => MaybePromise<T>,
  ): Promise<T> {
    const { key, ttl = 60000 } = lockOptions;
    const lockAcquisitionTimestamp = Date.now();

    const lock = await this.redlock.lock(key, ttl);
    try {
      const cbResponse = callback(lock);
      const cbResult = await (cbResponse instanceof Promise ? cbResponse : Promise.resolve(cbResponse));

      await this.redlock.release(lock);
      return cbResult;
    } catch (err) {
      await this.redlock.release(lock);
      throw err;
    }
  }

  onMount(server: BaseServer): void {
    throw new Error('Method not implemented.');
  }

  onUnmount(server: BaseServer): void {
    throw new Error('Method not implemented.');
  }

  onInit(server: BaseServer): Promise<void> {
    throw new Error('Method not implemented.');
  }

  onReady(server: BaseServer): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
