import { UserSchema, UnleashFlags } from '@bacen/base-sdk';
import IdentityService, { DocumentReport, PersonKYCReportSchema } from '@bacen/identity-sdk';
import { BaseServer, Service, ServiceOptions } from 'ts-framework-common';
import { UnleashUtil } from '@bacen/shared-sdk';
import Config from '../../../config';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import MainServer from '../../MainServer';
import { Document, User } from '../../models';
import StorageService from '../storageservice';

export interface PersonIdentityServiceOptions extends ServiceOptions {
  identity?: IdentityService;
  storage?: StorageService;
}

export class PersonIdentityService extends Service {
  protected static instance: PersonIdentityService;

  public readonly options: PersonIdentityServiceOptions;

  public readonly unleash: typeof UnleashUtil;

  public identity: IdentityService;

  public storage: StorageService;

  constructor(options: PersonIdentityServiceOptions) {
    super(options);
    this.unleash = UnleashUtil;
  }

  public static initialize(options: PersonIdentityServiceOptions = {}): PersonIdentityService {
    this.instance = new PersonIdentityService(options);
    return this.instance;
  }

  public static getInstance(): PersonIdentityService {
    return this.instance;
  }

  public onMount(server: MainServer) {
    this.identity = this.options.identity || IdentityService.getInstance();
    this.storage = this.options.storage || StorageService.getInstance();
  }

  public onUnmount(server: BaseServer): void {
    this.identity = undefined;
  }

  public async onInit(server: BaseServer): Promise<void> {}

  public async onReady(server: BaseServer): Promise<void> {}

  /**
   * Matches and fills user data supplied.
   *
   * @param user The user candidate
   */
  public async match(user: UserSchema): Promise<User> {
    const shouldSkipFillBasic = this.unleash.isEnabled(
      UnleashFlags.KYC_SKIP_FILL_BASIC,
      { userId: user.id, properties: { domainId: user.domain.id } },
      false,
    );

    const forceUniqueEmailAlias = this.unleash.isEnabled(
      UnleashFlags.FORCE_UNIQUE_EMAIL_ALIAS,
      {
        userId: user.id,
        properties: { domainId: user.domain.id },
      },
      false,
    );

    // Skip filling and matching with feature flag
    if (shouldSkipFillBasic) {
      return User.create(user as User);
    }

    let emails = [user.email];

    // Accept aliases in KYC email check
    if (!forceUniqueEmailAlias) {
      const [username, domain] = user.email.split('@');
      const [baseUsername] = username.split('+');
      emails = [`${baseUsername}@${domain}`];
    }

    // Try to match user with records in the identity service
    const { accepted, person, ...response } = await this.identity.people().match(user.consumer.taxId, {
      emails,
      name: user.name,
      taxId: user.consumer.taxId,
      birthDate: user.consumer.birthday,
      motherName: user.consumer.motherName,
      addresses: user.consumer.addresses,
      // Phone is validated using SMS, so we don't check in the Identity API
    });

    // if (response.taxIdStatus && response.taxIdStatus !== PersonTaxIdStatus.ACTIVE) {
    //   throw new Invalid5RequestError('The user registration is not active with the authorities');
    // }

    // const consistencyCheckErrors = [];

    // if (!response.name || response.name === '') {
    //   consistencyCheckErrors.push('Failed to validate consumer name');
    // } else if (!matches(user.name, response.name)) {
    //   consistencyCheckErrors.push('Name did not match');
    // } else {
    //   const namePieces = response.name.split(' ');
    //   user.firstName = namePieces.shift();
    //   user.lastName = namePieces.join(' ');
    // }

    // if (!response.motherName || response.motherName === '') {
    //   consistencyCheckErrors.push('Failed to validate mother name');
    // } else if (user.consumer.motherName) {
    //   if (!matches(user.consumer.motherName, response.motherName)) {
    //     consistencyCheckErrors.push('Mother name did not match');
    //   }
    // } else {
    //   user.consumer.motherName = response.motherName;
    // }

    // if (!response.birthDate) {
    //   consistencyCheckErrors.push('Failed to validate birth date');
    // } else if (user.consumer.birthday) {
    //   if (!isSameDate(response.birthDate, user.consumer.birthday)) {
    //     consistencyCheckErrors.push('Birth date did not match');
    //   }
    // } else {
    //   user.consumer.birthday = response.birthDate;
    // }

    if (!accepted) {
      throw new InvalidRequestError('User has not matched validation', response.flags);
    }

    // Fill basic user information, if not supplied
    user.consumer.motherName = user.consumer.motherName || person.motherName;
    user.consumer.birthday = user.consumer.birthday || person.birthDate;

    return User.create(user as User);
  }

  /**
   * Generates KYC report for specified user
   *
   * @param user The user instance
   * @param options The report options
   */
  public async report(
    user: User,
    options: { bucket: string } = { bucket: Config.googlecloud.storage.bucketName },
  ): Promise<PersonKYCReportSchema & { kycFileName?: string }> {
    const { accepted, ...report } = await this.identity.people().report(user.consumer.taxId);

    // Prepare for report storage
    const kycFileName = `${user.id}-${new Date().getTime()}.json`;
    const additionalData = { accepted, report, kycFileName };

    // Save report in storage for audit
    await this.storage.uploadFile({
      bucketName: options.bucket,
      fileName: kycFileName,
      data: JSON.stringify(additionalData),
    });

    return { accepted, kycFileName, ...report };
  }

  /**
   * Generates document report for all its sides
   *
   * @param document The document instance (or an object with a valid "id")
   */
  public async documentReport({ id }: Document): Promise<{ accepted: boolean; sides: DocumentReport[] }> {
    const document = await Document.findOne({
      where: { id },
      relations: ['consumer', 'consumer.user'],
    });

    const sides = await document.existingSides();
    const results = await Promise.all(sides.map(side => this.documentSideReport(side, document)));
    const accepted = results.map(item => item.accepted).reduce((a, b) => a && b, true);

    return { accepted, sides: results };
  }

  /**
   * Generates the report for a document side
   *
   * @param side The side to generate the report from
   * @param document The document instance to get the report
   */
  public async documentSideReport(
    side: string,
    document: Document,
  ): Promise<DocumentReport & { kycFileName?: string }> {
    const kycFileName = document.getImageFileName(side);
    const fileExists = await this.storage.fileExists({ fileName: kycFileName });

    if (!fileExists) {
      throw new EntityNotFoundError('Document not found', { side });
    }

    const base64image = await this.storage.getFile({ fileName: kycFileName });
    const { accepted, ...report } = await IdentityService.getInstance()
      .documents()
      .report(base64image);

    return { accepted, kycFileName, ...report };
  }
}
