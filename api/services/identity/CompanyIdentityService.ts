import { UserSchema, ConsumerSchema, UnleashFlags } from '@bacen/base-sdk';
import IdentityService, { CompanyKYCReportSchema, DocumentReport } from '@bacen/identity-sdk';
import { BaseServer, Service, ServiceOptions } from 'ts-framework-common';
import { UnleashUtil } from '@bacen/shared-sdk';
import Config from '../../../config';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import MainServer from '../../MainServer';
import { Document, User } from '../../models';
import StorageService from '../storageservice';

export interface CompanyIdentityServiceOptions extends ServiceOptions {
  identity?: IdentityService;
  storage?: StorageService;
}

export class CompanyIdentityService extends Service {
  protected static instance: CompanyIdentityService;

  public readonly options: CompanyIdentityServiceOptions;

  public readonly unleash: typeof UnleashUtil;

  public identity: IdentityService;

  public storage: StorageService;

  constructor(options: CompanyIdentityServiceOptions) {
    super(options);
    this.unleash = UnleashUtil;
  }

  public static initialize(options: CompanyIdentityServiceOptions): CompanyIdentityService {
    this.instance = new CompanyIdentityService(options);
    return this.instance;
  }

  public static getInstance(): CompanyIdentityService {
    return this.instance;
  }

  public onMount(server: MainServer) {
    this.identity = this.options.identity || IdentityService.getInstance();
    this.storage = this.options.storage || StorageService.getInstance();
  }

  public onUnmount(server: BaseServer): void {
    this.identity = undefined;
  }

  public async onInit(server: BaseServer): Promise<void> {}

  public async onReady(server: BaseServer): Promise<void> {}

  /**
   * Matches and fills user data supplied.
   *
   * @param user The user candidate
   */
  public async match(user: UserSchema): Promise<User> {
    const shouldFillData = this.unleash.isEnabled(
      UnleashFlags.KYC_SKIP_FILL_BASIC,
      { userId: user.id, properties: { domainId: user.domain.id } },
      false,
    );

    // Skip filling and matching with feature flag
    if (!shouldFillData) {
      return User.create(user as User);
    }

    const {
      name,
      email,
      consumer: {
        phones,
        addresses,
        birthday,
        companyData: { partners, activities, tradeName: registrationName } = {} as any,
      } = {} as ConsumerSchema,
    } = user || {};

    // Try to match user with records in the identity service
    const { accepted, company, ...response } = await this.identity.companies().match(user.consumer.taxId, {
      name,
      emails: [email],
      taxId: user.consumer.taxId,
      registrationName,
      birthDate: birthday,
      phones,
      addresses,
      partners,
      activities,
    });

    if (!accepted) {
      throw new InvalidRequestError('the following inconsistencies prevent consumer registration', response.flags);
    }

    return User.create(user as User);
  }

  /**
   * Generates KYC report for specified user
   *
   * @param user The user instance
   * @param options The report options
   */
  public async report(
    user: User,
    options: { bucket: string } = { bucket: Config.googlecloud.storage.bucketName },
  ): Promise<CompanyKYCReportSchema & { kycFileName?: string }> {
    const { accepted, ...report } = await this.identity.companies().report(user.consumer.taxId);

    // Prepare for report storage
    const kycFileName = `${user.id}-${new Date().getTime()}.json`;
    const additionalData = { accepted, report, kycFileName };

    // Save report in storage for audit
    await this.storage.uploadFile({
      bucketName: options.bucket,
      fileName: kycFileName,
      data: JSON.stringify(additionalData),
    });

    return { accepted, kycFileName, ...report };
  }

  /**
   * Generates document report for all its sides
   *
   * @param document The document instance (or an object with a valid "id")
   */
  public async documentReport({ id }: Document): Promise<{ accepted: boolean; sides: DocumentReport[] }> {
    const document = await Document.findOne({
      where: { id },
      relations: ['consumer', 'consumer.user'],
    });

    const sides = await document.existingSides();
    const results = await Promise.all(sides.map(side => this.documentSideReport(side, document)));
    const accepted = results.map(item => item.accepted).reduce((a, b) => a && b, true);

    return { accepted, sides: results };
  }

  /**
   * Generates the report for a document side
   *
   * @param side The side to generate the report from
   * @param document The document instance to get the report
   */
  public async documentSideReport(
    side: string,
    document: Document,
  ): Promise<DocumentReport & { kycFileName?: string }> {
    const kycFileName = document.getImageFileName(side);
    const fileExists = await this.storage.fileExists({ fileName: kycFileName });

    if (!fileExists) {
      throw new EntityNotFoundError('Document not found', { side });
    }

    const base64image = await this.storage.getFile({ fileName: kycFileName });
    const { accepted, ...report } = await IdentityService.getInstance()
      .documents()
      .report(base64image);

    return { accepted, kycFileName, ...report };
  }
}
