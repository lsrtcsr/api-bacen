export interface StoredResponse {
  status: number;

  contentType: string;

  body: any;
}

export interface IdempotenceStore {
  lock(key: string): Promise<boolean>;

  unlock(key: string): Promise<boolean>;

  get(key: string): Promise<StoredResponse | undefined>;

  set(key: string, response: StoredResponse): Promise<void>;
}
