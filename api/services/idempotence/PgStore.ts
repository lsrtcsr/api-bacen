import { getConnection, QueryRunner } from 'typeorm';
import { IdempotenceStore, StoredResponse } from './types';
import { PgLockService } from '../locks';
import { Request } from '../../models';

export class PgIdempotenceStore implements IdempotenceStore {
  private queryRunner: QueryRunner | null;

  private lockService: PgLockService | null;

  constructor(qr?: QueryRunner) {
    this.queryRunner = qr || null;
    this.lockService = null;
  }

  public async lock(key: string): Promise<boolean> {
    this.lazyLockService();

    return this.lockService.tryAcquireLock(key);
  }

  public async unlock(key: string): Promise<boolean> {
    this.lazyLockService();

    const didUnlock = await this.lockService.releaseLock(key);
    await this.relesaseQueryRunner();
    return didUnlock;
  }

  public async get(key: string): Promise<StoredResponse | undefined> {
    const request = await Request.safeFindOne({
      where: { idempotenceKey: key },
    });

    if (!request) {
      return;
    }

    return {
      body: request.body,
      contentType: request.contentType,
      status: request.status,
    };
  }

  public async set(key: string, response: StoredResponse): Promise<void> {
    await Request.insert({
      idempotenceKey: key,
      body: response.body,
      contentType: response.contentType,
      status: response.status,
    });
  }

  /**
   * Lazily initializes lockService if it has not already been initialized.
   */
  private lazyLockService(): void {
    if (!this.queryRunner) {
      this.queryRunner = getConnection().createQueryRunner();
    }

    this.lockService = new PgLockService(this.queryRunner, { throwOnFail: false });
  }

  /**
   * Releases the query runner if it exists and is not released.
   */
  private async relesaseQueryRunner(): Promise<void> {
    if (this.queryRunner && !this.queryRunner.isReleased) {
      await this.queryRunner.release();
      this.queryRunner = null;
    }
  }
}
