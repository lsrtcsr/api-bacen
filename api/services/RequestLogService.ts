import { Service, ServiceOptions } from 'ts-framework-common';
import { RequestLog } from '../models';
import { logger } from '../../config/logger.config';

export interface RequestLogServiceOptions extends ServiceOptions {}

export class RequestLogService extends Service {
  public options: RequestLogServiceOptions;

  protected static instance: RequestLogService;

  constructor(options: RequestLogServiceOptions = {}) {
    super(options);
  }

  static initialize(options: RequestLogServiceOptions = {}) {
    this.instance = new RequestLogService(options);
  }

  static getInstance() {
    return this.instance;
  }

  public async log(log: RequestLog): Promise<void> {
    try {
      await RequestLog.insert(log);
    } catch (err) {
      logger.error('Could not log request', err);
    }
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {}

  async onUnmount(server) {}
}
