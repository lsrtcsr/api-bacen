import { Storage, UploadResponse } from '@google-cloud/storage';
import { mkdirp, unlink, writeFile } from 'fs-extra';
import { join } from 'path';
import { Readable } from 'stream';
import { Logger, LoggerInstance } from 'ts-framework-common';
import { default as Config } from '../../config';
import { logger } from '../../config/logger.config';
import { wrapError } from '../errors';

export interface StorageServiceOptions {
  logger?: LoggerInstance;
}

export default class StorageService {
  protected logger: LoggerInstance;

  protected static instance: StorageService;

  public async getFile(options: { bucketName?: string; fileName: string }): Promise<string> {
    try {
      const buffer = await this.getFileAsBuffer(options);
      return buffer.toString('base64');
    } catch (error) {
      wrapError(error, { defaultMessage: 'Could not download file from Google Cloud' });
    }
  }

  public async getFileAsBuffer(options: { bucketName?: string; fileName: string }): Promise<Buffer> {
    const bucketName = options.bucketName || Config.googlecloud.documentPhotoStorage.bucketName;
    try {
      const bucket = new Storage().bucket(bucketName);
      const file = bucket.file(options.fileName);
      const results = await file.download();
      return results[0];
    } catch (error) {
      wrapError(error, { defaultMessage: 'Could not download file from Google Cloud' });
    }
  }

  public async fileExists(options: { bucketName?: string; fileName: string }): Promise<boolean> {
    const bucketName = options.bucketName || Config.googlecloud.documentPhotoStorage.bucketName;
    try {
      const bucket = new Storage().bucket(bucketName);
      const file = bucket.file(options.fileName);
      const exists = await file.exists();

      return exists[0];
    } catch (error) {
      if ((error && error.response && error.response.status === 404) || error.status === 404) {
        return false;
      }
      wrapError(error, { defaultMessage: 'Unable to verify file existence in Google Cloud' });
    }
  }

  public async getFileAsStream(options: { bucketName?: string; fileName: string }): Promise<Readable> {
    const bucketName = options.bucketName || Config.googlecloud.documentPhotoStorage.bucketName;
    try {
      const bucket = new Storage().bucket(bucketName);
      return bucket.file(options.fileName).createReadStream();
    } catch (error) {
      wrapError(error, { defaultMessage: 'Could not download file from Google Cloud' });
    }
  }

  // file in string base64
  public async uploadFile(options: {
    bucketName?: string;
    fileName: string;
    data: string;
    encoding?: string;
  }): Promise<UploadResponse> {
    const bucketName = options.bucketName || Config.googlecloud.documentPhotoStorage.bucketName;
    try {
      const storage = new Storage();
      const bucket = storage.bucket(bucketName);

      logger.silly('Making sure tmp path exists for file upload', { path: join(process.cwd(), '.tmp/') });
      await mkdirp(join(process.cwd(), '.tmp/'));
      const filePath = join(process.cwd(), `.tmp/${options.fileName}`);

      logger.silly('Writing local temporary file', { file: filePath });
      await writeFile(filePath, options.data, { encoding: options.encoding || 'utf8' });

      logger.debug('Uploading file to remote cloud bucket', {
        file: filePath,
        bucket: bucketName,
      });

      const response = await bucket.upload(filePath, { gzip: true, metadata: { cacheControl: 'no-cache' } });

      try {
        await unlink(filePath);
      } catch (exception) {
        Logger.getInstance().warn(exception);
      }
      return response;
    } catch (error) {
      wrapError(error, { defaultMessage: 'Could not upload image to Google Cloud' });
    }
  }

  public static getInstance(options: StorageServiceOptions = {}): StorageService {
    if (!StorageService.instance) {
      StorageService.instance = new StorageService(options);
    }

    return StorageService.instance;
  }

  constructor(public options: StorageServiceOptions) {
    this.logger = options.logger || Logger.getInstance();
  }
}
