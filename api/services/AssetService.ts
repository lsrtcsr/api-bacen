import { CustodyProvider } from '@bacen/base-sdk';
import { BaseError, BaseServer, Logger, Service, ServiceOptions } from 'ts-framework-common';
import { loadFile } from '../utils/FileUtil';

export interface AssetConfig {
  code: string;
  provider?: CustodyProvider;
  name?: string;
  authorizable?: boolean;
  required?: boolean;
}

export interface AssetServiceOptions extends ServiceOptions {
  rootAssetConfigPath: string;
  initialAssetConfigPath: string;
}

export class AssetService extends Service {
  private static instance: AssetService;

  private _rootAsset: AssetConfig;

  private _otherAssetsInCustody: AssetConfig[];

  private _authorizableAssets: AssetConfig[];

  constructor(public readonly options: AssetServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
  }

  public static initialize(options: AssetServiceOptions) {
    return (this.instance = new AssetService(options));
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError('AssetService is not initialized yet.');
    }

    return this.instance;
  }

  public async onInit(server: BaseServer): Promise<void> {
    try {
      // TODO: Use some form of strongly-typed deserialization, eg: class-transformer + class-validator
      const rawRootAsset = await loadFile(this.options.rootAssetConfigPath);
      this._rootAsset = typeof rawRootAsset === 'string' ? JSON.parse(rawRootAsset) : rawRootAsset;

      const rawInitialAssets = await loadFile(this.options.initialAssetConfigPath);
      this._otherAssetsInCustody =
        typeof rawInitialAssets === 'string' ? JSON.parse(rawInitialAssets) : rawInitialAssets;

      this.setAuhorizableAssets();
    } catch (err) {
      throw new BaseError('Could not load assetSeed config from specified paths.', err);
    }
  }

  private setAuhorizableAssets() {
    this._authorizableAssets = this.allInitialAssets.filter((asset) => asset.authorizable);
  }

  public get rootAsset(): AssetConfig {
    return this._rootAsset;
  }

  public get otherAssetsInCustody(): AssetConfig[] {
    return this._otherAssetsInCustody;
  }

  public get authorizableAssets(): AssetConfig[] | undefined {
    return this._authorizableAssets;
  }

  public get allInitialAssets(): AssetConfig[] {
    return this.otherAssetsInCustody.concat([this.rootAsset]);
  }

  public get requiredAssets(): AssetConfig[] {
    return [this.rootAsset].concat(this.otherAssetsInCustody.filter((asset) => asset.required));
  }

  public onMount(server: BaseServer): void {}

  public async onReady(server: BaseServer): Promise<void> {}

  public onUnmount(server: BaseServer): void {}
}
