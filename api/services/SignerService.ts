import * as Stellar from 'stellar-sdk';
import { BaseError, BaseServer, Logger, Service, ServiceOptions } from 'ts-framework-common';
import { CacheService } from './cache';
import { logger } from '../../config/logger.config';

export interface SignerConfig {
  secretSeed: string;
  publicKey?: string;
}

export interface SignerServiceOptions extends ServiceOptions {
  channelAccounts: SignerConfig[];
}

export class SignerService extends Service {
  private static instance: SignerService;

  private _channelAccounts: any[] = [];

  private channelIdx: number = -1;

  constructor(public readonly options: SignerServiceOptions) {
    super(options);
    this.logger = options.logger || Logger.getInstance();
  }

  public static initialize(options: SignerServiceOptions) {
    return (this.instance = new SignerService(options));
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError('SignerService is not initialized yet.');
    }

    return this.instance;
  }

  public async onInit(server: BaseServer): Promise<void> {
    this.setChannelAccounts(this.options);
  }

  private async setChannelAccounts(options: SignerServiceOptions) {
    try {
      const commands = [];
      commands.push(['del', 'channel']);

      this._channelAccounts = options.channelAccounts;
      for (let _i = 0; _i < this._channelAccounts.length; _i++) {
        commands.push(['rpush', 'channel', _i]);
      }

      await CacheService.getInstance().multi(commands);
    } catch (error) {
      logger.error('Could not setup index channel account into redis ', error);
    }
  }

  public async getNextChannelAccount(): Promise<string> {
    try {
      this.channelIdx = await CacheService.getInstance().round('channel');
      return this._channelAccounts[this.channelIdx].secretSeed;
    } catch (error) {
      logger.error('Could not retriave index channel account from redis', error);

      // in case of redis failure try to keep round robin as usual
      this.channelIdx += 1;
      if (this.channelIdx >= this.options.channelAccounts.length) {
        this.channelIdx = 0;
      }

      return this._channelAccounts[this.channelIdx].secretSeed;
    }
  }

  public async clearChannelAccounts(): Promise<void> {}

  public onMount(server: BaseServer): void {}

  public async onReady(server: BaseServer): Promise<void> {}

  public onUnmount(server: BaseServer): void {}
}
