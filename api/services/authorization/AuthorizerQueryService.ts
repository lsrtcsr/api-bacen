import {
  PaymentStatus,
  PaymentType,
  TransactionStatus,
  TransactionType,
  TransitoryAccountType,
} from '@bacen/base-sdk';
import { BaseError, Logger } from 'nano-errors';
import { Connection, QueryRunner } from 'typeorm';
import { ForbiddenRequestError, UninitializedServiceError } from '../../errors';
import { Asset, AssetRegistration, Consumer, PrepareTransactionOptions, Transaction, User, Wallet } from '../../models';
import { AssetService } from '../AssetService';

export interface UpdateBlockedBalanceOptions {
  /** The id of the Asset to which the assetRegistration belongs */
  asset: Asset;

  /** The id of the Wallet to which the assetRegistration belongs */
  wallet: Wallet;

  /** The amount to be blocked from the balance. */
  amount: number;
}

export class AuthorizerQueryService {
  private static instance: AuthorizerQueryService;

  constructor(private readonly connection: Connection) {}

  public static initialize(connection: Connection) {
    return (this.instance = new AuthorizerQueryService(connection));
  }

  public static getInstance() {
    if (!this.instance) {
      throw new UninitializedServiceError('AuthorizationService');
    }

    return this.instance;
  }

  public async transaction<T>(runInTransaction: (qr: QueryRunner) => Promise<T>, runner?: QueryRunner): Promise<T> {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    if (queryRunner && queryRunner.isReleased)
      throw new BaseError('Query runner already released. Cannot run queries anymore.');

    if (queryRunner && queryRunner.isTransactionActive)
      throw new BaseError('Cannot start transaction because its already started');

    let result: T;

    try {
      await queryRunner.startTransaction();
      result = await runInTransaction(queryRunner);
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      try {
        queryRunner.rollbackTransaction();
      } catch (rollbackErr) {}
      throw err;
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getAuthorizableAssets(runner?: QueryRunner): Promise<Asset[] | undefined> {
    const assetService = AssetService.getInstance();
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    if (!assetService.authorizableAssets) return undefined;

    const authorizableAssetsCodes = assetService.authorizableAssets.map((assetConfig) => assetConfig.code);

    try {
      const queryResult = await queryRunner.query(
        `
SELECT
"asset"."id" AS "asset__id",
"asset"."code" AS "asset__code",
"issuer"."id" AS "issuer__id",
"issuer"."stellar" AS "issuer__stellar"
FROM "assets" "asset"
INNER JOIN "wallets" "issuer" ON "issuer"."id" = "asset"."issuerId"
WHERE "asset"."code" = ANY ($1)`,
        [authorizableAssetsCodes],
      );

      const assets = queryResult.map((result: any) => {
        return new Asset({
          issuer: new Wallet({
            id: result.issuer__id,
            stellar: result.issuer__stellar,
          }),
          id: result.asset__id,
          code: result.asset__code,
        });
      });

      return assets;
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getAssetByCode(asset: string, runner?: QueryRunner): Promise<Asset | undefined> {
    const assetService = AssetService.getInstance();
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    if (!assetService.authorizableAssets) return undefined;

    try {
      const queryResult = await queryRunner.query(
        `
SELECT
"asset"."id" AS "asset__id",
"asset"."code" AS "asset__code",
"asset"."provider" AS "asset__provider",
"issuer"."id" AS "issuer__id",
"issuer"."stellar" AS "issuer__stellar"
FROM "assets" "asset"
INNER JOIN "wallets" "issuer" ON "issuer"."id" = "asset"."issuerId"
WHERE "code" = $1`,
        [asset],
      );

      return new Asset({
        issuer: new Wallet({
          id: queryResult[0]?.issuer__id,
          stellar: queryResult[0]?.issuer__stellar,
        }),
        id: queryResult[0]?.asset__id,
        code: queryResult[0]?.asset__code,
        provider: queryResult[0]?.asset__provider,
      });
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getWalletById(walletId: string, runner?: QueryRunner) {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    try {
      const queryResult = (
        await queryRunner.query(
          `
SELECT
  "wallet"."id" AS "wallet__id",
  "wallet"."created_at" AS "wallet__created_at",
  "wallet"."updated_at" AS "wallet__updated_at",
  "wallet"."deleted_at" AS "wallet__deleted_at",
  "wallet"."stellar" AS "wallet__stellar",
  "wallet"."additionalData" AS "wallet__additionalData", 
  "user"."id" AS "user__id",
  "consumer"."id" AS "consumer__id"
FROM "wallets" "wallet"
INNER JOIN "users" "user" ON "user"."id"="wallet"."userId"
INNER JOIN "consumers" "consumer" ON "consumer"."userId"="user"."id"
WHERE "wallet"."id" = $1
`,
          [walletId],
        )
      )[0];

      if (!queryResult) {
        return undefined;
      }
      const consumer = new Consumer({ id: queryResult.consumer__id });
      const user = new User({ consumer, id: queryResult.user__id });
      const wallet = new Wallet({
        user,
        id: queryResult.wallet__id,
        createdAt: new Date(queryResult.wallet__created_at),
        updatedAt: new Date(queryResult.wallet__updated_at),
        deletedAt: new Date(queryResult.wallet__deleted_at),
        stellar: queryResult.wallet__stellar,
        additionalData: queryResult.wallet__additionalData,
      });

      return wallet;
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getWalletPendingDebts(walletId: string, assetId: string, runner?: QueryRunner): Promise<number> {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    try {
      const balance = await queryRunner.query(
        `
SELECT "pendingDebtAmount"
FROM wallet_pending_balance_view
WHERE "walletId"=$1
AND "asset"=$2`,
        [walletId, assetId],
      );

      return parseFloat(balance[0]?.pendingDebtAmount ?? 0);
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getWalletPendingCredits(walletId: string, assetId: string, runner?: QueryRunner): Promise<number> {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    try {
      const balance = await queryRunner.query(
        `
SELECT "pendingCreditAmount"
FROM wallet_pending_balance_view
WHERE "walletId"=$1
AND "asset"=$2`,
        [walletId, assetId],
      );

      return parseFloat(balance[0]?.pendingCreditAmount ?? 0);
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async getRootTransitoryWalletByType(
    type: TransitoryAccountType,
    runner?: QueryRunner,
  ): Promise<Wallet | undefined> {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);

    try {
      const queryResult = (
        await queryRunner.query(
          `
SELECT
  "wallet"."id" AS "wallet__id",
  "wallet"."created_at" AS "wallet__created_at",
  "wallet"."updated_at" AS "wallet__updated_at",
  "wallet"."deleted_at" AS "wallet__deleted_at",
  "wallet"."stellar" AS "wallet__stellar",
  "wallet"."additionalData" AS "wallet__additionalData"
FROM "wallets" "wallet"
WHERE "wallet"."additionalData"->>'isTransitoryAccount' = 'true'
AND "wallet"."additionalData"->>'transitoryAccountType' = $1
`,
          [type],
        )
      )[0];

      return queryResult
        ? new Wallet({
            id: queryResult.wallet__id,
            createdAt: new Date(queryResult.wallet__created_at),
            updatedAt: new Date(queryResult.wallet__updated_at),
            deletedAt: new Date(queryResult.wallet__deleted_at),
            stellar: queryResult.wallet__stellar,
            additionalData: queryResult.wallet__additionalData,
          })
        : undefined;
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  /**
   * Updates the blockedBalance for the asset registration belonging to a (wallet, asset) pair.
   *
   * @param options The UpdateBlockedBalanceOptions.
   * @param runner The queryRunner to be used for the queries.
   */
  public async updateBlockedBalance(
    options: UpdateBlockedBalanceOptions,
    runner?: QueryRunner,
  ): Promise<AssetRegistration> {
    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);
    const { wallet, asset, amount } = options;
    const logger = Logger.getInstance();

    logger.debug('Updating blocked balance', {
      amount,
      wallet: wallet.id,
      asset: asset.code,
    });

    try {
      const updateQueryResult = await queryRunner.query(
        `
UPDATE "asset_registrations"
SET "blocked_balance" = "blocked_balance" + $1
WHERE "walletId" = $2 AND "assetId" = $3
RETURNING id AS id
  , "walletId" AS "walletId"
  , "assetId" AS "assetId"
  , "blocked_balance" AS "blockedBalance" 
  , "created_at" AS "createdAt"
  , "updated_at" AS "updatedAt"
  , "deleted_at" AS "deletedAt"
`,
        [amount, wallet.id, asset.id],
      );

      // UPDATE queries return two sets of results, the first is the actual returned record and the second is the
      // number of updated records, so in order to access the returned values we need to access queryResult[0][0]
      // instead of simply queryResult[0] as is the usual.
      return new AssetRegistration({
        asset,
        wallet,
        id: updateQueryResult[0][0].id,
        walletId: updateQueryResult[0][0].walletId,
        assetId: updateQueryResult[0][0].assetId,
        blockedBalance: updateQueryResult[0][0].blockedBalance,
        createdAt: updateQueryResult[0][0].createdAt,
        updatedAt: updateQueryResult[0][0].updatedAt,
        deletedAt: updateQueryResult[0][0].deletedAt,
      });
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  public async prepareTransaction(options: PrepareTransactionOptions, runner?: QueryRunner): Promise<Transaction> {
    /*
     * This method makes a series of assumptions in order to simplify the flow from the actual Transaction.prepare
     * method. Keep them in mind in case any of them changes this method has to be adapted
     *  - Authorizer transactions will always be of a single asset
     *  - The transaction asset will always have a provider
     *  - This method would always be called with authorizeProvider(the second param to Transaction.prepare) = false
     *  - The authorizer asset will always be one for which we control the source of truth
     */

    const { queryRunner, runnerCreatedByUs } = this.getQueryRunner(runner);
    const { source, recipients, type } = options;

    try {
      // We can be fairly certain that authorizer transactions will be of a single asset
      const { asset } = recipients[0];

      // prevents it from being done in duplicate
      if (
        options.type === PaymentType.TRANSACTION_REVERSAL &&
        options.additionalData &&
        options.additionalData.originalTransaction
      ) {
        const { count } = (
          await queryRunner.query(
            `
SELECT COUNT(*) AS count
FROM "transactions" "transaction"
WHERE "transaction"."additional_data"->>'reversal' = $1,
AND "transaction"."additional_data"->>'originalTransaction' = $2
`,
            [true, options.additionalData.originalTransaction],
          )
        )[0];

        if (count > 0) {
          throw new ForbiddenRequestError('Transaction already reversed', {
            id: options.additionalData.originalTransaction,
          });
        }
      }

      const ids = (
        await queryRunner.query(
          `
INSERT INTO "transactions"
("id", "created_at", "updated_at", "deleted_at", "transaction_type", "sourceId", "created_by", "additional_data")
VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, $1, $2, DEFAULT, $3)
RETURNING "id"
`,
          [TransactionType.PAYMENT, source.id, options.additionalData ?? {}],
        )
      ).map((row) => row.id);

      await queryRunner.query(
        `
INSERT INTO "transaction_states"
("id", "created_at", "updated_at", "deleted_at", "status", "additionalData", "transactionId")
VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, $1, DEFAULT, $2)
RETURNING "id"
`,
        [TransactionStatus.PENDING, ids[0]],
      );

      const paymentColumns = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
        'type',
        'amount',
        'transactionId',
        'destinationId',
        'cardId',
        'assetId',
        'status',
        'schedule_for',
      ];
      const paymentSchemas = recipients.map((recipient) => ({
        type: recipient.type || type,
        amount: recipient.amount,
        assetId: recipient.asset.id,
        destinationId: recipient.wallet.id,
        schedule_for: recipient.scheduleFor,
        transactionId: ids[0],
        status: recipient.status || PaymentStatus.AUTHORIZED,
      }));

      await queryRunner.query(
        `
INSERT INTO "payments"
${this.buildInsertExpression(paymentColumns, paymentSchemas)}
RETURNING "id"
`,
        this.buildInsertParams(paymentColumns, paymentSchemas),
      );

      await queryRunner.query(
        `
INSERT INTO "transaction_states"
("id", "created_at", "updated_at", "deleted_at", "status", "additionalData", "transactionId")
VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, $1, $2, $3)
RETURNING "id"
`,
        [TransactionStatus.AUTHORIZED, { authorized: [], skipped: [] }, ids[0]],
      );

      return new Transaction({ id: ids[0] });
    } finally {
      if (runnerCreatedByUs) await queryRunner.release();
    }
  }

  protected getQueryRunner(runner?: QueryRunner) {
    let runnerCreatedByUs = false;
    let queryRunner: QueryRunner;

    if (runner) {
      queryRunner = runner;
    } else {
      queryRunner = this.connection.createQueryRunner();
      runnerCreatedByUs = true;
    }

    return { runnerCreatedByUs, queryRunner };
  }

  protected buildInsertExpression(columns: string[], entities: object[]) {
    let expression = '(';
    expression += columns.map((colName) => `"${colName}"`).join(', ');
    expression += ')\nVALUES';

    let paramCount = 0;
    for (let i = 0; i < entities.length; i++) {
      const entity = entities[i];
      expression += '\n(';

      const values = [];
      for (const columnName of columns) {
        if (!(columnName in entity) || entity[columnName] === undefined || entity[columnName] === null) {
          values.push('DEFAULT');
        } else {
          values.push(`$${paramCount + 1}`);
          paramCount += 1;
        }
      }

      expression += values.join(', ');
      expression += ')';

      if (i !== entities.length - 1) {
        expression += ',';
      }
    }

    return expression;
  }

  protected buildInsertParams(columns: string[], entities: object[]): any[] {
    const values = [];

    for (const entity of entities) {
      for (const columnName of columns) {
        if (columnName in entity && entity[columnName] !== undefined && entity[columnName] !== null) {
          values.push(entity[columnName]);
        }
      }
    }

    return values;
  }
}
