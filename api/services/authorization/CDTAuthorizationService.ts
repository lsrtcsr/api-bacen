import { PaymentType, TransactionStatus } from '@bacen/base-sdk';
import * as moment from 'moment';
import { BaseError, Logger } from 'nano-errors';
import config from '../../../config';
import { Transaction, TRANSACTION_REFUNDABLE_STATES, TRANSACTION_REVERSIBLE_STATES } from '../../models';
import { Trace } from '../../utils';
import { AuthorizationService } from './AuthorizationService';
import { AuthorizationErrorMessage, AuthorizerEvent } from './enums';
import { AuthorizationRequestResponse, DryRunDetails, DryRunResponse } from './types';
import ResultParser from './util/ResultParser';

export interface CDTAuthorizationPayload {
  walletId: string;
  asset: string;
  payload: {
    transactionUUID: string;
    merchantCode: string;
    merchantName: string;
    password: string;
    branch: string;
    account: string;
    NSU: string;
    cardNumber: string;
    mcc: string;
    transmissionDateTime: string;
    entryModeCode: string;
    idTerminal: string;
    taxes: number;
    originalCurrencyCode: string;
    settlementCurrencyCode: string;
    billingCurrencyCode: string;
    originalAmount: number;
    settlementAmount: number;
    billingAmount: number;
    totalBillingAmount: number;
    fxUSDBRL: number;
    operationType: 'PURCHASE' | 'WITHDRAWAL';
  };
}

export enum CDTAuthorizationResponseCodes {
  SUCCESS = '00',
  UNAUTHORIZED = '01',
  ERROR = '06',
  TECHNICAL_FAILURE = '07',
  TRANSACTION_IN_PROGRESS = '09',
  PLEASE_REPEAT = '19',
  BLOCKED_ACCOUNT = '34',
  INCONSISTENT_DATA = '40',
  ACCOUNT_DOES_NOT_EXIST = '46',
  INSUFICCIENT_BALANCE = '51',
  EXCEEDS_DAILY_LIMIT = '61',
  TRANSACTION_DOES_NOT_EXIST = '80',
  INVALID_BILLING_CURRENCY = '40',
  CANCELED_TRANSACTION = '78',
}

export interface CDTAuthorizationResponse {
  result: CDTAuthorizationResponseCodes;
}

export class CDTAuthorizationService {
  private readonly AUTHORIZATION_EXECUTION_INTERVAL = moment.duration(10, 'seconds');

  private static instance?: CDTAuthorizationService;

  public static initialize(): CDTAuthorizationService {
    return (this.instance = new CDTAuthorizationService());
  }

  public static getInstance(): CDTAuthorizationService {
    if (!this.instance) throw new BaseError('AuthorizationService has not been initialize yet.');

    return this.instance;
  }

  @Trace()
  public async revert(transactionId: string): Promise<CDTAuthorizationResponse> {
    const transaction = await Transaction.createQueryBuilder('t')
      .innerJoinAndSelect('t.source', 'source')
      .innerJoinAndSelect('t.payments', 'payments')
      .innerJoinAndSelect('payments.asset', 'asset')
      .innerJoinAndSelect('t.states', 'states')
      .where("t.additional_data->>'externalId' = :transactionId", { transactionId })
      .getOne();

    if (!transaction) {
      return {
        result: CDTAuthorizationResponseCodes.TRANSACTION_DOES_NOT_EXIST,
      };
    }
    let result;

    if (transaction.status === TransactionStatus.REVERSED) {
      return { result: CDTAuthorizationResponseCodes.CANCELED_TRANSACTION };
    }
    if ([TransactionStatus.PENDING, ...TRANSACTION_REVERSIBLE_STATES].includes(transaction.status)) {
      result = await AuthorizationService.getInstance().authorize({
        event: AuthorizerEvent.AUTHORIZATION_REVERSAL,
        type: PaymentType.AUTHORIZED_CARD_REVERSAL,
        amount: transaction.payments[0].amount,
        asset: transaction.payments[0].asset.code,
        walletId: transaction.source.id,
        transactionId: transaction.id,
      });
    } else if (TRANSACTION_REFUNDABLE_STATES.includes(transaction.status)) {
      const reversalCount = await Transaction.createQueryBuilder('t')
        .where(`additional_data->>'reversal' = :reversal`, { reversal: true })
        .andWhere(`additional_data->>'originalTransaction' = :hash`, { hash: transaction.additionalData.hash })
        .getCount();

      if (reversalCount > 0) {
        return { result: CDTAuthorizationResponseCodes.CANCELED_TRANSACTION };
      }

      result = await AuthorizationService.getInstance().authorize({
        event: AuthorizerEvent.REFUND,
        type: PaymentType.AUTHORIZED_CARD_REVERSAL,
        additionalData: {
          reversal: true,
          originalTransaction: transaction.additionalData.hash,
        },
        amount: transaction.payments[0].amount,
        asset: transaction.payments[0].asset.code,
        walletId: transaction.source.id,
        transactionId: transaction.id,
      });
    } else {
      return { result: CDTAuthorizationResponseCodes.TECHNICAL_FAILURE };
    }

    if (result && !result.reason && !result.authorized) {
      return {
        result: CDTAuthorizationResponseCodes.TECHNICAL_FAILURE,
      };
    }

    return result.reason;
  }

  @Trace()
  public async authorize(payload: CDTAuthorizationPayload): Promise<CDTAuthorizationResponse> {
    if (!config.authorizer.allowedCurrencyCodes.includes(payload.payload.billingCurrencyCode)) {
      return { result: CDTAuthorizationResponseCodes.INVALID_BILLING_CURRENCY };
    }

    const amount = payload.payload.totalBillingAmount;

    const result = await AuthorizationService.getInstance().authorize({
      asset: payload.asset,
      event: AuthorizerEvent.AUTHORIZATION,
      type: PaymentType.AUTHORIZED_CARD,
      amount: amount.toFixed(2),
      walletId: payload.walletId,
      additionalData: {
        raw: payload.payload,
        externalId: payload.payload.transactionUUID,
        merchantName: payload.payload.merchantName,
        merchantCode: payload.payload.merchantCode,
      },
    });

    if (!result.reason && !result.authorized) {
      return {
        result: CDTAuthorizationResponseCodes.TECHNICAL_FAILURE,
      };
    }
    if (result.authorized && result.transactionId) {
      setTimeout(
        CDTAuthorizationService.executeAuthorization.bind(this, result, payload.walletId),
        this.AUTHORIZATION_EXECUTION_INTERVAL.asMilliseconds(),
      );
    }

    return ResultParser.parse(result.authorized, result.reason);
  }

  private static async executeAuthorization(result: AuthorizationRequestResponse, walletId: string): Promise<void> {
    const executionResult = await AuthorizationService.getInstance().authorize({
      walletId,
      event: AuthorizerEvent.SETTLEMENT,
      transactionId: result.transactionId,
      asset: null,
      amount: null,
    });

    if (!executionResult.authorized) {
      // TODO: Do something
    }
  }

  public async dryRun(payload: CDTAuthorizationPayload): Promise<DryRunResponse> {
    const details: DryRunDetails[] = [];

    const amount = payload.payload.totalBillingAmount;
    let genericAuthorizerResponse: Omit<DryRunResponse, 'responseCode'>;

    if (!amount) {
      throw new BaseError('Authorization amount is undefined');
    }

    try {
      genericAuthorizerResponse = await AuthorizationService.getInstance().dryRun({
        asset: payload.asset,
        event: AuthorizerEvent.AUTHORIZATION_DRY_RUN,
        type: PaymentType.AUTHORIZED_CARD,
        amount: amount.toFixed(2),
        walletId: payload.walletId,
        additionalData: {
          raw: payload.payload,
          externalId: payload.payload.transactionUUID,
          merchantName: payload.payload.merchantName,
          merchantCode: payload.payload.merchantCode,
        },
      });
    } catch (err) {
      Logger.getInstance().error(err);

      return {
        authorized: false,
        responseCode: CDTAuthorizationResponseCodes.TECHNICAL_FAILURE,
        raw: payload,
      };
    }

    if (!config.authorizer.allowedCurrencyCodes.includes(payload.payload.billingCurrencyCode)) {
      details.push({
        billingCurrencyCode: payload.payload.billingCurrencyCode,
        supportedCurrencyCodes: config.authorizer.allowedCurrencyCodes,
        ...genericAuthorizerResponse.details,
      });
      return {
        authorized: false,
        responseCode: CDTAuthorizationResponseCodes.INVALID_BILLING_CURRENCY,
        errors: [
          {
            reason: AuthorizationErrorMessage.CURRENCY_NOT_SUPPORTED,
          },
        ],
        details,
        raw: payload,
      };
    }

    if (genericAuthorizerResponse.authorized) {
      return {
        authorized: genericAuthorizerResponse.authorized,
        responseCode: CDTAuthorizationResponseCodes.SUCCESS,
        details: genericAuthorizerResponse.details,
        raw: payload,
      };
    }

    const { authorized, details: dt, errors } = genericAuthorizerResponse;

    return {
      authorized,
      responseCode: ResultParser.parseErrors(authorized, errors),
      details: dt,
      errors,
      raw: payload,
    };
  }
}
