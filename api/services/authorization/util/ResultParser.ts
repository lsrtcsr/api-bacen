import { DryRunError } from '../types';
import { CDTAuthorizationResponse, CDTAuthorizationResponseCodes } from '../CDTAuthorizationService';
import { AuthorizationErrorMessage } from '../enums';

export default class ResultParser {
  static parseErrors(authorized: boolean, errors: DryRunError[]): CDTAuthorizationResponseCodes {
    const reason = errors.map((error) => error.reason);

    return ResultParser.parse(authorized, reason).result;
  }

  static parse(authorized: boolean, reason: AuthorizationErrorMessage[]): CDTAuthorizationResponse {
    if (authorized) {
      return {
        result: CDTAuthorizationResponseCodes.SUCCESS,
      };
    }

    if (
      reason.includes(AuthorizationErrorMessage.INSUFICCIENT_BALANCE) ||
      reason.includes(AuthorizationErrorMessage.PENDING_TRANSACTIONS)
    ) {
      return { result: CDTAuthorizationResponseCodes.INSUFICCIENT_BALANCE };
    }
    if (
      reason.includes(AuthorizationErrorMessage.WALLET_NOT_READY) ||
      reason.includes(AuthorizationErrorMessage.CONSUMER_NOT_READY) ||
      reason.includes(AuthorizationErrorMessage.USER_NOT_READY)
    ) {
      return { result: CDTAuthorizationResponseCodes.BLOCKED_ACCOUNT };
    }
    if (
      reason.includes(AuthorizationErrorMessage.FSM_ERROR) ||
      reason.includes(AuthorizationErrorMessage.INTERNAL_ERROR)
    ) {
      return { result: CDTAuthorizationResponseCodes.TECHNICAL_FAILURE };
    }
    if (
      reason.includes(AuthorizationErrorMessage.NON_REVERSIBLE_STATE) ||
      reason.includes(AuthorizationErrorMessage.NON_REFUNDABLE_STATE)
    ) {
      return { result: CDTAuthorizationResponseCodes.INCONSISTENT_DATA };
    }
    if (reason.includes(AuthorizationErrorMessage.EXCEEDS_DAILY_LIMIT)) {
      return { result: CDTAuthorizationResponseCodes.EXCEEDS_DAILY_LIMIT };
    }

    return { result: CDTAuthorizationResponseCodes.UNAUTHORIZED };
  }
}
