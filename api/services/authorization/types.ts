import { LoggerInstance } from 'nano-errors';
import { PaymentType, TransitoryAccountType } from '@bacen/base-sdk';
import { Asset, Wallet, User } from '../../models';
import { AuthorizerEvent, AuthorizationErrorMessage } from './enums';
import { CDTAuthorizationResponseCodes } from './CDTAuthorizationService';
import { AuthorizationRequestDto } from '../../schemas/dto/AuthorizationRequestDto';

export interface AuthorizationServiceOptions {
  logger: LoggerInstance;
}

export interface AuthorizationRequestPayload {
  event: AuthorizerEvent;
  type?: PaymentType;
  asset: string;
  amount: string;
  walletId: string;
  transactionId?: string;
  transitoryAccountType?: TransitoryAccountType;
  additionalData?: any;
}

export interface EntitiesProp {
  wallet?: Wallet;
  assets?: Asset[];
}

export type AuthorizerFn = (
  payload: AuthorizationRequestDto,
  entities?: EntitiesProp,
) => Promise<AuthorizationRequestResponse>;

export interface AuthorizationRequestResponse {
  authorized: boolean;
  reason?: AuthorizationErrorMessage[];
  asset?: string;

  /** Used in settlement and  */
  transactionId?: string;

  /** Used in refund authorizations */
  originalTransactionId?: string;
}

export interface DryRunDetails {
  supportedCurrencyCodes?: string[];

  billingCurrencyCode?: string;

  asset?: string;

  transactionAmount?: string;

  blockchainBalance?: string;

  pendingDebts?: string;

  pendingCredits?: string;

  consolidatedBalance?: string;

  authorizableBalance?: string;

  totalLimit?: string;

  consumedLimit?: string;

  availableLimit?: string;
}

export interface DryRunError {
  reason: AuthorizationErrorMessage;
  explanation?: string;
}

export interface DryRunResponse {
  authorized: boolean;

  responseCode: CDTAuthorizationResponseCodes;

  details?: DryRunDetails[];

  errors?: DryRunError[];

  raw?: object;
}
