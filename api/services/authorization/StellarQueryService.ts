import { Pool } from 'pg';
import { BaseError, BaseServer, Service, ServiceOptions } from 'ts-framework-common';
import Config from '../../../config';
import { Asset, Wallet } from '../../models';
import { Trace } from '../../utils';

const ONE_STROUP = 1e-7;

export interface StellarQueryServiceOptions extends ServiceOptions {}

export class StellarQueryService extends Service {
  private static instance?: StellarQueryService;

  private readonly connectionPool?: Pool;

  private isConnected = false;

  constructor(opts: StellarQueryServiceOptions) {
    super(opts);

    if (Config.stellarDb.host && Config.stellarDb.user && Config.stellarDb.database && Config.stellarDb.password) {
      this.connectionPool = new Pool({
        host: Config.stellarDb.host,
        user: Config.stellarDb.user,
        port: Config.stellarDb.port,
        database: Config.stellarDb.database,
        password: Config.stellarDb.password,
        max: Config.stellarDb.poolSize,
        min: Math.min(Config.stellarDb.poolSize, 2),
      });

      this.connectionPool.on('error', (err) => {
        this.logger.error(`Stellar query service connection pool error: ${err.message}`, err);

        // TODO: Should we close the connection pool in case of errors?
        process.exit(-1);
      });
    } else {
      this.logger.debug(
        'StellarQueryService could not establish a connection to the stellar-  DB due to missing credentials',
      );
    }
  }

  public get isReady(): boolean {
    return this.connectionPool && this.isConnected;
  }

  public static initialize(opts: StellarQueryServiceOptions) {
    return (this.instance = new StellarQueryService(opts));
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError('Service not initialized', { service: 'StellarQueryService' });
    }

    return this.instance;
  }

  public describe() {
    return {
      name: 'StellarQueryService',
    };
  }

  public onMount(server: BaseServer): void {}

  public async onInit(server: BaseServer): Promise<void> {
    if (this.connectionPool) {
      const isReadOnly = await this.isReadOnly();

      if (!isReadOnly) {
        if (Config.stellarDb.ensureSlave) {
          throw new BaseError('Connection to stellar-  DB is not read-only');
        } else {
          this.logger.warn('Stellar query service connection to stellar-  DB is not read-only');
        }
      }

      this.isConnected = true;
    }
  }

  public async onReady(server: BaseServer): Promise<void> {}

  public async onUnmount(server: BaseServer): Promise<void> {
    if (this.connectionPool) {
      await this.connectionPool.end();
    }
  }

  protected async query<R = any>(query: string, params?: any[]): Promise<R[]> {
    if (!this.connectionPool) {
      throw new BaseError('Could not query stellar-  DB as it is not connected, check your credentials');
    }

    const client = await this.connectionPool.connect();

    try {
      const queryResult = await client.query<R>(query, params);
      return queryResult.rows;
    } finally {
      client.release();
    }
  }

  public async isReadOnly(): Promise<boolean> {
    const queryResult = await this.query<{ isReadOnly: boolean }>(`SELECT pg_is_in_recovery() AS "isReadOnly"`);
    return queryResult[0].isReadOnly;
  }

  @Trace()
  public async getBalance(wallet: Wallet, asset: Asset): Promise<number | undefined> {
    try {
      const balanceRows = await this.query<{ balance: string }>(
        `SELECT "balance" AS balance FROM trustlines WHERE "accountid"=$1 AND "issuer"=$2 AND "assetcode"=$3`,
        [wallet.stellar.publicKey, asset.issuer.stellar.publicKey, asset.code],
      );

      if (!balanceRows || !balanceRows.length) {
        return undefined;
      }

      return parseInt(balanceRows[0].balance) * ONE_STROUP;
    } catch (exception) {
      throw exception;
    }
  }

  @Trace()
  public async getBalanceForMultipleAssets(
    wallet: Wallet,
    assets: Asset[],
  ): Promise<{ code: string; balance: number }[]> {
    const assetsHaveSameIssuer = assets.every((asset, _, arr) => asset.issuer.id === arr[0].issuer.id);
    const assetsWhereClause = assetsHaveSameIssuer
      ? 'AND "issuer" = $2 AND "assetcode" = ANY($3)'
      : `AND (${assets
          .map((_, idx) => `("issuer" = $${2 * idx + 2} AND "assetcode" = $${2 * idx + 3})`)
          .join(' OR ')})`;

    const assetsParams = assetsHaveSameIssuer
      ? [assets[0].issuer.stellar.publicKey, assets.map((asset) => asset.code)]
      : assets.flatMap((asset) => [asset.issuer.stellar.publicKey, asset.code]);

    try {
      const balanceRows = await this.query<{ asset_code: string; balance: string }>(
        `SELECT "balance" AS balance, "assetcode" AS asset_code FROM trustlines WHERE "accountid" = $1 ${assetsWhereClause}`,
        [wallet.stellar.publicKey, ...assetsParams],
      );

      return balanceRows.map((row) => ({
        code: row.asset_code,
        balance: (parseInt(row.balance) ?? 0) * ONE_STROUP,
      }));
    } catch (exception) {
      throw exception;
    }
  }

  public getConnectionStatus(): { total: number; idle: number; waiting: number } {
    return {
      total: this.connectionPool.totalCount,
      idle: this.connectionPool.idleCount,
      waiting: this.connectionPool.waitingCount,
    };
  }
}
