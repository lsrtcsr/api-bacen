export enum AuthorizerEvent {
  AUTHORIZATION = 'authorization',
  AUTHORIZATION_DRY_RUN = 'authorization_dry_run',
  AUTHORIZATION_REVERSAL = 'authorization_reversal',
  SETTLEMENT = 'settlement',
  REFUND = 'refund',
}

export enum AuthorizationErrorMessage {
  INSUFICCIENT_BALANCE = 'Insufficient balance',
  PENDING_TRANSACTIONS = 'Insufficient balance due to pending transaction(s)',
  TRANSACTION_NOT_FOUND = 'Transaction not found',
  NON_REVERSIBLE_STATE = 'Transaction is not in a reversible state',
  NOT_AUTHORIZED = 'Transaction must be authorized in order to be settled',
  NON_REFUNDABLE_STATE = 'Transaction is in a non-refundable state',
  INTERNAL_ERROR = 'Authorization failed due to internal error',
  BAD_REQUEST = 'Amount must be positive value',
  FSM_ERROR = 'Could not perform state transition to executed',
  WALLET_NOT_EXISTS = 'Cannot find wallet',
  WALLET_NOT_READY = 'Wallet is not in the required status for this operation',
  USER_NOT_READY = 'User is not in the required status for this operation',
  CONSUMER_NOT_READY = 'Consumer is not in the required status for this operation',
  EXCEEDS_DAILY_LIMIT = 'Transaction exceeds daily limit',
  CURRENCY_NOT_SUPPORTED = 'Billing currency is not supported',
  ASSET_NOT_AUTHORIZABLE = 'Asset is not authorizable',
  ASSET_NOT_FOUND = 'Asset is not authorizable',
}
