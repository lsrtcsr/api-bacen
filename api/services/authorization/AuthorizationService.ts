import {
  PaymentType,
  TransactionAdditionalData,
  TransactionStatus,
  TransitoryAccountType,
  UnleashFlags,
} from '@bacen/base-sdk';
import { UnleashContext, UnleashUtil } from '@bacen/shared-sdk';
import { BaseError, LoggerInstance } from 'nano-errors';
import { getManager } from 'typeorm';
import { TransactionStateMachine } from '../../fsm';
import { LimitType, PrepareTransactionOptions, Transaction, TRANSACTION_REFUNDABLE_STATES, Wallet } from '../../models';
import { AuthorizationRequestDto } from '../../schemas/dto/AuthorizationRequestDto';
import { Trace } from '../../utils';
import { AssetService } from '../AssetService';
import { LimitSettingService } from '../LimitSettingService';
import { TransactionPipelinePublisher } from '../pipeline';
import { AuthorizerQueryService } from './AuthorizerQueryService';
import { AuthorizationErrorMessage, AuthorizerEvent } from './enums';
import { StellarQueryService } from './StellarQueryService';
import {
  AuthorizationRequestResponse,
  AuthorizationServiceOptions,
  AuthorizerFn,
  DryRunDetails,
  DryRunError,
  DryRunResponse,
  EntitiesProp,
} from './types';

export class AuthorizationService {
  private logger: LoggerInstance;

  private authorizationStrategies = new Map<AuthorizerEvent, AuthorizerFn>();

  private static instance?: AuthorizationService;

  private assetService: AssetService;

  constructor(public readonly options: AuthorizationServiceOptions) {
    this.logger = options.logger;

    this.authorizationStrategies.set(AuthorizerEvent.AUTHORIZATION, this.authorizeTransaction);
    this.authorizationStrategies.set(AuthorizerEvent.AUTHORIZATION_REVERSAL, this.authorizeTransactionReversal);
    this.authorizationStrategies.set(AuthorizerEvent.SETTLEMENT, this.authorizeTransactionSettlement);
    this.authorizationStrategies.set(AuthorizerEvent.REFUND, this.authorizeRefund);

    this.assetService = AssetService.getInstance();
  }

  public static initialize(options: AuthorizationServiceOptions): AuthorizationService {
    return (this.instance = new AuthorizationService(options));
  }

  public static getInstance(): AuthorizationService {
    if (!this.instance) throw new BaseError('AuthorizationService has not been initialize yet.');

    return this.instance;
  }

  // Proxy to Transaction.prepare so we can more easily instrument it
  @Trace({ name: 'Transaction.prepare' })
  public prepareTransaction(options: PrepareTransactionOptions, authorizeProvider?: boolean) {
    return Transaction.prepare(options, authorizeProvider);
  }

  @Trace()
  public async authorize(payload: AuthorizationRequestDto): Promise<AuthorizationRequestResponse> {
    const strategy = this.authorizationStrategies.get(payload.event)?.bind(this) as AuthorizerFn;
    if (!strategy) throw new BaseError(`Unrecognized event ${payload.event}`);

    try {
      const { errors, wallet } = await this.checkUserAndWalletState(payload);
      if (errors.length > 0) {
        return {
          authorized: false,
          reason: errors,
        };
      }

      const queryService = AuthorizerQueryService.getInstance();
      const assetsPromise = payload.asset.split(',').map(async (code) => {
        return await queryService.getAssetByCode(code);
      });
      const assets = await Promise.all(assetsPromise);

      if (!assets || assets.length === 0) {
        this.logger.error('Asset not found');

        return {
          authorized: false,
          reason: [AuthorizationErrorMessage.ASSET_NOT_FOUND],
        };
      }

      /*
       * We await result of strategy in here instead of just returning strategy(payload) bc we wanna catch any errors
       * that occur during the call to strategy, if we didn't do it like so we would need to try/catch on the code
       * calling authorize.
       */
      const authorized = await strategy(payload, { wallet, assets });
      return authorized;
    } catch (err) {
      // TODO: create issue
      this.logger.error('Coult not authorize transaction ', err);

      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
      };
    }
  }

  public async dryRun(payload: AuthorizationRequestDto): Promise<Omit<DryRunResponse, 'responseCode'>> {
    const { walletId, amount } = payload;

    const { errors, wallet } = await this.checkUserAndWalletState(payload);
    let details: DryRunDetails[] = [];
    let limits: any[];
    let allConsumed: any[];
    let hasLimits = false;

    const dryRunErrors: DryRunError[] = [];

    if (errors && errors.length) {
      errors.forEach((error) => dryRunErrors.push({ reason: error }));
    }

    const amountValue: number | undefined = this.parseNonNegativeAmount(amount);

    if (!amountValue) {
      dryRunErrors.push({ reason: AuthorizationErrorMessage.BAD_REQUEST });
    }

    const queryService = AuthorizerQueryService.getInstance();

    const assetsPromise = payload.asset.split(',').map(async (code) => {
      return await queryService.getAssetByCode(code);
    });
    const assets = await Promise.all(assetsPromise);

    const authorizableAssets: string[] = this.assetService.authorizableAssets.map((asset) => {
      if (asset.authorizable === true) {
        return asset.code;
      }
    });

    const listOfAssets = assets.map((asset) => {
      return asset.code;
    });

    const hasOnlyAuthorizedAssets = listOfAssets.every((item) => {
      return authorizableAssets.includes(item);
    });

    if (!hasOnlyAuthorizedAssets) {
      dryRunErrors.push({
        reason: AuthorizationErrorMessage.ASSET_NOT_AUTHORIZABLE,
        explanation: 'The received asset is not authorizable',
      });
    }

    const balances = await StellarQueryService.getInstance().getBalanceForMultipleAssets(wallet, assets);

    if (balances === undefined) {
      dryRunErrors.push({
        reason: AuthorizationErrorMessage.ASSET_NOT_FOUND,
        explanation: 'Asset is unavailable or has not been registered into specified wallet',
      });
    }

    const pendingDebtsPromise = assets.map(async (asset) => {
      return {
        asset: asset.code,
        pendingDebts: await queryService.getWalletPendingDebts(walletId, asset.id),
      };
    });

    const pendingCreditsPromises = assets.map(async (asset) => {
      return {
        asset: asset.code,
        pendingCredits: await queryService.getWalletPendingCredits(walletId, asset.id),
      };
    });

    const pendingDebts = await Promise.all(pendingDebtsPromise);
    const pendingCredits = await Promise.all(pendingCreditsPromises);

    const context: UnleashContext = { userId: wallet.user.id, properties: { domainId: wallet.user.domainId } };
    const skipLimits = UnleashUtil.isEnabled(UnleashFlags.DISABLE_AUTHORIZER_LIMITS, context, false);

    if (!skipLimits) {
      const limitsPromise = assets.map(async (asset) => {
        return {
          asset: asset.code,
          limit: await LimitSettingService.getInstance().getLimits(
            LimitType.TRANSACTION_SUM_CURRENT_DAY,
            wallet.user,
            asset.code,
          ),
        };
      });

      const allConsumedPromise = assets.map(async (asset) => {
        return {
          asset: asset.code,
          consumed: await LimitSettingService.getInstance().getValueConsumed({
            type: LimitType.TRANSACTION_SUM_CURRENT_DAY,
            wallet,
            asset,
          }),
        };
      });

      limits = await Promise.all(limitsPromise);
      allConsumed = await Promise.all(allConsumedPromise);
      hasLimits = !!limits.filter((item) => item.limit > 0);
    }

    details = assets.map((asset) => {
      const assetBalance = balances.filter((balance) => balance.code === asset.code)[0].balance;
      const assetPendingCredits = pendingCredits.filter((credit) => credit.asset === asset.code)[0].pendingCredits;
      const assetPendingDebts = pendingDebts.filter((pending) => pending.asset === asset.code)[0].pendingDebts;

      const dryRunDetails: DryRunDetails = {
        asset: asset.code,
        transactionAmount: amount,
        blockchainBalance: assetBalance.toString(),
        pendingCredits: assetPendingCredits.toString(),
        pendingDebts: assetPendingDebts.toString(),
        consolidatedBalance: (Number(assetBalance) + Number(assetPendingCredits) - Number(assetPendingDebts)).toFixed(
          7,
        ),
        authorizableBalance: (Number(assetBalance) - Number(assetPendingDebts)).toFixed(7),
      };

      if (hasLimits) {
        limits.map((limitItem) => {
          const assetConsumed = allConsumed.filter((consumed) => consumed.asset === limitItem.asset)[0].consumed;
          dryRunDetails.consumedLimit = assetConsumed.toFixed(7);
          dryRunDetails.availableLimit = (limitItem.limit - assetConsumed).toFixed(7);
        });
      }
      return dryRunDetails;
    });

    const balance = balances.reduce((sum, item) => sum + item.balance, 0);
    const pendingDebt = pendingDebts.reduce((sum, item) => sum + item.pendingDebts, 0);

    if (!balance || balance < parseFloat(amount)) {
      dryRunErrors.push({
        reason: AuthorizationErrorMessage.INSUFICCIENT_BALANCE,
        explanation: 'Blockchain balance is less than transaction amount',
      });
    }

    /*
     * We assume that all authorized assets have the same base value
     */
    if (balance - pendingDebt < parseFloat(amount)) {
      dryRunErrors.push({
        reason: AuthorizationErrorMessage.INSUFICCIENT_BALANCE,
        explanation: 'Authorizable balance is less than transaction amount',
      });
    }

    if (!skipLimits) {
      const checkLimits = assets.map((asset) => {
        const assetLimit = limits.filter((limit) => limit.asset === asset.code)[0].limit;
        const assetConsumed = allConsumed.filter((consumed) => consumed.asset === asset.code)[0].consumed;
        return {
          asset: asset.code,
          exceeds: assetLimit === 0 ? false : assetConsumed + parseFloat(amount) > assetLimit,
        };
      });

      const hasExceeds = checkLimits.filter((check) => check.exceeds === true)[0];

      if (hasExceeds) {
        dryRunErrors.push({
          reason: AuthorizationErrorMessage.EXCEEDS_DAILY_LIMIT,
          explanation: 'Transaction amount is larger than available limit',
        });
      }
    }

    if (dryRunErrors.length > 0) {
      return {
        authorized: false,
        details,
        errors: dryRunErrors,
      };
    }

    return {
      authorized: true,
      details,
    };
  }

  /*
   * ****************************************************************************************************************
   * **********************************************   PRIVATE METHODS   *********************************************
   * ****************************************************************************************************************
   */
  @Trace()
  private async checkUserAndWalletState({
    walletId,
  }: AuthorizationRequestDto): Promise<{ errors?: AuthorizationErrorMessage[] } & EntitiesProp> {
    const queryService = AuthorizerQueryService.getInstance();
    const wallet = await queryService.getWalletById(walletId);

    const errors = [];

    if (!wallet) {
      errors.push(AuthorizationErrorMessage.WALLET_NOT_EXISTS);
      return {
        errors,
      };
    }

    const [isWalletReady, isUserReady, isConsumerReady] = await Promise.all([
      wallet.isActive(),
      wallet.user.isActive(),
      wallet.user.consumer.isActive(),
    ]);

    if (!isWalletReady) errors.push(AuthorizationErrorMessage.WALLET_NOT_READY);
    if (!isUserReady) errors.push(AuthorizationErrorMessage.USER_NOT_READY);
    if (!isConsumerReady) errors.push(AuthorizationErrorMessage.CONSUMER_NOT_READY);

    return {
      errors,
      wallet,
    };
  }

  @Trace()
  private async authorizeTransaction(
    payload: AuthorizationRequestDto,
    { wallet, assets }: EntitiesProp,
  ): Promise<AuthorizationRequestResponse> {
    const { walletId, amount, type = PaymentType.CARD } = payload;

    const queryService = AuthorizerQueryService.getInstance();

    const listOfAssets = assets.map((asset) => {
      return asset.code;
    });
    const authorizableAssets: string[] = this.assetService.authorizableAssets.map((asset) => {
      if (asset.authorizable === true) return asset.code;
    });

    const hasOnlyAuthorizedAssets = listOfAssets.every((item) => {
      return authorizableAssets.includes(item);
    });

    if (!hasOnlyAuthorizedAssets) {
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.ASSET_NOT_AUTHORIZABLE],
      };
    }

    const amountValue: number | undefined = this.parseNonNegativeAmount(amount);

    if (!amountValue) {
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.BAD_REQUEST],
      };
    }

    const balance = await StellarQueryService.getInstance().getBalanceForMultipleAssets(wallet, assets);

    if (balance.length === 0)
      if (!this.assetService.authorizableAssets.find((asset) => asset.code === payload.asset))
        return {
          authorized: false,
          reason: [AuthorizationErrorMessage.INSUFICCIENT_BALANCE],
        };

    const totalBalance = balance.reduce((count, balance) => (count += balance.balance), 0);

    if (totalBalance < parseFloat(amount))
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.INSUFICCIENT_BALANCE],
      };

    const pendingBalancePromise = assets.map(async (asset) => {
      return {
        asset: asset.code,
        pending: await queryService.getWalletPendingDebts(walletId, asset.id),
      };
    });

    const pendingBalance = await Promise.all(pendingBalancePromise);
    const totalPendingBalance = pendingBalance.map((item) => item.pending).reduce((count, b) => (count += b), 0);

    if (totalBalance - totalPendingBalance < parseFloat(amount))
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.PENDING_TRANSACTIONS],
      };

    const context: UnleashContext = { userId: wallet.user.id, properties: { domainId: wallet.user.domainId } };
    const skipLimits = UnleashUtil.isEnabled(UnleashFlags.DISABLE_AUTHORIZER_LIMITS, context, false);

    if (!skipLimits) {
      const limitsPromise = assets.map(async (asset) => {
        return {
          asset: asset.code,
          limit: await LimitSettingService.getInstance().getLimits(
            LimitType.TRANSACTION_SUM_CURRENT_DAY,
            wallet.user,
            asset.code,
          ),
        };
      });

      const allConsumedPromise = assets.map(async (asset) => {
        return {
          asset: asset.code,
          consumed: await LimitSettingService.getInstance().getValueConsumed({
            type: LimitType.TRANSACTION_SUM_CURRENT_DAY,
            wallet,
            asset,
          }),
        };
      });

      const limits = await Promise.all(limitsPromise);
      const allConsumed = await Promise.all(allConsumedPromise);
      const hasLimits = limits.filter((item) => item.limit > 0);

      if (hasLimits.length !== 0) {
        const checkLimits = assets.map((asset) => {
          const assetLimit = limits.filter((limit) => limit.asset === asset.code)[0].limit;
          const assetConsumed = allConsumed.filter((consumed) => consumed.asset === asset.code)[0].consumed;
          return {
            asset: asset.code,
            exceeds: assetLimit === 0 ? false : assetConsumed + parseFloat(amount) > assetLimit,
          };
        });

        const hasExceeds = checkLimits.filter((check) => check.exceeds === true)[0];

        if (hasExceeds) {
          return {
            authorized: false,
            asset: hasExceeds.asset,
            reason: [AuthorizationErrorMessage.EXCEEDS_DAILY_LIMIT],
          };
        }
      }
    }

    return await queryService.transaction(async (qr) => {
      const transitoryAccountType = payload.transitoryAccountType || TransitoryAccountType.CARD_TRANSACTION;

      const transientWallet = await queryService.getRootTransitoryWalletByType(transitoryAccountType, qr);

      if (!transientWallet) {
        this.logger.error(`root transitory wallet not found`, {
          transitoryAccountType,
          paymentType: type,
        });

        return {
          authorized: false,
          reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
        };
      }

      const assetsList = payload.asset.split(',');
      let remainingAmount = parseFloat(amount);
      const recipients = [];
      for (let i = 0; i < assetsList.length; i++) {
        const asset = assets.find((a) => a.code === assetsList[i]);
        const assetBalance = balance.find((a) => a.code === assetsList[i]);
        const assetPendingBalance = pendingBalance.find((a) => a.asset === assetsList[i]);
        const availableBalance = assetBalance.balance - assetPendingBalance.pending;

        if (remainingAmount < availableBalance) {
          recipients.push({
            asset,
            amount: remainingAmount,
            wallet: transientWallet,
          });
          break;
        } else if (availableBalance > 0) {
          recipients.push({
            asset,
            amount: availableBalance,
            wallet: transientWallet,
          });

          remainingAmount -= availableBalance;
        }
      }

      const authorizationTx = await queryService.prepareTransaction(
        {
          type,
          source: wallet,
          additionalData: payload.additionalData,
          recipients,
        },
        qr,
      );

      return {
        authorized: true,
        transactionId: authorizationTx.id,
      };
    });
  }

  @Trace()
  private async authorizeTransactionReversal(payload: AuthorizationRequestDto): Promise<AuthorizationRequestResponse> {
    const { walletId, amount, transactionId, additionalData } = payload;
    const transaction = await Transaction.findOne(transactionId, {
      relations: [
        'source',
        'source.assetRegistrations',
        'source.assetRegistrations.states',
        'source.user',
        'states',
        'payments',
        'payments.destination',
        'payments.destination.assetRegistrations',
        'payments.destination.assetRegistrations.states',
        'payments.destination.user',
        'payments.asset',
        'payments.asset.issuer',
      ],
    });

    if (!transaction)
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
      };

    const fsm = new TransactionStateMachine(transaction);

    if (!fsm.canGoTo(TransactionStatus.REVERSED))
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.NON_REVERSIBLE_STATE],
      };

    try {
      await fsm.goTo(TransactionStatus.REVERSED, additionalData);
    } catch (err) {
      this.logger.error(`Transaction reversion failed`);
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.INTERNAL_ERROR],
      };
    }

    return {
      transactionId,
      authorized: true,
    };
  }

  @Trace()
  private async authorizeTransactionSettlement(
    payload: AuthorizationRequestDto,
  ): Promise<AuthorizationRequestResponse> {
    const { transactionId } = payload;
    const transaction = await Transaction.findOne(transactionId, {
      relations: [
        'source',
        'source.assetRegistrations',
        'source.assetRegistrations.states',
        'source.user',
        'states',
        'payments',
        'payments.destination',
        'payments.destination.assetRegistrations',
        'payments.destination.assetRegistrations.states',
        'payments.destination.user',
        'payments.asset',
        'payments.asset.issuer',
      ],
    });

    if (!transaction)
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
      };

    if (transaction.status !== TransactionStatus.AUTHORIZED)
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.NOT_AUTHORIZED],
      };

    /*
     * The transition from authorized to executed used to be done in the body of this method, which ended
     * up causing problems due to lack of retry, sometimes transactions would fail to be written to the blockchain
     * with tx_bad_seq errors causing them to get stuck in authorized state and the error being swallowed since for
     * the dock external authorizer this method was run as a timer callback. Removing the state transition from this
     * method we are now relying on the publication to the pipeline being an effective proof of success for writing
     * the transaction to the blockchain. We should have visibility if any errors happen in the pipeline but this is a
     * that we must keep in mind.
     */
    await TransactionPipelinePublisher.getInstance().send(transaction);

    return {
      transactionId,
      authorized: true,
    };
  }

  @Trace()
  private async authorizeRefund(
    payload: AuthorizationRequestDto,
    { assets }: EntitiesProp,
  ): Promise<AuthorizationRequestResponse> {
    const { walletId, amount, transactionId, type } = payload;
    const transaction = await Transaction.findOne(transactionId, {
      relations: [
        'source',
        'source.assetRegistrations',
        'source.assetRegistrations.states',
        'source.user',
        'states',
        'payments',
        'payments.destination',
        'payments.destination.assetRegistrations',
        'payments.destination.assetRegistrations.states',
        'payments.destination.user',
        'payments.asset',
        'payments.asset.issuer',
      ],
    });

    if (!transaction)
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.TRANSACTION_NOT_FOUND],
      };

    if (!TRANSACTION_REFUNDABLE_STATES.includes(transaction.status))
      return {
        authorized: false,
        reason: [AuthorizationErrorMessage.NON_REFUNDABLE_STATE],
      };

    if (!this.assetService.authorizableAssets.find((asset) => payload.asset.split(',').includes(asset.code)))
      return {
        reason: [AuthorizationErrorMessage.ASSET_NOT_FOUND],
        authorized: false,
      };

    const refund = await getManager().transaction(async (tx) => {
      let wallet: Wallet;
      if (payload.transitoryAccountType) {
        wallet = await Wallet.getRootTransitoryAccountWallet(payload.transitoryAccountType, tx);
      } else {
        wallet = transaction.payments[0].destination;
      }

      const operations = transaction.payments.map((payment) => {
        return {
          asset: payment.asset,
          amount: payment.amount,
          wallet: transaction.source,
        };
      });

      const refundTransaction = await this.prepareTransaction({
        source: wallet,
        recipients: operations,
        type,
        additionalData: {
          reversal: true,
          originalTransaction: transaction.additionalData.hash,
        } as TransactionAdditionalData,
        manager: tx,
      });

      await TransactionPipelinePublisher.getInstance().send(refundTransaction);

      return refundTransaction;
    });

    return {
      originalTransactionId: transaction.id,
      transactionId: refund.id,
      authorized: true,
    };
  }

  public parseNonNegativeAmount(amount: string) {
    const parsedAmount: number = parseFloat(amount);
    return parsedAmount <= 0 ? undefined : parsedAmount;
  }
}
