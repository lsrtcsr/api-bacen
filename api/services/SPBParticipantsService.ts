import {
  CustodyFeature,
  CustodyProvider,
  ArrangementParticipantSchema,
  ParticipantStatus,
  Arrangement,
} from '@bacen/base-sdk';
import { BaseError, Service, ServiceOptions } from 'ts-framework-common';
import { ProviderUtil } from '../utils';
import { CacheService } from './cache';
import { DiscoveryService, DiscoveryType } from './discovery';
import { ProviderManagerService } from './provider';
import { AssetService } from './AssetService';

export interface SPBParticipantsServiceOptions extends ServiceOptions {
  spbParticipantsProvider?: CustodyProvider;
}

export class SPBParticipantsService extends Service {
  public options: SPBParticipantsServiceOptions;

  protected static instance: SPBParticipantsService;

  protected static readonly cacheKey = 'SPB_PARTICIPANTS';

  protected static readonly DEFAULT_TTL = 60 * 60 * 24; // 24 hours

  protected spbParticipantsProvider?: CustodyProvider;

  constructor(options: SPBParticipantsServiceOptions = {}) {
    super(options);
    this.spbParticipantsProvider = options.spbParticipantsProvider;
  }

  static initialize(options: SPBParticipantsServiceOptions = {}): SPBParticipantsService {
    return (this.instance = new SPBParticipantsService(options));
  }

  static getInstance() {
    return this.instance ?? (this.instance = new SPBParticipantsService());
  }

  public async getAll(): Promise<ArrangementParticipantSchema[]> {
    const cache = CacheService.getInstance();
    let participants: any = await cache.get(SPBParticipantsService.cacheKey);

    if (!participants) {
      participants = await this.loadFromProvider();
    }

    return participants;
  }

  public async getByCode(code: string): Promise<ArrangementParticipantSchema> {
    const cache = CacheService.getInstance();
    let participants: ArrangementParticipantSchema[] = await cache.get(SPBParticipantsService.cacheKey);

    if (!participants) {
      participants = await this.loadFromProvider();
    }

    return participants.find((participant) => Number(participant.code) === Number(code));
  }

  public async isArrangementParticipant(ispb: string, arrangement: Arrangement): Promise<boolean> {
    const cache = CacheService.getInstance();
    let participants: ArrangementParticipantSchema[] = await cache.get(SPBParticipantsService.cacheKey);

    if (!participants) {
      participants = await this.loadFromProvider();
    }

    return participants.some(
      (participant) =>
        participant.ispb === ispb &&
        ((arrangement === Arrangement.PIX &&
          participant.spiParticipant &&
          participant.spiStatus === ParticipantStatus.ACTIVE) ||
          (arrangement === Arrangement.STR &&
            participant.strParticipant &&
            participant.strStatus === ParticipantStatus.ACTIVE)),
    );
  }

  private async loadFromProvider(): Promise<ArrangementParticipantSchema[]> {
    let custodyProvider: CustodyProvider;
    if (this.spbParticipantsProvider) {
      custodyProvider = this.spbParticipantsProvider;
    } else {
      custodyProvider = AssetService.getInstance().rootAsset.provider;
    }

    let participants: ArrangementParticipantSchema[] = [];
    try {
      const provider = ProviderManagerService.getInstance().from(custodyProvider);
      const participantFeature = provider.feature(CustodyFeature.PARTICIPATING_INSTITUTION);
      await ProviderUtil.throwIfFeatureNotAvailable(participantFeature);

      participants = await participantFeature.getAll();
      const cache = CacheService.getInstance();
      cache.set(SPBParticipantsService.cacheKey, participants, SPBParticipantsService.DEFAULT_TTL);
    } catch (error) {
      this.logger.warn(`Provider ${custodyProvider} not initialized or SPB Participants Feature not available`, {
        originalMessage: error.originalMessage || error.message,
      });
    }

    return participants;
  }

  async onMount(server) {}

  async onInit(server) {}

  async onReady(server) {
    if (process.env.NODE_ENV !== 'test') {
      const discovery = DiscoveryService.getInstance();

      if (
        discovery.status(DiscoveryType.PARATI_PROVIDER_PARTICIPATING_INSTITUTION) ||
        discovery.status(DiscoveryType.CREDITAS_PROVIDER_PARTICIPATING_INSTITUTION)
      ) {
        await this.loadFromProvider();
      } else {
        // This fixes early calls to provider that has not been started yet, mainly in new installations and in the Startup Server
        this.logger?.warn('STR provider is not available, participating institution list will be delayed for 1min');
        setTimeout(
          () =>
            this.loadFromProvider().catch((error) =>
              this.logger?.error('Could not fetch participating instituions. ', error),
            ),
          60000,
        );
      }
    }
  }

  async onUnmount(server) {}
}
