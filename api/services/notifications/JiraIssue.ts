export interface SimpleType {
  id?: string;
}

export interface ContentType {
  text?: string;
  type?: string;
  content?: ContentType[];
}

export interface DescriptionType extends ContentType {
  version?: number;
}

export interface JiraIssueSchema {
  summary?: string;
  project?: SimpleType;
  issuetype?: SimpleType;
  description?: DescriptionType;
  customfield_10011?: string;
  labels?: string[];
}

export class JiraIssue implements JiraIssueSchema {
  summary?: string;

  project?: SimpleType;

  issuetype?: SimpleType;

  description?: DescriptionType;

  customfield_10011?: string;

  labels?: string[];

  constructor(data: Partial<JiraIssueSchema>) {
    const { ...otherData } = data;
    Object.assign(this, otherData);
  }

  public set epicId(id: string) {
    this.customfield_10011 = id;
  }

  public set projectId(id: string) {
    this.project.id = id;
  }

  public set issueTypeId(id: string) {
    this.issuetype.id = id;
  }

  public set descriptionText(description: string) {
    this.description.content[0].content[0].text = description;
  }

  public get descriptionText() {
    return this.description.content[0].content[0].text;
  }

  public toJSON() {
    return {
      ...(this as any),
    };
  }
}
