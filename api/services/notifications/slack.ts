import { Slack, SlackMessage, SlackMessageSchema, SlackServiceOptions } from 'ts-framework-notification';

export class SlackService extends Slack {
  /**
   * The singleton service instance.
   */
  protected static instance: SlackService;

  /**
   * Gets the singleton slack service.
   *
   * @param options The service options
   */
  public static getInstance(options?: SlackServiceOptions): SlackService {
    if (!SlackService.instance) {
      SlackService.instance = new SlackService({ ...options });
    }
    return SlackService.instance;
  }

  /**
   * Sends a Slack message.
   *
   * @param options The message options
   */
  public async send(options: SlackMessageSchema): Promise<any> {
    return super.send(new SlackMessage({ ...options }));
  }
}
