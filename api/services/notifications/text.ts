import { Text, TextMessage, TextMessageSchema, TextServiceOptions } from 'ts-framework-notification';

export class TextService extends Text {
  /**
   * The singleton service instance.
   */
  protected static instance: TextService;

  /**
   * Gets the singleton text service.
   *
   * @param options The service options
   */
  public static getInstance(options?: TextServiceOptions): TextService {
    if (!TextService.instance) {
      TextService.instance = new TextService({
        // TODO
        // debug: !(options && options.connectionUrl),
        ...options,
      });
    }
    return TextService.instance;
  }

  public static initialize(options?: TextServiceOptions): TextService {
    TextService.instance = new TextService({
      // TODO
      // debug: !(options && options.connectionUrl),
      ...options,
    });
    return TextService.instance;
  }

  /**
   * Sends a SMS text message.
   *
   * @param options The message options
   */
  public async send(options: TextMessageSchema): Promise<any> {
    return super.send(
      new TextMessage({
        ...options,
      }),
    );
  }
}
