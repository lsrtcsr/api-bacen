import { BaseError } from 'ts-framework-common';
import { Email, EmailMessage, EmailServiceOptions } from 'ts-framework-notification';
import { EmailMessageSchema } from 'ts-framework-notification/dist/types/email';

export interface EmailTemplateLocals {
  /** Non visible preview test. Must be plaintext */
  preview?: string;

  /** Header image. Must be a well-formed publicly-accessible URL to an inamge */
  header?: string;

  /** Main image. Must be a well-formed publicly-accessible URL to an inamge */
  logo?: string;

  /** Main email section title. Must be plaintext */
  title?: string;

  /** Main email section body. Accepts HTML or plaintext */
  body?: string;

  /** Call-to-action button. */
  button?: {
    /** Button text. Must be plaintext */
    label: string;

    /** The URL the button links to. Must be a well-formed URL. */
    url: string;
  };

  /** Secondary text section */
  section?: {
    /** Secondary section title. Must be plaintext */
    title: string;

    /** Secondary section body. Accepts HTML or plaintext */
    body: string;
  };

  /** Footer text. Must be plaintext */
  footer?: string;

  /** Company signature */
  company?: {
    /** Signature first line. Accepts HTML or plaintext */
    title: string;

    /** Signature second line. Accepts HTML or plaintext */
    subtitle: string;
  };

  /** Unsubscribe link */
  unsubscribe?: {
    /** Unsubscribe link text. Must be plaintext */
    label: string;

    /** Unsubscribe link URL, must be a well-formed URL */
    url: string;
  };
}

export interface SendEmailOptions extends EmailMessageSchema {
  locals: EmailTemplateLocals;
}

export class EmailService extends Email {
  /**
   * The singleton service instance.
   */
  protected static instance: EmailService;

  /**
   * The base locals to all email messages, can be overriden in method locals.
   */
  protected static baseLocals = {
    logo: 'https://i.imgur.com/QTYUAxG.png',
    footer: '<div style="text-align:center">nxtep.io</div>',
    company: {
      title: 'bacen  ',
      subtitle: '<a href="https://www.bacen.com.br" style="color: white">www.bacen.com.br</a>',
    },
  };

  constructor(options: EmailServiceOptions) {
    super(options);
  }

  /**
   * Gets the singleton email service.
   */
  public static getInstance(): EmailService {
    if (!EmailService.instance) {
      throw new BaseError("Email service is invalid or hasn't been initialized yet");
    }
    return EmailService.instance;
  }

  /**
   * Initializes the singleton email service.
   *
   * @param options The service options
   */
  public static initialize(options?: EmailServiceOptions): EmailService {
    const serviceOptions = options && options.connectionUrl ? { debug: false, ...options } : { debug: true };
    EmailService.instance = new EmailService(serviceOptions);

    return EmailService.instance;
  }

  /**
   * Sends an email message.
   *
   * @param options The message options
   */
  public async send(opts: SendEmailOptions): Promise<any> {
    const { locals, from = 'noreply@bt .app', ...otherOptions } = { ...opts, from: this.options.from };

    let response;
    try {
      response = super.send(
        new EmailMessage({
          from,
          locals: {
            ...EmailService.baseLocals,
            ...locals,
          },
          ...otherOptions,
        }),
      );
    } catch (exception) {
      this.logger.error('Email sending failed: ', { ...exception });
      throw exception;
    }

    return response;
  }
}
