import Axios, { AxiosInstance } from 'axios';
import { BaseError, Logger, LoggerInstance, ServiceOptions, Service } from 'ts-framework-common';
import { JiraIssueSchema, JiraIssue } from './JiraIssue';

export interface JiraServiceOptions extends ServiceOptions {
  logger?: LoggerInstance;
  baseUrl?: string;
  issueAPIEndpoint?: string;
  auth?: {
    username: string;
    apiToken: string;
  };
  debug?: boolean;
}

export class Jira extends Service {
  public readonly options: JiraServiceOptions;

  public client: AxiosInstance;

  public logger: LoggerInstance;

  constructor(options: JiraServiceOptions) {
    super(options);

    this.client = Axios.create();
    this.options = options;
    this.logger = options.logger || Logger.getInstance();
  }

  /**
   * Post message on Jira.
   *
   * @param options The post options
   */
  public async send(message: Partial<JiraIssueSchema>): Promise<any> {
    const data = new JiraIssue(message);

    if (this.options.debug) {
      // Logs the notification body in the console as a debug log
      const debugMessage = `${this.options.name} received a message in debug mode`;
      this.logger.debug(debugMessage, { message: { fields: data.toJSON() } });
      return { debug: true, message: { fields: data } };
    }

    if (!this.options.auth.username || !this.options.auth.apiToken) {
      throw new BaseError('Credentials not supplied');
    }

    const baseUrl = this.options && this.options.baseUrl;
    const endpoint = this.options && this.options.issueAPIEndpoint;

    if (!baseUrl || !endpoint) {
      throw new BaseError('Url not supplied');
    }

    const response = await this.client.post(
      baseUrl.concat(endpoint),
      { fields: data.toJSON() },
      {
        headers: {
          Authorization: 'Basic '.concat(this.getAuthToken()),
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    if (![200, 201].includes(response.status)) {
      throw new BaseError((response.data && response.data.message) || 'Error attempting to create issue on Jira');
    }

    return response;
  }

  private getAuthToken() {
    const mask = `${this.options.auth.username}:${this.options.auth.apiToken}`;
    return Buffer.from(mask).toString('base64');
  }

  async onInit(server) {}

  async onReady(server) {}

  async onMount(server) {
    this.client = this.client || Axios.create();
  }

  async onUnmount(server) {
    this.client = undefined;
  }
}
