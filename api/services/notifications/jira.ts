import { Jira, JiraServiceOptions } from './JiraService';
import Config from '../../../config';
import { JiraIssue } from './JiraIssue';

export class JiraService extends Jira {
  /**
   * The singleton service instance.
   */
  protected static instance: JiraService;

  constructor(options: JiraServiceOptions) {
    super(options);
  }

  public static initialize(options: JiraServiceOptions): JiraService {
    if (!JiraService.instance) {
      JiraService.instance = new JiraService(options);
    }
    return JiraService.instance;
  }

  /**
   * Gets the singleton text service.
   *
   * @param options The service options
   */
  public static getInstance(): JiraService {
    return JiraService.instance;
  }

  /**
   * Creates a new Issue in Jira.
   *
   * @param options The message options
   */
  public async createIssue(options?: {
    epicId?: string;
    projectId?: string;
    issueTypeId: string;
    summary?: string;
    description?: string;
    labels?: string[];
  }): Promise<any> {
    const defaultFields = Config.jira.fields;
    const issue = new JiraIssue({ ...defaultFields });

    let result;
    try {
      if (options.summary) issue.summary = options.summary;
      if (options.epicId) issue.epicId = options.epicId;
      if (options.projectId) issue.projectId = options.projectId;
      if (options.issueTypeId) issue.issueTypeId = options.issueTypeId;
      if (options.description) issue.descriptionText = options.description;
      if (options.labels) issue.labels.push(...options.labels);

      result = await this.send(issue);
    } catch (exception) {
      this.logger.error('Error attempting to create issue on Jira: ', { ...exception });
      throw exception;
    }

    return result;
  }
}
