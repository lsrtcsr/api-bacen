import { BaseResponse } from 'ts-framework';
import { bacenRequest, AssetRegistration, Asset } from '../../models';
import { InvalidRequestError } from '../../errors';

export default function () {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const { id } = req.params;
    const { wallet: walletId } = req.body;

    const asset = await Asset.getByIdOrCode(id);

    const assetRegistration = await AssetRegistration.createQueryBuilder('assetRegistration')
      .where('assetRegistration.asset = :assetId', { assetId: asset.id })
      .andWhere('assetRegistration.wallet = :walletId', { walletId })
      .getOne();

    if (assetRegistration) {
      throw new InvalidRequestError(`The asset is alredy registered`, {
        assetRegistration,
      });
    }

    next();
  };
}
