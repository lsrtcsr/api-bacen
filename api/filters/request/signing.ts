import { BaseRequest, BaseResponse } from 'ts-framework';
import { Signing } from 'ts-framework-signing';
import { UnauthorizedRequestError } from '../../errors';
import { OAuthClient } from '../../models';

export function signing(clientId?: string) {
  return Signing.middleware({
    /**
     * Gets the secret token from request for signing.
     */
    secret: (async (req: BaseRequest, res: BaseResponse) => {
      if (clientId) {
        const client = await OAuthClient.findOne({ clientId });
        if (!client) {
          throw new UnauthorizedRequestError('Invalid clientId');
        }
        return client.clientSecret;
      }
      if (res.locals && res.locals.oauth) {
        const accessToken = res.locals.oauth.token;

        if (accessToken && accessToken.client) {
          return accessToken.client.clientSecret;
        }
      }
    }) as any,
  });
}
