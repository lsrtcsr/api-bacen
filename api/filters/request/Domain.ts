import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpError } from 'ts-framework';
import { MiddlewareFn } from '..';
import { EntityNotFoundError } from '../../errors';
import { bacenRequest, Domain } from '../../models';

export function RequestDomainMiddlewareFactory(
  domainProp: (req: bacenRequest) => string | undefined,
): MiddlewareFn {
  return async function RequestDomainMiddleware(
    req: bacenRequest,
    res: BaseResponse,
    next: (err?: any) => void,
  ): Promise<void> {
    const domain = domainProp(req);
    let domainInstance: Domain;

    if (req.user?.role === UserRole.ADMIN && domain) {
      // Only admins can choose the domain
      domainInstance = await Domain.safeFindOne({ where: { id: domain } });
    } else if (req.user?.domain) {
      // If the user has a domain, use it
      domainInstance = await Domain.safeFindOne({ where: { id: req.user.domain.id } });
    } else {
      // Else, use the default domain
      domainInstance = await Domain.getDefaultDomain();
    }

    if (!domainInstance) {
      throw new EntityNotFoundError('domain');
    }

    req.cache.set<Domain>('request_domain', domainInstance);
    next();
  };
}
