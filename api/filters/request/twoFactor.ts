import { BaseRequest, BaseResponse } from 'ts-framework';
import { BaseError } from 'ts-framework-common';
import { TwoFactorHelper } from '../../helpers';

/**
 * The Two Factor Authentication middleware.
 */
export function twoFactorAuthVerify(req: BaseRequest, res: BaseResponse, next: Function) {
  if (!req.user.require2FA) {
    return next();
  }

  if (TwoFactorHelper.verifyToken(req.user.twoFactorSecret, req.body.token)) {
    return next();
  }

  throw new BaseError('Two factor authetication failed', {
    status: 400,
    user: req.user.id,
    secret: req.user.twoFactorSecret,
  });
}
