import { BaseResponse } from 'ts-framework';
import { bacenRequest, Asset } from '../../models';
import { InvalidRequestError } from '../../errors';

export default (paramKey: string, isRequired: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const assetId = req.param(paramKey) || req.body[paramKey];

    if (!assetId && !isRequired) return next();

    if (!assetId && isRequired) {
      throw new InvalidRequestError(`The asset parameter is required for that operation`);
    }

    const asset = await Asset.getByIdOrCode(assetId);
    if (!asset) throw new InvalidRequestError(`There is no asset with code ${assetId}`);

    if (!asset.provider) {
      throw new InvalidRequestError(
        `The supplied asset ${assetId} has no provider, which is required for this operation`,
      );
    }

    next();
  };
};
