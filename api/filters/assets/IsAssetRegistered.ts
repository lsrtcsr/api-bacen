import { AssetRegistrationStatus } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import { Asset, bacenRequest, Wallet } from '../../models';

export interface IsAssetRegisteredMiddlewareFactoryOptions {
  walletIdFn: (req: bacenRequest) => string | undefined;

  assetKeyFn: (req: bacenRequest) => string | undefined;
}

export function checkIfAssetIsRegisteredOnWallet(asset: Asset, wallet: Wallet): void {
  const isWalletIssuer = asset.issuer.id === wallet.id;
  const registrationStatus = wallet.assetRegistrations.find((ar) => ar.asset.id === asset.id)?.status;

  if (!isWalletIssuer && registrationStatus !== AssetRegistrationStatus.READY) {
    throw new InvalidRequestError('Asset is not registered on this wallet', {
      asset: asset.code,
      walletId: wallet.id,
      assetRegistrationStatus: registrationStatus,
    });
  }
}

export function IsAssetRegisteredMiddlewareFactory(options: IsAssetRegisteredMiddlewareFactoryOptions) {
  const { walletIdFn, assetKeyFn } = options;

  return async function IsAssetRegisteredMiddleware(
    req: bacenRequest,
    res: BaseResponse,
    next: (err?: any) => void,
  ): Promise<void> {
    const walletId = walletIdFn(req);
    const assetIdOrCode = assetKeyFn(req);

    if (!assetIdOrCode) {
      throw new InvalidRequestError('walletId is required');
    }

    if (!walletId) {
      throw new InvalidRequestError('walletId is required');
    } else {
      const wallet =
        req.cache.get<Wallet>(`wallet_${walletId}`) ||
        (await Wallet.createQueryBuilder('wallet')
          .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
          .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
          .where('wallet.id = :walletId', { walletId })
          .andWhere('wallet.deletedAt IS NULL')
          .getOne());

      if (!wallet) {
        throw new EntityNotFoundError('wallet', { walletId });
      }

      const asset =
        req.cache.get<Asset>(`asset_${assetIdOrCode}`) || (await Asset.getByIdOrCode(assetIdOrCode.toUpperCase()));

      if (!asset) {
        throw new EntityNotFoundError('asset', { asset: assetIdOrCode });
      }

      checkIfAssetIsRegisteredOnWallet(asset, wallet);
    }

    next();
  };
}
