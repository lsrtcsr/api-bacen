import { BaseResponse } from 'ts-framework';
import * as Payments from './payments';
import * as Permissions from './permissions';
import * as PhoneCredits from './phone-credits';
import * as PhoneVerification from './phone-verification';
import * as Request from './request';
import * as Status from './status';
import * as Assets from './assets';
import * as AssetRegistration from './asset-registration';
import * as Feature from './feature';
import * as UserFilter from './users';
import { bacenRequest } from '../models';

export { default as OAuth } from './oauth';
export { default as Params } from './params';
export { default as Query } from './query';
export { default as Exists } from './exists';
export { default as Limits } from './limits';
export { default as Amount } from './amount';
export { default as Billing } from './billing';
export { default as Applicable } from './applicable';

export interface MiddlewareFn {
  (req: bacenRequest, res: BaseResponse, next: (err?: any) => void): void;
}

export {
  Assets,
  AssetRegistration,
  Payments,
  Permissions,
  PhoneCredits,
  PhoneVerification,
  Request,
  Status,
  Feature,
  UserFilter,
};
