import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import { bacenRequest, Contract, ContractStatus } from '../../models';

// tslint:disable-next-line:max-line-length
export default (paramKey: string, requiredStatus: ContractStatus) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    // Get id from payload
    const entityId = req.param(paramKey);

    // Try to get instance from database
    const instance = await Contract.safeFindOne({
      where: { id: entityId },
      relations: ['states'],
    });

    if (!instance) {
      throw new EntityNotFoundError('Subscription not found', { id: entityId });
    }

    if (instance.status !== requiredStatus) {
      throw new InvalidRequestError('Subscription is not in the required state for the requested operation', {
        entityId,
        requiredStatus,
        currentStatus: instance.status,
      });
    }

    next();
  };
};
