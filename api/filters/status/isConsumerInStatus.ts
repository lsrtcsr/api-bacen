import { ConsumerStatus } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError, InvalidRequestError, InvalidParameterError } from '../../errors';
import { bacenRequest } from '../../models/request/bacenRequest';
import { User } from '../../models/user/User';
import { Wallet } from '../../models/wallet/Wallet';

// tslint:disable-next-line:max-line-length
export default (paramKey: string, requiredStatus: ConsumerStatus | ConsumerStatus[], resourceType = 'User') => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    // Get id from payload
    const resourceId = req.param(paramKey) || req.body[paramKey];

    let user: User;
    if (resourceType === User.name) {
      user = await User.safeFindOne({ where: { id: resourceId }, relations: ['consumer', 'consumer.states'] });
    } else if (resourceType === Wallet.name) {
      const wallet =
        req.cache.get<Wallet>(`wallet_${resourceId}`) ||
        (await Wallet.safeFindOne({
          where: { id: resourceId },
          relations: ['user', 'user.consumer', 'user.consumer.states'],
        }));
      user = wallet.user;
    } else {
      throw new InvalidParameterError(
        `Unable to verify consumer status: provided resource type (${resourceType}) is not supported`,
      );
    }

    if (!user) {
      throw new EntityNotFoundError(`${resourceType} not found`, { id: resourceId });
    }

    const statuses: ConsumerStatus[] = Array.isArray(requiredStatus) ? requiredStatus : [requiredStatus];
    if (!statuses.includes(user.consumer.persistentStatus)) {
      throw new InvalidRequestError('Consumer is not in the required state for the requested operation', {
        requiredStatus,
        userId: user.id,
      });
    }

    next();
  };
};
