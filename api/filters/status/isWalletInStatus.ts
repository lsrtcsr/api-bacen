import { WalletStatus } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import { bacenRequest, Wallet } from '../../models';

export interface IsWalletInStatusOptions {
  /**
   * The name of the incoming request property which is the walletId
   */
  walletIdFn: (req: bacenRequest) => string | undefined;

  /**
   * The status the wallet must be on to proceed.
   */
  requiredStatus: WalletStatus;

  /**
   * A flag telling if the middleware chain should proceed in case the wallet is not fount
   */
  allowMissingWallet?: boolean;
}

// tslint:disable-next-line:max-line-length
export default (options: IsWalletInStatusOptions) => {
  const { walletIdFn, requiredStatus, allowMissingWallet = false } = options;

  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    // Get id from payload
    const walletId = walletIdFn(req);

    // Try to get instance from database
    const instance =
      req.cache.get<Wallet>(`wallet_${walletId}`) ||
      (await Wallet.createQueryBuilder('wallet')
        .leftJoinAndSelect('wallet.states', 'states')
        .where('wallet.id = :walletId', { walletId })
        .andWhere('wallet.deletedAt IS NULL')
        .getOne());

    if (instance) {
      if (instance.persistentStatus !== requiredStatus) {
        throw new InvalidRequestError('Wallet is not in the required state for the requested operation', {
          entityId: walletId,
          requiredStatus,
        });
      }
    } else if (!allowMissingWallet) {
      throw new EntityNotFoundError('Wallet not found', { id: walletId });
    }

    next();
  };
};
