import { AccountType, PhoneType, UserRole, UnleashFlags } from '@bacen/base-sdk';
import { InvalidRequestError } from 'oauth2-server';
import { BaseRequest, BaseResponse } from 'ts-framework';
import { BaseError } from 'ts-framework-common';
import { UnleashUtil } from '@bacen/shared-sdk';
import { bacenRequest, Phone, Wallet } from '../models';

export const isPhoneVerifiable = async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { phoneId } = req.params;

  const phone = await Phone.findOne({ where: { id: phoneId }, relations: ['consumer'] });

  if (phone.consumer.type !== AccountType.PERSONAL) {
    throw new InvalidRequestError('Only individual consumer need to verify phone.');
  }

  if (phone.type !== PhoneType.MOBILE) {
    throw new InvalidRequestError('The given phone in not eligible for verification.');
  }

  next();
};

export const checkPhoneVerificationStatus = async (req: BaseRequest, res: BaseResponse, next: Function) => {
  const { walletId } = req.params;
  const { source }: { source: string } = req.body;

  const wallet = await Wallet.findOne({
    where: { id: walletId || source },
    relations: ['user', 'user.domain', 'user.consumer', 'user.consumer.phones'],
  });

  if (wallet.user.role !== UserRole.CONSUMER) return next();

  const context = { userId: wallet.user.id, properties: { domainId: wallet.user.domain.id } };
  const skipPhoneVerification = UnleashUtil.isEnabled(UnleashFlags.SKIP_PHONE_VERIFICATION, context, false);

  if (skipPhoneVerification) return next();

  const forcePhoneVerification = UnleashUtil.isEnabled(UnleashFlags.FORCE_PHONE_VERIFICATION, context, false);

  const verifiedPhone = wallet.user.consumer.phones.find(
    phone => phone.type === PhoneType.MOBILE && !!phone.verifiedAt,
  );

  if (forcePhoneVerification && !verifiedPhone) {
    const mobilePhone = wallet.user.consumer.phones.find(phone => phone.type === PhoneType.MOBILE);

    throw new BaseError('To perform this action you must verify your phone', {
      status: 400,
      user: req.user.id,
      url: `/consumers/${wallet.user.id}/phones/${mobilePhone.id}/verify-phone`,
    });
  }

  return next();
};
