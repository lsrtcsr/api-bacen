import { HttpError, HttpCode } from 'ts-framework';
import { UserRole, UnleashFlags, ServiceType } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { bacenRequest, Asset, User, Wallet, Payment, WalletPendingBalanceView } from '../../models';
import { EntityNotFoundError, ForbiddenRequestError } from '../../errors';
import { AuthorizerQueryService, BillingService, ContractService, LegacyWalletService } from '../../services';
import { BalanceMap } from '../payments';

export default async function checkBalanceAvailability(serviceType: ServiceType, req: bacenRequest) {
  try {
    const walletService = LegacyWalletService.getInstance();
    const authorizerQueryService = AuthorizerQueryService.getInstance();
    const currentUser = req.user;

    let accountable: User;
    let wallet: Wallet;
    let walletId: string;

    if ((walletId = req.params.walletId || req.params.id)) {
      wallet =
        req.cache.get<Wallet>(`wallet_${walletId}`) ||
        (await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user', 'user.domain'] }));
      accountable = wallet.user;
    } else if ((walletId = req.body.source || req.body.destination)) {
      wallet =
        req.cache.get<Wallet>(`wallet_${walletId}`) ||
        (await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user', 'user.domain'] }));
      accountable = wallet.user;
    } else {
      // TODO review
      const user = await User.safeFindOne({ where: { id: currentUser.id }, relations: ['wallets'] });
      accountable = currentUser;

      wallet = user.wallets[0];
      wallet.user = user;
    }

    if (!wallet) {
      throw new EntityNotFoundError('Source Wallet');
    }

    // Prepare feature flag context
    let forceBilling = false;
    const context = { userId: accountable.id, properties: { domainId: accountable.domain.id } };
    if (accountable.role === UserRole.CONSUMER) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_CONSUMER_BILLING, context, false);
    } else if (accountable.role === UserRole.MEDIATOR) {
      forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false);
    }

    const currentContract = await ContractService.getInstance().getCurrentContract(wallet.user.id);
    req.cache.set(`current_contract_user_${wallet.user.id}`, currentContract);

    if (forceBilling && !currentContract) {
      throw new ForbiddenRequestError(
        `Billing plan subscription not found! To perform this operation, the user ${accountable.id} must subscribe a plan`,
      );
    }

    const rootAsset: Asset = await Asset.getRootAsset();

    const amountRequestMap: BalanceMap = {};
    amountRequestMap[rootAsset.code] = {
      asset: rootAsset,
      amount: 0,
    };

    let transactionAmount = 0;
    if (req.body.amount) {
      const asset = await Asset.getByIdOrCode(req.body.asset || req.params.id || 'root');
      amountRequestMap[asset.code] = amountRequestMap[asset.code] || { amount: 0, asset };

      if (serviceType !== ServiceType.BOLETO_EMISSION) amountRequestMap[asset.code].amount -= +req.body.amount;
      transactionAmount += +req.body.amount;
    } else if (req.body.recipients) {
      for (const recipient of req.body.recipients) {
        const asset = await Asset.getByIdOrCode(recipient.asset);
        amountRequestMap[asset.code] = amountRequestMap[asset.code] || { amount: 0, asset };

        amountRequestMap[asset.code].amount -= +recipient.amount;
        transactionAmount += +recipient.amount;
      }
    }

    let serviceFee: Partial<Payment>;
    if (!currentContract || !currentContract.prepaid) {
      serviceFee = { asset: rootAsset, amount: '0.0000000' };
    } else {
      serviceFee = await BillingService.getInstance().getServiceFee({
        serviceType,
        transactionAmount,
        user: wallet.user,
        contract: currentContract,
      });
    }

    if (serviceFee && Number(serviceFee.amount) !== 0) {
      // subtract service fee from wallet balance
      amountRequestMap[rootAsset.code].amount -= Number(serviceFee.amount);
    }

    // We need to hold onto the original amounts if we need to release the blockedBalances on a failed authorization.
    // TODO: amount may be zero, should we filter only amounts larger than zero or throw if any amount is less than zero?
    const blockedAmounts: [Asset, number][] = Object.values(amountRequestMap).map(({ asset, amount }) => [
      asset,
      -amount,
    ]);
    const assets = blockedAmounts.map(([asset]) => asset);

    const [blockchainBalances, pendingDebtsAmount, blockedBalances] = await Promise.all([
      walletService.getBalancesFromStellar(wallet, assets),
      WalletPendingBalanceView.getWalletPendingDebts(wallet.id),
      Promise.all(
        blockedAmounts.map(([asset, amount]) => authorizerQueryService.updateBlockedBalance({ amount, asset, wallet })),
      ),
    ]);

    const remainingBalances: BalanceMap = Object.fromEntries(
      Object.entries(amountRequestMap).map(([key, { asset }]) => [key, { asset, amount: 0 }]),
    );
    for (const [assetCode, amountMap] of Object.entries(remainingBalances)) {
      const assetBlockChainBalance = blockchainBalances.find(({ code }) => code === assetCode).balance;

      const assetPendingDebts = Number(pendingDebtsAmount[amountMap.asset.id] ?? 0);

      const assetBlockedBalance = Number(
        blockedBalances.find((registration) => registration.assetId === amountMap.asset.id).blockedBalance,
      );

      amountMap.amount += assetBlockChainBalance - assetPendingDebts - assetBlockedBalance;
    }

    const negativeBalances = Object.values(remainingBalances).filter(({ amount }) => amount < 0);
    if (negativeBalances.length > 0) {
      // Release blocked balances.
      await Promise.all(
        blockedAmounts.map(([asset, amount]) =>
          authorizerQueryService.updateBlockedBalance({ amount: -amount, asset, wallet }),
        ),
      );

      /*
       * If the blockedBalance returned from the update blockedBalance query is different than the ammount we requested
       * to be blocked, in other words, there was already an outstanding block in the wallet's balance, it probably is
       * due to concurrent requests trying to create a debt to this wallet, so we return an error which more or less says
       * "try again". After some discussion it was decided that the less terrible way to signal that would be with an
       * HTTP 425 "too early" status code.
       *
       * This is effectively a race condition which we allow to happen because the possible side-effect is that we end up
       * authorizing LESS than would be possible to a given wallet, which while it is definitelly not a great user
       * to be told that you can't execute an operation due to insufficient balance when you're sure that you have enough
       * money, it is certainly better than letting the user spend more money than they have.
       *
       * The other option we had to handle this concurrency issue was to use explicit locking of the wallet, allowing
       * only a single concurrent transaction per wallet at a time. This was done previously but for the PIX feature a
       * client had a use case which required a single wallet operating as a pre-paid wallet to execute the transactions
       * for 6 million accounts, which would not be possible by processing all the transactions serially. So we allowed
       * concurrent transactions with a minor tweak so that the consequences of a race condition would be a mild
       * annoyance to the user instead of creating a liability for the company, bankrupting it and everybody losing their
       * jobs.
       *
       * TODO: add a link do a diagram showing why this race-condition is "safe"
       */
      const mismatchedBlockedBalances = blockedAmounts.some(([asset, amount]) => {
        const { blockedBalance } = blockedBalances.find(({ assetId }) => assetId === asset.id);
        return Number(blockedBalance) !== amount;
      });

      if (mismatchedBlockedBalances) {
        // TODO: add more info.
        throw new HttpError('Try again', 425);
      }

      throw new HttpError('User does not have enough balance.', HttpCode.Client.PRECONDITION_FAILED, {
        assets: negativeBalances.map(({ asset }) => asset.code),
      });
    }

    req.cache.set<[Asset, number][]>('blocked_amounts', blockedAmounts);
  } catch (exception) {
    throw exception;
  }
}
