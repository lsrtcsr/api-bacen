import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';

export const canUserWriteLimitValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const consumer = new User({ id: userId });

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      const consumerDomain = await getDomainFromUser(consumer);
      return currentUserDomain.id === consumerDomain.id;

    case UserRole.CONSUMER:
    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteLimit = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteLimitValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
