import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';

export const canUserReadOneLimitValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const consumer = new User({ id: userId });

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      const consumerDomain = await getDomainFromUser(consumer);
      return currentUserDomain.id === consumerDomain.id;

    case UserRole.CONSUMER:
      return currentUser.id === consumer.id;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneLimit = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOneLimitValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
