import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest } from '../../../models';

export type PermissionValidator = (req: bacenRequest) => Promise<boolean>;

/**
 * Ensures at least one permission validator is fulfilled.
 */
const oneOf = (validators: PermissionValidator[]) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    // Create an array of promises
    const promises = validators.map(validator => validator(req));
    // Resolve all of them at once
    const results = await Promise.all(promises);

    // True if at least one of them is true, false otherwise
    const finalResult = results.some(result => result);

    // Validation failed, return error
    if (!finalResult) throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
    // Call next middleware
    next();
  };
};

export default oneOf;
