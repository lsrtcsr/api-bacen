import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpError, HttpCode } from 'ts-framework';
import { bacenRequest, User } from '../../../models';

const canUserWritePostbackValidator = (req: bacenRequest): boolean => {
  const currentUser: User = req.user;

  // If the user is a consumer and is blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.AUDIT:
    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export async function canUserWritePostback(req: bacenRequest, res: BaseResponse, next: (err?: any) => void) {
  if (canUserWritePostbackValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
}
