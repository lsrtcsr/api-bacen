import { BaseResponse } from 'ts-framework';
import { NextFunction } from 'express';
import { UserRole, ConsumerStatus } from '@bacen/base-sdk';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserAcceptLegalTermValidator = async function(req: bacenRequest) {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const consumer = new User({ id: userId });

  // If the user is a consumer and is blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      const consumerDomain = await getDomainFromUser(consumer);
      return currentUserDomain.id === consumerDomain.id;

    case UserRole.CONSUMER:
      return currentUser.id === consumer.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserAcceptLegalTerm = async function(req: bacenRequest, res: BaseResponse, next: NextFunction) {
  if (await canUserAcceptLegalTermValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError('Invalid user role');
  }
};
