import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest } from '../../../models';
import { User } from '../../../models/user';
import { getDomainFromUser } from '../domain/util';
import { PermissionValidator } from '../wrapper';

export const canUserVerifyMobilePhoneValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId, phoneId } = req.params;
  const currentUser: User = req.user;

  const targetUser = await User.safeFindOne({ where: { id: userId }, relations: ['consumer', 'consumer.phones'] });

  if (!targetUser || !targetUser.consumer || !targetUser.consumer.phones) {
    return false;
  }

  const phoneFound = targetUser.consumer.phones.find(phone => phone.id === phoneId);

  if (!phoneFound) {
    return false;
  }

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      const [targetDomain, currentDomain] = await Promise.all([
        getDomainFromUser(targetUser),
        getDomainFromUser(currentUser),
      ]);
      return targetDomain.id === currentDomain.id;

    case UserRole.CONSUMER:
      return targetUser.id === currentUser.id;

    case UserRole.AUDIT:
    case UserRole.OPERATOR:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserVerifyMobilePhone = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserVerifyMobilePhoneValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
