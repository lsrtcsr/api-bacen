import { ConsumerStatus, UserRole, UnleashFlags } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';

export const canUserDeleteUserValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const user = new User({ id: userId });

  // Check if the wallet's owner is in the mediator domain
  const currentDomain = await getDomainFromUser(currentUser);
  const requestedUserDomain = await getDomainFromUser(user);

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)) return true;
      return currentUser.id === user.id || currentDomain.id === requestedUserDomain.id;

    case UserRole.CONSUMER:
      return currentUser.id === user.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteUser = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteUserValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
