import { ConsumerStatus, UnleashFlags, UserRole } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest, User } from '../../../models';
import { PermissionValidator } from '../wrapper';

export const canUserChangeUserDomainValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const user = new User({ id: userId });

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const isMultiDomain = UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS);
      return isMultiDomain;

    default:
      return false;
  }
};

export const canUserChangeUserDomain = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserChangeUserDomainValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
