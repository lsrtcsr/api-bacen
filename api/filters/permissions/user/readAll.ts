import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User } from '../../../models';

export const canUserReadAllUsersValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadAllUsers = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadAllUsersValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
