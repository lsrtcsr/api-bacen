import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';

export const canUserWriteUserValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const { role } = req.body;
  const currentUser: User = req.user;
  const user = new User({ id: userId });

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.AUDIT:
    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
      // No creates, only self updates
      if (!userId) return false;
      // [Security] Prevent user role elevation
      if (role) return false;

      return currentUser.id === user.id;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteUser = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteUserValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
