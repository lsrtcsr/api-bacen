import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getInvoiceFromUser, belongToTheSameDomain } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserWriteInvoiceValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const invoiceId: string = req.params.invoiceId || req.params.id;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      return await belongToTheSameDomain(invoiceId, currentUser);

    case UserRole.CONSUMER:
      const userInvoice = await getInvoiceFromUser(invoiceId, currentUser);
      return !!userInvoice;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteInvoice = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteInvoiceValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
