import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadAllInvoicesValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.MEDIATOR: // review
      return true;

    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadAllInvoices = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadAllInvoicesValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
