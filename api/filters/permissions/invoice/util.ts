import { FindConditions } from 'typeorm';
import { isUUID } from '../../../utils';
import { User, Invoice, Domain } from '../../../models';

export const getInvoiceFromUser = async (id: string, user: User): Promise<Invoice> => {
  let where: FindConditions<Invoice> = isUUID(id) ? { id } : { current: true };
  where = { contractor: { id: user.id }, ...where };

  const invoice = await Invoice.safeFindOne({
    where,
    relations: ['contractor', 'contractor.domain'],
  });

  if (!invoice) undefined;

  return invoice;
};

export const belongToTheSameDomain = async (invoiceId: string, user: User): Promise<boolean> => {
  let domain: Domain;
  if (user.domain) {
    domain = user.domain;
  } else {
    const userWithDomain = await User.safeFindOne({ where: { id: user.id }, relations: ['domain'] });
    domain = userWithDomain.domain;
  }

  const where: FindConditions<Invoice> = isUUID(invoiceId) ? { id: invoiceId } : { current: true };
  const invoice = await Invoice.safeFindOne({
    where,
    relations: ['contractor', 'contractor.domain'],
  });

  return domain.id === invoice.contractor.domain.id;
};
