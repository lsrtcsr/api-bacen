import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';

import { bacenRequest, User } from '../../../models';
import { getInvoiceFromUser, belongToTheSameDomain } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadOneInvoiceValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const invoiceId: string = req.params.invoiceId || req.params.id;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
      return true;

    case UserRole.MEDIATOR:
      return await belongToTheSameDomain(invoiceId, currentUser);

    case UserRole.CONSUMER:
      const userInvoice = await getInvoiceFromUser(invoiceId, currentUser);
      return !!userInvoice;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneInvoice = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOneInvoiceValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
