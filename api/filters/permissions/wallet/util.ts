import { ConsumerStatus } from '@bacen/base-sdk';
import { Consumer, Domain, User, Wallet } from '../../../models';

export const getUserFromWallet = async (wallet: Wallet): Promise<User> => {
  if (wallet.user) return wallet.user;

  const walletWithUser = await Wallet.safeFindOne({ where: { id: wallet.id }, relations: ['user'] });

  if (walletWithUser) {
    return walletWithUser.user;
  }
};

export const getDomainFromWallet = async (wallet: Wallet): Promise<Domain> => {
  if (wallet.user && wallet.user.domain) return wallet.user.domain;

  const walletWithDomain = await Wallet.safeFindOne({ where: { id: wallet.id }, relations: ['user', 'user.domain'] });

  if (walletWithDomain && walletWithDomain.user) {
    return walletWithDomain.user.domain;
  }
};

export const getLastStateFromConsumer = async (consumerUser: User) => {
  if (consumerUser.consumer && consumerUser.consumer.states) return consumerUser.consumer.getStates()[0];

  const consumerWithStates = await Consumer.safeFindOne({
    where: { user: { id: consumerUser.id } },
    relations: ['states'],
  });

  if (consumerWithStates) {
    return consumerWithStates.getStates()[0];
  }
};

export const consumerCanAccessWallets = async (consumerUser: User) => {
  const lastState = await getLastStateFromConsumer(consumerUser);
  return lastState.status === ConsumerStatus.READY;
};
