import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User, Wallet } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { consumerCanAccessWallets, getDomainFromWallet, getUserFromWallet } from './util';

export const canUserDeleteWalletValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const walletId = req.params.id;
  const wallet = new Wallet({ id: walletId });

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      // Check if the wallet's owner is in the mediator domain
      const walletDomain = await getDomainFromWallet(wallet);
      const currentUserDomain = await getDomainFromUser(currentUser);
      return currentUserDomain.id === walletDomain.id;

    case UserRole.CONSUMER:
      if (!consumerCanAccessWallets(currentUser)) return false;

      // Check if the requesting user is the wallet's owner
      const walletUser = await getUserFromWallet(wallet);
      return currentUser.id === walletUser.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteWallet = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteWalletValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
