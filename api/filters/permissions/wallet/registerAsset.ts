import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User, Wallet } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { consumerCanAccessWallets, getDomainFromWallet } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserRegisterAssetOnWalletValidator: PermissionValidator = async function(req: bacenRequest) {
  const currentUser = await User.findOne(req.user.id, {
    relations: ['consumer', 'consumer.states', 'domain', 'states', 'wallets'],
  })!;
  const wallet = await Wallet.findOne(req.body.wallet, { relations: ['user', 'user.domain'] });

  // TODO: Handle this check on another filter
  if (!wallet) return false;

  // If currentUser is a consumer and is blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.OPERATOR:
      // registering on their own wallet, allow it
      if (currentUser.wallets.map(w => w.id).includes(wallet.id)) return true;

      // Check if the wallet's owner is in the mediator domain
      const walletDomain = await getDomainFromWallet(wallet);
      const currentUserDomain = await getDomainFromUser(currentUser);
      return currentUserDomain.id === walletDomain.id;

    case UserRole.CONSUMER:
      // registering on their own wallet, allow it
      if (currentUser.wallets.map(w => w.id).includes(wallet.id)) return true;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export async function canUserRegisterWalletOnAsset(
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) {
  if (await canUserRegisterAssetOnWalletValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
}
