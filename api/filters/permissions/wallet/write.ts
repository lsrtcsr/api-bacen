import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { consumerCanAccessWallets } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserWriteWalletValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.OPERATOR:
      // Creating wallet to themself, allow it
      if (!req.body.user || (req.user && req.user.id === req.body.user)) return true;

      // Check if the wallet's owner is in the mediator domain
      const walletDomain = await getDomainFromUser(currentUser);
      const currentUserDomain = await getDomainFromUser(currentUser);
      return currentUserDomain.id === walletDomain.id;

    case UserRole.CONSUMER:
      if (!consumerCanAccessWallets(currentUser)) return false;

      // Allow only if the consumer is creating the wallet to themself
      return !req.body.user;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteWallet = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteWalletValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
