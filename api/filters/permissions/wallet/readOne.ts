import { UserRole, UnleashFlags } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User, Wallet } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { ForbiddenRequestError } from '../../../errors';
import { consumerCanAccessWallets, getDomainFromWallet, getUserFromWallet } from './util';

export const canUserReadOneWalletValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const walletId = req.params.id || req.params.walletId;
  const wallet = new Wallet({ id: walletId });

  // Check if the wallet's owner is in the mediator domain
  const walletDomain = await getDomainFromWallet(wallet);
  const currentUserDomain = await getDomainFromUser(currentUser);

  // Maybe wallet does not exist, or is soft deleted
  if (!walletDomain || !currentUserDomain) {
    return false;
  }

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      // if allowed, return the data if the mediator is in another domain
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)) return true;
      // Check if the wallet's owner is in the mediator domain
      return currentUserDomain.id === walletDomain.id;

    case UserRole.CONSUMER:
      if (!consumerCanAccessWallets(currentUser)) return false;

      // Check if the requesting user is the wallet's owner
      const walletUser = await getUserFromWallet(wallet);

      // All wallets are visible to users within the same domain
      return currentUser.id === walletUser.id || currentUserDomain.id === walletDomain.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneWallet = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOneWalletValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
