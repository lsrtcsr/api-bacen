import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from './util';

export const canUserDeleteDomainValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const domainId: string = req.params.domainId || req.params.id;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      return currentUserDomain.id === domainId;

    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteDomain = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteDomainValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
