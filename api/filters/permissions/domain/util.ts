import { Domain, User } from '../../../models';

export const getDomainFromUser = async (user: User): Promise<Domain> => {
  if (user.domain) return user.domain;

  const userWithDomain = await User.safeFindOne({ where: { id: user.id }, relations: ['domain'] });
  return userWithDomain.domain;
};
