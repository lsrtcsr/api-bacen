import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';

export const canUserReadAllIssuesValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
    default:
      return false;
  }
};

export const canUserReadAllIssues = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadAllIssuesValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
