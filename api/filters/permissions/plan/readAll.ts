import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';

import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadAllPlansValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
      return true;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadAllPlans = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadAllPlansValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
