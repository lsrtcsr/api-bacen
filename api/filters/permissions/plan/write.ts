import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';
import { belongToTheSameDomain } from './util';

export const canUserWritePlanValidator: PermissionValidator = async (req: bacenRequest) => {
  const { id } = req.params;
  const { domain } = req.body;

  const currentUser: User = req.user;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      if (!id && !domain) return true;
      if (domain) return currentUser.domain.id === domain;
      return await belongToTheSameDomain(currentUser, id);

    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWritePlan = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWritePlanValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
