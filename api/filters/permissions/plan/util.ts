import { UserRole } from '@bacen/base-sdk';
import { User, Plan, Domain } from '../../../models';

export const getPlanFromUser = async (id: string, user: User): Promise<Plan> => {
  let supplier: Domain;
  if (user.role === UserRole.MEDIATOR) {
    supplier = await Domain.getRootDomain();
  } else if (user.role === UserRole.CONSUMER) {
    if (user.domain) {
      supplier = user.domain;
    } else {
      const userWithDomain = await User.safeFindOne({
        where: { id: user.id },
        relations: ['domain'],
      });
      supplier = userWithDomain.domain;
    }
  }

  return Plan.safeFindOne({ where: { id, supplier } });
};

export const belongToTheSameDomain = async (user: User, planId: string): Promise<boolean> => {
  let domain: Domain;
  if (user.domain) {
    domain = user.domain;
  } else {
    const userWithDomain = await User.safeFindOne({ where: { id: user.id }, relations: ['domain'] });
    domain = userWithDomain.domain;
  }

  const plan = await Plan.safeFindOne({
    where: { id: planId },
    relations: ['supplier'],
  });

  if (!plan) return false;

  return domain.id === plan.supplier.id;
};
