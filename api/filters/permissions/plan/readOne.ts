import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';

import { bacenRequest, User } from '../../../models';
import { belongToTheSameDomain } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadOnePlanValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const planId: string = req.params.planId || req.params.id;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
      return true;

    case UserRole.CONSUMER:
    case UserRole.MEDIATOR:
      return await belongToTheSameDomain(currentUser, planId);

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOnePlan = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOnePlanValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
