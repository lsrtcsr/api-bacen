import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User } from '../../../models';

export const canUserReadAccountRelatedValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.AUDIT:
    case UserRole.MEDIATOR:
    case UserRole.OPERATOR:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadAccountRelated = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadAccountRelatedValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError('Invalid user role');
  }
};
