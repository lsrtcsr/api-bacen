import { UnleashFlags, UserRole } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseResponse } from 'ts-framework';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import { PermissionValidator } from '../wrapper';

export const canUserReadOneConsumerValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId } = req.params;
  const currentUser: User = req.user;
  const consumer = new User({ id: userId });

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      const consumerDomain = await getDomainFromUser(consumer);
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)) return true;
      return currentUserDomain.id === consumerDomain.id;

    case UserRole.CONSUMER:
      return currentUser.id === consumer.id;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneConsumer = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadOneConsumerValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError('Invalid user role');
  }
};
