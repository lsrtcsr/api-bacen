import { ConsumerStatus, UserRole, DocumentType } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';

export const canUserWriteConsumerValidator: PermissionValidator = async (req: bacenRequest) => {
  const { userId, type } = req.params as { userId: string; type: DocumentType };
  const currentUser: User = req.user;
  const consumer = new User({ id: userId });

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      const currentUserDomain = await getDomainFromUser(currentUser);
      const consumerDomain = await getDomainFromUser(consumer);
      return currentUserDomain.id === consumerDomain.id;

    case UserRole.CONSUMER:
      return currentUser.id === consumer.id && type !== DocumentType.OTHER;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteConsumer = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteConsumerValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError('Invalid user role');
  }
};
