import { UserRole, PaymentType } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, Payment, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import {
  getDestinationUserFromPayment,
  getSourceDomainFromPayment,
  getSourceUserFromPayment,
  getDomainFromPaymentDestination,
} from './util';

export const canUserReadOnePaymentValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const paymentId = req.params.id;
  const payment = await Payment.safeFindOne({ where: { id: paymentId } });

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      // Check if the payment's source (or destionation) is in the mediator domain
      const paymentDomain =
        payment.type === PaymentType.DEPOSIT
          ? await getDomainFromPaymentDestination(payment)
          : await getSourceDomainFromPayment(payment);
      const currentUserDomain = await getDomainFromUser(currentUser);
      return currentUserDomain.id === paymentDomain.id;

    case UserRole.CONSUMER:
      // Check if the requesting user is the payment's source
      const paymentUser = await getSourceUserFromPayment(payment);
      if (currentUser.id === paymentUser.id) return true;

      // Check if the requesting user is the payment's destination
      const paymentDestination = await getDestinationUserFromPayment(payment);
      return currentUser.id === paymentDestination.id;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOnePayment = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOnePaymentValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
