import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { isUUID } from '@bacen/shared-sdk';
import { PermissionValidator } from '../wrapper';
import { EntityNotFoundError, ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User, Wallet } from '../../../models';

// tslint:disable-next-line:max-line-length
export const canUserConfirmPaymentValidator: (walletKey: string) => PermissionValidator = (walletKey: string) => async (
  req: bacenRequest,
) => {
  const currentUser: User = req.user;
  const walletId = req.param(walletKey);

  // A blocked consumer, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  if (!isUUID(walletId)) {
    throw new HttpError('Wallet id is not a valid UUID', HttpCode.Client.BAD_REQUEST, {
      [walletKey]: walletId,
    });
  }

  // Try to get instance from database
  const wallet = await Wallet.createQueryBuilder('wallet')
    .leftJoinAndSelect('wallet.states', 'states')
    .leftJoinAndSelect('wallet.user', 'user')
    .leftJoinAndSelect('user.domain', 'domain')
    .where('wallet.id = :walletId', { walletId })
    .andWhere('wallet.deletedAt IS NULL')
    .andWhere('user.deletedAt IS NULL')
    .getOne();

  if (!wallet) {
    throw new EntityNotFoundError('Wallet not found', { id: req.body[walletKey], key: walletKey });
  }

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.OPERATOR:
    case UserRole.CONSUMER:
    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

// tslint:disable-next-line:max-line-length
export const canUserConfirmPayment = (walletKey: string) => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserConfirmPaymentValidator(walletKey)(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
