import { UserRole, UnleashFlags } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { PermissionValidator } from '../wrapper';
import { ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User } from '../../../models';
import { getDomainFromUser } from '../domain/util';
import {
  getDestinationUsersFromTransaction,
  getSourceDomainFromTransaction,
  getSourceUserFromTransaction,
} from './util';

export const canUserReadOneTransactionValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const transactionId = req.params.id;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
      // Check if the transaction's source is in the mediator domain
      const transactionDomain = await getSourceDomainFromTransaction(transactionId);
      const currentUserDomain = await getDomainFromUser(currentUser);

      // if allowed, return the data if the mediator is in another domain
      if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_MEDIATOR_DOMAIN_REQUEST_FILTERS)) return true;
      return currentUserDomain.id === transactionDomain.id;

    case UserRole.CONSUMER:
      // Check if the requesting user is the transaction's source
      const transactionUser = await getSourceUserFromTransaction(transactionId);
      if (currentUser.id === transactionUser.id) return true;

      // Check if the requesting user is the one of the transaction's destinations
      const transactionDestinations = await getDestinationUsersFromTransaction(transactionId);
      return !!transactionDestinations.find(user => user.id === currentUser.id);

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

// tslint:disable-next-line:max-line-length
export const canUserReadOneTransaction = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadOneTransactionValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
