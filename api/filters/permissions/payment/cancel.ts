import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { EntityNotFoundError, ForbiddenRequestError } from '../../../errors';
import { bacenRequest } from '../../../models/request/bacenRequest';
import { User } from '../../../models/user/User';
import { Wallet } from '../../../models/wallet/Wallet';
import { Payment } from '../../../models/payment/Payment';

// tslint:disable-next-line:max-line-length
export const canUserCancelPaymentValidator: (paymentKey: string) => PermissionValidator = (
  paymentKey: string,
) => async (req: bacenRequest) => {
  const currentUser: User = req.user;

  // A blocked consumer, just reject right away
  if (currentUser.consumer && currentUser.consumer.status !== ConsumerStatus.READY) return false;

  const payment = await Payment.safeFindOne({
    where: { id: req.param(paymentKey) },
    relations: ['transaction', 'transaction.source', 'transaction.source.user', 'transaction.source.user.domain'],
  });

  if (!payment) {
    throw new EntityNotFoundError('Payment not found', { id: req.param(paymentKey), key: paymentKey });
  }

  const wallet: Wallet = payment.transaction.source;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.OPERATOR:
      // just on the same domain
      return currentUser.domain.id === wallet.user.domain.id;

    case UserRole.CONSUMER:
      return currentUser.id === payment.transaction.source.user.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

// tslint:disable-next-line:max-line-length
export const canUserCancelPayment = (paymentKey: string) => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserCancelPaymentValidator(paymentKey)(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
