import { UserRole } from '@bacen/base-sdk';
import { EntityNotFoundError } from '../../../errors';
import { Domain, Payment, Transaction, User } from '../../../models';
import { isUUID } from '../../../utils';

export const getSourceDomainFromTransaction = async (id: string): Promise<Domain> => {
  if (!id) return undefined;

  let qb = Transaction.createQueryBuilder('transaction')
    .leftJoinAndSelect('transaction.source', 'source')
    .leftJoinAndSelect('source.user', 'sourceuser')
    .leftJoinAndSelect('sourceuser.domain', 'sourceuserdomain')
    .where('transaction.deletedAt IS NULL');

  if (isUUID(id)) {
    qb = qb.andWhere('transaction.id = :id', { id });
  } else {
    qb = qb.andWhere(
      "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
      { hash: id, instructionId: id },
    );
  }
  const transactionWithDomain = await qb.getOne();

  if (transactionWithDomain.source.user.role !== UserRole.ADMIN) {
    return transactionWithDomain.source.user.domain;
  }

  const payment = await Payment.safeFindOne({
    where: { transaction: { id: transactionWithDomain.id } },
    relations: ['transaction', 'destination', 'destination.user', 'destination.user.domain'],
  });
  return getDomainFromPaymentDestination(payment);
};

export const getSourceDomainFromPayment = async (payment: Payment): Promise<Domain> => {
  const paymentWithDomain =
    payment.transaction &&
    payment.transaction.source &&
    payment.transaction.source.user &&
    payment.transaction.source.user.domain
      ? payment
      : await Payment.safeFindOne({
          where: { id: payment.id },
          relations: ['transaction', 'transaction.source', 'transaction.source.user', 'transaction.source.user.domain'],
        });

  if (!paymentWithDomain.transaction.source) {
    throw new EntityNotFoundError('Source', { id: payment.id });
  }

  if (paymentWithDomain.transaction.source.user.role !== UserRole.ADMIN) {
    return paymentWithDomain.transaction.source.user.domain;
  }

  return getDomainFromPaymentDestination(payment);
};

export const getDomainFromPaymentDestination = async (payment: Payment): Promise<Domain> => {
  const paymentWithDomain =
    payment.destination && payment.destination.user && payment.destination.user.domain
      ? payment
      : await Payment.safeFindOne({
          where: { id: payment.id },
          relations: ['destination', 'destination.user', 'destination.user.domain'],
        });

  return paymentWithDomain.destination.user.domain;
};

export const getSourceUserFromTransaction = async (id: string): Promise<User> => {
  let qb = Transaction.createQueryBuilder('transaction')
    .leftJoinAndSelect('transaction.source', 'source')
    .leftJoinAndSelect('source.user', 'sourceuser')
    .where('transaction.deletedAt IS NULL');

  if (isUUID(id)) {
    qb = qb.andWhere('transaction.id = :id', { id });
  } else {
    qb = qb.andWhere(
      "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
      { hash: id, instructionId: id },
    );
  }
  const transactionWithUser = await qb.getOne();

  if (!transactionWithUser.source) {
    throw new EntityNotFoundError('Source', { id });
  }

  return transactionWithUser.source.user;
};

export const getSourceUserFromPayment = async (payment: Payment): Promise<User> => {
  if (
    payment.transaction &&
    payment.transaction.source &&
    payment.transaction.source.user &&
    payment.transaction.source.user.domain
  ) {
    return payment.transaction.source.user;
  }

  const paymentWithUser = await Payment.safeFindOne({
    where: { id: payment.id },
    relations: ['transaction', 'transaction.source', 'transaction.source.user'],
  });

  if (!paymentWithUser.transaction.source) {
    throw new EntityNotFoundError('Source', { id: payment.id });
  }

  return paymentWithUser.transaction.source.user;
};

export const getDestinationUsersFromTransaction = async (id: string): Promise<User[]> => {
  let qb = Transaction.createQueryBuilder('transaction')
    .leftJoinAndSelect('transaction.source', 'source')
    .leftJoinAndSelect('transaction.payments', 'payments')
    .leftJoinAndSelect('payments.destination', 'destination')
    .leftJoinAndSelect('destination.user', 'destinationuser')
    .where('transaction.deletedAt IS NULL');

  if (isUUID(id)) {
    qb = qb.andWhere('transaction.id = :id', { id });
  } else {
    // We should think about moving these two fields to columns in SQL instead of using them inside json columns.
    qb = qb.andWhere(
      "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
      { hash: id, instructionId: id },
    );
  }
  const transactionWithPayments = await qb.getOne();

  return transactionWithPayments.payments.map((payment) => payment.destination.user);
};

export const getDestinationUserFromPayment = async (payment: Payment): Promise<User> => {
  if (payment.destination && payment.destination.user) {
    return payment.destination.user;
  }

  const paymentWithUser = await Payment.safeFindOne({
    where: { id: payment.id },
    relations: ['destination', 'destination.user'],
  });

  return paymentWithUser.destination.user;
};
