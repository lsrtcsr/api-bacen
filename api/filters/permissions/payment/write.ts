import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { isUUID } from '@bacen/shared-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { EntityNotFoundError, ForbiddenRequestError } from '../../../errors';
import { bacenRequest, User, Wallet } from '../../../models';
import { PermissionValidator } from '../wrapper';

// tslint:disable-next-line:max-line-length
export const canUserWritePaymentValidator: (walletKey: string) => PermissionValidator = (walletKey: string) => async (
  req: bacenRequest,
) => {
  const currentUser: User = req.user;

  // A blocked consumer, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  if (!isUUID(req.param(walletKey))) {
    throw new HttpError('Wallet id is not a valid UUID', HttpCode.Client.BAD_REQUEST, {
      [walletKey]: req.param(walletKey),
    });
  }

  // Try to get instance from database
  const wallet =
    req.cache.get<Wallet>(`wallet_${req.param(walletKey)}`) ||
    (await Wallet.safeFindOne({
      where: { id: req.param(walletKey) },
      relations: ['states', 'user', 'user.domain'],
    }));

  if (!wallet) {
    throw new EntityNotFoundError('Wallet not found', { id: req.body[walletKey], key: walletKey });
  }

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      // My wallet, or in my domain
      return wallet.user.id === currentUser.id || wallet.user.domain.id === currentUser.domain.id;

    case UserRole.CONSUMER:
      // Only if same user as source wallet
      return wallet.user.id === currentUser.id;

    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

// tslint:disable-next-line:max-line-length
export const canUserWritePayment = (walletKey: string) => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserWritePaymentValidator(walletKey)(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
