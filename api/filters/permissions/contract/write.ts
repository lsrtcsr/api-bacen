import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';
import { getContractFromUser, belongToTheSameDomain as userAndContractBelongToTheSameDomain } from './util';
import { getPlanFromUser, belongToTheSameDomain as userAndPlanBelongToTheSameDomain } from '../plan/util';

export const canUserWriteContractValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const planId: string = req.params.planId || req.params.id;
  const { subscriptionId } = req.params;

  const { subscriber: userId }: { subscriber: string } = req.body;

  const subscriber: User = userId ? await User.safeFindOne({ where: { id: userId }, relations: ['domain'] }) : req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      if (subscriptionId) return await userAndContractBelongToTheSameDomain(currentUser, subscriptionId);
      if (subscriber.id !== currentUser.id) return await userAndPlanBelongToTheSameDomain(subscriber, planId);
      return true;

    case UserRole.CONSUMER:
      const found = subscriptionId
        ? await getContractFromUser(subscriptionId, currentUser)
        : await getPlanFromUser(planId, subscriber);
      return !!found;

    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteContract = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteContractValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
