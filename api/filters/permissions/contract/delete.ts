import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { belongToTheSameDomain } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserDeleteContractValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const contractId: string = req.params.contractId || req.params.id;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
      return await belongToTheSameDomain(currentUser, contractId);

    case UserRole.CONSUMER:
    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteContract = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteContractValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
