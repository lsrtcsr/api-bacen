import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';

import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadAllContractsValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadAllContracts = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadAllContractsValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
