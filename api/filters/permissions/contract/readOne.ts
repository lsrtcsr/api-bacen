import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';

import { bacenRequest, User } from '../../../models';
import { getContractFromUser, belongToTheSameDomain } from './util';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadOneContractValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const contractId: string = req.params.contractId || req.params.id;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
      return true;

    case UserRole.MEDIATOR:
      return await belongToTheSameDomain(currentUser, contractId);

    case UserRole.CONSUMER:
      const userContract = await getContractFromUser(contractId, currentUser);
      return !!userContract;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneContract = async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: Error) => void,
) => {
  if (await canUserReadOneContractValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
