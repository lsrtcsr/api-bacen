import { User, Contract, Domain } from '../../../models';

export const getContractFromUser = async (id: string, contractor: User): Promise<Contract> => {
  const contract = await Contract.safeFindOne({ where: { id, contractor } });

  if (!contract) return undefined;

  return contract;
};

export const belongToTheSameDomain = async (user: User, contractId: string): Promise<boolean> => {
  let domain: Domain;
  if (user.domain) {
    domain = user.domain;
  } else {
    const userWithDomain = await User.safeFindOne({ where: { id: user.id }, relations: ['domain'] });
    domain = userWithDomain.domain;
  }

  const contract = await Contract.safeFindOne({
    where: { id: contractId },
    relations: ['contractor', 'contractor.domain'],
  });

  if (!contract) return false;

  return domain.id === contract.contractor.domain.id;
};
