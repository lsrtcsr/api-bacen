import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';

export const canUserWriteServiceValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;
  const serviceId: string = req.params.serviceId || req.params.id;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserWriteService = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserWriteServiceValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
