import { UserRole } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';
import { ForbiddenRequestError } from '../../../errors';

export const canUserReadOneServiceValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.MEDIATOR:
      return true;

    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneService = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOneServiceValidator(req)) {
    next();
  } else {
    throw new ForbiddenRequestError();
  }
};
