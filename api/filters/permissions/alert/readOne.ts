import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';

export const canUserReadOneAlertValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.AUDIT:
    case UserRole.OPERATOR:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserReadOneAlert = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserReadOneAlertValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
