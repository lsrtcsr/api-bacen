import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest, User } from '../../../models';
import { PermissionValidator } from '../wrapper';

export const canUserDeleteAlertValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
      return true;

    case UserRole.MEDIATOR:
    case UserRole.AUDIT:
    case UserRole.CONSUMER:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteAlert = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteAlertValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
