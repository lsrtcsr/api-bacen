import { ConsumerStatus, UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { PermissionValidator } from '../wrapper';
import { bacenRequest, User } from '../../../models';

export const canUserDeleteAssetValidator: PermissionValidator = async (req: bacenRequest) => {
  const currentUser: User = req.user;

  // If the it's a consumer and it's blocked, just reject right away
  if (currentUser.consumer && currentUser.consumer.status === ConsumerStatus.BLOCKED) return false;

  switch (currentUser.role) {
    case UserRole.ADMIN:
    case UserRole.MEDIATOR:
      return true;

    case UserRole.CONSUMER:
    case UserRole.AUDIT:
    case UserRole.PUBLIC:
    default:
      return false;
  }
};

export const canUserDeleteAsset = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  if (await canUserDeleteAssetValidator(req)) {
    next();
  } else {
    throw new HttpError('Forbidden', HttpCode.Client.FORBIDDEN);
  }
};
