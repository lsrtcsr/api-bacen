import { BaseResponse } from 'ts-framework';
import { CardStatus } from '@bacen/base-sdk';
import { In } from 'typeorm';
import { BadRequestError } from 'stellar-sdk';
import { bacenRequest, Card } from '../../models';
import { UnauthorizedRequestError } from '../../errors';

export const activation = async (req: bacenRequest, _res: BaseResponse, next: (err?: any) => void) => {
  const { walletId, cardId } = req.params;

  const applicableStatuses = [CardStatus.BLOCKED, CardStatus.BLOCKED_INVALID_PASSWORD, CardStatus.BLOCKED_EXTERNAL];

  const count = await Card.safeCount({
    where: { id: cardId, wallet: { id: walletId }, status: In(applicableStatuses) },
  });
  if (!count) throw new UnauthorizedRequestError('Card is not applicable for activation', { walletId, cardId });

  next();
};

export const changePassword = async (req: bacenRequest, _res: BaseResponse, next: (err?: any) => void) => {
  const { walletId, cardId } = req.params;

  const notApplicableStatuses = [CardStatus.CANCELLED, CardStatus.SUSPECT];

  const count = await Card.safeCount({
    where: { id: cardId, wallet: { id: walletId }, status: In(notApplicableStatuses) },
  });
  if (count) throw new UnauthorizedRequestError('Card is not applicable for password reset', { walletId, cardId });

  next();
};

export const cardLength = async (req: bacenRequest, _res: BaseResponse, next: (err?: any) => void) => {
  const { unblockCode } = req.body;

  if (unblockCode.toString().trim().length < 4 || unblockCode.toString().trim().length > 16)
    throw new BadRequestError('unlockCode must have a length between 4 and 16 characters', { unblockCode });

  next();
};
