import { BaseResponse, HttpError, HttpCode, BaseRequest } from 'ts-framework';
import { UnleashContext, UnleashUtil } from '@bacen/shared-sdk';
import { UnleashFlags } from '@bacen/base-sdk';
import { Wallet, Asset } from '../../models';

/**
 * Checks if the `DISABLE_PHYSICAL_CARD_EMISSION` feature flag is enabled, if so returns 400.
 *
 * This middleware depends on the paramers `req.params.walletId` and `req.body.asset` being valid and pointing
 * to existing models, so it must be run after `Exists.Wallet` and `Assets.hasProvider` in the middleware chain.
 *
 * @param req The Request object
 * @param res The Response object
 * @param next The next function
 */
export async function isPhysicalCardEmissionEnabled(
  req: BaseRequest,
  res: BaseResponse,
  next: (err?: any) => void,
): Promise<void> {
  const wallet = await Wallet.findOne(req.params.walletId, { relations: ['user'] });
  const asset = await Asset.getByIdOrCode(req.body.asset);

  const context: UnleashContext = {
    userId: wallet.user.id,
    properties: { domainId: wallet.user.domainId, assetCode: asset.code, walletId: wallet.id },
  };
  const isDisabled = UnleashUtil.isEnabled(UnleashFlags.DISABLE_PHYSICAL_CARD_EMISSION, context, false);

  if (isDisabled) {
    throw new HttpError(
      'Physical card emission is disabled, please contact your administrator',
      HttpCode.Client.BAD_REQUEST,
    );
  }

  next();
}
