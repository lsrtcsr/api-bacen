import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest, User, Wallet } from '../../models';
import { LimitSettingService } from '../../services/LimitSettingService';
import { LimitType } from '../../models/settings/UserLimit';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { walletId } = req.params;
  const wallet = await Wallet.safeFindOne({ where: { id: walletId }, relations: ['user'] });

  const hasLimit = await LimitSettingService.getInstance().hasLimit(LimitType.NUM_OF_ACTIVE_CARDS, wallet.user);
  if (!hasLimit) {
    throw new HttpError(`User ${wallet.user.id} has reached the limit of active cards`, HttpCode.Client.BAD_REQUEST);
  }

  next();
};
