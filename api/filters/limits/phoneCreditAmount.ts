import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { CustodyProvider } from '@bacen/base-sdk';
import { bacenRequest, User, Asset, LimitType } from '../../models';
import { LimitSettingService } from '../../services/LimitSettingService';
import { Wallet } from '../../models/wallet/Wallet';
import Config from '../../../config';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const limitService: LimitSettingService = LimitSettingService.getInstance();
  const sourceWalletId: string = req.body.source;
  const provider =
    ((req.body.provider || req.query.provider) as CustodyProvider) || Config.provider.defaultMobileRechargeProvider;

  const user: User = await Wallet.getWalletOwner(sourceWalletId);
  const wallet = await Wallet.safeFindOne({ where: { id: sourceWalletId } });

  const paymentAsset: Asset = await Asset.getByProvider(provider);
  const amount: number = +req.body.amount;

  const dailyLimit: number = await limitService.getLimits(LimitType.MOBILE_CREDIT_SUM_CURRENT_DAY, user);
  const totalSpentToday: number = await limitService.getValueConsumed({
    wallet,
    type: LimitType.MOBILE_CREDIT_SUM_CURRENT_DAY,
    asset: paymentAsset,
  });

  if (amount + totalSpentToday > dailyLimit) {
    throw new HttpError('Transaction exceeds daily limit', HttpCode.Client.PRECONDITION_FAILED);
  }

  next();
};
