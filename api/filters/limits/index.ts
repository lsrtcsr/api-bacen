import numOfCards from './numOfCards';
import transactionAmount from './transactionAmount';
import phoneCreditAmount from './phoneCreditAmount';
import consumerLegal from './consumerLegal';

export default {
  phoneCreditAmount,
  transactionAmount,
  numberOfCards: numOfCards,
  consumerLegal,
};
