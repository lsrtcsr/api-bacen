import { AccountType } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import Config from '../../../config';
import { InvalidRequestError } from '../../errors';
import { bacenRequest, Consumer, User } from '../../models';

// TODO: This filter is in the wrong place.
export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const {
    consumer = {} as any,
  }: User & {
    consumer: Consumer;
  } = req.body;

  // For Personal account, check age
  if (consumer.type === AccountType.PERSONAL) {
    // Legal age validation - Only permit 18+
    const legalBirthdayDate = new Date();
    legalBirthdayDate.setFullYear(legalBirthdayDate.getFullYear() - Config.consumerLegal.MINIMUM_AGE);
    if (new Date(consumer.birthday).getTime() > legalBirthdayDate.getTime()) {
      throw new InvalidRequestError('Consumer does not have the minimum legal age for account creation', {
        birthday: consumer.birthday,
      });
    }
  }

  next();
};
