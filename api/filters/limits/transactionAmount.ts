import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { Logger } from 'nano-errors';
import { UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { bacenRequest, User, Asset, LimitType } from '../../models';
import { LimitSettingService } from '../../services/LimitSettingService';
import { Wallet } from '../../models/wallet/Wallet';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  let walletId: string;
  if (req.body.source) {
    walletId = req.body.source;
  } else if (req.params.walletId) {
    walletId = req.params.walletId;
  } else {
    throw new HttpError('User not found', HttpCode.Client.NOT_FOUND);
  }

  if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_AUTHORIZER_LIMITS)) {
    Logger.getInstance().silly(`Skipping limit checks for wallet because of feature flag`, { walletId });
    next();
    return;
  }

  const limitService: LimitSettingService = LimitSettingService.getInstance();
  let transactionAmount = 0;
  let assetIdOrCode: string;
  const { recipients, amount } = req.body;

  const wallet = req.cache.get<Wallet>(`wallet_${walletId}`) || (await Wallet.safeFindOne({ where: { id: walletId } }));
  const user: User = req.cache.get<Wallet>(`wallet_${walletId}`)?.user || (await Wallet.getWalletOwner(walletId));

  if (recipients) {
    /*
     * Used in `POST /payments`. each recipient has the schema
     * {
     *     destination: string,
     *     amount: string,
     *     asset: string,
     * }
     */

    recipients.forEach((recipient) => {
      transactionAmount += Number(recipient.amount);
    });
    assetIdOrCode = recipients[0].asset;
  } else if (amount) {
    // Used in `POST /wallets/:walletId/withdraw`.

    transactionAmount = Number(amount);
    assetIdOrCode = req.body.asset;
  } else {
    throw new HttpError('Recipients not found', HttpCode.Client.NOT_FOUND);
  }

  const paymentAsset = assetIdOrCode ? await Asset.getByIdOrCode(assetIdOrCode) : await Asset.getRootAsset();

  const [dailyLimit, monthlyLimit] = await Promise.all([
    limitService.getLimits(LimitType.TRANSACTION_SUM_CURRENT_DAY, user, paymentAsset.code, user.domain),
    limitService.getLimits(LimitType.TRANSACTION_SUM_CURRENT_MONTH, user, paymentAsset.code, user.domain),
  ]);

  const totalSpentToday: number = await limitService.getValueConsumed({
    type: LimitType.TRANSACTION_SUM_CURRENT_DAY,
    wallet,
    asset: paymentAsset,
  });

  Logger.getInstance().debug(`Checking daily limit for wallet ${walletId}`, {
    dailyLimit,
    totalSpentToday,
  });

  if (dailyLimit !== 0 && transactionAmount + totalSpentToday > dailyLimit) {
    throw new HttpError(`Transaction exceeds daily limit of ${dailyLimit}`, HttpCode.Client.PRECONDITION_FAILED);
  }

  const totalSpentThisMonth: number = await limitService.getValueConsumed({
    type: LimitType.TRANSACTION_SUM_CURRENT_MONTH,
    wallet,
    asset: paymentAsset,
  });

  Logger.getInstance().debug(`Checking monthly limit for wallet ${walletId}`, {
    monthlyLimit,
    totalSpentThisMonth,
  });

  if (monthlyLimit !== 0 && transactionAmount + totalSpentThisMonth > monthlyLimit) {
    throw new HttpError(`Transaction exceeds monthly limit of ${monthlyLimit}`, HttpCode.Client.PRECONDITION_FAILED);
  }

  next();
};
