import { BaseRequest, BaseResponse } from 'ts-framework';
import { BaseDto } from '../../schemas';
import { InvalidParameterError } from '../../errors';

export function dtoValidationMiddlewareFactory(dtoClass: typeof BaseDto) {
  return async function createOrderDtoValidator(
    req: BaseRequest,
    res: BaseResponse,
    next: (err?: any) => void,
  ): Promise<void> {
    try {
      const validatedPayload = await dtoClass.transformAndValidate(req.body);
      req.body = validatedPayload;
      next();
    } catch (err) {
      throw new InvalidParameterError('request body', err);
    }
  };
}
