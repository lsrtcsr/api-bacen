import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest, Asset, Wallet, AssetRegistration } from '../../models';

export const hasUserEnoughBalance = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  const { provider, source, amount } = req.body;
  const [asset, wallet] = await Promise.all([
    Asset.getByProvider(provider),
    Wallet.safeFindOne({ where: { id: source } }),
  ]);

  if (!asset) throw new HttpError('Asset not found', HttpCode.Client.NOT_FOUND, { provider });
  if (!wallet) throw new HttpError('Wallet not found', HttpCode.Client.NOT_FOUND, { source });

  const isRegisterd = await AssetRegistration.isRegistered(wallet, asset);
  if (!isRegisterd)
    throw new HttpError('Asset is not registered on source wallet', HttpCode.Client.BAD_REQUEST, {
      asset: asset.code,
      source,
    });

  const authorizableBalance = await wallet.getAuthorizableBalance(asset);
  if (authorizableBalance < Number(amount)) {
    throw new HttpError(
      'Wallet does not have enough balance to perform this operation',
      HttpCode.Client.PRECONDITION_FAILED,
      {
        amount,
        asset: asset.code,
        source,
        authorizableBalance,
      },
    );
  }

  next();
};
