import { dtoValidationMiddlewareFactory } from '../dto';
import { CompleteOrderDto } from '../../schemas';

export const completeOrderDtoValidator = dtoValidationMiddlewareFactory(CompleteOrderDto);
