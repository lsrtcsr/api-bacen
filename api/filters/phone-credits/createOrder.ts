import { BaseRequest, BaseResponse } from 'ts-framework';
import { CreateOrderDto } from '../../schemas';
import { dtoValidationMiddlewareFactory } from '../dto';

export const createOrderDtoValidator = dtoValidationMiddlewareFactory(CreateOrderDto);
