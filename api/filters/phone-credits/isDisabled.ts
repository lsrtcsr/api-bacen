import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { UnleashUtil } from '@bacen/shared-sdk';
import { UnleashFlags } from '@bacen/base-sdk';
import { bacenRequest } from '../../models';

export const isDisabled = async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
  const isDisabled = UnleashUtil.isEnabled(UnleashFlags.DEACTIVATE_PHONE_CREDITS);

  if (isDisabled) {
    throw new HttpError(
      'Phone credits is temporarily unavailable, contact us for more information.',
      HttpCode.Server.SERVICE_UNAVAILABLE,
    );
  }

  next();
};
