import * as validator from 'validator';

import { UserRole } from '@bacen/base-sdk';
import { BaseRequest, BaseResponse, HttpCode, HttpError } from 'ts-framework';

import { UserRepository } from '../../repositories';

export default () => async (req: BaseRequest, res: BaseResponse, next: () => void) => {
  const {
    firstName,
    lastName,
    email,
    password,
    role,
    username,
  }: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    role: UserRole;
    username?: string;
  } = req.body;

  if (validator.isEmpty(firstName)) {
    throw new HttpError('Invalid firstName in body field', HttpCode.Client.BAD_REQUEST);
  }

  if (validator.isEmpty(lastName)) {
    throw new HttpError('Invalid lastName in body field', HttpCode.Client.BAD_REQUEST);
  }

  if (!email || !validator.isEmail(email)) {
    throw new HttpError('Invalid e-mail in body field', HttpCode.Client.BAD_REQUEST);
  }

  if (!password) {
    throw new HttpError('Invalid password in body field', HttpCode.Client.BAD_REQUEST);
  }

  const allowedRoles = [UserRole.OPERATOR, UserRole.AUDIT];

  if (!role || Object.values(allowedRoles).indexOf(role) === -1) {
    throw new HttpError('Invalid role in body field', HttpCode.Client.BAD_REQUEST, {
      role,
      allowedRoles,
    });
  }

  if (username) {
    const user = await new UserRepository().findByUsername(username);

    if (user) {
      throw new HttpError('User already exists', HttpCode.Client.BAD_REQUEST);
    }
  }

  next();
};
