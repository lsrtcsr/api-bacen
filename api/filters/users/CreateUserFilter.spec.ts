import * as sinon from 'sinon';
import * as HttpMock from 'node-mocks-http';
import * as faker from 'faker';
import * as typeorm from 'typeorm';

import { UserRole } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';

import { bacenRequest, User } from '../../models';
import { UserFilter } from '..';
import { UserRepository } from '../../repositories';

describe('api.filters.users.CreateUserFilter', () => {
  let nextStub: sinon.SinonStub;
  let getManagerStub: sinon.SinonStub;
  let transactionStub: sinon.SinonStub;
  let findByUsernameStub: sinon.SinonStub;

  beforeAll(async () => {
    nextStub = sinon.stub();

    transactionStub = sinon
      .stub()
      .callsFake((transactionFnOrIsolationLevel: Function | string, maybeTransactionFn?: Function) => {
        const transactionFn = maybeTransactionFn || (transactionFnOrIsolationLevel as Function);
        return transactionFn();
      });

    getManagerStub = sinon.stub(typeorm, 'getManager').returns({
      transaction: transactionStub,
    } as any);

    findByUsernameStub = sinon.stub(UserRepository.prototype, 'findByUserName').resolves(new User());
  });

  afterEach(async () => {
    nextStub.resetBehavior();
    nextStub.resetHistory();

    transactionStub.resetBehavior();
    transactionStub.resetHistory();

    getManagerStub.resetBehavior();
    getManagerStub.resetHistory();
  });

  afterAll(() => {
    findByUsernameStub.restore();
  });

  it('throws bad request if email is invalid', async () => {
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      body: {
        email: 'invalidemail',
      },
    });

    let exception: HttpError;

    try {
      await UserFilter.CreateUserFilter()(req, res, nextStub);
    } catch (error) {
      exception = error;
    }

    expect(nextStub.calledOnce).toBe(false);
    expect(exception!).toBeDefined();
    expect(exception!).toBeInstanceOf(HttpError);
    expect(exception!.status).toBe(HttpCode.Client.BAD_REQUEST);
  });

  it('throws bad request if password is not present in body field', async () => {
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      body: {
        email: faker.internet.email(),
        password: undefined,
      },
    });

    let exception: HttpError;

    try {
      await UserFilter.CreateUserFilter()(req, res, nextStub);
    } catch (error) {
      exception = error;
    }

    expect(nextStub.calledOnce).toBe(false);
    expect(exception!).toBeDefined();
    expect(exception!).toBeInstanceOf(HttpError);
    expect(exception!.status).toBe(HttpCode.Client.BAD_REQUEST);
  });

  it('throws bad request if role is invalid', async () => {
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      body: {
        email: faker.internet.email(),
        password: faker.internet.password(),
        role: UserRole.CONSUMER,
      },
    });

    let exception: HttpError;

    try {
      await UserFilter.CreateUserFilter()(req, res, nextStub);
    } catch (error) {
      exception = error;
    }

    expect(nextStub.calledOnce).toBe(false);
    expect(exception!).toBeDefined();
    expect(exception!).toBeInstanceOf(HttpError);
    expect(exception!.status).toBe(HttpCode.Client.BAD_REQUEST);
  });

  it('throws bad request if username exists', async () => {
    const { req, res } = HttpMock.createMocks<bacenRequest, BaseResponse>({
      body: {
        email: faker.internet.email(),
        password: faker.internet.password(),
        role: UserRole.OPERATOR,
        username: faker.internet.userName(),
      },
    });

    let exception: HttpError;

    try {
      await UserFilter.CreateUserFilter()(req, res, nextStub);
    } catch (error) {
      exception = error;
    }

    expect(nextStub.calledOnce).toBe(false);
    expect(exception!).toBeDefined();
    expect(exception!).toBeInstanceOf(HttpError);
    expect(exception!.status).toBe(HttpCode.Client.BAD_REQUEST);
  });
});
