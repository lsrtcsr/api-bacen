import { isValidAmount } from './is-valid';

export default {
  isValidAmount,
};
