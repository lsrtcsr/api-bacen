import { BaseRequest, BaseResponse } from 'ts-framework';
import { UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { InvalidParameterError } from '../../errors';

export const MAX_AMOUNT_DECIMALS = 7;

const AMOUNT_REGEX = /^\d+(\.\d{1,7})?$/;

export const isValidAmount = (req: BaseRequest, res: BaseResponse, next: Function) => {
  let hasInvalidAmount = false;

  if (req.body.amount && !checkValidity(req.body.amount)) {
    hasInvalidAmount = true;
  }

  if (req.body.recipients) {
    req.body.recipients.forEach((recipient: any) => {
      if (!checkValidity(recipient.amount)) hasInvalidAmount = true;
    });
  }

  if (hasInvalidAmount) throw new InvalidParameterError('amount');

  next();
};

const checkValidity = (amount: string | number): boolean => {
  const isString = toString.call(amount) === toString.call('abc');

  if (isString) {
    const str = amount as string;
    if (!AMOUNT_REGEX.test(str)) throw new InvalidParameterError('amount');
    return +str > 0 && countDecimals(str) <= MAX_AMOUNT_DECIMALS && countDecimals(str) >= 0;
  }
  if (UnleashUtil.isEnabled(UnleashFlags.FORCE_STRING_AMOUNT)) {
    throw new InvalidParameterError('amount');
  } else {
    // TODO: This can be dangerous and lead to potential rounding errors
    return checkValidity((amount as number).toFixed(MAX_AMOUNT_DECIMALS));
  }
};

const countDecimals = (amount: string): number => {
  // Check is integer
  if (Math.floor(+amount) === +amount) {
    return 0;
  }

  // Split decimal string to count length
  const parts = amount.split('.');
  return parts.length ? parts[1].length : 0;
};
