import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { Asset, bacenRequest, Wallet, WalletPendingBalanceView } from '../../models';
import { isUUID } from '../../utils';
import { AuthorizerQueryService, LegacyWalletService } from '../../services';

export interface BalanceMapItem {
  asset: Asset;
  amount: number;
}

export interface BalanceMap {
  [key: string]: BalanceMapItem;
}
export type AmountsFromRequestFn = (req: bacenRequest) => Promise<BalanceMap>;
export type SourceWalletFromRequestFn = (req: bacenRequest) => Promise<Wallet>;
export type DefaultRecipentsListFn = (req: bacenRequest) => Promise<string[]>;

/**
 * Gets default balance map for a request. This is a legacy function and should be
 * overwritten with something way less unstable.
 *
 * @param req The request object
 */
export const defaultAmountsFn = (assetField = 'id') => async (req: bacenRequest): Promise<BalanceMap> => {
  const amountRequestMap: BalanceMap = {};

  if (req.body.amount) {
    const assetCode = req.params[assetField] || req.body[assetField];
    const asset = await Asset.getByIdOrCode(assetCode || 'root');

    // Body has only one amount to be checked
    amountRequestMap[asset.code] = amountRequestMap[asset.code] || {
      asset,
      amount: -req.body.amount,
    };
  } else {
    // Sum all balances from list of recipients
    for (const recipient of req.body.recipients) {
      let asset: Asset;
      const key = recipient.asset || 'root';

      if (!amountRequestMap[key]) {
        if (!key || key === 'root') {
          asset = await Asset.getRootAsset();
        } else if (isUUID(key)) {
          asset = await Asset.findOne(key, { relations: ['issuer'] });
        } else {
          asset = await Asset.createQueryBuilder('asset')
            .leftJoinAndSelect('asset.issuer', 'issuer')
            .where('asset.code = :code', { code: key.toUpperCase() })
            .getOne();
        }

        if (!asset) {
          throw new HttpError('Asset was not found or is unavailable', HttpCode.Client.NOT_FOUND);
        }

        // Body has only one amount to be checked
        amountRequestMap[asset?.code] = { asset, amount: -recipient.amount };
      } else {
        amountRequestMap[key].amount -= +recipient.amount;
      }
    }
  }

  return amountRequestMap;
};

/**
 * Gets source wallet for checking the balances from.
 *
 * @param req The request object
 */
export const defaultSourceWallet = async (req: bacenRequest): Promise<Wallet> => {
  let walletParam: string;

  if (req.body.source) {
    walletParam = req.body.source;
  } else if (req.params.walletId) {
    walletParam = req.params.walletId;
  } else {
    throw new EntityNotFoundError('Source Wallet');
  }

  return req.cache.get<Wallet>(`wallet_${walletParam}`) || Wallet.findOne(walletParam);
};

/**
 * Gets list of recients of a transfer.
 *
 * @param req The request object
 */
export const defaultRecipientsList = async (req: bacenRequest) => {
  const { body } = req;

  const recipientsList = body.recipients.map((recipient) => {
    return recipient.destination;
  });

  return recipientsList;
};

/**
 * Ensure that source wallet is not on recipient list.
 *
 * @param sourceFn The function to get the source wokallet from the request
 * @param recipientsFn The function to get the recipients list from the request
 */
export const isSourceOneOfRecipients = (
  sourceFn: SourceWalletFromRequestFn = defaultSourceWallet,
  recipientsFn: DefaultRecipentsListFn = defaultRecipientsList,
) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
    const wallet: Wallet = await sourceFn(req);
    const recipients = await recipientsFn(req);

    const hasSourceOnList = recipients.filter((recipient) => recipient === wallet.id);

    if (hasSourceOnList.length > 0) {
      throw new HttpError("Can't send a payment to yourself.", HttpCode.Client.PRECONDITION_FAILED, {});
    }

    next();
  };
};

/**
 * Ensure wallet has enough balance for operation, including the pending payments.
 *
 * @param sourceFn The function to get the source wokallet from the request
 * @param amountsFn The function to get the amounts map from the request
 */
export const hasUserEnoughBalance = (
  sourceFn: SourceWalletFromRequestFn = defaultSourceWallet,
  amountsFn: AmountsFromRequestFn = defaultAmountsFn(),
) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) => {
    const walletService = LegacyWalletService.getInstance();
    const authorizerQueryService = AuthorizerQueryService.getInstance();
    const amountRequestMap: BalanceMap = await amountsFn(req);
    // We need to hold onto the original amounts if we need to release the blockedBalances on a failed authorization.
    const blockedAmounts: [Asset, number][] = Object.values(amountRequestMap).map(({ asset, amount }) => [
      asset,
      -amount,
    ]);

    // Make sure we have found at least one asset in the map
    if (!amountRequestMap || Object.keys(amountRequestMap).length < 1) {
      // TODO: Evaluate corner cases that might eventually come to this, for now nothing we can do, just move on with the request
      // throw new InternalServerError('Could not validate user balance for this asset or operation, contact the support');
      next();
      return;
    }

    const wallet: Wallet = await sourceFn(req);

    // Make sure we have a source wallet to check
    if (!wallet) {
      throw new EntityNotFoundError('Source Wallet');
    }

    const assets = blockedAmounts.map(([asset]) => asset);

    const [blockchainBalances, pendingDebtsAmount, blockedBalances] = await Promise.all([
      walletService.getBalancesFromStellar(wallet, assets),
      WalletPendingBalanceView.getWalletPendingDebts(wallet.id),
      Promise.all(
        blockedAmounts.map(([asset, amount]) => authorizerQueryService.updateBlockedBalance({ amount, asset, wallet })),
      ),
    ]);

    const remainingBalances: BalanceMap = Object.fromEntries(
      Object.entries(amountRequestMap).map(([key, { asset }]) => [key, { asset, amount: 0 }]),
    );
    for (const [assetCode, amountMap] of Object.entries(remainingBalances)) {
      const assetBlockChainBalance = blockchainBalances.find(({ code }) => code === assetCode).balance;

      const assetPendingDebts = Number(pendingDebtsAmount[amountMap.asset.id] ?? 0);

      const assetBlockedBalance = Number(
        blockedBalances.find((registration) => registration.assetId === amountMap.asset.id).blockedBalance,
      );

      amountMap.amount += assetBlockChainBalance - assetPendingDebts - assetBlockedBalance;
    }

    const negativeBalances = Object.values(remainingBalances).filter(({ amount }) => amount < 0);
    if (negativeBalances.length > 0) {
      // Release blocked balances.
      await Promise.all(
        blockedAmounts.map(([asset, amount]) =>
          authorizerQueryService.updateBlockedBalance({ amount: -amount, asset, wallet }),
        ),
      );

      /*
       * If the blockedBalance returned from the update blockedBalance query is different than the ammount we requested
       * to be blocked, in other words, there was already an outstanding block in the wallet's balance, it probably is
       * due to concurrent requests trying to create a debt to this wallet, so we return an error which more or less says
       * "try again". After some discussion it was decided that the less terrible way to signal that would be with an
       * HTTP 425 "too early" status code.
       *
       * This is effectively a race condition which we allow to happen because the possible side-effect is that we end up
       * authorizing LESS than would be possible to a given wallet, which while it is definitelly not a great user
       * to be told that you can't execute an operation due to insufficient balance when you're sure that you have enough
       * money, it is certainly better than letting the user spend more money than they have.
       *
       * The other option we had to handle this concurrency issue was to use explicit locking of the wallet, allowing
       * only a single concurrent transaction per wallet at a time. This was done previously but for the PIX feature a
       * client had a use case which required a single wallet operating as a pre-paid wallet to execute the transactions
       * for 6 million accounts, which would not be possible by processing all the transactions serially. So we allowed
       * concurrent transactions with a minor tweak so that the consequences of a race condition would be a mild
       * annoyance to the user instead of creating a liability for the company, bankrupting it and everybody losing their
       * jobs.
       *
       * TODO: add a link do a diagram showing why this race-condition is "safe"
       */
      const mismatchedBlockedBalances = blockedAmounts.some(([asset, amount]) => {
        const { blockedBalance } = blockedBalances.find(({ assetId }) => assetId === asset.id);
        return Number(blockedBalance) !== amount;
      });

      if (mismatchedBlockedBalances) {
        // TODO: add more info.
        throw new HttpError('Try again', 425);
      }

      throw new HttpError('User does not have enough balance.', HttpCode.Client.PRECONDITION_FAILED, {
        assets: negativeBalances.map(({ asset }) => asset.code),
      });
    }

    req.cache.set<[Asset, number][]>('blocked_amounts', blockedAmounts);
    next();
  };
};
