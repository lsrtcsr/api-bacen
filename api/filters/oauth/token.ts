import { UserStatus } from '@bacen/base-sdk';
import * as Sentry from '@sentry/node';
import { InsufficientScopeError } from 'oauth2-server';
import { BaseResponse } from 'ts-framework';
import { logger } from '../../../config/logger.config';
import { ForbiddenRequestError, UnauthorizedRequestError } from '../../errors';
import { bacenRequest, Domain, OAuthAccessToken, OAuthClient, User, UserState } from '../../models';
import config from '../../../config';
import { MetricsService, AvailableMetrics } from '../../services';

export const logAction = (req: bacenRequest) => {
  let userMessage: string;
  if (req.user) {
    userMessage = `${req?.user?.role}`;
  } else {
    userMessage = 'Anonymous';
  }
  const { method, path } = req;

  logger.debug(`${userMessage} sent ${method} request to ${path}`, {
    path: req.path,
    user: req?.user?.id,
    role: req?.user?.role,
    url: req.originalUrl,
    method: req.method,
  });
};

/**
 * The OAuth 2.0 authentication middleware.
 *
 * @param req The express request
 * @param res The express response
 * @param next The express next middleware in chain
 */
const token = (scopes?: string | string[], allowFailure = false) => (
  req: bacenRequest,
  res: BaseResponse,
  next: () => void,
) => {
  const scope = Array.isArray(scopes) ? scopes.join(',') : scopes;

  // This is a weird trick to take advantage of the `express-oauth-server` without actually using it's middleware
  // We pass a function as `next` to the middleware, and then we can use it as a common function with a callback
  // Weird as hell, but it's CTO code, go talk to him before trying to refactor it
  return (req.app as any).oauth.authenticate({ scope })(req, res, async (error?: Error) => {
    if (error) {
      if (allowFailure) {
        return next();
      }

      req.logger.error('Uknown OAuth error', error);

      if (error instanceof InsufficientScopeError) {
        return res.error(new ForbiddenRequestError('Insufficient scope in access token for this action'));
      }

      return res.error(new UnauthorizedRequestError('Credentials missing or invalid'));
    }

    if (res.locals.oauth && res.locals.oauth.token) {
      const { user } = res.locals.oauth.token;

      req.instanceId = config.server.instanceId;
      req.ispb = config.dict.ispb;

      if (!user.virtual) {
        req.accessToken = res.locals.oauth.token;

        // Handle regular users in request
        req.user = await User.loadRequestUser(user.id);

        req.user.virtual = false;
      } else {
        // Handle virtual users for client credentials
        const client = res.locals.oauth.token.user.clientId;

        req.user = User.create({
          firstName: client.clientId,
          lastName: '',
          virtual: true,
          ...res.locals.oauth.token.user,
        });

        req.user.states = [UserState.create({ status: UserStatus.ACTIVE, user: req.user })];
        req.user.client = client;
        req.user.domain = res.locals.oauth.token.user.domain || (await Domain.getDefaultDomain());
        req.user.virtual = true;
      }

      Sentry.configureScope((scope) => {
        scope.setUser({ email: req.user.email, id: req.user.id });
      });

      // Go on with the request
      next();

      // Log the request
      logAction(req);

      // Asynchronously check for user agent information in token
      // if (!res.locals.oauth.token.source || !res.locals.oauth.token.ip) {
      //   try {
      //     // Update user agent in the database
      //     await OAuthAccessToken.updateUserAgent(res.locals.oauth.token.accessToken, req['clientIp'], req['useragent']);
      //   } catch (error) {
      //     req.logger.error(error);
      //   }
      // }
    }
  });
};

export default token;
