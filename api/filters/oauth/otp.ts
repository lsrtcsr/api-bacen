import { UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { ForbiddenRequestError, UnauthorizedRequestError } from '../../errors';
import { bacenRequest, Consumer, Phone, User } from '../../models';

/**
 * Wrapper for getting the OTP from the requested user.
 */
export type UserFromRequest = (req: bacenRequest) => Promise<User | undefined>;

export default function ensureOTP(forceForAllUsers: boolean = false, getUserFromRequest?: UserFromRequest) {
  return async (req: bacenRequest, res: BaseResponse, next: Function) => {
    const token = req.header('x-bacen-otp');
    const user = getUserFromRequest ? await getUserFromRequest(req) : req.user;

    if (!user) {
      throw new UnauthorizedRequestError('You need to be authenticated to perform this action');
    }

    // Check the need for a full OTP checking, based on user account
    const otp = user.twoFactorSecret;
    const shouldCheckOTP = forceForAllUsers || user.twoFactorRequired;

    if (shouldCheckOTP && (!token || !token.length)) {
      throw new ForbiddenRequestError('To perform this operation you need a two factor OTP token', {
        url: req.url,
      });
    } else if (shouldCheckOTP && !otp) {
      throw new HttpError(
        'No two factor configuration available for specified user',
        HttpCode.Client.PRECONDITION_FAILED,
      );
    } else if (shouldCheckOTP) {
      const isValid = await otp.verify(token);

      if (!isValid) {
        throw new ForbiddenRequestError('Invalid or expired OTP token', {
          url: req.url,
        });
      }
    }

    // Check the need for a legacy phone token based on the verification flow
    const isPhoneFlagEnabled = UnleashUtil.isEnabled(UnleashFlags.FORCE_PHONE_TWO_FACTOR);

    if (isPhoneFlagEnabled) {
      // Check the need for a full OTP checking, based on user account
      const consumer = await Consumer.safeFindOne({ where: { user } });
      const phone = await Phone.safeFindOne({ where: { consumer, default: true } });

      if (!(await phone.validateVerificationHash(token, req.accessToken))) {
        throw new ForbiddenRequestError('The SMS verification token is invalid or has expired.');
      }
    }

    next();
  };
}
