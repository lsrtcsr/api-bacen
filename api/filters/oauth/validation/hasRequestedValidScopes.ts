import { HttpCode, HttpError } from 'ts-framework';

/**
 * Checks if the scopes requested exists in current access token
 */
export default async function hasRequestedValidScopes(req, res, next) {
  const accessToken = res.locals.oauth ? res.locals.oauth.token : undefined;

  if (!accessToken) {
    throw new HttpError('The access token provided is not valid', HttpCode.Client.BAD_REQUEST);
  }

  // Get requested scopes from body or params
  const requestedScopes: string[] = req.param('scope') || req.param('scopes');

  // Scopes are optional, but if passed must be validated
  if (requestedScopes && requestedScopes.length) {
    // Get the available scopes from current access token
    const availableScopes: string[] = accessToken.scope;

    // Ensures all requested scopes exist in the available scopes array
    const hasAll: boolean = requestedScopes
      .map(requested => {
        return availableScopes.indexOf(requested) >= 0;
      })
      .reduce((aggr, next) => {
        return aggr && next;
      }, true);

    if (!hasAll) {
      // The client has requested something we could not find in the current access token scopes
      throw new HttpError('The requested scopes are invalid or not available', HttpCode.Client.FORBIDDEN);
    }
  }

  // Everything looks ok
  next();
}
