import { BaseResponse } from 'ts-framework';
import { bacenRequest, Domain } from '../../models';

export default async function url(req: bacenRequest, res: BaseResponse, next: (err?: Error) => void) {
  const { hostname } = req;

  let sourceDomain = await Domain.createQueryBuilder('domain')
    .where(':hostname = ANY(domain.urls) AND domain.deletedAt IS NULL', { hostname })
    .getOne();

  // This URL does not exists on our DB - use the default domain
  if (!sourceDomain) {
    sourceDomain = await Domain.getDefaultDomain();
  }

  req.  = { ...req. , domain: sourceDomain };
  return next();
}
