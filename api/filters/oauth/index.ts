import hasRequestedValidScopes from './validation/hasRequestedValidScopes';
import hasValidRevoke from './validation/hasValidRevoke';
import token from './token';
import url from './url';
import otp from './otp';

export default {
  hasRequestedValidScopes,
  hasValidRevoke,
  token,
  otp,
  url,
};
