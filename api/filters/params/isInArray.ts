import { BaseResponse } from 'ts-framework';
import { InvalidRequestError } from '../../errors';
import { bacenRequest } from '../../models';

export interface IsInArrayOptions {
  /**
   * Param name
   */
  paramName: string;

  /**
   * The name of the incoming request property which request has it
   */
  paramFn?: (req: bacenRequest) => string | undefined;

  /**
   * The list
   */
  requiredArray: any[];

  /**
   * A flag telling if the middleware chain should proceed in case the param is not found
   */
  allowEmpty?: boolean;
}

// tslint:disable-next-line:max-line-length
export default (options: IsInArrayOptions) => {
  const { paramName, paramFn, requiredArray, allowEmpty = true } = options;

  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    // Get from payload
    const paramValue: string = paramFn ? paramFn(req) : req.body[paramName];

    if (!paramValue && !allowEmpty) {
      throw new InvalidRequestError(`Param '${paramName}' was not found on request and it's required`, {
        paramName,
        allowEmpty,
      });
    }

    if (!requiredArray.includes(paramValue) && !allowEmpty) {
      throw new InvalidRequestError(`Param '${paramName}' was not included on required list`, {
        paramName,
        requiredArray,
      });
    }

    next();
  };
};
