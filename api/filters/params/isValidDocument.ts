import Ajv, { JSONSchemaType } from 'ajv';
import addFormats from 'ajv-formats';
import { DocumentType } from '@bacen/base-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { bacenRequest } from '../../models';
import { DocumentDto } from '../../repositories';

export const DOCUMENT_TYPES_WITH_NUMBER: Set<DocumentType> = new Set([
  DocumentType.BRL_DRIVERS_LICENSE,
  DocumentType.BRL_INDIVIDUAL_REG,
  DocumentType.PASSPORT,
]);

const ajv = new Ajv({ allErrors: true, removeAdditional: true });
// probably some typeErrors due to avj being on latest version and addFormats still expecting some interface from
// previous versions
addFormats(ajv as any, { formats: ['date', 'date-time', 'uuid'] });

const createDocumentDtoValidationSchema = {
  type: 'object',
  properties: {
    type: { type: 'string', enum: Object.values(DocumentType) },
    number: { type: 'string', nullable: true, pattern: '\\d+' },
    issuer: { type: 'string', nullable: true },
    issuerState: { type: 'string', minLength: 2, maxLength: 2, nullable: true },
    expirationDate: { type: 'string', format: 'date', nullable: true },
  },
  required: ['type'],
} as JSONSchemaType<DocumentDto>;

const validator = ajv.compile(createDocumentDtoValidationSchema);

export function CreateDocumentDtoValidationMiddleware(
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: any) => void,
): void {
  validator(req.body);
  const validationErrors = validator.errors;

  if (validationErrors?.length) {
    throw new HttpError('Invalid request body', HttpCode.Client.BAD_REQUEST, { errors: validationErrors });
  }

  const { type, number } = req.body;

  if (DOCUMENT_TYPES_WITH_NUMBER.has(type) && !number) {
    throw new HttpError('Number field is required for the provided documentType', HttpCode.Client.BAD_REQUEST, {
      type,
    });
  }

  next();
}
