import Validate, { Params } from 'ts-framework-validation';
import isInArray from './isInArray';
import isValidAddress from './isValidAddress';
import isValidAsset from './isValidAsset';
import isValidBanking from './isValidBanking';
import { CreateDocumentDtoValidationMiddleware as isValidDocument } from './isValidDocument';
import isValidDomain from './isValidDomain';
import isValidPhone from './isValidPhone';
import isValidUser from './isValidUser';
import isValidWallet from './isValidWallet';
import isValidProvider from './isValidProvider';
import isValidAuthorization from './isValidAuthorization';

export default {
  /** ************************************************************************************* */
  /*     Entity validators                                                               */
  /** ************************************************************************************* */

  isInArray,
  isValidProvider,
  isValidAddress,
  isValidAsset,
  isValidBanking,
  isValidDocument,
  isValidDomain,
  isValidPhone,
  isValidUser,
  isValidWallet,
  isValidAuthorization,

  /** ************************************************************************************* */
  /*     Default validators                                                               */
  /** ************************************************************************************* */

  isValidId: Validate.middleware('id', Params.isValidId),
  isValidName: Validate.middleware('name', Params.isValidName),
  isValidFirstName: Validate.middleware('firstName', Params.isValidName),
  isValidLastName: Validate.middleware('lastName', Params.isValidName),
  isValidDescription: Validate.middleware('description', Params.isValidName),
  isValidEmail: Validate.middleware('email', Params.isValidEmail),
  isValidPhoneNumber: Validate.middleware('phone', Params.isValidPhoneNumber),
  isValidPassword: Validate.middleware('password', Params.isValidPassword),

  /** ************************************************************************************* */
  /*     Custom validators                                                                */
  /** ************************************************************************************* */

  /**
   * Checks a user token.
   *
   * TODO: Better logic, maybe uuid.v4() ?.
   */
  isValidToken: Validate.middleware('token', async (token) => token && token.length >= 8),

  /**
   * Checks a phone varification token
   */
  isValidPhoneVerificationToken: Validate.middleware('token', async (token) => token && token.length >= 4),
};
