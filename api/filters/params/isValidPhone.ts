import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest, Phone } from '../../models';

export default (skipMissingProperties: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const phone = Phone.create(req.body);

    const validationErrors = await phone.validate({ skipMissingProperties });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('Phone', validationErrors);
    }

    next();
  };
};
