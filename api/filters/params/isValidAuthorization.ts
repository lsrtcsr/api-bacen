import { AuthorizerEvent } from '@bacen/base-sdk';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest } from '../../models';
import { AuthorizationRequestDto } from '../../schemas/dto/AuthorizationRequestDto';

export default (skipMissingProperties: boolean = false) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const authorizationRequest = plainToClass(AuthorizationRequestDto, req.body);

    const validationErrors = await validate(authorizationRequest, {
      groups: [req.body.event || AuthorizerEvent.AUTHORIZATION],
    });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('AuthorizationRequest', validationErrors);
    }

    next();
  };
};
