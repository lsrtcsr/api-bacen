import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { Address, bacenRequest } from '../../models';

export default (skipMissingProperties: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const address = Address.create(req.body);

    const validationErrors = await address.validate({ skipMissingProperties });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('Address', validationErrors);
    }

    next();
  };
};
