import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest, Domain } from '../../models';

export default (skipMissingProperties: boolean = false) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const domain = Domain.create(req.body);

    const validationErrors = await domain.validate({ skipMissingProperties });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('Domain', validationErrors);
    }

    next();
  };
};
