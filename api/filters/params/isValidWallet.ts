import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest, Wallet } from '../../models';

export default (skipMissingProperties: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const wallet = Wallet.create(req.body);

    const validationErrors = await wallet.validate({ skipMissingProperties });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('Wallet', validationErrors);
    }

    next();
  };
};
