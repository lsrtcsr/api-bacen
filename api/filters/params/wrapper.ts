import * as dot from 'dot-prop';
import { HttpError } from 'ts-framework';
import { InvalidParameterError } from '../../errors';

// TODO: Remove this when it's integrated in ts-framework

/**
 * The type definition for a param validator instance.
 */
export type ParamValidator = (data: any) => Promise<boolean>;

/**
 * Run a single async param validation.
 *
 * @param req The express request
 * @param param The param key
 * @param filter The param validator
 */
const validateParams = async (obj: any, param: string, filter: ParamValidator) => {
  const value = dot.get(obj, param);

  try {
    const result = await filter(value);

    if (result) {
      return true;
    }
  } catch (exception) {
    if (exception instanceof HttpError) {
      throw exception;
    }
    throw new InvalidParameterError('Parameter', null, {
      param,
      value,
      exception: exception.message,
    });
  }

  throw new InvalidParameterError('Parameter', null, {
    param,
    value,
  });
};

const validateOptionalParams = async (obj: any, param: string, filter: ParamValidator) => {
  const value = dot.get(obj, param);

  if (!value) {
    return true;
  }

  return validateParams(obj, param, filter);
};

/**
 * Wraps a simple param validator into an Express middleware.
 *
 * @param {String} param The param name to be fetch using `req.param(name)`
 * @param {ParamValidator} filter The filter instance to be wrapped
 */
export const wrapper = (param: string, filter: ParamValidator) => async (req, res, next) => {
  await validateParams({ ...req.params, ...req.query, ...req.body }, param, filter);
  next();
};

/**
 * Wraps a series of params into a single Express middleware.
 *
 * @param {String} param The param name to be fetch using `req.param(name)`
 * @param {ParamValidator} filter The filter instance to be wrapped
 */
export const wrapGroup = (params: { [label: string]: ParamValidator }) => async (req, res, next) => {
  const p = Object.keys(params);

  // Validate all params in series
  for (let i = 0; i < p.length; i += 1) {
    const param = p[i];
    await validateParams({ ...req.params, ...req.query, ...req.body }, param, params[p[i]]);
  }
  next();
};

export const wrapOptionalGroup = (params?: { [label: string]: ParamValidator }) => async (req, res, next) => {
  const p = Object.keys(params);

  // Validate all params in series
  for (let i = 0; i < p.length; i += 1) {
    const param = p[i];
    await validateOptionalParams({ ...req.params, ...req.query, ...req.body }, param, params[p[i]]);
  }
  next();
};

export const wrapArrayGroup = (arrayName: string, params: { [label: string]: ParamValidator }) => async (
  elements: any[],
) => {
  for (let i = 0; i < elements.length; i += 1) {
    const p = Object.keys(elements[i]);

    // Validate all params in series
    for (let j = 0; j < p.length; j += 1) {
      const param = p[j];
      const path = `${arrayName}.${i}.${param}`;

      try {
        await validateParams(elements[i], param, params[param]);
      } catch (exception) {
        throw new InvalidParameterError('Parameter', null, {
          path,
          value: elements[i][param],
          param: arrayName,
          index: i,
        });
      }
    }
  }
  return true;
};
