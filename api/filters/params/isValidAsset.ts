import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest, Asset } from '../../models';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const asset = Asset.create(req.body);
  const validationErrors = await asset.validate();

  if (validationErrors.length > 0) {
    return next(new InvalidParameterError('Asset', validationErrors));
  }

  next();
};
