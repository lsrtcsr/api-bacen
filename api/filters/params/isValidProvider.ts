import { BaseResponse } from 'ts-framework';
import { CustodyProvider } from '@bacen/base-sdk';
import { InvalidRequestError } from '../../errors';
import { bacenRequest } from '../../models';

export default (paramKey: string, isRequired: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const provider: CustodyProvider = req.param(paramKey) || req.body[paramKey];

    if (!provider && !isRequired) return next();

    if ((!provider && isRequired) || !Object.values(CustodyProvider).some(value => value === provider)) {
      throw new InvalidRequestError('The required parameter "provider" was not sent or is invalid');
    }

    next();
  };
};
