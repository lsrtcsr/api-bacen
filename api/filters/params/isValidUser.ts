import { BaseResponse } from 'ts-framework';
import { plainToClass } from 'class-transformer';
import { InvalidParameterError } from '../../errors';
import { bacenRequest, User, CompanyData } from '../../models';

export default (skipMissingProperties = false) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const user = User.create(req.body);

    if (req.body.consumer && req.body.consumer.taxId) {
      req.body.consumer.taxId = req.body.consumer.taxId.replace(/[^0-9]/g, '');
    }

    const validationGroups = [];
    validationGroups.push(user.role);
    user.consumer && validationGroups.push(user.consumer.type);

    let validationErrors = await user.validate({ skipMissingProperties, groups: validationGroups });

    if (user.consumer) {
      const consumerValidationErrors = await user.consumer.validate({
        skipMissingProperties,
        groups: validationGroups,
      });

      validationErrors = [...validationErrors, ...consumerValidationErrors];

      if (user.consumer?.companyData) {
        const companyData = plainToClass(CompanyData, user.consumer.companyData);
        const companyDataValidationErrors = await companyData.validate({
          skipMissingProperties,
          groups: validationGroups,
        });

        validationErrors = [...validationErrors, ...companyDataValidationErrors];

        if (user.consumer?.companyData?.partners) {
          const companyPartnersValidationPromises = companyData.partners.map((partner) => partner.validate());
          const companyPartnersValidationErrors = await Promise.all(companyPartnersValidationPromises);

          validationErrors = [...validationErrors, ...companyPartnersValidationErrors.flat()];
        }
      }
    }

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('User', validationErrors);
    }

    next();
  };
};
