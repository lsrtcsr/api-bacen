import { BaseResponse } from 'ts-framework';
import { AccountType } from '@bacen/base-sdk';
import { InvalidParameterError } from '../../errors';
import { Banking, bacenRequest } from '../../models';

export default (skipMissingProperties: boolean = true) => {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const banking: Banking = Banking.create(req.body);

    const type = banking.holderType || AccountType.PERSONAL;
    const validationGroups = [];
    validationGroups.push(type);

    const validationErrors = await banking.validate({ skipMissingProperties, groups: validationGroups });

    if (validationErrors.length > 0) {
      throw new InvalidParameterError('Banking', validationErrors);
    }

    next();
  };
};
