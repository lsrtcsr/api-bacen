import { BaseResponse } from 'ts-framework';
import { bacenRequest, Phone } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId, phoneId } = req.params;

  const count = await Phone.createQueryBuilder('phone')
    .leftJoin('phone.consumer', 'consumer')
    .leftJoin('consumer.user', 'user')
    .where('user.id = :userId', { userId })
    .andWhere('phone.id = :phoneId', { phoneId })
    .andWhere('phone.deleted_at IS NULL')
    .getCount();

  if (!count) throw new EntityNotFoundError('Phone', { userId, phoneId });

  next();
};
