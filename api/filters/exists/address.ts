import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { Address, bacenRequest } from '../../models';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId, addressId } = req.params;

  const count = await Address.createQueryBuilder('address')
    .leftJoin('address.consumer', 'consumer')
    .leftJoin('consumer.user', 'user')
    .where('user.id = :userId', { userId })
    .andWhere('address.id = :addressId', { addressId })
    .andWhere('address.deletedAt IS NULL')
    .getCount();

  if (count < 1) throw new EntityNotFoundError('Address', { addressId, userId });

  next();
};
