import { DocumentType } from '@bacen/base-sdk';
import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { bacenRequest, Document } from '../../models';
import { isUUID } from '../../utils';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId, documentId, documentIdOrType } = req.params;

  const idOrType = documentId || documentIdOrType;

  let documentQuery = Document.createQueryBuilder('document')
    .leftJoinAndSelect('document.consumer', 'consumer')
    .leftJoinAndSelect('consumer.user', 'user')
    .leftJoinAndSelect('document.states', 'document_states')
    .where('user.id = :userId', { userId })
    .orderBy('"isActive"', 'DESC')
    .andWhere('document.deletedAt IS NULL');

  if (isUUID(idOrType)) {
    // Is Id
    documentQuery = documentQuery.andWhere('document.id = :documentId', { documentId: idOrType });
  } else {
    // Is type
    if (!Object.values(DocumentType).includes(idOrType as any)) {
      throw new EntityNotFoundError('Document', { user: userId, document: idOrType });
    }

    documentQuery = documentQuery.andWhere('document.type = :documentType', { documentType: idOrType });
  }

  const document = await documentQuery.getCount();
  if (!document) throw new EntityNotFoundError('Document', { user: userId, document: idOrType });

  next();
};
