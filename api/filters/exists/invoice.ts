import { BaseResponse } from 'ts-framework';
import { FindConditions } from 'typeorm';
import { bacenRequest, Invoice } from '../../models';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import { isUUID } from '../../utils';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId, id } = req.params;

  const where: FindConditions<Invoice> = userId ? { contractor: { id: userId } } : {};
  if (isUUID(id)) {
    where.id = id;
  } else if (userId) {
    where.current = true;
  } else {
    throw new InvalidRequestError('The user ID is required for querying the current invoice');
  }

  const count = await Invoice.safeCount({ where });
  if (!count) throw new EntityNotFoundError(`${id} not found`);

  next();
};
