import { BaseResponse } from 'ts-framework';
import { bacenRequest, Plan } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { id } = req.params;

  const count = await Plan.safeCount({ where: { id } });
  if (!count) throw new EntityNotFoundError(`${id} not found`);

  next();
};
