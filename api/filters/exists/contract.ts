import { BaseResponse } from 'ts-framework';
import { bacenRequest, Contract } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { subscriptionId } = req.params;

  const count = await Contract.safeCount({ where: { id: subscriptionId } });
  if (!count) throw new EntityNotFoundError(`${subscriptionId} not found`);

  next();
};
