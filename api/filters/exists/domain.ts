import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { bacenRequest, Domain } from '../../models';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { id, domain }: { id?: string; domain?: string } = { ...req.params, ...req.query, ...req.body };

  const count = await Domain.safeCount({ where: { id: domain || id } });
  if (!count) throw new EntityNotFoundError('Domain', { id: domain || id });

  next();
};
