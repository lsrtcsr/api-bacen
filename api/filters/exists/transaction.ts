import { BaseResponse } from 'ts-framework';
import { bacenRequest, Transaction } from '../../models';
import { EntityNotFoundError } from '../../errors';
import { isUUID } from '../../utils';

export default (paramName = 'transactionId') => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: any) => void,
) => {
  const id = req.param(paramName);

  let qb = Transaction.createQueryBuilder('transaction').where('transaction.deletedAt IS NULL');

  if (isUUID(id)) {
    qb = qb.andWhere('transaction.id = :id', { id });
  } else {
    qb = qb.andWhere(
      "(transaction.additional_data->>'hash' = :hash OR transaction.additional_data->>'instructionId' = :instructionId)",
      { hash: id, instructionId: id },
    );
  }

  const count = await qb.getCount();
  if (!count) throw new EntityNotFoundError('Transaction', { id });

  next();
};
