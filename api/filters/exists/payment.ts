import { BaseResponse } from 'ts-framework';
import { bacenRequest, Payment } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default (paramName: string = 'paymentId') => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: any) => void,
) => {
  const id = req.param(paramName);

  const count = await Payment.safeCount({ where: { id } });
  if (!count) throw new EntityNotFoundError('Payment', { id });

  next();
};
