import { BaseResponse } from 'ts-framework';
import { bacenRequest, Asset } from '../../models';
import { EntityNotFoundError } from '../../errors';

const DEFAULT_ASSET_KEY_FN = (req: bacenRequest) => req.params.assetId || req.params.id;

export interface AssetExistsOptions {
  /**
   * A flag defining Wheter or not to save the loaded asset into the request cache.
   */
  cache?: boolean;

  /**
   * A function which extracts the assetId from the incoming request.
   */
  assetKeyFn?: (req: bacenRequest) => string | undefined;

  /**
   * A flag saying wheter we should allow the middleware chain to continue if we do not find an asset instead of
   * throwing and returing imediatelly
   */
  allowFailure?: boolean;
}

export default function assetExistsMiddlewareFactory(options: AssetExistsOptions = {}) {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const { assetKeyFn = DEFAULT_ASSET_KEY_FN, cache = false, allowFailure = false } = options;
    // issuer relation is mandatory and already included.
    const assetIdOrCode = assetKeyFn(req) ?? '';

    if (!cache) {
      const asset = await Asset.getByIdOrCode(assetIdOrCode.toUpperCase());
      if (!asset && !allowFailure) throw new EntityNotFoundError('asset', { assetId: assetIdOrCode });
    } else {
      const asset = await Asset.getByIdOrCode(assetIdOrCode.toUpperCase());

      if (asset) {
        req.cache.set<Asset>(`asset_${assetIdOrCode}`, asset);
      } else if (!allowFailure) {
        throw new EntityNotFoundError('asset', { assetId: assetIdOrCode });
      }
    }

    next();
  };
}
