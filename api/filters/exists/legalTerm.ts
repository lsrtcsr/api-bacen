import { BaseResponse } from 'ts-framework';
import { bacenRequest, LegalTerm } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default (paramName: string = 'legalTermId') => async (
  req: bacenRequest,
  res: BaseResponse,
  next: (err?: any) => void,
) => {
  const id = req.param(paramName);

  const count = await LegalTerm.safeCount({ where: { id } });
  if (!count) throw new EntityNotFoundError('LegalTerm', { id });

  next();
};
