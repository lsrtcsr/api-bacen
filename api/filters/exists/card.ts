import { BaseResponse } from 'ts-framework';
import { bacenRequest, Card } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { walletId, cardId } = req.params;

  const count = await Card.safeCount({ where: { id: cardId, wallet: { id: walletId } } });
  if (!count) throw new EntityNotFoundError('Card', { walletId, cardId });

  next();
};
