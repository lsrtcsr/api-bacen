import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { Alert, bacenRequest } from '../../models';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { id } = req.params;

  const count = await Alert.safeCount({ where: { id } });
  if (!count) throw new EntityNotFoundError('Alert', { id });

  next();
};
