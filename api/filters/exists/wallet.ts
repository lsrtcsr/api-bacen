import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { leftJoinAndSelectRelations } from '@bacen/shared-sdk';
import { bacenRequest, Wallet } from '../../models';

const DEFAULT_WALLET_KEY_FN = (req: bacenRequest) => req.params.walletId || req.params.id;

export interface WalletExistsOptions {
  /**
   * A flag defining Wheter or not to save the loaded wallet into the request cache.
   */
  cache?: boolean;

  /**
   * A function which extracts the walletId from the incoming request.
   */
  walletKeyFn?: (req: bacenRequest) => string | undefined;

  /**
   * A flag saying wheter we should allow the middleware chain to continue if we do not find a wallet instead of
   * throwing and returing imediatelly
   */
  allowFailure?: boolean;

  /**
   * The relations to be joined on the wallet query. Only relevant when used in conjunction with `cache: true`
   */
  relations?: string[];
}

export default function walletExistsMiddlewareFactory(options: WalletExistsOptions = {}) {
  return async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
    const {
      walletKeyFn = DEFAULT_WALLET_KEY_FN,
      cache = false,
      relations: relationsArg = [],
      allowFailure = false,
    } = options;
    // user relation is mandatory
    const relationList = Array.from(new Set(['user', ...relationsArg]));
    const walletId = walletKeyFn(req);

    if (!cache) {
      const count = await Wallet.createQueryBuilder('wallet')
        .leftJoin('wallet.user', 'user')
        .where('wallet.id = :walletId', { walletId })
        .andWhere('wallet.deletedAt IS NULL')
        .andWhere('user.deletedAt IS NULL')
        .getCount();
      if (!count && !allowFailure) throw new HttpError(`Wallet ${walletId} not found`, HttpCode.Client.NOT_FOUND);
    } else {
      const baseQuery = Wallet.createQueryBuilder('wallet')
        .where('wallet.id = :walletId', { walletId })
        .andWhere('wallet.deletedAt IS NULL')
        .andWhere('user.deletedAt IS NULL');

      leftJoinAndSelectRelations(baseQuery, relationList, 'wallet');

      const wallet = await baseQuery.getOne();
      if (wallet) {
        req.cache.set<Wallet>(`wallet_${walletId}`, wallet);
      } else if (!allowFailure) {
        throw new HttpError(`Wallet ${walletId} not found`, HttpCode.Client.NOT_FOUND);
      }
    }

    next();
  };
}
