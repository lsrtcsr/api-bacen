import { BaseResponse } from 'ts-framework';
import { Issue, bacenRequest } from '../../models';
import { EntityNotFoundError } from '../../errors';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { id } = req.params;

  const count = await Issue.safeCount({ where: { id } });
  if (!count) throw new EntityNotFoundError('Issue', { id });

  next();
};
