import { UserRole } from '@bacen/base-sdk';
import AddressExists from './address';
import AlertExists from './alert';
import AssetExistsMiddlewareFactory from './asset';
import BankingExists from './banking';
import CardExists from './card';
import DocumentExists from './document';
import DomainExists from './domain';
import IssueExists from './issue';
import PaymentExists from './payment';
import PhoneExists from './phone';
import UserExists, { UserHasConsumerMiddlewareFactory } from './user';
import WalletExistsFactory from './wallet';
import PlanExists from './plan';
import ContractExists from './contract';
import InvoiceExists from './invoice';
import ServiceExists from './service';
import LegalTermExists from './legalTerm';
import TransactionExists from './transaction';

export default {
  Address: AddressExists,
  Alert: AlertExists,
  Asset: AssetExistsMiddlewareFactory,
  Banking: BankingExists,
  Card: CardExists,
  Document: DocumentExists,
  Domain: DomainExists,
  Issue: IssueExists,
  Phone: PhoneExists,
  Wallet: WalletExistsFactory,
  Consumer: UserExists(UserRole.CONSUMER),
  Mediator: UserExists(UserRole.MEDIATOR),
  User: UserExists(),
  UserHasConsumer: UserHasConsumerMiddlewareFactory,
  Payment: PaymentExists,
  Plan: PlanExists,
  Contract: ContractExists,
  Invoice: InvoiceExists,
  Service: ServiceExists,
  LegalTerm: LegalTermExists,
  Transaction: TransactionExists,
};
