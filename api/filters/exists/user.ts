import { UserRole } from '@bacen/base-sdk';
import { leftJoinAndSelectRelations } from '@bacen/shared-sdk';
import { BaseResponse, HttpCode, HttpError } from 'ts-framework';
import { IsNull, Not } from 'typeorm';
import { EntityNotFoundError, InvalidRequestError } from '../../errors';
import { bacenRequest, Consumer, User } from '../../models';
import { isUUID } from '../../utils';

export default (role?: UserRole) => async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId } = req.params;

  // Check with a valid userId is informed
  if (!isUUID(userId)) {
    throw new InvalidRequestError(`${userId} should be a valid UUID v4`);
  }

  const count = await User.safeCount({ where: { id: userId, role: role || Not(IsNull()) } });
  if (!count) throw new EntityNotFoundError('User');

  next();
};

export interface UserHasConsumerMiddlewareOptions {
  userIdParam: string;

  cache?: boolean;

  relations?: string[];
}

export function UserHasConsumerMiddlewareFactory(
  options: UserHasConsumerMiddlewareOptions = { userIdParam: 'userId' },
): (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => void {
  const { userIdParam, cache = true, relations = [] } = options;
  const normalizedRelations = Array.from(new Set(['user', ...relations]));

  return async function UserHasConsumerMiddleware(
    req: bacenRequest,
    res: BaseResponse,
    next: (err?: any) => void,
  ): Promise<void> {
    const userId = req.params[userIdParam];

    if (!userId) {
      throw new HttpError('userId parameter is required', HttpCode.Client.BAD_REQUEST, {
        param: userIdParam,
      });
    }

    const consumerQb = Consumer.createQueryBuilder('consumer').where('user.id = :userId', { userId });

    leftJoinAndSelectRelations(consumerQb, normalizedRelations, 'consumer');
    const consumer = await consumerQb.getOne();

    if (!consumer) {
      throw new HttpError('User does not have a corresponding consumer', HttpCode.Client.BAD_REQUEST, {
        userId,
      });
    }

    if (cache) {
      req.cache.set<Consumer>(`consumer_${userId}`, consumer);
    }
    next();
  };
}
