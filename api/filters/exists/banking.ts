import { BaseResponse } from 'ts-framework';
import { EntityNotFoundError } from '../../errors';
import { Banking, bacenRequest } from '../../models';

export default async (req: bacenRequest, res: BaseResponse, next: (err?: any) => void) => {
  const { userId, bankingId } = req.params;

  const count = await Banking.createQueryBuilder('banking')
    .leftJoin('banking.consumer', 'consumer')
    .leftJoin('consumer.user', 'user')
    .where('user.id = :userId', { userId })
    .andWhere('banking.id = :bankingId', { bankingId })
    .andWhere('banking.deleted_at IS NULL')
    .getCount();

  if (!count) throw new EntityNotFoundError('Banking', { userId, bankingId });

  next();
};
