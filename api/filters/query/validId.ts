import { BaseResponse } from 'ts-framework';
import { InvalidRequestError } from '../../errors';
import { bacenRequest } from '../../models';
import { isUUID } from '../../utils';

export const isValidId = (idProp: string = 'id') => async (
  req: bacenRequest,
  res: BaseResponse,
  next: () => void,
) => {
  if (!isUUID(req.param(idProp))) {
    throw new InvalidRequestError(`${idProp} should be a valid UUID v4`);
  }

  next();
};
