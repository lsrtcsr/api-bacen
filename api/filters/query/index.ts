import pagination from './pagination';
import { isValidId } from './validId';

export default { pagination, isValidId };
