import { BaseResponse } from 'ts-framework';
import { InvalidParameterError } from '../../errors';
import { bacenRequest } from '../../models';

/**
 * Creates a pagination object in the request query attribute.
 *
 * Defaults to:
 *   skip: 0
 *   limit: 25
 *
 * @param req The express request
 * @param res The express response
 * @param next The express next middleware in chain
 */
export default async function pagination(req: bacenRequest, res: BaseResponse, next: () => void) {
  const [skip, limit] = [req.param('skip'), req.param('limit')];

  if (skip && isNaN(skip)) {
    throw new InvalidParameterError('skip');
  }

  if (limit && isNaN(limit)) {
    throw new InvalidParameterError('limit');
  }

  req.query.pagination = {
    // TODO: Move defaults to config file
    skip: Number(req.query.skip) || 0,
    limit: Number(req.query.limit) || 25,
  };

  next();
}
