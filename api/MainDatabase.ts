import { EntityDatabase } from 'ts-framework-sql';
import Config from '../config';
import * as Models from './models';

export default class MainDatabase extends EntityDatabase {
  public static readonly ENTITIES = [
    Models.ExtendedEntity,
    Models.Address,
    Models.Alert,
    Models.Asset,
    Models.AssetRegistration,
    Models.AssetRegistrationState,
    Models.Banking,
    Models.Banking,
    Models.Card,
    Models.Consumer,
    Models.ConsumerState,
    Models.Contract,
    Models.ContractState,
    Models.Document,
    Models.Document,
    Models.DocumentState,
    Models.Domain,
    Models.DomainSettingsLog,
    Models.Entry,
    Models.EntryType,
    Models.Event,
    Models.Invoice,
    Models.InvoiceState,
    Models.Issue,
    Models.LegalTerm,
    Models.LegalTermAcceptance,
    Models.Notification,
    Models.OAuthAccessToken,
    Models.OAuthClient,
    Models.OAuthOTPToken,
    Models.OAuthRefreshToken,
    Models.OAuthSecretToken,
    Models.Payment,
    Models.ViewPaymentWithStates,
    Models.Period,
    Models.PeriodState,
    Models.Phone,
    Models.Plan,
    Models.Postback,
    Models.PostbackDelivery,
    Models.PriceItem,
    Models.Service,
    Models.Settings,
    Models.Transaction,
    Models.TransactionStateItem,
    Models.ViewTransactionByStates,
    Models.ViewTransactionWithStates,
    Models.User,
    Models.UserLimit,
    Models.UserState,
    Models.ViewConsumerByStates,
    Models.ViewConsumerWithStates,
    Models.Wallet,
    Models.WalletState,
    Models.ViewWalletByStates,
    Models.WalletPendingBalanceView,
    Models.Base2HandledTransaction,
    Models.RequestLog,
    Models.ViewPaymentAccounting,
    Models.Request,
  ];

  protected static readonly instance: MainDatabase = new MainDatabase({
    connectionOpts: {
      ...Config.database,
      entities: MainDatabase.ENTITIES,
    },
  } as any);

  /**
   * Gets the singleton database instance.
   */
  static getInstance(): MainDatabase {
    return this.instance;
  }
}
