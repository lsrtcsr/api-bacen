import { BaseResponse } from 'ts-framework';
import { getConnection } from 'typeorm';
import { bacenRequest } from '../../models';
import { PgLockServiceOptions, PgLockService } from '../../services';
import { RouteHandler } from '../../utils';

export type EnableFn = (req?: bacenRequest) => boolean;

export function UseLock(lockIdParam: string, enable: EnableFn = () => true, lockOptions: PgLockServiceOptions = {}) {
  return function(target: Record<string, any>, propertyKey: string, descriptor: TypedPropertyDescriptor<RouteHandler>) {
    const originalMethod = descriptor.value;

    descriptor.value = async function(req: bacenRequest, res: BaseResponse) {
      const isEnabled = enable(req);

      if (isEnabled) {
        const lockId = req.param(lockIdParam);
        const queryRunner = getConnection().createQueryRunner();
        const lockService = new PgLockService(queryRunner, lockOptions);

        try {
          await lockService.tryAcquireLock(lockId);
          await originalMethod.apply(this, [req, res]);
        } finally {
          await lockService.releaseLock(lockId);
          // If the lock fails to be released, since it is a session-level lock it will be released once we close the
          // connection on queryRunner.close()
          await queryRunner.release();
        }
      } else {
        await originalMethod.apply(this, [req, res]);
      }
    };

    return descriptor;
  };
}
