import { BaseRequest, BaseResponse, HttpError } from 'ts-framework';
import * as shimmer from 'shimmer';
import { RouteHandler, Ctor } from '../../utils';
import { IdempotenceStore } from '../../services';

export type IdempotencyKeyFn = (req: BaseRequest) => string | undefined;

export const DEFAULT_IDEMPOTENCY_KEY_FN = (req: BaseRequest) => req.get('X-Idempotence-Key');

export interface IdempotentSetOptions {
  /**
   * The function which retrieves the idempotence key from the request
   * Default: `(req) => req.get('X-Idempotence-Key')`
   */
  keyFn?: IdempotencyKeyFn;

  /**
   * The underlying store to record requests with a given idempotence key.
   */
  store: Ctor<IdempotenceStore, []>;

  /**
   * Controls wheter we should store failed requests for given idempotence key
   * Default: true
   */
  recordFailure?: boolean;

  /**
   * The HTTP status codes which should be considerd a successfull request. Only meningful when `recordFailure = false`
   * Default: [200]
   */
  successCodes?: number[];
}

export function Idempotent(options: IdempotentSetOptions) {
  return function(
    target: Record<string, any>,
    propertyName: string,
    descriptor: TypedPropertyDescriptor<RouteHandler>,
  ) {
    const {
      keyFn = DEFAULT_IDEMPOTENCY_KEY_FN,
      recordFailure = false,
      successCodes = [200],
      store: StoreCtor,
    } = options;

    const originalMethod = descriptor.value;
    descriptor.value = async function(req: BaseRequest, res: BaseResponse) {
      const idempotenceKey = keyFn(req);
      const store = new StoreCtor();

      if (idempotenceKey) {
        try {
          const didLock = await store.lock(idempotenceKey);
          if (!didLock) {
            throw new HttpError('There is already an ongoing operation using the same idempotence key', 423, {
              idempotenceKey,
            });
          }

          const previousResult = await store.get(idempotenceKey);
          if (previousResult) {
            req.logger.debug('Serving request from cached response for given idempotence key', {
              idempotenceKey,
              path: req.path,
            });
            // TODO: should we set the same headers ?
            res
              .type(previousResult.contentType)
              .status(previousResult.status)
              .send(previousResult.body);

            // skip originalMethod and return from here.
            return;
          }

          let sentData: any;
          // monkey-patch res.send so we can have access to the response body.
          shimmer.wrap(res, 'send', originalImpl => {
            return function(this: BaseResponse) {
              // We call slice to copy the first element of arguments
              sentData = Array.prototype.slice.call(arguments, 0, 1)[0];

              return originalImpl.apply(this, arguments);
            };
          });

          await originalMethod.call(this, req, res);
          shimmer.unwrap(res, 'send');

          if (recordFailure || successCodes.includes(res.statusCode)) {
            await store.set(idempotenceKey, {
              contentType: res.get('Content-Type') || 'plain/text',
              status: res.statusCode,
              body: sentData,
            });
          }
        } finally {
          // We run unlock in a finally block to try and guarantee that it will be run and we won't leave anything in a
          // locked state forever.
          await store.unlock(idempotenceKey);
        }
      } else {
        await originalMethod.call(this, req, res);
      }
    };

    return descriptor;
  };
}
