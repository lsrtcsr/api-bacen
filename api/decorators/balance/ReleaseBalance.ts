import { BaseResponse } from 'ts-framework';
import { RouteHandler, MaybePromise } from '../../utils';
import { bacenRequest, Wallet, Asset } from '../../models';
import { AuthorizerQueryService } from '../../services';

import { defaultSourceWallet } from '../../filters/payments';

export function ReleaseBalance(walletFn: (req: bacenRequest) => MaybePromise<Wallet> = defaultSourceWallet) {
  return function(
    target: Record<string, any>,
    propertyName: string,
    descriptor: TypedPropertyDescriptor<RouteHandler>,
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = async function(req: bacenRequest, res: BaseResponse): Promise<void> {
      const authorizerQueryService = AuthorizerQueryService.getInstance();

      try {
        return await originalMethod.call(this, req, res);
      } finally {
        const blockedAmounts = req.cache.get<[Asset, number][]>('blocked_amounts');

        if (blockedAmounts?.length > 0) {
          const wallet = await walletFn(req);
          /*
           * Release blocked balances after request handler.
           * this will not result in double unblocking, if the operation was refused due to insufficient balance an exception
           * would have been thrown and code execution would not have reached this point.
           */
          await Promise.all(
            blockedAmounts.map(([asset, amount]) =>
              authorizerQueryService.updateBlockedBalance({ amount: -amount, asset, wallet }),
            ),
          );
        }
      }
    };
  };
}
