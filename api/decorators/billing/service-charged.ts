import * as moment from 'moment';
import { UserRole, UnleashFlags, ServiceType } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseResponse } from 'ts-framework';
import { Asset, bacenRequest, Transaction, Wallet, User, Contract, AssetRegistration } from '../../models';
import {
  BillingService,
  TransactionPipelinePublisher,
  EventHandlingGateway,
  ContractService,
  AuthorizerQueryService,
} from '../../services';
import { hasFinancialFlow, isUUID, RouteHandler } from '../../utils';
import { EntityNotFoundError, ForbiddenRequestError } from '../../errors';
import checkBalanceAvailability from '../../filters/billing/checkBalanceAvailability';

export default function serviceCharged(serviceType: ServiceType) {
  return function (
    target: Record<string, any>,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<RouteHandler>,
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = async function (req: bacenRequest, res: BaseResponse) {
      await checkBalanceAvailability(serviceType, req);
      const authorizerQueryService = AuthorizerQueryService.getInstance();
      const currentUser = req.user;

      async function executeOriginalMethodAndReleaseBlockedBalance(req: bacenRequest, res: BaseResponse) {
        try {
          await originalMethod.call(this, req, res);
        } finally {
          const blockedAmounts = req.cache.get<[Asset, number][]>('blocked_amounts');
          if (blockedAmounts?.length > 0) {
            req.logger.debug('Releasing blocked balances', {
              blockedAmounts,
              wallet: wallet.id,
              path: req.path,
            });

            /*
             * Release blocked balances after request handler.
             * this will not result in double unblocking, if the operation was refused due to insufficient balance an exception
             * would have been thrown and code execution would not have reached this point.
             */
            await Promise.all(
              blockedAmounts.map(([asset, amount]) =>
                authorizerQueryService.updateBlockedBalance({ amount: -amount, asset, wallet }),
              ),
            );
          }
        }
      }

      let accountable: User;
      let wallet: Wallet;

      let id: string;
      if ((id = req.params.walletId || req.params.id)) {
        wallet =
          req.cache.get<Wallet>(`wallet_${id}`) ||
          (await Wallet.createQueryBuilder('wallet')
            .leftJoinAndSelect('wallet.user', 'user')
            .leftJoinAndSelect('user.domain', 'domain')
            .getOne());

        const assetRegistrations = await AssetRegistration.createQueryBuilder('assetRegistration')
          .leftJoinAndSelect('assetRegistration.states', 'states')
          .leftJoinAndSelect('assetRegistration.asset', 'asset')
          .where('assetRegistration.wallet = :walletId', { walletId: id })
          .getMany();
        wallet.assetRegistrations = assetRegistrations;
        req.cache.set<AssetRegistration[]>(`wallet_asset_registrations_${id}`, assetRegistrations);

        accountable = wallet.user;
      } else if ((id = req.body.source || req.body.destination)) {
        wallet = await Wallet.createQueryBuilder('wallet')
          .leftJoinAndSelect('wallet.user', 'user')
          .leftJoinAndSelect('user.domain', 'domain')
          .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
          .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
          .leftJoinAndSelect('assetRegistrations.asset', 'assets')
          .where('wallet.id = :id', { id })
          .getOne();

        accountable = wallet.user;
      } else {
        // TODO review this assumption
        accountable = await User.safeFindOne({ where: { id: currentUser.id }, relations: ['domain', 'wallets'] });
        wallet = accountable.wallets[0];
      }

      if (!wallet) {
        throw new EntityNotFoundError('Source Wallet');
      }

      // Prepare feature flag context
      let forceBilling = false;
      const context = { userId: accountable.id, properties: { domainId: accountable.domain.id } };
      if (accountable.role === UserRole.CONSUMER) {
        forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_CONSUMER_BILLING, context, false);
      } else if (accountable.role === UserRole.MEDIATOR) {
        forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false);
      }

      // if cachedCurrentContract is undefined it means there is no cache entry. if it is null it means there is
      // no currentContract and this fact has been stored in the cache.
      const cachedCurrentContract = req.cache.get<Contract | null>(`current_contract_user_${accountable.id}`);
      const currentContract =
        typeof cachedCurrentContract === 'undefined'
          ? await ContractService.getInstance().getCurrentContract(accountable.id)
          : cachedCurrentContract;

      if (!forceBilling && !currentContract) {
        return await executeOriginalMethodAndReleaseBlockedBalance(req, res);
      }

      if (forceBilling && !currentContract) {
        throw new ForbiddenRequestError(
          `User invoice not found! To perform this operation, the user ${accountable.id} must subscribe a plan`,
        );
      }

      const rootAsset = await Asset.getRootAsset();

      let transactionAmount = 0;
      if (req.body.amount) {
        let asset: Asset;

        if (req.params.id && isUUID(req.params.id)) {
          asset = await Asset.findOne({ id: req.params.id });
        } else if (req.params.id && req.params.id !== 'root') {
          asset = await Asset.findOne({ code: req.params.id });
        } else {
          asset = await Asset.getRootAsset();
        }

        if (asset.id === rootAsset.id) {
          transactionAmount += +req.body.amount;
        }
      } else if (req.body.recipients) {
        for (const recipient of req.body.recipients) {
          if (recipient.asset !== rootAsset.code) continue;

          transactionAmount += +recipient.amount;
        }
      }

      const serviceFee = await BillingService.getInstance().getServiceFee({
        serviceType,
        transactionAmount,
        asset: rootAsset,
        user: accountable,
        contract: currentContract,
      });

      const endpointHasFinancialFlow = hasFinancialFlow(serviceType);

      if (endpointHasFinancialFlow && currentContract.prepaid && serviceFee && Number(serviceFee.amount) !== 0) {
        req.serviceFee = serviceFee;
      }

      const result = await executeOriginalMethodAndReleaseBlockedBalance(req, res);

      if (!endpointHasFinancialFlow) {
        let transaction: Transaction;
        if (currentContract.prepaid && serviceFee && Number(serviceFee.amount) !== 0) {
          transaction = await rootAsset.destroy({
            serviceFee,
            createdBy: req.accessToken,
            targetWallet: wallet,
          });
          await TransactionPipelinePublisher.getInstance().send(transaction);
        }

        await EventHandlingGateway.getInstance().process(serviceType, {
          transaction,
          user: accountable,
          serviceData: undefined, // TODO
          operationDate: moment(),
        });
      }

      return result;
    };

    return descriptor;
  };
}
