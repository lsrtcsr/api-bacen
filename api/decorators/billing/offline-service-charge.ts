import {
  UserRole,
  UnleashFlags,
  PaymentStatus,
  SeverityLevel,
  IssueType,
  IssueCategory,
  IssueDetails,
  ServiceType,
} from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import * as Package from 'pjson';
import * as moment from 'moment';
import { Asset, bacenRequest, Wallet, Transaction } from '../../models';
import {
  BillingService,
  ContractService,
  TransactionPipelinePublisher,
  EventHandlingGateway,
  IssueHandler,
  AuthorizerQueryService,
} from '../../services';
import { EntityNotFoundError, ForbiddenRequestError } from '../../errors';

export default function chargeOfflineService() {
  return function (target: Record<string, any>, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
    const originalMethod = descriptor.value;

    descriptor.value = async function (...args: any[]) {
      const authorizerQueryService = AuthorizerQueryService.getInstance();
      const req = args[0] as bacenRequest;
      const { id } = req.params;
      const { amount, source, destination, serviceType } = req.body;

      const walletId = source || destination;

      const wallet =
        req.cache.get<Wallet>(`wallet_${walletId}`) ||
        (await Wallet.createQueryBuilder('wallet')
          .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistrations')
          .leftJoinAndSelect('assetRegistrations.asset', 'asset')
          .leftJoinAndSelect('assetRegistrations.states', 'assetRegistrationStates')
          .leftJoinAndSelect('wallet.user', 'user')
          .leftJoinAndSelect('user.domain', 'domain')
          .where('wallet.id = :walletId', { walletId })
          .andWhere('wallet.deletedAt IS NULL')
          .andWhere('user.deletedAt IS NULL')
          .getOne());

      if (!wallet) {
        throw new EntityNotFoundError('Source Wallet');
      }

      async function executeOriginalMethodAndReleaseBlockedBalance() {
        try {
          return await originalMethod.apply(this, args);
        } finally {
          const blockedAmounts = req.cache.get<[Asset, number][]>('blocked_amounts');
          if (blockedAmounts?.length > 0) {
            /*
             * Release blocked balances after request handler.
             * this will not result in double unblocking, if the operation was refused due to insufficient balance an exception
             * would have been thrown and code execution would not have reached this point.
             */
            await Promise.all(
              blockedAmounts.map(([asset, amount]) =>
                authorizerQueryService.updateBlockedBalance({ amount: -amount, asset, wallet }),
              ),
            );
          }
        }
      }

      if (!serviceType) return await executeOriginalMethodAndReleaseBlockedBalance();

      // Prepare feature flag context
      let forceBilling = false;
      const context = { userId: wallet.user.id, properties: { domainId: wallet.user.domain.id } };
      if (wallet.user.role === UserRole.CONSUMER) {
        forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_CONSUMER_BILLING, context, false);
      } else if (wallet.user.role === UserRole.MEDIATOR) {
        forceBilling = UnleashUtil.isEnabled(UnleashFlags.FORCE_MEDIATOR_BILLING, context, false);
      }

      const currentContract = await ContractService.getInstance().getCurrentContract(wallet.user.id);

      if (!forceBilling && !currentContract) {
        return await executeOriginalMethodAndReleaseBlockedBalance();
      }

      if (forceBilling && !currentContract) {
        throw new ForbiddenRequestError(
          `User invoice not found! To perform this operation, the user ${wallet.user.id} must subscribe a plan`,
        );
      }

      const result = await executeOriginalMethodAndReleaseBlockedBalance();

      const providerAsset = await Asset.getByIdOrCode(id.toUpperCase(), { relations: ['issuer'] });
      const rootAsset = await Asset.getRootAsset();

      const entryType = await currentContract.plan.getChargeSettingsByService(
        serviceType as ServiceType,
        providerAsset.provider,
      );

      let feePaymentProof: Transaction;
      if (entryType) {
        const feePaymentAsset = entryType.settings?.liability === 'provider' ? providerAsset : rootAsset;

        const serviceFee = await BillingService.getInstance().getServiceFee({
          serviceType,
          provider: providerAsset.provider,
          transactionAmount: amount,
          asset: feePaymentAsset,
          user: wallet.user,
          contract: currentContract,
        });

        const availableBalance = await wallet.getAvailableBalanceByAsset(feePaymentAsset);
        const hasBalance = serviceFee ? availableBalance >= parseFloat(serviceFee.amount) : false;

        if (serviceFee && parseFloat(serviceFee.amount) !== 0 && currentContract.prepaid && hasBalance) {
          try {
            serviceFee.status =
              entryType.settings.liability === 'provider' ? PaymentStatus.SETTLED : PaymentStatus.AUTHORIZED;

            feePaymentProof = await feePaymentAsset.destroy({
              serviceFee,
              targetWallet: wallet,
              createdBy: req.accessToken,
              additionalData: { serviceConsumptionProof: result.id },
            });
            await TransactionPipelinePublisher.getInstance().send(feePaymentProof);

            await Transaction.update(result.id, {
              additionalData: {
                serviceCharge: feePaymentProof.id,
                ...result.additionalData,
              },
            });
          } catch (error) {
            await IssueHandler.getInstance().handle({
              type: IssueType.SERVICE_CHARGE_PAYMENT_FAILED,
              category: IssueCategory.BILLING,
              severity: SeverityLevel.HIGH,
              componentId: Package.name,
              details: IssueDetails.from(result, error),
              description: `Service fee charge failed: ${error.message}`,
            });
          }
        }
      }

      await EventHandlingGateway.getInstance().process(serviceType, result, {
        feePaymentProof,
        accountable: wallet.user,
        provider: providerAsset.provider,
        operationDate: moment(),
      });

      return result;
    };

    return descriptor;
  };
}
