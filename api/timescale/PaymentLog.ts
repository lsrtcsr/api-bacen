import {
  Column,
  Entity,
  DeepPartial,
  getConnection,
  Between,
  MoreThanOrEqual,
  LessThanOrEqual,
  FindOperator,
} from 'typeorm';
import { PaymentType } from '@bacen/base-sdk';
import { TimescaleEntity } from './base';
import { Payment } from '../models/payment';

export interface PaymentLogFindOptions {
  source?: string;
  destination?: string;
  period?: { start?: Date; end?: Date };
  asset?: string;
}

@Entity(PaymentLog.tableName)
export default class PaymentLog extends TimescaleEntity {
  static readonly tableName = 'payment';

  @Column({ nullable: false })
  transactionHash: string;

  @Column({ nullable: false })
  paymentId: string;

  @Column({ nullable: false })
  asset: string;

  @Column({ nullable: false })
  source: string;

  @Column({ nullable: false, type: 'enum', enum: PaymentType })
  type: PaymentType = PaymentType.TRANSFER;

  @Column({ nullable: false })
  domainId: string;

  @Column({ nullable: false })
  destination: string;

  @Column({ nullable: false })
  amount: string;

  constructor(data: Partial<PaymentLog> = {}) {
    super(data);
  }

  public static async from(payment: Payment, partial: DeepPartial<PaymentLog> = {}): Promise<PaymentLog> {
    const log = getConnection('timescale')
      .getRepository(PaymentLog)
      .create({
        source: payment.transaction.source.id,
        paymentId: payment.id,
        asset: payment.asset.code,
        amount: payment.amount,
        destination: payment.destination.id,
        domainId: payment.destination.user ? payment.destination.user.domain.id : undefined,
        type: payment.type,
        transactionHash: payment.transaction.additionalData.hash,
        ...partial,
      });

    const results = await getConnection('timescale').manager.insert(PaymentLog, log);
    return getConnection('timescale').manager.findOne(PaymentLog, results.identifiers[0].id);
  }

  static mountWhere = (options: PaymentLogFindOptions) => {
    const { source, destination, period, asset } = options;

    const where: { source?: string; destination?: string; asset?: string; time?: FindOperator<Date> } = {};
    if (source) {
      where.source = source;
    }

    if (destination) {
      where.destination = destination;
    }

    if (asset) {
      where.asset = asset;
    }

    if (period?.start && period?.end) {
      where.time = Between(period.start, period.end);
    } else if (period?.start) {
      where.time = MoreThanOrEqual(period.start);
    } else if (period?.end) {
      where.time = LessThanOrEqual(period.end);
    }

    return where;
  };

  public static async getPayments(options: PaymentLogFindOptions) {
    const where = PaymentLog.mountWhere(options);

    return getConnection('timescale')
      .getRepository(PaymentLog)
      .find(where);
  }

  protected static async getPositiveStatements(
    destination: string,
    asset?: string,
  ): Promise<{ destination: string; asset: string; total_amount: number }[]> {
    return getConnection('timescale')
      .getRepository(PaymentLog)
      .createQueryBuilder('payment')
      .select(['destination', 'asset', 'SUM("amount") AS total_amount'])
      .groupBy('destination')
      .groupBy('asset')
      .where({ destination, asset })
      .getRawMany();
  }

  protected static async getNegativeStatements(
    source: string,
    asset?: string,
  ): Promise<{ source: string; asset: string; total_amount: number }[]> {
    return getConnection('timescale')
      .getRepository(PaymentLog)
      .createQueryBuilder('payment')
      .select(['source', 'asset', 'SUM("amount") AS total_amount'])
      .groupBy('source')
      .groupBy('asset')
      .where({ source, asset })
      .getRawMany();
  }

  public static async getBalance(walletId: string, asset?: string): Promise<number[]> {
    const positive = await PaymentLog.getPositiveStatements(walletId, asset);
    const rawNegative = await PaymentLog.getNegativeStatements(walletId, asset);
    const negative = rawNegative.map(asset => ({ ...asset, total_amount: -asset.total_amount }));

    const result = [];

    return [...positive, ...negative].reduce((acc, curr) => {
      if (!acc[curr.asset]) {
        acc[curr.asset] = 0;
      }
      acc[curr.asset] += curr.total_amount;
      return acc;
    }, result);
  }
}
