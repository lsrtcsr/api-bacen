import { BaseEntity, PrimaryColumn } from 'typeorm';

export abstract class TimescaleEntity extends BaseEntity {
  @PrimaryColumn({ default: () => 'CURRENT_TIMESTAMP' })
  time: Date;

  constructor(data: Partial<TimescaleEntity> = {}) {
    super();
    Object.assign(this, data);
  }
}
