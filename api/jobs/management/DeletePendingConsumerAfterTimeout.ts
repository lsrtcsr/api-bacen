import { UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { BaseServer, Job, JobOptions } from 'ts-framework-common';
import { User, ViewConsumerWithStates } from '../../models';

export interface DeletePendingConsumerAfterTimeoutJobOptions extends JobOptions {}

export class DeletePendingConsumerAfterTimeoutJob extends Job {
  constructor(public options: DeletePendingConsumerAfterTimeoutJobOptions) {
    super({ name: 'DeletePendingConsumerAfterTimeoutJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_PENDING_CONSUMER_DELETION)) {
      this.logger.warn(`DeletePendingConsumerAfterTimeoutJob: disabled by feature flag, skip pending deletion`, {
        flag: UnleashFlags.DISABLE_PENDING_CONSUMER_DELETION,
      });
      return;
    }

    this.logger.debug(
      `DeletePendingConsumerAfterTimeoutJob: Seaching for pending consumers with no state changes for a while...`,
      {
        interval: '1 week',
      },
    );

    const results = await ViewConsumerWithStates.findPendingConsumers();

    this.logger.debug(`DeletePendingConsumerAfterTimeoutJob: Pending consumers with no state changes search results`, {
      length: results.length,
    });

    for (const result of results) {
      this.logger.warn(
        `DeletePendingConsumerAfterTimeoutJob: Found pending consumer with no state changes for a while, deleting...`,
        {
          user: result.user_id,
          consumer: result.consumer_id,
          lastConsumerStatus: result.status,
          lastConsumerStateChange: result.status_created_at,
        },
      );
      await User.deleteAccount(result.user_id);
    }
  }
}
