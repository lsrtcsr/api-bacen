import { Job, BaseServer, JobOptions } from 'ts-framework-common';
import {
  WalletStatus,
  ConsumerStatus,
  AssetRegistrationStatus,
  UserRole,
  UserStatus,
  UnleashFlags,
} from '@bacen/base-sdk';
import { getManager, EntityManager } from 'typeorm';
import { UnleashUtil } from '@bacen/shared-sdk';
import { AssetService, ConsumerPipelinePublisher, MediatorPipelinePublisher } from '../../services';
import { Wallet, AssetRegistration, Asset, AssetRegistrationState, User } from '../../models';
import { WalletStateMachine, ConsumerStateMachine, UserStateMachine } from '../../fsm';
import { consumePaginatedQuery } from '../../utils';

const DEFAULT_BATCH_SIZE = 100;

export interface RegisterMissingInitialAssetsJobOptions extends JobOptions {
  userBatchSize?: number;
}

/**
 * Checks if all initialAssets exist in all wallets, otherwise creates a pending AssetRegistration for that
 * asset and wallet, sends the wallet's consumer to `PROCESSING_WALLETS`, sends the wallet to `REGISTERED_IN_STELLAR`,
 * and publishes the consumer on the pipeline to be processed again.
 *
 * Ment to be ran during  -api-workers startup.
 */
export class RegisterMissingInitialAssetsJob extends Job {
  public options: RegisterMissingInitialAssetsJobOptions;

  constructor(options: RegisterMissingInitialAssetsJobOptions) {
    super({ name: 'RegisterMissingInitialAssetsJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    if (UnleashUtil.isEnabled(UnleashFlags.ENABLE_REGISTER_MISSING_ASSETS)) {
      this.logger.info('RegisterMissingInitialAssetsJob: Starting...');
      const users = await this.generateMissingAssets();

      for (const user of users) {
        await this.sendUserToPìpeline(user);
      }
      this.logger.info(`RegisterMissingInitialAssetsJob: exiting`);
    } else {
      this.logger.warn(`RegisterMissingInitialAssetsJob: skipping due to feature flag is not enabled`);
    }
  }

  private async sendUserToPìpeline(user: User): Promise<void> {
    const consumerPipelinePublisher = ConsumerPipelinePublisher.getInstance();
    const mediatorPipelinePublisher = MediatorPipelinePublisher.getInstance();

    if (user.role === UserRole.CONSUMER) {
      const consumerFsm = new ConsumerStateMachine(user);
      await consumerFsm.goTo(ConsumerStatus.PROCESSING_WALLETS);

      await consumerPipelinePublisher.send(user);
      this.logger.info(`Sent user ${user.id} to consumer pipeline.`);
    } else if (user.role === UserRole.MEDIATOR) {
      const userFsm = new UserStateMachine(user);
      await userFsm.goTo(UserStatus.PROCESSING);

      await mediatorPipelinePublisher.send(user);
      this.logger.info(`Sent user ${user.id} to mediator pipeline.`);
    }
  }

  private async generateMissingAssets(manager: EntityManager = getManager()): Promise<User[]> {
    const assetService = AssetService.getInstance();
    const initialAssetsCodes = assetService.requiredAssets.map(asset => asset.code);
    const usersToReprocess = new Set<{
      user: User;
      walletsMissingAssets: Set<{ wallet: Wallet; missingAssetCodes: string[] }>;
    }>();

    const baseUsersQuery = await manager
      .createQueryBuilder(User, 'user')
      .innerJoinAndSelect('user.domain', 'domain')
      .innerJoinAndSelect('user.consumer', 'consumer')
      .innerJoinAndSelect('consumer.states', 'consumerState')
      .innerJoinAndSelect('user.states', 'state')
      .innerJoinAndSelect('user.wallets', 'wallet')
      .innerJoinAndSelect('wallet.states', 'walletStates')
      .leftJoinAndSelect('wallet.assetRegistrations', 'assetRegistration')
      .leftJoinAndSelect('assetRegistration.asset', 'asset');

    for await (const batch of consumePaginatedQuery(baseUsersQuery, this.options.userBatchSize || DEFAULT_BATCH_SIZE)) {
      for (const user of batch) {
        const {
          consumer: { status: consumerState },
        } = user;
        const isConsumerReady =
          consumerState === ConsumerStatus.PROCESSING_PROVIDER_DOCUMENTS ||
          consumerState === ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION ||
          consumerState === ConsumerStatus.READY;

        if (!isConsumerReady) continue;

        const walletsMissingAssets = new Set<{ wallet: Wallet; missingAssetCodes: string[] }>();
        for (const wallet of user.wallets) {
          const { status: walletState } = wallet;
          const walletRegisteredAssetsObj: Record<string, true> = Object.fromEntries(
            wallet.assetRegistrations.map(ar => [ar.asset.code, true]),
          );

          const isWalletRegistered =
            walletState === WalletStatus.PENDING_PROVIDER_APPROVAL ||
            walletState === WalletStatus.READY ||
            walletState === WalletStatus.REGISTERED_IN_PROVIDER ||
            (walletState === WalletStatus.REGISTERED_IN_STELLAR && wallet.assetRegistrations.length > 0);

          const missingAssetCodes = initialAssetsCodes.filter(code => !walletRegisteredAssetsObj[code]);
          if (isWalletRegistered && missingAssetCodes.length) {
            walletsMissingAssets.add({ wallet, missingAssetCodes });
          }
        }

        if (walletsMissingAssets.size > 0) {
          usersToReprocess.add({ user, walletsMissingAssets });
        }
      }
    }

    if (usersToReprocess.size > 0) {
      const walletsCount = Array.from(usersToReprocess).reduce(
        (count, { walletsMissingAssets }) => count + walletsMissingAssets.size,
        0,
      );

      this.logger.info(
        `RegisterMissingInitialAssetsJob: Found ${walletsCount} wallets missing initial assets. Reprocessing...`,
      );

      for (const { user, walletsMissingAssets } of usersToReprocess) {
        for (const { wallet, missingAssetCodes } of walletsMissingAssets) {
          this.logger.info(`reprocessing wallet ${wallet.id} of consumer ${user.id}`);
          const existingAssetCodes = wallet.assetRegistrations.map(ar => ar.asset.code);

          const missingAssets = await manager
            .createQueryBuilder(Asset, 'asset')
            .where('"asset"."code" IN (:...missingAssetCodes)', { missingAssetCodes })
            .getMany();

          await manager.transaction(async tx => {
            const assetRegistrations = await Promise.all(
              missingAssets.map(missingAssetItem =>
                AssetRegistration.insertAndFind({ asset: missingAssetItem, wallet }, { manager }),
              ),
            );

            await Promise.all(
              assetRegistrations.map(ar =>
                manager.insert(AssetRegistrationState, {
                  assetRegistration: ar,
                  status: AssetRegistrationStatus.PENDING,
                }),
              ),
            );
          });

          const walletFsm = new WalletStateMachine(wallet);
          if (wallet.status !== WalletStatus.REGISTERED_IN_STELLAR) {
            await walletFsm.goTo(WalletStatus.REGISTERED_IN_STELLAR);
          }
        }
      }
    } else {
      this.logger.info(`RegisterMissingInitialAssetsJob: No wallets missing assets`);
    }

    return Array.from(usersToReprocess).map(userToReprocess => userToReprocess.user);
  }
}
