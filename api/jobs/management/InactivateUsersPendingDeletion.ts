import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import { ConsumerState, ConsumerStatus, UserStatus } from '@bacen/base-sdk';
import { User, UserState } from '../../models';

export interface InactivateUsersPendingDeletionJobOptions extends JobOptions {}

/**
 * Queries the database for consumers in state PENDING_DELETION but whose user is not in state
 * INACTIVE and inactivates them in a single INSERT query.
 */
export class InactivateUsersPendingDeletionJob extends Job {
  constructor(options: InactivateUsersPendingDeletionJobOptions) {
    super({ ...options, name: 'InactivateUsersPendingDeletionJob' });
  }

  public describe() {
    return {
      name: this.options.name,
    };
  }

  public async run(): Promise<void> {
    this.logger.debug(`InactivateUsersPendingDeletionJob: Starting`);

    const activeUsersPendingDeletion = await User.createQueryBuilder('user')
      .leftJoinAndSelect('user.states', 'states')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumer_states')
      .leftJoin(
        qb =>
          qb
            .from(ConsumerState, 'consumerState')
            .select('"consumerId"')
            .addSelect('MAX(created_at)', 'created_at')
            .groupBy('"consumerId"'),
        'last_consumer_state',
        'last_consumer_state.created_at = consumer_states.created_at AND last_consumer_state."consumerId" = consumer.id',
      )
      .leftJoin(
        qb =>
          qb
            .from(UserState, 'userState')
            .select('"userId"')
            .addSelect('MAX(created_at)', 'created_at')
            .groupBy('"userId"'),
        'last_user_state',
        'last_user_state.created_at = states.created_at AND last_user_state."userId" = user.id',
      )
      .where('consumer_states.status = :consumerStatus', { consumerStatus: ConsumerStatus.PENDING_DELETION })
      .andWhere('states.status != :userStatus', { userStatus: UserStatus.INACTIVE })
      .getMany();

    if (activeUsersPendingDeletion.length > 0) {
      const insertResult = await UserState.insert(
        activeUsersPendingDeletion.map(user =>
          UserState.create({
            user,
            status: UserStatus.INACTIVE,
          }),
        ),
      );

      this.logger.info(`Inactivated ${insertResult.identifiers.length} users. Exiting`, {
        ids: insertResult.identifiers.map(returnedId => returnedId.id),
      });
    } else {
      this.logger.info(`InactivateUsersPendingDeletionJob: No active users pending deletion found. Exiting.`);
    }
  }
}
