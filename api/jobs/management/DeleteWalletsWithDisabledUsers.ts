import { Job, BaseServer, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import { Wallet } from '../../models';
import { consumePaginatedQuery } from '../../utils';

/**
 * Soft-deletes all wallets whose user is soft-deleted but they themselves
 * are not deleted.
 *
 * batches wallets to avoid resource blow up
 */
export class DeleteWalletsWithDisabledUsersJob extends Job {
  private static readonly pageSize = 1_000;

  constructor(options: JobOptions) {
    super({ ...options, name: 'DeleteWalletsWithDisabledUsersJob' });
  }

  public describe() {
    return { name: this.options.name };
  }

  async run(server: BaseServer): Promise<void> {
    const activeWalletsWithDisabledUsersQuery = Wallet.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.user', 'user')
      .where('wallet.deletedAt IS NULL')
      .andWhere('user.deletedAt IS NOT NULL');

    const activeWalletsWithDisabledUsersCount = await activeWalletsWithDisabledUsersQuery.getCount();

    if (activeWalletsWithDisabledUsersCount > 0) {
      this.logger.info('DeleteWalletsWithDisabledUsersJob: Found active wallets with disabled users.', {
        count: activeWalletsWithDisabledUsersCount,
      });

      let walletIdx = 0;
      for await (const walletBatch of consumePaginatedQuery(
        activeWalletsWithDisabledUsersQuery,
        DeleteWalletsWithDisabledUsersJob.pageSize,
      )) {
        const batchStart = walletIdx + 1;
        const batchEnd = Math.min(
          walletIdx + DeleteWalletsWithDisabledUsersJob.pageSize,
          activeWalletsWithDisabledUsersCount,
        );
        this.logger.debug(`processing wallets ${batchStart}...${batchEnd} of ${activeWalletsWithDisabledUsersCount}`);

        const walletIds = walletBatch.map(wallet => wallet.id);
        await Wallet.update(walletIds, { deletedAt: new Date() });
        walletIdx += DeleteWalletsWithDisabledUsersJob.pageSize;
      }
    } else {
      this.logger.info('DeleteWalletsWithDisabledUsersJob: No active wallets with disabled users.');
    }
    this.logger.info('DeleteWalletsWithDisabledUsersJob: exiting...');
  }
}
