import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import * as moment from 'moment';
import { Period, PeriodStatus } from '../../models';
import { PeriodPipelinePublisher } from '../../services';

export interface InvoicePeriodClosingJobOptions extends JobOptions {}

export class InvoicePeriodClosingJob extends Job {
  constructor(public options: InvoicePeriodClosingJobOptions) {
    super({ name: 'InvoicePeriodClosingJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    this.logger.debug(`InvoicePeriodClosingJob: Seaching for periods whose scheduled closing date was exceeded....`);
    const now = moment();
    const allActivePeriods: Period[] = await Period.safeFind({
      where: { current: true },
      relations: ['states', 'invoice', 'invoice.contractor'],
    });

    const target = allActivePeriods.filter(
      period =>
        !period.invoice.contractor.deletedAt &&
        period.status === PeriodStatus.ACTIVE &&
        period.closureScheduledFor.isSameOrBefore(now) &&
        period.invoice.closureScheduledFor.isAfter(now),
    );

    const periodPipelinePublisher = PeriodPipelinePublisher.getInstance();
    target.forEach(async period => await periodPipelinePublisher.send(period));
    this.logger.debug(`InvoicePeriodClosingJob: All ${target.length} periods whose
     scheduled closing date was exceeded were sent to the pipeline, everything ok!`);
  }
}
