import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import * as moment from 'moment';
import { InvoiceStatus, Invoice } from '../../models';
import { InvoicePipelinePublisher } from '../../services';

export interface InvoiceClosingJobOptions extends JobOptions {}

export class InvoiceClosingJob extends Job {
  constructor(public options: InvoiceClosingJobOptions) {
    super({ name: 'InvoiceClosingJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    this.logger.debug(`InvoiceClosingJob: Seaching for invoices whose scheduled closing date was exceeded....`);
    const allActiveInvoices: Invoice[] = await Invoice.safeFind({
      where: { current: true },
      relations: ['contractor', 'states'],
    });

    const now = moment();
    const target = allActiveInvoices.filter(
      invoice =>
        !invoice.contractor.deletedAt &&
        invoice.closureScheduledFor.isSameOrBefore(now) &&
        invoice.status === InvoiceStatus.ACTIVE,
    );

    const invoicePipelinePublisher = InvoicePipelinePublisher.getInstance();
    target.forEach(async invoice => await invoicePipelinePublisher.send(invoice));
    this.logger.debug(`InvoiceClosingJob: All ${target.length} invoices whose
     scheduled closing date was exceeded were sent to the pipeline, everything ok!`);
  }
}
