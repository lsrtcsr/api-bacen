import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import { getManager } from 'typeorm';
import * as moment from 'moment';
import { EntryType } from '../../models';

export interface InvoiceEntryTypeManagementJobOptions extends JobOptions {}

export class InvoiceEntryTypeManagementJob extends Job {
  constructor(public options: InvoiceEntryTypeManagementJobOptions) {
    super({ name: 'InvoiceEntryTypeManagementJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    this.logger.debug(`InvoiceEntryTypeManagementJob: Invalidating expired entry types....`);
    await getManager().transaction(async transaction => {
      const validItems = await transaction.find(EntryType, {
        where: { valid: true },
      });
      const expiredItems = validItems.filter(item => item.validUntil && item.validUntil.isSameOrBefore(moment()));

      expiredItems.forEach(async expiredItem => await expiredItem.invalidate(transaction));
    });
    this.logger.debug(`InvoiceEntryTypeManagementJob: All expired entry types invalidated, everything ok!`);
  }
}
