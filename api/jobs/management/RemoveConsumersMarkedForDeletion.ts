import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import { ConsumerStatus, UnleashFlags } from '@bacen/base-sdk';
import { UnleashUtil } from '@bacen/shared-sdk';
import { User, ConsumerState } from '../../models';
import { ConsumerStateMachine } from '../../fsm';

export interface RemoveConsumersMarkedForDeletionJobOptions extends JobOptions {
  markedForDeletionStates: ConsumerStatus[];
}

/**
 * Queries the database for consumers in state PENDING_DELETION whose `deleteBy` has already expired
 * and sends them to status DELETED one by one.
 */
export class RemoveConsumersMarkedForDeletionJob extends Job {
  private readonly markedForDeletionStates: ConsumerStatus[];

  constructor({ markedForDeletionStates, ...options }: RemoveConsumersMarkedForDeletionJobOptions) {
    super({ ...options, name: 'RemoveConsumersMarkedForDeletionJob' });
    this.markedForDeletionStates = markedForDeletionStates;
  }

  public describe() {
    return {
      name: this.options.name,
    };
  }

  public async run(): Promise<void> {
    if (UnleashUtil.isEnabled(UnleashFlags.DISABLE_PENDING_CONSUMER_DELETION)) {
      this.logger.warn(`RemoveConsumersMarkedForDeletionJob: disabled by feature flag, skip pending deletion`, {
        flag: UnleashFlags.DISABLE_PENDING_CONSUMER_DELETION,
      });
      return;
    }

    this.logger.debug(`RemoveConsumersMarkedForDeletionJob: Starting...`);

    const usersMarkedForDeletion = await User.createQueryBuilder('user')
      .leftJoinAndSelect('user.states', 'states')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumer_states')
      .leftJoinAndSelect('user.wallets', 'wallets')
      .leftJoin(
        qb =>
          qb
            .from(ConsumerState, 'consumer_state')
            .select('"consumerId"', 'consumerId')
            .addSelect('MAX(created_at)', 'created_at')
            .groupBy('"consumerId"'),
        'last_consumer_state',
        'last_consumer_state."consumerId" = consumer.id AND last_consumer_state.created_at=consumer_states.created_at',
      )
      .where('consumer_states.status = :...status', { status: this.markedForDeletionStates })
      .andWhere(
        /*
         * In order to avoid issues with the time we run this job, we use DATE_TRUNC to ensure that all consumers being
         * deleted on the current job have expired up to the day prior.
         */
        `DATE_TRUNC('day', (consumer_states."additionalData"->>'deleteBy')::timestamp) < DATE_TRUNC('day', now())`,
      )
      .getMany();

    if (usersMarkedForDeletion.length > 0) {
      this.logger.info(
        `RemoveConsumersMarkedForDeletionJob: Found ${usersMarkedForDeletion.length} consumers marked for deletion.`,
      );

      for (const user of usersMarkedForDeletion) {
        this.logger.info(`RemoveConsumersMarkedForDeletionJob: Deleting user`, {
          userId: user.id,
          reason: user.getStates()![0].additionalData.reason,
          deleteBy: user.getStates()![0].additionalData.deleteBy,
        });

        try {
          const fsm = new ConsumerStateMachine(user);
          const success = await fsm.goTo(ConsumerStatus.DELETED);

          if (!success) {
            this.logger.error(`RemoveConsumersMarkedForDeletionJob: User deletion failed silently`, {
              userId: user.id,
            });
          } else {
            this.logger.info(`RemoveConsumersMarkedForDeletionJob: User deleted successfully`, {
              userId: user.id,
            });
          }
        } catch (err) {
          this.logger.error(`RemoveConsumersMarkedForDeletionJob: User deletion failed`, {
            userId: user.id,
            error: err.toJSON ? err.toJSON() : err.message ? err.message : err,
          });
        }
      }

      this.logger.debug(`RemoveConsumersMarkedForDeletionJob: Finished.`);
    } else {
      this.logger.info(`RemoveConsumersMarkedForDeletionJob: No consumers marked for deletion. Continuing...`);
    }
  }
}
