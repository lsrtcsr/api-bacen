import { Job, JobOptions, BaseServer } from 'ts-framework-common';
import { LessThanOrEqual } from 'typeorm';
import { SeverityLevel, IssueType, IssueCategory, IssueDetails } from '@bacen/base-sdk';
import * as Package from 'pjson';
import * as moment from 'moment';
import { Contract, ContractStatus, ContractState } from '../../models';
import { ContractStateMachine } from '../../fsm';
import { IssueHandler } from '../../services';

export interface FinishExpiredSubscriptionsJobOptions extends JobOptions {}

export class FinishExpiredSubscriptionsJob extends Job {
  constructor(public options: FinishExpiredSubscriptionsJobOptions) {
    super({ name: 'FinishExpiredSubscriptionsJob', ...options });
  }

  public async run(server: BaseServer): Promise<void> {
    this.logger.info(`FinishExpiredSubscriptionsJob: Seaching for expired subscriptions....`);
    const now = new Date();
    const expiredSubscriptions: Contract[] = await Contract.createQueryBuilder('contract')
      .leftJoinAndSelect('contract.states', 'states')
      .where('contract.current = :current', { current: true })
      .andWhere('contract.valid_until IS NOT NULL')
      .andWhere('contract.valid_until < :now', { now })
      .getMany();

    for (const expiredSubscription of expiredSubscriptions) {
      try {
        const currentState = expiredSubscription.getStates()[0];
        await ContractState.update(currentState.id, {
          additionalData: {
            stateTransitionRequest: {
              force: false,
              to: ContractStatus.EXPIRED,
              at: new Date(),
              by: 'FinishExpiredSubscriptionsJob',
            },
          } as any,
        });

        const contractFSM = new ContractStateMachine(expiredSubscription);
        await contractFSM.goTo(ContractStatus.EXPIRED);

        this.logger.info(`FinishExpiredSubscriptionsJob: expired subscription ${expiredSubscription.id} was closed`);
      } catch (exception) {
        await IssueHandler.getInstance().handle({
          type: IssueType.FSM_ACTION_EXECUTION_FAILED,
          category: IssueCategory.BILLING,
          severity: SeverityLevel.HIGH,
          componentId: Package.name,
          details: IssueDetails.from(expiredSubscription, exception),
          description: 'got error expiring subscription',
        });
      }
    }
  }
}
