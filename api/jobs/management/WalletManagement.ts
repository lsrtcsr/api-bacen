import { IssueCategory, IssueDetails, IssueType, SeverityLevel } from '@bacen/base-sdk';
import { StellarService } from '@bacen/stellar-service';
import * as Package from 'pjson';
import * as Stellar from 'stellar-sdk';
import { BaseServer, Job, JobOptions } from 'ts-framework-common';
import { Wallet } from '../../models';
import { IssueHandler } from '../../services';

// TODO: Move to config
export const MIN_BALANCE = 100;

export interface WalletManagementJobOptions extends JobOptions {
  channelAccounts: { secretSeed: string }[];
}

export class WalletManagementJob extends Job {
  private channelAccounts: WalletManagementJobOptions['channelAccounts'];

  constructor(public options: WalletManagementJobOptions) {
    super({ name: 'WalletManagementJob', ...options });
    this.channelAccounts = options.channelAccounts;
  }

  public describe() {
    return {
      name: 'WalletManagementJob',
    };
  }

  public async run(server: BaseServer): Promise<void> {
    this.logger.debug(`WalletManagementJob: Checking channel accounts balances in Lumens....`);
    if (this.channelAccounts) {
      for (const channelAccount of this.channelAccounts) {
        const channelKeypair = Stellar.Keypair.fromSecret(channelAccount.secretSeed);
        const account = await StellarService.getInstance().loadAccount(channelKeypair.publicKey());

        for (const balance of account.balances) {
          if (+balance.balance <= MIN_BALANCE && balance.asset_type === 'native') {
            await IssueHandler.getInstance().handle({
              type: IssueType.FSM_ACTION_EXECUTION_FAILED,
              category: IssueCategory.PAYMENT,
              severity: SeverityLevel.NORMAL,
              componentId: Package.name,
              details: new IssueDetails({
                resourceType: Wallet.constructor.name,
                resourceId: channelKeypair.publicKey().toString(),
                resourceSnapshot: balance.toString(),
              }),
            });
          }
        }
      }
    }
    this.logger.debug(`WalletManagementJob: All channels beyond critical balance, everything ok!`);
  }
}
