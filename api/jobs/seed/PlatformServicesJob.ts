import { Job, JobOptions } from 'ts-framework-common';
import Server from 'ts-framework';
import { getManager } from 'typeorm';
import { ServiceSchema } from '@bacen/base-sdk';
import { Service } from '../../models';

export interface PlatformServicesJobOptions extends JobOptions {
  platformServicesSettings: ServiceSchema[];
}

export class PlatformServicesJob extends Job {
  public readonly options: PlatformServicesJobOptions;

  constructor(options: PlatformServicesJobOptions) {
    super({ name: 'defaultPlatformServicesJob', ...options });
  }

  public async run(server: Server) {
    const alreadyRegistered = (await Service.count()) > 0;

    // if platform services already exist we have nothing to do here
    if (alreadyRegistered) {
      this.logger.debug('defaultPlatformServicesJob: nothing to do, exiting...');
    } else {
      this.logger.debug('defaultLegalTermsJob: Database is empty, creating default LegalTerms...');
      const services = this.options.platformServicesSettings.map(service =>
        Service.create({
          type: service.type,
          name: service.name,
          description: service.description,
          provider: service.provider,
        }),
      );

      await getManager().transaction(async tx => {
        await tx.insert(Service, services);
      });
      this.logger.info(`An initial set of platform services was generated for billing purpose`);

      this.logger.debug('defaultPlatformServicesJob: Default platform services created successfully');
    }
  }
}
