import { Job, JobOptions } from 'ts-framework-common';
import Server from 'ts-framework';
import { PlanStatus, DomainRole, EntryClazz } from '@bacen/base-sdk';
import * as moment from 'moment';
import { Service, Plan, Domain } from '../../models';
import Config from '../../../config';
import { PlanService } from '../../services';
import { PlanServiceSchema } from '../../schemas';

export interface DefaultBillingPlanJobOptions extends JobOptions {
  supplier: DomainRole;
}

export class DefaultBillingPlanJob extends Job {
  public readonly options: DefaultBillingPlanJobOptions;

  constructor(options: DefaultBillingPlanJobOptions) {
    super({ name: 'defaultBillingPlanJob', ...options });
  }

  public async run(server: Server) {
    const supplier = await Domain.findOne({ where: { role: this.options.supplier } });
    const alreadyExists = await Plan.safeFindOne({ where: { supplier, default: true, status: PlanStatus.AVAILABLE } });

    // if domain default plan already exist we have nothing to do here
    if (alreadyExists) {
      this.logger.debug('defaultBillingPlanJob: nothing to do, exiting...');
    } else {
      this.logger.debug('defaultBillingPlanJob: Database is empty, creating default Billing Plan...');

      const platformServices = await Service.find();
      const billingTriggers = ['invoice_closing', 'transaction'];

      let items: PlanServiceSchema[];
      for (const billingTrigger of billingTriggers) {
        items = Config.seed.platformServices
          .filter(service => service.billingTrigger === billingTrigger)
          .map(service => {
            const platformService = service.provider
              ? platformServices.find(s => s.type === service.type && s.provider === service.provider)
              : platformServices.find(s => s.type === service.type);

            return {
              billingTrigger,
              serviceId: platformService.id,
              entryClazz: EntryClazz.SERVICE_CHARGE,
              name: platformService.name,
              price: 0,
            } as PlanServiceSchema;
          });
      }

      const planSettings = Config.seed.defaultPlansSettings.find(plan => plan.supplier === this.options.supplier);

      await PlanService.getInstance({ logger: this.options.logger }).createPlan({
        supplier,
        planData: {
          items,
          name: planSettings.name,
          billingSettings: planSettings.billingSettings,
          validFrom: moment(),
          default: true,
        },
      });

      const subscriberType = this.options.supplier === DomainRole.ROOT ? 'mediator' : 'consumer';
      this.logger.info(`A default ${subscriberType} billing plan was generated`);

      this.logger.debug('defaultBillingPlanJob: Default billing plan created successfully');
    }
  }
}
