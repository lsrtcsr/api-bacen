import { OAuthClientSchema } from '@bacen/base-sdk';
import { EnvConfigStorage, KubeConfigTemplate, NanoConfig, YamlConfigStorage } from 'nano-config';
import * as path from 'path';
import { Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import * as uuid from 'uuid';
import MainServer from '../../MainServer';
import { OAuthClient } from '../../models';

export interface OAuthClientJobOptions extends JobOptions {
  k8s?: boolean;
  clients: Partial<OAuthClientSchema>[];
}

export class OAuthClientJob extends Job {
  public options: OAuthClientJobOptions;

  constructor(options: OAuthClientJobOptions) {
    super({ name: 'OAuthClientJob', ...options });
  }

  /**
   * Create the initial OAuth 2.0 client instances.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    const clientsCount = await OAuthClient.find({});
    let models = this.options.clients.map(
      model =>
        new OAuthClient({
          ...model,
          clientSecret: model.clientSecret || uuid.v4(),
        } as OAuthClient),
    );

    models = models.filter(model => clientsCount.filter(client => client.clientId === model.clientId).length === 0);

    if (models.length > 0) {
      await getManager().transaction(async transactionEntityManager => {
        // Insert all in a single operation
        await transactionEntityManager.insert(
          OAuthClient,
          models.map(m => OAuthClient.create(m)),
        );

        if (this.options.k8s) {
          const name = `${process.env.K8S_PREFIX || ' '}-oauth-secrets`;
          const basePath = path.join(__dirname, '../../../.k8s');

          const config = new KubeConfigTemplate<any>({
            name,
            basePath,
            kind: 'Secret',
            storage: new YamlConfigStorage({ name, basePath }),
            schema: models
              .map(model => ({ [model.clientId]: { format: String, default: '' } }))
              .reduce((aggr, next) => ({ ...aggr, ...next }), {}),
          });

          for (const model of models) {
            config.set(model.clientId, model.clientSecret);
          }

          // Dump file and continue
          await config.dump();
        } else {
          const { PROVIDER_ENV_PATH: providerEnvPath = '../../../../../.bacen' } = process.env;
          let basePath = path.resolve(__dirname, providerEnvPath);

          if (
            path
              .dirname(basePath)
              .split(path.sep)
              .pop() === ' '
          ) {
            basePath = path.resolve(__dirname, '../', providerEnvPath);
          }

          // Write an env file for each oauth client
          for (const model of models) {
            const name = model.clientId;
            const config = new NanoConfig({
              name,
              basePath,
              storage: new EnvConfigStorage({ name, basePath }),
              schema: {
                bacen_CLIENT_ID: { format: String, default: '' },
                bacen_CLIENT_SECRET: { format: String, default: '' },
              },
            });

            config.set('bacen_CLIENT_ID', model.clientId);
            config.set('bacen_CLIENT_SECRET', model.clientSecret);

            // Dump file and continue
            await config.dump();
            this.logger.debug(`OAuth Client Job created development env file for "${model.clientId}"`);
          }
        }

        // Complete log for verbose purposes
        this.logger.info(
          `${'\n-------------------------------------------------------------------------\n' +
            '                                                                           \n' +
            '              NOTICE: The initial OAuth Clients were generated             \n' +
            '                                                                           \n' +
            '                                                                           \n'}${models
            .map(
              model =>
                '                                                                           \n' +
                `        Client ID:       ${model.clientId}                                 \n` +
                `        Client Secret:   ${model.clientSecret}                             \n` +
                '                                                                           \n',
            )
            .join('')}                                                                          \n` +
            `\n------------------------------------------------------------------------\n`,
        );
      });
    }
  }
}
