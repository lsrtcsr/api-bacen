import { UserRole, UserSchema, UserStatus, WalletStatus, AssetRegistrationStatus } from '@bacen/base-sdk';
import { EnvConfigStorage, NanoConfig } from 'nano-config';
import { dirname, resolve, sep } from 'path';
import { Keypair } from 'stellar-sdk';
import { Job, JobOptions } from 'ts-framework-common';
import { DeepPartial, getManager } from 'typeorm';
import Config from '../../../config';
import MainServer from '../../MainServer';
import {
  Asset,
  Domain,
  User,
  UserState,
  Wallet,
  WalletState,
  AssetRegistration,
  AssetRegistrationState,
} from '../../models';
import { AssetService } from '../../services';

export interface RootAdminJobOptions extends JobOptions {
  rootAdmin: DeepPartial<UserSchema> & { password?: string };
}

export class RootAdminJob extends Job {
  public options: RootAdminJobOptions;

  constructor(options: RootAdminJobOptions) {
    super({ name: 'RootAdminJob', ...options });
  }

  /**
   * Create the initial OAuth 2.0 root user instance and domain.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    let rootAdmin: User;
    let rootAsset: Asset;
    let rootStellarWallet: Wallet;

    const adminUsersCount = await User.safeCount({ where: { role: UserRole.ADMIN } });

    if (!adminUsersCount) {
      // Create the root admin user
      this.logger.debug(`RootAdminJob: Creating root admin...`);
      const assetService = AssetService.getInstance();

      await getManager().transaction(async transactionalEntityManager => {
        const rootDomain = await Domain.getRootDomain({ manager: transactionalEntityManager });

        const rootAdminSchema = User.create({
          ...(this.options.rootAdmin as any),
          domain: rootDomain,
          role: UserRole.ADMIN,
        });
        await rootAdminSchema.setPassword(this.options.rootAdmin.password);
        const rootAdminResult = await transactionalEntityManager.insert(User, rootAdminSchema);
        const now = Date.now();

        // Insert admin active state
        await transactionalEntityManager.insert(UserState, [
          UserState.create({
            user: { id: rootAdminResult.identifiers[0].id },
            status: UserStatus.PENDING,
            createdAt: new Date(now),
            updatedAt: new Date(now),
          }),
          UserState.create({
            user: { id: rootAdminResult.identifiers[0].id },
            status: UserStatus.PROCESSING,
            createdAt: new Date(now + 1),
            updatedAt: new Date(now + 1),
          }),
          UserState.create({
            user: { id: rootAdminResult.identifiers[0].id },
            status: UserStatus.ACTIVE,
            createdAt: new Date(now + 2),
            updatedAt: new Date(now + 2),
          }),
        ]);

        // Create the root admin wallets
        rootAdmin = await transactionalEntityManager.findOne(User, rootAdminResult.identifiers[0].id, {
          relations: ['states'],
        });
        this.logger.debug(`RootAdminJob: Creating root admin wallet...`);

        // TODO: Ensure wallet exists in network before continuing
        const rootKeyPair = Keypair.fromSecret(Config.stellar.rootWallet.secretKey);
        rootStellarWallet = await Wallet.insertAndFind(
          {
            user: { id: rootAdminResult.identifiers[0].id },
            stellar: {
              publicKey: rootKeyPair.publicKey(),
              secretKey: rootKeyPair.secret(),
            },
          },
          { manager: transactionalEntityManager },
        );

        await transactionalEntityManager.insert(WalletState, [
          WalletState.create({ wallet: { id: rootStellarWallet.id }, status: WalletStatus.PENDING }),
          WalletState.create({ wallet: { id: rootStellarWallet.id }, status: WalletStatus.REGISTERED_IN_STELLAR }),
        ]);

        // Create the root asset in the root admin wallet
        rootAsset = await Asset.insertAndFind(
          {
            issuer: rootStellarWallet,
            ...assetService.rootAsset,
          },
          { manager: transactionalEntityManager },
        );

        const assetRegistration = await AssetRegistration.insertAndFind(
          { asset: rootAsset, wallet: rootStellarWallet },
          { manager: transactionalEntityManager },
        );

        await transactionalEntityManager.insert(AssetRegistrationState, {
          assetRegistration,
          status: AssetRegistrationStatus.READY,
        });

        // Complete log for verbose purposes
        this.logger.info(
          '\n------------------------------------------------------------------------\n' +
            '                                                                          \n' +
            '              NOTICE: A root user was generated                           \n' +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  ADMIN                                                             \n' +
            '                                                                          \n' +
            `        ID:            ${rootAdmin.id}                                    \n` +
            `        Name:          ${rootAdmin.name}                                  \n` +
            `        Email:         ${rootAdmin.email}                                 \n` +
            `        Password:      ${this.options.rootAdmin.password}                 \n` +
            `        Status:        ${rootAdmin.status}                                \n` +
            `        Admin Wallet:  ${rootStellarWallet.id}                            \n` +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  ROOT ASSET                                                        \n' +
            '                                                                          \n' +
            `        ID:         ${rootAsset.id}                                       \n` +
            `        Name:       ${rootAsset.name}                                     \n` +
            `        Code:       ${rootAsset.code}                                     \n` +
            '                                                                          \n' +
            '                                                                          \n' +
            '\n------------------------------------------------------------------------\n',
        );

        // Save file locally for development purposes
        if (process.env.NODE_EN !== 'production') {
          const name = 'root-admin';
          const { PROVIDER_ENV_PATH: providerEnvPath = '../../../../../.bacen' } = process.env;
          let basePath = resolve(__dirname, providerEnvPath);

          if (
            dirname(basePath)
              .split(sep)
              .pop() === ' '
          ) {
            basePath = resolve(__dirname, '../', providerEnvPath);
          }

          const config = new NanoConfig({
            basePath,
            name,
            storage: new EnvConfigStorage({ name, basePath }),
            schema: {
              bacen_ADMIN_EMAIL: { format: String, default: rootAdmin.email },
              bacen_ADMIN_PASSWORD: { format: String, default: this.options.rootAdmin.password },
            },
          });

          await config.dump();
        }
      });
    }
  }
}
