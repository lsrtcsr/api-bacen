import { BaseError, Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import { AssetRegistrationStatus } from '@bacen/base-sdk';
import MainServer from '../../MainServer';
import { Asset, Wallet, AssetRegistration, AssetRegistrationState } from '../../models';
import { AssetService } from '../../services';

export interface OtherAssetsHeldInCustodyJobOptions extends JobOptions {}

export class OtherAssetsHeldInCustodyJob extends Job {
  public options: OtherAssetsHeldInCustodyJobOptions;

  constructor(options: OtherAssetsHeldInCustodyJobOptions) {
    super({ name: 'OtherAssetsHeldInCustodyJob', ...options });
  }

  /**
   * Create the initial OAuth 2.0 root user instance and domain.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    const rootWallet = await Wallet.getRootWallet();

    if (!rootWallet) {
      throw new BaseError('Could not find Root Walet for asset creations, check your Stellar ENVs');
    }

    await getManager().transaction(async manager => {
      const assetService = AssetService.getInstance();
      this.logger.debug(`OtherAssetsHeldInCustodyJob: starting job to synchronize assets in database`, {
        length: assetService.otherAssetsInCustody.length,
        codes: assetService.otherAssetsInCustody.map(asset => asset.code),
      });

      for (const assetData of assetService.otherAssetsInCustody) {
        if (assetData.code === assetService.rootAsset.code) continue;

        const count = await Asset.safeCount({ where: { code: assetData.code } });
        if (count > 0) continue;

        this.logger.info(`OtherAssetsHeldInCustodyJob: Creating asset ${assetData.code}`, {
          name: assetData.name,
          code: assetData.code,
          provider: assetData.provider,
          authorizable: assetData.authorizable,
        });

        // Create the asset in the root admin wallet
        const asset = await Asset.insertAndFind(
          {
            issuer: rootWallet,
            code: assetData.code,
            name: assetData.name,
            provider: assetData.provider,
          },
          { manager },
        );

        const assetRegistration = await AssetRegistration.insertAndFind({ asset, wallet: rootWallet }, { manager });

        await manager.insert(AssetRegistrationState, { assetRegistration, status: AssetRegistrationStatus.READY });

        // Complete log for verbose purposes
        this.logger.info(
          '\n--------------------------------------------------------------------------\n' +
            '                                                                          \n' +
            '              NOTICE: An asset was generated                              \n' +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  ASSET                                                             \n' +
            '                                                                          \n' +
            `        ID:         ${asset.id}                                           \n` +
            `        Name:       ${asset.name}                                         \n` +
            `        Code:       ${asset.code}                                         \n` +
            '                                                                          \n' +
            '                                                                          \n' +
            '\n------------------------------------------------------------------------\n',
        );
      }
    });

    this.logger.info('OtherAssetsHeldInCustodyJob: exiting...');
  }
}
