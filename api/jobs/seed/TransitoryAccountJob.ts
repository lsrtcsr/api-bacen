import { TransitoryAccountType } from '@bacen/base-sdk';
import { BaseError, Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import MainServer from '../../MainServer';
import { User, Wallet } from '../../models';
import { WalletService } from '../../services';
import { getDefaultFingerprint } from '../../utils';

export interface TransitoryAccountJobOptions extends JobOptions {}

export class TransitoryAccountJob extends Job {
  public options: TransitoryAccountJobOptions;

  constructor(options: TransitoryAccountJobOptions) {
    super({ name: 'TransitoryAccountJob', ...options });
  }

  /**
   * Creates the transitory account wallet.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    // TODO: This should come from a config file
    const initialTransitoryAccountTypes = [
      TransitoryAccountType.CARD_TRANSACTION,
      TransitoryAccountType.SERVICE_FEE,
      TransitoryAccountType.BOLETO_PAYMENT,
    ];

    for (const type of initialTransitoryAccountTypes) {
      // If acc. already exists there is nothing we need to do.
      const transitoryAccountExists = await Wallet.getRootTransitoryAccountWallet(type);

      if (!transitoryAccountExists) {
        const createdTransitoryAcc = await getManager().transaction(async (tx) => {
          const walletService = WalletService.getInstance();

          const rootMediator = await User.getRootMediator(tx);
          if (!rootMediator) throw new BaseError('Root mediator does not exist, exiting...');

          const transitoryAccWallet = await walletService.createRootMediatorInitialWallet({
            manager: tx,
            user: rootMediator,
            walletAdditionalData: {
              isTransitoryAccount: true,
              transitoryAccountType: type,
            },
            stateAdditionalData: {
              acceptProviderLegalTerms: true,
              source: 'TransitoryAccountJob',
              fingerprint: getDefaultFingerprint(),
            },
          });

          return transitoryAccWallet;
        });

        this.logger.info(
          '\n------------------------------------------------------------------------\n' +
            '                                                                          \n' +
            ' NOTICE: A transitory account wallet was generated for the root mediator  \n' +
            '                                                                          \n' +
            '                                                                          \n' +
            `        Name:              ${createdTransitoryAcc.user!.name}             \n` +
            `        Email:             ${createdTransitoryAcc.user!.email}            \n` +
            `        Type:              ${type}                                        \n` +
            `        Mediator Wallet:   ${createdTransitoryAcc.id}                     \n` +
            '                                                                          \n' +
            '\n------------------------------------------------------------------------\n',
        );
      } else {
        this.logger.debug('Transitory account wallet already exists, no actions needed.');
      }
    }
  }
}
