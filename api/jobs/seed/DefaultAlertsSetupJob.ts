import { Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import MainServer from '../../MainServer';
import { Alert } from '../../models';

export interface DefaultAlertsSetupJobOptions extends JobOptions {
  defaultAlerts: Partial<Alert>[];
}

export class DefaultAlertsSetupJob extends Job {
  public options: DefaultAlertsSetupJobOptions;

  constructor(options: DefaultAlertsSetupJobOptions) {
    super({ name: 'DefaultAlertsSetupJob', ...options });
  }

  /**
   * Create the initial Alerts
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    const alertsCount = await Alert.safeCount();

    if (!alertsCount) {
      this.logger.debug(`DefaultAlertsSetupJob: Database is empty, creating default alerts...`);

      const transientAlerts = this.options.defaultAlerts.map(alertSettings => Alert.create(alertSettings));

      await getManager().transaction(async transaction => {
        this.logger.debug(`DefaultAlertsSetupJob: Creating default alerts...`);
        await transaction.insert(Alert, transientAlerts);
      });
      this.logger.info(`DefaultAlertsSetupJob: Default alerts created successfuly...`);
    }
  }
}
