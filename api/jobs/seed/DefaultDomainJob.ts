import { DomainRole, DomainSchema } from '@bacen/base-sdk';
import { Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import MainServer from '../../MainServer';
import { Domain, DomainSettings } from '../../models';

export interface DefaultDomainJobOptions extends JobOptions {
  defaultDomain: DomainSchema;
}

export class DefaultDomainJob extends Job {
  public options: DefaultDomainJobOptions;

  constructor(options: DefaultDomainJobOptions) {
    super({ name: 'DefaultDomainJob', ...options });
  }

  /**
   * Create the initial OAuth 2.0 root user instance and domain.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    let defaultDomain: Domain;

    const defaultDomainCount = await Domain.safeCount({ where: { role: DomainRole.DEFAULT } });

    if (!defaultDomainCount) {
      // Create the default domain
      this.logger.debug(`DefaultDomainJob: Database is empty, creating default domain...`);

      await getManager().transaction(async transactionalEntityManager => {
        // Create the default domain
        defaultDomain = await Domain.insertAndFind(
          { ...(this.options.defaultDomain as any), settings: new DomainSettings() },
          { manager: transactionalEntityManager },
        );

        // Complete log for verbose purposes
        this.logger.info(
          '\n--------------------------------------------------------------------------\n' +
            '                                                                          \n' +
            '              NOTICE: A default domain was generated                      \n' +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  DOMAIN (Pending mediator creation)                                \n' +
            '                                                                          \n' +
            `        ID:         ${defaultDomain.id}                                   \n` +
            `        Name:       ${defaultDomain.name}                                 \n` +
            `        URL:        ${defaultDomain.urls.join(', ')}                      \n` +
            '                                                                          \n' +
            '\n------------------------------------------------------------------------\n',
        );
      });
    }
  }
}
