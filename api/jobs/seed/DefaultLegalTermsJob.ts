import { Job, JobOptions } from 'ts-framework-common';
import Server from 'ts-framework';
import { getManager } from 'typeorm';
import { LegalTerm } from '../../models';

export interface DefaultLegalTermsJobOptions extends JobOptions {
  defaultLegalTerms: Pick<LegalTerm, 'name' | 'type' | 'body' | 'provider'>[];
}

export class DefaultLegalTermsJob extends Job {
  public readonly options: DefaultLegalTermsJobOptions;

  constructor(options: DefaultLegalTermsJobOptions) {
    super({ name: 'defaultLegalTermsJob', ...options });
  }

  public async run(server: Server) {
    const hasLegalTerms = (await LegalTerm.count()) > 0;

    // if legal terms already exist we have nothing to do here
    if (!hasLegalTerms) {
      this.logger.debug('defaultLegalTermsJob: Database is empty, creating default LegalTerms...');
      const legalTerms = this.options.defaultLegalTerms.map(legalTerm => LegalTerm.create(legalTerm));

      await getManager().transaction(async tx => {
        await tx.insert(LegalTerm, legalTerms);
      });

      this.logger.debug('defaultLegalTermsJob: Default legal terms created successfully');
    } else {
      this.logger.debug('defaultLegalTermsJob: nothing to do, exiting...');
    }
  }
}
