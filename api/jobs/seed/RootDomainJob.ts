import { ConsumerStatus, DomainRole, DomainSchema, UserRole, UserStatus } from '@bacen/base-sdk';
import { EnvConfigStorage, NanoConfig } from 'nano-config';
import { dirname, resolve, sep } from 'path';
import { Job, JobOptions } from 'ts-framework-common';
import { getManager } from 'typeorm';
import { RootMediatorSeed } from '../../../config/seed/mediatorSeed.config';
import MainServer from '../../MainServer';
import { Domain, DomainSettings, User } from '../../models';
import { ConsumerRepository, UserRepository } from '../../repositories';
import { MediatorPipelinePublisher, WalletService } from '../../services';
import { getDefaultFingerprint } from '../../utils';

export interface RootDomainJobOptions extends JobOptions {
  rootDomain: DomainSchema;
  rootMediator: RootMediatorSeed;
}

export class RootDomainJob extends Job {
  public options: RootDomainJobOptions;

  constructor(options: RootDomainJobOptions) {
    super({ name: 'RootDomainJob', ...options });
  }

  /**
   * Create the initial OAuth 2.0 root user instance and domain.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    let rootDomain: Domain;

    const rootDomainCount = await Domain.safeCount({ where: { role: DomainRole.ROOT } });

    if (!rootDomainCount) {
      // Create the root domain
      this.logger.debug(`RootDomainJob: Database is empty, creating root domain...`);

      const rootMediator = await getManager().transaction(async (tx) => {
        const userRepositoy = new UserRepository(tx);
        const consumerRepository = new ConsumerRepository(tx);

        // Create the root domain
        rootDomain = await Domain.insertAndFind(
          { ...(this.options.rootDomain as any), settings: new DomainSettings() },
          { manager: tx },
        );

        const rootAdmin = await User.findOne({
          where: {
            role: UserRole.ADMIN,
          },
          relations: ['domain'],
        });

        if (rootAdmin && !rootAdmin.domain) {
          await tx.update(User, rootAdmin.id, { domain: rootDomain });
        }

        // Create the root domain mediator user
        this.logger.debug(`RootDomainJob: Creating domain mediator...`);

        const { consumer: consumerSchema, ...userSchema } = this.options.rootMediator;

        const rootMediator = await userRepositoy.create({ domain: rootDomain, ...userSchema });
        await userRepositoy.appendState(rootMediator, {
          status: UserStatus.PENDING,
          additionalData: {
            source: 'RootDomainJob',
            skipped: true,
          },
        });

        // Prepare consumer details
        const { addresses, phones, companyData, ...consumerData } = consumerSchema;

        // Validate consumer stuff
        const consumer = await consumerRepository.create({ user: rootMediator, companyData, ...consumerData });
        // Get address information from CEP
        if (addresses) {
          await consumerRepository.createConsumerAddresses(consumer, addresses);
        }

        if (phones) {
          await consumerRepository.createConsumerPhones(consumer, phones);
        }

        const createdAt = Date.now();

        // Create initial consumer states skipping document verification
        await consumerRepository.appendState(consumer, {
          createdAt: new Date(createdAt - 3),
          status: ConsumerStatus.PENDING_DOCUMENTS,
          additionalData: {
            source: 'RootDomainJob',
            skipped: true,
          } as any,
        });

        // Insert mediator with processing state
        await userRepositoy.appendState(rootMediator, { status: UserStatus.PROCESSING });

        // Insert consumer processing states
        await consumerRepository.appendState(consumer, [
          {
            status: ConsumerStatus.READY,
            additionalData: {
              source: 'RootDomainJob',
            },
          },
        ]);

        // Create the root domain mediator wallets
        this.logger.debug(`RootDomainJob: Creating mediator wallet...`);
        const walletService = WalletService.getInstance();
        const mediatorStellarWallet = await walletService.createRootMediatorInitialWallet({
          manager: tx,
          user: rootMediator,
          stateAdditionalData: {
            source: 'RootDomainJob',
            acceptProviderLegalTerms: true,
            fingerprint: getDefaultFingerprint(),
          },
        });

        // Send mediator to pipeline
        await MediatorPipelinePublisher.getInstance().send(rootMediator);

        // Complete log for verbose purposes
        this.logger.info(
          '\n------------------------------------------------------------------------\n' +
            '                                                                          \n' +
            '              NOTICE: A root domain was generated                         \n' +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  DOMAIN                                                            \n' +
            '                                                                          \n' +
            `        ID:         ${rootDomain.id}                                      \n` +
            `        Name:       ${rootDomain.name}                                    \n` +
            `        URL:        ${rootDomain.urls.join(', ')}                         \n` +
            '                                                                          \n' +
            '                                                                          \n' +
            '     *  MEDIATOR                                                          \n' +
            '                                                                          \n' +
            `        Name:              ${rootMediator.name}                           \n` +
            `        Email:             ${rootMediator.email}                          \n` +
            `        Password:          ${this.options.rootMediator.password}          \n` +
            `        Status:            ${rootMediator.status}                         \n` +
            `        Mediator Wallet:   ${mediatorStellarWallet.id}                    \n` +
            '                                                                          \n' +
            '\n------------------------------------------------------------------------\n',
        );

        return rootMediator;
      });

      // Save file locally for development purposes
      if (process.env.NODE_EN !== 'production') {
        const name = 'root-mediator';
        const { PROVIDER_ENV_PATH: providerEnvPath = '../../../../../.bacen' } = process.env;
        let basePath = resolve(__dirname, providerEnvPath);

        if (dirname(basePath).split(sep).pop() === ' ') {
          basePath = resolve(__dirname, '../', providerEnvPath);
        }

        const config = new NanoConfig({
          basePath,
          name,
          storage: new EnvConfigStorage({ name, basePath }),
          schema: {
            bacen_ADMIN_EMAIL: { format: String, default: rootMediator.email },
            bacen_ADMIN_PASSWORD: { format: String, default: this.options.rootMediator.password },
          },
        });

        await config.dump();
      }
    }
  }
}
