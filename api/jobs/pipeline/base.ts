import { Job, JobOptions } from 'ts-framework-common';
import MainServer from '../../MainServer';
import { BasePipelineSubscriber } from '../../services';

export interface BasePipelineJobOptions<Data, Payload, Listener extends Function> extends JobOptions {
  service?: BasePipelineSubscriber<Data, Payload, Listener>;
  routes?: string[];
}

export default abstract class BasePipelineJob<Data, Payload, Listener extends Function> extends Job {
  protected readonly service: BasePipelineSubscriber<Data, Payload, Listener>;

  public abstract onData: Listener;

  public options: BasePipelineJobOptions<Data, Payload, Listener>;

  constructor(options: BasePipelineJobOptions<Data, Payload, Listener>) {
    super(options);

    if (this.options.service) {
      this.service = this.options.service;
    }

    this.run = this.run.bind(this);
  }

  async onMount(server) {
    await this.service.onMount();
    await super.onMount(server);
  }

  async onInit(server) {
    await this.service.onInit();
    await super.onInit(server);
  }

  public async run(server: MainServer): Promise<void> {
    return this.service.subscribe(this.onData.bind(this), {});
  }

  async onUnmount(server) {
    await this.service.onUnmount();
    await super.onUnmount(server);
  }
}
