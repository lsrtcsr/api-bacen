import { AMQPMessage } from 'ts-framework-queue';
import { BaseError } from 'ts-framework-common';
import { get as getTracer } from '@google-cloud/trace-agent';
import * as Package from 'pjson';
import {
  TransactionStatus,
  SeverityLevel,
  IssueType,
  IssueCategory,
  IssueDetails,
  PaymentStatus,
} from '@bacen/base-sdk';
import fclone from 'fclone';
import { Throttle } from '@bacen/shared-sdk';
import { TransactionStateMachine } from '../../../fsm';
import {
  TransactionStateItem,
  Transaction,
  TRANSACTION_BLOCKED_BALANCE_STATES,
  TRANSACTION_FINAL_STATES,
} from '../../../models';
import {
  AMQPService,
  TransactionPipelinePayload,
  TransactionPipelineRoutes,
  TransactionPipelineSubscriber,
  TransactionPipelineSubscription,
  IssueHandler,
} from '../../../services';
import BasePipelineJob, { BasePipelineJobOptions } from '../base';
import Config from '../../../../config';
import { traceAsyncCall } from '../../../utils';

export interface TransactionPipelineJobOptions
  extends BasePipelineJobOptions<Transaction, TransactionPipelinePayload, TransactionPipelineSubscription> {
  amqp?: AMQPService;
}

export class TransactionPipelineJob extends BasePipelineJob<
  Transaction,
  TransactionPipelinePayload,
  TransactionPipelineSubscription
> {
  public options: TransactionPipelineJobOptions;

  constructor(options: TransactionPipelineJobOptions) {
    super({
      ...options,
      name: 'TransactionPipelineJob',
      routes: [TransactionPipelineRoutes.DEFAULT],
      service: TransactionPipelineSubscriber.initialize({
        amqp: options.amqp,
        routes: [TransactionPipelineRoutes.DEFAULT],
      }),
    });
  }

  public onData: TransactionPipelineSubscription = async (
    instance: Transaction | undefined,
    msg: AMQPMessage,
    { publish, ack },
  ): Promise<void> => {
    const throttledAck = traceAsyncCall(Throttle(ack, 0, 1), { name: 'throttledAck' });
    const throttledPublish = traceAsyncCall(Throttle(publish, 5, 10), { name: 'throttledPublish' });

    const transaction = await Transaction.safeFindOne({
      where: { id: instance.id },
      relations: ['payments', 'states'],
    });

    try {
      if (!transaction) {
        throw new Error('Transaction is undefined');
      }

      const transactionFSM = new TransactionStateMachine(transaction);
      const transactionStatus = transaction.status;

      const tracer = getTracer();
      const rootSpan = tracer.getCurrentRootSpan();
      if (tracer.isRealSpan(rootSpan)) {
        rootSpan.addLabel('entity', 'Transaction');
        rootSpan.addLabel('transactionId', transaction.id);
        rootSpan.addLabel('transactionStatus', transactionStatus);
      }

      if (
        TRANSACTION_BLOCKED_BALANCE_STATES.includes(transactionStatus) &&
        !transaction.payments.every(payment => payment.status === PaymentStatus.FAILED)
      ) {
        const success = await transactionFSM.goTo(TransactionStatus.EXECUTED);

        if (success) {
          // Send new message to queue for mediator notification
          await publish(TransactionPipelineRoutes.DEFAULT, transaction);
        }
      } else if (transactionStatus === TransactionStatus.EXECUTED) {
        const success = await transactionFSM.goTo(TransactionStatus.NOTIFIED);

        if (!success) {
          // Send the message back to the end of the queue
          await publish(TransactionPipelineRoutes.DEFAULT, transaction);
        }
      } else if (transaction.payments.every(payment => payment.status === PaymentStatus.FAILED)) {
        const success = await transactionFSM.goTo(TransactionStatus.FAILED);

        if (!success) {
          // Send the message back to the end of the queue
          await publish(TransactionPipelineRoutes.DEFAULT, transaction);
        }
      } else if (TRANSACTION_FINAL_STATES.includes(transactionStatus)) {
        const issueHandler = IssueHandler.getInstance();
        await issueHandler.handle({
          type: IssueType.FSM_ACTION_EXECUTION_FAILED,
          category: IssueCategory.PAYMENT,
          severity: SeverityLevel.NORMAL,
          componentId: Package.name,
          description: 'Transaction is already in a final state',
          details: IssueDetails.from(transaction),
        });

        this.logger.warn(`${this.options.name} there are no actions to be performed`, {
          transaction: transaction.id,
          status: transactionStatus,
        });
        return ack();
      }

      // Acknowledge task completion
      await throttledAck();
      this.logger.info(`${this.options.name} completed a task`, {
        transaction: {
          id: transaction.id,
          status: transactionFSM.instance.status,
          additionalData: transaction.additionalData,
        },
      });
    } catch (exception) {
      return this.onFail(transaction, msg, { publish: throttledPublish, ack: throttledAck }, exception);
    }
  };

  public onFail = async (
    transaction: Transaction,
    msg: AMQPMessage,
    { ack, publish },
    exception: any,
  ): Promise<void> => {
    const baseError = exception.stackId ? exception : new BaseError(exception);

    // Log execution exception for audit
    this.logger.error(`${this.options.name} got an exception trying to run task. `, {
      transaction: transaction ? transaction.id : undefined,
      message: exception.message,
      stackId: baseError.stackId,
      details: {
        message: exception.message,
        ...(exception.details || (exception.response ? exception.response.data : undefined)),
        stack: exception.stack || baseError.stack,
      },
      task: {
        appId: msg?.properties?.appId,
        userId: msg?.properties?.userId,
        messageId: msg?.properties?.messageId,
        queueName: msg?.fields?.exchange,
        deliveryTag: msg?.fields?.deliveryTag,
        routingKey: msg?.fields?.routingKey,
      },
    });

    const issueHandler = IssueHandler.getInstance();
    await issueHandler.handle({
      type: IssueType.UNKNOWN,
      category: IssueCategory.STELLAR_TRANSACTION,
      severity: SeverityLevel.HIGH,
      componentId: Package.name,
      description: baseError.message || 'error processing transaction',
      details: IssueDetails.from(transaction, baseError),
    });

    // Store exception in Audit and in state additionalData
    if (transaction) {
      const states = transaction.getStates();

      if (states && states.length) {
        const retries = states[0].additionalData?.retries || 0;

        // Send the message back to queue with timeout
        if (retries < Config.queue.transaction.maxRetries) {
          await TransactionStateItem.createQueryBuilder('state')
            .update()
            .where({ id: states[0].id })
            .set(
              fclone({
                additionalData: {
                  retries: retries + 1,
                  status: exception.status,
                  stackId: baseError.stackId,
                  exception: exception.title || exception.message,
                  ...exception.extras,
                  ...exception.details,
                },
              }),
            )
            .execute();

          // Send the message back to the end of the queue
          await publish(TransactionPipelineRoutes.DEFAULT, transaction);
          await ack();
          return;
        }

        await issueHandler.handle({
          type: IssueType.MAX_NUMBER_RETRIES_REACHED,
          category: IssueCategory.OTHER,
          severity: SeverityLevel.HIGH,
          componentId: Package.name,
          description: `error processing transaction: ${baseError.message}`,
          details: IssueDetails.from(transaction, baseError),
        });
      }
    }

    // There's nothing we can do with it, let's just ack it
    return ack();
  };
}
