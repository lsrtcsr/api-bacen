import { AMQPMessage } from 'ts-framework-queue';
import { BaseError } from 'ts-framework-common';
import { get as getTracer } from '@google-cloud/trace-agent';
import * as currency from 'currency.js';
import * as Package from 'pjson';
import { Throttle } from '@bacen/shared-sdk';
import { SeverityLevel, IssueType, IssueCategory } from '@bacen/base-sdk';
import fclone from 'fclone';
import { InvoiceStatus, Invoice, EntryStatus, InvoiceState } from '../../../../models';
import BasePipelineJob, { BasePipelineJobOptions } from '../../base';
import { InvoiceStateMachine } from '../../../../fsm/billing/invoice';
import {
  AMQPService,
  InvoicePipelinePayload,
  InvoicePipelineRoutes,
  InvoicePipelineSubscriber,
  InvoicePipelineSubscription,
  IssueHandler,
  CacheService,
} from '../../../../services';
import Config from '../../../../../config';
import { traceAsyncCall } from '../../../../utils';

export interface InvoicePipelineJobOptions
  extends BasePipelineJobOptions<Invoice, InvoicePipelinePayload, InvoicePipelineSubscription> {
  amqp?: AMQPService;
}

export class InvoicePipelineJob extends BasePipelineJob<Invoice, InvoicePipelinePayload, InvoicePipelineSubscription> {
  public options: InvoicePipelineJobOptions;

  protected readonly stateOrder: Map<InvoiceStatus, number> = new Map();

  constructor(options: InvoicePipelineJobOptions) {
    super({
      ...(options as any),
      name: 'InvoicePipelineJob',
      routes: [InvoicePipelineRoutes.DEFAULT],
      service: InvoicePipelineSubscriber.initialize({
        amqp: options.amqp,
        routes: [InvoicePipelineRoutes.DEFAULT],
      }),
    });

    this.stateOrder.set(InvoiceStatus.ACTIVE, 1);
    this.stateOrder.set(InvoiceStatus.CLOSING, 2);
    this.stateOrder.set(InvoiceStatus.CLOSED, 3);
    this.stateOrder.set(InvoiceStatus.PENDING_SETTLEMENT, 4);
    this.stateOrder.set(InvoiceStatus.SETTLED, 5);
  }

  /**
   * TODO
   * if max num of retries was reached, publish it to dead letter queue
   */
  public onData: InvoicePipelineSubscription = async (
    invoice: Invoice | undefined,
    msg: AMQPMessage,
    { publish, ack },
  ): Promise<void> => {
    if (!invoice) {
      throw new Error('Invoice is undefined');
    }

    const throttledAck = traceAsyncCall(Throttle(ack, 1, 5), { name: 'throttledAck' });
    const throttledPublish = traceAsyncCall(Throttle(publish, 5, 15), { name: 'throttledPublish' });

    try {
      const invoiceFSM = new InvoiceStateMachine(invoice);
      const invoiceStatus = invoice.status;

      const tracer = getTracer();
      const rootSpan = tracer.getCurrentRootSpan();
      if (tracer.isRealSpan(rootSpan)) {
        rootSpan.addLabel('entity', 'Invoice');
        rootSpan.addLabel('invoiceId', invoice.id);
        rootSpan.addLabel('invoiceStatus', invoiceStatus);
      }

      const stateLocked = await CacheService.getInstance().get(invoice.id);
      if (this.isSameOrBefore(invoiceStatus, stateLocked)) {
        this.logger.warn('Duplicated message: aborting', { invoice: invoice.id });
        return throttledAck();
      }

      await CacheService.getInstance().set(invoice.id, invoiceStatus, 5 * 60);

      if (invoiceStatus === InvoiceStatus.ACTIVE) {
        const success = await invoiceFSM.goTo(InvoiceStatus.CLOSING);

        // Send the message back to queue with timeout
        if (success) await throttledPublish(InvoicePipelineRoutes.DEFAULT, invoice);
        else return this.retry(invoice, throttledPublish, throttledAck);
      } else if (invoiceStatus === InvoiceStatus.CLOSING) {
        // if there is still an open period, wait until it is closed
        if (await invoice.getCurrentPeriod()) {
          return this.retry(invoice, throttledPublish, throttledAck);
        }

        // otherwise, go ahead
        const success = await invoiceFSM.goTo(InvoiceStatus.CLOSED);

        if (success) await throttledPublish(InvoicePipelineRoutes.DEFAULT, invoice);
        else return this.retry(invoice, throttledPublish, throttledAck);
      } else if (invoiceStatus === InvoiceStatus.CLOSED) {
        const balance = await invoice.balance(EntryStatus.PENDING);
        const success =
          currency(balance).value === 0
            ? await invoiceFSM.goTo(InvoiceStatus.SETTLED)
            : await invoiceFSM.goTo(InvoiceStatus.PENDING_SETTLEMENT);
      } else if (
        [InvoiceStatus.SETTLED, InvoiceStatus.CANCELED, InvoiceStatus.PENDING_SETTLEMENT].includes(invoiceStatus)
      ) {
        this.logger.warn(`${this.options.name} there are no action to be performed`, {
          invoice: invoice.id,
          status: invoiceStatus,
        });
      } else {
        this.logger.warn(`${this.options.name} received message in unknown state, skipping...`, {
          invoice: invoice.id,
          status: invoiceStatus,
        });
        return throttledAck();
      }

      // Acknowledge task completion
      await throttledAck();
      this.logger.info(`${this.options.name} completed a task`, {
        invoice: invoice.id,
        status: invoiceStatus,
      });
    } catch (exception) {
      this.onFail(invoice, msg, { ack: throttledAck, publish: throttledPublish }, exception);
    } finally {
      await CacheService.getInstance().del(invoice.id);
    }
  };

  public isSameOrBefore(current: InvoiceStatus, locked: InvoiceStatus): boolean {
    return this.stateOrder.get(current) <= this.stateOrder.get(locked);
  }

  public onFail = async (invoice: Invoice, msg: AMQPMessage, { ack, publish }, exception: any): Promise<void> => {
    const baseError = exception.stackId ? exception : new BaseError(exception.message, exception);

    // Log execution exception for audit
    this.logger.error(`${this.options.name} got an exception trying to run task`, {
      invoice: invoice ? invoice.id : undefined,
      message: exception.message,
      stackId: baseError.stackId,
      details: exception.details || (exception.response && exception.response.data),
      task: {
        appId: msg.properties.appId,
        userId: msg.properties.userId,
        messageId: msg.properties.messageId,
        queueName: msg.fields.exchange,
        deliveryTag: msg.fields.deliveryTag,
        routingKey: msg.fields.routingKey,
      },
    });

    await this.retry(invoice, publish, ack, exception);
  };

  public async retry(instance: Invoice, publish: Function, ack: Function, exception?: any): Promise<void> {
    if (!instance) return ack();

    const invoice =
      instance.states && instance.states.length
        ? instance
        : await Invoice.findOne({ where: { id: instance.id }, relations: ['states'] });

    const states = invoice.getStates();
    const retries = states[0].additionalData.retries || 0;

    const errorData = exception
      ? fclone({
          stackId: exception.stackId,
          exception: exception.title || exception.message,
          ...exception.extras,
          ...exception.details,
        })
      : undefined;

    // Send the message back to queue with timeout
    if (retries < Config.queue.invoice.maxRetries) {
      await InvoiceState.createQueryBuilder('state')
        .update()
        .where({ id: states[0].id })
        .set({ additionalData: { retries: retries + 1, error: errorData } })
        .execute();

      // Send the message back to the end of the queue
      await publish(InvoicePipelineRoutes.DEFAULT, invoice);
      await ack();
      return;
    }

    await IssueHandler.getInstance().handle({
      type: IssueType.MAX_NUMBER_RETRIES_REACHED,
      category: IssueCategory.INVOICE_STATE_TRANSITION,
      severity: SeverityLevel.HIGH,
      componentId: Package.name,
      details: {
        exception,
        resourceId: invoice.id,
        resourceType: 'Invoice',
        resourceSnapshot: invoice,
      },
      description: `invoice ${invoice.id} removed from queue due to max retries was reached`,
    });

    await ack();
  }
}
