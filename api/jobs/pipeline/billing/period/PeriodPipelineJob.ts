import { AMQPMessage } from 'ts-framework-queue';
import { BaseError } from 'ts-framework-common';
import { get as getTracer } from '@google-cloud/trace-agent';
import * as Package from 'pjson';
import { SeverityLevel, IssueType, IssueCategory } from '@bacen/base-sdk';
import { Throttle } from '@bacen/shared-sdk';
import fclone from 'fclone';
import { PeriodStatus, Period, PeriodState } from '../../../../models';
import BasePipelineJob, { BasePipelineJobOptions } from '../../base';
import { PeriodStateMachine } from '../../../../fsm/billing/period';
import {
  AMQPService,
  PeriodPipelinePayload,
  PeriodPipelineRoutes,
  PeriodPipelineSubscriber,
  PeriodPipelineSubscription,
  IssueHandler,
  CacheService,
} from '../../../../services';
import Config from '../../../../../config';
import { traceAsyncCall } from '../../../../utils';

export interface PeriodPipelineJobOptions
  extends BasePipelineJobOptions<Period, PeriodPipelinePayload, PeriodPipelineSubscription> {
  amqp?: AMQPService;
}

export class PeriodPipelineJob extends BasePipelineJob<Period, PeriodPipelinePayload, PeriodPipelineSubscription> {
  public options: PeriodPipelineJobOptions;

  protected readonly stateOrder: Map<PeriodStatus, number> = new Map();

  constructor(options: PeriodPipelineJobOptions) {
    super({
      ...(options as any),
      name: 'PeriodPipelineJob',
      routes: [PeriodPipelineRoutes.DEFAULT],
      service: PeriodPipelineSubscriber.initialize({
        amqp: options.amqp,
        routes: [PeriodPipelineRoutes.DEFAULT],
      }),
    });

    this.stateOrder.set(PeriodStatus.ACTIVE, 1);
    this.stateOrder.set(PeriodStatus.CLOSED, 2);
  }

  /**
   * review!!!
   */
  public onData: PeriodPipelineSubscription = async (
    data: Period | undefined,
    msg: AMQPMessage,
    { publish, ack },
  ): Promise<void> => {
    if (!data) throw new Error('Period is undefined');

    const throttledAck = traceAsyncCall(Throttle(ack, 1, 5), { name: 'throttledAck' });
    const throttledPublish = traceAsyncCall(Throttle(publish, 5, 15), { name: 'throttledPublish' });

    const period =
      data.invoice && data.states && data.states.length
        ? data
        : await Period.safeFindOne({ where: { id: data.id }, relations: ['invoice', 'states'] });

    try {
      const periodFSM = new PeriodStateMachine(period);
      const periodStatus = period.status;

      const tracer = getTracer();
      const rootSpan = tracer.getCurrentRootSpan();
      if (tracer.isRealSpan(rootSpan)) {
        rootSpan.addLabel('entity', 'Period');
        rootSpan.addLabel('periodId', period.id);
        rootSpan.addLabel('periodStatus', periodStatus);
      }

      const stateLocked = await CacheService.getInstance().get(period.id);
      if (this.isSameOrBefore(periodStatus, stateLocked)) {
        this.logger.warn(`Period closing aborted: period/period is being closed`, {
          period: period.id,
          invoice: period.invoice.id,
        });
        return throttledAck();
      }

      await CacheService.getInstance().set(period.id, periodStatus, 5 * 60);

      if (periodStatus === PeriodStatus.ACTIVE) {
        const success = await periodFSM.goTo(PeriodStatus.CLOSED);

        if (!success) return this.retry(period, throttledPublish, throttledAck);
      } else if ([PeriodStatus.CLOSED, PeriodStatus.CANCELED].includes(periodStatus)) {
        this.logger.warn(`${this.options.name} there are no action to be performed`, {
          period: period.id,
          status: periodStatus,
        });
      } else {
        this.logger.warn(`${this.options.name} received message in unknown state, skipping...`, {
          period: period.id,
          status: periodStatus,
        });
        return throttledAck();
      }

      // Acknowledge task completion
      await throttledAck();
      this.logger.info(`${this.options.name} completed a task`, {
        period: period.id,
        status: periodStatus,
      });
    } catch (exception) {
      this.onFail(period, msg, { ack, publish }, exception);
    } finally {
      await CacheService.getInstance().del(period.id);
    }
  };

  public isSameOrBefore(current: PeriodStatus, locked: PeriodStatus): boolean {
    return this.stateOrder.get(current) <= this.stateOrder.get(locked);
  }

  public onFail = async (period: Period, msg: AMQPMessage, { ack, publish }, exception: any): Promise<void> => {
    const baseError = exception.stackId ? exception : new BaseError(exception.message, exception);

    // Log execution exception for audit
    this.logger.error(`${this.options.name} got an exception trying to run task`, {
      period: period ? period.id : undefined,
      message: exception.message,
      stackId: baseError.stackId,
      details: exception.details || (exception.response && exception.response.data),
      task: {
        appId: msg.properties.appId,
        userId: msg.properties.userId,
        messageId: msg.properties.messageId,
        queueName: msg.fields.exchange,
        deliveryTag: msg.fields.deliveryTag,
        routingKey: msg.fields.routingKey,
      },
    });

    await this.retry(period, publish, ack, exception);
  };

  public async retry(instance: Period, publish: Function, ack: Function, exception?: any): Promise<void> {
    if (!instance) return ack();

    const period =
      instance.states && instance.states.length
        ? instance
        : await Period.findOne({ where: { id: instance.id }, relations: ['states'] });

    const states = period.getStates();
    const retries = states[0].additionalData.retries || 0;

    const errorData = exception
      ? fclone({
          stackId: exception.stackId,
          exception: exception.title || exception.message,
          ...exception.extras,
          ...exception.details,
        })
      : undefined;

    // Send the message back to queue with timeout
    if (retries < Config.queue.period.maxRetries) {
      await PeriodState.createQueryBuilder('state')
        .update()
        .where({ id: states[0].id })
        .set({ additionalData: { retries: retries + 1, error: errorData } })
        .execute();

      // Send the message back to the end of the queue
      await publish(PeriodPipelineRoutes.DEFAULT, period);
      await ack();
      return;
    }

    await IssueHandler.getInstance().handle({
      type: IssueType.MAX_NUMBER_RETRIES_REACHED,
      category: IssueCategory.INVOICE_STATE_TRANSITION,
      severity: SeverityLevel.HIGH,
      componentId: Package.name,
      details: {
        exception,
        resourceId: period.id,
        resourceType: 'Period',
        resourceSnapshot: period,
      },
      description: `period ${period.id} removed from queue due to max retries was reached`,
    });

    await ack();
  }
}
