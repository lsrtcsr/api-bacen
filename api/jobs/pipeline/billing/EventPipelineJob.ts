import { AMQPMessage } from 'ts-framework-queue';
import { BaseError } from 'ts-framework-common';
import { get as getTracer } from '@google-cloud/trace-agent';
import * as Package from 'pjson';
import fclone from 'fclone';
import { Throttle } from '@bacen/shared-sdk';
import { SeverityLevel, IssueType, IssueCategory, IssueDetails } from '@bacen/base-sdk';
import BasePipelineJob, { BasePipelineJobOptions } from '../base';
import {
  AMQPService,
  IssueHandler,
  EventPipelinePayload,
  EventPipelineSubscription,
  EventPipelineRoutes,
  EventPipelineSubscriber,
  BillingService,
} from '../../../services';
import { ServiceConsumptionData } from '../../../schemas/request/billing';
import { Event, EventStatus } from '../../../models';
import Config from '../../../../config';
import { GenericError } from '../../../errors';
import { traceAsyncCall } from '../../../utils';

export interface EventPipelineJobOptions
  extends BasePipelineJobOptions<ServiceConsumptionData, EventPipelinePayload, EventPipelineSubscription> {
  amqp?: AMQPService;
}

export class EventPipelineJob extends BasePipelineJob<
  ServiceConsumptionData,
  EventPipelinePayload,
  EventPipelineSubscription
> {
  public options: EventPipelineJobOptions;

  constructor(options: EventPipelineJobOptions) {
    super({
      ...(options as any),
      name: 'EventPipelineJob',
      routes: [EventPipelineRoutes.DEFAULT],
      service: EventPipelineSubscriber.initialize({
        amqp: options.amqp,
        routes: [EventPipelineRoutes.DEFAULT],
      }),
    });
  }

  public onData: EventPipelineSubscription = async (
    event: Event | undefined,
    msg: AMQPMessage,
    { publish, ack },
  ): Promise<void> => {
    if (!event) throw new Error('Event data is undefined');

    const throttledAck = traceAsyncCall(Throttle(ack, 1, 5), { name: 'throttledAck' });
    const throttledPublish = traceAsyncCall(Throttle(publish, 5, 15), { name: 'throttledPublish' });

    try {
      const tracer = getTracer();
      const rootSpan = tracer.getCurrentRootSpan();
      if (tracer.isRealSpan(rootSpan)) {
        rootSpan.addLabel('entity', 'Event');
        rootSpan.addLabel('eventId', event.id);
        rootSpan.addLabel('eventStatus', event.status);
      }

      if (event.status === EventStatus.PROCESSED) {
        this.logger.info(`Event ${event.id} already processed, skipping...`);
        return throttledAck();
      }

      await BillingService.getInstance().createEntry(event);

      await Event.update(event.id, { status: EventStatus.PROCESSED });

      // Acknowledge task completion
      await throttledAck();
      this.logger.info(`${this.options.name} completed a task`, { event: event.id });
    } catch (exception) {
      this.onFail(event, msg, { ack: throttledAck, publish: throttledPublish }, exception);
    }
  };

  public onFail = async (event: Event, msg: AMQPMessage, { ack, publish }, exception: any): Promise<void> => {
    if (
      exception instanceof GenericError &&
      [IssueType.PERIOD_CLOSING_DATE_EXPIRED, IssueType.INVOICE_CLOSING_DATE_EXPIRED].includes(exception.type)
    ) {
      await IssueHandler.getInstance().handle({
        type: exception.type,
        category: exception.category,
        severity: SeverityLevel.NORMAL,
        componentId: Package.name,
        details: IssueDetails.from(event, exception),
        description: exception.message,
      });

      return ack();
    }

    const baseError = exception.stackId ? exception : new BaseError(exception.message, exception);

    // Log execution exception for audit
    this.logger.error(`${this.options.name} got an exception trying to run task`, {
      event,
      message: exception.message,
      stackId: baseError.stackId,
      details: exception.details || (exception.response && exception.response.data),
      task: {
        appId: msg.properties.appId,
        userId: msg.properties.userId,
        messageId: msg.properties.messageId,
        queueName: msg.fields.exchange,
        deliveryTag: msg.fields.deliveryTag,
        routingKey: msg.fields.routingKey,
      },
    });

    await this.retry(event, publish, ack, exception);
  };

  public async retry(event: Event, publish: Function, ack: Function, exception?: any): Promise<void> {
    if (!event) return ack();

    const retries = event.retries || 0;

    const data: ServiceConsumptionData = exception
      ? fclone({
          ...(event.data || {}),
          error: {
            stackId: exception.stackId,
            exception: exception.title || exception.message,
            ...exception.extras,
            ...exception.details,
          },
        })
      : fclone(event.data || {});

    // Send the message back to queue with timeout
    if (retries < Config.queue.invoice.maxRetries) {
      await Event.update(event.id, { retries: retries + 1, data });

      // Send the message back to the end of the queue
      await publish(EventPipelineRoutes.DEFAULT, event);
      await ack();
      return;
    }

    await IssueHandler.getInstance().handle({
      type: IssueType.MAX_NUMBER_RETRIES_REACHED,
      category: IssueCategory.INVOICE_STATE_TRANSITION,
      severity: SeverityLevel.HIGH,
      componentId: Package.name,
      details: {
        exception,
        resourceId: event.id,
        resourceType: 'Event',
        resourceSnapshot: event,
      },
      description: `event ${event.id} removed from queue due to max retries was reached`,
    });

    await ack();
  }
}
