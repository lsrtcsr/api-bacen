import { Job, JobOptions } from 'ts-framework-common';
import { ConsumerStatus, UnleashFlags, UserRole } from '@bacen/base-sdk';
import * as moment from 'moment';
import { UnleashUtil } from '@bacen/shared-sdk';
import { ConsumerPipelinePublisher } from '../../../services';
import MainServer from '../../../MainServer';
import { User, ConsumerState } from '../../../models';
import { ConsumerStateMachine } from '../../../fsm';
import config from '../../../../config';

export interface KYCRefreshJobOptions extends JobOptions {}

export class KYCRefreshJob extends Job {
  constructor(public options: KYCRefreshJobOptions) {
    super({ ...options });
  }

  /**
   * Send users to KYC recheck periodically.
   *
   * @param server The main server instance.
   */
  public async run(server: MainServer): Promise<void> {
    const skipKYCRecheck = UnleashUtil.isEnabled(UnleashFlags.KYC_SKIP_RECHECK);
    if (skipKYCRecheck) {
      this.logger.info(`Skipping KYC recheck for feature flag is active.`);
      return;
    }

    // Calculating how long ago was n days ago
    const fromDate = moment()
      .subtract(config.kyc.recheck.days, 'days')
      .endOf('day')
      .toDate();

    this.logger.info(
      `Looking for consumers who have been verified for ${config.kyc.recheck.days} days or more (< ${fromDate}).`,
    );

    // Looking for users with background checks that are too old
    const qb = User.createQueryBuilder('user');
    const usersWithOutdatedKYC = await qb
      .leftJoinAndSelect('user.domain', 'domain')
      .leftJoinAndSelect('user.consumer', 'consumer')
      .leftJoinAndSelect('consumer.states', 'consumerstate')
      .where('user.role = :role', { role: UserRole.CONSUMER })
      .andWhere(qb => {
        const subQuery = qb
          .subQuery()
          .select('MAX(consumer_states.created_at)')
          .from(ConsumerState, 'consumer_states')
          .where('consumer_states."consumerId" = consumer.id')
          .groupBy('consumer_states.consumer')
          .getQuery();
        return `consumerstate.created_at IN ${subQuery}`;
      })
      .andWhere('consumerstate.status = :status', { status: ConsumerStatus.READY })
      .andWhere(
        `:fromDate >= ${qb
          .subQuery()
          .select('MAX(consumer_states.created_at)')
          .from(ConsumerState, 'consumer_states')
          .where('consumer_states."consumerId" = consumer.id')
          .andWhere('consumer_states.status IN (:...statuses)', {
            statuses: [ConsumerStatus.PROCESSING_DOCUMENTS, ConsumerStatus.KYC_RECHECKING],
          })
          .getQuery()}`,
      )
      .setParameter('fromDate', fromDate)
      .getMany();

    const consumerPipelinePublisher = ConsumerPipelinePublisher.getInstance();

    for (const user of usersWithOutdatedKYC) {
      // Instantiating their FSM and changing their state
      const consumerFSM = new ConsumerStateMachine(user);
      await consumerFSM.goTo(ConsumerStatus.KYC_RECHECKING);
      await consumerPipelinePublisher.send(user);

      this.logger.info(
        `Consumer ${user.firstName} ${user.lastName} (${user.consumer.id}) was marked as due for a KYC recheck.`,
      );
    }
  }
}
