import {
  ConsumerStatus,
  IssueCategory,
  IssueDetails,
  IssueType,
  SeverityLevel,
  UserStatus,
} from '@bacen/base-sdk';
import { get as getTracer } from '@google-cloud/trace-agent';
import { Throttle } from '@bacen/shared-sdk';
import fclone from 'fclone';
import { BaseError } from 'nano-errors';
import * as Package from 'pjson';
import { AMQPMessage } from 'ts-framework-queue';
import Config from '../../../../config';
import { GenericError } from '../../../errors';
import { ConsumerStateMachine, UserStateMachine } from '../../../fsm';
import { ConsumerState, User } from '../../../models';
import {
  AMQPService,
  ConsumerPipelinePayload,
  ConsumerPipelineRoutes,
  ConsumerPipelineSubscriber,
  ConsumerPipelineSubscription,
  IssueHandler,
} from '../../../services';
import BasePipelineJob, { BasePipelineJobOptions } from '../base';
import { traceAsyncCall } from '../../../utils';

export interface ConsumerPipelineJobOptions
  extends BasePipelineJobOptions<User, ConsumerPipelinePayload, ConsumerPipelineSubscription> {
  amqp?: AMQPService;
}

export class ConsumerPipelineJob extends BasePipelineJob<User, ConsumerPipelinePayload, ConsumerPipelineSubscription> {
  public options: ConsumerPipelineJobOptions;

  constructor(options: Partial<ConsumerPipelineJobOptions>) {
    super({
      ...options,
      name: 'ConsumerPipelineJob',
      routes: [ConsumerPipelineRoutes.DEFAULT],
      service: ConsumerPipelineSubscriber.initialize({
        amqp: options.amqp,
        routes: [ConsumerPipelineRoutes.DEFAULT],
      }),
    });
  }

  public onData: ConsumerPipelineSubscription = async (
    instance: User,
    msg: AMQPMessage,
    { publish, ack, nack },
  ): Promise<void> => {
    if (!instance) {
      throw new Error('User is undefined');
    }

    const throttledPublish = traceAsyncCall(Throttle(publish, 1, 5), { name: 'throttledPublish' });

    const user =
      instance.consumer &&
      instance.states &&
      instance.domain &&
      instance.wallets &&
      instance.consumer.addresses &&
      instance.consumer.phones &&
      instance.consumer.states
        ? instance
        : await User.findOne({
            where: { id: instance.id },
            relations: [
              'states',
              'domain',
              'wallets',
              'consumer',
              'consumer.addresses',
              'consumer.phones',
              'consumer.states',
            ],
          });

    try {
      let result = false;
      const consumerFSM = new ConsumerStateMachine(user);
      const consumerStatus = user.consumer.status;

      const tracer = getTracer();
      const rootSpan = tracer.getCurrentRootSpan();
      if (tracer.isRealSpan(rootSpan)) {
        rootSpan.addLabel('entity', 'Consumer');
        rootSpan.addLabel('uesrId', user.id);
        rootSpan.addLabel('consumerStatus', consumerStatus);
      }

      // Consumer FSM lifecycle definition
      if (consumerStatus === ConsumerStatus.PENDING_PHONE_VERIFICATION) {
        // It will fail silently if the phone has not been verified yet
        result = await consumerFSM.goTo(ConsumerStatus.PENDING_LEGAL_ACCEPTANCE);
      } else if (consumerStatus === ConsumerStatus.PENDING_LEGAL_ACCEPTANCE) {
        // Will fail silently if user has not accepted all legal terms already.
        result = await consumerFSM.goTo(ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION);
      } else if (consumerStatus === ConsumerStatus.PENDING_BILLING_PLAN_SUBSCRIPTION) {
        // It will fail explicitly if the consumer has not subscribed to a billing plan yet.
        result = await consumerFSM.goTo(ConsumerStatus.READY);
      } else if (
        // Consumer is ready, but the main User is not, this could be a corner case, so let's try to handle it
        (user.status === UserStatus.PROCESSING || user.status === UserStatus.INACTIVE) &&
        consumerStatus === ConsumerStatus.READY
      ) {
        const userFSM = new UserStateMachine(user);
        result = await userFSM.goTo(UserStatus.ACTIVE);
      }

      // Acknowledge task completion
      await ack();

      if (result) {
        this.logger.info(`${this.options.name} completed a task`, {
          user: user.id,
          status: consumerStatus,
        });

        // Send the message back to the end of the queue
        await publish(ConsumerPipelineRoutes.DEFAULT, user);
      } else {
        this.logger.debug(`${this.options.name} skipped a task, fsm rejected the transition silently`, {
          user: user.id,
          status: consumerStatus,
        });
      }
    } catch (exception) {
      return this.onFail(user, msg, { ack, publish: throttledPublish }, exception);
    }
  };

  public onFail = async (user: User, msg: AMQPMessage, { ack, publish }, exception: any): Promise<void> => {
    const consumerStatus = user.consumer?.status;
    const baseError = exception.stackId ? exception : new BaseError(exception);

    // Log execution exception for audit
    this.logger.error(`${this.options.name} got an exception trying to run task. `, {
      user: user ? user.id : undefined,
      message: exception.message,
      stackId: baseError.stackId,
      details: {
        message: exception.message,
        originalMessage: exception.originalMessage,
        stack: exception.stack || baseError.stack,
        status: exception.response?.status,
      },
      task: {
        appId: msg.properties.appId,
        userId: msg.properties.userId,
        messageId: msg.properties.messageId,
        queueName: msg.fields.exchange,
        deliveryTag: msg.fields.deliveryTag,
        routingKey: msg.fields.routingKey,
      },
    });

    // Store exception in Audit and in state additionalData
    if (user && user.consumer) {
      const states = user.consumer.getStates();

      if (states && states.length) {
        if (
          user?.consumer?.status !== ConsumerStatus.PROVIDER_FAILED &&
          exception instanceof GenericError &&
          exception.category === IssueCategory.CUSTODY_PROVIDER &&
          exception.type === IssueType.CUSTODY_PROVIDER_IRRECOVERABLE_ERROR
        ) {
          const consumerFSM = new ConsumerStateMachine(user);
          await consumerFSM.goTo(ConsumerStatus.PROVIDER_FAILED);
          return ack();
        }

        const retries = states[0].additionalData.retries || 0;

        // Send the message back to queue with timeout
        if (retries < Config.queue.consumer.maxRetries) {
          await ConsumerState.createQueryBuilder('state')
            .update()
            .where({ id: states[0].id })
            .set(
              fclone({
                additionalData: {
                  retries: retries + 1,
                  stackId: baseError.stackId,
                  exception: exception.title || exception.message,
                  originalMessage: exception.originalMessage,
                  status: exception.response?.status,
                },
              }),
            )
            .execute();

          // Send the message back to the end of the queue
          await publish(ConsumerPipelineRoutes.DEFAULT, user);
          await ack();
          return;
        }

        const consumerFSM = new ConsumerStateMachine(user);
        const status = exception.details?.response?.status || undefined;
        const isOnboardingInProvider = [
          ConsumerStatus.PROCESSING_WALLETS,
          ConsumerStatus.PROCESSING_PROVIDER_DOCUMENTS,
        ].includes(consumerStatus);

        if (exception instanceof GenericError && exception.category === IssueCategory.IDENTITY_DATA_PROVIDER) {
          await consumerFSM.goTo(ConsumerStatus.MANUAL_VERIFICATION);
        } else if (isOnboardingInProvider && status && status < 500) {
          // Some expected error, so provider must have rejected the registration
          await consumerFSM.goTo(ConsumerStatus.PROVIDER_REJECTED, exception?.details?.response?.data);
        } else if (
          consumerFSM.canGoTo(ConsumerStatus.PROVIDER_FAILED) &&
          exception instanceof GenericError &&
          [IssueType.STELLAR_NETWORK_REGISTRATION_FAILED, IssueType.CUSTODY_PROVIDER_REGISTRATION_FAILED].includes(
            exception.type,
          )
        ) {
          await consumerFSM.goTo(ConsumerStatus.PROVIDER_FAILED, exception?.details?.response?.data);
        } else if (consumerFSM.canGoTo(ConsumerStatus.PROVIDER_FAILED)) {
          // TODO: Send to specific unknown error state, this is not being correctly handled
          await consumerFSM.goTo(ConsumerStatus.PROVIDER_FAILED, exception?.details?.response?.data);
        }

        await IssueHandler.getInstance().handle({
          type: IssueType.MAX_NUMBER_RETRIES_REACHED,
          severity: SeverityLevel.NORMAL,
          category: IssueCategory.KYC_FLOW,
          description: `consumer ${user.consumer.id} removed from queue due to max retries was reached`,
          componentId: Package.name,
          details: IssueDetails.from(user, exception),
        });
      }
    }
    return ack();
  };
}
