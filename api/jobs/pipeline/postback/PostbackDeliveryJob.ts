import { RequestSigningHeaders, RequestUtil } from '@bacen/base-sdk';
import { Throttle } from '@bacen/shared-sdk';
import fclone from 'fclone';
import { Logger } from 'nano-errors';
import { AMQPMessage } from 'ts-framework-queue';
import { PostbackDelivery } from '../../../models';
import {
  AMQPService,
  PostbackDeliveryRoutes,
  PostbackDeliveryService,
  PostbackDeliverySubscriber,
  PostbackDeliverySubscription,
} from '../../../services';
import { traceAsyncCall } from '../../../utils';
import BasePipeline, { BasePipelineJobOptions } from '../base';

export interface PostbackDeliveryJobOptions
  extends BasePipelineJobOptions<PostbackDelivery, { id: string }, PostbackDeliverySubscription> {
  amqp?: AMQPService;
}

export class PostbackDeliveryJob extends BasePipeline<PostbackDelivery, { id: string }, PostbackDeliverySubscription> {
  public options: PostbackDeliveryJobOptions;

  public delivery = PostbackDeliveryService.getInstance();

  public constructor(options: PostbackDeliveryJobOptions) {
    super({
      ...options,
      name: 'PostbackDeliveryJob',
      routes: [PostbackDeliveryRoutes.POSTBACK],
      service: PostbackDeliverySubscriber.initialize({
        amqp: options.amqp,
        routes: [PostbackDeliveryRoutes.POSTBACK],
      }),
    } as BasePipelineJobOptions<PostbackDelivery, { id: string }, PostbackDeliverySubscription>);
  }

  public onData: PostbackDeliverySubscription = async (
    delivery: PostbackDelivery,
    msg: AMQPMessage,
    { ack, nack, publish },
  ): Promise<void> => {
    const throttledPublish = traceAsyncCall(Throttle(publish, 0, 1), { name: 'throttledPublish' });

    const payload = delivery?.postback?.payload as any;
    let signature: Partial<RequestSigningHeaders>;

    const client = await delivery.postback.getClient(payload);

    if (client) {
      const { url } = delivery;
      signature = RequestUtil.sign(client.clientSecret, {
        url,
        body: JSON.stringify(delivery.postback.payload),
        method: 'POST',
      });

      this.logger.info(`${this.options.name} Successfully sent Postback signed by clientId: ${client.id}`);
    } else {
      this.logger.warn(`${this.options.name} Successfully sent Postback without signature`);
    }

    try {
      // TODO: Should tell in tell in headers the URL that is being used in the signature?
      const response = await this.delivery.send(delivery.url, payload, {
        headers: { ...signature },
      });
      await delivery.recordSuccessAttempt(
        fclone({
          data: response?.data || fclone(response),
          status: response?.status,
        }),
      );
      return ack();
    } catch (exception) {
      Logger.getInstance().warn(
        `${this.options.name} Could not deliver postback. `,
        exception?.response?.data || exception.message,
      );
      ack();

      const hasRemainingRetries = await delivery.recordFailedAttempt(
        fclone({
          message: exception?.response?.message || exception?.message,
          data: exception?.response?.data || exception?.data || fclone(exception),
          status: exception?.response?.status || exception?.status,
        }),
      );

      if (hasRemainingRetries) {
        // Send the message back to the end of the queue
        await throttledPublish(PostbackDeliveryRoutes.POSTBACK, delivery);
      }
    }
  };
}
