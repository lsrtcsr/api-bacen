import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import MainDatabase from '../MainDatabase';
import TimescaleDatabase from '../TimescaleDatabase';

export interface DatabaseGroupOptions extends ComponentGroupOptions {}

// Prepare the database instance as soon as possible to prevent clashes in
// model registration. We can connect to the real database later.
MainDatabase.getInstance();
TimescaleDatabase.getInstance();

export class DatabaseGroup extends ComponentGroup {
  protected static instance: DatabaseGroup;

  constructor(options: DatabaseGroupOptions) {
    const { children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'DatabaseGroup',
      children: [...children, MainDatabase.getInstance(), TimescaleDatabase.getInstance()],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Database group manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: DatabaseGroupOptions) {
    const instance = new DatabaseGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
