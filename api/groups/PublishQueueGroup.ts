import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import Config from '../../config';
import {
  AMQPService,
  ConsumerPipelinePublisher,
  TransactionPipelinePublisher,
  PostbackDeliveryService,
  MediatorPipelinePublisher,
  EventPipelinePublisher,
  InvoicePipelinePublisher,
  PeriodPipelinePublisher,
  PostbackDeliveryPublisher,
} from '../services';

export interface PublishQueueGroupOptions extends ComponentGroupOptions {}

export class PublishQueueGroup extends ComponentGroup {
  protected static instance: PublishQueueGroup;

  constructor(options: PublishQueueGroupOptions) {
    const { children = [], ...otherOptions } = options || {};

    const logger = otherOptions.logger || Logger.getInstance();
    const amqp = AMQPService.initialize({ ...Config.queue.amqp, logger });

    super({
      logger,
      ...otherOptions,
      name: 'PublishQueueGroup',
      children: [
        /* Base services */
        amqp,

        // Prepare pipeline queue services
        ConsumerPipelinePublisher.initialize({ amqp, logger }),
        MediatorPipelinePublisher.initialize({ amqp, logger }),
        TransactionPipelinePublisher.initialize({ amqp, logger }),

        // Prepare postback queue services
        PostbackDeliveryPublisher.initialize({ amqp, logger }),

        // Postback delivery service
        PostbackDeliveryService.initialize({ logger }),

        // Billing framework queues services
        EventPipelinePublisher.initialize({ amqp, logger }),
        InvoicePipelinePublisher.initialize({ amqp, logger }),
        PeriodPipelinePublisher.initialize({ amqp, logger }),

        /* Add other children from options */
        ...children,
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: PublishQueueGroupOptions) {
    const instance = new PublishQueueGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
