import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import { DomainRole } from '@bacen/base-sdk';
import Config from '../../config';
import * as Jobs from '../jobs';

export interface StartupJobManagerOptions extends ComponentGroupOptions {
  k8s?: boolean;
}

export class StartupJobManager extends ComponentGroup {
  protected static instance: StartupJobManager;

  constructor(options: StartupJobManagerOptions) {
    const { k8s = false, children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'StartupJobManager',
      children: [
        ...children,
        new Jobs.OAuthClientJob({
          k8s,
          logger,
          clients: Config.oauth.clients,
        }),
        new Jobs.RootAdminJob({
          logger,
          rootAdmin: Config.seed.rootAdmin,
        } as Jobs.RootAdminJobOptions),
        new Jobs.OtherAssetsHeldInCustodyJob({
          logger,
        }),
        new Jobs.RootDomainJob({
          logger,
          rootDomain: Config.seed.rootDomain,
          rootMediator: Config.seed.rootMediator,
        }),
        new Jobs.PlatformServicesJob({
          logger,
          platformServicesSettings: Config.seed.platformServices,
        } as Jobs.PlatformServicesJobOptions),
        new Jobs.DefaultBillingPlanJob({
          logger,
          supplier: DomainRole.ROOT,
        } as Jobs.DefaultBillingPlanJobOptions),
        new Jobs.DefaultDomainJob({
          logger,
          defaultDomain: Config.seed.defaultDomain,
        }),
        new Jobs.DefaultBillingPlanJob({
          logger,
          supplier: DomainRole.DEFAULT,
        } as Jobs.DefaultBillingPlanJobOptions),
        new Jobs.DefaultAlertsSetupJob({
          logger,
          defaultAlerts: Config.seed.defaultAlerts,
        } as Jobs.DefaultAlertsSetupJobOptions),
        new Jobs.TransitoryAccountJob({
          logger,
        }),
        new Jobs.DefaultLegalTermsJob({
          logger,
          defaultLegalTerms: Config.seed.legalTerms,
        } as Jobs.DefaultLegalTermsJobOptions),
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Startup Job manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: StartupJobManagerOptions) {
    const instance = new StartupJobManager(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
