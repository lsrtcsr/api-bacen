import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import Config from '../../../config';
import { AMQPService, PostbackDeliverySubscriber } from '../../services';

export interface PostbackSubscribeQueueGroupOptions extends ComponentGroupOptions {}

export class PostbackSubscribeQueueGroup extends ComponentGroup {
  protected static instance: PostbackSubscribeQueueGroup;

  constructor(options: PostbackSubscribeQueueGroupOptions) {
    const { children = [], ...otherOptions } = options || {};

    const logger = otherOptions.logger || Logger.getInstance();
    const amqp = AMQPService.initialize({ ...Config.queue.amqp, logger });

    super({
      logger,
      ...otherOptions,
      name: 'PostbackSubscribeQueueGroup',
      children: [
        /* Base services */
        amqp,

        // Prepare postback queue services
        PostbackDeliverySubscriber.initialize({ amqp, logger }),

        /* Add other children from options */
        ...children,
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: PostbackSubscribeQueueGroupOptions) {
    const instance = new PostbackSubscribeQueueGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
