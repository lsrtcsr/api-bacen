import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import Config from '../../../config';
import { AMQPService, ConsumerPipelineSubscriber, MediatorPipelineSubscriber } from '../../services';

export interface ConsumerSubscribeQueueGroupOptions extends ComponentGroupOptions {}

export class ConsumerSubscribeQueueGroup extends ComponentGroup {
  protected static instance: ConsumerSubscribeQueueGroup;

  constructor(options: ConsumerSubscribeQueueGroupOptions) {
    const { children = [], ...otherOptions } = options || {};

    const logger = otherOptions.logger || Logger.getInstance();
    const amqp = AMQPService.initialize({ ...Config.queue.amqp, logger });

    super({
      logger,
      ...otherOptions,
      name: 'ConsumerSubscribeQueueGroup',
      children: [
        /* Base services */
        amqp,

        // Prepare pipeline queue services
        ConsumerPipelineSubscriber.initialize({ amqp, logger }),
        MediatorPipelineSubscriber.initialize({ amqp, logger }),

        /* Add other children from options */
        ...children,
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: ConsumerSubscribeQueueGroupOptions) {
    const instance = new ConsumerSubscribeQueueGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
