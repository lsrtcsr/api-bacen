import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import Config from '../../../config';
import {
  AMQPService,
  EventPipelineSubscriber,
  InvoicePipelineSubscriber,
  PeriodPipelineSubscriber,
} from '../../services';

export interface BillingSubscribeQueueGroupOptions extends ComponentGroupOptions {}

export class BillingSubscribeQueueGroup extends ComponentGroup {
  protected static instance: BillingSubscribeQueueGroup;

  constructor(options: BillingSubscribeQueueGroupOptions) {
    const { children = [], ...otherOptions } = options || {};

    const logger = otherOptions.logger || Logger.getInstance();
    const amqp = AMQPService.initialize({ ...Config.queue.amqp, logger });

    super({
      logger,
      ...otherOptions,
      name: 'BillingSubscribeQueueGroup',
      children: [
        /* Base services */
        amqp,

        // Prepare billing queue services
        EventPipelineSubscriber.initialize({ amqp, logger }),
        InvoicePipelineSubscriber.initialize({ amqp, logger }),
        PeriodPipelineSubscriber.initialize({ amqp, logger }),

        /* Add other children from options */
        ...children,
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: BillingSubscribeQueueGroupOptions) {
    const instance = new BillingSubscribeQueueGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
