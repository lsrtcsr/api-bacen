import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import Config from '../../../config';
import { AMQPService, TransactionPipelineSubscriber } from '../../services';

export interface TransactionSubscribeQueueGroupOptions extends ComponentGroupOptions {}

export class TransactionSubscribeQueueGroup extends ComponentGroup {
  protected static instance: TransactionSubscribeQueueGroup;

  constructor(options: TransactionSubscribeQueueGroupOptions) {
    const { children = [], ...otherOptions } = options || {};

    const logger = otherOptions.logger || Logger.getInstance();
    const amqp = AMQPService.initialize({ ...Config.queue.amqp, logger });

    super({
      logger,
      ...otherOptions,
      name: 'TransactionSubscribeQueueGroup',
      children: [
        /* Base services */
        amqp,

        // Prepare pipeline queue services
        TransactionPipelineSubscriber.initialize({ amqp, logger }),

        /* Add other children from options */
        ...children,
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("AMQP service manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: TransactionSubscribeQueueGroupOptions) {
    const instance = new TransactionSubscribeQueueGroup(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
