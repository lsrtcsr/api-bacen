import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import * as Jobs from '../../jobs';

export interface BillingPipelineJobManagerOptions extends ComponentGroupOptions {}

export class BillingPipelineJobManager extends ComponentGroup {
  protected static instance: BillingPipelineJobManager;

  constructor(options: BillingPipelineJobManagerOptions) {
    const { children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'BillingPipelineJobManager',
      children: [
        ...children,
        new Jobs.EventPipelineJob({ logger }),
        new Jobs.InvoicePipelineJob({ logger }),
        new Jobs.PeriodPipelineJob({ logger }),
        new Jobs.InvoiceClosingJob({ logger }),
        new Jobs.InvoicePeriodClosingJob({ logger }),
        new Jobs.InvoiceEntryTypeManagementJob({ logger }),
        new Jobs.FinishExpiredSubscriptionsJob({ logger }),
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Pipeline Job manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: BillingPipelineJobManagerOptions) {
    const instance = new BillingPipelineJobManager(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
