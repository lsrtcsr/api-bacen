import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import * as Jobs from '../../jobs';

export interface PostbackPipelineJobManagerOptions extends ComponentGroupOptions {}

export class PostbackPipelineJobManager extends ComponentGroup {
  protected static instance: PostbackPipelineJobManager;

  constructor(options: PostbackPipelineJobManagerOptions) {
    const { children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'PostbackPipelineJobManager',
      children: [...children, new Jobs.PostbackDeliveryJob({ logger })],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Postback pipeline Job manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: PostbackPipelineJobManagerOptions) {
    const instance = new PostbackPipelineJobManager(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
