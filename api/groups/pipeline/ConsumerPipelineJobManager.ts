import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import * as Jobs from '../../jobs';

export interface ConsumerPipelineJobManagerOptions extends ComponentGroupOptions {}

export class ConsumerPipelineJobManager extends ComponentGroup {
  protected static instance: ConsumerPipelineJobManager;

  constructor(options: ConsumerPipelineJobManagerOptions) {
    const { children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'ConsumerPipelineJobManager',
      children: [
        ...children,
        new Jobs.MediatorPipelineJob({ logger }),
        new Jobs.ConsumerPipelineJob({ logger }),
        new Jobs.KYCRefreshJob({ logger }),
      ],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Consumer pipeline Job manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: ConsumerPipelineJobManagerOptions) {
    const instance = new ConsumerPipelineJobManager(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
