import { BaseError, ComponentGroup, ComponentGroupOptions, Logger } from 'ts-framework-common';
import * as Jobs from '../../jobs';

export interface TransactionPipelineJobManagerOptions extends ComponentGroupOptions {}

export class TransactionPipelineJobManager extends ComponentGroup {
  protected static instance: TransactionPipelineJobManager;

  constructor(options: TransactionPipelineJobManagerOptions) {
    const { children = [], ...otherOptions } = options || {};
    const logger = otherOptions.logger || Logger.getInstance();

    super({
      logger,
      ...otherOptions,
      name: 'TransactionPipelineJobManager',
      children: [...children, new Jobs.TransactionPipelineJob({ logger })],
    });
  }

  public static getInstance() {
    if (!this.instance) {
      throw new BaseError("Pipeline Job manager is invalid or hasn't been initialized yet");
    }
    return this.instance;
  }

  public static initialize(options: TransactionPipelineJobManagerOptions) {
    const instance = new TransactionPipelineJobManager(options);

    if (!this.instance) {
      this.instance = instance;
    }

    return instance;
  }
}
