import { PaymentSchema } from '@bacen/base-sdk';
import { ServerApi } from 'stellar-sdk';
import * as currency from 'currency.js';
import { Payment, Wallet, Transaction } from '../../models';

export interface StatementSchema {
  payments: StatementEntrySchema[];
  balance: string;
}

export interface StatementEntrySchema {
  entryType: 'debit' | 'credit';
  from: string;
  to: string;
  asset: string;
  amount: string;
  createdAt: string;
  transaction: string | 'not found';
  hash: string;
  amountMatches: boolean;
  payment: PaymentSchema;
}

export class Statement implements StatementSchema {
  payments: StatementEntry[];

  balance: string;

  public constructor(schema: Partial<StatementSchema> = {}) {
    Object.assign(this, schema);
  }

  public static from(payments: StatementEntry[]) {
    return new Statement({
      payments,
      balance: payments
        .reduce((accumulator, current) => {
          return current.entryType === 'debit' ? accumulator.subtract(current.amount) : accumulator.add(current.amount);
        }, currency(0, { precision: 7 }))
        .format(false),
    });
  }

  public toJSON() {
    return {
      balance: this.balance,
      payments: this.payments.map(payment => payment.toJSON()),
    };
  }
}

export class StatementEntry implements StatementEntrySchema {
  entryType: 'debit' | 'credit';

  from: string;

  to: string;

  asset: string;

  amount: string;

  createdAt: string;

  transaction: string | 'not found';

  hash: string;

  amountMatches: boolean;

  payment: Payment;

  public constructor(schema: Partial<StatementEntrySchema> = {}) {
    Object.assign(this, schema);
  }

  public static async from(record: ServerApi.PaymentOperationRecord, wallet: Wallet): Promise<StatementEntry> {
    const entryType = record.source_account === wallet.stellar.publicKey ? 'debit' : 'credit';
    const from = entryType === 'debit' ? wallet : await Wallet.getByPublicKey(record.from);
    const to = entryType === 'credit' ? wallet : await Wallet.getByPublicKey(record.to);

    const transaction = await Transaction.getByStellarTransactionHash(record.transaction_hash);
    let payment: Payment;
    let amountMatches: boolean;
    if (transaction) {
      payment = transaction.payments.find(payment => Number(payment.amount) === Number(record.amount));
      amountMatches = Number(record.amount) === Number(payment.amount);
    }

    return new StatementEntry({
      amountMatches,
      entryType,
      from: from?.id || record.from,
      to: to?.id || record.to,
      asset: record.asset_code,
      amount: record.amount,
      createdAt: record.created_at,
      transaction: (transaction && transaction.id) || 'not found',
      hash: record.transaction_hash,
      payment: ((payment && payment.toJSON()) as any) || undefined,
    });
  }

  public toJSON() {
    return {
      entryType: this.entryType,
      from: this.from,
      to: this.to,
      asset: this.asset,
      amount: this.amount,
      createdAt: this.createdAt,
      transaction: this.transaction,
      hash: this.hash,
      amountMatches: this.amountMatches,
      payment: this.payment,
    };
  }
}
