export enum OperationType {
  WITHDRAWAL = 'withdrawal',
  DEPOSIT = 'deposit',
  P2P = 'p2p',
}

export enum PaymentType {
  BANKTRANSFER = 'banktransfer',
  BOLETO = 'boleto',
  P2P = 'p2p',
}

export enum AdjustmentType {
  CREDIT = 'credit',
  DEBIT = 'debit',
}

export interface OfflinePaymentRequest {
  providerId: string;
  adjustmentTypeId: number;
  adjustmentType?: AdjustmentType;
  accountId: string;
  amount: number;
  transactionDate?: Date;
  transactionId: string;
  sentManually?: boolean;
  responsible?: string;
  reason?: string;
}

export interface OfflinePaymentResponse {
  sourceId?: string;
  destinationId?: string;
  assetId?: string;
  amount: string;
  type: PaymentType;
  operation: OperationType;
}
