import { BasePostback, CustomPostbackSchema } from '@bacen/base-sdk';
import { IsNotEmpty, ValidateNested, IsString } from 'class-validator';
import { Expose, Type } from 'class-transformer';
import { BaseDto } from '../base';

export class CustomPostbackDto extends BaseDto implements CustomPostbackSchema {
  @IsString()
  @IsNotEmpty()
  @Expose()
  entityType: any;

  @IsString()
  @IsNotEmpty()
  @Expose()
  entityId: string;

  @IsString()
  @IsNotEmpty()
  @Expose()
  domainId?: string;

  @IsNotEmpty()
  @ValidateNested()
  @Expose()
  payload: any;
}
