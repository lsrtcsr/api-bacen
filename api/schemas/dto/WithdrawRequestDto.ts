import { Banking, Arrangement, PaymentPriority } from '@bacen/base-sdk';
import {
  IsOptional,
  IsString,
  IsNumberString,
  IsDefined,
  IsNotEmpty,
  IsUUID,
  ValidateNested,
  IsDate,
  ValidationOptions,
  IsEnum,
} from 'class-validator';
import { ClassTransformOptions, Expose, Type } from 'class-transformer';
import { BaseDto } from './base';
import { IsFutureDate } from '../../validations';
import Config from '../../../config';

export class ExtraData {
  @Expose()
  @IsOptional({ always: true })
  @IsNumberString({ groups: ['bankingId', 'bank'] })
  @IsString({ groups: ['pix'] })
  public purpose?: string;

  @Expose()
  @IsOptional({ always: true })
  @IsDate({ always: true })
  @IsFutureDate({ always: true })
  @Type(() => Date)
  public scheduleFor?: Date;

  @Expose()
  @IsOptional({ always: true })
  public creditAgreementId?: string;

  @Expose()
  @IsOptional({ always: true })
  @IsEnum(() => PaymentPriority)
  public priority?: PaymentPriority;

  @Expose()
  @IsOptional({ always: true })
  @IsString({ always: true })
  public accountExternalId?: string;

  @Expose()
  @IsOptional({ always: true })
  @IsString({ always: true })
  public remittanceInfo?: string;
}

export class WithdrawRequestDto extends BaseDto {
  @Expose()
  @IsOptional({ always: true })
  @IsString({ always: true })
  public asset: string;

  @Expose()
  @IsNotEmpty({ always: true })
  @IsNumberString({ always: true })
  // maior que zero.
  public amount: string;

  @Expose()
  @IsOptional({ always: true })
  @IsEnum(() => Arrangement)
  public arrangement?: Arrangement;

  @Expose()
  @IsOptional({ groups: ['bank', 'bankingId'] })
  @IsString({ always: true })
  // create custom validator
  public key?: string;

  @Expose()
  @IsOptional({ groups: ['bank', 'pix'] })
  @IsDefined({ always: true, message: 'Either bank or bankingId should be defined' })
  @IsUUID('4', { groups: ['bankingId'] })
  public bankingId?: string;

  @Expose()
  @IsOptional({ groups: ['bankingId', 'pix'] })
  @IsDefined({ always: true, message: 'Either bank or bankingId should be defined' })
  @ValidateNested({ groups: ['bank'] })
  @Type(() => Banking)
  public bank?: Banking;

  @Expose()
  @IsOptional({ always: true })
  @ValidateNested({ always: true })
  @Type(() => ExtraData)
  public extra?: ExtraData;

  @Expose()
  @IsOptional({ always: true })
  public additionalData?: any;

  public static create(
    data: Partial<WithdrawRequestDto>,
    transformOptions?: ClassTransformOptions,
    validationOptions?: ValidationOptions,
  ): Promise<WithdrawRequestDto> {
    let groups = ['bank', 'bankingId'].filter(group => data[group]);

    if (data?.arrangement === Arrangement.PIX || Config.spb.defaultArrangement === Arrangement.PIX) groups.push('pix');

    // This is a workaround for requiring that at least one of bank or bankingId should be defined, as class-validator doesn't
    // handle well validations depending on more than one field.
    // If neither field of 'bank' and 'bankingId' is specified we set a dummy group "neither" which doesn't really have any
    // validations associated with it just so we trigger the isDefined validation on bankingId and bank all while not triggering
    // the isOptional validations associated with them.
    groups = groups.length ? groups : ['neither'];

    if (data.bank) {
      groups = groups.concat(data.bank.holderType);
    }

    return this.transformAndValidate(data, transformOptions, { groups, ...validationOptions });
  }
}
