import { validate, ValidationOptions, ValidationError } from 'class-validator';
import { classToClass, ClassTransformOptions } from 'class-transformer';
import { ObjectType } from 'typeorm';

export class BaseDto {
  public validate(options?: ValidationOptions): Promise<ValidationError[]> {
    return validate(this, options);
  }

  public static transform<T extends BaseDto>(
    this: ObjectType<T>,
    data: Partial<T>,
    options?: ClassTransformOptions,
  ): T {
    return classToClass(new (this as new (...args: any) => T)(data), options);
  }

  public static async transformAndValidate<T extends BaseDto>(
    this: ObjectType<T>,
    data: Partial<T>,
    transformOptions: ClassTransformOptions = { excludeExtraneousValues: true },
    validateOptions?: ValidationOptions,
  ): Promise<T> {
    const obj: T = BaseDto.transform.call(this, data, transformOptions);
    const validationErrors = await obj.validate(validateOptions);

    if (validationErrors.length > 0) throw validationErrors;

    return obj;
  }

  // DTOs should never be instantiated using the constructor, neither sould any subclass of BaseDto implement their own constructor, but we're not
  // actually able to enforce that.
  protected constructor(data: any) {
    Object.assign(this, data);
  }
}
