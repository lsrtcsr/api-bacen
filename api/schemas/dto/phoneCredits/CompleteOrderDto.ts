import { IsNumberString, IsUUID, IsOptional, IsEnum } from 'class-validator';
import { CustodyProvider } from '@bacen/base-sdk';
import { Expose } from 'class-transformer';
import { BaseDto } from '../base';

export class CompleteOrderDto extends BaseDto {
  @Expose()
  @IsNumberString()
  amount: string;

  @Expose()
  @IsUUID()
  source: string;

  @Expose()
  @IsEnum(CustodyProvider)
  @IsOptional()
  provider: CustodyProvider;
}
