import { Expose } from 'class-transformer';
import { IsEnum, IsOptional, IsUUID, IsString, IsNumberString, MaxLength, Length } from 'class-validator';
import { CustodyProvider } from '@bacen/base-sdk';
import { BaseDto } from '../base';

export class CreateOrderDto extends BaseDto {
  @Expose()
  @IsEnum(CustodyProvider)
  @IsOptional()
  provider: CustodyProvider;

  @Expose()
  @IsUUID()
  source: string;

  @Expose()
  @Length(2, 2)
  @IsNumberString()
  phoneCode: string;

  @Expose()
  @Length(8, 9)
  @IsNumberString()
  phoneNumber: string;

  @Expose()
  @MaxLength(3)
  @IsNumberString()
  providerCode: string;
}
