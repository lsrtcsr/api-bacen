import { AuthorizerEvent, PaymentType, TransactionAdditionalData, TransitoryAccountType } from '@bacen/base-sdk';
import { IsEnum, IsObject, IsOptional, IsString, IsUUID, IsArray } from 'class-validator';

export class AuthorizationRequestDto {
  @IsEnum(AuthorizerEvent)
  event: AuthorizerEvent;

  @IsUUID()
  walletId?: string;

  @IsOptional()
  @IsEnum(TransitoryAccountType)
  transitoryAccountType?: TransitoryAccountType;

  @IsObject()
  @IsOptional()
  additionalData?: Partial<TransactionAdditionalData>;

  // ----- Authorization field ----- //

  @IsEnum(PaymentType, { groups: [AuthorizerEvent.AUTHORIZATION] })
  type?: PaymentType;

  @IsString({ groups: [AuthorizerEvent.AUTHORIZATION] })
  asset?: string;

  @IsString({ groups: [AuthorizerEvent.AUTHORIZATION] })
  amount?: string;

  @IsUUID('4', { groups: [AuthorizerEvent.SETTLEMENT, AuthorizerEvent.REFUND] })
  transactionId?: string;
}
