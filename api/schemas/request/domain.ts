import { DomainSettingType } from '../../models';

export interface DomainSettingRequestSchema {
  type: DomainSettingType;
  value: number;
}
