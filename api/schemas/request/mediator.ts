export interface CompanyData {
  name: string;
  fantasyName?: string;
  taxId: string;
  stateRegistration?: string;
  openingDate: Date;
  comercialOrigin: number;
  bankNumber?: number;
  agencyNumber?: number;
  accountNumber?: string;
  email: string;
  billingDay: number;
  printedName?: string;
  revenue?: number;
  inputChannel?: string;
  s ?: number;
  globalLimit?: number;
  maxLimit?: number;
  installmentLimit?: number;
  phones: Phone[];
  addresses: Address[];
  partners: CompanyPartner[];
}

export interface CompanyPartner {
  taxId: string;
  rgEmissionDate: string;
  birthDate: string;
  email: string;
  civilState: string;
  nationality: string;
  name: string;
  rg: string;
  gender: string;
  rgState: string;
  revenue: number;
  job: string;
  rgEmissionEntity: string;
  phones: Phone[];
}

export interface Phone {
  type: number;
  regionalCode: string;
  number: string;
}

export interface Address {
  type: number;
  cep: string;
  street: string;
  number?: number;
  neighborhood?: string;
  complement?: string;
  reference?: string;
  city: string;
  state: string;
  country?: string;
  mailAddress: boolean;
}
