import { PaymentRequestSchema } from '@bacen/base-sdk';
import { IsArray, ArrayMaxSize, IsOptional } from 'class-validator';
import { IsNotRootWallet, IsNotRootDestination } from '../../utils/ValidationUtil';

export class PaymentRequest implements PaymentRequestSchema {
  @IsNotRootWallet()
  source: string;

  @IsArray()
  @ArrayMaxSize(5, {
    message: 'Payment list maximum size is 5',
  })
  @IsNotRootDestination({
    each: true,
  })
  recipients: {
    asset: string;
    amount: string;
    destination: string;
  }[];

  @IsOptional()
  extra?: { [key: string]: string | number };

  public constructor(schema: Partial<PaymentRequestSchema> = {}) {
    Object.assign(this, schema);
  }
}
