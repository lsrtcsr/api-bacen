import * as moment from 'moment';
import { EntityManager } from 'typeorm';
import { ServiceType } from '@bacen/base-sdk';
import { Payment, EventType, User, Plan, EntryClazz, BillingSettingsSchema } from '../../models';

export interface ServiceConsumptionData {
  userId?: string;
  type?: ServiceType;
  event?: any;
  extra?: any;
  calculationRule?: Function;
  eventDate?: moment.Moment;
  payment?: Payment;
  settled?: boolean;
  liability?: 'consumer' | 'mediator';
  error?: any;
}

export interface PlanRequestSchema {
  name: string;
  validFrom: moment.Moment;
  items: PlanServiceSchema[];
  billingSettings: BillingSettingsSchema;
  default: boolean;
}

export interface PlanServiceSchema {
  serviceId: string;
  name?: string;
  price?: number;
  entryClazz?: EntryClazz;
  billingTrigger?: EventType;
  freeUntil?: number;
  validFrom?: moment.Moment;
  validUntil?: moment.Moment;
  entryCalculationMode?: 'absolute' | 'price_percentage' | 'transaction_percentage';
  value?: number;
  liability?: 'bacen' | 'provider';
  reverseOnFail?: boolean;
  rule?: {
    thresholdCheckCalculationMode: 'transaction_count' | 'transaction_amount';
    excludeServiceFee?: boolean;
    threshold: number;
    maxNumberOfEntries?: number;
  };
}

export interface ServiceRequestSchema {
  name: string;
  description?: string;
  type: ServiceType;
}

export interface SubscriptionRequestSchema {
  user: User;
  plan?: Plan | string;
  prepaid?: boolean;
  validFrom?: moment.Moment;
  transaction?: EntityManager;
}
