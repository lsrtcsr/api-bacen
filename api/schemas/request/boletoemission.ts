import { UserSchema } from '@bacen/base-sdk';

export interface BoletoEmitRequestSchema {
  destination: string;
  amount: string;
  extra: {
    expiresAt?: Date;
    source?: UserSchema;
  };
}
