import * as express from 'express';
import { ServerOptions } from 'ts-framework';
import { AmqpService } from '@bacen/events';
import Config from '../config';
import { PublishQueueGroup, StartupJobManager } from './groups';
import { ImpersonateGrantType, SecretTokenGrantType } from './helpers';
import MainServer, { MainServerOptions } from './MainServer';
import { AssetService, SignerService } from './services';
import * as Events from './events';
import * as Consumers from './consumers';

const { logger } = Config.server;

// Initialize job manager groups
StartupJobManager.initialize({ logger, k8s: process.env.K8S_ENABLED === 'true' });

export interface StartupServerOptions extends MainServerOptions {}

export default class StartupServer extends MainServer {
  public options: StartupServerOptions;

  constructor(options?: StartupServerOptions, app: express.Application = express()) {
    const { children = [], router = {}, ...otherOptions } = { ...Config.server, ...options };
    const components = [
      /* Services */
      AssetService.getInstance(),
      SignerService.getInstance(),

      /* Pipeline Services */
      PublishQueueGroup.getInstance(),

      AmqpService.initialize({
        logger,
        connectionUrl: Config.queue.amqp.host,
        consumers: [
          Consumers.RegisterAssetConsumer,
          Consumers.RegisterRootMediatorAssetsConsumer,
          Consumers.RegisterWalletOnStellarConsumer,
          Consumers.ApproveAssetRegistrationConsumer,
        ],
        events: Object.values(Events),
        skipConsume: true,
      }),

      /* Job manager */
      StartupJobManager.getInstance(),
    ];

    // Setup priority routes, before controller router
    app.get('/', (_req, res) => res.redirect('/status'));

    // Initialize the base service with the configurations
    super(
      {
        children: [...children, ...components],
        ...otherOptions,
        router: {
          ...router,
          controllers: require('./controllers').default,
          oauth: {
            useErrorHandler: true,
            allowExtendedTokenAttributes: true,
            model: require('./models/oauth2/middleware').default,
            token: {
              allowExtendedTokenAttributes: true,
              extendedGrantTypes: {
                secret_token: SecretTokenGrantType,
                impersonate: ImpersonateGrantType,
              },
            },
          },
        },
      } as ServerOptions,
      app,
    );
  }

  async listen(): Promise<any> {
    await super.onInit();
    await super.onReady();
  }
}
