select
        w.id       as wallet_origem
      , t.id       as transaction_id
      , ts.id      as transactionS_id
      , tp.amount  as valor_origem
      , tp.type    as tipo_origem
      , destino.id as wallet_destino
      , tp.status  as status_origem
from transactions t
         join transaction_states ts on ts."transactionId" = t.id
         join wallets w on t."sourceId" = w.id
         join payments tp on t.id = tp."transactionId"
         join wallets destino on tp."destinationId" = destino.id
where tp.status = 'settled' -- authorized
  and (w.id = 'bf1a40ae-b346-447b-aa91-47685c83fd69' or destino.id = 'bf1a40ae-b346-447b-aa91-47685c83fd69');