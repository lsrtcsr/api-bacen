import '../config/gcloud.config';

// Import has collateral effects, should not be on top
import Config from '../config';
import MainServer from '../api/MainServer';

process.on('unhandledRejection', (r) => console.error(r));

require('source-map-support').install();
require('reflect-metadata');
require('@bacen/shared-sdk').MemoryLeakUtil.initialize({ logger: Config.logger, jobName: ' -api' });

if (process.env.NEW_RELIC_KEY) {
  require('newrelic');
}

const server = new MainServer();

// Start listening for requests...
server.listen().catch((error) => {
  console.error(error);
  process.exit(-1);
});
