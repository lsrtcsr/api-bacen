import '../../config/gcloud.config';

import Config from '../../config';
import { TransactionWorkersServer } from '../../api/workers';

require('source-map-support').install();
require('reflect-metadata');
require('@bacen/shared-sdk').MemoryLeakUtil.initialize({
  logger: Config.logger,
  jobName: ' -api.transaction-workers',
});

if (process.env.NEW_RELIC_KEY) {
  require('newrelic');
}

const server = new TransactionWorkersServer();

// Start listening for requests...
server.onInit().catch((error) => {
  console.error(error);
  process.exit(-1);
});
