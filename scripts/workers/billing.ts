import '../../config/gcloud.config';

import Config from '../../config';
import { BillingWorkersServer } from '../../api/workers';

process.on('unhandledRejection', (r) => console.error(r));

require('source-map-support').install();
require('reflect-metadata');
require('@bacen/shared-sdk').MemoryLeakUtil.initialize({
  logger: Config.logger,
  jobName: ' -api.billing-workers',
});

if (process.env.NEW_RELIC_KEY) {
  require('newrelic');
}

const server = new BillingWorkersServer();

process.on('unhandledRejection', (r) => console.error(r));

// Start listening for requests...
server.onInit().catch((error) => {
  console.error(error);
  process.exit(-1);
});
