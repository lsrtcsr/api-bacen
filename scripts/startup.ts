import '../config/gcloud.config';

// Import has collateral effects, should not be on top
import StartupServer from '../api/StartupServer';

process.on('unhandledRejection', r => console.error(r));

require('source-map-support').install();
require('reflect-metadata');

const server = new StartupServer();

// Start listening for requests...
server
  .listen()
  .then(() => {
    server.logger.info('Startup routines completed successfully');
    process.exit(0);
  })
  .catch(error => {
    console.error(error);
    process.exit(-1);
  });
