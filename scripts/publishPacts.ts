#!/usr/bin/env node
import * as path from 'path';
import { Publisher } from '@pact-foundation/pact-node';
import { Logger } from 'nano-errors';
import * as yargs from 'yargs';

const local = process.env.USER ? `-local_${process.env.USER}` : '';

(async () => {
  const logger = Logger.initialize({ level: process.env.LOG_LEVEL || 'info' });
  const { tags, dryRun = false } = yargs.array('tags').argv as any;

  if (!dryRun) {
    await new Publisher({
      tags,
      pactFilesOrDirs: [path.resolve(process.cwd(), 'pacts')],
      pactBroker: process.env.PACT_BROKER_URL || 'https://pact-nightly.bt .app/',
      consumerVersion: `${require('../package.json').version}${local}`,
    }).publish();
  }
})();
